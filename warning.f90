! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! ***************************************************************************

!> print time stamp preceded by a message 
subroutine time_stamp(message, dtime, ctime, iout)

    use kinds_f90
    use comms_mpi_module, only : master !, idnode

    implicit none

    real(kind = wp), intent(in) :: dtime, ctime
    character*(*),   intent(in) :: message
    integer,         intent(in) :: iout

    real(kind = wp), save :: tlast

    integer :: days, hours, mins 
    real(kind = wp) :: secs

    if( master ) then

        hours = int(dtime/3600.0)
        days  = hours/24
        mins  = int((dtime-hours*3600)/60.0)
        secs  = dtime-hours*3600-mins*60
        hours = hours-days*24

        if( days > 0 ) then

            write(iout,"(//,1x,a,i4,a,2(i3,a),f7.3,a,//)") trim(adjustL(message)), &
                  days,' d :',hours,' h :',mins,' m :',secs,' s'

        !else if( hours > 0 ) then
        !else if( mins  > 0 ) then
        
        else if( dtime > 60.0_wp .and. dtime < 1000.0_wp ) then

            write(iout,"(//,1x,a,2(i3,a),f7.3,a,f7.3,a,//)") trim(adjustL(message)), &
                  hours,' h :',mins,' m :',secs,' s (',dtime,')'

        else 

            write(iout,"(//,1x,a,2(i3,a),f7.3,a,//)") trim(adjustL(message)), &
                  hours,' h :',mins,' m :',secs,' s'

        end if

    end if
    
    tlast = ctime

end subroutine time_stamp


!> give a cry - warning or error message (error code is the last input parameter)
subroutine cry(iout,frmt,msg,ierr)

!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_monte subroutine for throwing messages and, if needed, bringing about
! a controlled termination of the program (when ierr > 0)
!
! adds flexibility of informing the user with specific notes/warnings/errors
!
! copyright - daresbury laboratory
! author    - andrey brukhno october 2015
!
!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    use comms_mpi_module, only : master, idnode
    !use parallel_loop_module, only : idgrp

    implicit none

        !> output file unit to cry to
    integer, intent(in) :: iout

        !> format and message strings
    character(*), intent(in) :: frmt, msg

        !> error id number (0 -> no error)
    integer, intent(in) :: ierr

    !AB: master nodes speak for their entire workgroups 
    !AB: provided every process within a workgroup goes exactly the same route

    if( master ) then

      if( ierr > 0 ) write(iout,'(/,/,1x,a,i3,a)')'Node ',idnode,' :'

      if( trim(frmt) == '*' ) then
      !AB: standard format (compiler dependent!)

        write(iout,*) trim(msg)

      else if( trim(frmt) == '' ) then
      !AB: default format

        write(iout,'(1x,a)') trim(msg)

      else 
      !AB: given format

        write(iout,trim(frmt)) trim(msg)

      end if

      flush(iout)

    end if

    if( ierr > 0 ) call error(ierr)

end subroutine cry


!> write out a warning message with its code and proceed further
subroutine warning(kode)

    use comms_mpi_module, only : master !, exit_mpi_comms
    use constants_module

    implicit None

    integer, Intent( In    ) :: kode

    if( .not. master ) return

    write(uout,'(/,/,1x,a,i5)') 'DL_MONTE WARNING: ', kode

    ! codes 1 - 99 are for warnings associated with controls (CONTROL file)

    ! codes 21 - 30 are for warnings about inconsistencies between cell_type, coultype and such (CONTROL & FIELD files)

    ! codes 100 - 200 are for warnings about molecular topology and force-field (potentials) parameter set up (FIELD file)

    if      (kode ==    1) then

        write(uout,'(/,/,1x,a)') 'angle potential may not be compatible with ensemble'

    else if (kode == 21) then
        ! AB: cell_type == {-10, -20} => NO MFA correction in use
        ! AB: MUST BE complemented by coultype := mfa_type = NO_MFA (-3 currently)

        write(uout,'(/,/,1x,2a)') 'case of 2D-slit - ', &
        'resetting MFA correction to "no MFA" => using simple explicit in-box elecrostatics ("direct" without cut-off)'

    else if (kode == 22) then

        write(uout,'(/,/,1x,2a)') 'case of 2D-slit - ', &
        'resetting MFA arrays dimension due to restrictions (minbins/maxbins)'

    else if (kode == 23) then

        write(uout,'(/,2(/,1x,a),/,/,1x,a)') & 
        'inconsistency between FIELD and CONTROL files:', &
        'charges are present but Coulomb calculations are switched off (due to "noewald ..." directive?)', &
        'NOTE: charges will be totally ignored!'

    else if (kode == 24) then

        write(uout,'(/,2(/,1x,a),/,/,1x,a)') & 
        'inconsistency between FIELD and CONTROL files:', &
        'charges are absent but Coulomb calculations are specified (by default)', &
        'NOTE: switching off Coulomb calculations!'

    else if (kode == 25) then

        write(uout,'(/,/,1x,2a)') 'case of 2D-slit - ', &
        'resetting fractional dZ for MFA due to restrictions (minbins/maxbins)'

    else

        write(uout,'(/,/,1x,a)') 'wrong kode given'

    endif

end subroutine
