! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! *                                                                         *
! *   T.L.Underwood - Lattice/Phase-Switch MC method & optimizations        *
! *   t.l.Underwood[@]bath.ac.uk                                            *
! *                                                                         *
! *   J. Grant - Modifications for Ewald refactor                           *
! *   r.j.grant[@]bath.ac.uk                                                *
! ***************************************************************************

!> @brief
!> - Molecular topology & force-field (interactions, Hamiltonian): initialisation & manipulation, based on input (FIELD file)
!> @usage
!> - `readfield(..)` reads in FIELD file
!> -  other routines deal with memory allocation and various energy calculations
!> - @stdusage
!> - @stdspecs
!> @using
!> - `kinds_f90`
!> - many other modules within routines

!> @modulefor topology & force-field(s): reading FIELD file & allocating max memory

module field_module

    use kinds_f90
    use slit_module, only: in_bulk
    use constants_module

    implicit none

    !> work space for metal potentials
    !TU: There is not a work space for different configurations, which I suspect means that metal potentials
    !TU: will not work correctly if nconfig>1.
    real(kind = wp), allocatable, dimension(:) :: rho

    !> temp work space phase sums
    !real(kind = wp), allocatable, dimension(:,:) :: cossum, sinsum

    !> temp work space rcpsum
    complex(kind = wp), allocatable, dimension(:,:) :: tmprcpsum
    complex(kind = wp), allocatable, dimension(:)   :: tmprcpxatom,  tmprcpyatom,  tmprcpzatom
    complex(kind = wp), allocatable, dimension(:)   :: tmprcpxatom1, tmprcpyatom1, tmprcpzatom1

    ! top capping for vdw energies to prevent numerical overflows due to steep short-range repulsion, default: 1.0e5
    real(kind = wp), save :: vdwcap, eunit

    ! shifted VdW flag
    integer, save :: vdwshift = 0

    !TU: These variables pertain to AB's old implementation of long-range corrections. These are no longer in use,
    !TU: with the exception of 'vdwlrc' which has been renamed 'vdwshift' (see above) to avoid confusion
    ! long-range VdW (dispersion) correction flag
    !integer, save :: vdwlrc = 0
    !logical, save :: is_vdwlrc = .false.


contains


!> @brief
!> - Reads in FIELD file: atomic specs & molecular species topology and interaction parameters
!> - atoms, bonds, angles, dihedrals, VdW etc (force-field)
!> - also allocates the memory needed for the molecule data to be read from CONFIG file
!> @usage 
!> - called once, in the very beginning, from `dl_monte.f90`
!> - @stdusage 
!> - @stdspecs
!> @using 
!> - `kinds_f90`
!> - `parse_module`
!> - `constants_module`
!> - `species_module`
!> - `vdw_module`
!> - `bondlist_module`
!> - `bond_module`
!> - `anglist_module`
!> - `molecule_module`
!> - `angle_module`
!> - `inversion_module`
!> - `dihedral_module`
!> - `thbpotential_module`
!> - `metpot_module`
!> - `tersoff_module`
!> - `comms_mpi_module`, only : master, idnode
!> - `external_potential_module`, only : allocate_extpot_arrays, read_external_pot

!> read in FIELD (topology & force-field) file and allocate (max) memory for all molecular species
subroutine readfield(nconfigs, shortrangecut, vdwcut, energyunit)

    use kinds_f90
    use parse_module
    use constants_module
    use species_module
    use vdw_module
    use bondlist_module
    use bond_module
    use anglist_module
    use molecule_module
    use angle_module
    use inversion_module
    use dihedral_module
    use thbpotential_module
    use metpot_module
    use tersoff_module
    use comms_mpi_module, only : master, idnode
    use external_potential_module, only : allocate_extpot_arrays, read_external_pot

    implicit none

        !> number of configurations (microstates) in CONFIG file
    integer, intent(out) :: nconfigs

        !> short range cut-off radius
    real (kind = wp), intent(out) :: shortrangecut, vdwcut

        !> energy unit to be used for input & output
    real (kind = wp), intent(out) :: energyunit

    integer :: nang, nbond, i, nread, nter
    integer :: npot, nmet, nvdw, nextern

    character :: record*100, word*40
    character, allocatable, dimension(:) :: mname*8 

    logical :: safe, more

    nread = ufld

    energyunit = 1.0_wp

    shortrangecut = 0.0_wp

    vdwcut = 0.0_wp
    vdwcap = 1.0e5_wp

    if (idnode == 0) open(ufld, file = 'FIELD', status = 'old')

 10 call get_line(safe, nread, record)
    if (.not.safe) go to 1000
    call lower_case(record)

    if( record == "" .or. record(1:1) == "#" ) Go To 10

    if( master ) then
        write(uout,"(1x,100('-'),/,1x,a,/,1x,100('-'),/,a)") &
              "FIELD title  (100 symbols max) :","'"//trim(adjustL(record))//"'"
        write(uout,"(1x,100('-'))")
    end if

    call get_word(record, word)

 11 call get_line(safe,nread,record)
    if (.not.safe) Go To 1000
    call lower_case(record)
    call get_word(record, word)

    if( word == "" .or. word(1:1) == "#" ) Go To 11

    if( word /= "cutoff" .or. word(1:4) == "rcut" ) then
    !AB: skipping directive 'cutoff' is not permitted! (wrong assumption in v2.02)

        call error(5)

    else
    !AB: below GLOBAL CUTOFF is read in (VDW cutoff <= global cutoff)

        call get_word(record, word)
        shortrangecut = word_2_real(word)

    !AB: NOTE: VDW cutoff can be read either in 'vdw' directive:"
    !AB: "VDW [types] <int> [ [cutoff <real>] [ecap] <real> [correction] <int> ]"
    !AB: or in CONTROL via directive "vdwcut <real>"
    end if

 22 call get_line(safe,nread,record)
    if (.not.safe) Go To 1000
    call lower_case(record)
    call get_word(record, word)

    if( word == "" .or. word(1:1) == "#" ) Go To 22

    if (word(1:4) /= "unit") then

        call error(6)

    else

        call get_word(record, word)

        if (word(1:4) == "inte") then

            energyunit = 1.0

        else if (word(1:4) == "kcal") then

            energyunit = KCALTOINTERNAL

        else if (word(1:2) == "kj") then

            energyunit = KJTOINTERNAL

        else if (word(1:1) == "k") then

            energyunit = KTOINTERNAL

        else if (word(1:2) == "ev") then

            energyunit = EVTOINTERNAL

        else

            call error(6)

        end if

    end if

    eunit = energyunit

    !read the number of cells
33  call get_line(safe, nread, record)
    if (.not.safe) go to 1000
    call get_word(record, word)

    call lower_case(word)

    if( word == "" .or. word(1:1) == "#" ) Go To 33

    if (word(1:5) /= 'nconf') call error(304)

    call get_word(record, word)
    nconfigs = nint(word_2_real(word))

    if( nconfigs < 1 .or. nconfigs > 999 ) call error(303)


    !read in unique sites
    call readspecies()


    !get the potentials next
    more = .true.

    do while(more)

        call get_line(safe, nread, record)
        if (.not.safe) go to 1000

        call lower_case(record)
        call get_word(record, word)

        if( word == "" .or. word(1:1) == "#" ) cycle

        if(word(1:3) == "vdw") then

            call get_word(record, word)
            if (word(1:5) == "types") call get_word(record, word)
            nvdw = nint(word_2_real(word))

            !AB: look for optional parameters, if any!

            word = trim(adjustL(record))

            if( trim(word) /= "" .and. word(1:1) /= "#" ) then
            !AB: <<-- DL_MONTE-2 new style 'VDW' directive
            !AB: "VDW [types] <int> [ [shift/lrc/disp/corr] [rcut/cutoff <real>] [ecap/repcap] <real> ]"

                !AB: below VdW cutoff is read in (<= global cutoff)
                call get_word(record, word)

                if( word(1:5) == "shift" ) then
                    vdwshift = 1

                    !AB: look for the next optional parameter!
                    call get_word(record, word)

                !TU: AB's old implementation of long-range corrections, which is now no longer in use...
                !else if( word(1:3) == "lrc" .or. word(1:4) == "disp" .or. word(1:4) == "corr" ) then
                !    vdwlrc = 1
                !    is_vdwlrc = .true.
                !
                !    !AB: look for the next optional parameter!
                !    call get_word(record, word)

                end if

                if( trim(word) /= "" .and. word(1:1) /= "#" ) then
                    if( word(1:4) == "rcut" .or. word(1:6) == "cutoff" ) then 

                        call get_word(record, word)
                        vdwcut = word_2_real(word)

                        !AB: look for the next optional parameter!
                        call get_word(record, word)

                    end if
                end if

                !AB: repulsive energy capping
                !call get_word(record, word)
                if( trim(word) /= "" .and. word(1:1) /= "#" ) then

                    if( word(1:4) == "ecap" .or. word(1:6) == "repcap" ) call get_word(record, word)

                    vdwcap = word_2_real(word)

                    call cry(uout,'', &
                             "WARNING: VDW specs with energy capping 'Ecap'/'RepCap' in FIELD input "//&
                            &"' - the value found will be used (assuming internal energy units) !!!",0)

                end if

                !TU: This pertains to AB's implementation of long-range VdW corrections, and was already
                !TU: commented out when I found it. Surely it can be deleted.
                !  !AB: long-range (dispersion) correction flag
                !  call get_word(record, word)
                !  if( trim(word) /= "" .and. word(1:1) /= "#" ) then 
                !      if( word(1:3) == "lrc" .or. word(1:4) == "disp" .or. word(1:4) == "corr" ) then
                !          call get_word(record, word)
                !          vdwlrc = nint(word_2_real(word))
                !          call cry(uout,'', &
                !               "WARNING: VDW specs with long-range correction in FIELD input "//&
                !              &"' - not implemented yet !!!",0)
                !      end if
                !  end if

            !AB: -->> DL_MONTE-2 new style 'VDW' directive
            end if

            !AB: make sure sound VDW cutoff has been specified (done in control_module.f90 upon checks)
            if( vdwcut < 1.0e-10_wp .or. vdwcut > shortrangecut ) vdwcut = shortrangecut

            call allocate_vdw_arrays(nvdw)

            call read_vdw(energyunit, vdwcut, vdwcap, vdwshift)

        else if (word(1:6) == "tersof") then

            call get_word(record, word)
            if (word(1:5) == "types") call get_word(record, word)
            nter = nint(word_2_real(word))

            call allocate_tersoff_arrays(nter)

            call read_tersoff(shortrangecut, energyunit)

        else if (word(1:5) == "metal") then

            call get_word(record, word)
            if (word(1:5) == "types") call get_word(record, word)
            nmet = nint(word_2_real(word))

            call allocate_metal_arrays(nmet)

            call read_metpot(shortrangecut, energyunit)

        else if (word(1:3) == "tbp" .or. word(1:5) == "three") then

            call get_word(record, word)
            if (word(1:5) == "types") call get_word(record, word)
            npot = nint(word_2_real(word))

            call alloc_thbpar_arrays(npot)

            call read_thb_par(energyunit)

        else if (word(1:4) == "bond") then

            call get_word(record, word)
            if (word(1:5) == "types") call get_word(record, word)
            npot = nint(word_2_real(word))

            call allocate_bondpot_arrays(npot)

            call read_bond(shortrangecut, energyunit)

        else if (word(1:5) == "angle") then

            call get_word(record, word)
            if (word(1:5) == "types") call get_word(record, word)
            npot = nint(word_2_real(word))

            call alloc_angpar_arrays(npot)

            call read_angle_par(npot, energyunit)

        else if (word(1:5) == "inver") then

            call get_word(record, word)
            if (word(1:5) == "types") call get_word(record, word)
            npot = nint(word_2_real(word))

            call alloc_invpar_arrays(npot)

            call read_inversion_par(npot, energyunit)

        else if (word(1:5) == "dihed") then

            call get_word(record, word)
            if (word(1:5) == "types") call get_word(record, word)
            npot = nint(word_2_real(word))

            call alloc_dihpar_arrays(npot)

            call read_dihedral_par(npot, energyunit)

        else if (word(1:6) == "extern") then

            call get_word(record, word)
            if (word(1:5) == "types") call get_word(record, word)
            nextern = nint(word_2_real(word))

            call allocate_extpot_arrays(nextern)

            call read_external_pot(energyunit, shortrangecut, vdwcap)

        else if (word(1:5) == "close") then

            more = .false.

        end if

    end do

    return

1000 call error(101)

end subroutine readfield

!> @brief
!> - All neighbour and exclusion lists (arrays) for book-keeping are set here
!> @usage 
!> - in order to be able to use the local (to the molecule) flag blist%npairs 
!! to determine whether an exclist is needed, the bondlist array needs to be 
!! allocated and copied before the exclist.
!> @using 
!> - `kinds_f90`
!> - `cell_module`
!> - `species_module`
!> - `nbrlist_module`, only : alloc_nbrlist_arrays, alloc_exclist_array
!> - `bondlist_module`, only : alloc_and_copy_bondlist_arrays
!> - `thbpotential_module`, only : numthb
!> - `thblist_module`, only : alloc_thblist_arrays
!> - `anglist_module`, only : alloc_and_copy_anglist_arrays
!> - `metpot_module`, only : nmetpot
!> - `molecule_module`, only : alloc_mol_smden_arrays
!> - `invlist_module`, only : alloc_and_copy_invlist_arrays
!> - `dihlist_module`, only : alloc_and_copy_dihlist_arrays

!> allocates the arrays for book-keeping lists: neighbours, bonds, angles, dihedrals, exclusions etc
subroutine alloc_list_arrays(ib, uselist, autolist, max_nonb_nbrs, max_thb_nbrs)

! NOTE: `nbrlist` routine ONLY allocates non-bonded neighbour-list and exclist routine only does `exclist`, 
! so both need to be called if nbrlist in use.

    use kinds_f90
    use cell_module
    use species_module
    use nbrlist_module, only : alloc_nbrlist_arrays, alloc_exclist_array
    use bondlist_module, only : alloc_and_copy_bondlist_arrays
    use thbpotential_module, only : numthb
    use thblist_module, only : alloc_thblist_arrays
    use anglist_module, only : alloc_and_copy_anglist_arrays
    use metpot_module, only : nmetpot
    use molecule_module, only : alloc_mol_smden_arrays
    use invlist_module, only : alloc_and_copy_invlist_arrays
    use dihlist_module, only : alloc_and_copy_dihlist_arrays

    implicit none

    
        !> replica identifier
    integer, intent(in) :: ib

        !> number of non-bonded neighbours
    integer, intent(in) :: max_nonb_nbrs

        !> number of three-body triplets which an atom could be at the centre of
    integer, intent(in) :: max_thb_nbrs

    !TU-: 'autolist' is never used here!
    logical, intent(in) :: uselist, autolist

    integer :: im, nmax, k

    do im = 1, cfgs(ib)%num_mols

        k  = cfgs(ib)%mols(im)%mol_label
        call alloc_and_copy_bondlist_arrays(cfgs(ib)%mols(im)%blist, uniq_mol(k)%blist)

        !allocate memory for excluded list and neighbourlist
        if(uselist) then

            if( max_nonb_nbrs > 0 ) then

                call alloc_nbrlist_arrays(cfgs(ib)%mols(im)%nlist, cfgs(ib)%mols(im)%mxatom, &
                                          max_nonb_nbrs)
            else

                !TU: When this procedure is called in 'montecarlo_module.f90' 'job%max_nonb_nbrs' is 
                !TU: passed as 'max_nonb_nbrs'. 'job%max_nonb_nbrs' is 0 by default, and corresponds to
                !TU: the user having not specified a neighbour list size. Hence if 'max_nonb_nbrs=0' a
                !TU: neighbour list size must be chosen. 'cfgs(ib)%mols(im)%mxatom' is safe.

                call alloc_nbrlist_arrays(cfgs(ib)%mols(im)%nlist, cfgs(ib)%mols(im)%mxatom, &
                                          cfgs(ib)%maxno_of_atoms)

            end if

        end if

        !TU: Allocate the three-body list for the molecule if there are any three-body
        !TU: potentials in use. Note that three-body lists are compulsory with
        !TU: three-body potentials
        if (numthb > 0) call alloc_thblist_arrays(cfgs(ib)%mols(im)%mxatom, max_thb_nbrs, cfgs(ib)%mols(im)%tlist)


        !TU: Sets the exclude list 'matrix' to be of size cfgs(ib)%mols(im)%mxatom x cfgs(ib)%mols(im)%mxatom.
        if( cfgs(ib)%mols(im)%blist%npairs > 0 ) &
            call alloc_exclist_array(cfgs(ib)%mols(im)%nlist, cfgs(ib)%mols(im)%mxatom) 

        call alloc_and_copy_anglist_arrays(cfgs(ib)%mols(im)%alist, uniq_mol(k)%alist)
        call alloc_and_copy_invlist_arrays(cfgs(ib)%mols(im)%ilist, uniq_mol(k)%ilist)
        call alloc_and_copy_dihlist_arrays(cfgs(ib)%mols(im)%dlist, uniq_mol(k)%dlist)

        !storage arrays for rho
        if (nmetpot > 0) call alloc_mol_smden_arrays(cfgs(ib)%mols(im))

    end do

end subroutine alloc_list_arrays

!> @brief
!> - Allocating the global density array
!> @using 
!> - `cell_module`

!> Allocating the global density array
subroutine setden_work()

    use cell_module

    implicit none

    integer :: i, maxat

    maxat = 0

    do i = 1, nconfigs

        if(cfgs(i)%maxno_of_atoms > maxat) maxat = cfgs(i)%maxno_of_atoms

    end do

    allocate(rho(maxat))

end subroutine setden_work

!> Copying the global density array for replica `ib`
subroutine copy_density(ib)

    use cell_module

    implicit none

    integer, intent(in) :: ib

    integer :: im, i, ii

    do im = 1, cfgs(ib)%num_mols

        do i = 1, cfgs(ib)%mols(im)%natom

            ii = cfgs(ib)%mols(im)%glob_no(i)

            cfgs(ib)%mols(im)%smden(i) = rho(ii)

        end do

    end do

end subroutine copy_density

!> Copies the temp atom density to that of molecule
!> It does not do it in parallel so dont have to do another gsum - may change for big cells
subroutine copy_atom_density(ib, im, i)

    use cell_module

    implicit none

    integer, intent(in) :: ib, im, i

    integer :: ii, jm, j, jj, last, nbr
    
    ii = cfgs(ib)%mols(im)%glob_no(i)
    last = cfgs(ib)%mols(im)%nlist%numnbrs(i)

    !copy density of neighbours
    do nbr = 1, last

        jm = cfgs(ib)%mols(im)%nlist%nbr_list(1, nbr, i)
        j  = cfgs(ib)%mols(im)%nlist%nbr_list(2, nbr, i)
        jj = cfgs(ib)%mols(jm)%glob_no(j)

        cfgs(ib)%mols(jm)%smden(j) = rho(jj)

    end do

end subroutine copy_atom_density

!> @brief
!> - All the Ewald reciprocal space arrays for book-keeping are set here:
!> - `tmprcpxatom`(0:(2+maxgvecx))
!> - `tmprcpyatom`(-(maxgvecy+2):(maxgvecy+2))
!> - `tmprcpzatom`(-(maxgvecz+2):(maxgvecz+2))
!> - `tmprcpsum`(maxgvec,nconfigs)
!> @usage
!> - only applicable `if (any(job%coultype == 1))`
!> - called only once before the main loop in montecarlo_module.f90
!> @using 
!> - `cell_module`
!> - `comms_mpi_module`, only : gsum_world

!> Setting up the Ewald reciprocal space book-keeping arrays
subroutine setrcp_work(coultype)

    use cell_module
    use comms_mpi_module, only : gsum_world

    implicit none

        !> type(s) of Coulomb treatment (vector for multi-replica mode; must be > 0 here)
    integer, intent(in) :: coultype(:)

    integer :: i, maxat, maxgvec, maxgvecx, maxgvecy, maxgvecz
    integer, dimension(4) :: fail

    fail = 0
    maxat = 0
    maxgvec = 0
    maxgvecx = 0
    maxgvecy = 0
    maxgvecz = 0

    do i = 1, nconfigs

        if( coultype(i) == 1 .and. cfgs(i)%maxno_of_atoms > maxat ) maxat = cfgs(i)%maxno_of_atoms
        if( coultype(i) == 1 .and. cfgs(i)%cl%gvector_size > maxgvec ) maxgvec = cfgs(i)%cl%gvector_size
        if( coultype(i) == 1 .and. cfgs(i)%cl%gcellx > maxgvecx ) maxgvecx = cfgs(i)%cl%gcellx
        if( coultype(i) == 1 .and. cfgs(i)%cl%gcelly > maxgvecy ) maxgvecy = cfgs(i)%cl%gcelly
        if( coultype(i) == 1 .and. cfgs(i)%cl%gcellz > maxgvecz ) maxgvecz = cfgs(i)%cl%gcellz

    end do

    if(.not.allocated(tmprcpsum))   allocate (tmprcpsum(maxgvec,nconfigs), stat = fail(4))
    if(.not.allocated(tmprcpxatom)) allocate (tmprcpxatom(0:(2+maxgvecx)), stat = fail(1))
    if(.not.allocated(tmprcpyatom)) allocate (tmprcpyatom(-(maxgvecy+2):(maxgvecy+2)), stat = fail(2))
    if(.not.allocated(tmprcpzatom)) allocate (tmprcpzatom(-(maxgvecz+2):(maxgvecz+2)), stat = fail(3))
    
    !AB: make sure ALL processes are aware of the local error and call error(..) -> MPI_FINALIZE(..)
    call gsum_world(fail)
    if (any(fail > 0)) call error(316)

    tmprcpxatom(0) = (1.0_wp,0.0_wp)
    tmprcpyatom(0) = (1.0_wp,0.0_wp)
    tmprcpzatom(0) = (1.0_wp,0.0_wp)

    if(.not.allocated(tmprcpxatom1)) allocate (tmprcpxatom1(0:(2+maxgvecx)), stat = fail(1))
    if(.not.allocated(tmprcpyatom1)) allocate (tmprcpyatom1(-(maxgvecy+2):(maxgvecy+2)), stat = fail(2))
    if(.not.allocated(tmprcpzatom1)) allocate (tmprcpzatom1(-(maxgvecz+2):(maxgvecz+2)), stat = fail(3))
    call gsum_world(fail)
    if (any(fail > 0)) call error(316)

    tmprcpxatom1(0) = (1.0_wp,0.0_wp)
    tmprcpyatom1(0) = (1.0_wp,0.0_wp)
    tmprcpzatom1(0) = (1.0_wp,0.0_wp)

    !$OMP WORKSHARE

    tmprcpsum = 0.0_wp + 0.0_wp * COMPLEXI !(0.0_wp,0.0_wp)

    !$OMP END WORKSHARE

end subroutine setrcp_work


!> Calculates the total Ewald sums in reciprocal space
subroutine total_recip(ib, ercp, mrcp, vir)

    use kinds_f90
    use cell_module
    use constants_module, only : CTOINTERNAL
    use species_module, only : number_of_molecules
    use comms_mpi_module, only : gsum
    use parallel_loop_module, only : rcp_start, rcp_step

    implicit none

    integer, intent(in) :: ib
    
    real(kind = wp), intent(inout) :: ercp, mrcp(:)

    real(kind = wp), intent(inout) :: vir

    integer :: rcp, rcp_last, im, i, j
    
    real(kind = wp) :: pphase
    
    ercp = 0.0_wp
    vir  = 0.0_wp
    mrcp = 0.0_wp

    !loop over vectors rather than atoms for speed over
    !larger number of processors rather than the "normal" way for MC

    rcp_last = cfgs(ib)%cl%numgvec

    tmprcpsum(:,ib) = 0.0_wp + 0.0_wp * COMPLEXI
    
    if( cfgs(ib)%vec%is_simple_orthogonal ) then

        do im = 1, cfgs(ib)%num_mols

            do i = 1, cfgs(ib)%mols(im)%natom
            
                if( cfgs(ib)%mols(im)%atms(i)%is_charged ) then
            
                    tmprcpxatom(1) = exp ( cfgs(ib)%vec%rcpvector(1,1) * &
                                           cfgs(ib)%mols(im)%atms(i)%rpos(1) * COMPLEXTWOPI )
                    tmprcpyatom(1) = exp ( cfgs(ib)%vec%rcpvector(2,2) * &
                                           cfgs(ib)%mols(im)%atms(i)%rpos(2) * COMPLEXTWOPI )
                    tmprcpzatom(1) = exp ( cfgs(ib)%vec%rcpvector(3,3) * &
                                           cfgs(ib)%mols(im)%atms(i)%rpos(3) * COMPLEXTWOPI )

                    do j = 2, cfgs(ib)%cl%gcellx
                        tmprcpxatom(j) = tmprcpxatom(j-1) * &
                                         tmprcpxatom(1)
                    end do

                        tmprcpyatom(-1) = conjg( tmprcpyatom(1) )
                    do j = 2, cfgs(ib)%cl%gcelly
                        tmprcpyatom(j) = tmprcpyatom(j-1) * &
                                         tmprcpyatom(1)
                        tmprcpyatom(-j) = conjg( tmprcpyatom(j) )
                    end do

                        tmprcpzatom(-1) = conjg( tmprcpzatom(1) )
                    do j = 2, cfgs(ib)%cl%gcellz
                        tmprcpzatom(j) = tmprcpzatom(j-1) * &
                                         tmprcpzatom(1)
                        tmprcpzatom(-j) = conjg( tmprcpzatom(j) )
                    end do

!JG: Loop to calculate contributions to sum, naively over rcps requres array elements as indices
                    do rcp = rcp_start, rcp_last, rcp_step

                       tmprcpsum(rcp,ib) = tmprcpsum(rcp,ib) + cfgs(ib)%mols(im)%atms(i)%charge * &
                                                             ( tmprcpxatom(cfgs(ib)%cl%nxyz(1,rcp)) * &
                                                               tmprcpyatom(cfgs(ib)%cl%nxyz(2,rcp)) * &
                                                               tmprcpzatom(cfgs(ib)%cl%nxyz(3,rcp)) )
                    end do

                end if
                
            end do

        end do
        
!JG: Previous loop required summming energy contributions over rcps
        do rcp = rcp_start, rcp_last, rcp_step

            ercp = ercp + cfgs(ib)%cl%g0(rcp) * ( tmprcpsum(rcp,ib) * conjg( tmprcpsum(rcp,ib) ) )

        end do

    else

        do im = 1, cfgs(ib)%num_mols

            do i = 1, cfgs(ib)%mols(im)%natom
            
                if( cfgs(ib)%mols(im)%atms(i)%is_charged ) then

                    do rcp = rcp_start, rcp_last, rcp_step

                        pphase = cfgs(ib)%cl%gx(rcp) * cfgs(ib)%mols(im)%atms(i)%rpos(1) + &
                                 cfgs(ib)%cl%gy(rcp) * cfgs(ib)%mols(im)%atms(i)%rpos(2) + &
                                 cfgs(ib)%cl%gz(rcp) * cfgs(ib)%mols(im)%atms(i)%rpos(3)

                        tmprcpsum(rcp,ib) = tmprcpsum(rcp,ib) + cfgs(ib)%mols(im)%atms(i)%charge &
                                          * exp( pphase * COMPLEXI )

                    end do

                end if

            end do

        end do

        do rcp = rcp_start, rcp_last, rcp_step

            ercp = ercp + cfgs(ib)%cl%g0(rcp) * ( tmprcpsum(rcp,ib) * conjg( tmprcpsum(rcp,ib) ) )

        end do

    end if

    if (cfgs(ib)%cl%gsumrecip) then

        call gsum(ercp)

!JG: It may be useful to reinstate molecular rcp energy calculation in the future
!        call gsum(mrcp)

    end if

    !vir = -ercp

end subroutine total_recip


!> calculate the energy change due to reciprocal ewald terms upon moving a molecule (for rigid body moves)
subroutine move_molecule_recip(ib, im, ercpn, mrcpn)!, moldata)

    use kinds_f90
    use constants_module, only : CTOINTERNAL
    use coul_module
    use cell_module
    use comms_mpi_module, only : is_parallel, gsum
    use parallel_loop_module, only : rcp_start, rcp_step

    implicit none

    integer, intent(in) :: ib, im
    
    real (kind = wp), intent(inout) :: ercpn, mrcpn(:)
    
    !logical, intent(in) :: moldata

    integer :: ircp, rcp_last, i, j
    
    real(kind = wp) :: pphase1, pphase

    ercpn = 0.0_wp
    !mrcpn = 0.0_wp

    rcp_last = cfgs(ib)%cl%numgvec

    !TU: The line below was causing an error in PSMC with electrostatics,
    !TU: while the commented out loop below did not. This is because the
    !TU: size of tmprcpsum can be larger than rcpsum in PSMC. The fix 
    !TU: is below.
    !tmprcpsum(:,ib) = cfgs(ib)%cl%rcpsum(:)
    tmprcpsum( 1:size(cfgs(ib)%cl%rcpsum), ib) = cfgs(ib)%cl%rcpsum(:)

!    do ircp = rcp_start, rcp_last, rcp_step
!        tmprcpsum(ircp,ib) =  cfgs(ib)%cl%rcpsum(ircp)
!    end do

    if( cfgs(ib)%vec%is_simple_orthogonal) then

        do i = 1, cfgs(ib)%mols(im)%natom
            
            if( cfgs(ib)%mols(im)%atms(i)%is_charged ) then

                tmprcpxatom1(1) = exp ( cfgs(ib)%vec%rcpvector(1,1) * &
                                        cfgs(ib)%mols(im)%atms(i)%store_rpos(1) * COMPLEXTWOPI )
                tmprcpyatom1(1) = exp ( cfgs(ib)%vec%rcpvector(2,2) * &
                                        cfgs(ib)%mols(im)%atms(i)%store_rpos(2) * COMPLEXTWOPI )
                tmprcpzatom1(1) = exp ( cfgs(ib)%vec%rcpvector(3,3) * &
                                        cfgs(ib)%mols(im)%atms(i)%store_rpos(3) * COMPLEXTWOPI )

                tmprcpxatom(1) = exp ( cfgs(ib)%vec%rcpvector(1,1) * &
                                       cfgs(ib)%mols(im)%atms(i)%rpos(1) * COMPLEXTWOPI )
                tmprcpyatom(1) = exp ( cfgs(ib)%vec%rcpvector(2,2) * &
                                       cfgs(ib)%mols(im)%atms(i)%rpos(2) * COMPLEXTWOPI )
                tmprcpzatom(1) = exp ( cfgs(ib)%vec%rcpvector(3,3) * &
                                       cfgs(ib)%mols(im)%atms(i)%rpos(3) * COMPLEXTWOPI )

                do j = 2, cfgs(ib)%cl%gcellx
                    tmprcpxatom1(j) = tmprcpxatom1(j-1) * &
                                      tmprcpxatom1(1)
                    tmprcpxatom(j)  = tmprcpxatom(j-1) * &
                                      tmprcpxatom(1)
                end do

                tmprcpyatom1(-1) = conjg( tmprcpyatom1(1) )
                tmprcpyatom(-1)  = conjg( tmprcpyatom(1) )
                do j = 2, cfgs(ib)%cl%gcelly
                    tmprcpyatom1(j)  = tmprcpyatom1(j-1) * &
                                       tmprcpyatom1(1)
                    tmprcpyatom1(-j) = conjg( tmprcpyatom1(j) )
                    
                    tmprcpyatom(j)   = tmprcpyatom(j-1) * &
                                       tmprcpyatom(1)
                    tmprcpyatom(-j)  = conjg( tmprcpyatom(j) )
                end do

                tmprcpzatom1(-1) = conjg( tmprcpzatom1(1) )
                tmprcpzatom(-1)  = conjg( tmprcpzatom(1) )
                do j = 2, cfgs(ib)%cl%gcellz
                    tmprcpzatom1(j)  = tmprcpzatom1(j-1) * &
                                       tmprcpzatom1(1)
                    tmprcpzatom1(-j) = conjg( tmprcpzatom1(j) )
                    
                    tmprcpzatom(j)   = tmprcpzatom(j-1) * &
                                       tmprcpzatom(1)
                    tmprcpzatom(-j)  = conjg( tmprcpzatom(j) )
                end do

                do ircp = rcp_start, rcp_last, rcp_step

                    tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) + cfgs(ib)%mols(im)%atms(i)%charge * &
                                       ( tmprcpxatom(cfgs(ib)%cl%nxyz(1,ircp)) * &
                                         tmprcpyatom(cfgs(ib)%cl%nxyz(2,ircp)) * &
                                         tmprcpzatom(cfgs(ib)%cl%nxyz(3,ircp)) - &
                                         tmprcpxatom1(cfgs(ib)%cl%nxyz(1,ircp)) * &
                                         tmprcpyatom1(cfgs(ib)%cl%nxyz(2,ircp)) * &
                                         tmprcpzatom1(cfgs(ib)%cl%nxyz(3,ircp)) )

                end do

            end if

        end do

        do ircp = rcp_start, rcp_last, rcp_step

             ercpn = ercpn + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )

        end do

    else

        do i = 1, cfgs(ib)%mols(im)%natom
            
            if( cfgs(ib)%mols(im)%atms(i)%is_charged ) then

                do ircp = rcp_start, rcp_last, rcp_step

                    pphase1 = cfgs(ib)%cl%gx(ircp) * cfgs(ib)%mols(im)%atms(i)%store_rpos(1) + &
                              cfgs(ib)%cl%gy(ircp) * cfgs(ib)%mols(im)%atms(i)%store_rpos(2) + &
                              cfgs(ib)%cl%gz(ircp) * cfgs(ib)%mols(im)%atms(i)%store_rpos(3)

                    pphase = cfgs(ib)%cl%gx(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(1) + &
                             cfgs(ib)%cl%gy(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(2) + &
                             cfgs(ib)%cl%gz(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(3)
            
                    tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) + cfgs(ib)%mols(im)%atms(i)%charge * &
                                       ( exp( pphase * COMPLEXI ) - exp( pphase1 * COMPLEXI ) )

                end do

            end if

        end do

        do ircp = rcp_start, rcp_last, rcp_step

             ercpn = ercpn + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )

        end do

    end if

    if( is_parallel ) then

        call gsum(ercpn)
     !   call gsum(mrcpn)

    end if

end subroutine move_molecule_recip


!> calculate the current reciprocal ewald contributions and remove them from sums (for GCMC moves)
subroutine remove_molecule_recip(ib, im, ercpn, mrcpn, moldata)

    use kinds_f90
    use constants_module, only : CTOINTERNAL
    use coul_module
    use cell_module
    use comms_mpi_module, only : is_parallel, gsum
    use parallel_loop_module, only : rcp_start, rcp_step

    implicit none

    integer, intent(in) :: ib, im
    
    real (kind = wp), intent(inout) :: ercpn, mrcpn(:)
    
    logical, intent(in) :: moldata

    integer :: ircp, rcp_last, i, j
    
    real(kind = wp) :: pphase

    ercpn = 0.0_wp
    !mrcpn = 0.0_wp

    rcp_last = cfgs(ib)%cl%numgvec

    !TU: The line below I think will cause an error in 2-box simulations,
    !TU: while the commented out loop below I think will not. This is because the
    !TU: size of tmprcpsum can be larger than rcpsum in 2-box simulation. 
    !TU: I've fixed it below
    !tmprcpsum(:,ib) = cfgs(ib)%cl%rcpsum(:)
    tmprcpsum( 1:size(cfgs(ib)%cl%rcpsum), ib) = cfgs(ib)%cl%rcpsum(:)

!    do ircp = rcp_start, rcp_last, rcp_step
!        tmprcpsum(ircp,ib) =  cfgs(ib)%cl%rcpsum(ircp)
!    end do

    if( cfgs(ib)%vec%is_simple_orthogonal) then

        do i = 1, cfgs(ib)%mols(im)%natom
            
            if( cfgs(ib)%mols(im)%atms(i)%is_charged ) then

                tmprcpxatom(1) = exp ( cfgs(ib)%vec%rcpvector(1,1) * &
                                       cfgs(ib)%mols(im)%atms(i)%rpos(1) * COMPLEXTWOPI )
                tmprcpyatom(1) = exp ( cfgs(ib)%vec%rcpvector(2,2) * &
                                       cfgs(ib)%mols(im)%atms(i)%rpos(2) * COMPLEXTWOPI )
                tmprcpzatom(1) = exp ( cfgs(ib)%vec%rcpvector(3,3) * &
                                       cfgs(ib)%mols(im)%atms(i)%rpos(3) * COMPLEXTWOPI )

                do j = 2, cfgs(ib)%cl%gcellx
                    tmprcpxatom(j) = tmprcpxatom(j-1) * &
                                     tmprcpxatom(1)
                end do

                    tmprcpyatom(-1) = conjg( tmprcpyatom(1) )
                do j = 2, cfgs(ib)%cl%gcelly
                    tmprcpyatom(j) = tmprcpyatom(j-1) * &
                                     tmprcpyatom(1)
                    tmprcpyatom(-j) = conjg( tmprcpyatom(j) )
                end do

                    tmprcpzatom(-1) = conjg( tmprcpzatom(1) )
                do j = 2, cfgs(ib)%cl%gcellz
                    tmprcpzatom(j) = tmprcpzatom(j-1) * &
                                     tmprcpzatom(1)
                    tmprcpzatom(-j) = conjg( tmprcpzatom(j) )
                end do

                do ircp = rcp_start, rcp_last, rcp_step

                    tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) - cfgs(ib)%mols(im)%atms(i)%charge * &
                                       ( tmprcpxatom(cfgs(ib)%cl%nxyz(1,ircp)) * &
                                         tmprcpyatom(cfgs(ib)%cl%nxyz(2,ircp)) * &
                                         tmprcpzatom(cfgs(ib)%cl%nxyz(3,ircp)) )

                end do

            end if

        end do

        do ircp = rcp_start, rcp_last, rcp_step

             ercpn = ercpn + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )

        end do

    else

        do i = 1, cfgs(ib)%mols(im)%natom
            
            if( cfgs(ib)%mols(im)%atms(i)%is_charged ) then

                do ircp = rcp_start, rcp_last, rcp_step

                    pphase = cfgs(ib)%cl%gx(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(1) + &
                             cfgs(ib)%cl%gy(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(2) + &
                             cfgs(ib)%cl%gz(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(3)
            
                    tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) - cfgs(ib)%mols(im)%atms(i)%charge &
                                       * exp( pphase * COMPLEXI )

                end do

            end if

        end do

        do ircp = rcp_start, rcp_last, rcp_step

             ercpn = ercpn + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )

        end do

    end if

    if( is_parallel ) then

        call gsum(ercpn)
     !   call gsum(mrcpn)

    end if

end subroutine remove_molecule_recip


!> calculate the reciprocal ewald contribution and energy of moved molecule (for rigid_body moves)
subroutine add_molecule_recip(ib, im, ercpn, mrcpn, moldata)

    use kinds_f90
    use constants_module, only : CTOINTERNAL
    use coul_module
    use cell_module
    use comms_mpi_module, only : is_parallel, gsum
    use parallel_loop_module, only : rcp_start, rcp_step

    implicit none

    integer, intent(in) :: ib, im
    
    real (kind = wp), intent(inout) :: ercpn, mrcpn(:)
    
    logical, intent(in) :: moldata
    
    integer :: j, i, ircp, rcp_last
    real(kind = wp) :: pphase

    ercpn = 0.0_wp
    !mrcpn = 0.0_wp

    rcp_last  = cfgs(ib)%cl%numgvec

    if( cfgs(ib)%vec%is_simple_orthogonal) then

        do i = 1, cfgs(ib)%mols(im)%natom
            
            if( cfgs(ib)%mols(im)%atms(i)%is_charged ) then

                tmprcpxatom(1) = exp ( cfgs(ib)%vec%rcpvector(1,1) * &
                                       cfgs(ib)%mols(im)%atms(i)%rpos(1) * COMPLEXTWOPI )
                tmprcpyatom(1) = exp ( cfgs(ib)%vec%rcpvector(2,2) * &
                                       cfgs(ib)%mols(im)%atms(i)%rpos(2) * COMPLEXTWOPI )
                tmprcpzatom(1) = exp ( cfgs(ib)%vec%rcpvector(3,3) * &
                                       cfgs(ib)%mols(im)%atms(i)%rpos(3) * COMPLEXTWOPI )

                do j = 2, cfgs(ib)%cl%gcellx
                    tmprcpxatom(j) = tmprcpxatom(j-1) * &
                                     tmprcpxatom(1)
                end do

                    tmprcpyatom(-1) = conjg( tmprcpyatom(1) )
                do j = 2, cfgs(ib)%cl%gcelly
                    tmprcpyatom(j) = tmprcpyatom(j-1) * &
                                     tmprcpyatom(1)
                    tmprcpyatom(-j) = conjg( tmprcpyatom(j) )
                end do

                    tmprcpzatom(-1) = conjg( tmprcpzatom(1) )
                do j = 2, cfgs(ib)%cl%gcellz
                    tmprcpzatom(j) = tmprcpzatom(j-1) * &
                                     tmprcpzatom(1)
                    tmprcpzatom(-j) = conjg( tmprcpzatom(j) )
                end do

                do ircp = rcp_start, rcp_last, rcp_step

                    tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) + cfgs(ib)%mols(im)%atms(i)%charge * &
                                       ( tmprcpxatom(cfgs(ib)%cl%nxyz(1,ircp)) * &
                                         tmprcpyatom(cfgs(ib)%cl%nxyz(2,ircp)) * &
                                         tmprcpzatom(cfgs(ib)%cl%nxyz(3,ircp)) )

                end do

            end if

        end do

        do ircp = rcp_start, rcp_last, rcp_step

            ercpn = ercpn + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )

        end do

    else

        do i = 1, cfgs(ib)%mols(im)%natom
            
            if( cfgs(ib)%mols(im)%atms(i)%is_charged ) then

                do ircp = rcp_start, rcp_last, rcp_step

                    pphase = cfgs(ib)%cl%gx(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(1) + &
                             cfgs(ib)%cl%gy(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(2) + &
                             cfgs(ib)%cl%gz(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(3)

                    tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) + cfgs(ib)%mols(im)%atms(i)%charge &
                                       * exp( pphase * COMPLEXI )

                end do

            end if

        end do

        do ircp = rcp_start, rcp_last, rcp_step

            ercpn = ercpn + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )

        end do

    end if

    if( is_parallel ) then

        call gsum(ercpn)
        !call gsum(mrcpn)

    end if

end subroutine add_molecule_recip


!> calculate reciprocal ewald contirubtion due to inserted molecule (for GCMC moves)
subroutine insert_molecule_recip(ib, im, ercpn, mrcpn, moldata)

    use kinds_f90
    use constants_module, only : CTOINTERNAL
    use coul_module
    use cell_module
    use comms_mpi_module, only : is_parallel, gsum
    use parallel_loop_module, only : rcp_start, rcp_step

    implicit none

    integer, intent(in) :: ib, im
    
    real (kind = wp), intent(inout) :: ercpn, mrcpn(:)
    
    logical, intent(in) :: moldata
    
    integer :: i, j, ircp
    
    real(kind = wp) :: pphase

    ercpn = 0.0_wp
    !mrcpn = 0.0_wp

    !TU: The line below I think will cause an error in 2-box simulations,
    !TU: while the commented out loop below I think will not. This is because the
    !TU: size of tmprcpsum can be larger than rcpsum in 2-box simulation. 
    !TU: I've fixed it below
    !tmprcpsum(:,ib) = cfgs(ib)%cl%rcpsum(:)
    tmprcpsum( 1:size(cfgs(ib)%cl%rcpsum), ib) = cfgs(ib)%cl%rcpsum(:)

!    do ircp = rcp_start, cfgs(ib)%cl%numgvec, rcp_step
!        tmprcpsum(ircp,ib) =  cfgs(ib)%cl%rcpsum(ircp) 
!    end do

    if( cfgs(ib)%vec%is_simple_orthogonal) then

        do i = 1, cfgs(ib)%mols(im)%natom
            
            if( cfgs(ib)%mols(im)%atms(i)%is_charged ) then

                tmprcpxatom(1) = exp ( cfgs(ib)%vec%rcpvector(1,1) * &
                                       cfgs(ib)%mols(im)%atms(i)%rpos(1) * COMPLEXTWOPI )
                tmprcpyatom(1) = exp ( cfgs(ib)%vec%rcpvector(2,2) * &
                                       cfgs(ib)%mols(im)%atms(i)%rpos(2) * COMPLEXTWOPI )
                tmprcpzatom(1) = exp ( cfgs(ib)%vec%rcpvector(3,3) * &
                                       cfgs(ib)%mols(im)%atms(i)%rpos(3) * COMPLEXTWOPI )

                do j = 2, cfgs(ib)%cl%gcellx
                    tmprcpxatom(j) = tmprcpxatom(j-1) * &
                                     tmprcpxatom(1)
                end do

                    tmprcpyatom(-1) = conjg( tmprcpyatom(1) )
                do j = 2, cfgs(ib)%cl%gcelly
                    tmprcpyatom(j) = tmprcpyatom(j-1) * &
                                     tmprcpyatom(1)
                    tmprcpyatom(-j) = conjg( tmprcpyatom(j) )
                end do

                    tmprcpzatom(-1) = conjg( tmprcpzatom(1) )
                do j = 2, cfgs(ib)%cl%gcellz
                    tmprcpzatom(j) = tmprcpzatom(j-1) * &
                                     tmprcpzatom(1)
                    tmprcpzatom(-j) = conjg( tmprcpzatom(j) )
                end do

                do ircp = rcp_start, cfgs(ib)%cl%numgvec, rcp_step

                    tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) + cfgs(ib)%mols(im)%atms(i)%charge * &
                                       ( tmprcpxatom(cfgs(ib)%cl%nxyz(1,ircp)) * &
                                         tmprcpyatom(cfgs(ib)%cl%nxyz(2,ircp)) * &
                                         tmprcpzatom(cfgs(ib)%cl%nxyz(3,ircp)) )

                end do

            end if

        end do

        do ircp = rcp_start, cfgs(ib)%cl%numgvec, rcp_step

            ercpn = ercpn + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )

        end do

    else

        do ircp = rcp_start, cfgs(ib)%cl%numgvec, rcp_step

            do i = 1, cfgs(ib)%mols(im)%natom
                
                if( cfgs(ib)%mols(im)%atms(i)%is_charged ) then

                    pphase = cfgs(ib)%cl%gx(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(1) + &
                             cfgs(ib)%cl%gy(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(2) + &
                             cfgs(ib)%cl%gz(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(3)

                    tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) + cfgs(ib)%mols(im)%atms(i)%charge &
                                       * exp( pphase * COMPLEXI )

                end if

            end do

            ercpn = ercpn + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )

        end do

    end if 

    if( is_parallel ) then

        call gsum(ercpn)

    end if

end subroutine insert_molecule_recip


!> Calculate the changes to reciprocal contribution and energy \n
!> due to swapping the position of two differently charged species
! New routine for swap_atoms
subroutine swap_atoms_recip(ib, im, iatom, jm, jatom, chgi, chgj, tmprcp)

    use kinds_f90
    use cell_module
    use coul_module
    use species_module, only: number_of_molecules
    use constants_module, only : CTOINTERNAL
    use comms_mpi_module, only : is_parallel, gsum
    use parallel_loop_module, only : rcp_start, rcp_step

    implicit none

    integer, intent(in) :: ib, im , iatom, jm, jatom
    
    real(kind = wp), intent (inout) :: tmprcp, chgi, chgj
    
!    real(kind = wp), intent (inout) :: mrcpo(:), mrcpn(:)
!    logical, intent (in) :: moldata

    integer :: j, ircp, start, step, nvec  

    real(kind = wp) :: deltachg, engo, engn, xxn, yyn, zzn
    real(kind = wp) :: pphase, oldrcp, newrcp

    !ercpo = 0.0_wp
    tmprcp = 0.0_wp

!JG: deltachg is the difference from jatom - iatom.
! Thus contributions need to be added at site of iatom
! And subtracted at site of jatom.

    !AB: no contribution if both charges are zero
    !if(   abs(cfgs(ib)%mols(im)%atms(iatom)%charge) < 1.0e-8_wp &
    !.and. abs(cfgs(ib)%mols(jm)%atms(jatom)%charge) < 1.0e-8_wp ) return
    if(  .not.cfgs(ib)%mols(im)%atms(iatom)%is_charged &
    .and. .not.cfgs(ib)%mols(jm)%atms(jatom)%is_charged ) return

    deltachg = chgj - chgi

    nvec = cfgs(ib)%cl%numgvec

    start = rcp_start
    step = rcp_step

    oldrcp = 0.0_wp
    newrcp = 0.0_wp

    !TU: The line below I think will cause an error in 2-box simulations,
    !TU: while the commented out loop below I think will not. This is because the
    !TU: size of tmprcpsum can be larger than rcpsum in 2-box simulation. 
    !TU: I've fixed it below
    !tmprcpsum(:,ib) = cfgs(ib)%cl%rcpsum(:)
    tmprcpsum( 1:size(cfgs(ib)%cl%rcpsum), ib) = cfgs(ib)%cl%rcpsum(:)

!    do ircp = start, nvec, step
!       tmprcpsum(ircp,ib) =  cfgs(ib)%cl%rcpsum(ircp)
!    end do

    if( cfgs(ib)%vec%is_simple_orthogonal ) then

        tmprcpxatom(1) = exp ( cfgs(ib)%vec%rcpvector(1,1) * &
                               cfgs(ib)%mols(im)%atms(iatom)%rpos(1) * COMPLEXTWOPI )
        tmprcpyatom(1) = exp ( cfgs(ib)%vec%rcpvector(2,2) * &
                               cfgs(ib)%mols(im)%atms(iatom)%rpos(2) * COMPLEXTWOPI )
        tmprcpzatom(1) = exp ( cfgs(ib)%vec%rcpvector(3,3) * &
                               cfgs(ib)%mols(im)%atms(iatom)%rpos(3) * COMPLEXTWOPI )

        do j = 2, cfgs(ib)%cl%gcellx
            tmprcpxatom(j) = tmprcpxatom(j-1) * &
                             tmprcpxatom(1)
        end do

            tmprcpyatom(-1) = conjg( tmprcpyatom(1) )
        do j = 2, cfgs(ib)%cl%gcelly
            tmprcpyatom(j) = tmprcpyatom(j-1) * &
                             tmprcpyatom(1)
            tmprcpyatom(-j) = conjg( tmprcpyatom(j) )
        end do

            tmprcpzatom(-1) = conjg( tmprcpzatom(1) )
        do j = 2, cfgs(ib)%cl%gcellz
            tmprcpzatom(j) = tmprcpzatom(j-1) * &
                             tmprcpzatom(1)
            tmprcpzatom(-j) = conjg( tmprcpzatom(j) )
        end do

        do ircp = start, nvec, step

            oldrcp = oldrcp + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )

            tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) + deltachg * &
                               ( tmprcpxatom(cfgs(ib)%cl%nxyz(1,ircp)) * &
                                 tmprcpyatom(cfgs(ib)%cl%nxyz(2,ircp)) * &
                                 tmprcpzatom(cfgs(ib)%cl%nxyz(3,ircp)) )

        end do

    else

        do ircp = start, nvec, step

            oldrcp = oldrcp + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )

            pphase = cfgs(ib)%cl%gx(ircp) * cfgs(ib)%mols(im)%atms(iatom)%rpos(1) + &
                     cfgs(ib)%cl%gy(ircp) * cfgs(ib)%mols(im)%atms(iatom)%rpos(2) + &
                     cfgs(ib)%cl%gz(ircp) * cfgs(ib)%mols(im)%atms(iatom)%rpos(3)

            tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) + deltachg * exp( pphase * COMPLEXI )

        end do

    end if

    if( cfgs(ib)%vec%is_simple_orthogonal ) then

        tmprcpxatom(1) = exp ( cfgs(ib)%vec%rcpvector(1,1) * &
                               cfgs(ib)%mols(jm)%atms(jatom)%rpos(1) * COMPLEXTWOPI )
        tmprcpyatom(1) = exp ( cfgs(ib)%vec%rcpvector(2,2) * &
                               cfgs(ib)%mols(jm)%atms(jatom)%rpos(2) * COMPLEXTWOPI )
        tmprcpzatom(1) = exp ( cfgs(ib)%vec%rcpvector(3,3) * &
                               cfgs(ib)%mols(jm)%atms(jatom)%rpos(3) * COMPLEXTWOPI )

        do j = 2, cfgs(ib)%cl%gcellx
            tmprcpxatom(j) = tmprcpxatom(j-1) * &
                             tmprcpxatom(1)
        end do

            tmprcpyatom(-1) = conjg( tmprcpyatom(1) )
       do j = 2, cfgs(ib)%cl%gcelly
            tmprcpyatom(j) = tmprcpyatom(j-1) * &
                             tmprcpyatom(1)
            tmprcpyatom(-j) = conjg( tmprcpyatom(j) )
        end do

            tmprcpzatom(-1) = conjg( tmprcpzatom(1) )
        do j = 2, cfgs(ib)%cl%gcellz
            tmprcpzatom(j) = tmprcpzatom(j-1) * &
                             tmprcpzatom(1)
            tmprcpzatom(-j) = conjg( tmprcpzatom(j) )
        end do

        do ircp = start, nvec, step

            tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) - deltachg * &
                               ( tmprcpxatom(cfgs(ib)%cl%nxyz(1,ircp)) * &
                                 tmprcpyatom(cfgs(ib)%cl%nxyz(2,ircp)) * &
                                 tmprcpzatom(cfgs(ib)%cl%nxyz(3,ircp)) )

            newrcp = newrcp + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )

        end do

    else

        do ircp = start, nvec, step

            pphase = cfgs(ib)%cl%gx(ircp) * cfgs(ib)%mols(jm)%atms(jatom)%rpos(1) + &
                     cfgs(ib)%cl%gy(ircp) * cfgs(ib)%mols(jm)%atms(jatom)%rpos(2) + &
                     cfgs(ib)%cl%gz(ircp) * cfgs(ib)%mols(jm)%atms(jatom)%rpos(3)

            tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) - deltachg * exp( pphase * COMPLEXI )

            newrcp = newrcp + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )

        end do

    end if

    tmprcp = newrcp - oldrcp

    if (cfgs(ib)%cl%gsumrecip) then

        call gsum(tmprcp)

    end if

end subroutine swap_atoms_recip

!Calculate the update of reciprocal contribution and energy upon atom move
subroutine move_atom_recip(ib, im, i, ercpo, ercpn, mrcpo, mrcpn, moldata)

    use kinds_f90
    use cell_module
    use coul_module
    use species_module, only: number_of_molecules
    use constants_module, only : CTOINTERNAL
    use comms_mpi_module, only : is_parallel, gsum
    use parallel_loop_module, only : rcp_start, rcp_step

    implicit none

    integer, intent(in) :: ib, im , i
    
    real(kind = wp), intent (inout) :: ercpo, ercpn
    
    real(kind = wp), intent (inout) :: mrcpo(:), mrcpn(:)
    
    logical, intent (in) :: moldata

    integer :: j, ircp, start, step, nvec

    real(kind = wp) :: pphase, pphase1

    ercpo = 0.0_wp
    ercpn = 0.0_wp
    !mrcpo = 0.0_wp
    !mrcpn = 0.0_wp

    !AB: checked outside
    !if( .not.cfgs(ib)%mols(im)%atms(i)%is_charged ) return
    !if(abs(cfgs(ib)%mols(im)%atms(i)%charge) < 1.0e-8_wp) return

    nvec  = cfgs(ib)%cl%numgvec
    start = rcp_start
    step  = rcp_step

    !TU: The line below I think will cause an error in 2-box simulations,
    !TU: while the commented out loop below I think will not. This is because the
    !TU: size of tmprcpsum can be larger than rcpsum in 2-box simulation. 
    !TU: I've fixed it below
    !tmprcpsum(:,ib) = cfgs(ib)%cl%rcpsum(:)
    tmprcpsum( 1:size(cfgs(ib)%cl%rcpsum), ib) = cfgs(ib)%cl%rcpsum(:)

    do ircp = start, nvec, step
        !tmprcpsum(ircp,ib) =  cfgs(ib)%cl%rcpsum(ircp)
        ercpo = ercpo + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )
    end do

    if( cfgs(ib)%vec%is_simple_orthogonal ) then

        tmprcpxatom1(1) = exp ( cfgs(ib)%vec%rcpvector(1,1) * &
                                cfgs(ib)%mols(im)%atms(i)%store_rpos(1) * COMPLEXTWOPI )
        tmprcpyatom1(1) = exp ( cfgs(ib)%vec%rcpvector(2,2) * &
                                cfgs(ib)%mols(im)%atms(i)%store_rpos(2) * COMPLEXTWOPI )
        tmprcpzatom1(1) = exp ( cfgs(ib)%vec%rcpvector(3,3) * &
                                cfgs(ib)%mols(im)%atms(i)%store_rpos(3) * COMPLEXTWOPI )

        tmprcpxatom(1) = exp ( cfgs(ib)%vec%rcpvector(1,1) * &
                               cfgs(ib)%mols(im)%atms(i)%rpos(1) * COMPLEXTWOPI )
        tmprcpyatom(1) = exp ( cfgs(ib)%vec%rcpvector(2,2) * &
                               cfgs(ib)%mols(im)%atms(i)%rpos(2) * COMPLEXTWOPI )
        tmprcpzatom(1) = exp ( cfgs(ib)%vec%rcpvector(3,3) * &
                               cfgs(ib)%mols(im)%atms(i)%rpos(3) * COMPLEXTWOPI )

        do j = 2, cfgs(ib)%cl%gcellx
            tmprcpxatom1(j) = tmprcpxatom1(j-1) * &
                              tmprcpxatom1(1)
            tmprcpxatom(j)  = tmprcpxatom(j-1) * &
                              tmprcpxatom(1)
        end do

        tmprcpyatom1(-1) = conjg( tmprcpyatom1(1) )
        tmprcpyatom(-1)  = conjg( tmprcpyatom(1) )
        do j = 2, cfgs(ib)%cl%gcelly
            tmprcpyatom1(j)  = tmprcpyatom1(j-1) * &
                               tmprcpyatom1(1)
            tmprcpyatom1(-j) = conjg( tmprcpyatom1(j) )

            tmprcpyatom(j)   = tmprcpyatom(j-1) * &
                               tmprcpyatom(1)
            tmprcpyatom(-j)  = conjg( tmprcpyatom(j) )
        end do

        tmprcpzatom1(-1) = conjg( tmprcpzatom1(1) )
        tmprcpzatom(-1)  = conjg( tmprcpzatom(1) )
        do j = 2, cfgs(ib)%cl%gcellz
            tmprcpzatom1(j)  = tmprcpzatom1(j-1) * &
                               tmprcpzatom1(1)
            tmprcpzatom1(-j) = conjg( tmprcpzatom1(j) )

            tmprcpzatom(j)   = tmprcpzatom(j-1) * &
                               tmprcpzatom(1)
            tmprcpzatom(-j)  = conjg( tmprcpzatom(j) )
        end do

        do ircp = start, nvec, step

            tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) + cfgs(ib)%mols(im)%atms(i)%charge * &
                               ( tmprcpxatom(cfgs(ib)%cl%nxyz(1,ircp)) * &
                                 tmprcpyatom(cfgs(ib)%cl%nxyz(2,ircp)) * &
                                 tmprcpzatom(cfgs(ib)%cl%nxyz(3,ircp)) - &
                                 tmprcpxatom1(cfgs(ib)%cl%nxyz(1,ircp)) * &
                                 tmprcpyatom1(cfgs(ib)%cl%nxyz(2,ircp)) * &
                                 tmprcpzatom1(cfgs(ib)%cl%nxyz(3,ircp)) )

            ercpn = ercpn + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )

        end do

    else

        do ircp = start, nvec, step

            pphase1 = cfgs(ib)%cl%gx(ircp) * cfgs(ib)%mols(im)%atms(i)%store_rpos(1) + &
                      cfgs(ib)%cl%gy(ircp) * cfgs(ib)%mols(im)%atms(i)%store_rpos(2) + &
                      cfgs(ib)%cl%gz(ircp) * cfgs(ib)%mols(im)%atms(i)%store_rpos(3)

            pphase = cfgs(ib)%cl%gx(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(1) + &
                     cfgs(ib)%cl%gy(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(2) + &
                     cfgs(ib)%cl%gz(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(3)

            tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) + cfgs(ib)%mols(im)%atms(i)%charge * &
                               ( exp( pphase * COMPLEXI ) - exp( pphase1 * COMPLEXI ) )

            ercpn = ercpn + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )

        end do

    end if

    if (cfgs(ib)%cl%gsumrecip) then

        call gsum(ercpo)
        call gsum(ercpn)

    end if

end subroutine move_atom_recip

!JG: Calculate the current reciprocal contirubtion and energy
subroutine remove_atom_recip(ib, im, i, ercpo, ercpn, mrcpo, mrcpn, moldata)

    use kinds_f90
    use cell_module
    use coul_module
    use species_module, only: number_of_molecules
    use constants_module, only : CTOINTERNAL
    use comms_mpi_module, only : is_parallel, gsum
    use parallel_loop_module, only : rcp_start, rcp_step

    implicit none

    integer, intent(in) :: ib, im , i
    
    real(kind = wp), intent (inout) :: ercpo, ercpn
    
    real(kind = wp), intent (inout) :: mrcpo(:), mrcpn(:)
    
    logical, intent (in) :: moldata

    integer :: j, ircp, start, step, nvec

    real(kind = wp) :: pphase

    ercpo = 0.0_wp
    !mrcpo = 0.0_wp
    !mrcpn = 0.0_wp

    !AB: checked outside
    !if( .not.cfgs(ib)%mols(im)%atms(i)%is_charged ) return
    !if(abs(cfgs(ib)%mols(im)%atms(i)%charge) < 1.0e-8_wp) return

    nvec  = cfgs(ib)%cl%numgvec
    start = rcp_start
    step  = rcp_step

    !TU: The line below I think will cause an error in 2-box simulations,
    !TU: while the commented out loop below I think will not. This is because the
    !TU: size of tmprcpsum can be larger than rcpsum in 2-box simulation. 
    !TU: I've fixed it below
    !tmprcpsum(:,ib) = cfgs(ib)%cl%rcpsum(:)
    tmprcpsum( 1:size(cfgs(ib)%cl%rcpsum), ib) = cfgs(ib)%cl%rcpsum(:)

    do ircp = start, nvec, step
        !tmprcpsum(ircp,ib) =  cfgs(ib)%cl%rcpsum(ircp)
        ercpo = ercpo + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )
    end do

    if( cfgs(ib)%vec%is_simple_orthogonal ) then

        tmprcpxatom(1) = exp ( cfgs(ib)%vec%rcpvector(1,1) * &
                               cfgs(ib)%mols(im)%atms(i)%rpos(1) * COMPLEXTWOPI )
        tmprcpyatom(1) = exp ( cfgs(ib)%vec%rcpvector(2,2) * &
                               cfgs(ib)%mols(im)%atms(i)%rpos(2) * COMPLEXTWOPI )
        tmprcpzatom(1) = exp ( cfgs(ib)%vec%rcpvector(3,3) * &
                               cfgs(ib)%mols(im)%atms(i)%rpos(3) * COMPLEXTWOPI )

        do j = 2, cfgs(ib)%cl%gcellx
            tmprcpxatom(j) = tmprcpxatom(j-1) * &
                             tmprcpxatom(1)
        end do

            tmprcpyatom(-1) = conjg( tmprcpyatom(1) )
        do j = 2, cfgs(ib)%cl%gcelly
            tmprcpyatom(j) = tmprcpyatom(j-1) * &
                             tmprcpyatom(1)
            tmprcpyatom(-j) = conjg( tmprcpyatom(j) )
        end do

            tmprcpzatom(-1) = conjg( tmprcpzatom(1) )
        do j = 2, cfgs(ib)%cl%gcellz
            tmprcpzatom(j) = tmprcpzatom(j-1) * &
                             tmprcpzatom(1)
            tmprcpzatom(-j) = conjg( tmprcpzatom(j) )
        end do

        do ircp = start, nvec, step

            tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) - cfgs(ib)%mols(im)%atms(i)%charge * &
                              ( tmprcpxatom(cfgs(ib)%cl%nxyz(1,ircp)) * &
                                tmprcpyatom(cfgs(ib)%cl%nxyz(2,ircp)) * &
                                tmprcpzatom(cfgs(ib)%cl%nxyz(3,ircp)) )

        end do

    else

        do ircp = start, nvec, step

            pphase = cfgs(ib)%cl%gx(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(1) + &
                     cfgs(ib)%cl%gy(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(2) + &
                     cfgs(ib)%cl%gz(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(3)

            tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) - cfgs(ib)%mols(im)%atms(i)%charge * exp( pphase * COMPLEXI )

        end do

    end if

    if (cfgs(ib)%cl%gsumrecip) then

        call gsum(ercpo)

    end if

end subroutine remove_atom_recip

!> Calculate the proposed reciprocal contribution and energy
subroutine add_atom_recip(ib, im, i, ercpo, ercpn, mrcpo, mrcpn, moldata)

    use kinds_f90
    use cell_module
    use coul_module
    use species_module, only: number_of_molecules
    use constants_module, only : CTOINTERNAL
    use comms_mpi_module, only : is_parallel, gsum
    use parallel_loop_module, only : rcp_start, rcp_step

    implicit none

    integer, intent(in) :: ib, im , i
    real(kind = wp), intent (inout) :: ercpo, ercpn
    real(kind = wp), intent (inout) :: mrcpo(:), mrcpn(:)
    logical, intent (in) :: moldata

    integer :: j, ircp, start, step, nvec

    real(kind = wp) :: pphase

    ercpn = 0.0_wp
    !mrcpo = 0.0_wp
    !mrcpn = 0.0_wp

    !AB: checked outside
    !if( .not.cfgs(ib)%mols(im)%atms(i)%is_charged ) return
    !if(abs(cfgs(ib)%mols(im)%atms(i)%charge) < 1.0e-8_wp) return

    nvec  = cfgs(ib)%cl%numgvec
    start = rcp_start
    step  = rcp_step

    if( cfgs(ib)%vec%is_simple_orthogonal ) then

        tmprcpxatom(1) = exp ( cfgs(ib)%vec%rcpvector(1,1) * &
                               cfgs(ib)%mols(im)%atms(i)%rpos(1) * COMPLEXTWOPI )
        tmprcpyatom(1) = exp ( cfgs(ib)%vec%rcpvector(2,2) * &
                               cfgs(ib)%mols(im)%atms(i)%rpos(2) * COMPLEXTWOPI )
        tmprcpzatom(1) = exp ( cfgs(ib)%vec%rcpvector(3,3) * &
                               cfgs(ib)%mols(im)%atms(i)%rpos(3) * COMPLEXTWOPI )

        do j = 2, cfgs(ib)%cl%gcellx
            tmprcpxatom(j) = tmprcpxatom(j-1) * &
                             tmprcpxatom(1)
        end do

            tmprcpyatom(-1) = conjg( tmprcpyatom(1) )
        do j = 2, cfgs(ib)%cl%gcelly
            tmprcpyatom(j) = tmprcpyatom(j-1) * &
                             tmprcpyatom(1)
            tmprcpyatom(-j) = conjg( tmprcpyatom(j) )
        end do

            tmprcpzatom(-1) = conjg( tmprcpzatom(1) )
        do j = 2, cfgs(ib)%cl%gcellz
            tmprcpzatom(j) = tmprcpzatom(j-1) * &
                             tmprcpzatom(1)
            tmprcpzatom(-j) = conjg( tmprcpzatom(j) )
        end do

        do ircp = start, nvec, step

            tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) + cfgs(ib)%mols(im)%atms(i)%charge * &
                               ( tmprcpxatom(cfgs(ib)%cl%nxyz(1,ircp)) * &
                                 tmprcpyatom(cfgs(ib)%cl%nxyz(2,ircp)) * &
                                 tmprcpzatom(cfgs(ib)%cl%nxyz(3,ircp)) )

            ercpn = ercpn + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )

        end do

    else

        do ircp = start, nvec, step

            pphase = cfgs(ib)%cl%gx(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(1) + &
                     cfgs(ib)%cl%gy(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(2) + &
                     cfgs(ib)%cl%gz(ircp) * cfgs(ib)%mols(im)%atms(i)%rpos(3)

            tmprcpsum(ircp,ib) = tmprcpsum(ircp,ib) + cfgs(ib)%mols(im)%atms(i)%charge * exp( pphase * COMPLEXI )

            ercpn = ercpn + cfgs(ib)%cl%g0(ircp) * ( tmprcpsum(ircp,ib) * conjg( tmprcpsum(ircp,ib) ) )

        end do

    end if

    if (cfgs(ib)%cl%gsumrecip) then

        call gsum(ercpn)

    end if

end subroutine add_atom_recip

!> @brief
!> - Updating Ewald sums in reciprocal space for replica `ib`:
!> - `tmprcpsum(i,ib)` (where `i` is the index of reciprocal k-vectors)
!! should be calculated elsewhere beforehand (see `add_atom_recip` etc)
!> - the corresponding elements `tmprcpsum(i,ib)` are reset to zeros
!> @using 
!> - `cell_module`
!> - `parallel_loop_module`, only : rcp_start, rcp_step

!> Updates Ewald sums in reciprocal space for replica `ib`
subroutine update_rcpsums(ib)

    use cell_module
    use parallel_loop_module, only : rcp_start, rcp_step

    implicit none

        !> replica identifier
    integer, intent(in) :: ib

    integer :: i, rcp_last !, start, step

    !rcp_last = cfgs(ib)%cl%numgvec
    !start = rcp_start
    !step  = rcp_step

    !do i = rcp_start, rcp_last, rcp_step
    !    cfgs(ib)%cl%rcpsum(i) = tmprcpsum(i,ib)
    !    tmprcpsum(i,ib) = 0.0_wp + 0.0_wp * COMPLEXI
    !end do

    !TU: The line below was causing an error in PSMC with electrostatics,
    !TU: while the commented out loop above did not. This is because the
    !TU: size of tmprcpsum can be larger than rcpsum in PSMC. The fix 
    !TU: is below.
    !cfgs(ib)%cl%rcpsum(:) = tmprcpsum(:,ib)
    cfgs(ib)%cl%rcpsum(:) = tmprcpsum( 1:size(cfgs(ib)%cl%rcpsum), ib)

    tmprcpsum(:,ib) = 0.0_wp + 0.0_wp * COMPLEXI

end subroutine update_rcpsums


!> calculates the total MFA correction for replica ib
subroutine update_mfa_energy(ib, emfa, etot)

    use kinds_f90
    use cell_module, only : cfgs
    use slit_module, only : uel_slit
    use comms_mpi_module, only : is_parallel, gsync, gsum
    use parallel_loop_module, only : tb_start_mol, tb_step_mol, tb_start_atm, tb_step_atm

    implicit none

    integer, intent(in)             :: ib
    real(kind = wp), intent (inout) :: emfa, etot

    integer :: im, i
    real(kind = wp) :: emfa0 !, chgi

    emfa0 = emfa
    emfa  = 0.0_wp

    !do  im = 1, cfgs(ib)%num_mols
    !    do  i = 1, cfgs(ib)%mols(im)%natom

    do im = tb_start_mol, cfgs(ib)%num_mols, tb_step_mol
        do i = tb_start_atm, cfgs(ib)%mols(im)%natom, tb_step_atm

            !chgi = cfgs(ib)%mols(im)%atms(i)%charge

            !AB: Extra interactions due to charged walls / MFA in the slit case
            if( cfgs(ib)%mols(im)%atms(i)%is_charged ) then
                emfa = emfa + cfgs(ib)%mols(im)%atms(i)%charge* & ! chgi*
                       uel_slit(ib, cfgs(ib)%mols(im)%atms(i)%rpos(3), &
                                    cfgs(ib)%vec%latvector(3,3))
            end if
        end do
    end do

    if( is_parallel ) then
        call gsync
        call gsum(emfa)
    end if

    etot = etot-emfa0+emfa

end subroutine update_mfa_energy

!> @brief
!> - Calculates Feynman-Hibbs corrections
!> @usage 
!> - Only works in conjunction with tabulated (interpolated) VdW potentials
!> @using 
!> - `kinds_f90`
!> - `vdw_module`, only : fh_correct_vdw

!> Calculates Feynman-Hibbs corrections
subroutine fh_correction(temperature, rvdw)

    use kinds_f90
    use vdw_module, only : fh_correct_vdw

    implicit none

        !> temperature and VdW cutoff radius
    real (kind = wp), intent(in) :: temperature, rvdw

    call  fh_correct_vdw(temperature, rvdw)

end subroutine fh_correction


#define MOLENG
#define MANY

!TU: Note that here the VdW energy returned by this procedure includes the long-range VdW correction
!> calculates the total energy in replica `ib` using a verlet neighbour list
subroutine total_energy(ib, coultyp, vdwcut, shortrangecut, dielec, ereal, evdw, &
                        ethree, epair, eang, efour, emany, eext, emfa, vir, & 
                        mrealnonb, mrealbond, mself, mcrct, mvdwn, mvdwb, mpair, mthree, mang, &
                        mfour, mmany, mext)

    use kinds_f90
    use constants_module, only : CTOINTERNAL, RCORE2, uout, HALF, SLIT_EXT0, SLIT_EXT1, SLIT_EXT2, &
                                 ATM_TYPE_METAL
    use species_module, only : number_of_molecules
    use vdw_module
    use bond_module, only : ntpbond, bond_energy
    use cell_module
    use thbpotential_module, only : numthb
    use metpot_module, only : lmetab, metal_energy, eam_embed, nmetpot
    use tersoff_module
    use coul_module, only : explicitCoulomb, realspaceenergy, atomself_energy, &
                            shiftrealspaceenergy, shiftdamprealspaceenergy, ewald_correct
    use angle_module, only : numang
    use inversion_module, only : numinv
    use dihedral_module, only : numdih
    use external_potential_module, only : ext_pot_energy, nextpot
    use comms_mpi_module, only : is_serial, is_parallel, gsync, gsum
    use parallel_loop_module, only : tb_start_mol, tb_step_mol, tb_start_atm, tb_step_atm
    use slit_module, only : in_slit, walls_soft, onoff_rc, onoff_xy, coul_explicit, &
                            uel_slit, slit_zfrac1, slit_zfrac2

    implicit none

    integer, intent(in)             :: ib, coultyp
    real(kind = wp), intent (in)    :: vdwcut, shortrangecut, dielec
    real(kind = wp), intent (inout) :: ereal, evdw, ethree, epair, eang, efour, emany, eext, emfa, vir
    real(kind = wp), intent (inout) :: mrealnonb(:), mrealbond(:), mvdwn(:), mvdwb(:), mpair(:), &
                                       mthree(:), mang(:), mfour(:), mmany(:), mself(:), mcrct(:), mext(:)

    integer :: coultype
    integer :: typi, typj, i, ii, im,  j, jm, nbr, imol, jmol, rcp, exclude, nlast
    real(kind = wp) :: volinv, virn, virc, virr, virv, virt, sigmac, sigmav, sigmab, gammac, gammav, gammab
    real(kind = wp) :: r, rsq, virter, virp, rxy2, rcut2
    real(kind = wp) :: chgi, chgj, xx, yy, zz, rxi, ryi, rzi, chg, trho
    real(kind = wp) :: ax, ay, az, dz, bx, by, bz, rx, ry, rz
    real(kind = wp) :: eng, eself, ecrct, eters, tmpext, buf(7)
    integer :: dihtyp
    real(kind = wp) :: dihvdwinc, dihcoulinc

    !TU: Spins of a pair of atoms; required for orientation-dependent VdW potentials
    real(kind = wp) :: spini(3), spinj(3)
    !TU: Separation vector between atoms after accounting for PBCs
    real(kind = wp) :: rvec(3)
    
    real(kind = wp) :: tmpdih, tmpreal, tmpvdw, tmpinv, tmpang

    !AB: testing cutoff in slit
    !integer :: ncpairs
    !ncpairs = 0

    logical :: coul_in_slit, do_coul, do_corr, coul_one, coul_two, coul_thr

    integer, save :: ncall = 0

    ncall = ncall+1

    if( coultyp == 0 ) then
        coultype = 0
        coul_in_slit = .false.
        
        do_coul  = .false.
        do_corr  = .false.
        coul_one = .false.
        coul_two = .false.
        coul_thr = .false.
    else
        coultype = abs(coultyp)
        coul_in_slit = in_slit .and. coul_explicit
        
        do_coul  = .true.
        do_corr  = ( coultyp == 1 )
        coul_one = ( coultype < 3 )
        coul_two = ( coultype == 3 )
        coul_thr = ( coultype == 4 )
    end if

    !AB: square of cut-off radius
    rcut2 = shortrangecut*shortrangecut

    !TU: The following 2 lines pertain to AB's old implmentation of long-range VdW
    !TU: corrections, which is no longer in use
    !volinv= 1.0_wp
    !if( is_vdwlrc ) volinv= 1.0_wp/cfgs(ib)%vec%volume
    volinv= 1.0_wp/cfgs(ib)%vec%volume

    !AB: temporary
    eng   = 0.0_wp
    eself = 0.0_wp
    ecrct = 0.0_wp

    tmpinv  = 0.0_wp
    tmpreal = 0.0_wp
    tmpdih  = 0.0_wp
    tmpvdw  = 0.0_wp
    tmpang  = 0.0_wp

    !AB: actual I/O (ref)
    !ereal, evdw, ethree, epair, eang, efour, emany, eext, emfa,
    ereal = 0.0_wp
    evdw = 0.0_wp
    ethree = 0.0_wp
    epair = 0.0_wp
    eang = 0.0_wp
    efour = 0.0_wp
    emany = 0.0_wp
    eext = 0.0_wp
    emfa = 0.0_wp

#ifdef MOLENG
    mrealnonb = 0.0_wp
    mrealbond = 0.0_wp
    mvdwn = 0.0_wp
    mvdwb = 0.0_wp
    mpair = 0.0_wp
    mthree = 0.0_wp
    mang = 0.0_wp
    mfour = 0.0_wp
    mmany = 0.0_wp
    mself = 0.0_wp
    mcrct = 0.0_wp
    mext = 0.0_wp
#endif

    vir = 0.0_wp
    virn = 0.0_wp
    virp = 0.0_wp
    virv = 0.0_wp
    virc = 0.0_wp
    virr = 0.0_wp
    virt = 0.0_wp
    virter = 0.0_wp

    if( nmetpot > 0 ) rho = 0.0_wp

    !TU: Long-range correction
    evdw = vdw_lrc_energy( volinv, cfgs(ib)%nums_elemts )

    ! do two body - real space coulomb and vdw
    do im = tb_start_mol, cfgs(ib)%num_mols, tb_step_mol

        do i = tb_start_atm, cfgs(ib)%mols(im)%natom, tb_step_atm

            imol = cfgs(ib)%mols(im)%mol_label
            typi = cfgs(ib)%mols(im)%atms(i)%atlabel
            chgi = cfgs(ib)%mols(im)%atms(i)%charge
              ii = cfgs(ib)%mols(im)%glob_no(i)

#ifdef MANY
            if(cfgs(ib)%mols(im)%atms(i)%atype == ATM_TYPE_METAL .and. nmetpot > 0) then

                trho = 0.0_wp
                call metalatom_rho(ib, im, i, ii, shortrangecut, trho)
                rho(ii) = trho

            end if

            if(ntersoff > 0) then

                eters = 0.0
                call atom_tersoff(ib, im, i, shortrangecut, eters, virter)
                eters = eters * 0.5_wp
                emany = emany + eters
                mmany(imol) = mmany(imol) + eters

            end if
#endif
            if( do_corr ) then 

                call atomself_energy(chgi, cfgs(ib)%cl%eta, dielec, eng)

                eself = eself + eng
#ifdef MOLENG
                mself(imol) = mself(imol) + eng
#endif
            end if

            ax = cfgs(ib)%mols(im)%atms(i)%rpos(1)
            ay = cfgs(ib)%mols(im)%atms(i)%rpos(2)
            az = cfgs(ib)%mols(im)%atms(i)%rpos(3)

            !AB: Add extra interactions due to charged walls / MFA in the slit case
            if( coul_in_slit .and. cfgs(ib)%mols(im)%atms(i)%is_charged ) &
                emfa = emfa + chgi*uel_slit(ib, az, cfgs(ib)%vec%latvector(3,3))

            if(nextpot > 0) then

                tmpext = 0.0_wp

                if( walls_soft > 0 ) then

                    call ext_pot_energy(typi, ax, ay, (az - cfgs(ib)%vec%latvector(3,3)*slit_zfrac1), tmpext)

                    eext = eext + tmpext
#ifdef MOLENG
                    mext(imol) = mext(imol) + tmpext
#endif
                    if( walls_soft == 2 ) then

                        call ext_pot_energy(typi, ax, ay, (cfgs(ib)%vec%latvector(3,3)*slit_zfrac2 - az), tmpext)

                        eext = eext + tmpext
#ifdef MOLENG
                        mext(imol) = mext(imol) + tmpext
#endif
                    end if

                else
                
                    call ext_pot_energy(typi, ax, ay, az, tmpext)

                    eext = eext + tmpext
#ifdef MOLENG
                    mext(imol) = mext(imol) + tmpext
#endif

                end if

            end if
            
            nlast = cfgs(ib)%mols(im)%nlist%numnbrs(i)

            do nbr = 1, nlast !cfgs(ib)%mols(im)%nlist%numnbrs(i)

                jm = cfgs(ib)%mols(im)%nlist%nbr_list(1, nbr, i)
                j = cfgs(ib)%mols(im)%nlist%nbr_list(2, nbr, i)

                if( cfgs(ib)%mols(im)%glob_no(i) >= cfgs(ib)%mols(jm)%glob_no(j) ) cycle

                typj = cfgs(ib)%mols(jm)%atms(j)%atlabel
                jmol = cfgs(ib)%mols(jm)%mol_label

                bx = cfgs(ib)%mols(jm)%atms(j)%rpos(1)
                by = cfgs(ib)%mols(jm)%atms(j)%rpos(2)
                bz = cfgs(ib)%mols(jm)%atms(j)%rpos(3)

                xx = ax - bx
                yy = ay - by
                zz = az - bz

                call pbc_cart_pos_calc(ib, xx, yy, zz)

                rxy2 = xx * xx + yy * yy
                rsq  = rxy2 + zz * zz

                rvec = (/ xx, yy, zz /)
                
                if ( rsq*onoff_rc < rcut2 ) then 
                !AB: onoff_rc = 1 -> normal spherical cutoff (in bulk)
                !AB: onoff_rc = 0 -> no spherical cuttoff (always true in slit)

                    r = sqrt(rsq)

                    exclude = 0
                    if( im == jm ) then

                        if( cfgs(ib)%mols(im)%blist%npairs > 0 ) then
                        !AB: first check for molecule connectivity 
                        !AB: even with 'exclude' in FIELD, not to miss on bonds!

                            exclude = cfgs(ib)%mols(im)%nlist%exclist(i,j)

                        else if( cfgs(ib)%mols(im)%exc_coul_ints ) then
                        !AB: if no bonds, still check for 'exclude' in FIELD
                            exclude = MAXINT1

                        end if
                    end if

                    sigmav = 0.0
                    gammav = 0.0
                    sigmab = 0.0
                    gammab = 0.0


                    !TU: Details for the exclusion/inclusion model:
                    !TU: * exclude=0 means the pair are in different molecules, and hence VdW and Coulomb interactions 
                    !TU:   are INCLUDED
                    !TU: * exclude=1,2,3,.. (small integers) means the pair is bonded with bond type 'i', and they (VdW and
                    !TU:   Coulomb interactions) are EXCLUDED
                    !TU: * exclude=-1,-2,-3,.. (small integers) means the pair is bonded with bond type 'i', and the
                    !TU:   interactions are INCLUDED
                    !TU: * exclude=MAXINT1 means the pair is within the same molecule, is unbonded, not part of
                    !TU:   an angle or dihedral, and the interactions are EXCLUDED
                    !TU: * exclude=-MAXINT1 means the pair is within the same molecule, is unbonded, not part of an angle
                    !TU:   or dihedral, and the interactions are INCLUDED
                    !TU: * exclude=MAXINT3 means the pair is within the same molecule, unbonded, is part of an angle (1-3), 
                    !TU:   is not part of a dihedral, and the interactions are EXCLUDED
                    !TU: * exclude=-MAXINT3 means the pair is within the same molecule, unbonded, is part of an angle (1-3), 
                    !TU:   is not part of a dihedral, and the interactions are INCLUDED
                    !TU: * exclude between (inclusive) EXCDIH+1 and EXCDIH+ndih, where ndih is the number of types of 
                    !TU:   dihedrals specified in FIELD, means the pair is within the same molecule, unbonded,
                    !TU:   not part of an angle (1-3), and is a 1-4 in a dihedral (where EXCDIH+i means dihedral 
                    !TU:   type i), and ther interactions are EXCLUDED
                    !TU: * exclude between (inclusive) -(EXCDIH+1) and -(EXCDIH+ndih) means the pair is within the same
                    !TU:   molecule, unbonded, not part of an angle (1-3), is a 1-4 in a dihedral, and the interactions
                    !TU:   are INCLUDED or PARTIALLY INCLUDED (for VdW or electrostatic)
                    !TU:

                    !TU: exclude<EXCDIH and exclude/=0 catches bonded pairs - note that 'bond_energy' returns 0 if 
                    !TU: abs(exclude) > the number of bond types defined in FIELD, and hence will return 0 for a pair 
                    !TU: of unbonded particles which are part of a 1-3 or 1-4 pair and are included or partially included
                    !TU: (for which 'exclude' is 
                    !AB: if there are bonds and this is a bond => calculate bond energy
                    !AB: pressuming that ntpbond cannot be negative.
                    if( ntpbond*exclude /= 0 .and. exclude < EXCDIH ) then

                        call bond_energy(exclude, r, rsq, sigmab, gammab)
                        epair = epair + sigmab
#ifdef MOLENG
                        mpair(imol) = mpair(imol) + sigmab
#endif
                        !virn = virn + gammab * rsq

                    end if

                    
                    !TU: exclude < 1 means we include the interaction (fully or partially for the case of 1-4 dihedral)
                    !AB: if this pair is NOT EXCLUDED from all 'non-bonded' interactions (i.e. INCLUDED)
                    !AB: include BOTH VDW AND COULOMB for both bonded (< 0) and non-bonded (== 0) pairs
                    if( exclude < 1 ) then

                        !AB: include VDW interactions if any
                        if( ntpvdw > 0 ) then 

                            spini = cfgs(ib)%mols(im)%atms(i)%spin
                            spinj = cfgs(ib)%mols(jm)%atms(j)%spin
                            
                            call vdw_energy(typi, typj ,r , rsq, vdwcut, eunit, sigmav, volinv, rvec, spini, spinj)
                            
                            !TU: Check for partial VdW inclusion for 1-4 dihedral and
                            !TU: amend the VdW contribution accordingly
                            call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
                            if( dihtyp > 0 ) then

                                sigmav = sigmav * dihvdwinc

                            end if

                            if( ncall == 1 .and. (sigmav - vdwcap) > 1.e-8_wp ) &
                                write(uout,*)"total_energy(): VdW energy > E_cap (overlap?) !!!",r, &
                                             sigmav,vdwcap,(sigmav-vdwcap),im,i,jm,j

                            evdw = evdw + sigmav

                            if( is_serial .and. sigmav > vdwcap ) return

#ifdef MOLENG
                            if (imol == jmol) then
                                mvdwb(imol) = mvdwb(imol) + sigmav
                            else
                                mvdwn(imol) = mvdwn(imol) + sigmav 
                                mvdwn(jmol) = mvdwn(jmol) + sigmav 
                            end if
#endif
                            !virv = virv + gammav * rsq

                        end if

                        !AB: include Coulomb interactions if any
                        if( do_coul ) then

                            !TU-: This branch corresponds to the atoms being in different molecules, or 
                            !TU-: UNBONDED within the same molecule while intra-molecular Coulomb energies
                            !TU-: are INCLUDED

                            sigmac = 0.0
                            gammac = 0.0

                            !chgi = cfgs(ib)%mols(im)%atms(i)%charge
                            chgj = cfgs(ib)%mols(jm)%atms(j)%charge

                            if( coul_explicit ) then

                                !AB: aka "explicit" or "direct"
                                !AB: bare Coulomb term: ~ q1*q2/r
                                !AB: not shifted & not damped

                                !AB: testing cutoff in slit
                                !ncpairs = ncpairs+1

                                !AB: onoff_xy = 1 -> cylindrical cutoff (in slit)
                                !AB: onoff_xy = 0 -> no cylindrical cuttoff (always true in bulk)

                                if( rxy2*onoff_xy < rcut2 ) &
                                    call explicitCoulomb(chgi, chgj, r , dielec, sigmac, gammac)

                            else if( coul_one ) then

                                !AB: corresponds to "ewald" (1) or "real" (2)
                                !AB: normal real-space Ewald term: ~ (q1*q2/r)*erfc(eta*r)
                                !AB: coultype = 2 does not include reciprocal space sums nor any shift!
                                !AB: the latter case renamed from "direct" to "real"

                                call realspaceenergy(chgi, chgj, r , dielec, sigmac, gammac)

                            else if( coul_two ) then

                                !AB: corresponds to "shifted"
                                !AB: normal Coulomb term shifted to produce zero at cutoff: ~ (q1*q2/r) + shift

                                call shiftrealspaceenergy(chgi, chgj, r, rsq, dielec, sigmac, gammac)

                            else if( coul_thr ) then

                                !AB: corresponds to "damped"
                                !AB: damped and shifted Coulomb term: ~ (q1*q2/r)*erfc(eta*r) + shift

                                call shiftdamprealspaceenergy(chgi, chgj, r, shortrangecut, dielec, sigmac, gammac)

                            end if

                            !TU: Check for partial Coulomb inclusion for 1-4 dihedral and
                            !TU: amend the Coulomb contribution accordingly
                            call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
                            if( dihtyp > 0 ) then

                                sigmac = sigmac * dihcoulinc

                            end if

                            ereal = ereal + sigmac

#ifdef MOLENG
                            if (imol == jmol) then
                                mrealbond(imol) = mrealbond(imol) + sigmac
                            else
                                mrealnonb(imol) = mrealnonb(imol) + sigmac
                                mrealnonb(jmol) = mrealnonb(jmol) + sigmac 
                            end if
#endif
                            !virc = virc + gammac * rsq

                        end if

#ifdef MANY
                        !the twobody part of the "metal" potential is added to the vdw energy
                        if( exclude == 0 .and. nmetpot > 0 .and. &
                            cfgs(ib)%mols(im)%atms(i)%atype == ATM_TYPE_METAL .and. &
                            cfgs(ib)%mols(jm)%atms(j)%atype == ATM_TYPE_METAL ) then

                            sigmav = 0.0
                            call metal_energy(typi, typj, r, sigmav, vir)
                            evdw = evdw + sigmav

                            if (imol == jmol) then
                                mvdwb(imol) = mvdwb(imol) + sigmav
                            else
                                mvdwn(imol) = mvdwn(imol) + sigmav 
                                mvdwn(jmol) = mvdwn(jmol) + sigmav 
                            end if

                        end if
#endif

                    end if

                    !TU: exclude>=1 corresponds to excluded pairs; dihtyp>1 corresponds to partially or fully included
                    !TU: 1-4 dihedral pairs. Either way we must correct for exclusions or partial exclusions
                    call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
                    if( (exclude >=1 .or. dihtyp>0) .and. do_corr ) then 
                    !AB: correct for overcounted Ewald contribution due to excluded pairs

                        sigmac = 0.0
                        gammac = 0.0

                        !chgi = cfgs(ib)%mols(im)%atms(i)%charge
                        chgj = cfgs(ib)%mols(jm)%atms(j)%charge

                        call ewald_correct(chgi, chgj, r , rsq, cfgs(ib)%cl%eta, &
                                           dielec, sigmac, gammac)

                        !TU: For partial inclusion of 1-4 Coulomb we must partially EXCLUDE fraction (1-dihcoulinc) 
                        !TU: of the interaction
                        if( dihtyp>0 ) then

                            sigmac = sigmac * (1.0_wp - dihcoulinc)

                        end if

                        ecrct = ecrct + sigmac

#ifdef MOLENG
                        mcrct(imol) = mcrct(imol) - sigmac
#endif
                    end if

                end if ! done - if ( rsq*onoff_rc < rcut2 )

            end do

        end do

    end do

    !AB: testing cutoff in slit
    !write(uout,'(/,1x,(a,i10),/)')'Check interacting pairs : ',ncpairs

#ifdef MANY
    !metal potentials beyond pairs
    if(nmetpot > 0) then

        do im = tb_start_mol, cfgs(ib)%num_mols, tb_step_mol
        
            imol = cfgs(ib)%mols(im)%mol_label

            do i = tb_start_atm, cfgs(ib)%mols(im)%natom, tb_step_atm
        
                ii = cfgs(ib)%mols(im)%glob_no(i)

                if (lmetab) then ! calculate embedding energy

                    call eam_embed(cfgs(ib)%mols(im)%atms(i)%atlabel, rho(ii), eng)
                    emany = emany - eng
                    
                    mmany(imol) = mmany(imol) - eng

                else if(rho(ii) > 0.0_wp) then

                    eng = sqrt(rho(ii))
                    emany = emany - eng !sqrt(rho(ii))

                    mmany(imol) = mmany(imol) - eng !sqrt(rho(ii))

                end if

            end do

        end do
        
        if( is_parallel ) then
        !if( stepa > 1 ) then 
            call gsum(emany)
            call gsum(mmany)
            call gsum(rho)
        end if

    end if

    !add in nonbonded three body
    if (numthb > 0) call thb_energy(ib, ethree, mthree, virt)
#endif

    !add in bonded three-body energy (angles)
    if (numang > 0) call total_ang_energy(ib, eang, mang)

    !bonded dihedral energy
    if (numdih > 0) call total_dih_energy(ib, efour, mfour)

    !bonded inversion energy
    if (numinv > 0) call total_inv_energy(ib, efour, mfour)

    if( is_parallel ) then
    
        call gsync

        buf(1) = evdw
        buf(2) = ereal
        buf(3) = epair
        buf(4) = ecrct
        buf(5) = eself
        buf(6) = eext
        buf(7) = emfa

        call gsum(buf)

        evdw  = buf(1)
        ereal = buf(2)
        epair = buf(3)
        ecrct = buf(4)
        eself = buf(5)
        eext  = buf(6)
        emfa  = buf(7)

#ifdef MOLENG
        call gsum(mrealnonb)
        call gsum(mrealbond)
        call gsum(mself)
        call gsum(mcrct)
        call gsum(mvdwn)
        call gsum(mvdwb)
        call gsum(mpair)
        call gsum(mext)
#endif
    end if

    vir = 0.0_wp

    ereal = eself + (ereal - ecrct)
    
#ifdef MOLENG
    !mcrct =-mcrct
    mvdwn = mvdwn*0.5_wp
    mrealnonb = mrealnonb*0.5_wp
#endif

end subroutine total_energy


!> calculate the energy of a given molecule (for 'rigid' moves: translation/rotation)
!> reciprocal space ewald and bonded interactions are not calculated
subroutine molecule_energy(ib, im, coultyp, dielec, vdwcut, shortrangecut, verletshell, &
                        ereal, evdw, ethree, emany, eext, emfa, vir, mrealnonb, mrealbond, mvdwn,  &
                        mvdwb, mthree, mfour, mmany, mext)

    use kinds_f90
    use control_type
    use constants_module, only : CTOINTERNAL, RCORE2, HALF, SLIT_EXT0, SLIT_EXT1, SLIT_EXT2, &
                                 ATM_TYPE_METAL
    use species_module, only : number_of_molecules
    use vdw_module
    use coul_module
    use thbpotential_module
    use cell_module
    use metpot_module, only : lmetab, metal_energy, eam_embed, nmetpot
    use tersoff_module
    use external_potential_module, only : ext_pot_energy, nextpot
    use slit_module, only : in_slit, walls_soft, onoff_rc, onoff_xy, coul_explicit, &
                            uel_slit, slit_zfrac1, slit_zfrac2
    use comms_mpi_module, only : is_serial, is_parallel, gsync, gsum
    use parallel_loop_module, only : tb_start_mol, tb_step_mol, tb_start_atm, tb_step_atm

    implicit none

    integer, intent(in) :: ib, im, coultyp

    real(kind = wp), intent (in) :: dielec, vdwcut, shortrangecut, verletshell

    real(kind = wp), intent (inout) :: ereal, evdw, ethree, emany, eext, emfa, vir
    real(kind = wp), intent (inout) :: mrealnonb(:), mrealbond(:), mvdwn(:), mvdwb(:), mthree(:), &
                                       mfour(:), mmany(:), mext(:)

    integer :: coultype
    integer :: typi, typj, i, ii, j, jm, nbr, imol, jmol, exclude, start, step, last, nlast
    real(kind = wp) :: volinv, virn, virc, virr, virv, virt, sigmac, sigmav, sigmab, gammac, gammav, gammab
    real(kind = wp) :: r, rsq, virter, tmpext, rxy2, rcut2
    real(kind = wp) :: ax, ay, az, bx, by, bz, rx, ry, rz
    real(kind = wp) :: chgi, chgj, xx, yy, zz, rxi, ryi, rzi, chge, trho
    real(kind = wp) :: eng, eters, buf(4)

    integer :: dm

    logical :: coul_in_slit, do_coul, do_corr, coul_one, coul_two, coul_thr

    !TU: Spins of a pair of atoms; required for orientation-dependent VdW potentials
    real(kind = wp) :: spini(3), spinj(3)
    !TU: Separation vector between atoms after accounting for PBCs
    real(kind = wp) :: rvec(3)
    

    if( coultyp == 0 .or. .not.cfgs(ib)%mols(im)%has_charge ) then
        coultype = 0
        coul_in_slit = .false.
        
        do_coul  = .false.
        do_corr  = .false.
        coul_one = .false.
        coul_two = .false.
        coul_thr = .false.
    else
        coultype = abs(coultyp)
        coul_in_slit = in_slit .and. coul_explicit
        
        do_coul  = .true.
        do_corr  = ( coultyp == 1 )
        coul_one = ( coultype < 3 )
        coul_two = ( coultype == 3 )
        coul_thr = ( coultype == 4 )
    end if

    !AB: square of cut-off radius
    rcut2 = shortrangecut*shortrangecut

    !TU: The following 2 lines pertain to AB's old implmentation of long-range VdW
    !TU: corrections, which is no longer in use
    !volinv= 1.0_wp
    !if( is_vdwlrc ) volinv= 1.0_wp/cfgs(ib)%vec%volume
    volinv = 0.0_wp

    !zero energies + virials
    ereal = 0.0_wp
    evdw = 0.0_wp
    ethree = 0.0_wp
    emany = 0.0_wp
    eext = 0.0_wp
    emfa = 0.0_wp
    eng = 0.0_wp

#ifdef MOLENG
    mrealnonb = 0.0_wp
    mrealbond = 0.0_wp
    mvdwn = 0.0_wp
    mvdwb = 0.0_wp
    mthree = 0.0_wp
    mfour = 0.0_wp
    mmany = 0.0_wp
    mext = 0.0_wp
#endif

    vir = 0.0_wp
    virn = 0.0_wp
    virv = 0.0_wp
    virc = 0.0_wp
    virr = 0.0_wp
    virt = 0.0_wp
    virter = 0.0_wp

    if(nmetpot > 0) rho = 0.0_wp

    imol = cfgs(ib)%mols(im)%mol_label

    start = 1
    step  = 1
    if( is_parallel ) then
        !start = max(tb_start_mol,tb_start_atm)
        !step  = max(tb_step_mol,tb_step_atm)
        start = tb_start_atm
        step  = tb_step_atm
    end if

    last = cfgs(ib)%mols(im)%natom

    do i = start, last, step
    !do i = tb_start_atm, cfgs(ib)%mols(im)%natom, tb_step_atm
    !do i = 1, cfgs(ib)%mols(im)%natom

        !nbr  = cfgs(ib)%mols(im)%nlist%numnbrs(i)
        typi = cfgs(ib)%mols(im)%atms(i)%atlabel
        chgi = cfgs(ib)%mols(im)%atms(i)%charge
          ii = cfgs(ib)%mols(im)%glob_no(i)

#ifdef MANY
        if(cfgs(ib)%mols(im)%atms(i)%atype == ATM_TYPE_METAL) then

            trho = 0.0_wp
            call metalatom_rho(ib, im, i, ii, shortrangecut, trho)
            rho(ii) = trho

        end if

        if(ntersoff > 0) then

            eters = 0.0_wp
            call atom_tersoff(ib, im, i, shortrangecut, eters, virter)
            eters = eters * 0.5_wp
            emany = emany + eters
            mmany(imol) = mmany(imol) + eters

        end if
#endif

        ax = cfgs(ib)%mols(im)%atms(i)%rpos(1)
        ay = cfgs(ib)%mols(im)%atms(i)%rpos(2)
        az = cfgs(ib)%mols(im)%atms(i)%rpos(3)

        !AB: Add extra interactions due to charged walls / MFA in the slit case
        if( coul_in_slit .and. cfgs(ib)%mols(im)%atms(i)%is_charged ) &
            emfa = emfa + cfgs(ib)%mols(im)%atms(i)%charge* &
                          uel_slit(ib, az, cfgs(ib)%vec%latvector(3,3))

        if(nextpot > 0) then

            tmpext = 0.0_wp

            if( walls_soft > 0 ) then

                call ext_pot_energy(typi, ax, ay, (az - cfgs(ib)%vec%latvector(3,3)*slit_zfrac1), tmpext)

                eext = eext + tmpext
#ifdef MOLENG
                mext(imol) = mext(imol) + tmpext
#endif
                if( walls_soft == 2 ) then

                    call ext_pot_energy(typi, ax, ay, (cfgs(ib)%vec%latvector(3,3)*slit_zfrac2 - az), tmpext)

                    eext = eext + tmpext
#ifdef MOLENG
                    mext(imol) = mext(imol) + tmpext
#endif
                    
                end if

            else

                call ext_pot_energy(typi, ax, ay, az, tmpext)

                eext = eext + tmpext
#ifdef MOLENG
                mext(imol) = mext(imol) + tmpext
#endif
            end if

        end if
            
        nlast = cfgs(ib)%mols(im)%nlist%numnbrs(i)

        do nbr = 1, nlast !cfgs(ib)%mols(im)%nlist%numnbrs(i)

            jm = cfgs(ib)%mols(im)%nlist%nbr_list(1, nbr, i)
            j = cfgs(ib)%mols(im)%nlist%nbr_list(2, nbr, i)
            
            if( im == jm ) cycle
            if( cfgs(ib)%mols(im)%glob_no(i) == cfgs(ib)%mols(jm)%glob_no(j) ) cycle

            typj = cfgs(ib)%mols(jm)%atms(j)%atlabel
            jmol = cfgs(ib)%mols(jm)%mol_label

            bx = cfgs(ib)%mols(jm)%atms(j)%rpos(1)
            by = cfgs(ib)%mols(jm)%atms(j)%rpos(2)
            bz = cfgs(ib)%mols(jm)%atms(j)%rpos(3)

            xx = ax - bx
            yy = ay - by
            zz = az - bz

            call pbc_cart_pos_calc(ib, xx, yy, zz)

            rxy2 = xx * xx + yy * yy
            rsq  = rxy2 + zz * zz

            rvec = (/ xx, yy, zz /)
            
            if ( rsq*onoff_rc < rcut2 ) then 

                r = sqrt(rsq)

                exclude = 0

                sigmav = 0.0_wp
                gammav = 0.0_wp
                sigmab = 0.0_wp
                gammab = 0.0_wp

                !if( exclude < 1 ) then

                    if( ntpvdw > 0 ) then
                        
                        spini = cfgs(ib)%mols(im)%atms(i)%spin
                        spinj = cfgs(ib)%mols(jm)%atms(j)%spin

                        call vdw_energy(typi, typj ,r , rsq, vdwcut, eunit, sigmav, volinv, rvec, spini, spinj)

#ifdef MOLENG
                        if( imol == jmol ) then
                            mvdwb(imol) = mvdwb(imol) + sigmav
                        else
                            mvdwn(imol) = mvdwn(imol) + sigmav
                            mvdwn(jmol) = mvdwn(jmol) + sigmav
                        end if
#endif
                        evdw = evdw + sigmav

                        if( is_serial .and. sigmav > vdwcap ) return
!                        if( sigmav > vdwcap ) then
!debugging!
!                           write(*,*)
!                           write(*,*)"field::molecule_energy(..) VdW energy overflow : ", &
!                                evdw," >? ",vdwcap !,"; latest r = ",r
!                           write(*,*)"field::molecule_energy(..) latest r = ",r," for atom-types: ", &
!                                     typi," & ",typj,", atoms: ",i," & ",j
!                           write(*,*)
!
!                           if( is_serial ) return
!
!                        end if

                        !virv = virv + gammav * rsq

                    end if

                    if( do_coul ) then

                        sigmac = 0.0_wp
                        gammac = 0.0_wp

                        chgj = cfgs(ib)%mols(jm)%atms(j)%charge

                        if( coul_explicit ) then

                            if( rxy2*onoff_xy < rcut2 ) &
                                call explicitCoulomb(chgi, chgj, r , dielec, sigmac, gammac)

                        else if( coul_one ) then

                            call realspaceenergy(chgi, chgj, r , dielec, sigmac, gammac)

                        else if( coul_two ) then

                            call shiftrealspaceenergy(chgi, chgj, r, rsq, dielec, sigmac, gammac)

                        else if( coul_thr ) then

                            call shiftdamprealspaceenergy(chgi, chgj, r, shortrangecut, dielec, sigmac, gammac)

                        end if

#ifdef MOLENG
                        if (imol == jmol) then
                            mrealbond(imol) = mrealbond(imol) + sigmac 
                        else
                            mrealnonb(imol) = mrealnonb(imol) + sigmac
                            mrealnonb(jmol) = mrealnonb(jmol) + sigmac
                        end if
#endif
                        ereal = ereal + sigmac

                        !virc = virc + gammac * rsq

                    end if

#ifdef MANY
                    if( exclude == 0 .and. &
                        cfgs(ib)%mols(im)%atms(i)%atype == ATM_TYPE_METAL .and. &
                        cfgs(ib)%mols(jm)%atms(j)%atype == ATM_TYPE_METAL ) then

                        call metal_energy(typi, typj, r, eng, vir)
                        evdw = evdw + eng

                        if( imol == jmol ) then
                            mvdwb(imol) = mvdwb(imol) + eng
                        else
                            mvdwn(imol) = mvdwn(imol) + eng
                            mvdwn(jmol) = mvdwn(jmol) + eng
                        end if

                    end if
#endif

                !end if !( exclude < 1 )

            end if !( rsq*onoff_rc < rcut2 )

        end do

    end do

#ifdef MANY
    !add in nonbonded three body
    if(numthb > 0) call thb_energy(ib, ethree, mthree, virt)

    !metal potentials beyond pairs
    if(nmetpot > 0) then

        do i = start, last, step
        !do i = tb_start_atm, cfgs(ib)%mols(im)%natom, tb_step_atm
        !do i = 1, cfgs(ib)%mols(im)%natom

            !imol = cfgs(ib)%mols(im)%mol_label
            ii = cfgs(ib)%mols(im)%glob_no(i)

            if (lmetab) then ! calculate embedding energy

                call eam_embed(cfgs(ib)%mols(im)%atms(i)%atlabel, rho(ii), eng)
                emany = emany - eng 

                mmany(imol) = mmany(imol) - eng

            else if(rho(ii) > 0.0_wp) then

                eng = sqrt(rho(ii))
                emany = emany - eng !sqrt(rho(ii))

                mmany(imol) = mmany(imol) - eng !sqrt(rho(ii))

            end if

        end do
        
        if( step > 1 ) then 
            call gsum(emany)
            call gsum(mmany)
            call gsum(rho)
        end if
        
    end if
#endif

    if( step > 1 ) then 
    !if( is_parallel .and. stepa > 1 ) then
    
        call gsync
        
        call gsum(evdw)
        call gsum(ereal)
        call gsum(eext)
        call gsum(emfa)
        
        buf(1) = evdw
        buf(2) = ereal
        buf(3) = eext
        buf(4) = emfa

        call gsum(buf)

        evdw  = buf(1)
        ereal = buf(2)
        eext  = buf(3)
        emfa  = buf(4)

#ifdef MOLENG
        call gsum(mrealnonb)
        call gsum(mrealbond)
        call gsum(mvdwb)
        call gsum(mvdwn)
        call gsum(mext)
#endif
    end if

#ifdef MOLENG
    mrealnonb = mrealnonb * 0.5_wp
    mvdwn = mvdwn * 0.5_wp
    !mfour = mfour * 0.5_wp
#endif

end subroutine molecule_energy


!> calculate the energy of a given molecule (for GCMC moves)
!> the self energy and bonded interactions are also calculated
!> reciprocal space ewald is done elsewhere
subroutine molecule_energy2(ib, im, coultyp, dielec, vdwcut, shortrangecut, verletshell, &
                        ereal, eselfcoul, evdw, eselfvdw, ethree, epair, eang, efour, emany, eext, emfa, &
                        vir, mrealnonb, mrealbond, mself, mcrct, mvdwn, mvdwb, mpair, &
                        mthree, mang, mfour, mmany, mext)

    use kinds_f90
    use control_type
    use constants_module, only : CTOINTERNAL, RCORE2, HALF, SLIT_EXT0, SLIT_EXT1, SLIT_EXT2, &
                                 ATM_TYPE_METAL
    use species_module, only : number_of_molecules
    use vdw_module
    use bond_module, only : ntpbond, bond_energy
    use angle_module, only : numang
    use dihedral_module, only : numdih
    use inversion_module, only : numinv
    use coul_module
    use thbpotential_module
    use cell_module
    use metpot_module, only : lmetab, metal_energy, eam_embed, nmetpot
    use tersoff_module
    use external_potential_module, only : ext_pot_energy, nextpot
    use slit_module, only : in_slit, walls_soft, onoff_rc, onoff_xy, coul_explicit, &
                            uel_slit, slit_zfrac1, slit_zfrac2
    use comms_mpi_module, only : is_serial, is_parallel, gsync, gsum
    use parallel_loop_module, only : tb_start_mol, tb_step_mol, &
                                     tb_start_atm, tb_step_atm

    implicit none

    integer, intent(in) :: ib, im, coultyp

    real(kind = wp), intent (in) :: dielec, vdwcut, shortrangecut, verletshell

    real(kind = wp), intent (inout) :: ereal, evdw, ethree, epair, eang, efour, emany, &
                                       eext, emfa, vir, eselfcoul, eselfvdw
    real(kind = wp), intent (inout) :: mrealnonb(:), mrealbond(:), mvdwn(:), mvdwb(:), mpair(:), &
                                       mthree(:), mang(:), mfour(:), mmany(:), mself(:), mcrct(:) , mext(:)

    integer :: coultype
    integer :: typi, typj, i, ii, j, jm, nbr, imol, jmol, ircp, exclude, start, step, last, nlast
    real(kind = wp) :: volinv, virn, virc, virr, virv, virt, sigmac, sigmav, sigmab, gammac, gammav, gammab
    real(kind = wp) :: r, rsq, virter, tmpext, rxy2, rcut2
    real(kind = wp) :: ax, ay, az, bx, by, bz, rx, ry, rz
    real(kind = wp) :: chgi, chgj, xx, yy, zz, rxi, ryi, rzi, chge, trho
    real(kind = wp) :: eng, eters, ecrct, buf(8)
    integer :: dihtyp
    real(kind = wp) :: dihvdwinc, dihcoulinc


    integer dm

    logical :: coul_in_slit, do_coul, do_corr, coul_one, coul_two, coul_thr

    !TU: Spins of a pair of atoms; required for orientation-dependent VdW potentials
    real(kind = wp) :: spini(3), spinj(3)
    !TU: Separation vector between atoms after accounting for PBCs
    real(kind = wp) :: rvec(3)

    if( coultyp == 0 .or. .not.cfgs(ib)%mols(im)%has_charge ) then
        coultype = 0
        coul_in_slit = .false.
        
        do_coul  = .false.
        do_corr  = .false.
        coul_one = .false.
        coul_two = .false.
        coul_thr = .false.
    else
        coultype = abs(coultyp)
        coul_in_slit = in_slit .and. coul_explicit
        
        do_coul  = .true.
        do_corr  = ( coultyp == 1 )
        coul_one = ( coultype < 3 )
        coul_two = ( coultype == 3 )
        coul_thr = ( coultype == 4 )
    end if

    !AB: square of cut-off radius
    rcut2 = shortrangecut*shortrangecut

    !TU: The following 2 lines pertain to AB's old implmentation of long-range VdW
    !TU: corrections, which is no longer in use
    !volinv= 1.0_wp
    !if( is_vdwlrc ) volinv= 1.0_wp/cfgs(ib)%vec%volume
    volinv = 0.0_wp

    !AB: temporary
    eng = 0.0_wp
    eselfcoul = 0.0_wp
    eselfvdw = 0.0_wp
    ecrct = 0.0_wp

    !AB: actual I/O (ref)
    !ereal, evdw, ethree, epair, eang, efour, emany, eext, emfa,
    ereal = 0.0_wp
    evdw = 0.0_wp
    ethree = 0.0_wp
    epair = 0.0_wp
    eang = 0.0_wp
    efour = 0.0_wp
    emany = 0.0_wp
    eext = 0.0_wp
    emfa = 0.0_wp

#ifdef MOLENG
    mrealnonb = 0.0_wp
    mrealbond = 0.0_wp
    mvdwn = 0.0_wp
    mvdwb = 0.0_wp
    mpair = 0.0_wp
    mthree = 0.0_wp
    mang = 0.0_wp
    mfour = 0.0_wp
    mmany = 0.0_wp
    mself = 0.0_wp
    mcrct = 0.0_wp
    mext = 0.0_wp
#endif

    vir = 0.0_wp
    virn = 0.0_wp
    virv = 0.0_wp
    virc = 0.0_wp
    virr = 0.0_wp
    virt = 0.0_wp
    virter = 0.0_wp

    if(nmetpot > 0) then
        rho = 0.0_wp
    end if

    imol = cfgs(ib)%mols(im)%mol_label
    
    start = 1
    step  = 1
    if( is_parallel ) then
        !start = max(tb_start_mol,tb_start_atm)
        !step  = max(tb_step_mol,tb_step_atm)
        start = tb_start_atm
        step  = tb_step_atm
    end if
    
    last = cfgs(ib)%mols(im)%natom

    do i = start, last, step
    !do i = tb_start_atm, cfgs(ib)%mols(im)%natom, tb_step_atm
    !do i = 1, cfgs(ib)%mols(im)%natom

        !nbr  = cfgs(ib)%mols(im)%nlist%numnbrs(i)
        typi = cfgs(ib)%mols(im)%atms(i)%atlabel
        chgi = cfgs(ib)%mols(im)%atms(i)%charge
          ii = cfgs(ib)%mols(im)%glob_no(i)

#ifdef MANY
        if(cfgs(ib)%mols(im)%atms(i)%atype == ATM_TYPE_METAL) then

            trho = 0.0_wp
            call metalatom_rho(ib, im, i, ii, shortrangecut, trho)
            rho(ii) = trho

        end if

        if(ntersoff > 0) then

            eters = 0.0_wp
            call atom_tersoff(ib, im, i, shortrangecut, eters, virter)
            eters = eters * 0.5_wp
            emany = emany + eters
            mmany(imol) = mmany(imol) + eters

        end if
#endif

        if( do_corr ) then 

            call atomself_energy(chgi, cfgs(ib)%cl%eta, dielec, eng)

            eselfcoul = eselfcoul + eng
#ifdef MOLENG
            mself(imol) = mself(imol) + eng
#endif
        end if

        ax = cfgs(ib)%mols(im)%atms(i)%rpos(1)
        ay = cfgs(ib)%mols(im)%atms(i)%rpos(2)
        az = cfgs(ib)%mols(im)%atms(i)%rpos(3)

        !AB: Add extra interactions due to charged walls / MFA in the slit case
        if( coul_in_slit .and. cfgs(ib)%mols(im)%atms(i)%is_charged ) &
            emfa = emfa + chgi*uel_slit(ib, az, cfgs(ib)%vec%latvector(3,3))

        if(nextpot > 0) then

            tmpext = 0.0_wp

            if( walls_soft > 0 ) then

                call ext_pot_energy(typi, ax, ay, (az - cfgs(ib)%vec%latvector(3,3)*slit_zfrac1), tmpext)

                eext = eext + tmpext
#ifdef MOLENG
                mext(imol) = mext(imol) + tmpext
#endif
                if( walls_soft == 2 ) then

                    call ext_pot_energy(typi, ax, ay, (cfgs(ib)%vec%latvector(3,3)*slit_zfrac2 - az), tmpext)

                    eext = eext + tmpext
#ifdef MOLENG
                    mext(imol) = mext(imol) + tmpext
#endif
                end if

            else

                call ext_pot_energy(typi, ax, ay, az, tmpext)

                eext = eext + tmpext
#ifdef MOLENG
                mext(imol) = mext(imol) + tmpext
#endif
            end if

        end if
            
        nlast = cfgs(ib)%mols(im)%nlist%numnbrs(i)

        do nbr = 1, nlast !cfgs(ib)%mols(im)%nlist%numnbrs(i)

            jm = cfgs(ib)%mols(im)%nlist%nbr_list(1, nbr, i)
            j = cfgs(ib)%mols(im)%nlist%nbr_list(2, nbr, i)

            if( cfgs(ib)%mols(im)%glob_no(i) == cfgs(ib)%mols(jm)%glob_no(j) ) cycle

            typj = cfgs(ib)%mols(jm)%atms(j)%atlabel
            jmol = cfgs(ib)%mols(jm)%mol_label

            bx = cfgs(ib)%mols(jm)%atms(j)%rpos(1)
            by = cfgs(ib)%mols(jm)%atms(j)%rpos(2)
            bz = cfgs(ib)%mols(jm)%atms(j)%rpos(3)

            xx = ax - bx
            yy = ay - by
            zz = az - bz

            call pbc_cart_pos_calc(ib, xx, yy, zz)

            rxy2 = xx * xx + yy * yy
            rsq  = rxy2 + zz * zz
            
            rvec = (/ xx, yy, zz /)

            if( rsq*onoff_rc < rcut2 ) then 

                r = sqrt(rsq)

                exclude = 0
                if( im == jm ) then
                
                    if( cfgs(ib)%mols(im)%blist%npairs > 0 ) then

                        exclude = cfgs(ib)%mols(im)%nlist%exclist(i,j)

                    else if( cfgs(ib)%mols(im)%exc_coul_ints ) then

                        exclude = MAXINT1

                    end if
                    
                end if

                if( ntpbond*exclude /= 0 .and. exclude < EXCDIH ) then

                    call bond_energy(exclude, r, rsq, sigmab, gammab)
                    epair = epair + sigmab
#ifdef MOLENG
                    mpair(imol) = mpair(imol) + sigmab
#endif
                    !virn = virn + gammab * rsq

                end if 

                sigmav = 0.0_wp
                gammav = 0.0_wp
                sigmab = 0.0_wp
                gammab = 0.0_wp

                !TU: Details for the exclusion/inclusion model:
                !TU: * exclude=0 means the pair are in different molecules, and hence VdW and Coulomb interactions 
                !TU:   are INCLUDED
                !TU: * exclude=1,2,3,.. (small integers) means the pair is bonded with bond type 'i', and they (VdW and
                !TU:   Coulomb interactions) are EXCLUDED
                !TU: * exclude=-1,-2,-3,.. (small integers) means the pair is bonded with bond type 'i', and the
                !TU:   interactions are INCLUDED
                !TU: * exclude=MAXINT1 means the pair is within the same molecule, is unbonded, not part of
                !TU:   an angle or dihedral, and the interactions are EXCLUDED
                !TU: * exclude=-MAXINT1 means the pair is within the same molecule, is unbonded, not part of an angle
                !TU:   or dihedral, and the interactions are INCLUDED
                !TU: * exclude=MAXINT3 means the pair is within the same molecule, unbonded, is part of an angle (1-3), 
                !TU:   is not part of a dihedral, and the interactions are EXCLUDED
                !TU: * exclude=-MAXINT3 means the pair is within the same molecule, unbonded, is part of an angle (1-3), 
                !TU:   is not part of a dihedral, and the interactions are INCLUDED
                !TU: * exclude between (inclusive) EXCDIH+1 and EXCDIH+ndih, where ndih is the number of types of 
                !TU:   dihedrals specified in FIELD, means the pair is within the same molecule, unbonded,
                !TU:   not part of an angle (1-3), and is a 1-4 in a dihedral (where EXCDIH+i means dihedral 
                !TU:   type i), and ther interactions are EXCLUDED
                !TU: * exclude between (inclusive) -(EXCDIH+1) and -(EXCDIH+ndih) means the pair is within the same
                !TU:   molecule, unbonded, not part of an angle (1-3), is a 1-4 in a dihedral, and the interactions
                !TU:   are INCLUDED or PARTIALLY INCLUDED (for VdW or electrostatic)
                !TU:
                
                !TU: exclude < 1 means we include the interaction (fully or partially for the case of 1-4 dihedral)
                !AB: if this pair is NOT EXCLUDED from all 'non-bonded' interactions (i.e. INCLUDED)
                !AB: include BOTH VDW AND COULOMB for both bonded (< 0) and non-bonded (== 0) pairs
                if( exclude < 1 ) then

                    if(ntpvdw > 0 ) then

                        spini = cfgs(ib)%mols(im)%atms(i)%spin
                        spinj = cfgs(ib)%mols(jm)%atms(j)%spin
                        
                        call vdw_energy (typi, typj ,r , rsq, vdwcut, eunit, sigmav, volinv, rvec, spini, spinj)

                        !TU: Check for partial VdW inclusion for 1-4 dihedral and
                        !TU: amend the VdW contribution accordingly
                        call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
                        if( dihtyp > 0 ) then
                            sigmav = sigmav * dihvdwinc
                        end if

                        if (imol == jmol) then
                        
                            if(im == jm) then
                                eselfvdw = eselfvdw + sigmav * 0.5
                            else
                                evdw = evdw + sigmav
                            end if
#ifdef MOLENG
                            mvdwb(imol) = mvdwb(imol) + sigmav
#endif
                        else
                            evdw = evdw + sigmav
#ifdef MOLENG
                            mvdwn(imol) = mvdwn(imol) + sigmav
                            mvdwn(jmol) = mvdwn(jmol) + sigmav
#endif
                        end if

                        if( is_serial .and. sigmav > vdwcap ) return
!                        if( sigmav > vdwcap ) then
!debugging!
!                            write(*,*)
!                            write(*,*)"field::molecule_energy2(..) VdW energy overflow : ", &
!                                  evdw,' + ',eselfvdw," >? ",vdwcap !,"; latest r = ",r
!                            write(*,*)"field::molecule_energy2(..) latest r = ",r," for atom-types: ", &
!                                  typi," & ",typj,", atoms: ",i," & ",j
!                            write(*,*)
!
!                            if( is_serial ) return
!                        end if

                        !virv = virv + gammav * rsq

                    end if

                    if( do_coul ) then

                        sigmac = 0.0_wp
                        gammac = 0.0_wp

                        !chgi = cfgs(ib)%mols(im)%atms(i)%charge
                        chgj = cfgs(ib)%mols(jm)%atms(j)%charge

                        if( coul_explicit ) then

                            if( rxy2*onoff_xy < rcut2 ) &
                                call explicitCoulomb(chgi, chgj, r , dielec, sigmac, gammac)

                        else if( coul_one ) then

                            call realspaceenergy(chgi, chgj, r , dielec, sigmac, gammac)

                        else if( coul_two ) then

                            call shiftrealspaceenergy(chgi, chgj, r, rsq, dielec, sigmac, gammac)

                        else if( coul_thr ) then

                            call shiftdamprealspaceenergy(chgi, chgj, r, shortrangecut, dielec, sigmac, gammac)

                        end if

                        !TU: Check for partial Coulomb inclusion for 1-4 dihedral and
                        !TU: amend the Coulomb contribution accordingly
                        call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
                        if( dihtyp > 0 ) then
                            sigmac = sigmac * dihcoulinc
                        end if

                        if (imol == jmol) then
                            if( im == jm ) sigmac = sigmac * 0.5_wp
#ifdef MOLENG
                            mrealbond(imol) = mrealbond(imol) + sigmac
                        else
                            mrealnonb(imol) = mrealnonb(imol) + sigmac
                            mrealnonb(jmol) = mrealnonb(jmol) + sigmac
#endif
                        end if
                        
                        ereal = ereal + sigmac

                        !virc = virc + gammac * rsq

                    end if

#ifdef MANY
                    if( exclude == 0 .and. &
                        cfgs(ib)%mols(im)%atms(i)%atype == ATM_TYPE_METAL .and. &
                        cfgs(ib)%mols(jm)%atms(j)%atype == ATM_TYPE_METAL ) then

                        call metal_energy(typi, typj, r, eng, vir)
                        evdw = evdw + eng

                        if (imol == jmol) then
                            mvdwb(imol) = mvdwb(imol) + eng
                        else
                            mvdwn(imol) = mvdwn(imol) + eng
                            mvdwn(jmol) = mvdwn(jmol) + eng
                        end if

                    end if
#endif

                end if

                !TU: exclude>=1 corresponds to excluded pairs; dihtyp>1 corresponds to partially or fully included
                !TU: 1-4 dihedral pairs. Either way we must correct for exclusions or partial exclusions
                call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
                if( (exclude >=1 .or. dihtyp>0) .and. do_corr ) then 

                    sigmac = 0.0_wp
                    gammac = 0.0_wp

                    !chgi = cfgs(ib)%mols(im)%atms(i)%charge
                    chgj = cfgs(ib)%mols(jm)%atms(j)%charge

                    call ewald_correct(chgi, chgj, r , rsq, cfgs(ib)%cl%eta, &
                                       dielec, sigmac, gammac)

                    !TU: For partial inclusion of 1-4 Coulomb we must partially EXCLUDE fraction (1-dihcoulinc) 
                    !TU: of the interaction
                    if( dihtyp>0 ) then

                        sigmac = sigmac * (1.0_wp - dihcoulinc)

                    end if

                    ecrct = ecrct + sigmac
#ifdef MOLENG
                    mcrct(imol) = mcrct(imol) - sigmac
#endif
                end if !( exclude < 1 )

            end if !( rsq*onoff_rc < rcut2 )

        end do

    end do

#ifdef MANY
    !metal potentials beyond pairs
    if(nmetpot > 0) then

        do i = start, last, step
        !do i = tb_start_atm, cfgs(ib)%mols(im)%natom, tb_step_atm
        !do i = 1, cfgs(ib)%mols(im)%natom

            ii = cfgs(ib)%mols(im)%glob_no(i)

            if (lmetab) then ! calculate embedding energy

                call eam_embed(cfgs(ib)%mols(im)%atms(i)%atlabel, rho(ii), eng)
                emany = emany - eng 

                !imol = cfgs(ib)%mols(im)%mol_label
                mmany(imol) = mmany(imol) - eng

            else if(rho(ii) > 0.0_wp) then

                eng = sqrt(rho(ii))
                emany = emany - eng !sqrt(rho(ii))

                !imol = cfgs(ib)%mols(im)%mol_label
                mmany(imol) = mmany(imol) - eng !sqrt(rho(ii))

            end if

        end do
        
        !if( is_parallel ) then
        if( step > 1 ) then 
            call gsum(emany)
            call gsum(mmany)
            call gsum(rho)
        end if

    end if

    !add in nonbonded three body
    if(numthb > 0) call thb_energy(ib, ethree, mthree, virt)
#endif

    !add in bonded three-body energy (angles)
    if(numang > 0) call mol_ang_energy(ib, im, eang, mang)

    !bonded dihedral energy
    if (numdih > 0) call total_dih_energy(ib, efour, mfour)

    !bonded inversion energy
    if (numinv > 0) call total_inv_energy(ib, efour, mfour)

    if( step > 1) then
    !if( is_parallel .and. stepa > 1) then
    
        call gsync

        !if( stepa > 1 ) then 
        !    call gsum(eselfcoul)
        !    call gsum(eext)
        !    call gsum(emfa)
        !end if

        buf(1) = evdw
        buf(2) = ereal
        buf(3) = epair
        buf(4) = ecrct
        buf(5) = eselfvdw
        buf(6) = eselfcoul
        buf(7) = eext
        buf(8) = emfa

        call gsum(buf)

        evdw  = buf(1)
        ereal = buf(2)
        epair = buf(3)
        ecrct = buf(4)
        eselfvdw  = buf(5)
        eselfcoul = buf(6)
        eext  = buf(7)
        emfa  = buf(8)

#ifdef MOLENG
        call gsum(mrealnonb)
        call gsum(mrealbond)
        call gsum(mvdwb)
        call gsum(mvdwn)
        call gsum(mcrct)
        call gsum(mpair)
        call gsum(mext)
#endif
    end if

    !multiply by 1/2 to account for double counting
    epair = epair * 0.5_wp
    ereal = ereal - ecrct * 0.5_wp
    
#ifdef MOLENG
    mrealnonb = mrealnonb * 0.5_wp
    mvdwn = mvdwn * 0.5_wp
    mcrct = mcrct * 0.5_wp
    mpair = mpair * 0.5_wp
    mang  = mang  * 0.5_wp
    mfour = mfour * 0.5_wp
#endif

end subroutine molecule_energy2


!TU: The below procedure is only to be used for 'rigid' molecule moves (without lists)
!calculate the energy of a given molecule - excluding reciprocal space ewald
!bonded interactions are not calculated
!does not use nbr lisr
subroutine molecule_energy_nolist(ib, im, coultyp, dielec, vdwcut, shortrangecut, &
                        ereal, evdw, ethree, emany, eext, emfa, vir, mrealnonb, mrealbond, mvdwn,  &
                        mvdwb, mthree, mfour, mmany, mext)

    use kinds_f90
    use control_type
    use constants_module, only : CTOINTERNAL, RCORE2, HALF, SLIT_EXT0, SLIT_EXT1, SLIT_EXT2, &
                                 ATM_TYPE_METAL
    use species_module, only : number_of_molecules
    use vdw_module
    use coul_module
    use thbpotential_module
    use cell_module
    use metpot_module, only : lmetab, metal_energy, eam_embed, nmetpot
    use tersoff_module
    use external_potential_module, only : ext_pot_energy, nextpot
    use comms_mpi_module, only : is_serial, is_parallel, gsync, gsum
    use parallel_loop_module, only : tb_start_mol, tb_step_mol, &
                                     tb_start_atm, tb_step_atm
                                     
    use slit_module, only : in_slit, walls_soft, onoff_rc, onoff_xy, &
                            coul_explicit, uel_slit, slit_zfrac1, slit_zfrac2

    implicit none

    integer, intent(in) :: ib, im, coultyp

    real(kind = wp), intent (in) :: dielec, vdwcut, shortrangecut

    real(kind = wp), intent (inout) :: ereal, evdw, ethree, emany, vir, eext, emfa
    real(kind = wp), intent (inout) :: mrealnonb(:), mrealbond(:), mvdwn(:), mvdwb(:), mthree(:), &
                                       mfour(:), mmany(:), mext(:)

    integer :: coultype
    integer :: typi, typj, i, ii, j, jm, imol, jmol, ircp, starta, stepa, startm, last, stepm, exclude
    real(kind = wp) :: volinv, virn, virc, virr, virv, virt, sigmac, sigmav, sigmab, gammac, gammav, gammab
    real(kind = wp) :: r, rsq, virter, tmpext, rxy2, rcut2
    real(kind = wp) :: ax, ay, az, bx, by, bz, rx, ry, rz
    real(kind = wp) :: chgi, chgj, xx, yy, zz, rxi, ryi, rzi, chge, trho
    real(kind = wp) :: eng, eters, buf(2)

    integer dm

    logical :: coul_in_slit, do_coul, do_corr, coul_one, coul_two, coul_thr

    !TU: Spins of a pair of atoms; required for orientation-dependent VdW potentials
    real(kind = wp) :: spini(3), spinj(3)
    !TU: Separation vector between atoms after accounting for PBCs
    real(kind = wp) :: rvec(3)
    
    if( coultyp == 0 .or. .not.cfgs(ib)%mols(im)%has_charge ) then
        coultype = 0
        coul_in_slit = .false.
        
        do_coul  = .false.
        do_corr  = .false.
        coul_one = .false.
        coul_two = .false.
        coul_thr = .false.
    else
        coultype = abs(coultyp)
        coul_in_slit = in_slit .and. coul_explicit
        
        do_coul  = .true.
        do_corr  = ( coultyp == 1 )
        coul_one = ( coultype < 3 )
        coul_two = ( coultype == 3 )
        coul_thr = ( coultype == 4 )
    end if

    !AB: square of cut-off radius
    rcut2 = shortrangecut*shortrangecut

    !TU: The following 2 lines pertain to AB's old implmentation of long-range VdW
    !TU: corrections, which is no longer in use
    !volinv= 1.0_wp
    !if( is_vdwlrc ) volinv= 1.0_wp/cfgs(ib)%vec%volume
    volinv = 0.0_wp

    !zero energies + virials
    eng = 0.0_wp
    ereal = 0.0_wp
    evdw = 0.0_wp
    ethree = 0.0_wp
    emany = 0.0_wp
    eext = 0.0_wp
    emfa = 0.0_wp

#ifdef MOLENG
    mrealnonb = 0.0_wp
    mrealbond = 0.0_wp
    mvdwn = 0.0_wp
    mvdwb = 0.0_wp
    mthree = 0.0_wp
    mfour = 0.0_wp
    mmany = 0.0_wp
    mext = 0.0_wp
#endif

    vir = 0.0_wp
    virn = 0.0_wp
    virv = 0.0_wp
    virc = 0.0_wp
    virr = 0.0_wp
    virt = 0.0_wp
    virter = 0.0_wp

    if(nmetpot > 0) then
        rho = 0.0_wp
    end if

    imol = cfgs(ib)%mols(im)%mol_label

    !set up parameters for parallel loop. Note this is for the inner loop over
    !the number of molecules - this was slightly faster in tests, howver, it probably will
    !depend on the system studied ie number of molecules and the number of atoms within each molecule
    starta = tb_start_atm
    stepa = tb_step_atm
    startm = tb_start_mol
    stepm = tb_step_mol
    last = cfgs(ib)%num_mols

    do i = starta, cfgs(ib)%mols(im)%natom, stepa

        typi = cfgs(ib)%mols(im)%atms(i)%atlabel
        chgi = cfgs(ib)%mols(im)%atms(i)%charge
          ii = cfgs(ib)%mols(im)%glob_no(i)

#ifdef MANY
        if(cfgs(ib)%mols(im)%atms(i)%atype == ATM_TYPE_METAL) then

            trho = 0.0_wp
            call metalatom_rho(ib, im, i, ii, shortrangecut, trho)
            rho(ii) = trho

        end if

        if(ntersoff > 0) then

            eters = 0.0_wp
            call atom_tersoff(ib, im, i, shortrangecut, eters, virter)
            eters = eters * 0.5_wp
            emany = emany + eters

        end if
#endif

        ax = cfgs(ib)%mols(im)%atms(i)%rpos(1)
        ay = cfgs(ib)%mols(im)%atms(i)%rpos(2)
        az = cfgs(ib)%mols(im)%atms(i)%rpos(3)

        !AB: Add extra interactions due to charged walls / MFA in the slit case
        if( coul_in_slit .and. cfgs(ib)%mols(im)%atms(i)%is_charged ) &
            emfa = emfa + cfgs(ib)%mols(im)%atms(i)%charge* &
                          uel_slit(ib, az, cfgs(ib)%vec%latvector(3,3))

        if(nextpot > 0) then

            tmpext = 0.0_wp

            if( walls_soft > 0 ) then

                call ext_pot_energy(typi, ax, ay, (az - cfgs(ib)%vec%latvector(3,3)*slit_zfrac1), tmpext)

                eext = eext + tmpext
#ifdef MOLENG
                mext(imol) = mext(imol) + tmpext
#endif
                if( walls_soft == 2 ) then

                    call ext_pot_energy(typi, ax, ay, (cfgs(ib)%vec%latvector(3,3)*slit_zfrac2 - az), tmpext)

                    eext = eext + tmpext
#ifdef MOLENG
                    mext(imol) = mext(imol) + tmpext
#endif
                end if

            else

                call ext_pot_energy(typi, ax, ay, az, tmpext)

                eext = eext + tmpext
#ifdef MOLENG
                mext(imol) = mext(imol) + tmpext
#endif
            end if

        end if

        do jm = startm, last, stepm
        
            if( im == jm ) cycle

            do j = 1, cfgs(ib)%mols(jm)%natom

                if( cfgs(ib)%mols(im)%glob_no(i) == cfgs(ib)%mols(jm)%glob_no(j) ) cycle

                typj = cfgs(ib)%mols(jm)%atms(j)%atlabel
                jmol = cfgs(ib)%mols(jm)%mol_label

                bx = cfgs(ib)%mols(jm)%atms(j)%rpos(1)
                by = cfgs(ib)%mols(jm)%atms(j)%rpos(2)
                bz = cfgs(ib)%mols(jm)%atms(j)%rpos(3)

                xx = ax - bx
                yy = ay - by
                zz = az - bz

                call pbc_cart_pos_calc(ib, xx, yy, zz)

                rxy2 = xx * xx + yy * yy
                rsq  = rxy2 + zz * zz

                rvec = (/ xx, yy, zz /)
                
                if( rsq*onoff_rc < rcut2 ) then 

                    r = sqrt(rsq)

                    exclude = 0

                    sigmav = 0.0_wp
                    gammav = 0.0_wp
                    sigmab = 0.0_wp
                    gammab = 0.0_wp

                    !if( exclude < 1 ) then

                        if( ntpvdw > 0 ) then

                            spini = cfgs(ib)%mols(im)%atms(i)%spin
                            spinj = cfgs(ib)%mols(jm)%atms(j)%spin
                            
                            call vdw_energy (typi, typj ,r , rsq, vdwcut, eunit, sigmav, volinv, rvec, spini, spinj)

#ifdef MOLENG
                            if( imol == jmol ) then
                                mvdwb(imol) = mvdwb(imol) + sigmav
                            else
                                mvdwn(imol) = mvdwn(imol) + sigmav
                                mvdwn(jmol) = mvdwn(jmol) + sigmav
                            end if
#endif
                            evdw = evdw + sigmav

                            if( is_serial .and. sigmav > vdwcap ) return
!                            if( sigmav > vdwcap ) then
!debugging!
!                                write(*,*)
!                                write(*,*)"field::molecule_energy_nolist(..) VdW energy overflow : ", &
!                                          evdw," >? ",vdwcap !,"; latest r = ",r
!                                write(*,*)"field::molecule_energy_nolist(..) latest r = ", &
!                                          r," for atom-types: ",typi," & ",typj,", atoms: ",i," & ",j
!                                write(*,*)
!
!                                if( is_serial ) return
!
!                            end if

                            !virv = virv + gammav * rsq

                        end if

                        if( do_coul ) then

                            sigmac = 0.0_wp
                            gammac = 0.0_wp

                            !chgi = cfgs(ib)%mols(im)%atms(i)%charge
                            chgj = cfgs(ib)%mols(jm)%atms(j)%charge

                            if( coul_explicit ) then

                                if( rxy2*onoff_xy < rcut2 ) &
                                    call explicitCoulomb(chgi, chgj, r , dielec, sigmac, gammac)

                            else if( coul_one ) then

                                call realspaceenergy(chgi, chgj, r , dielec, sigmac, gammac)

                            else if( coul_two ) then

                                call shiftrealspaceenergy(chgi, chgj, r, rsq, dielec, sigmac, gammac)

                            else if( coul_thr ) then

                                call shiftdamprealspaceenergy(chgi, chgj, r, shortrangecut, dielec, sigmac, gammac)

                            end if

                            ereal = ereal + sigmac

#ifdef MOLENG
                            if (imol == jmol) then
                                mrealbond(imol) = mrealbond(imol) + sigmac 
                            else
                                mrealnonb(imol) = mrealnonb(imol) + sigmac
                                mrealnonb(jmol) = mrealnonb(jmol) + sigmac
                            end if
#endif
                            !virc = virc + gammac * rsq

                        end if
#ifdef MANY

                        if( exclude == 0 .and. &
                            cfgs(ib)%mols(im)%atms(i)%atype == ATM_TYPE_METAL .and. &
                            cfgs(ib)%mols(jm)%atms(j)%atype == ATM_TYPE_METAL ) then

                            call metal_energy(typi, typj, r, eng, vir)
                            evdw = evdw + eng

                            if (imol == jmol) then
                                mvdwb(imol) = mvdwb(imol) + eng
                            else
                                mvdwn(imol) = mvdwn(imol) + eng
                                mvdwn(jmol) = mvdwn(jmol) + eng
                            end if

                        end if
#endif

                    !end if !( exclude < 1 )

                end if !( rsq*onoff_rc < rcut2 )

            end do

        end do

    end do

#ifdef MANY
    !metal potentials beyond pairs
    if(nmetpot > 0) then

        do i = tb_start_atm, cfgs(ib)%mols(im)%natom, tb_step_atm
        !do i = 1, cfgs(ib)%mols(im)%natom

            ii = cfgs(ib)%mols(im)%glob_no(i)

            if (lmetab) then ! calculate embedding energy

                call eam_embed(cfgs(ib)%mols(im)%atms(i)%atlabel, rho(ii), eng)
                emany = emany - eng 

                !imol = cfgs(ib)%mols(im)%mol_label
                mmany(imol) = mmany(imol) - eng 

            else if(rho(ii) > 0.0_wp) then

                eng = sqrt(rho(ii))
                emany = emany - eng !sqrt(rho(ii))

                !imol = cfgs(ib)%mols(im)%mol_label
                mmany(imol) = mmany(imol) - eng !sqrt(rho(ii))

            end if

        end do
        
        !if( is_parallel ) then
        if( stepa > 1 ) then 
            call gsum(emany)
            call gsum(mmany)
            call gsum(rho)
        end if

    end if

    !add in nonbonded three body
    if(numthb > 0) call thb_energy(ib, ethree, mthree, virt)
#endif

    if( is_parallel ) then
    
        call gsync
        
        if( stepa > 1 ) then
        
            buf(1) = eext
            buf(2) = emfa

            call gsum(buf)

            eext = buf(1)
            emfa = buf(2)
            
            call gsum(mext)
            
        end if

        buf(1) = evdw
        buf(2) = ereal

        call gsum(buf)

        evdw  = buf(1)
        ereal = buf(2)

#ifdef MOLENG
        call gsum(mrealnonb)
        call gsum(mrealbond)
        call gsum(mvdwb)
        call gsum(mvdwn)
#endif
    end if

#ifdef MOLENG
    mrealnonb = mrealnonb * 0.5_wp
    mvdwn = mvdwn * 0.5_wp
    !mfour = mfour * 0.5_wp
#endif

end subroutine molecule_energy_nolist


!calculate the energy of a given molecule - excluding reciprocal space ewald
!the self energy and bonded energy is also calculated
!does not use nbr list
subroutine molecule_energy2_nolist(ib, im, coultyp, dielec, vdwcut, shortrangecut, &
                        ereal, eselfcoul, evdw, eselfvdw, ethree, epair, eang, efour, emany, eext, emfa, &
                        vir, mrealnonb, mrealbond, mself, mcrct, mvdwn, mvdwb, mpair, mthree, mang, &
                        mfour, mmany, mext)

    use kinds_f90
    use control_type
    use constants_module, only : CTOINTERNAL, RCORE2, HALF, SLIT_EXT0, SLIT_EXT1, SLIT_EXT2, &
                                 ATM_TYPE_METAL
    use species_module, only : number_of_molecules
    use vdw_module
    use bond_module, only : ntpbond, bond_energy
    use angle_module, only : numang
    use dihedral_module, only : numdih
    use inversion_module, only : numinv
    use coul_module
    use thbpotential_module
    use cell_module
    use metpot_module, only : lmetab, metal_energy, eam_embed, nmetpot
    use tersoff_module
    use external_potential_module, only : ext_pot_energy, nextpot
    use comms_mpi_module, only : is_serial, is_parallel, gsync, gsum
    use parallel_loop_module, only : tb_start_mol, tb_step_mol, &
                                     tb_start_atm, tb_step_atm
    use slit_module, only : in_slit, walls_soft, onoff_rc, onoff_xy, &
                            coul_explicit, uel_slit, slit_zfrac1, slit_zfrac2

    implicit none

    integer, intent(in) :: ib, im, coultyp

    real(kind = wp), intent (in) :: dielec, vdwcut, shortrangecut

    real(kind = wp), intent (inout) :: ereal, evdw, ethree, epair, eang, efour, &
                                       emany, vir, eselfcoul, eselfvdw, eext, emfa
    real(kind = wp), intent (inout) :: mrealnonb(:), mrealbond(:), mvdwn(:), mvdwb(:), mpair(:), &
                                       mthree(:), mang(:), mfour(:), mmany(:), mself(:), mcrct(:), mext(:)

    integer :: coultype
    integer :: typi, typj, i, ii, j, jm, jj, exclude, imol, jmol, ircp, starta, stepa, startm, stepm, last
    real(kind = wp) :: volinv, virn, virc, virr, virv, virt, sigmac, sigmav, sigmab, gammac, gammav, gammab
    real(kind = wp) :: r, rsq, virter, tmpext, rxy2, rcut2
    real(kind = wp) :: ax, ay, az, bx, by, bz, rx, ry, rz
    real(kind = wp) :: chgi, chgj, xx, yy, zz, rxi, ryi, rzi, chge, trho
    real(kind = wp) :: eng, eters, ecrct, buf(5)
    integer :: dihtyp
    real(kind = wp) :: dihvdwinc, dihcoulinc

    integer dm

    logical :: coul_in_slit, do_coul, do_corr, coul_one, coul_two, coul_thr

    !TU: Spins of a pair of atoms; required for orientation-dependent VdW potentials
    real(kind = wp) :: spini(3), spinj(3)
    !TU: Separation vector between atoms after accounting for PBCs
    real(kind = wp) :: rvec(3)
    
    if( coultyp == 0 .or. .not.cfgs(ib)%mols(im)%has_charge ) then
        coultype = 0
        coul_in_slit = .false.
        
        do_coul  = .false.
        do_corr  = .false.
        coul_one = .false.
        coul_two = .false.
        coul_thr = .false.
    else
        coultype = abs(coultyp)
        coul_in_slit = in_slit .and. coul_explicit
        
        do_coul  = .true.
        do_corr  = ( coultyp == 1 )
        coul_one = ( coultype < 3 )
        coul_two = ( coultype == 3 )
        coul_thr = ( coultype == 4 )
    end if

    !AB: square of cut-off radius
    rcut2 = shortrangecut*shortrangecut

    !TU: The following 2 lines pertain to AB's old implmentation of long-range VdW
    !TU: corrections, which is no longer in use
    !volinv= 1.0_wp
    !if( is_vdwlrc ) volinv= 1.0_wp/cfgs(ib)%vec%volume
    volinv = 0.0_wp

    !AB: temporary
    eng = 0.0_wp
    eselfcoul = 0.0_wp
    eselfvdw = 0.0_wp
    ecrct = 0.0_wp

    !AB: actual I/O (ref)
    !ereal, evdw, ethree, epair, eang, efour, emany, eext, emfa,
    ereal = 0.0_wp
    evdw = 0.0_wp
    ethree = 0.0_wp
    epair = 0.0_wp
    eang = 0.0_wp
    efour = 0.0_wp
    emany = 0.0_wp
    eext = 0.0_wp
    emfa = 0.0_wp

#ifdef MOLENG
    mrealnonb = 0.0_wp
    mrealbond = 0.0_wp
    mvdwn = 0.0_wp
    mvdwb = 0.0_wp
    mpair = 0.0_wp
    mthree = 0.0_wp
    mang = 0.0_wp
    mfour = 0.0_wp
    mmany = 0.0_wp
    mself = 0.0_wp
    mcrct = 0.0_wp
    mext = 0.0_wp
#endif

    vir = 0.0_wp
    virn = 0.0_wp
    virv = 0.0_wp
    virc = 0.0_wp
    virr = 0.0_wp
    virt = 0.0_wp
    virter = 0.0_wp

    if(nmetpot > 0) then
        rho = 0.0_wp
    end if

    imol = cfgs(ib)%mols(im)%mol_label

    !set up parameters for parallel loop. Note this is for the inner loop over
    !the number of molecules - this was slightly faster in tests, howver, it probably will
    !depend on the system studied ie number of molecules and the number of atoms within each molecule
    starta = tb_start_atm
    stepa = tb_step_atm
    startm = tb_start_mol
    stepm = tb_step_mol
    last = cfgs(ib)%num_mols

    do i = starta, cfgs(ib)%mols(im)%natom, stepa

        typi = cfgs(ib)%mols(im)%atms(i)%atlabel
        chgi = cfgs(ib)%mols(im)%atms(i)%charge
          ii = cfgs(ib)%mols(im)%glob_no(i)

#ifdef MANY
        if(cfgs(ib)%mols(im)%atms(i)%atype == ATM_TYPE_METAL) then

            trho = 0.0_wp
            call metalatom_rho(ib, im, i, ii, shortrangecut, trho)
            rho(ii) = trho

        end if

        if(ntersoff > 0) then

            eters = 0.0_wp
            call atom_tersoff(ib, im, i, shortrangecut, eters, virter)
            eters = eters * 0.5_wp
            emany = emany + eters

        end if
#endif

        if( do_corr ) then 

            call atomself_energy(chgi, cfgs(ib)%cl%eta, dielec, eng)

            eselfcoul = eselfcoul + eng
#ifdef MOLENG
            mself(imol) = mself(imol) + eng
#endif
        end if

        ax = cfgs(ib)%mols(im)%atms(i)%rpos(1)
        ay = cfgs(ib)%mols(im)%atms(i)%rpos(2)
        az = cfgs(ib)%mols(im)%atms(i)%rpos(3)

        !AB: Add extra interactions due to charged walls / MFA in the slit case
        if( coul_in_slit .and. cfgs(ib)%mols(im)%atms(i)%is_charged ) &
            emfa = emfa + chgi*uel_slit(ib, az, cfgs(ib)%vec%latvector(3,3))

        if(nextpot > 0) then

            tmpext = 0.0_wp

            if( walls_soft > 0 ) then

                call ext_pot_energy(typi, ax, ay, (az - cfgs(ib)%vec%latvector(3,3)*slit_zfrac1), tmpext)

                eext = eext + tmpext
#ifdef MOLENG
                mext(imol) = mext(imol) + tmpext
#endif
                if( walls_soft == 2 ) then

                    call ext_pot_energy(typi, ax, ay, (cfgs(ib)%vec%latvector(3,3)*slit_zfrac2 - az), tmpext)

                    eext = eext + tmpext
#ifdef MOLENG
                    mext(imol) = mext(imol) + tmpext
#endif
                end if

            else

                call ext_pot_energy(typi, ax, ay, az, tmpext)

#ifdef MOLENG
                eext = eext + tmpext
                mext(imol) = mext(imol) + tmpext
#endif
            end if

        end if

        do jm = startm, cfgs(ib)%num_mols, stepm

            do j = 1, cfgs(ib)%mols(jm)%natom
                
                if( cfgs(ib)%mols(im)%glob_no(i) == cfgs(ib)%mols(jm)%glob_no(j) ) cycle

                typj = cfgs(ib)%mols(jm)%atms(j)%atlabel
                jmol = cfgs(ib)%mols(jm)%mol_label

                bx = cfgs(ib)%mols(jm)%atms(j)%rpos(1)
                by = cfgs(ib)%mols(jm)%atms(j)%rpos(2)
                bz = cfgs(ib)%mols(jm)%atms(j)%rpos(3)

                xx = ax - bx
                yy = ay - by
                zz = az - bz

                call pbc_cart_pos_calc(ib, xx, yy, zz)

                rxy2 = xx * xx + yy * yy
                rsq  = rxy2 + zz * zz

                rvec = (/ xx, yy, zz /)
                
                if ( rsq*onoff_rc < rcut2 ) then 

                    r = sqrt(rsq)

                    exclude = 0
                    if( im == jm ) then
                        if( cfgs(ib)%mols(im)%blist%npairs > 0 ) then

                            exclude = cfgs(ib)%mols(im)%nlist%exclist(i,j)

                        else if( cfgs(ib)%mols(im)%exc_coul_ints ) then

                            exclude = MAXINT1

                        end if
                    end if

                    if( ntpbond*exclude /= 0 .and. exclude < EXCDIH ) then

                        call bond_energy(exclude, r, rsq, sigmab, gammab)
                        epair = epair + sigmab
#ifdef MOLENG
                        mpair(imol) = mpair(imol) + sigmab
#endif

                        !virn = virn + gammab * rsq

                    end if 

                    sigmav = 0.0_wp
                    gammav = 0.0_wp
                    sigmab = 0.0_wp
                    gammab = 0.0_wp

                    !TU: Details for the exclusion/inclusion model:
                    !TU: * exclude=0 means the pair are in different molecules, and hence VdW and Coulomb interactions 
                    !TU:   are INCLUDED
                    !TU: * exclude=1,2,3,.. (small integers) means the pair is bonded with bond type 'i', and they (VdW and
                    !TU:   Coulomb interactions) are EXCLUDED
                    !TU: * exclude=-1,-2,-3,.. (small integers) means the pair is bonded with bond type 'i', and the
                    !TU:   interactions are INCLUDED
                    !TU: * exclude=MAXINT1 means the pair is within the same molecule, is unbonded, not part of
                    !TU:   an angle or dihedral, and the interactions are EXCLUDED
                    !TU: * exclude=-MAXINT1 means the pair is within the same molecule, is unbonded, not part of an angle
                    !TU:   or dihedral, and the interactions are INCLUDED
                    !TU: * exclude=MAXINT3 means the pair is within the same molecule, unbonded, is part of an angle (1-3), 
                    !TU:   is not part of a dihedral, and the interactions are EXCLUDED
                    !TU: * exclude=-MAXINT3 means the pair is within the same molecule, unbonded, is part of an angle (1-3), 
                    !TU:   is not part of a dihedral, and the interactions are INCLUDED
                    !TU: * exclude between (inclusive) EXCDIH+1 and EXCDIH+ndih, where ndih is the number of types of 
                    !TU:   dihedrals specified in FIELD, means the pair is within the same molecule, unbonded,
                    !TU:   not part of an angle (1-3), and is a 1-4 in a dihedral (where EXCDIH+i means dihedral 
                    !TU:   type i), and ther interactions are EXCLUDED
                    !TU: * exclude between (inclusive) -(EXCDIH+1) and -(EXCDIH+ndih) means the pair is within the same
                    !TU:   molecule, unbonded, not part of an angle (1-3), is a 1-4 in a dihedral, and the interactions
                    !TU:   are INCLUDED or PARTIALLY INCLUDED (for VdW or electrostatic)
                    !TU:

                    !AB: if this pair is NOT EXCLUDED from all 'non-bonded' interactions (i.e. INCLUDED)
                    !AB: include BOTH VDW AND COULOMB for both bonded (< 0) and non-bonded (== 0) pairs
                    if( exclude < 1 ) then

                        if( ntpvdw > 0 ) then

                            spini = cfgs(ib)%mols(im)%atms(i)%spin
                            spinj = cfgs(ib)%mols(jm)%atms(j)%spin
                            
                            call vdw_energy(typi, typj ,r , rsq, vdwcut, eunit, sigmav, volinv, rvec, spini, spinj)

                            !TU: Check for partial VdW inclusion for 1-4 dihedral and
                            !TU: amend the VdW contribution accordingly
                            call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
                            if( dihtyp > 0 ) then

                                sigmav = sigmav * dihvdwinc

                            end if


                            if (imol == jmol) then
                                if(im == jm) then
                                    eselfvdw = eselfvdw + sigmav * 0.5_wp
                                else
                                    evdw = evdw + sigmav
                                end if
#ifdef MOLENG
                                mvdwb(imol) = mvdwb(imol) + sigmav
                            else
                                evdw = evdw + sigmav
                                mvdwn(imol) = mvdwn(imol) + sigmav
                                mvdwn(jmol) = mvdwn(jmol) + sigmav
#endif
                            end if

                            if( is_serial .and. sigmav > vdwcap ) return
!                            if( sigmav > vdwcap ) then
!debugging!
!                                write(*,*)
!                                write(*,*)"field::molecule_energy2_nolist(..) VdW energy overflow : ", &
!                                      evdw,' + ',eselfvdw," >? ",vdwcap !,"; latest r = ",r
!                                write(*,*)"field::molecule_energy2_nolist(..) latest r = ", &
!                                      r," for atom-types: ",typi," & ",typj,", atoms: ",i," & ",j
!                                write(*,*)
!
!                                if( is_serial ) return
!
!                            end if

                            !virv = virv + gammav * rsq

                        end if

                        if( do_coul ) then

                            sigmac = 0.0_wp
                            gammac = 0.0_wp

                            !chgi = cfgs(ib)%mols(im)%atms(i)%charge
                            chgj = cfgs(ib)%mols(jm)%atms(j)%charge

                            if( coul_explicit ) then

                                if( rxy2*onoff_xy < rcut2 ) &
                                    call explicitCoulomb(chgi, chgj, r , dielec, sigmac, gammac)

                            else if( coul_one ) then

                                call realspaceenergy(chgi, chgj, r , dielec, sigmac, gammac)

                            else if( coul_two ) then

                                call shiftrealspaceenergy(chgi, chgj, r, rsq, dielec, sigmac, gammac)

                            else if( coul_thr ) then

                                call shiftdamprealspaceenergy(chgi, chgj, r, shortrangecut, dielec, sigmac, gammac)

                            end if

                            !TU: Check for partial Coulomb inclusion for 1-4 dihedral and
                            !TU: amend the Coulomb contribution accordingly
                            call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
                            if( dihtyp > 0 ) then
                                sigmac = sigmac * dihcoulinc
                            end if

                            if (imol == jmol) then
                                if( im == jm ) sigmac = sigmac * 0.5_wp
#ifdef MOLENG
                                mrealbond(imol) = mrealbond(imol) + sigmac
                            else
                                mrealnonb(imol) = mrealnonb(imol) + sigmac
                                mrealnonb(jmol) = mrealnonb(jmol) + sigmac
#endif
                            end if
                            
                            ereal = ereal + sigmac

                            !virc = virc + gammac * rsq

                        end if

#ifdef MANY
                        if( exclude == 0 .and. &
                            cfgs(ib)%mols(im)%atms(i)%atype == ATM_TYPE_METAL .and. &
                            cfgs(ib)%mols(jm)%atms(j)%atype == ATM_TYPE_METAL ) then

                            call metal_energy(typi, typj, r, eng, vir)
                            evdw = evdw + eng

                            if (imol == jmol) then
                                mvdwb(imol) = mvdwb(imol) + eng
                            else
                                mvdwn(imol) = mvdwn(imol) + eng
                                mvdwn(jmol) = mvdwn(jmol) + eng
                            end if

                        end if
#endif

                    end if

                    !TU: exclude>=1 corresponds to excluded pairs; dihtyp>1 corresponds to partially or fully included
                    !TU: 1-4 dihedral pairs. Either way we must correct for exclusions or partial exclusions
                    call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
                    if( (exclude >=1 .or. dihtyp>0) .and. do_corr ) then 

                        sigmac = 0.0_wp
                        gammac = 0.0_wp

                        !chgi = cfgs(ib)%mols(im)%atms(i)%charge
                        chgj = cfgs(ib)%mols(jm)%atms(j)%charge

                        call ewald_correct(chgi, chgj, r , rsq, cfgs(ib)%cl%eta, &
                                           dielec, sigmac, gammac)

                        !TU: For partial inclusion of 1-4 Coulomb we must partially EXCLUDE fraction (1-dihcoulinc) 
                        !TU: of the interaction
                        if( dihtyp>0 ) then

                            sigmac = sigmac * (1.0_wp - dihcoulinc)

                        end if

                        ecrct = ecrct + sigmac
#ifdef MOLENG
                        mcrct(imol) = mcrct(imol) - sigmac
#endif

                    end if

                end if

            end do

        end do

    end do

#ifdef MANY
    !metal potentials beyond pairs
    if(nmetpot > 0) then
        
        do i = tb_start_atm, cfgs(ib)%mols(im)%natom, tb_step_atm
        !do i = 1, cfgs(ib)%mols(im)%natom
        
            ii = cfgs(ib)%mols(im)%glob_no(i)

            if (lmetab) then ! calculate embedding energy

                call eam_embed(cfgs(ib)%mols(im)%atms(i)%atlabel, rho(ii), eng)
                emany = emany - eng 
                    
                !imol = cfgs(ib)%mols(im)%mol_label
                mmany(imol) = mmany(imol) - eng

            else if(rho(ii) > 0.0_wp) then

                eng = sqrt(rho(ii))
                emany = emany - eng !sqrt(rho(ii))

                !imol = cfgs(ib)%mols(im)%mol_label
                mmany(imol) = mmany(imol) - eng !sqrt(rho(ii))
        
            end if

        end do
        
        !if( is_parallel ) then
        if( stepa > 1 ) then 
            call gsum(emany)
            call gsum(mmany)
            call gsum(rho)
        end if

    end if

    !add in nonbonded three body
    if(numthb > 0) call thb_energy(ib, ethree, mthree, virt)
#endif

    !add in bonded three-body energy (angles)
    if(numang > 0) call mol_ang_energy(ib, im, eang, mang)

    !bonded dihedral energy
    if (numdih > 0) call total_dih_energy(ib, efour, mfour)

    !bonded inversion energy
    if (numinv > 0) call total_inv_energy(ib, efour, mfour)

    if( is_parallel ) then
    
        call gsync

        if( stepa > 1 ) then 
        
            call gsum(eselfcoul)
            call gsum(eext)
            call gsum(emfa)
            call gsum(mext)
        
        end if

        buf(1) = evdw
        buf(2) = ereal
        buf(3) = epair
        buf(4) = ecrct
        buf(5) = eselfvdw

        call gsum(buf)

        evdw  = buf(1)
        ereal = buf(2)
        epair = buf(3)
        ecrct = buf(4)
        eselfvdw = buf(5)

#ifdef MOLENG
        call gsum(mrealnonb)
        call gsum(mrealbond)
        call gsum(mvdwb)
        call gsum(mvdwn)
        call gsum(mcrct)
        call gsum(mpair)
#endif

    end if

    !multiply by 1/2 to account for double counting
    epair = epair * 0.5_wp
    ereal = ereal - ecrct * 0.5_wp

#ifdef MOLENG
    mrealnonb = mrealnonb * 0.5_wp
    mvdwn = mvdwn * 0.5_wp
    mcrct = mcrct * 0.5_wp
    mpair = mpair * 0.5_wp
    mang  = mang  * 0.5_wp
    mfour = mfour * 0.5_wp
#endif

end subroutine molecule_energy2_nolist


!TU: Note that the VdW energy returned by this procedure includes the long-range VdW correction
!> calculates the total energy of the cell **
subroutine total_energy_nolist(ib, coultyp, vdwcut, shortrangecut, dielec, &
                        ereal, evdw, ethree, epair, eang, efour, emany, eext, emfa, &
                        vir, mrealnonb, mrealbond, mself, mcrct, mvdwn, mvdwb, mpair, mthree, mang, &
                        mfour, mmany, mext)

    use kinds_f90
    use constants_module, only : CTOINTERNAL, RCORE2, uout, HALF, SLIT_EXT0, SLIT_EXT1, SLIT_EXT2, &
                                 ATM_TYPE_METAL
    use species_module, only : number_of_molecules
    use vdw_module
    use bond_module, only : ntpbond, bond_energy
    use cell_module
    use thbpotential_module, only : numthb
    use metpot_module, only : lmetab, metal_energy, eam_embed, nmetpot
    use tersoff_module
    use coul_module, only : explicitCoulomb, realspaceenergy, atomself_energy, &
                            shiftrealspaceenergy, shiftdamprealspaceenergy, ewald_correct
    use angle_module, only : numang
    use inversion_module, only : numinv
    use dihedral_module, only : numdih
    use external_potential_module, only : ext_pot_energy, nextpot
    use comms_mpi_module, only : is_serial, is_parallel, gsync, gsum, idnode
    use parallel_loop_module, only : tb_start_mol, tb_step_mol, tb_start_atm, tb_step_atm
    use slit_module, only : in_slit, walls_soft, onoff_rc, onoff_xy, coul_explicit, &
                            uel_slit, slit_zfrac1, slit_zfrac2

    implicit none

    integer, intent(in)             :: ib, coultyp
    real(kind = wp), intent (in)    :: vdwcut, shortrangecut, dielec
    real(kind = wp), intent (inout) :: ereal, evdw, ethree, epair, eang, efour, emany, eext, emfa, vir
    real(kind = wp), intent (inout) :: mrealnonb(:), mrealbond(:), mvdwn(:), mvdwb(:), mpair(:), &
                                       mthree(:), mang(:), mfour(:), mmany(:), mcrct(:), mself(:), mext(:)

    integer :: coultype
    integer :: typi, typj, im, i, ii, j, jm, nbr, imol, jmol, last, startm, stepm, starta, stepa, exclude
    real(kind = wp) :: volinv, virn, virc, virr, virv, virt, sigmac, sigmav, sigmab, gammac, gammav, gammab
    real(kind = wp) :: r, rsq, virter, virp, tmpext, rxy2, rcut2
    real(kind = wp) :: chgi, chgj, xx, yy, zz, rxi, ryi, rzi, chg, eself, ecrct, trho
    real(kind = wp) :: ax, ay, az, bx, by, bz, rx, ry, rz
    real(kind = wp) :: eng, eters, buf(7)
    integer :: dihtyp
    real(kind = wp) :: dihvdwinc, dihcoulinc

    !TU: Spins of a pair of atoms; required for orientation-dependent VdW potentials
    real(kind = wp) :: spini(3), spinj(3)
    !TU: Separation vector between atoms after accounting for PBCs
    real(kind = wp) :: rvec(3)
    
    real(kind = wp) :: tmpdih, tmpinv, tmpang, tmpreal, tmpvdw
    !integer :: ncpairs
    !ncpairs = 0

    logical :: coul_in_slit, do_coul, do_corr, coul_one, coul_two, coul_thr

    integer, save :: ncall = 0

    ncall = ncall+1

    if( coultyp == 0 ) then
        coultype = 0
        coul_in_slit = .false.
        
        do_coul  = .false.
        do_corr  = .false.
        coul_one = .false.
        coul_two = .false.
        coul_thr = .false.
    else
        coultype = abs(coultyp)
        coul_in_slit = in_slit .and. coul_explicit
        
        do_coul  = .true.
        do_corr  = ( coultyp == 1 )
        coul_one = ( coultype < 3 )
        coul_two = ( coultype == 3 )
        coul_thr = ( coultype == 4 )
    end if

    !AB: square of cut-off radius
    rcut2 = shortrangecut*shortrangecut


    !TU: The following 2 lines pertain to AB's old implmentation of long-range VdW
    !TU: corrections, which is no longer in use
    !volinv= 1.0_wp
    !if( is_vdwlrc ) volinv= 1.0_wp/cfgs(ib)%vec%volume
    volinv= 1.0_wp/cfgs(ib)%vec%volume

    !zero energies + virials
    eng    = 0.0_wp
    eself  = 0.0_wp
    ereal  = 0.0_wp
    evdw   = 0.0_wp
    ethree = 0.0_wp
    epair  = 0.0_wp
    eang   = 0.0_wp
    emany  = 0.0_wp
    ecrct  = 0.0_wp
    efour  = 0.0_wp
    eext   = 0.0_wp
    emfa   = 0.0_wp

    tmpinv  = 0.0_wp
    tmpreal = 0.0_wp
    tmpdih  = 0.0_wp
    tmpvdw  = 0.0_wp
    tmpang  = 0.0_wp

#ifdef MOLENG
    mrealnonb = 0.0_wp
    mrealbond = 0.0_wp
    mvdwn  = 0.0_wp
    mvdwb  = 0.0_wp
    mpair  = 0.0_wp
    mthree = 0.0_wp
    mang   = 0.0_wp
    mfour  = 0.0_wp
    mmany  = 0.0_wp
    mself  = 0.0_wp
    mcrct  = 0.0_wp
    mext   = 0.0_wp
#endif

    vir    = 0.0_wp
    virn   = 0.0_wp
    virp   = 0.0_wp
    virv   = 0.0_wp
    virc   = 0.0_wp
    virr   = 0.0_wp
    virt   = 0.0_wp
    virter = 0.0_wp

    if(nmetpot > 0) rho = 0.0_wp

    !TU: Long-range correction
    evdw = vdw_lrc_energy( volinv, cfgs(ib)%nums_elemts )

    !!$OMP PARALLEL DEFAULT(none) &
    !!$OMP SHARED(ib, cfgs, ntpvdw, ntpbond, startm, starta, last, stepm, stepa, shortrangecut, coultype, dielec) &
    !!$OMP SHARED(mrealbond, mrealnonb, vdwcut, mvdwb, mvdwn, mpair) &
    !!$OMP PRIVATE(i, im, j, jm, imol, jmol, typi, typj, nbr, ax, ay, az, bx, by, bz, xx, yy, zz, rx, ry, rz, rsq, r) &
    !!$OMP PRIVATE(sigmac, gammac, sigmav, gammav, chgi, chgj, sigmab, gammab, exclude) &
    !!$OMP REDUCTION(+: evdw, ereal, ecrct, epair, mcrct, virv, virc, volinv, virn, vir)

    last = cfgs(ib)%num_mols
    startm = tb_start_mol
    stepm = tb_step_mol
    starta = tb_start_atm
    stepa = tb_step_atm

    do im = startm, last, stepm

        do i = starta, cfgs(ib)%mols(im)%natom, stepa

            imol = cfgs(ib)%mols(im)%mol_label
            typi = cfgs(ib)%mols(im)%atms(i)%atlabel
            chgi = cfgs(ib)%mols(im)%atms(i)%charge
              ii = cfgs(ib)%mols(im)%glob_no(i)

#ifdef MANY
            if(cfgs(ib)%mols(im)%atms(i)%atype == ATM_TYPE_METAL) then

                trho = 0.0_wp
                call metalatom_rho(ib, im, i, ii, shortrangecut, trho)
                rho(ii) = trho

            end if

            if(ntersoff > 0) then

                eters = 0.0_wp
                call atom_tersoff(ib, im, i, shortrangecut, eters, virter)
                eters = eters * 0.5_wp
                emany = emany + eters
                mmany(imol) = mmany(imol) + eters

            end if
#endif

            if( do_corr ) then 

                call atomself_energy(chgi, cfgs(ib)%cl%eta, dielec, eng)

                eself = eself + eng
#ifdef MOLENG
                mself(imol) = mself(imol) + eng
#endif

            end if

            ax = cfgs(ib)%mols(im)%atms(i)%rpos(1)
            ay = cfgs(ib)%mols(im)%atms(i)%rpos(2)
            az = cfgs(ib)%mols(im)%atms(i)%rpos(3)

            !AB: Add extra interactions due to charged walls / MFA in the slit case
            if( coul_in_slit .and. cfgs(ib)%mols(im)%atms(i)%is_charged ) &
                emfa = emfa + chgi*uel_slit(ib, az, cfgs(ib)%vec%latvector(3,3))

            if(nextpot > 0) then

                tmpext = 0.0_wp

                if( walls_soft > 0 ) then

                    call ext_pot_energy(typi, ax, ay, (az - cfgs(ib)%vec%latvector(3,3)*slit_zfrac1), tmpext)

                    eext = eext + tmpext
#ifdef MOLENG
                    mext(imol) = mext(imol) + tmpext
#endif

                    if( walls_soft == 2 ) then

                        call ext_pot_energy(typi, ax, ay, (cfgs(ib)%vec%latvector(3,3)*slit_zfrac2 - az), tmpext)

                        eext = eext + tmpext
#ifdef MOLENG
                        mext(imol) = mext(imol) + tmpext
#endif
                    end if

                else
                
                    call ext_pot_energy(typi, ax, ay, az, tmpext)

                    eext = eext + tmpext
#ifdef MOLENG
                    mext(imol) = mext(imol) + tmpext
#endif

                end if

            end if

            do jm = 1, cfgs(ib)%num_mols

                do j = 1, cfgs(ib)%mols(jm)%natom

                  if( cfgs(ib)%mols(im)%glob_no(i) >= cfgs(ib)%mols(jm)%glob_no(j) ) cycle
                  !if( cfgs(ib)%mols(im)%glob_no(i) > cfgs(ib)%mols(jm)%glob_no(j) ) cycle

                  typj = cfgs(ib)%mols(jm)%atms(j)%atlabel
                  jmol = cfgs(ib)%mols(jm)%mol_label

                  bx = cfgs(ib)%mols(jm)%atms(j)%rpos(1)
                  by = cfgs(ib)%mols(jm)%atms(j)%rpos(2)
                  bz = cfgs(ib)%mols(jm)%atms(j)%rpos(3)

                  xx = ax - bx
                  yy = ay - by
                  zz = az - bz

                  call pbc_cart_pos_calc(ib, xx, yy, zz)
                  
                  rxy2 = xx * xx + yy * yy
                  rsq  = rxy2 + zz * zz

                  rvec = (/ xx, yy, zz /)
                  
                  if( rsq*onoff_rc < rcut2 ) then 

                      r = sqrt(rsq)

                      exclude = 0
                      if( im == jm ) then

                          if( cfgs(ib)%mols(im)%blist%npairs > 0 ) then

                              exclude = cfgs(ib)%mols(im)%nlist%exclist(i,j)

                          else if( cfgs(ib)%mols(im)%exc_coul_ints ) then

                              exclude = MAXINT1

                          end if
                      end if

                      if( ntpbond*exclude /= 0 .and. exclude < EXCDIH ) then

                          call bond_energy(exclude, r, rsq, sigmab, gammab)
                          epair = epair + sigmab
#ifdef MOLENG
                          mpair(imol) = mpair(imol) + sigmab
#endif

                          !virn = virn + gammab * rsq

                      end if 

                      sigmav = 0.0_wp
                      gammav = 0.0_wp
                      sigmab = 0.0_wp
                      gammab = 0.0_wp

                      !TU: Details for the exclusion/inclusion model:
                      !TU: * exclude=0 means the pair are in different molecules, and hence VdW and Coulomb interactions 
                      !TU:   are INCLUDED
                      !TU: * exclude=1,2,3,.. (small integers) means the pair is bonded with bond type 'i', and they (VdW and
                      !TU:   Coulomb interactions) are EXCLUDED
                      !TU: * exclude=-1,-2,-3,.. (small integers) means the pair is bonded with bond type 'i', and the
                      !TU:   interactions are INCLUDED
                      !TU: * exclude=MAXINT1 means the pair is within the same molecule, is unbonded, not part of
                      !TU:   an angle or dihedral, and the interactions are EXCLUDED
                      !TU: * exclude=-MAXINT1 means the pair is within the same molecule, is unbonded, not part of an angle
                      !TU:   or dihedral, and the interactions are INCLUDED
                      !TU: * exclude=MAXINT3 means the pair is within the same molecule, unbonded, is part of an angle (1-3), 
                      !TU:   is not part of a dihedral, and the interactions are EXCLUDED
                      !TU: * exclude=-MAXINT3 means the pair is within the same molecule, unbonded, is part of an angle (1-3), 
                      !TU:   is not part of a dihedral, and the interactions are INCLUDED
                      !TU: * exclude between (inclusive) EXCDIH+1 and EXCDIH+ndih, where ndih is the number of types of 
                      !TU:   dihedrals specified in FIELD, means the pair is within the same molecule, unbonded,
                      !TU:   not part of an angle (1-3), and is a 1-4 in a dihedral (where EXCDIH+i means dihedral 
                      !TU:   type i), and ther interactions are EXCLUDED
                      !TU: * exclude between (inclusive) -(EXCDIH+1) and -(EXCDIH+ndih) means the pair is within the same
                      !TU:   molecule, unbonded, not part of an angle (1-3), is a 1-4 in a dihedral, and the interactions
                      !TU:   are INCLUDED or PARTIALLY INCLUDED (for VdW or electrostatic)
                      !TU:


                      !AB: if this pair is NOT EXCLUDED from all 'non-bonded' interactions (i.e. INCLUDED)
                      !AB: include BOTH VDW AND COULOMB for both bonded (< 0) and non-bonded (== 0) pairs
                      if( exclude < 1 ) then

                          !if( cfgs(ib)%mols(im)%glob_no(i) == cfgs(ib)%mols(jm)%glob_no(j) ) cycle !goto 101

                          if( ntpvdw > 0 ) then

                              spini = cfgs(ib)%mols(im)%atms(i)%spin
                              spinj = cfgs(ib)%mols(jm)%atms(j)%spin
                              
                              call vdw_energy(typi, typj ,r , rsq, vdwcut, eunit, sigmav, volinv, rvec, spini, spinj)
                            
                              !TU: Check for partial VdW inclusion for 1-4 dihedral and
                              !TU: amend the VdW contribution accordingly
                              call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
                              if( dihtyp > 0 ) then
  
                                  sigmav = sigmav * dihvdwinc

                              end if

                              if( ncall == 1 .and. (sigmav - vdwcap) > 1.e-8_wp ) &
                              !if( sigmav > vdwcap ) &
                                  write(uout,*)"total_energy_nolist(): VdW energy > E_cap (overlap?) !!!",r, &
                                               sigmav,vdwcap,(sigmav-vdwcap),im,i,jm,j

                              evdw = evdw + sigmav

                              if( is_serial .and. sigmav > vdwcap ) return

#ifdef MOLENG
                              if (imol == jmol) then
                                  mvdwb(imol) = mvdwb(imol) + sigmav
                              else
                                  mvdwn(imol) = mvdwn(imol) + sigmav 
                                  mvdwn(jmol) = mvdwn(jmol) + sigmav 
                              end if
#endif

                              !virv = virv + gammav * rsq

                          end if

                          if( do_coul ) then

                              sigmac = 0.0_wp
                              gammac = 0.0_wp
    
                              !chgi = cfgs(ib)%mols(im)%atms(i)%charge
                              chgj = cfgs(ib)%mols(jm)%atms(j)%charge

                              if( coul_explicit ) then
                                  !ncpairs = ncpairs+1

                                  if( rxy2*onoff_xy < rcut2 ) &
                                      call explicitCoulomb(chgi, chgj, r , dielec, sigmac, gammac)

                              else if( coul_one ) then

                                  call realspaceenergy(chgi, chgj, r , dielec, sigmac, gammac)

                              else if( coul_two ) then

                                  call shiftrealspaceenergy(chgi, chgj, r, rsq, dielec, sigmac, gammac)

                              else if( coul_thr ) then

                                  call shiftdamprealspaceenergy(chgi, chgj, r, shortrangecut, dielec, sigmac, gammac)

                              end if

                              !TU: Check for partial Coulomb inclusion for 1-4 dihedral and
                              !TU: amend the Coulomb contribution accordingly
                              call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
                              if( dihtyp > 0 ) then
                                  sigmac = sigmac * dihcoulinc
                              end if

                              ereal = ereal + sigmac

#ifdef MOLENG
                              if (imol == jmol) then
                                  mrealbond(imol) = mrealbond(imol) + sigmac
                              else
                                  mrealnonb(imol) = mrealnonb(imol) + sigmac
                                  mrealnonb(jmol) = mrealnonb(jmol) + sigmac
                              end if
#endif

                              !virc = virc + gammac * rsq

                          end if

#ifdef MANY
                          !the twobody part of the "metal" potential is added to the vdw energy
                          if( exclude == 0 .and. &
                              cfgs(ib)%mols(im)%atms(i)%atype == ATM_TYPE_METAL .and. &
                              cfgs(ib)%mols(jm)%atms(j)%atype == ATM_TYPE_METAL ) then

                              sigmav = 0.0_wp
                              call metal_energy(typi, typj, r, sigmav, vir)
                              evdw = evdw + sigmav
  
                              if (imol == jmol) then
                                  mvdwb(imol) = mvdwb(imol) + sigmav
                              else
                                  mvdwn(imol) = mvdwn(imol) + sigmav 
                                  mvdwn(jmol) = mvdwn(jmol) + sigmav 
                              end if

                          end if
#endif

                      end if

                      !TU: exclude>=1 corresponds to excluded pairs; dihtyp>1 corresponds to partially or fully included
                      !TU: 1-4 dihedral pairs. Either way we must correct for exclusions or partial exclusions
                      call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
                      if( (exclude >=1 .or. dihtyp>0) .and. do_corr ) then  

                          sigmac = 0.0_wp
                          gammac = 0.0_wp

                          !chgi = cfgs(ib)%mols(im)%atms(i)%charge
                          chgj = cfgs(ib)%mols(jm)%atms(j)%charge

                          call ewald_correct(chgi, chgj, r , rsq, cfgs(ib)%cl%eta, &
                                             dielec, sigmac, gammac)

                          !TU: For partial inclusion of 1-4 Coulomb we must partially EXCLUDE fraction (1-dihcoulinc) 
                          !TU: of the interaction
                          if( dihtyp>0 ) then

                              sigmac = sigmac * (1.0_wp - dihcoulinc)

                          end if

                          !if( cfgs(ib)%mols(im)%glob_no(i) == cfgs(ib)%mols(jm)%glob_no(j) ) &
                          !    sigmac = sigmac*0.5_wp

                          ecrct = ecrct + sigmac
#ifdef MOLENG
                          mcrct(imol) = mcrct(imol) - sigmac
#endif

                      end if !( exclude < 1 )

                  end if !( rsq*onoff_rc < rcut2 )
 
                end do

            end do

        end do

    end do

#ifdef MANY
    !add in nonbonded three body
    if (numthb > 0) call thb_energy(ib, ethree, mthree, virt)

    !metal potentials beyond pairs
    if(nmetpot > 0) then

        do im = tb_start_mol, cfgs(ib)%num_mols, tb_step_mol

            imol = cfgs(ib)%mols(im)%mol_label

            do i = tb_start_atm, cfgs(ib)%mols(im)%natom, tb_step_atm

                ii = cfgs(ib)%mols(im)%glob_no(i)

                if (lmetab) then ! calculate embedding energy

                    call eam_embed(cfgs(ib)%mols(im)%atms(i)%atlabel, rho(ii), eng)
                    emany = emany - eng

                    mmany(imol) = mmany(imol) - eng

                else if(rho(ii) > 0.0_wp) then

                    eng = sqrt(rho(ii))
                    emany = emany - eng !sqrt(rho(ii))

                    mmany(imol) = mmany(imol) - eng !sqrt(rho(ii))

                end if

            end do

        end do

        if( is_parallel ) then
        !if( stepa > 1 ) then 
            call gsum(emany)
            call gsum(mmany)
            call gsum(rho)
        end if

    end if
#endif

    !write(uout,'(/,1x,(a,i10),/)')'Check interacting pairs : ',ncpairs

    !!$OMP END PARALLEL

    !add in bonded three-body energy (angles)
    if (numang > 0) call total_ang_energy(ib, eang, mang)

    !bonded dihedral energy
    if (numdih > 0) call total_dih_energy(ib, efour, mfour)

    !bonded inversion energy
    if (numinv > 0) call total_inv_energy(ib, efour, mfour)

    if( is_parallel ) then
    
        call gsync

        buf(1) = evdw
        buf(2) = ereal
        buf(3) = epair
        buf(4) = ecrct
        buf(5) = eself
        buf(6) = eext
        buf(7) = emfa

        call gsum(buf)

        evdw  = buf(1)
        ereal = buf(2)
        epair = buf(3)
        ecrct = buf(4)
        eself = buf(5)
        eext  = buf(6)
        emfa  = buf(7)

#ifdef MOLENG
        call gsum(mrealnonb)
        call gsum(mrealbond)
        call gsum(mself)
        call gsum(mcrct)
        call gsum(mvdwn)
        call gsum(mvdwb)
        call gsum(mpair)
        call gsum(mext)
#endif

        !if( nmetpot > 0 ) call gsum(rho)

    end if

    vir = 0.0_wp

    ereal = eself + (ereal - ecrct)
    
#ifdef MOLENG
    mrealnonb = mrealnonb*0.5_wp
    mvdwn = mvdwn*0.5_wp
    !mcrct =-mcrct
#endif

end subroutine total_energy_nolist


!TU: Note that the VdW energy returned by this procedure does not include the VdW long-range correction
!> calculates the energy of an atom
subroutine atom_energy(ib, im, i, coultyp, dielec, vdwcut, shortrangecut, &
                       verletshell, ereal, evdw, ethree, epair, eang, efour,  &
                       eext, emfa, vir, mrealnonb, mrealbond, mcrct, mvdwn, mvdwb, &
                       mpair, mthree, mang, mfour, mext, lcoul, lmbswap)

    use kinds_f90
    use control_type
    use bond_module, only : ntpbond, bond_energy
    use vdw_module
    use nbrlist_type
    use molecule_type
    use cell_module
    use coul_module
    use constants_module, only : CTOINTERNAL, RCORE2, HALF, SLIT_EXT0, SLIT_EXT1, SLIT_EXT2, &
                                 ATM_TYPE_METAL
    use thbpotential_module, only : numthb
    use angle_module, only : numang
    use inversion_module, only : numinv
    use dihedral_module, only : numdih
    use external_potential_module, only : ext_pot_energy, nextpot
    use metpot_module
    use comms_mpi_module, only : is_serial, is_parallel, gsync, gsum
    use parallel_loop_module, only : tb_start_mol, tb_step_mol, tb_start_atm, tb_step_atm
    use slit_module, only : in_slit, walls_soft, onoff_rc, onoff_xy, coul_explicit, &
                            uel_slit, slit_zfrac1, slit_zfrac2

    implicit none

    integer, intent(in) :: ib, im , i, coultyp
    real(kind = wp), intent (inout) :: ereal, evdw, ethree, epair, eang, efour, eext, emfa, vir
    real(kind = wp), intent (inout) :: mrealnonb(:), mrealbond(:), mvdwn(:), mvdwb(:), mpair(:), &
                                       mthree(:), mang(:), mfour(:), mcrct(:), mext(:)
    real(kind = wp), intent (in) :: dielec, vdwcut, shortrangecut, verletshell

    logical, intent(in) :: lcoul, lmbswap

    logical :: do_coul, do_corr, coul_one, coul_two, coul_thr !, coul_in_slit

    integer :: coultype
    integer :: typi, typj, j, jm, nbr, imol, jmol, exclude, start, step, last

    real(kind = wp) :: volinv, virn, virc, virr, virv, sigmac, sigmav, gammac, gammav
    real(kind = wp) :: r, rsq, rxy2, rcut2
    real(kind = wp) :: eters, virter, sigmab, gammab, virp, tmpext
    real(kind = wp) :: chgi, chgj, chge, eng, ecrct
    real(kind = wp) :: ax, ay, az, bx, by, bz, rx, ry, rz
    real(kind = wp) :: rxi, ryi, rzi, xx, yy, zz, buf(4)
    integer :: dihtyp
    real(kind = wp) :: dihvdwinc, dihcoulinc

    !TU: Spins of a pair of atoms; required for orientation-dependent VdW potentials
    real(kind = wp) :: spini(3), spinj(3)
    !TU: Separation vector between atoms after accounting for PBCs
    real(kind = wp) :: rvec(3)
    
    real(kind = wp) :: tmpdih, tmpreal, tmpvdw, tmpinv, tmpang
    
    chgi = cfgs(ib)%mols(im)%atms(i)%charge

    if( coultyp == 0 .or. .not.cfgs(ib)%mols(im)%atms(i)%is_charged ) then
        coultype = 0
        do_coul  = .false.
        do_corr  = .false.
        coul_one = .false.
        coul_two = .false.
        coul_thr = .false.
    else if( lcoul ) then
        coultype = abs(coultyp)
        do_coul  = .true.
        do_corr  = ( coultyp == 1 )
        coul_one = ( coultype < 3 )
        coul_two = ( coultype == 3 )
        coul_thr = ( coultype == 4 )
    end if

    !AB: square of cut-off radius
    rcut2 = shortrangecut*shortrangecut

    !TU: The following 2 lines pertain to AB's old implmentation of long-range VdW
    !TU: corrections, which is no longer in use
    !volinv= 1.0_wp
    !if( is_vdwlrc ) volinv= 1.0_wp/cfgs(ib)%vec%volume
    volinv = 0.0_wp

    !zero energies + virials
    ereal = 0.0_wp
    evdw = 0.0_wp
    ethree = 0.0_wp
    epair = 0.0_wp
    eang = 0.0_wp
    efour = 0.0_wp
    ecrct = 0.0_wp
    eext = 0.0_wp
    emfa = 0.0_wp

    tmpinv = 0.0_wp
    tmpreal = 0.0_wp
    tmpdih = 0.0_wp
    tmpvdw = 0.0_wp
    tmpang = 0.0_wp

#ifdef MOLENG
    mrealnonb = 0.0_wp
    mrealbond = 0.0_wp
    mvdwn = 0.0_wp
    mvdwb = 0.0_wp
    mpair = 0.0_wp
    mthree = 0.0_wp
    mang = 0.0_wp
    mfour = 0.0_wp
    mcrct = 0.0_wp
    mext = 0.0_wp
#endif

    vir = 0.0_wp
    virn = 0.0_wp
    virv = 0.0_wp
    virc = 0.0_wp
    virr = 0.0_wp
    virp = 0.0_wp

    imol = cfgs(ib)%mols(im)%mol_label
    typi = cfgs(ib)%mols(im)%atms(i)%atlabel

    !AB: no ecoulself during moves - (1) done by total_energy* and does not change 
    !AB: no ecoulself during GCMC  - (2) charged atoms not allowed for insertion separately
    
    !AB: no eters/metal contribution - all done separately?

    ax = cfgs(ib)%mols(im)%atms(i)%rpos(1)
    ay = cfgs(ib)%mols(im)%atms(i)%rpos(2)
    az = cfgs(ib)%mols(im)%atms(i)%rpos(3)

    !AB: Add extra interactions due to charged walls / MFA in the slit case
    if( in_slit .and. coul_explicit .and. cfgs(ib)%mols(im)%atms(i)%is_charged ) &
        emfa = emfa + cfgs(ib)%mols(im)%atms(i)%charge* &
                      uel_slit(ib, az, cfgs(ib)%vec%latvector(3,3))

    if(nextpot > 0) then

        tmpext = 0.0_wp

        if( walls_soft > 0 ) then

            call ext_pot_energy(typi, ax, ay, (az - cfgs(ib)%vec%latvector(3,3)*slit_zfrac1), tmpext)

            eext = eext + tmpext
#ifdef MOLENG
            mext(imol) = mext(imol) + tmpext
#endif

            if( walls_soft == 2 ) then

                call ext_pot_energy(typi, ax, ay, (cfgs(ib)%vec%latvector(3,3)*slit_zfrac2 - az), tmpext)

                eext = eext + tmpext
#ifdef MOLENG
                mext(imol) = mext(imol) + tmpext
#endif
            end if

        else

            call ext_pot_energy(typi, ax, ay, az, tmpext)

            eext = eext + tmpext
#ifdef MOLENG
            mext(imol) = mext(imol) + tmpext
#endif

        end if

    end if

    last  = cfgs(ib)%mols(im)%nlist%numnbrs(i)
    start = 1
    step  = 1
    !if( is_parallel ) then
    !    step  = max(tb_step_mol,tb_step_atm)
    !    if( step > last ) then
    !        start = 1
    !        step  = 1
    !    else
    !        start = max(tb_start_mol,tb_start_atm)
    !    end if
    !    start = tb_start_atm
    !    step  = tb_step_atm
    !end if

    ! do two body - real space coulomb and vdw

    do nbr = start, last, step

        jm = cfgs(ib)%mols(im)%nlist%nbr_list(1, nbr, i)
        j = cfgs(ib)%mols(im)%nlist%nbr_list(2, nbr, i)

        if( cfgs(ib)%mols(im)%glob_no(i) == cfgs(ib)%mols(jm)%glob_no(j) ) cycle

        jmol = cfgs(ib)%mols(jm)%mol_label
        typj = cfgs(ib)%mols(jm)%atms(j)%atlabel

        bx = cfgs(ib)%mols(jm)%atms(j)%rpos(1)
        by = cfgs(ib)%mols(jm)%atms(j)%rpos(2)
        bz = cfgs(ib)%mols(jm)%atms(j)%rpos(3)

        xx = ax - bx
        yy = ay - by
        zz = az - bz

        call pbc_cart_pos_calc(ib, xx, yy, zz)

        rxy2 = xx * xx + yy * yy
        rsq  = rxy2 + zz * zz

        rvec = (/ xx, yy, zz /)
        
        if( rsq*onoff_rc < rcut2 ) then 

            r = sqrt(rsq)

            exclude = 0
            if( im == jm ) then

                if( cfgs(ib)%mols(im)%blist%npairs > 0 ) then

                    exclude = cfgs(ib)%mols(im)%nlist%exclist(i,j)

                else if( cfgs(ib)%mols(im)%exc_coul_ints ) then

                    exclude = MAXINT1

                end if
            end if

            if( ntpbond*exclude /= 0 .and. exclude < EXCDIH ) then

                call bond_energy(exclude, r, rsq, sigmab, gammab)
                epair = epair + sigmab
#ifdef MOLENG
                mpair(imol) = mpair(imol) + sigmab
#endif

                !virn = virn + gammab * rsq

            end if 

            sigmav = 0.0_wp
            gammav = 0.0_wp
            sigmab = 0.0_wp
            gammab = 0.0_wp

            !TU: Details for the exclusion/inclusion model:
            !TU: * exclude=0 means the pair are in different molecules, and hence VdW and Coulomb interactions 
            !TU:   are INCLUDED
            !TU: * exclude=1,2,3,.. (small integers) means the pair is bonded with bond type 'i', and they (VdW and
            !TU:   Coulomb interactions) are EXCLUDED
            !TU: * exclude=-1,-2,-3,.. (small integers) means the pair is bonded with bond type 'i', and the
            !TU:   interactions are INCLUDED
            !TU: * exclude=MAXINT1 means the pair is within the same molecule, is unbonded, not part of
            !TU:   an angle or dihedral, and the interactions are EXCLUDED
            !TU: * exclude=-MAXINT1 means the pair is within the same molecule, is unbonded, not part of an angle
            !TU:   or dihedral, and the interactions are INCLUDED
            !TU: * exclude=MAXINT3 means the pair is within the same molecule, unbonded, is part of an angle (1-3), 
            !TU:   is not part of a dihedral, and the interactions are EXCLUDED
            !TU: * exclude=-MAXINT3 means the pair is within the same molecule, unbonded, is part of an angle (1-3), 
            !TU:   is not part of a dihedral, and the interactions are INCLUDED
            !TU: * exclude between (inclusive) EXCDIH+1 and EXCDIH+ndih, where ndih is the number of types of 
            !TU:   dihedrals specified in FIELD, means the pair is within the same molecule, unbonded,
            !TU:   not part of an angle (1-3), and is a 1-4 in a dihedral (where EXCDIH+i means dihedral 
            !TU:   type i), and ther interactions are EXCLUDED
            !TU: * exclude between (inclusive) -(EXCDIH+1) and -(EXCDIH+ndih) means the pair is within the same
            !TU:   molecule, unbonded, not part of an angle (1-3), is a 1-4 in a dihedral, and the interactions
            !TU:   are INCLUDED or PARTIALLY INCLUDED (for VdW or electrostatic)
            !TU:

            !AB: if this pair is NOT EXCLUDED from all 'non-bonded' interactions (i.e. INCLUDED)
            !AB: include BOTH VDW AND COULOMB for both bonded (< 0) and non-bonded (== 0) pairs
            if( exclude < 1 ) then

                if( ntpvdw > 0 ) then

                    spini = cfgs(ib)%mols(im)%atms(i)%spin
                    spinj = cfgs(ib)%mols(jm)%atms(j)%spin
                    
                    call vdw_energy(typi, typj ,r , rsq, vdwcut, eunit, sigmav, volinv, rvec, spini, spinj)

                    !TU: Check for partial VdW inclusion for 1-4 dihedral and
                    !TU: amend the VdW contribution accordingly
                    call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
                    if( dihtyp > 0 ) then

                        sigmav = sigmav * dihvdwinc

                    end if

                    evdw = evdw + sigmav

                    if( is_serial .and. sigmav > vdwcap ) return
                    !if (sigmav > vdwcap) then
                    !    write(uout,*)"atom_energy(): VdW overlap - ", &
                    !          evdw," <- ",sigmav," > ",vdwcap," at r = ",r
                    !   if( is_serial ) return
                    !end if

#ifdef MOLENG
                    if (imol == jmol) then
                        mvdwb(imol) = mvdwb(imol) + sigmav
                    else
                        mvdwn(imol) = mvdwn(imol) + sigmav
                        mvdwn(jmol) = mvdwn(jmol) + sigmav
                    end if
#endif

                    !virv = virv + gammav * rsq

                end if

                if( do_coul ) then

                    sigmac = 0.0_wp
                    gammac = 0.0_wp

                    chgj = cfgs(ib)%mols(jm)%atms(j)%charge
    
                    if( coul_explicit ) then

                        if( rxy2*onoff_xy < rcut2 ) &
                            call explicitCoulomb(chgi, chgj, r , dielec, sigmac, gammac)

                    else if( coul_one ) then

                        call realspaceenergy(chgi, chgj, r , dielec, sigmac, gammac)

                    else if( coul_two ) then

                        call shiftrealspaceenergy(chgi, chgj, r, rsq, dielec, sigmac, gammac)

                    else if( coul_thr ) then

                        call shiftdamprealspaceenergy(chgi, chgj, r, shortrangecut, dielec, sigmac, gammac)

                    end if

                    !TU: Check for partial Coulomb inclusion for 1-4 dihedral and
                    !TU: amend the Coulomb contribution accordingly
                    call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
                    if( dihtyp > 0 ) then
                        sigmac = sigmac * dihcoulinc
                    end if


                    ereal = ereal + sigmac

#ifdef MOLENG
                    if(imol == jmol) then
                        mrealbond(imol) = mrealbond(imol) + sigmac
                    else
                        mrealnonb(imol) = mrealnonb(imol) + sigmac
                    end if
#endif

                    !virc = virc + gammac * rsq

                end if

#ifdef MANY
                if( exclude == 0 .and. &
                    cfgs(ib)%mols(im)%atms(i)%atype == ATM_TYPE_METAL .and. &
                    cfgs(ib)%mols(jm)%atms(j)%atype == ATM_TYPE_METAL ) then

                    call metal_energy(typi, typj, r, eng, vir)
                    evdw = evdw + eng

                    if (imol == jmol) then
                        mvdwb(imol) = mvdwb(imol) + eng
                    else
                        mvdwn(imol) = mvdwn(imol) + eng
                        mvdwn(jmol) = mvdwn(jmol) + eng
                    end if

                end if
#endif

            end if

            !TU: exclude>=1 corresponds to excluded pairs; dihtyp>1 corresponds to partially or fully included
            !TU: 1-4 dihedral pairs. Either way we must correct for exclusions or partial exclusions
            call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
            if( (exclude >=1 .or. dihtyp>0) .and. do_corr ) then 

                sigmac = 0.0_wp
                gammac = 0.0_wp

                chgi = cfgs(ib)%mols(im)%atms(i)%charge
                chgj = cfgs(ib)%mols(jm)%atms(j)%charge

                call ewald_correct(chgi, chgj, r , rsq, cfgs(ib)%cl%eta, &
                                   dielec, sigmac, gammac)

                !TU: For partial inclusion of 1-4 Coulomb we must partially EXCLUDE fraction (1-dihcoulinc) 
                !TU: of the interaction
                if( dihtyp>0 ) then

                    sigmac = sigmac * (1.0_wp - dihcoulinc)

                end if

                ecrct = ecrct + sigmac

#ifdef MOLENG
                mcrct(imol) = mcrct(imol) - sigmac
#endif

            end if !( exclude < 1 ) else ( do_corr )

        end if !( rsq*onoff_rc < rcut2 )

    end do

    !the angle and thb may be better off calculating differences

#ifdef MANY
    !add in nonbonded three body
    if(numthb > 0) then

        eng = 0.0_wp
        call atom_thb_energy_update(ib, im, i, eng, mthree)
        ethree = ethree + eng

    end if
#endif

    !angles for this atom
    if( cfgs(ib)%mols(im)%alist%nang > 0 ) then

        eng = 0.0_wp
        call atom_ang_energy_update(ib, im, i, eng, mang)
        eang = eang + eng

    end if

    !dihedral angles
    if (numdih > 0) then

        eng = 0.0_wp
        call total_dih_energy(ib, eng, mfour)
        efour = efour + eng

    end if

    !inversion energy (bonded)
    if (numinv > 0) then 

        eng = 0.0_wp
        call total_inv_energy(ib, eng, mfour)
        efour = efour + eng

    end if

    !AB: no eters/metal contribution - all done separately?

    !if( is_parallel ) then
    if( step > 1 ) then
    
        call gsync

        buf(1) = evdw
        buf(2) = ereal
        buf(3) = ecrct
        buf(4) = epair
        
        call gsum(buf)

        evdw  = buf(1)
        ereal = buf(2)
        ecrct = buf(3)
        epair = buf(4)

    end if

    ereal = ereal - ecrct

#ifdef MOLENG
    mrealnonb = mrealnonb * 0.5_wp 
    mrealbond = mrealbond * 0.5_wp
    mvdwn = mvdwn * 0.5_wp
#endif

end subroutine atom_energy


!TU: Note that the VdW energy returned by this procedure does not include the VdW long-range correction
!> calculates the energy of an atom
subroutine atom_energy_nolist(ib, im, i, coultyp, dielec, vdwcut, shortrangecut, &
                       ereal, evdw, ethree, epair, eang, efour, eext, emfa, &
                       vir, mrealnonb, mrealbond, mcrct, mvdwn, mvdwb, mpair, mthree, mang, &
                       mfour, mext, lcoul, lmbswap)

    use kinds_f90
    use control_type
    use bond_module, only : ntpbond, bond_energy
    use vdw_module
    use nbrlist_type
    use molecule_type
    use cell_module
    use coul_module
    use constants_module, only : CTOINTERNAL, RCORE2, HALF, SLIT_EXT0, SLIT_EXT1, SLIT_EXT2, &
                                 ATM_TYPE_METAL
    use thbpotential_module, only : numthb
    use angle_module, only : numang
    use inversion_module, only : numinv
    use dihedral_module, only : numdih
    use metpot_module
    use external_potential_module, only : ext_pot_energy, nextpot
    use comms_mpi_module, only : is_serial, is_parallel, gsync, gsum
    use parallel_loop_module, only : tb_start_mol, tb_step_mol, tb_start_atm, tb_step_atm
    use slit_module, only : in_slit, walls_soft, onoff_rc, onoff_xy, coul_explicit, &
                            uel_slit, slit_zfrac1, slit_zfrac2

    implicit none

    integer, intent(in) :: ib, im , i, coultyp
    real(kind = wp), intent (inout) :: ereal, evdw, ethree, epair, eang, efour, eext, emfa, vir
    real(kind = wp), intent (inout) :: mrealnonb(:), mrealbond(:), mvdwn(:), mvdwb(:), mpair(:), &
                                       mthree(:), mang(:), mfour(:), mcrct(:), mext(:)
    real(kind = wp), intent (in) :: dielec, vdwcut, shortrangecut

    logical, intent(in) :: lcoul, lmbswap

    logical :: do_coul, do_corr, coul_one, coul_two, coul_thr !, coul_in_slit

    integer :: coultype
    integer :: typi, typj, j, jm, nbr, imol, jmol, startm, stepm, starta, stepa, last, exclude

    real(kind = wp) :: volinv, virn, virc, virr, virv, sigmac, sigmav, gammac, gammav
    real(kind = wp) :: r, rsq, rxy2, rcut2
    real(kind = wp) :: eters, virter, sigmab, gammab, virp
    real(kind = wp) :: chgi, chgj, chge, eng, ecrct, tmpext
    real(kind = wp) :: ax, ay, az, bx, by, bz, rx, ry, rz
    real(kind = wp) :: rxi, ryi, rzi, xx, yy, zz, buf(4)
    integer :: dihtyp
    real(kind = wp) :: dihvdwinc, dihcoulinc

    real(kind = wp) :: tmpdih, tmpinv, tmpang, tmpreal, tmpvdw

    !TU: Spins of a pair of atoms; required for orientation-dependent VdW potentials
    real(kind = wp) :: spini(3), spinj(3)
    !TU: Separation vector between atoms after accounting for PBCs
    real(kind = wp) :: rvec(3)
    
    chgi = cfgs(ib)%mols(im)%atms(i)%charge

    if( coultyp == 0 .or. .not.cfgs(ib)%mols(im)%atms(i)%is_charged ) then
        coultype = 0
        do_coul  = .false.
        do_corr  = .false.
        coul_one = .false.
        coul_two = .false.
        coul_thr = .false.
    else if( lcoul ) then
        coultype = abs(coultyp)
        do_coul  = .true.
        do_corr  = ( coultyp == 1 )
        coul_one = ( coultype < 3 )
        coul_two = ( coultype == 3 )
        coul_thr = ( coultype == 4 )
    end if

    !AB: square of cut-off radius
    rcut2 = shortrangecut*shortrangecut

    !TU: The following 2 lines pertain to AB's old implmentation of long-range VdW
    !TU: corrections, which is no longer in use
    !volinv= 1.0_wp
    !if( is_vdwlrc ) volinv= 1.0_wp/cfgs(ib)%vec%volume
    volinv = 0.0_wp

    !zero energies + virials
    ereal = 0.0_wp
    evdw = 0.0_wp
    ethree = 0.0_wp
    epair = 0.0_wp
    eang = 0.0_wp
    efour = 0.0_wp
    ecrct = 0.0_wp
    eext = 0.0_wp
    emfa = 0.0_wp

    tmpinv = 0.0_wp
    tmpreal = 0.0_wp
    tmpdih = 0.0_wp
    tmpvdw = 0.0_wp
    tmpang = 0.0_wp

#ifdef MOLENG
    mrealnonb = 0.0_wp
    mrealbond = 0.0_wp
    mvdwn = 0.0_wp
    mvdwb = 0.0_wp
    mpair = 0.0_wp
    mthree = 0.0_wp
    mang = 0.0_wp
    mfour = 0.0_wp
    mcrct = 0.0_wp
    mext = 0.0_wp
#endif

    vir = 0.0_wp
    virn = 0.0_wp
    virv = 0.0_wp
    virc = 0.0_wp
    virr = 0.0_wp
    virp = 0.0_wp

    imol = cfgs(ib)%mols(im)%mol_label
    typi = cfgs(ib)%mols(im)%atms(i)%atlabel

    !AB: no eters/metal contribution - all done separately?

    !AB: no ecoulself during moves - (1) done by total_energy* and does not change 
    !AB: no ecoulself during GCMC  - (2) charged atoms not allowed for insertion separately

    ax = cfgs(ib)%mols(im)%atms(i)%rpos(1)
    ay = cfgs(ib)%mols(im)%atms(i)%rpos(2)
    az = cfgs(ib)%mols(im)%atms(i)%rpos(3)

    !AB: Add extra interactions due to charged walls / MFA in the slit case
    if( in_slit .and. coul_explicit .and. cfgs(ib)%mols(im)%atms(i)%is_charged ) &
        emfa = emfa + cfgs(ib)%mols(im)%atms(i)%charge* &
                      uel_slit(ib, az, cfgs(ib)%vec%latvector(3,3))

    if(nextpot > 0) then

        tmpext = 0.0_wp

        if( walls_soft > 0 ) then

            call ext_pot_energy(typi, ax, ay, (az - cfgs(ib)%vec%latvector(3,3)*slit_zfrac1), tmpext)

            eext = eext + tmpext
#ifdef MOLENG
            mext(imol) = mext(imol) + tmpext
#endif

            if( walls_soft == 2 ) then

                call ext_pot_energy(typi, ax, ay, (cfgs(ib)%vec%latvector(3,3)*slit_zfrac2 - az), tmpext)

                eext = eext + tmpext
#ifdef MOLENG
                mext(imol) = mext(imol) + tmpext
#endif
            end if

        else

            call ext_pot_energy(typi, ax, ay, az, tmpext)

            eext = eext + tmpext
#ifdef MOLENG
            mext(imol) = mext(imol) + tmpext
#endif

        end if

    end if

    startm = tb_start_mol
    stepm = tb_step_mol
    starta = tb_start_atm
    stepa = tb_step_atm
    last = cfgs(ib)%num_mols

    ! do two body - real space coulomb and vdw
    do jm = startm, last, stepm

        do j = starta, cfgs(ib)%mols(jm)%natom, stepa

            if( cfgs(ib)%mols(im)%glob_no(i) == cfgs(ib)%mols(jm)%glob_no(j) ) cycle

            jmol = cfgs(ib)%mols(jm)%mol_label
            typj = cfgs(ib)%mols(jm)%atms(j)%atlabel

            bx = cfgs(ib)%mols(jm)%atms(j)%rpos(1)
            by = cfgs(ib)%mols(jm)%atms(j)%rpos(2)
            bz = cfgs(ib)%mols(jm)%atms(j)%rpos(3)

            xx = ax - bx
            yy = ay - by
            zz = az - bz

            call pbc_cart_pos_calc(ib, xx, yy, zz)

            rxy2 = xx * xx + yy * yy
            rsq  = rxy2 + zz * zz

            rvec = (/ xx, yy, zz /)
            
            if( rsq*onoff_rc < rcut2 ) then 

                r = sqrt(rsq)

                exclude = 0
                if( im == jm ) then
                    !TU: What if there are no bonds but there are dihedrals? Will this work? I think not
                    if( cfgs(ib)%mols(im)%blist%npairs > 0 ) then

                        exclude = cfgs(ib)%mols(im)%nlist%exclist(i,j)

                    else if( cfgs(ib)%mols(im)%exc_coul_ints ) then

                        exclude = MAXINT1

                    end if
                end if

                if( ntpbond*exclude /= 0 .and. exclude < EXCDIH ) then

                    call bond_energy(exclude, r, rsq, sigmab, gammab)
                    epair = epair + sigmab
#ifdef MOLENG
                    mpair(imol) = mpair(imol) + sigmab
#endif

                    !virn = virn + gammab * rsq

                end if 

                sigmav = 0.0_wp
                gammav = 0.0_wp
                sigmab = 0.0_wp
                gammab = 0.0_wp


                !TU: Details for the exclusion/inclusion model:
                !TU: * exclude=0 means the pair are in different molecules, and hence VdW and Coulomb interactions 
                !TU:   are INCLUDED
                !TU: * exclude=1,2,3,.. (small integers) means the pair is bonded with bond type 'i', and they (VdW and
                !TU:   Coulomb interactions) are EXCLUDED
                !TU: * exclude=-1,-2,-3,.. (small integers) means the pair is bonded with bond type 'i', and the
                !TU:   interactions are INCLUDED
                !TU: * exclude=MAXINT1 means the pair is within the same molecule, is unbonded, not part of
                !TU:   an angle or dihedral, and the interactions are EXCLUDED
                !TU: * exclude=-MAXINT1 means the pair is within the same molecule, is unbonded, not part of an angle
                !TU:   or dihedral, and the interactions are INCLUDED
                !TU: * exclude=MAXINT3 means the pair is within the same molecule, unbonded, is part of an angle (1-3), 
                !TU:   is not part of a dihedral, and the interactions are EXCLUDED
                !TU: * exclude=-MAXINT3 means the pair is within the same molecule, unbonded, is part of an angle (1-3), 
                !TU:   is not part of a dihedral, and the interactions are INCLUDED
                !TU: * exclude between (inclusive) EXCDIH+1 and EXCDIH+ndih, where ndih is the number of types of 
                !TU:   dihedrals specified in FIELD, means the pair is within the same molecule, unbonded,
                !TU:   not part of an angle (1-3), and is a 1-4 in a dihedral (where EXCDIH+i means dihedral 
                !TU:   type i), and ther interactions are EXCLUDED
                !TU: * exclude between (inclusive) -(EXCDIH+1) and -(EXCDIH+ndih) means the pair is within the same
                !TU:   molecule, unbonded, not part of an angle (1-3), is a 1-4 in a dihedral, and the interactions
                !TU:   are INCLUDED or PARTIALLY INCLUDED (for VdW or electrostatic)
                !TU:


                !AB: if this pair is NOT EXCLUDED from all 'non-bonded' interactions (i.e. INCLUDED)
                !AB: include BOTH VDW AND COULOMB for both bonded (< 0) and non-bonded (== 0) pairs
                if( exclude < 1 ) then

                    if(ntpvdw > 0) then

                        spini = cfgs(ib)%mols(im)%atms(i)%spin
                        spinj = cfgs(ib)%mols(jm)%atms(j)%spin
                        
                        call vdw_energy(typi, typj ,r , rsq, vdwcut, eunit, sigmav, volinv, rvec, spini, spinj)

                        !TU: Check for partial VdW inclusion for 1-4 dihedral and
                        !TU: amend the VdW contribution accordingly
                        call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
                        if( dihtyp > 0 ) then
                           
                            sigmav = sigmav * dihvdwinc

                        end if

                        evdw = evdw + sigmav

                        if( is_serial .and. sigmav > vdwcap ) return
                        !if( sigmav > vdwcap ) then
                        !    write(uout,*)"atom_energy_nolist(): VdW overlap - ", &
                        !          evdw," <- ",sigmav," > ",vdwcap," at r = ",r
                        !    if( is_serial ) return
                        !end if

#ifdef MOLENG
                        if (imol == jmol) then
                            mvdwb(imol) = mvdwb(imol) + sigmav
                        else
                            mvdwn(imol) = mvdwn(imol) + sigmav
                            mvdwn(jmol) = mvdwn(jmol) + sigmav
                        end if
#endif

                        virv = virv + gammav * rsq

                    end if

                    if( do_coul ) then

                        sigmac = 0.0_wp
                        gammac = 0.0_wp

                        chgj = cfgs(ib)%mols(jm)%atms(j)%charge

                        if( coul_explicit ) then

                            if( rxy2*onoff_xy < rcut2 ) &
                                call explicitCoulomb(chgi, chgj, r , dielec, sigmac, gammac)

                        else if( coul_one ) then

                            call realspaceenergy(chgi, chgj, r , dielec, sigmac, gammac)

                        else if( coul_two ) then

                            call shiftrealspaceenergy(chgi, chgj, r, rsq, dielec, sigmac, gammac)

                        else if( coul_thr ) then

                            call shiftdamprealspaceenergy(chgi, chgj, r, shortrangecut, dielec, sigmac, gammac)

                        end if

                        !TU: Check for partial Coulomb inclusion for 1-4 dihedral and
                        !TU: amend the Coulomb contribution accordingly
                        call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
                        if( dihtyp > 0 ) then
                           
                            sigmac = sigmac * dihcoulinc

                        end if


                        ereal = ereal + sigmac

#ifdef MOLENG
                        if(imol == jmol) then
                            mrealbond(imol) = mrealbond(imol) + sigmac
                        else
                            mrealnonb(imol) = mrealnonb(imol) + sigmac
                        end if
#endif

                        !virc = virc + gammac * rsq

                    end if

#ifdef MANY
                    if( exclude == 0 .and. &
                        cfgs(ib)%mols(im)%atms(i)%atype == ATM_TYPE_METAL .and. &
                        cfgs(ib)%mols(jm)%atms(j)%atype == ATM_TYPE_METAL ) then

                        call metal_energy(typi, typj, r, eng, vir)
                        evdw = evdw + eng

                        if (imol == jmol) then
                            mvdwb(imol) = mvdwb(imol) + eng
                        else
                            mvdwn(imol) = mvdwn(imol) + eng
                            mvdwn(jmol) = mvdwn(jmol) + eng
                        end if

                    end if
#endif

                end if

                !TU: exclude>=1 corresponds to excluded pairs; dihtyp>1 corresponds to partially or fully included
                !TU: 1-4 dihedral pairs. Either way we must correct for exclusions or partial exclusions
                call dih_inclusion(exclude, dihtyp, dihvdwinc, dihcoulinc)
                if( (exclude >=1 .or. dihtyp>0) .and. do_corr ) then 

                    sigmac = 0.0_wp
                    gammac = 0.0_wp

                    chgi = cfgs(ib)%mols(im)%atms(i)%charge
                    chgj = cfgs(ib)%mols(jm)%atms(j)%charge

                    call ewald_correct(chgi, chgj, r , rsq, cfgs(ib)%cl%eta, &
                                       dielec, sigmac, gammac)

                    !TU: For partial inclusion of 1-4 Coulomb we must partially EXCLUDE fraction (1-dihcoulinc) 
                    !TU: of the interaction
                    if( dihtyp>0 ) then

                        sigmac = sigmac * (1.0_wp - dihcoulinc)

                    end if

                    ecrct = ecrct + sigmac

#ifdef MOLENG
                    mcrct(imol) = mcrct(imol) - sigmac
#endif

                end if !( exclude < 1 ) else ( do_corr )

            end if !( rsq*onoff_rc < rcut2 )

        end do

    end do

    !the angle and thb may be better off calculating differences

#ifdef MANY
    !add in nonbonded three body
    if(numthb > 0) then

        eng = 0.0_wp
        call atom_thb_energy_update(ib, im, i, eng, mthree)
        ethree = ethree + eng

    end if
#endif

    !angles for this atom
    !if(numang > 0 .and. cfgs(ib)%mols(im)%alist%nang > 0) then
    if(cfgs(ib)%mols(im)%alist%nang > 0) then

        eng = 0.0_wp
        call atom_ang_energy_update(ib, im, i, eng, mang)
        eang = eang + eng

    end if

    !dihedral angles
    if (numdih > 0) then

        eng = 0.0_wp
        call total_dih_energy(ib, eng, mfour)
        efour = efour + eng

    end if

    !inversion energy (bonded)
    if (numinv > 0) then 

        eng = 0.0_wp
        call total_inv_energy(ib, eng, mfour)
        efour = efour + eng

    end if

    !AB: no eters/metal contribution - all done separately?

    if( is_parallel ) then
    
        call gsync

        buf(1) = evdw
        buf(2) = ereal
        buf(3) = ecrct
        buf(4) = epair
        
        call gsum(buf)

        evdw  = buf(1)
        ereal = buf(2)
        ecrct = buf(3)
        epair = buf(4)

    end if

    ereal = ereal - ecrct

#ifdef MOLENG
    mrealnonb = mrealnonb * 0.5_wp 
    mrealbond = mrealbond * 0.5_wp
    mvdwn = mvdwn * 0.5_wp
#endif

end subroutine atom_energy_nolist


!metal energy for a move
subroutine atom_metal_energy(ib, im, i, cutoff, emeto, emetn, mmeto, mmetn)

    use kinds_f90
    use metpot_module
    use cell_module
    use comms_mpi_module, only : is_parallel, gsum
    use parallel_loop_module, only : tb_start_atm, tb_step_atm
    use constants_module, only : ATM_TYPE_METAL
    
    implicit none

    integer, intent(in) :: ib, im, i
    real(kind = wp), intent(in) :: cutoff
    real(kind = wp), intent(out) :: emeto, emetn, mmeto(:), mmetn(:)

    integer :: j, jm, jj, last, nbr, ii
    real(kind = wp) :: trho, eng

    emeto = 0.0_wp
    emetn = 0.0_wp
    mmeto = 0.0_wp
    mmetn = 0.0_wp

    if (cfgs(ib)%mols(im)%atms(i)%atype /= ATM_TYPE_METAL) return !if the central atom is not of metal type return

    rho = 0.0_wp

    ii = cfgs(ib)%mols(im)%glob_no(i)
    last = cfgs(ib)%mols(im)%nlist%numnbrs(i)

    !recalculate new density of neighbours
    do nbr = tb_start_atm, last, tb_step_atm

        jm = cfgs(ib)%mols(im)%nlist%nbr_list(1, nbr, i)
        j = cfgs(ib)%mols(im)%nlist%nbr_list(2, nbr, i)
        jj = cfgs(ib)%mols(jm)%glob_no(j)

        if (cfgs(ib)%mols(jm)%atms(j)%atype /= ATM_TYPE_METAL) cycle

        trho = 0.0_wp
        call metalatom_rho(ib, jm, j, jj, cutoff, trho)
        rho(jj) = trho

    end do

    do nbr = tb_start_atm, last, tb_step_atm

        jm = cfgs(ib)%mols(im)%nlist%nbr_list(1, nbr, i)
        j = cfgs(ib)%mols(im)%nlist%nbr_list(2, nbr, i)
        jj = cfgs(ib)%mols(jm)%glob_no(j)

        if (lmetab) then ! calculate embedding energy

            call eam_embed(cfgs(ib)%mols(jm)%atms(j)%atlabel, rho(jj), eng)
            emetn = emetn - eng 
            call eam_embed(cfgs(ib)%mols(jm)%atms(j)%atlabel, cfgs(ib)%mols(jm)%smden(j), eng)
            emeto = emeto - eng 

        else

            if(rho(jj) > 0.0_wp) emetn = emetn - sqrt(rho(jj))
            if(cfgs(ib)%mols(jm)%smden(j) > 0.0_wp) emeto = emeto - sqrt(cfgs(ib)%mols(jm)%smden(j))
        end if

    end do


    if( is_parallel ) then

        call gsum(emetn)
        call gsum(emeto)
        call gsum(rho)

    end if

end subroutine atom_metal_energy

subroutine atom_metal_energy_full(ib, cutoff, emet, mmet)

    use kinds_f90
    use metpot_module
    use cell_module
    use comms_mpi_module, only : is_parallel, gsum
    use parallel_loop_module, only : tb_start_atm, tb_step_atm, tb_start_mol, tb_step_mol
    use constants_module, only : ATM_TYPE_METAL
    
    implicit none

    integer, intent(in) :: ib
    real(kind = wp), intent(in) :: cutoff
    real(kind = wp), intent(out) :: emet, mmet(:)

    integer :: j, jm, jj
    real(kind = wp) :: trho, eng

    emet = 0.0_wp
    mmet = 0.0_wp

    rho = 0.0_wp

    !recalculate new density of neighbours
    do jm = tb_start_mol, cfgs(ib)%num_mols, tb_step_mol

        do j = tb_start_atm, cfgs(ib)%mols(jm)%natom, tb_step_atm

            jj = cfgs(ib)%mols(jm)%glob_no(j)

            if (cfgs(ib)%mols(jm)%atms(j)%atype /= ATM_TYPE_METAL) cycle

            trho = 0.0_wp
            call metalatom_rho(ib, jm, j, jj, cutoff, trho)
            rho(jj) = trho

        end do

    end do

    do jm = tb_start_mol, cfgs(ib)%num_mols, tb_step_mol

        do j = tb_start_atm, cfgs(ib)%mols(jm)%natom, tb_step_atm

            jj = cfgs(ib)%mols(jm)%glob_no(j)

            if (lmetab) then ! calculate embedding energy

                call eam_embed(cfgs(ib)%mols(jm)%atms(j)%atlabel, rho(jj), eng)
                emet = emet - eng

            else

                if(rho(jj) > 0.0_wp) emet = emet - sqrt(rho(jj))

            end if

        end do

    end do

    if( is_parallel ) then

        call gsum(emet)
        call gsum(rho)

    end if

end subroutine atom_metal_energy_full


!calculates the density term for metal potentials
subroutine metalatom_rho(ib, im, i, ii, cutoff, trho)

    use kinds_f90
    use constants_module, only : RCORE2, ATM_TYPE_METAL
    use cell_module
    use metpot_module, only : metal_rho, metal_eamrho, lmetab

    implicit none

    integer, intent(in) :: ib, im, i, ii
    real(kind = wp), intent(in) :: cutoff
    real(kind = wp), intent(out) :: trho

    integer :: jm, j, typi, typj, nbr, last
    real(kind = wp) :: r, rcut2, tmp, ax, ay, az, bx, by, bz, xx, yy, zz, rsq, rx, ry, rz

    rcut2 = cutoff*cutoff

    trho = 0.0_wp

    ax = cfgs(ib)%mols(im)%atms(i)%rpos(1)
    ay = cfgs(ib)%mols(im)%atms(i)%rpos(2)
    az = cfgs(ib)%mols(im)%atms(i)%rpos(3)

    typi = cfgs(ib)%mols(im)%atms(i)%atlabel
    last = cfgs(ib)%mols(im)%nlist%numnbrs(i)

    do nbr = 1, last

        jm = cfgs(ib)%mols(im)%nlist%nbr_list(1, nbr, i)
        j = cfgs(ib)%mols(im)%nlist%nbr_list(2, nbr, i)

        if( cfgs(ib)%mols(im)%glob_no(i) == cfgs(ib)%mols(jm)%glob_no(j) ) cycle

        if (cfgs(ib)%mols(jm)%atms(j)%atype /= ATM_TYPE_METAL) cycle

        typj = cfgs(ib)%mols(jm)%atms(j)%atlabel

        bx = cfgs(ib)%mols(jm)%atms(j)%rpos(1)
        by = cfgs(ib)%mols(jm)%atms(j)%rpos(2)
        bz = cfgs(ib)%mols(jm)%atms(j)%rpos(3)

        rx = ax - bx
        ry = ay - by
        rz = az - bz

        call pbc_cart_pos_calc(ib, rx, ry, rz)

        rsq = rx * rx + ry * ry + rz * rz

        if ( rsq < rcut2 ) then 
        !if (r <= cutoff .and. r > 0.0001) then

            r = sqrt(rsq)

            tmp = 0.0_wp

            if(lmetab) then   
                ! embedded atom type potential
                call metal_eamrho(typj, r, tmp)
            else
                !finnis-sinclair type
                call metal_rho(typi, typj, r, tmp)
            end if

            trho = trho + tmp

        end if

    end do

end subroutine metalatom_rho


subroutine atom_tersoff_energy(ib, im, i, cutoff, eters, virter)


    use kinds_f90
    use cell_module
    use tersoff_module

    implicit none

    integer, intent(in) :: ib, im, i
    real(kind = wp), intent(in) :: cutoff
    real(kind = wp), intent(out) :: eters, virter

    integer :: nbr, j, jm

    real(kind = wp) :: eng 

    eters = 0.0_wp
    virter = 0.0_wp

    do nbr = 1, cfgs(ib)%mols(im)%nlist%numnbrs(i)

        jm = cfgs(ib)%mols(im)%nlist%nbr_list(1, nbr, i)
        j = cfgs(ib)%mols(im)%nlist%nbr_list(2, nbr, i)

        eng = 0.0_wp
        call atom_tersoff(ib, jm, j, cutoff, eng, virter)
        eters = eters + eng * 0.5_wp

    end do

end subroutine atom_tersoff_energy


subroutine atom_tersoff(ib, im, i, cutoff, eters, virter)

    use kinds_f90
    use cell_module
    use tersoff_module

    implicit none

    integer, intent(in) :: ib, im, i

    real(kind = wp), intent(in)  :: cutoff
    real(kind = wp), intent(out) :: eters, virter

    integer :: nbr, jm, j, km, k, typi, typj, typk, nbr_j, nbr_k
    integer :: iter, jter, kter, jjter, kkter

    real(kind = wp) :: v1, v2, v3, g1, g2, g3
    real(kind=wp) :: ix, iy, iz, jx, jy, jz
    real(kind=wp) :: rxij, ryij, rzij
    real(kind=wp) :: rij, costh, gamma, xx, yy, zz
    real(kind=wp) :: vterm, eterm, gtheta, gam_ij
    real(kind=wp) :: bi, ci, di, ei, hi

    !work arrays
    real(kind = wp) :: eat(cfgs(ib)%number_of_atoms), erp(cfgs(ib)%number_of_atoms), &
                       esc(cfgs(ib)%number_of_atoms), gat(cfgs(ib)%number_of_atoms)
    real(kind = wp) :: grp(cfgs(ib)%number_of_atoms), gsc(cfgs(ib)%number_of_atoms)
    real(kind = wp) :: xtf(cfgs(ib)%number_of_atoms), ytf(cfgs(ib)%number_of_atoms), &
                       ztf(cfgs(ib)%number_of_atoms), rtf(cfgs(ib)%number_of_atoms)

    logical :: flag

    eters = 0.0_wp
    virter = 0.0_wp
    flag = .false.

    typi = cfgs(ib)%mols(im)%atms(i)%atlabel

    eat = 0.0_wp
    gat = 0.0_wp
    erp = 0.0_wp
    grp = 0.0_wp
    esc = 0.0_wp
    gsc = 0.0_wp
    xtf = 0.0_wp
    ytf = 0.0_wp
    ztf = 0.0_wp
    rtf = 0.0_wp

    ix = cfgs(ib)%mols(im)%atms(i)%rpos(1)
    iy = cfgs(ib)%mols(im)%atms(i)%rpos(2)
    iz = cfgs(ib)%mols(im)%atms(i)%rpos(3)

    ! set up "two-body" work arrays
    do nbr = 1, cfgs(ib)%mols(im)%nlist%numnbrs(i)

        jm = cfgs(ib)%mols(im)%nlist%nbr_list(1, nbr, i)
        j = cfgs(ib)%mols(im)%nlist%nbr_list(2, nbr, i)

        typj = cfgs(ib)%mols(jm)%atms(j)%atlabel

        jx = cfgs(ib)%mols(jm)%atms(j)%rpos(1)
        jy = cfgs(ib)%mols(jm)%atms(j)%rpos(2)
        jz = cfgs(ib)%mols(jm)%atms(j)%rpos(3)

        !get distances 
        rxij = ix - jx
        ryij = iy - jy
        rzij = iz - jz

        xx = cfgs(ib)%vec%invlat(1,1) * rxij + cfgs(ib)%vec%invlat(2,1) * ryij +    &
                                         cfgs(ib)%vec%invlat(3,1) * rzij
        yy = cfgs(ib)%vec%invlat(1,2) * rxij + cfgs(ib)%vec%invlat(2,2) * ryij +    &
                                         cfgs(ib)%vec%invlat(3,2) * rzij
        zz = cfgs(ib)%vec%invlat(1,3) * rxij + cfgs(ib)%vec%invlat(2,3) * ryij +    &
                                         cfgs(ib)%vec%invlat(3,3) * rzij

        rxij = xx - nint(xx)
        ryij = yy - nint(yy)
        rzij = zz - nint(zz)

        ! get correct distances
        xx = cfgs(ib)%vec%latvector(1,1) * rxij + cfgs(ib)%vec%latvector(2,1) * ryij + cfgs(ib)%vec%latvector(3,1) * rzij
        yy = cfgs(ib)%vec%latvector(1,2) * rxij + cfgs(ib)%vec%latvector(2,2) * ryij + cfgs(ib)%vec%latvector(3,2) * rzij
        zz = cfgs(ib)%vec%latvector(1,3) * rxij + cfgs(ib)%vec%latvector(2,3) * ryij + cfgs(ib)%vec%latvector(3,3) * rzij
        rij = sqrt(xx * xx + yy * yy + zz * zz)

        if (rij <= cutoff .and. rij > 1.0e-8_wp) then

            call tersoff_two(typi, typj, rij, v1, v2, v3, g1, g2, g3)

            eat(nbr) = v1
            erp(nbr) = v2
            esc(nbr) = v3

            gat(nbr) = g1
            grp(nbr) = g2
            gsc(nbr) = g3

            xtf(nbr) = xx / rij
            ytf(nbr) = yy / rij
            ztf(nbr) = zz / rij
            rtf(nbr) = rij

        end if
    end do

    !the many body part

    iter = lstter(typi)

    bi = prmter(iter,7)
    ei = prmter(iter,8)
    ci = prmter(iter,9)
    di = prmter(iter,10)
    hi = prmter(iter,11)

    ! loop over j-nbrs
    do nbr_j = 1, cfgs(ib)%mols(im)%nlist%numnbrs(i)

        Jm = cfgs(ib)%mols(im)%nlist%nbr_list(1, nbr_j, i)
        j = cfgs(ib)%mols(im)%nlist%nbr_list(2, nbr_j, i)

        typj = cfgs(ib)%mols(jm)%atms(j)%atlabel
        jter = lstter(typj)

        if (jter > 0 .and. j /= i .and. rtf(nbr_j) <= cutoff .and. rtf(nbr_j) > 1.0e-8_wp) then

            jjter = (max(iter,jter) * (max(iter,jter) - 1)) / 2 + min(iter,jter)

            vterm = 0.0_wp
            eterm = 0.0_wp
            flag = .false.

            !loop over k-nbrs
            do nbr_k = 1, cfgs(ib)%mols(im)%nlist%numnbrs(i)

                km = cfgs(ib)%mols(im)%nlist%nbr_list(1, nbr_k, i)
                k = cfgs(ib)%mols(im)%nlist%nbr_list(2, nbr_k, i)

                typk = cfgs(ib)%mols(km)%atms(k)%atlabel
                kter = lstter(typk)

                if (kter > 0 .and. k /= i .and. k /= j .and. rtf(nbr_k) <= cutoff) then

                    kkter = (max(iter,kter) * (max(iter,kter) - 1)) / 2 + min(iter,kter)

                    if (rtf(nbr_k) <= vmbp(1,kkter,1)) then

                        !work out angle
                        costh = (xtf(nbr_j) * xtf(nbr_k) + ytf(nbr_j) * ytf(nbr_k) + ztf(nbr_j) * ztf(nbr_k))

                        gtheta= 1.0_wp + (ci/di)**2 - ci**2 / (di**2 + (hi - costh)**2)
                        eterm = eterm + gtheta * prmter2(kkter,2) * esc(nbr_k)
                        vterm = vterm + gtheta * prmter2(kkter,2) * gsc(nbr_k) * rtf(nbr_k)

                        flag = .true.

                    end if

                end if

            end do

        end if

        if(flag) then !get energy

            gam_ij=prmter2(jjter,1)*(1.d0+(bi*eterm)**ei)**(-0.5d0/ei)
            gamma = 0.5 * prmter2(jjter,1) * bi * (bi * eterm)**(ei - 1.0) * eat(nbr_j) * &
                    (1.0 + (bi * eterm)**ei)**(-0.5 / ei - 1.0)
            eters = eters + erp(nbr_j) - gam_ij * eat(nbr_j)
            virter = virter + gamma * vterm + (grp(nbr_j) - gam_ij * gat(nbr_j)) * rtf(nbr_j)

        end if

    end do

end subroutine atom_tersoff


subroutine thb_energy(ib, ethree, mthree, virt)
    use kinds_f90
    use cell_module
    use molecule_type
    use species_module, only : number_of_molecules


    implicit none

    integer, intent(in) :: ib

    real(kind = wp), intent(out) :: ethree, mthree(number_of_molecules), virt
    integer :: i, im

    real(kind = wp) :: eng

    ethree = 0.0
    mthree = 0.0

    do im = 1, cfgs(ib)%num_mols

        do i = 1, cfgs(ib)%mols(im)%natom
            eng = 0.0
            call atom_thb_energy(ib, im, i, eng, mthree)
            ethree = ethree + eng
        end do
    
    end do
   
end subroutine thb_energy


!******************************************************************************
! calculate the three body energy of an atom - only works if atom is at the
! centre of a triplet. called when calculating
! the total energy of a box.
!******************************************************************************/
subroutine atom_thb_energy(ib, im, i, ethree, mthree)

    use kinds_f90
    use species_module, only : number_of_molecules
    use cell_module
    use thbpotential_module

    implicit none

    integer, intent(in) :: i, im, ib
    real(kind = wp), intent(out) :: ethree, mthree(number_of_molecules)

    integer :: jm, j, km, k, trp, pot, imol, jmol, kmol

    real(kind=wp) :: ix, iy, iz, jx, jy, jz, kx, ky, kz, rx, ry, rz
    real(kind=wp) :: rxij, ryij, rzij, rxik, ryik, rzik, rxjk, ryjk, rzjk
    real(kind=wp) :: rij, rik, rjk, costh, theta, eng, xx, yy, zz

    ethree = 0.0_wp

    do trp = 1, cfgs(ib)%mols(im)%tlist%numtrip(i)

        jm = cfgs(ib)%mols(im)%tlist%thb_triplets(i,trp,2)
        j = cfgs(ib)%mols(im)%tlist%thb_triplets(i,trp,3)

        km = cfgs(ib)%mols(im)%tlist%thb_triplets(i,trp,4)
        k = cfgs(ib)%mols(im)%tlist%thb_triplets(i,trp,5)

        ! now have suitable types - calc distances
        ix = cfgs(ib)%mols(im)%atms(i)%rpos(1)
        iy = cfgs(ib)%mols(im)%atms(i)%rpos(2)
        iz = cfgs(ib)%mols(im)%atms(i)%rpos(3)

        jx = cfgs(ib)%mols(jm)%atms(j)%rpos(1)
        jy = cfgs(ib)%mols(jm)%atms(j)%rpos(2)
        jz = cfgs(ib)%mols(jm)%atms(j)%rpos(3)

        kx = cfgs(ib)%mols(km)%atms(k)%rpos(1)
        ky = cfgs(ib)%mols(km)%atms(k)%rpos(2)
        kz = cfgs(ib)%mols(km)%atms(k)%rpos(3)

        rxij = ix - jx
        ryij = iy - jy
        rzij = iz - jz

        call pbc_cart_pos_calc(ib, rxij, ryij, rzij)

        rij  = sqrt( rxij*rxij  +  ryij*ryij  +  rzij*rzij )
        rxij = rxij / rij
        ryij = ryij / rij
        rzij = rzij / rij

        rxik = ix - kx
        ryik = iy - ky
        rzik = iz - kz

        call pbc_cart_pos_calc(ib, rxik, ryik, rzik)

        rik  = sqrt( rxik*rxik  +  ryik*ryik  +  rzik*rzik )
        rxik = rxik / rik
        ryik = ryik / rik
        rzik = rzik / rik

        costh = rxij * rxik + ryij * ryik + rzij * rzik

        costh = min(costh, 1.0_wp)
        costh = max(costh, -1.0_wp)

        theta = acos(costh)

        pot = cfgs(ib)%mols(im)%tlist%thb_triplets(i,trp,1)
        eng = 0.0_wp
        call calc_thb_energy(pot, theta, rij, rik, rjk, eng)
        
        ethree = ethree + eng

        imol = cfgs(ib)%mols(im)%mol_label
        jmol = cfgs(ib)%mols(jm)%mol_label
        kmol = cfgs(ib)%mols(km)%mol_label

        mthree(imol) = mthree(imol) + eng

        if(jmol /= imol) mthree(jmol) = mthree(jmol) + eng
        if(kmol /= jmol .and. kmol /= imol) mthree(kmol) = mthree(kmol) + eng

    end do

end subroutine atom_thb_energy


!************************************************
!calculates nonbonded three-body energy
!when an atom is moved. It checks that an atom is at the
!center of a triplet and at the edges
!************************************************
subroutine atom_thb_energy_update(ib, am, atm, ethree, mthree)

    use kinds_f90
    use species_module, only : number_of_molecules
    use cell_module
    use thbpotential_module

    implicit none

    integer, intent(in) :: ib, am, atm
    real(kind = wp), intent(out) :: ethree, mthree(number_of_molecules)

    integer :: im, i, jm, j, km, k, trp, pot, imol, jmol, kmol

    real(kind=wp) :: ix, iy, iz, jx, jy, jz, kx, ky, kz, rx, ry, rz
    real(kind=wp) :: rxij, ryij, rzij, rxik, ryik, rzik, rxjk, ryjk, rzjk
    real(kind=wp) :: rij, rik, rjk, costh, theta, eng, xx, yy, zz

    ethree = 0.0_wp
    mthree = 0.0_wp

    im = am
    i = atm

    !atom i is at the centre of a triplet

    if (cfgs(ib)%mols(im)%tlist%numtrip(i) > 0) then

        !the atom here is at the centre of a triplet
        do trp = 1, cfgs(ib)%mols(im)%tlist%numtrip(i)

            jm = cfgs(ib)%mols(im)%tlist%thb_triplets(i,trp,2)
            j = cfgs(ib)%mols(im)%tlist%thb_triplets(i,trp,3)

            km = cfgs(ib)%mols(im)%tlist%thb_triplets(i,trp,4)
            k = cfgs(ib)%mols(im)%tlist%thb_triplets(i,trp,5)

            ! now have suitable types - calc distances
            ix = cfgs(ib)%mols(im)%atms(i)%rpos(1)
            iy = cfgs(ib)%mols(im)%atms(i)%rpos(2)
            iz = cfgs(ib)%mols(im)%atms(i)%rpos(3)

            jx = cfgs(ib)%mols(jm)%atms(j)%rpos(1)
            jy = cfgs(ib)%mols(jm)%atms(j)%rpos(2)
            jz = cfgs(ib)%mols(jm)%atms(j)%rpos(3)

            kx = cfgs(ib)%mols(km)%atms(k)%rpos(1)
            ky = cfgs(ib)%mols(km)%atms(k)%rpos(2)
            kz = cfgs(ib)%mols(km)%atms(k)%rpos(3)

            rxij = ix - jx
            ryij = iy - jy
            rzij = iz - jz

            call pbc_cart_pos_calc(ib, rxij, ryij, rzij)

            rij  = sqrt( rxij*rxij  +  ryij*ryij  +  rzij*rzij )
            rxij = rxij / rij
            ryij = ryij / rij
            rzij = rzij / rij

            rxik = ix - kx
            ryik = iy - ky
            rzik = iz - kz

            call pbc_cart_pos_calc(ib, rxik, ryik, rzik)

            rik  = sqrt( rxik*rxik  +  ryik*ryik  +  rzik*rzik )
            rxik = rxik / rik
            ryik = ryik / rik
            rzik = rzik / rik

            costh = rxij * rxik + ryij * ryik + rzij * rzik
            costh = min(costh, 1.0_wp)
            costh = max(costh, -1.0_wp)

            theta = acos(costh)

            pot = cfgs(ib)%mols(im)%tlist%thb_triplets(i,trp,1)
            eng = 0.0_wp

            call calc_thb_energy(pot, theta, rij, rik, rjk, eng)

            ethree = ethree + eng

            imol = cfgs(ib)%mols(im)%mol_label
            jmol = cfgs(ib)%mols(jm)%mol_label
            kmol = cfgs(ib)%mols(km)%mol_label

            mthree(imol) = mthree(imol) + eng

            if(jmol /= imol) mthree(jmol) = mthree(jmol) + eng
            if(kmol /= jmol .and. kmol /= imol) mthree(kmol) = mthree(kmol) + eng

        end do

    else

    !check to see if atom i is part of triplet but not at center.
    !At present check all possible permutations but it may be possible to
    !use the two body neighbour list to cut down search. However,
    !the nbrlist and thblist can, in theory, go out of sync.

        do im = 1, cfgs(ib)%num_mols

            do i = 1, cfgs(ib)%mols(im)%natom

                do trp = 1, cfgs(ib)%mols(im)%tlist%numtrip(i)

                    jm = cfgs(ib)%mols(im)%tlist%thb_triplets(i,trp,2)
                    j = cfgs(ib)%mols(im)%tlist%thb_triplets(i,trp,3)

                    km = cfgs(ib)%mols(im)%tlist%thb_triplets(i,trp,4)
                    k = cfgs(ib)%mols(im)%tlist%thb_triplets(i,trp,5)

                    if( (jm == am .and. j == atm) .or. (km == am .and. k == atm) ) then

                        ! now have suitable types - calc distances
                        ix = cfgs(ib)%mols(im)%atms(i)%rpos(1)
                        iy = cfgs(ib)%mols(im)%atms(i)%rpos(2)
                        iz = cfgs(ib)%mols(im)%atms(i)%rpos(3)

                        jx = cfgs(ib)%mols(jm)%atms(j)%rpos(1)
                        jy = cfgs(ib)%mols(jm)%atms(j)%rpos(2)
                        jz = cfgs(ib)%mols(jm)%atms(j)%rpos(3)

                        kx = cfgs(ib)%mols(km)%atms(k)%rpos(1)
                        ky = cfgs(ib)%mols(km)%atms(k)%rpos(2)
                        kz = cfgs(ib)%mols(km)%atms(k)%rpos(3)

                        xx = ix - jx
                        yy = iy - jy
                        zz = iz - jz

                        call pbc_cart_pos_calc(ib, xx, yy, zz)

                        rij  = sqrt( xx*xx  +  yy*yy  +  zz*zz )
                        rxij = xx / rij
                        ryij = yy / rij
                        rzij = zz / rij

                        xx = ix - kx
                        yy = iy - ky
                        zz = iz - kz

                        call pbc_cart_pos_calc(ib, xx, yy, zz)

                        rik  = sqrt( xx*xx  +  yy*yy  +  zz*zz )
                        rxik = xx / rik
                        ryik = yy / rik
                        rzik = zz / rik

                        costh = rxij * rxik + ryij * ryik + rzij * rzik
                        costh = min(costh, 1.0_wp)
                        costh = max(costh, -1.0_wp)

                        theta = acos(costh)

                        pot = cfgs(ib)%mols(im)%tlist%thb_triplets(i,trp,1)
                        eng = 0.0_wp

                        call calc_thb_energy(pot, theta, rij, rik, rjk, eng)

                        ethree = ethree + eng

                        imol = cfgs(ib)%mols(im)%mol_label
                        jmol = cfgs(ib)%mols(jm)%mol_label
                        kmol = cfgs(ib)%mols(km)%mol_label

                        mthree(imol) = mthree(imol) + eng

                        if(jmol /= imol) mthree(jmol) = mthree(jmol) + eng
                        if(kmol /= jmol .and. kmol /= imol) mthree(kmol) = mthree(kmol) + eng

                    end if

                end do

            end do

        end do

    end if

end subroutine atom_thb_energy_update


!> sums up the interaction via bonded 3-body potentials (angles) within molecules
subroutine total_ang_energy(ib, eang, mang)

    use kinds_f90
    use constants_module, only : uout
    use species_module, only : number_of_molecules
    use cell_module
    use angle_module, only : calc_angle_energy

    implicit none

    integer, intent(in) :: ib
    real(kind = wp), intent(out) :: eang, mang(number_of_molecules)

    integer :: im, i, j, k, trp, pot, imol

    real(kind=wp) :: ix, iy, iz, jx, jy, jz, kx, ky, kz, rx, ry, rz
    real(kind=wp) :: rxij, ryij, rzij, rxik, ryik, rzik, rxjk, ryjk, rzjk
    real(kind=wp) :: rij, rik, rjk, costh, theta, eng, gamma, xx, yy, zz

    character :: chCos1*20, chCos2*20, chAng1*20, chAng2*20

    eang = 0.0
    mang = 0.0

    do im = 1, cfgs(ib)%num_mols

        do trp = 1, cfgs(ib)%mols(im)%alist%nang

            i = cfgs(ib)%mols(im)%alist%apair(trp,1)
            j = cfgs(ib)%mols(im)%alist%apair(trp,2)
            k = cfgs(ib)%mols(im)%alist%apair(trp,3)

            ! now have suitable types - calc distances
            ix = cfgs(ib)%mols(im)%atms(i)%rpos(1)
            iy = cfgs(ib)%mols(im)%atms(i)%rpos(2)
            iz = cfgs(ib)%mols(im)%atms(i)%rpos(3)

            jx = cfgs(ib)%mols(im)%atms(j)%rpos(1)
            jy = cfgs(ib)%mols(im)%atms(j)%rpos(2)
            jz = cfgs(ib)%mols(im)%atms(j)%rpos(3)

            kx = cfgs(ib)%mols(im)%atms(k)%rpos(1)
            ky = cfgs(ib)%mols(im)%atms(k)%rpos(2)
            kz = cfgs(ib)%mols(im)%atms(k)%rpos(3)

            xx = jx - ix
            yy = jy - iy
            zz = jz - iz

            call pbc_cart_pos_calc(ib, xx, yy, zz)

            rij  = sqrt( xx*xx  +  yy*yy  +  zz*zz )
            rxij = xx / rij
            ryij = yy / rij
            rzij = zz / rij

            xx = kx - ix
            yy = ky - iy
            zz = kz - iz

            call pbc_cart_pos_calc(ib, xx, yy, zz)

            rik  = sqrt( xx*xx  +  yy*yy  +  zz*zz )
            rxik = xx / rik
            ryik = yy / rik
            rzik = zz / rik

            pot = cfgs(ib)%mols(im)%alist%apair(trp,4)

            !calculate theta
            costh = rxij * rxik + ryij * ryik + rzij * rzik

            write(chCos1,'(f20.10)')costh

            costh = min(costh, 1.0_wp)
            costh = max(costh, -1.0_wp)
            theta = acos(costh)

            !If (Abs(costh) > 1.0_wp) costh = Sign(1.0_wp,costh)
            !theta = Acos(costh)

            write(chCos2,'(f20.10)')costh
            write(chAng2,'(f20.10)')theta/TORADIANS

            eng = 0.0
            gamma = 0.0

            call calc_angle_energy(pot, rij, rik, theta, costh, eng)

            write(chAng1,'(f20.10)')eng

            !call cry(uout,'', &
            !        "total_ang_energy(): cos1, cos2, angle = "//&
            !        &trim(chCos1)//trim(chCos2)//trim(chAng2)//&
            !        &" -> energy = "//trim(chAng1),0)

            eang = eang + eng

            imol = cfgs(ib)%mols(im)%mol_label
            mang(imol) = mang(imol) + eng

        end do

    end do

end subroutine total_ang_energy


!> interaction via bonded 3-body potentials (angles) for a single molecule
subroutine mol_ang_energy(ib, im, eang, mang)

    use kinds_f90
    use species_module, only : number_of_molecules
    use cell_module
    use angle_module, only : calc_angle_energy

    implicit none

    integer, intent(in) :: im, ib
    real(kind = wp), intent(out) :: eang, mang(number_of_molecules)

    integer :: i, j, k, trp, pot, imol

    real(kind=wp) :: ix, iy, iz, jx, jy, jz, kx, ky, kz, rx, ry, rz
    real(kind=wp) :: rxij, ryij, rzij, rxik, ryik, rzik, rxjk, ryjk, rzjk
    real(kind=wp) :: rij, rik, rjk, costh, theta, eng, gamma, xx, yy, zz

    eang = 0.0
    mang = 0.0

    do trp = 1, cfgs(ib)%mols(im)%alist%nang

        i = cfgs(ib)%mols(im)%alist%apair(trp,1)
        j = cfgs(ib)%mols(im)%alist%apair(trp,2)
        k = cfgs(ib)%mols(im)%alist%apair(trp,3)

        ! now have suitable types - calc distances
        ix = cfgs(ib)%mols(im)%atms(i)%rpos(1)
        iy = cfgs(ib)%mols(im)%atms(i)%rpos(2)
        iz = cfgs(ib)%mols(im)%atms(i)%rpos(3)

        jx = cfgs(ib)%mols(im)%atms(j)%rpos(1)
        jy = cfgs(ib)%mols(im)%atms(j)%rpos(2)
        jz = cfgs(ib)%mols(im)%atms(j)%rpos(3)

        kx = cfgs(ib)%mols(im)%atms(k)%rpos(1)
        ky = cfgs(ib)%mols(im)%atms(k)%rpos(2)
        kz = cfgs(ib)%mols(im)%atms(k)%rpos(3)

        xx = ix - jx
        yy = iy - jy
        zz = iz - jz

        call pbc_cart_pos_calc(ib, xx, yy, zz)

        rij  = sqrt( xx*xx  +  yy*yy  +  zz*zz )
        rxij = xx / rij
        ryij = yy / rij
        rzij = zz / rij

        xx = ix - kx
        yy = iy - ky
        zz = iz - kz

        call pbc_cart_pos_calc(ib, xx, yy, zz)

        rik  = sqrt( xx*xx  +  yy*yy  +  zz*zz )
        rxik = xx / rik
        ryik = yy / rik
        rzik = zz / rik

        pot = cfgs(ib)%mols(im)%alist%apair(trp,4)

        !calculate theta
        costh = rxij * rxik + ryij * ryik + rzij * rzik

        costh = min(costh, 1.0_wp)
        costh = max(costh, -1.0_wp)
        theta = acos(costh)

        !If (Abs(costh) > 1.0_wp) costh = Sign(1.0_wp,costh)
        !theta = Acos(costh)

        eng = 0.0
        gamma = 0.0

        call calc_angle_energy(pot, rij, rik, theta, costh, eng)
        !call calc_angle_energy(pot, rij, rik, theta, eng)

        eang = eang + eng

        imol = cfgs(ib)%mols(im)%mol_label
        mang(imol) = mang(imol) + eng

    end do

end subroutine mol_ang_energy


!> calculate the energy of an angle for moving atom
subroutine atom_ang_energy_update(ib, im, atm, eang, mang)

    use kinds_f90
    use species_module, only : number_of_molecules
    use cell_module
    use angle_module, only : calc_angle_energy

    implicit none

    integer, intent(in) :: im, ib, atm
    real(kind = wp), intent(out) :: eang, mang(number_of_molecules)

    integer :: i, j, k, trp, pot, imol

    real(kind=wp) :: ix, iy, iz, jx, jy, jz, kx, ky, kz, rx, ry, rz
    real(kind=wp) :: rxij, ryij, rzij, rxik, ryik, rzik, rxjk, ryjk, rzjk
    real(kind=wp) :: rij, rik, rjk, costh, theta, eng, gamma, xx, yy, zz

    eang = 0.0
    mang = 0.0


    do trp = 1, cfgs(ib)%mols(im)%alist%nang

        i = cfgs(ib)%mols(im)%alist%apair(trp,1)
        j = cfgs(ib)%mols(im)%alist%apair(trp,2)
        k = cfgs(ib)%mols(im)%alist%apair(trp,3)

        !check to see whether this atom is part of the molecule triplet
        if( atm == i .or. atm == j .or. atm == k ) then

            ! now have suitable types - calc distances
            ix = cfgs(ib)%mols(im)%atms(i)%rpos(1)
            iy = cfgs(ib)%mols(im)%atms(i)%rpos(2)
            iz = cfgs(ib)%mols(im)%atms(i)%rpos(3)

            jx = cfgs(ib)%mols(im)%atms(j)%rpos(1)
            jy = cfgs(ib)%mols(im)%atms(j)%rpos(2)
            jz = cfgs(ib)%mols(im)%atms(j)%rpos(3)

            kx = cfgs(ib)%mols(im)%atms(k)%rpos(1)
            ky = cfgs(ib)%mols(im)%atms(k)%rpos(2)
            kz = cfgs(ib)%mols(im)%atms(k)%rpos(3)

            xx = ix - jx
            yy = iy - jy
            zz = iz - jz

            call pbc_cart_pos_calc(ib, xx, yy, zz)

            rij  = sqrt( xx*xx  +  yy*yy  +  zz*zz )
            rxij = xx / rij
            ryij = yy / rij
            rzij = zz / rij

            xx = ix - kx
            yy = iy - ky
            zz = iz - kz

            call pbc_cart_pos_calc(ib, xx, yy, zz)

            rik  = sqrt( xx*xx  +  yy*yy  +  zz*zz )
            rxik = xx / rik
            ryik = yy / rik
            rzik = zz / rik

            pot = cfgs(ib)%mols(im)%alist%apair(trp,4)

            !calculate theta
            costh = rxij * rxik + ryij * ryik + rzij * rzik

            costh = min(costh, 1.0_wp)
            costh = max(costh, -1.0_wp)
            theta = acos(costh)

            !If (Abs(costh) > 1.0_wp) costh = Sign(1.0_wp,costh)
            !theta = Acos(costh)

            eng = 0.0
            gamma = 0.0

            call calc_angle_energy(pot, rij, rik, theta, costh, eng)
            !call calc_angle_energy(pot, rij, rik, theta, eng)

            eang = eang + eng

            imol = cfgs(ib)%mols(im)%mol_label
            mang(imol) = mang(imol) + eng

        end if

    end do

end subroutine atom_ang_energy_update


subroutine total_inv_energy(ib, einv, minv)


    use kinds_f90
    use species_module, only : number_of_molecules
    use cell_module
    use inversion_module, only : calc_inversion_energy

    implicit none

    integer, intent(in) :: ib
    real(kind = wp), intent(out) :: einv, minv(number_of_molecules)

    integer :: im, i, j, k, l, pot, imol, qd

    real(kind = wp) :: ix, iy, iz, jx, jy, jz, kx, ky, kz, lx, ly, lz, rx, ry, rz
    real(kind = wp) :: rxij, ryij, rzij, rxik, ryik, rzik, rxjk, ryjk, rzjk
    real(kind = wp) :: ril, rxil, ryil, rzil
    real(kind = wp) :: rij, rik, rjk, eng, xx, yy, zz
    real(kind = wp) :: rrij, rrik, rril, rij2, rik2, ril2
    real(kind = wp) :: ubx, uby, ubz, ubn, rub, vbx, vby, vbz, vbn, rvb, wwb
    real(kind = wp) :: ucx, ucy, ucz, ucn, ruc, vcx, vcy, vcz, vcn, rvc, wwc
    real(kind = wp) :: udx, udy, udz, udn, rud, vdx, vdy, vdz, vdn, rvd, wwd
    real(kind = wp) :: cosb, cosc, cosd

    einv = 0.0_wp
    minv = 0.0_wp

    do im = 1, cfgs(ib)%num_mols

        !loop over all inversions in a molecule
        do qd = 1, cfgs(ib)%mols(im)%ilist%ninv

            i = cfgs(ib)%mols(im)%ilist%ivpair(qd,1)
            j = cfgs(ib)%mols(im)%ilist%ivpair(qd,2)
            k = cfgs(ib)%mols(im)%ilist%ivpair(qd,3)
            l = cfgs(ib)%mols(im)%ilist%ivpair(qd,4)
            pot = cfgs(ib)%mols(im)%ilist%ivpair(qd,5)

            ix = cfgs(ib)%mols(im)%atms(i)%rpos(1)
            iy = cfgs(ib)%mols(im)%atms(i)%rpos(2)
            iz = cfgs(ib)%mols(im)%atms(i)%rpos(3)

            jx = cfgs(ib)%mols(im)%atms(j)%rpos(1)
            jy = cfgs(ib)%mols(im)%atms(j)%rpos(2)
            jz = cfgs(ib)%mols(im)%atms(j)%rpos(3)

            kx = cfgs(ib)%mols(im)%atms(k)%rpos(1)
            ky = cfgs(ib)%mols(im)%atms(k)%rpos(2)
            kz = cfgs(ib)%mols(im)%atms(k)%rpos(3)

            lx = cfgs(ib)%mols(im)%atms(l)%rpos(1)
            ly = cfgs(ib)%mols(im)%atms(l)%rpos(2)
            lz = cfgs(ib)%mols(im)%atms(l)%rpos(3)

            xx = jx - ix
            yy = jy - iy
            zz = jz - iz

            call pbc_cart_pos_calc(ib, xx, yy, zz)

            rij  = sqrt( xx*xx  +  yy*yy  +  zz*zz )
            rrij = 1.0_wp / rij

            xx = kx - ix
            yy = ky - iy
            zz = kz - iz

            call pbc_cart_pos_calc(ib, xx, yy, zz)

            rik  = sqrt( xx*xx  +  yy*yy  +  zz*zz )
            rrik = 1.0_wp / rik

            xx = lx - ix
            yy = ly - iy
            zz = lz - iz

            call pbc_cart_pos_calc(ib, xx, yy, zz)

            ril  = sqrt( xx*xx  +  yy*yy  +  zz*zz )
            rril = 1.0_wp / ril

            ubx = rxik * rrik + rxil * rril
            uby = ryik * rrik + ryil * rril
            ubz = rzik * rrik + rzil * rril
            ubn = 1.0_wp / sqrt(ubx**2 + uby**2 + ubz**2)
            ubx = ubn * ubx
            uby = ubn * uby
            ubz = ubn * ubz
            rub = rxij * ubx + ryij * uby + rzij * ubz
 
            vbx = rxik * rrik - rxil * rril
            vby = ryik * rrik - ryil * rril
            vbz = rzik * rrik - rzil * rril
            vbn = 1.0_wp / sqrt(vbx**2 + vby**2 + vbz**2)
            vbx = vbn * vbx
            vby = vbn * vby
            vbz = vbn * vbz
            rvb = rxij * vbx + ryij * vby + rzij * vbz
            wwb = sqrt(rub**2 + rvb**2)
 

            ucx = rxil * rril + rxij * rrij
            ucy = ryil * rril + ryij * rrij
            ucz = rzil * rril + rzij * rrij
            ucn = 1.0_wp / sqrt(ucx**2 + ucy**2 + ucz**2)
            ucx = ucn * ucx
            ucy = ucn * ucy
            ucz = ucn * ucz
            ruc = rxik * ucx + ryik * ucy + rzik * ucz


            vcx = rxil * rril - rxij * rrij
            vcy = ryil * rril - ryij * rrij
            vcz = rzil * rril - rzij * rrij
            vcn = 1.0_wp / sqrt(vcx**2 + vcy**2 + vcz**2)
            vcx = vcn * vcx
            vcy = vcn * vcy
            vcz = vcn * vcz
            rvc = rxik * vcx + ryik * vcy + rzik * vcz
            wwc = sqrt(ruc**2 + rvc**2)

            udx = rxij * rrij + rxik * rrik
            udy = ryij * rrij + ryik * rrik
            udz = rzij * rrij + rzik * rrik
            udn = 1.0_wp / sqrt(udx**2 + udy**2 + udz**2)
            udx = udn * udx
            udy = udn * udy
            udz = udn * udz
            rud = rxil * udx + ryil * udy + rzil * udz

            vdx = rxij * rrij - rxik * rrik
            vdy = ryij * rrij - ryik * rrik
            vdz = rzij * rrij - rzik * rrik
            vdn = 1.0_wp / sqrt(vdx**2 + vdy**2 + vdz**2)
            vdx = vdn * vdx
            vdy = vdn * vdy
            vdz = vdn * vdz
            rvd = rxil * vdx + ryil * vdy + rzil * vdz
            wwd = sqrt(rud**2 + rvd**2)

            ! calculate inversion angle cosines

            cosb = wwb * rrij
            cosc = wwc * rrik
            cosd = wwd * rril

            eng = 0.0_wp
            call calc_inversion_energy(pot, cosb, cosc, cosd, eng)
            einv = einv + eng

        end do

    end do

end subroutine total_inv_energy


subroutine molecule_inv_energy(ib, im, einv, minv)

    use kinds_f90
    use species_module, only : number_of_molecules
    use cell_module
    use inversion_module, only : calc_inversion_energy

    implicit none

    integer, intent(in) :: ib, im
    real(kind = wp), intent(out) :: einv, minv(number_of_molecules)

    integer :: i, j, k, l, pot, imol, qd

    real(kind = wp) :: ix, iy, iz, jx, jy, jz, kx, ky, kz, lx, ly, lz, rx, ry, rz
    real(kind = wp) :: rxij, ryij, rzij, rxik, ryik, rzik, rxjk, ryjk, rzjk
    real(kind = wp) :: ril, rxil, ryil, rzil
    real(kind = wp) :: rij, rik, rjk, eng, xx, yy, zz
    real(kind = wp) :: rrij, rrik, rril, rij2, rik2, ril2
    real(kind = wp) :: ubx, uby, ubz, ubn, rub, vbx, vby, vbz, vbn, rvb, wwb
    real(kind = wp) :: ucx, ucy, ucz, ucn, ruc, vcx, vcy, vcz, vcn, rvc, wwc
    real(kind = wp) :: udx, udy, udz, udn, rud, vdx, vdy, vdz, vdn, rvd, wwd
    real(kind = wp) :: cosb, cosc, cosd

    einv = 0.0_wp
    minv = 0.0_wp

    !loop over all inversions in a molecule
    do qd = 1, cfgs(ib)%mols(im)%ilist%ninv

        i = cfgs(ib)%mols(im)%ilist%ivpair(qd,1)
        j = cfgs(ib)%mols(im)%ilist%ivpair(qd,2)
        k = cfgs(ib)%mols(im)%ilist%ivpair(qd,3)
        l = cfgs(ib)%mols(im)%ilist%ivpair(qd,4)
        pot = cfgs(ib)%mols(im)%ilist%ivpair(qd,5)

        ix = cfgs(ib)%mols(im)%atms(i)%rpos(1)
        iy = cfgs(ib)%mols(im)%atms(i)%rpos(2)
        iz = cfgs(ib)%mols(im)%atms(i)%rpos(3)

        jx = cfgs(ib)%mols(im)%atms(j)%rpos(1)
        jy = cfgs(ib)%mols(im)%atms(j)%rpos(2)
        jz = cfgs(ib)%mols(im)%atms(j)%rpos(3)

        kx = cfgs(ib)%mols(im)%atms(k)%rpos(1)
        ky = cfgs(ib)%mols(im)%atms(k)%rpos(2)
        kz = cfgs(ib)%mols(im)%atms(k)%rpos(3)

        lx = cfgs(ib)%mols(im)%atms(l)%rpos(1)
        ly = cfgs(ib)%mols(im)%atms(l)%rpos(2)
        lz = cfgs(ib)%mols(im)%atms(l)%rpos(3)

        xx = jx - ix
        yy = jy - iy
        zz = jz - iz

        call pbc_cart_pos_calc(ib, xx, yy, zz)

        rij  = sqrt( xx*xx  +  yy*yy  +  zz*zz )
        rrij = 1.0_wp / rij

        xx = kx - ix
        yy = ky - iy
        zz = kz - iz

        call pbc_cart_pos_calc(ib, xx, yy, zz)

        rik  = sqrt( xx*xx  +  yy*yy  +  zz*zz )
        rrik = 1.0_wp / rik

        xx = lx - ix
        yy = ly - iy
        zz = lz - iz

        call pbc_cart_pos_calc(ib, xx, yy, zz)

        ril  = sqrt( xx*xx  +  yy*yy  +  zz*zz )
        rril = 1.0_wp / ril

        ubx = rxik * rrik + rxil * rril
        uby = ryik * rrik + ryil * rril
        ubz = rzik * rrik + rzil * rril
        ubn = 1.0_wp / sqrt(ubx**2 + uby**2 + ubz**2)
        ubx = ubn * ubx
        uby = ubn * uby
        ubz = ubn * ubz
        rub = rxij * ubx + ryij * uby + rzij * ubz

        vbx = rxik * rrik - rxil * rril
        vby = ryik * rrik - ryil * rril
        vbz = rzik * rrik - rzil * rril
        vbn = 1.0_wp / sqrt(vbx**2 + vby**2 + vbz**2)
        vbx = vbn * vbx
        vby = vbn * vby
        vbz = vbn * vbz
        rvb = rxij * vbx + ryij * vby + rzij * vbz
        wwb = sqrt(rub**2 + rvb**2)


        ucx = rxil * rril + rxij * rrij
        ucy = ryil * rril + ryij * rrij
        ucz = rzil * rril + rzij * rrij
        ucn = 1.0_wp / sqrt(ucx**2 + ucy**2 + ucz**2)
        ucx = ucn * ucx
        ucy = ucn * ucy
        ucz = ucn * ucz
        ruc = rxik * ucx + ryik * ucy + rzik * ucz


        vcx = rxil * rril - rxij * rrij
        vcy = ryil * rril - ryij * rrij
        vcz = rzil * rril - rzij * rrij
        vcn = 1.0_wp / sqrt(vcx**2 + vcy**2 + vcz**2)
        vcx = vcn * vcx
        vcy = vcn * vcy
        vcz = vcn * vcz
        rvc = rxik * vcx + ryik * vcy + rzik * vcz
        wwc = sqrt(ruc**2 + rvc**2)

        udx = rxij * rrij + rxik * rrik
        udy = ryij * rrij + ryik * rrik
        udz = rzij * rrij + rzik * rrik
        udn = 1.0_wp / sqrt(udx**2 + udy**2 + udz**2)
        udx = udn * udx
        udy = udn * udy
        udz = udn * udz
        rud = rxil * udx + ryil * udy + rzil * udz

        vdx = rxij * rrij - rxik * rrik
        vdy = ryij * rrij - ryik * rrik
        vdz = rzij * rrij - rzik * rrik
        vdn = 1.0_wp / sqrt(vdx**2 + vdy**2 + vdz**2)
        vdx = vdn * vdx
        vdy = vdn * vdy
        vdz = vdn * vdz
        rvd = rxil * vdx + ryil * vdy + rzil * vdz
        wwd = sqrt(rud**2 + rvd**2)

        ! calculate inversion angle cosines

        cosb = wwb * rrij
        cosc = wwc * rrik
        cosd = wwd * rril

        eng = 0.0_wp
        call calc_inversion_energy(pot, cosb, cosc, cosd, eng)
        einv = einv + eng

    end do

end subroutine molecule_inv_energy


subroutine atom_inv_energy_update(ib, im, atm, einv, minv)

    use kinds_f90
    use species_module, only : number_of_molecules
    use cell_module
    use inversion_module, only : calc_inversion_energy

    implicit none

    integer, intent(in) :: ib, im, atm
    real(kind = wp), intent(out) :: einv, minv(number_of_molecules)

    integer :: i, j, k, l, pot, imol, qd

    real(kind = wp) :: ix, iy, iz, jx, jy, jz, kx, ky, kz, lx, ly, lz, rx, ry, rz
    real(kind = wp) :: rxij, ryij, rzij, rxik, ryik, rzik, rxjk, ryjk, rzjk
    real(kind = wp) :: ril, rxil, ryil, rzil
    real(kind = wp) :: rij, rik, rjk, eng, xx, yy, zz
    real(kind = wp) :: rrij, rrik, rril, rij2, rik2, ril2
    real(kind = wp) :: ubx, uby, ubz, ubn, rub, vbx, vby, vbz, vbn, rvb, wwb
    real(kind = wp) :: ucx, ucy, ucz, ucn, ruc, vcx, vcy, vcz, vcn, rvc, wwc
    real(kind = wp) :: udx, udy, udz, udn, rud, vdx, vdy, vdz, vdn, rvd, wwd
    real(kind = wp) :: cosb, cosc, cosd

    einv = 0.0_wp
    minv = 0.0_wp

    !loop over all inversions in a molecule
    do qd = 1, cfgs(ib)%mols(im)%ilist%ninv

        i = cfgs(ib)%mols(im)%ilist%ivpair(qd,1)
        j = cfgs(ib)%mols(im)%ilist%ivpair(qd,2)
        k = cfgs(ib)%mols(im)%ilist%ivpair(qd,3)
        l = cfgs(ib)%mols(im)%ilist%ivpair(qd,4)

        if (atm == i .or. atm == j .or. atm == k .or. atm == l) then

            pot = cfgs(ib)%mols(im)%ilist%ivpair(qd,5)

            ix = cfgs(ib)%mols(im)%atms(i)%rpos(1)
            iy = cfgs(ib)%mols(im)%atms(i)%rpos(2)
            iz = cfgs(ib)%mols(im)%atms(i)%rpos(3)

            jx = cfgs(ib)%mols(im)%atms(j)%rpos(1)
            jy = cfgs(ib)%mols(im)%atms(j)%rpos(2)
            jz = cfgs(ib)%mols(im)%atms(j)%rpos(3)

            kx = cfgs(ib)%mols(im)%atms(k)%rpos(1)
            ky = cfgs(ib)%mols(im)%atms(k)%rpos(2)
            kz = cfgs(ib)%mols(im)%atms(k)%rpos(3)

            lx = cfgs(ib)%mols(im)%atms(l)%rpos(1)
            ly = cfgs(ib)%mols(im)%atms(l)%rpos(2)
            lz = cfgs(ib)%mols(im)%atms(l)%rpos(3)

            xx = jx - ix
            yy = jy - iy
            zz = jz - iz

            call pbc_cart_pos_calc(ib, xx, yy, zz)

            rij  = sqrt( xx*xx  +  yy*yy  +  zz*zz )
            rrij = 1.0_wp / rij

            xx = kx - ix
            yy = ky - iy
            zz = kz - iz

            call pbc_cart_pos_calc(ib, xx, yy, zz)

            rik  = sqrt( xx*xx  +  yy*yy  +  zz*zz )
            rrik = 1.0_wp / rik

            xx = lx - ix
            yy = ly - iy
            zz = lz - iz

            call pbc_cart_pos_calc(ib, xx, yy, zz)

            ril  = sqrt( xx*xx  +  yy*yy  +  zz*zz )
            rril = 1.0_wp / ril

            ubx = rxik * rrik + rxil * rril
            uby = ryik * rrik + ryil * rril
            ubz = rzik * rrik + rzil * rril
            ubn = 1.0_wp / sqrt(ubx**2 + uby**2 + ubz**2)
            ubx = ubn * ubx
            uby = ubn * uby
            ubz = ubn * ubz
            rub = rxij * ubx + ryij * uby + rzij * ubz
 
            vbx = rxik * rrik - rxil * rril
            vby = ryik * rrik - ryil * rril
            vbz = rzik * rrik - rzil * rril
            vbn = 1.0_wp / sqrt(vbx**2 + vby**2 + vbz**2)
            vbx = vbn * vbx
            vby = vbn * vby
            vbz = vbn * vbz
            rvb = rxij * vbx + ryij * vby + rzij * vbz
            wwb = sqrt(rub**2 + rvb**2)
 

            ucx = rxil * rril + rxij * rrij
            ucy = ryil * rril + ryij * rrij
            ucz = rzil * rril + rzij * rrij
            ucn = 1.0_wp / sqrt(ucx**2 + ucy**2 + ucz**2)
            ucx = ucn * ucx
            ucy = ucn * ucy
            ucz = ucn * ucz
            ruc = rxik * ucx + ryik * ucy + rzik * ucz


            vcx = rxil * rril - rxij * rrij
            vcy = ryil * rril - ryij * rrij
            vcz = rzil * rril - rzij * rrij
            vcn = 1.0_wp / sqrt(vcx**2 + vcy**2 + vcz**2)
            vcx = vcn * vcx
            vcy = vcn * vcy
            vcz = vcn * vcz
            rvc = rxik * vcx + ryik * vcy + rzik * vcz
            wwc = sqrt(ruc**2 + rvc**2)

            udx = rxij * rrij + rxik * rrik
            udy = ryij * rrij + ryik * rrik
            udz = rzij * rrij + rzik * rrik
            udn = 1.0_wp / sqrt(udx**2 + udy**2 + udz**2)
            udx = udn * udx
            udy = udn * udy
            udz = udn * udz
            rud = rxil * udx + ryil * udy + rzil * udz

            vdx = rxij * rrij - rxik * rrik
            vdy = ryij * rrij - ryik * rrik
            vdz = rzij * rrij - rzik * rrik
            vdn = 1.0_wp / sqrt(vdx**2 + vdy**2 + vdz**2)
            vdx = vdn * vdx
            vdy = vdn * vdy
            vdz = vdn * vdz
            rvd = rxil * vdx + ryil * vdy + rzil * vdz
            wwd = sqrt(rud**2 + rvd**2)

            ! calculate inversion angle cosines

            cosb = wwb * rrij
            cosc = wwc * rrik
            cosd = wwd * rril

            eng = 0.0_wp
            call calc_inversion_energy(pot, cosb, cosc, cosd, eng)
            einv = einv + eng

        end if

    end do

end subroutine atom_inv_energy_update


subroutine total_dih_energy(ib, edih, mdih)

    use kinds_f90
    use species_module, only : number_of_molecules
    use cell_module
    use dihedral_module, only : calc_dihedral_energy

    implicit none

    integer, intent(in) :: ib
    real(kind = wp), intent(out) :: edih, mdih(number_of_molecules)

    integer :: im, i, j, k, l, pot, imol, qd

    real(kind = wp) :: rxij, ryij, rzij, rxjk, ryjk, rzjk, rxkl, rykl, rzkl, rrjk, rrbc
    real(kind = wp) :: ix, iy, iz, jx, jy, jz, kx, ky, kz, lx, ly, lz
    real(kind = wp) :: eng, xx, yy, zz, rx, ry, rz
    real(kind = wp) :: pbx, pby, pbz, pb2, rpb1, rpb2
    real(kind = wp) :: pcx, pcy, pcz, pc2, rpc1, rpc2, pbpc
    real(kind = wp) :: cosp, sinp, phi

    edih = 0.0_wp
    mdih = 0.0_wp

    do im = 1, cfgs(ib)%num_mols

        !loop over all inversions in a molecule
        do qd = 1, cfgs(ib)%mols(im)%dlist%ndih

            i = cfgs(ib)%mols(im)%dlist%dipair(qd,1)
            j = cfgs(ib)%mols(im)%dlist%dipair(qd,2)
            k = cfgs(ib)%mols(im)%dlist%dipair(qd,3)
            l = cfgs(ib)%mols(im)%dlist%dipair(qd,4)
            pot = cfgs(ib)%mols(im)%dlist%dipair(qd,5)

            ix = cfgs(ib)%mols(im)%atms(i)%rpos(1)
            iy = cfgs(ib)%mols(im)%atms(i)%rpos(2)
            iz = cfgs(ib)%mols(im)%atms(i)%rpos(3)

            jx = cfgs(ib)%mols(im)%atms(j)%rpos(1)
            jy = cfgs(ib)%mols(im)%atms(j)%rpos(2)
            jz = cfgs(ib)%mols(im)%atms(j)%rpos(3)

            kx = cfgs(ib)%mols(im)%atms(k)%rpos(1)
            ky = cfgs(ib)%mols(im)%atms(k)%rpos(2)
            kz = cfgs(ib)%mols(im)%atms(k)%rpos(3)

            lx = cfgs(ib)%mols(im)%atms(l)%rpos(1)
            ly = cfgs(ib)%mols(im)%atms(l)%rpos(2)
            lz = cfgs(ib)%mols(im)%atms(l)%rpos(3)

            rxij = ix - jx
            ryij = iy - jy
            rzij = iz - jz

            call pbc_cart_pos_calc(ib, rxij, ryij, rzij)

            rxjk = jx - kx
            ryjk = jy - ky
            rzjk = jz - kz

            call pbc_cart_pos_calc(ib, rxjk, ryjk, rzjk)

            rrjk = 1.0_wp / sqrt(rxjk * rxjk + ryjk * ryjk + rzjk * rzjk)

            rxkl = kx - lx
            rykl = ky - ly
            rzkl = kz - lz

            call pbc_cart_pos_calc(ib, rxkl, rykl, rzkl)

            pbx = ryij * rzjk - rzij * ryjk
            pby = rzij * rxjk - rxij * rzjk
            pbz = rxij * ryjk - ryij * rxjk
            pb2 = pbx * pbx + pby * pby + pbz * pbz
            rpb1 = 1.0_wp / sqrt(pb2)
            rpb2 = rpb1 * rpb1

            ! construct second dihedral vector

            pcx = ryjk * rzkl - rzjk * rykl
            pcy = rzjk * rxkl - rxjk * rzkl
            pcz = rxjk * rykl - ryjk * rxkl
            pc2 = pcx * pcx + pcy * pcy + pcz * pcz
            rpc1 = 1.0_wp / sqrt(pc2)
            rpc2 = rpc1 * rpc1

            ! determine dihedral angle 

            pbpc = pbx * pcx + pby * pcy + pbz * pcz
            cosp = pbpc * rpb1 * rpc1
            sinp = (rxjk * (pcy * pbz - pcz * pby) + ryjk * (pbx * pcz - pbz * pcx) +   &
                   rzjk * (pcx * pby - pcy * pbx)) * (rpb1 * rpc1 * rrjk)

            !JG: We now calculate the angle explicitly even if it's not necessary
            phi = atan2(sinp,cosp)

            ! avoid singularity in sinp

            sinp = sign(max(1.0e-8_wp,abs(sinp)),sinp)
            eng = 0.0_wp
            call  calc_dihedral_energy(pot, phi, eng)

            !JG: Since only edih is passed to the calculate dihedral function it is
            ! not possible to account for Coulombic and vdW 1-4 shielding

            edih = edih + eng

        end do

    end do

end subroutine total_dih_energy


!TU: Details for the exclusion/inclusion model:
!TU: * exclude=0 means the pair are in different molecules, and hence VdW and Coulomb interactions 
!TU:   are INCLUDED
!TU: * exclude=1,2,3,.. (small integers) means the pair is bonded with bond type 'i', and they (VdW and
!TU:   Coulomb interactions) are EXCLUDED
!TU: * exclude=-1,-2,-3,.. (small integers) means the pair is bonded with bond type 'i', and the
!TU:   interactions are INCLUDED
!TU: * exclude=MAXINT1 means the pair is within the same molecule, is unbonded, not part of
!TU:   an angle or dihedral, and the interactions are EXCLUDED
!TU: * exclude=-MAXINT1 means the pair is within the same molecule, is unbonded, not part of an angle
!TU:   or dihedral, and the interactions are INCLUDED
!TU: * exclude=MAXINT3 means the pair is within the same molecule, unbonded, is part of an angle (1-3), 
!TU:   is not part of a dihedral, and the interactions are EXCLUDED
!TU: * exclude=-MAXINT3 means the pair is within the same molecule, unbonded, is part of an angle (1-3), 
!TU:   is not part of a dihedral, and the interactions are INCLUDED
!TU: * exclude between (inclusive) EXCDIH+1 and EXCDIH+ndih, where ndih is the number of types of 
!TU:   dihedrals specified in FIELD, means the pair is within the same molecule, unbonded,
!TU:   not part of an angle (1-3), and is a 1-4 in a dihedral (where EXCDIH+i means dihedral 
!TU:   type i), and ther interactions are EXCLUDED
!TU: * exclude between (inclusive) -(EXCDIH+1) and -(EXCDIH+ndih) means the pair is within the same
!TU:   molecule, unbonded, not part of an angle (1-3), is a 1-4 in a dihedral, and the interactions
!TU:   are INCLUDED or PARTIALLY INCLUDED (for VdW or electrostatic)
!TU:
!> set up exclusion list for a replica ('ic')
subroutine set_exclusions(ic)

    use kinds_f90
    use constants_module, only : uout
    use cell_module
    use molecule_type
    use bond_module, only : ntpbond, ltpbond
    use angle_module, only : numang, ltpang
    use dihedral_module, only : numdih, ltpdih
    use inversion_module, only : numinv, ltpinv
    use species_module, only : number_of_molecules
    use comms_mpi_module, only : master, gsum

    implicit none

    integer, intent(in) :: ic

    integer :: i, im, ii, k, kk, fail
    integer :: j, jm, jj, ib, jb, itp
    integer :: bnd, ang, dih, inv, idih !, ldih, jang, kang

    integer, save :: numexctot, numexcoul
    integer, save :: numexcbnd, numincbnd
    integer, save :: numexcang, numincang
    integer, save :: numexcdih, numincdih
    integer, save :: numexcinv, numincinv
    integer, save :: numexmtot, numexmcul
    integer, save :: iter = 0

    logical :: exclude_all

    if( ntpbond < 1 ) return

    !do im = 1, cfgs(ic)%num_mols
    !    if( cfgs(ic)%mols(im)%blist%npairs > 0 ) cfgs(ic)%mols(im)%nlist%exclist = 0
    !end do

    k = 0
    fail = 0

    numexctot = 0
    numexcoul = 0
    numexcbnd = 0
    numincbnd = 0
    numexcang = 0
    numincang = 0
    numexcdih = 0
    numincdih = 0
    
    numexmtot = 0
    numexmcul = 0

    iter = iter+1

    im_loop: &
    do im = 1, cfgs(ic)%num_mols
        
        numexmtot = numexctot
        numexmcul = numexcoul

        exclude_all = ( cfgs(ic)%mols(im)%exc_coul_ints )

        if( cfgs(ic)%mols(im)%blist%npairs < 1 ) then 

            numexctot = numexctot+cfgs(ic)%mols(im)%natom*(cfgs(ic)%mols(im)%natom-1)/2
            numexcoul = numexcoul+cfgs(ic)%mols(im)%natom*(cfgs(ic)%mols(im)%natom-1)/2
            if( .not.exclude_all ) &
                numexcoul = numexcoul-cfgs(ic)%mols(im)%natom*(cfgs(ic)%mols(im)%natom-1)

            goto 101 !cycle im_loop
        end if

        cfgs(ic)%mols(im)%nlist%exclist = 0

        i_loop: &
        do i = 1, cfgs(ic)%mols(im)%natom

            cfgs(ic)%mols(im)%nlist%exclist(i,i) = MAXINT1

            !AB: check for 'exclude' is not enough here as it can be used for molecules without bonds!
            !AB: 'rigid' should make all 'bonded' intra-molecular interactions ignored
            if( cfgs(ic)%mols(im)%rigid_body ) then
                if( exclude_all ) then
                    cfgs(ic)%mols(im)%nlist%exclist(i,:) = MAXINT1
                    numexctot = numexctot+(cfgs(ic)%mols(im)%natom-i)
                    numexcoul = numexcoul+(cfgs(ic)%mols(im)%natom-i)
                    !cycle i_loop
                else
                    numexctot = numexctot+(cfgs(ic)%mols(im)%natom-i)
                    numexcoul = numexcoul-(cfgs(ic)%mols(im)%natom-i)
                end if

                cycle i_loop
            end if

            jm = im ! same molecule

            im_j_loop: & 
            do j = i+1, cfgs(ic)%mols(jm)%natom

                !AB: total count of all putative exclusions (except self-interaction)
                numexctot = numexctot+1

                if( cfgs(ic)%mols(im)%blist%npairs > 0 ) then  !search through bonds

                    !AB: check for direct bonding (1-2) - this overrides any angular etc exclusions/inclusions!
                    do bnd = 1, cfgs(ic)%mols(im)%blist%npairs

                        ib = cfgs(ic)%mols(im)%blist%bpair(bnd,1)
                        jb = cfgs(ic)%mols(im)%blist%bpair(bnd,2)

                        if( (i == ib .and. j == jb) .or. (i == jb .and. j == ib) ) then

                            !this is a bond - so set the exclusion to bond potential number (type)

                            itp = cfgs(ic)%mols(im)%blist%bpair(bnd,3)

                            !AB: negative value is for INCLUDED Coulombic & VdW bonded interactions
                            if( ltpbond(itp) < 0 .and. .not.exclude_all ) then 
                                itp = -itp
                                numincbnd = numincbnd+1
                                numexcoul = numexcoul-1
                            else
                                numexcbnd = numexcbnd+1
                                numexcoul = numexcoul+1
                            end if

                            cfgs(ic)%mols(im)%nlist%exclist(i,j) = itp
                            cfgs(ic)%mols(im)%nlist%exclist(j,i) = itp

                            cycle im_j_loop

                        end if

                    end do

                    if( exclude_all ) then

                        cfgs(ic)%mols(im)%nlist%exclist(i,j) = MAXINT1
                        cfgs(ic)%mols(im)%nlist%exclist(j,i) = MAXINT1

                        numexcoul = numexcoul+1

                        cycle im_j_loop
                    else

                        cfgs(ic)%mols(im)%nlist%exclist(i,j) =-MAXINT1
                        cfgs(ic)%mols(im)%nlist%exclist(j,i) =-MAXINT1

                    end if

                !if(numang > 0 ) then  !search through angles

                    do ang = 1, cfgs(ic)%mols(im)%alist%nang

                        ib = cfgs(ic)%mols(im)%alist%apair(ang,2)
                        jb = cfgs(ic)%mols(im)%alist%apair(ang,3)

                        if( (i == ib .and. j == jb) .or. (i == jb .and. j == ib) ) then

                            itp = cfgs(ic)%mols(im)%alist%apair(ang,4)

                            !AB: negative value is for INCLUDED Coulombic & VdW bonded interactions
                            !AB: make sure the exclusion does not count to bond energy
                            if( ltpang(itp) < 0 ) then 
                                itp =-MAXINT3
                                numincang = numincang+1
                                numexcoul = numexcoul-1
                            else
                                itp = MAXINT3
                                numexcang = numexcang+1
                                numexcoul = numexcoul+1
                            end if

                            !this is not a bond - set exc list to max(potential number)+1
                            cfgs(ic)%mols(im)%nlist%exclist(i,j) = itp
                            cfgs(ic)%mols(im)%nlist%exclist(j,i) = itp

                            cycle im_j_loop

                        end if

                    end do

                !end if !if(numang > 0 ) 

                !if(numdih > 0 ) then  !search through torsions

                    do dih = 1, cfgs(ic)%mols(im)%dlist%ndih

                        ib = cfgs(ic)%mols(im)%dlist%dipair(dih,1)
                        jb = cfgs(ic)%mols(im)%dlist%dipair(dih,4)

                        if( (i == ib .and. j == jb) .or. (i == jb .and. j == ib) ) then

                            itp = cfgs(ic)%mols(im)%dlist%dipair(dih,5)

                            !AB: negative value is for INCLUDED Coulombic & VdW bonded interactions
                            !TU: ...or partially included 1-4 for dihedrals: see 'Details for the exclusion/inclusion
                            !TU: model' comment above
                            !TU: I think the below comment is confusing; what does it mean?
                            !AB: make sure the exclusion does not count to bond energy
                            if( ltpdih(itp) < 0 ) then 
                                itp =-EXCDIH-itp
                                numincdih = numincdih+1
                                numexcoul = numexcoul-1
                            else
                                itp = EXCDIH+itp
                                numexcdih = numexcdih+1
                                numexcoul = numexcoul+1
                            end if

                            !this is not a bond - set exc list to max(potential number)+2
                            cfgs(ic)%mols(im)%nlist%exclist(i,j) = itp
                            cfgs(ic)%mols(im)%nlist%exclist(j,i) = itp

                            cycle im_j_loop

                        end if

                    end do

                !end if !if(numdih > 0 ) 

                !if(numinv > 0 ) then  !search through inversions

                    !AB: exclusions in inversions act on all pairs involved (but likely overriden by bonds)
                    do inv = 1, cfgs(ic)%mols(im)%ilist%ninv

                        itp = cfgs(ic)%mols(im)%ilist%ivpair(inv,5)
                        
                        do ii = 1,3
                            do jj = ii+1,4

                                ib = cfgs(ic)%mols(im)%ilist%ivpair(inv,ii)
                                jb = cfgs(ic)%mols(im)%ilist%ivpair(inv,jj)

                                if( (i == ib .and. j == jb) .or. (i == jb .and. j == ib) ) then

                                    !AB: negative value is for INCLUDED Coulombic & VdW bonded interactions
                                    !AB: make sure the exclusion does not count to bond energy
                                    if( ltpinv(itp) < 0 ) then 
                                        itp =-MAXINT2
                                        numincinv = numincinv+1
                                        numexcoul = numexcoul-1
                                    else
                                        itp = MAXINT2
                                        numexcinv = numexcinv+1
                                        numexcoul = numexcoul+1
                                    end if

                                    !this is not a bond - set exc list to max(potential number)+3
                                    cfgs(ic)%mols(im)%nlist%exclist(i,j) = itp
                                    cfgs(ic)%mols(im)%nlist%exclist(j,i) = itp

                                    cycle im_j_loop

                                end if

                            end do
                        end do

                    end do

                !end if !if(numinv > 0 ) 

                end if !( ntpbond > 0 ) 

            end do im_j_loop ! same molecule

            !AB: the safest way is to call error() from an outer loop by all processes
            !AB: so that every process checks for and reports errors simultaneously
            call gsum(fail)
            if( fail > 0 ) call error(127)

        end do i_loop
        
101     continue

        if( iter < 2 .and. k /= cfgs(ic)%mols(im)%mol_label .and. master ) then

            k = cfgs(ic)%mols(im)%mol_label

            write(uout,"(/,/,25('-'),/,' first time exclusions (mol): ',i10,/,25('-'))") im
            write(uout,"(a,2i10)")'tot (max. & act.) = ',numexctot-numexmtot, numexcoul-numexmcul

        end if

    end do im_loop

    !return

    if( iter < 2 .and. master ) then
    
    !debugging
        if( allocated(cfgs(ic)%mols(1)%nlist%exclist) ) then
            write(uout,*)
            write(uout,*)'exclist 1: ',cfgs(ic)%mols(1)%rigid_body, &
                                       cfgs(ic)%mols(1)%exc_coul_ints
            do i = 1,cfgs(ic)%mols(1)%natom
               write(uout,*)cfgs(ic)%mols(1)%nlist%exclist(i,:)
            end do
        end if
        if( allocated(cfgs(ic)%mols(cfgs(ic)%num_mols)%nlist%exclist) ) then
            write(uout,*)
            write(uout,*)'exclist N: ',cfgs(ic)%mols(cfgs(ic)%num_mols)%rigid_body, &
                                       cfgs(ic)%mols(cfgs(ic)%num_mols)%exc_coul_ints
            do i = 1,cfgs(ic)%mols(cfgs(ic)%num_mols)%natom
               write(uout,*)cfgs(ic)%mols(cfgs(ic)%num_mols)%nlist%exclist(i,:)
            end do
        end if
    !debugging

        write(uout,"(/,/,25('-'),/,' first time exclusions: ',/,25('-'))")

        write(uout,"(a,2i10)")'tot (max. & act.) = ',numexctot, numexcoul
        write(uout,"(a,2i10)")'bnd (exc. & inc.) = ',numexcbnd,-numincbnd
        write(uout,"(a,2i10)")'ang (exc. & inc.) = ',numexcang,-numincang
        write(uout,"(a,2i10)")'dih (exc. & inc.) = ',numexcdih,-numincdih
        write(uout,"(a,2i10)")'inv (exc. & inc.) = ',numexcinv,-numincinv
        write(uout,*)

    end if

end subroutine set_exclusions



!> Checks whether or not the value of 'exclist' for a given pair of atoms
!> corresponds to a 1-4 inclusion (or partial inclusion, for the VdW or 
!> Coulomb), and if it does, returns which dihedral type specified in FIELD the 
!> pair corresponds to, and the VdW and Coulomb factors for inclusion. 
!> Note that the dihedral type returned is '0' if 'exclist' does not 
!> correspond to an included (or partially included) 1-4, and returns 
!> a positive integer corresponding to the dihedral type if it does.
subroutine dih_inclusion(exclist, dihtyp, vdwfactor, coulfactor)

    use dihedral_module, only : numdih, prmdih

    implicit none

        !> Integer value for 'exclist' for pair of atoms under consideration
    integer, intent(in) :: exclist

        !> Dihedral type defined in FIELD; 0 if the pair does not correspond to 1-4 inclusion
    integer, intent(out) :: dihtyp

        !> Factor for VdW inclusion for the dihedral type
    real(wp), intent(out) :: vdwfactor

        !> Factor for electrostatic inclusion for the dihedral type
    real(wp), intent(out) :: coulfactor

    dihtyp = 0 

    if( exclist < -EXCDIH .and. exclist >= -EXCDIH - numdih ) then

        dihtyp = -exclist - EXCDIH

        coulfactor = prmdih(dihtyp,4)
        vdwfactor = prmdih(dihtyp,5)

    end if

end subroutine dih_inclusion


!TU: See 'Details for the exclusion/inclusion model' comment above for details of what 'exclist' means
!> sets up excluded interaction list of an atom when a nbrlist is not being used
subroutine set_atom_exclist(ic, im, i)

    use kinds_f90
    use cell_module
    use bond_module, only : ntpbond, ltpbond
    use angle_module, only : numang, ltpang
    use dihedral_module, only : numdih, ltpdih
    use inversion_module, only : numinv, ltpinv

    implicit none

    integer, intent(in) :: ic, im, i

    integer :: j, ib, jb, itp, ii, jj

    integer :: bnd, ang, dih, inv, idih !, ldih, jang, kang
    
    logical :: exclude_all

    !only do if we have bond potentials
    !if( ntpbond == 0 ) return

    !only if this molecule has some bonds
    if( cfgs(ic)%mols(im)%blist%npairs < 1 ) return

    call clear_atom_exclist(ic, im, i)

    exclude_all = cfgs(ic)%mols(im)%exc_coul_ints

    !AB: check for 'exclude' is not enough here as it can be used for molecules without bonds!
    !AB: 'rigid' should make all 'bonded' intra-molecular interactions ignored
    if( cfgs(ic)%mols(im)%rigid_body ) then
        if( exclude_all ) then
            cfgs(ic)%mols(im)%nlist%exclist(i,:) = MAXINT1
        else
            cfgs(ic)%mols(im)%nlist%exclist(i,i) = MAXINT1
        end if
        
        return
    end if

    atoms: &
    do j = 1, cfgs(ic)%mols(im)%natom

        !AB: check for direct bonding (1-2) - this overrides any angular etc exclusions/inclusions!
        do bnd = 1, cfgs(ic)%mols(im)%blist%npairs

            ib = cfgs(ic)%mols(im)%blist%bpair(bnd,1)
            jb = cfgs(ic)%mols(im)%blist%bpair(bnd,2)

            if( (i == ib .and. j == jb) .or. (i == jb .and. j == ib) ) then

                !this is a bond - so set the exclusion to bond potential number (type)

                itp = cfgs(ic)%mols(im)%blist%bpair(bnd,3)

                !AB: negative value is for bonded pairs INCLUDED in VDW & Coulomb interactions
                if( ltpbond(itp) < 0 .and. .not.exclude_all ) itp = -itp

                cfgs(ic)%mols(im)%nlist%exclist(i,j) = itp
                cfgs(ic)%mols(im)%nlist%exclist(j,i) = itp

                cycle atoms

            end if

        end do

        if( exclude_all ) then 
        !AB: if 'exclude' was found for the molecule
        !AB: all intra-molecular pairs are EXCLUDED from VDW and Coulomb interactions
            cfgs(ic)%mols(im)%nlist%exclist(i,j) = MAXINT1
            cfgs(ic)%mols(im)%nlist%exclist(j,i) = MAXINT1
            cycle atoms
        else
        !AB: otherwise, presume non-bonded pairs are INCLUDED in VDW and Coulomb interactions
            cfgs(ic)%mols(im)%nlist%exclist(i,j) =-MAXINT1
            cfgs(ic)%mols(im)%nlist%exclist(j,i) =-MAXINT1
        end if

        !AB: check for angular (1-3) exclusions/inclusions
        if( cfgs(ic)%mols(im)%alist%nang > 0 ) then  !search through angles

            do ang = 1, cfgs(ic)%mols(im)%alist%nang

                ib = cfgs(ic)%mols(im)%alist%apair(ang,2)
                jb = cfgs(ic)%mols(im)%alist%apair(ang,3)

                if( (i == ib .and. j == jb) .or. (i == jb .and. j == ib) ) then

                    itp = cfgs(ic)%mols(im)%alist%apair(ang,4)

                    !AB: negative value is for INCLUDED Coulombic & VdW bonded interactions
                    !AB: make sure the exclusion does not count to bond energy (hence MAXINT1)
                    if( ltpang(itp) < 0 ) then 
                        itp =-MAXINT3
                    else
                        itp = MAXINT3
                    end if

                    cfgs(ic)%mols(im)%nlist%exclist(i,j) = itp
                    cfgs(ic)%mols(im)%nlist%exclist(j,i) = itp

                    cycle atoms

                end if

            end do

        end if !if(numang > 0 ) 

        !AB: check for dihedral (1-4) exclusions/inclusions
        if( cfgs(ic)%mols(im)%dlist%ndih > 0 ) then  !search through torsions

            do dih = 1, cfgs(ic)%mols(im)%dlist%ndih

                ib = cfgs(ic)%mols(im)%dlist%dipair(dih,1)
                jb = cfgs(ic)%mols(im)%dlist%dipair(dih,4)

                if( (i == ib .and. j == jb) .or. (i == jb .and. j == ib) ) then

                    itp = cfgs(ic)%mols(im)%dlist%dipair(dih,5)


                    !AB: negative value is for INCLUDED Coulombic & VdW bonded interactions
                    !TU: ...or partially included 1-4 for dihedrals
                    !TU: I think the below comment is confusing; what does it mean?
                    !AB: make sure the exclusion does not count to bond energy (hence MAXINT1)
                    if( ltpdih(itp) < 0 ) then 
                        itp =-EXCDIH-itp
                    else
                        itp = EXCDIH+itp
                    end if

                    !this is not a bond - set exc list to max(potential number)+2
                    cfgs(ic)%mols(im)%nlist%exclist(i,j) = itp
                    cfgs(ic)%mols(im)%nlist%exclist(j,i) = itp

                    cycle atoms

                end if

            end do

        end if !if(numdih > 0 ) 

        !AB: check for inversion (1-4) exclusions/inclusions
        !AB: exclusions in inversions act on all pairs involved (but likely overriden by bonds)
        if( cfgs(ic)%mols(im)%ilist%ninv > 0 ) then  !search through inversions

            do inv = 1, cfgs(ic)%mols(im)%ilist%ninv

                itp = cfgs(ic)%mols(im)%ilist%ivpair(inv,5)
                
                do ii = 1,3
                    do jj = ii+1,4

                        ib = cfgs(ic)%mols(im)%ilist%ivpair(inv,ii)
                        jb = cfgs(ic)%mols(im)%ilist%ivpair(inv,jj)

                        if( (i == ib .and. j == jb) .or. (i == jb .and. j == ib) ) then

                            !AB: negative value is for INCLUDED Coulombic & VdW bonded interactions
                            !AB: make sure the exclusion does not count to bond energy
                            if( ltpinv(itp) < 0 ) then 
                                itp =-MAXINT2
                            else
                                itp = MAXINT2
                            end if

                            !this is not a bond - set exc list to max(potential number)+3
                            cfgs(ic)%mols(im)%nlist%exclist(i,j) = itp
                            cfgs(ic)%mols(im)%nlist%exclist(j,i) = itp

                            cycle atoms

                        end if

                    end do
                end do

            end do

        end if !if(numinv > 0 ) 

    end do atoms

end subroutine set_atom_exclist


subroutine clear_atom_exclist(ic, im, ii)

    use kinds_f90
    use cell_module, only : cfgs

    implicit none

    integer, intent(in) :: ic, im, ii

    cfgs(ic)%mols(im)%nlist%exclist(ii,:) = 0

end subroutine clear_atom_exclist


subroutine clear_mol_exclist(ib, im)

    use kinds_f90
    use cell_module

    implicit none

    integer, intent(in) :: im, ib

    integer :: i

    if( cfgs(ib)%mols(im)%blist%npairs > 0 ) then

        do i = 1, cfgs(ib)%mols(im)%natom

            call clear_atom_exclist(ib, im, i)

        end do

    end if

end subroutine clear_mol_exclist


!> sets up excluded interaction list of a molecule when a nbrlist is not being used
subroutine set_molecule_exclist(ic, im)

    use kinds_f90
    use cell_module
    use bond_module, only : ntpbond

    implicit none

    integer, intent(in) :: ic, im

    integer :: i

    if( cfgs(ic)%mols(im)%blist%npairs > 0 ) then

        do i = 1, cfgs(ic)%mols(im)%natom

            call set_atom_exclist(ic, im, i)

        end do

    end if

end subroutine set_molecule_exclist


!> sets up excluded interaction list when a nbrlist is not being used
subroutine set_exclist(ic)

    use kinds_f90
    use cell_module
    use bond_module, only : ntpbond

    implicit none

    integer, intent(in) :: ic

    integer :: i, im

    if( ntpbond > 0 ) then

        do im = 1, cfgs(ic)%num_mols

            if( cfgs(ic)%mols(im)%blist%npairs > 0 ) then

                do i = 1, cfgs(ic)%mols(im)%natom

                    call set_atom_exclist(ic, im, i) 

                end do

            end if

        end do

    end if

end subroutine set_exclist


!***************************************************************************
!   neighbourlist routines
!
!***************************************************************************

!> populate the two-body neighbourlist(s) but not the exclusion list (done separately)
subroutine setnbrlists(ic, cutoff, delta, llist)

    use kinds_f90
    use constants_module, only : uout
    use cell_module
    use molecule_type
    use bond_module, only : ntpbond, ltpbond
    use angle_module, only : numang, ltpang
    use dihedral_module, only : numdih, ltpdih
    use inversion_module, only : numinv, ltpinv
    use comms_mpi_module, only : master, gsum

    implicit none

    integer, intent(in) :: ic
    real(kind = wp), intent(in) :: cutoff, delta
    logical, intent(in) :: llist

    integer :: i, im, neighbors, fail
    integer :: j, jm, bnd, ang, dih, inv
    integer :: ib, jb, itp
    real (kind = wp) :: radius2, xx, yy, zz, xx2, yy2, zz2
    real (kind = wp) :: ax, ay, az, bx, by, bz, r, rsq
    real (kind = wp) :: rx, ry, rz

    integer, save :: numexctot, numexcoul
    integer, save :: numexcbnd, numincbnd
    integer, save :: numexcang, numincang
    integer, save :: numexcdih, numincdih
    integer, save :: numexcinv, numincinv
    integer, save :: iter = 0

    !radius is the cutoff plus the verlet skin
    radius2 = (cutoff + delta)**2

    do im = 1, cfgs(ic)%num_mols

        cfgs(ic)%mols(im)%nlist%numnbrs = 0
         
        cfgs(ic)%mols(im)%nlist%nbr_list = 0

    end do

    fail = 0

    numexctot = 0
    numexcoul = 0
    numexcbnd = 0
    numincbnd = 0
    numexcang = 0
    numincang = 0
    numexcdih = 0
    numincdih = 0

    iter = iter+1

    !linklist method
    if (llist) then

        !TU-: There is nothing here!
        
    else ! verlet method

        im_loop: &
        do im = 1, cfgs(ic)%num_mols

            i_loop: &
            do i = 1, cfgs(ic)%mols(im)%natom

                ax = cfgs(ic)%mols(im)%atms(i)%rpos(1)
                ay = cfgs(ic)%mols(im)%atms(i)%rpos(2)
                az = cfgs(ic)%mols(im)%atms(i)%rpos(3)

                jm = im ! same molecule

                im_j_loop: & ! same molecule
                do j = i+1, cfgs(ic)%mols(jm)%natom

                    bx = cfgs(ic)%mols(jm)%atms(j)%rpos(1)
                    by = cfgs(ic)%mols(jm)%atms(j)%rpos(2)
                    bz = cfgs(ic)%mols(jm)%atms(j)%rpos(3)

                    xx = ax - bx
                    yy = ay - by
                    zz = az - bz

                    call pbc_cart_pos_calc(ic, xx, yy, zz)

                    xx2 = xx*xx
                    if( xx2 > radius2 ) cycle im_j_loop

                    yy2 = yy*yy
                    if( yy2 > radius2 ) cycle im_j_loop

                    zz2 = zz*zz
                    if( zz2 > radius2 ) cycle im_j_loop

                    rsq = xx2 + yy2 + zz2
                    !rsq = xx * xx + yy * yy + zz * zz

                    if( rsq < radius2 ) then

                        numexctot = numexctot+1

                        cfgs(ic)%mols(im)%nlist%numnbrs(i) = cfgs(ic)%mols(im)%nlist%numnbrs(i) + 1
                        cfgs(ic)%mols(jm)%nlist%numnbrs(j) = cfgs(ic)%mols(jm)%nlist%numnbrs(j) + 1

                        !TU-: ******************************************************************
                        !TU-: ** Note that atoms bonded to 'i' are put in the neighbour list. **
                        !TU-: ******************************************************************
                        neighbors = cfgs(ic)%mols(im)%nlist%numnbrs(i)
                        if (neighbors > cfgs(ic)%mols(im)%nlist%max_nonb) then

                            !AB: old way to dump REVCON upon failure
                            !AB: now REVCON includes energy(ies), which are not known/available here
                            !call dump_revcon(1,0,cgfeng(:))

                            fail = 127
                            exit im_loop
                            !exit im_j_loop

                            !AB: it's dangerous to call error() from within loops
                            !AB: a process specific error will result in "dead-end hang-up"
                            !call error(127)
                        end if

                        cfgs(ic)%mols(im)%nlist%nbr_list(1, neighbors, i) = jm
                        cfgs(ic)%mols(im)%nlist%nbr_list(2, neighbors, i) = j

                        neighbors = cfgs(ic)%mols(jm)%nlist%numnbrs(j)
                        if (neighbors > cfgs(ic)%mols(jm)%nlist%max_nonb) then

                            !AB: old way to dump REVCON upon failure
                            !AB: now REVCON includes energy(ies), which are not known/available here
                            !call dump_revcon(1,0,cgfeng(:))

                            fail = 127
                            exit im_loop
                            !exit im_j_loop

                            !AB: it's dangerous to call error() from within loops
                            !AB: a process specific error will result in "dead-end hang-up"
                            !call error(127)
                        end if

                        cfgs(ic)%mols(jm)%nlist%nbr_list(1, neighbors, j) = im
                        cfgs(ic)%mols(jm)%nlist%nbr_list(2, neighbors, j) = i

                    end if !( rsq < radius2 )

                end do im_j_loop ! same molecule

                jm_loop: & ! different molecules (looking up)
                do jm = im + 1, cfgs(ic)%num_mols

                    jm_j_loop: &
                    do j = 1, cfgs(ic)%mols(jm)%natom

                        bx = cfgs(ic)%mols(jm)%atms(j)%rpos(1)
                        by = cfgs(ic)%mols(jm)%atms(j)%rpos(2)
                        bz = cfgs(ic)%mols(jm)%atms(j)%rpos(3)

                        xx = ax - bx
                        yy = ay - by
                        zz = az - bz

                        call pbc_cart_pos_calc(ic, xx, yy, zz)

                        xx2 = xx*xx
                        if( xx2 > radius2 ) cycle jm_j_loop

                        yy2 = yy*yy
                        if( yy2 > radius2 ) cycle jm_j_loop

                        zz2 = zz*zz
                        if( zz2 > radius2 ) cycle jm_j_loop

                        rsq = xx * xx + yy * yy + zz * zz

                        if (rsq < radius2) then

                            cfgs(ic)%mols(im)%nlist%numnbrs(i) = cfgs(ic)%mols(im)%nlist%numnbrs(i) + 1
                            cfgs(ic)%mols(jm)%nlist%numnbrs(j) = cfgs(ic)%mols(jm)%nlist%numnbrs(j) + 1

                            !TU-: ******************************************************************
                            !TU-: ** Note that atoms bonded to 'i' are put in the neighbour list. **
                            !TU-: ******************************************************************
                            neighbors = cfgs(ic)%mols(im)%nlist%numnbrs(i)
                            if (neighbors > cfgs(ic)%mols(im)%nlist%max_nonb) then

                                !AB: old way to dump REVCON upon failure
                                !AB: now REVCON includes energy(ies), which are not known/available here
                                !call dump_revcon(1,0,cgfeng(:))

                                fail = 127
                                exit im_loop
                                !exit jm_loop

                                !AB: it's dangerous to call error() from within loops
                                !AB: a process specific error will result in "dead-end hang-up"
                                !call error(127)
                            end if

                            cfgs(ic)%mols(im)%nlist%nbr_list(1, neighbors, i) = jm
                            cfgs(ic)%mols(im)%nlist%nbr_list(2, neighbors, i) = j

                            neighbors = cfgs(ic)%mols(jm)%nlist%numnbrs(j)
                            if (neighbors > cfgs(ic)%mols(jm)%nlist%max_nonb) then

                                !AB: old way to dump REVCON upon failure
                                !AB: now REVCON includes energy(ies), which are not known/available here
                                !call dump_revcon(1,0,cgfeng(:))

                                fail = 127
                                exit im_loop
                                !exit jm_loop

                                !AB: it's dangerous to call error() from within loops
                                !AB: a process specific error will result in "dead-end hang-up"
                                !call error(127)

                            end if

                            cfgs(ic)%mols(jm)%nlist%nbr_list(1, neighbors, j) = im
                            cfgs(ic)%mols(jm)%nlist%nbr_list(2, neighbors, j) = i

                        end if

                    end do jm_j_loop

                end do jm_loop ! different molecules (looking up)

            end do i_loop

        end do im_loop

        !AB: the safest way is to call error() from an outer loop by all processes
        !AB: so that every process checks for and reports errors simultaneously
        call gsum(fail)
        if( fail > 0 ) call error(127)

    end if

end subroutine setnbrlists




!> Checks that the neighbour list of atom 'i' contains all atoms within the 
!> cut-off radius of 'i', for all 'i'. Note that this procedure does not update the 
!> neighbour lists if an incomplete neighbour list is discovered. However it does 
!> output a warning to OUTPUT.
subroutine checknbrlists(ib, cutoff, iter)

    use kinds_f90
    use cell_module

    implicit none

        !> Box number
    integer, intent(in) :: ib

        !> Cut-off radius for the neighbour list
    real(kind = wp), intent(in) :: cutoff

        !> MC step number (used in information output to OUTPUT)
    integer, intent(in) :: iter


    integer :: im, i

    write(uout,*)
    write(uout,'(1x,a,i4,a,i10,a)') "Checking neighbour lists for box ",ib, " at iteration ",iter,"..."
    write(uout,*)

    do im = 1, cfgs(ib)%num_mols

        do i = 1, cfgs(ib)%mols(im)%natom

           call check_atom_nbrlist(ib, im, i, cutoff, iter)

        end do

     end do

end subroutine checknbrlists



!> calculates the neighbour lists for a molecule but not the exclusion lists (generic routine, non GCMC)
!> it is used in gibbs_moves and mc_moves for updates (note that exclusions do not need to be updated)
subroutine set_mol_nbrlist(ib, im, cutoff, delta)

    use kinds_f90
    use cell_module
    use thbpotential_module, only : numthb

    implicit none

    integer, intent(in) :: im, ib
    real(kind = wp), intent(in) :: cutoff, delta

    integer :: i

    do i = 1, cfgs(ib)%mols(im)%natom

        call atom_nbrlist(ib, im, i, cutoff, delta)

    end do

end subroutine set_mol_nbrlist


!> calculates the neighbour and exclusion lists for a molecule (for GCMC exclusively)
!> also checks for overlaps if atoms are less than a minimum distance
subroutine set_mol_nbrlist_overlap(ib, im, cutoff, delta, mindist, overlap)

    use kinds_f90
    use cell_module
    use thbpotential_module, only : numthb

    implicit none

    integer, intent(in) :: im, ib
    real(kind = wp), intent(in) :: cutoff, delta, mindist
    logical, intent(out) :: overlap

    integer :: i

    overlap = .false.

    do i = 1, cfgs(ib)%mols(im)%natom

        call atom_nbrlist_overlap(ib, im, i, cutoff, delta, mindist, overlap)

        if(overlap) return

    end do

end subroutine set_mol_nbrlist_overlap


subroutine clear_mol_nbrlist(ib, im)

    use kinds_f90
    use cell_module
    use nbrlist_module, only : clear_atomnbrlist

    implicit none

    integer, intent(in) :: im, ib

    integer :: i

    do i = 1, cfgs(ib)%mols(im)%natom

        call clear_atomnbrlist(cfgs(ib)%mols(im)%nlist, i)

    end do

end subroutine clear_mol_nbrlist


!> calculate the neighbour lists for an atom but not the exclusion lists (generic routine, non GCMC)
!> it is used in gibbs_moves and mc_moves for updates (note that exclusions do not need to be updated)
subroutine atom_nbrlist(ic, im , i, cutoff, delta)

    use kinds_f90
    use cell_module
    use bond_module, only : ntpbond, ltpbond
    use angle_module, only : numang, ltpang
    use dihedral_module, only : numdih, ltpdih
    use inversion_module, only : numinv, ltpinv
    use nbrlist_module, only : clear_atomnbrlist
    use comms_mpi_module, only : gsum

    implicit none

    integer, intent(in) :: ic, im, i
    real(kind = wp), intent(in) :: cutoff, delta

    integer :: neighbors, j, jm, bnd, ang, dih, inv
    integer :: ib, jb, itp, fail
    real (kind = wp) :: radius2, xx, yy, zz, xx2, yy2, zz2
    real (kind = wp) :: ax, ay, az, bx, by, bz, r, rsq
    real (kind = wp) :: rx, ry, rz

    !radius is the cutoff plus the verlet skin
    radius2 = (cutoff + delta)**2

    ax = cfgs(ic)%mols(im)%atms(i)%rpos(1)
    ay = cfgs(ic)%mols(im)%atms(i)%rpos(2)
    az = cfgs(ic)%mols(im)%atms(i)%rpos(3)

    call clear_atomnbrlist(cfgs(ic)%mols(im)%nlist, i)

    fail = 0
    neighbors = 0

    jm_loop: &
    do jm = 1, cfgs(ic)%num_mols

        jm_j_loop: &
        do j = 1, cfgs(ic)%mols(jm)%natom

            !AB: atom DOES NOT include itself in its list

            if( cfgs(ic)%mols(im)%glob_no(i) == cfgs(ic)%mols(jm)%glob_no(j) ) &
                cycle jm_j_loop

            bx = cfgs(ic)%mols(jm)%atms(j)%rpos(1)
            by = cfgs(ic)%mols(jm)%atms(j)%rpos(2)
            bz = cfgs(ic)%mols(jm)%atms(j)%rpos(3)

            xx = ax - bx
            yy = ay - by
            zz = az - bz

            call pbc_cart_pos_calc(ic, xx, yy, zz)

            xx2 = xx*xx
            if( xx2 > radius2 ) cycle jm_j_loop

            yy2 = yy*yy
            if( yy2 > radius2 ) cycle jm_j_loop

            zz2 = zz*zz
            if( zz2 > radius2 ) cycle jm_j_loop

            rsq = xx2 + yy2 + zz2
            !rsq = xx * xx + yy * yy + zz * zz

            if (rsq < radius2) then

                neighbors = neighbors + 1

                if (neighbors > cfgs(ic)%mols(im)%nlist%max_nonb) then

                    !AB: old way to dump REVCON upon failure
                    !AB: now REVCON includes energy(ies), which are not known/available here
                    !call dump_revcon(1,0,cgfeng(:))

                    fail = 127
                    exit jm_loop

                    !AB: it's dangerous to call error() from within loops
                    !AB: a process specific error will result in "dead-end hang-up"
                    !call error(127)
                end if

                cfgs(ic)%mols(im)%nlist%nbr_list(1, neighbors, i) = jm
                cfgs(ic)%mols(im)%nlist%nbr_list(2, neighbors, i) = j

            end if

        end do jm_j_loop

    end do jm_loop

    !AB: the safest way is to call error() from an outer loop by all processes
    !AB: so that every process checks for and reports errors simultaneously
    !call gsum(fail)
    if( fail > 0 ) call error(127)

    cfgs(ic)%mols(im)%nlist%numnbrs(i) = neighbors

    call set_atom_rzero(ic, im, i)

end subroutine atom_nbrlist




!> Check that the neighbour list for atom 'i' contains all atoms within the cut-off
!> radius from 'i'.
!> This is intended for debugging and periodic 'safety checks' during the simulation. 
!> Note that this procedure does not update the neighbour list of 'i' if the list is
!> detected to be incomplete. However it does output a warning to OUTPUT.
subroutine check_atom_nbrlist(ic, im, i, cutoff, iter)

    use kinds_f90
    use cell_module    
    use comms_mpi_module, only : master
    use nbrlist_module, only : is_atom_in_list

    implicit none

        !> Box number
    integer, intent(in) :: ic

        !> Molecule number containing the atom whose list we are checking
    integer, intent(in) :: im

        !> Atom number in molecule `im` of the atom whose list we are checking
    integer, intent(in) :: i

        !> Cut-off radius for the potential
    real(kind = wp), intent(in) :: cutoff

        !> MC step number (used in information output to OUTPUT)
    integer, intent(in) :: iter


    integer :: neighbors, j, jm, nbr
    integer :: fail
    real (kind = wp) :: radius2, xx, yy, zz, xx2, yy2, zz2
    real (kind = wp) :: ax, ay, az, bx, by, bz, r, rsq
    real (kind = wp) :: rx, ry, rz

    logical :: found

    radius2 = cutoff**2

    ax = cfgs(ic)%mols(im)%atms(i)%rpos(1)
    ay = cfgs(ic)%mols(im)%atms(i)%rpos(2)
    az = cfgs(ic)%mols(im)%atms(i)%rpos(3)

    fail = 0
    neighbors = 0

    jm_loop: &
    do jm = 1, cfgs(ic)%num_mols

        jm_j_loop: &
        do j = 1, cfgs(ic)%mols(jm)%natom

            !AB: atom DOES NOT include itself in its list

            if( cfgs(ic)%mols(im)%glob_no(i) == cfgs(ic)%mols(jm)%glob_no(j) ) &
                cycle jm_j_loop

            bx = cfgs(ic)%mols(jm)%atms(j)%rpos(1)
            by = cfgs(ic)%mols(jm)%atms(j)%rpos(2)
            bz = cfgs(ic)%mols(jm)%atms(j)%rpos(3)

            xx = ax - bx
            yy = ay - by
            zz = az - bz

            call pbc_cart_pos_calc(ic, xx, yy, zz)

            xx2 = xx*xx
            if( xx2 > radius2 ) cycle jm_j_loop

            yy2 = yy*yy
            if( yy2 > radius2 ) cycle jm_j_loop

            zz2 = zz*zz
            if( zz2 > radius2 ) cycle jm_j_loop

            rsq = xx2 + yy2 + zz2

            if (rsq < radius2) then

                neighbors = neighbors + 1

                if (neighbors > cfgs(ic)%mols(im)%nlist%max_nonb) then

                    fail = 127
                    exit jm_loop

                end if

                !TU: We have found an atom within the cut-off. Now check that it is actually in the
                !TU: neighbour list...

                found = is_atom_in_list(cfgs(ic)%mols(im)%nlist, i, jm, j)

                if( .not. found ) then

                    if( master ) then

                        write(uout,*) 
                        write(uout,'(1x,a,i9,a)') "WARNING: Detected incomplete neighbour list at iteration ", &
                            iter,":"
                        write(uout,'(1x,a,i4,a,i4,a,i4,a,i4,a,i4)') "  Box ",ic,": atom ",j," in mol ",jm, &
                            " is not in list for atom ",i," in mol ",im
                        write(uout,*) "  Atom separation = ", sqrt(rsq)
                        write(uout,*)

                    end if
                   
                end if

            end if

        end do jm_j_loop

    end do jm_loop

    !AB: the safest way is to call error() from an outer loop by all processes
    !AB: so that every process checks for and reports errors simultaneously
    if( fail > 0 ) then

       call error(127)

    end if

end subroutine check_atom_nbrlist




!> calculate the neighbour and exclusion lists for an atom (for GCMC exclusively)
!> also checks for overlaps if atoms are less than a minimum distance
subroutine atom_nbrlist_overlap(ic, im , i, cutoff, delta, mindist, overlap)

    use kinds_f90
    use cell_module
    use bond_module, only : ntpbond, ltpbond
    use angle_module, only : numang, ltpang
    use dihedral_module, only : numdih, ltpdih
    use inversion_module, only : numinv, ltpinv
    use nbrlist_module, only : clear_atomnbrlist
    use comms_mpi_module, only : gsum

    implicit none

    integer, intent(in) :: ic, im, i
    real(kind = wp), intent(in) :: cutoff, delta, mindist
    logical, intent(out) :: overlap

    integer :: j, jm, bnd, ang, dih, inv, ii, jj
    integer :: ib, jb, itp, neighbors, fail
    real (kind = wp) :: radius2, xx, yy, zz, ax, ay, az, bx, by, bz, r, rsq
    real (kind = wp) :: rx, ry, rz, radmin2
    
    logical :: exclude_all

    !radius is the cutoff plus the verlet skin
    radius2 = (cutoff + delta)**2

    radmin2 = mindist * mindist

    overlap = .false.

!AB: skip fictitious atoms if any (set mass to zero) - needs more thorough thinking (for NB and XL)
!    if( cfgs(ic)%mols(im)%atms(i)%mass < FZERO ) return

    ax = cfgs(ic)%mols(im)%atms(i)%rpos(1)
    ay = cfgs(ic)%mols(im)%atms(i)%rpos(2)
    az = cfgs(ic)%mols(im)%atms(i)%rpos(3)

!AB: GCMC does not work with neighborlists because the arrays are not allocated nor initiated
!AB: this is in its turn because a new molecule is created by copying an instance of uniq_mol 
!AB: which does not have NL nor exclusion lists initiated - whence maybe do this here?
!
!    if( cfgs(ic)%mols(im)%nlist%numnbrs(i) < 1 ) then
!        if( max_nonb_nbrs > 0 ) then
!            call alloc_nbrlist_arrays(cfgs(ic)%mols(im)%nlist, cfgs(ic)%mols(im)%mxatom, max_nonb_nbrs)
!        else
!            call alloc_nbrlist_arrays(cfgs(ic)%mols(im)%nlist, cfgs(ic)%mols(im)%mxatom, cfgs(ic)%maxno_of_atoms)
!        end if
!    end if

    call clear_atomnbrlist(cfgs(ic)%mols(im)%nlist, i)

    call clear_atom_exclist(ic, im, i)

    exclude_all = cfgs(ic)%mols(im)%exc_coul_ints

    !AB: check for 'exclude' is not enough here as it can be used for molecules without bonds!
    !AB: 'rigid' should make all 'bonded' intra-molecular interactions ignored
    if( cfgs(ic)%mols(im)%rigid_body ) then
        if( exclude_all ) then
            cfgs(ic)%mols(im)%nlist%exclist(i,:) = MAXINT1
        else
            cfgs(ic)%mols(im)%nlist%exclist(i,i) = MAXINT1
        end if
        
        !do not return yet as neighbourlists must be set too
        !return
    end if

    fail = 0
    neighbors = 0

    jm_loop: &
    do jm = 1, cfgs(ic)%num_mols

        jm_j_loop: &
        do j = 1, cfgs(ic)%mols(jm)%natom

            !AB: atom DOES NOT include itself in its list

            if( cfgs(ic)%mols(im)%glob_no(i) == cfgs(ic)%mols(jm)%glob_no(j) ) &
                cycle jm_j_loop

!AB: skip fictitious atoms if any (set mass to zero) - needs more thorough thinking (for NB and XL)
!            if( cfgs(ic)%mols(jm)%atms(j)%mass < FZERO ) then
!                cfgs(ic)%mols(im)%nlist%exclist(i,j) = MAXINT1
!                cfgs(ic)%mols(im)%nlist%exclist(j,i) = MAXINT1
!                cycle jm_j_loop
!            end if

            !AB: exclusions must not depend on finding pairs within cut-off
            if( im == jm .and. .not.cfgs(ic)%mols(im)%rigid_body ) then

                !if( i == j ) cycle jm_j_loop 
                
                !if( ntpbond > 0 ) then  !search for bond interaction
                if( cfgs(ic)%mols(im)%blist%npairs > 0 ) then  !search for bond interaction

                    !AB: check for direct bonding (1-2) - this overrides any angular etc exclusions/inclusions!
                    do bnd = 1, cfgs(ic)%mols(im)%blist%npairs

                        ib = cfgs(ic)%mols(im)%blist%bpair(bnd,1)
                        jb = cfgs(ic)%mols(im)%blist%bpair(bnd,2)

                        if( (i == ib .and. j == jb) .or. (i == jb .and. j == ib) ) then

                            !this is a bond - so set the exclusion to bond potential number (type)

                            itp = cfgs(ic)%mols(im)%blist%bpair(bnd,3)

                            !AB: negative value is for INCLUDED Coulombic & VdW bonded interactions
                            if( ltpbond(itp) < 0 .and. .not.exclude_all ) itp = -itp

                            !this should be a bond - set exc list to potential number
                            cfgs(ic)%mols(im)%nlist%exclist(i,j) = itp
                            cfgs(ic)%mols(im)%nlist%exclist(j,i) = itp

                            goto 101 !cycle jm_j_loop
                        end if

                    end do

                    if( exclude_all ) then

                        cfgs(ic)%mols(im)%nlist%exclist(i,j) = MAXINT1
                        cfgs(ic)%mols(im)%nlist%exclist(j,i) = MAXINT1

                        goto 101 !cycle jm_j_loop
                    else

                        cfgs(ic)%mols(im)%nlist%exclist(i,j) =-MAXINT1
                        cfgs(ic)%mols(im)%nlist%exclist(j,i) =-MAXINT1

                    end if

                    do ang = 1, cfgs(ic)%mols(im)%alist%nang

                        ib = cfgs(ic)%mols(im)%alist%apair(ang,2)
                        jb = cfgs(ic)%mols(im)%alist%apair(ang,3)

                        if( (i == ib .and. j == jb) .or. (i == jb .and. j == ib) ) then

                            itp = cfgs(ic)%mols(im)%alist%apair(ang,4)

                            !AB: negative value is for INCLUDED Coulombic & VdW bonded interactions
                            !AB: make sure the exclusion does not count to bond energy (hence MAXINT1)
                            if( ltpang(itp) < 0 ) then 
                                itp =-MAXINT3
                            else
                                itp = MAXINT3
                            end if

                            !this is not a bond - set exc list to max(potential number)+1
                            cfgs(ic)%mols(im)%nlist%exclist(i,j) = itp
                            cfgs(ic)%mols(im)%nlist%exclist(j,i) = itp

                            goto 101 !cycle jm_j_loop

                        end if

                    end do

                    !AB: check for dihedral (1-4) exclusions/inclusions

                    do dih = 1, cfgs(ic)%mols(im)%dlist%ndih

                        ib = cfgs(ic)%mols(im)%dlist%dipair(dih,1)
                        jb = cfgs(ic)%mols(im)%dlist%dipair(dih,4)

                        if( (i == ib .and. j == jb) .or. (i == jb .and. j == ib) ) then

                            itp = cfgs(ic)%mols(im)%dlist%dipair(dih,5)

                            !AB: negative value is for INCLUDED Coulombic & VdW bonded interactions
                            !TU: ...or partially included 1-4 for dihedrals
                            !TU: I think the below comment is confusing; what does it mean?
                            !AB: make sure the exclusion does not count to bond energy (hence MAXINT1)
                            if( ltpdih(itp) < 0 ) then 
                                itp =-EXCDIH-itp
                            else
                                itp = EXCDIH+itp
                            end if

                            !this is not a bond - set exc list to max(potential number)+2
                            cfgs(ic)%mols(im)%nlist%exclist(i,j) = itp
                            cfgs(ic)%mols(im)%nlist%exclist(j,i) = itp

                            goto 101 !cycle jm_j_loop

                        end if

                    end do

                    !AB: check for inversion (1-4) exclusions/inclusions

                    !AB: exclusions in inversions act on all pairs involved (but likely overriden by bonds)
                    do inv = 1, cfgs(ic)%mols(im)%ilist%ninv

                        itp = cfgs(ic)%mols(im)%ilist%ivpair(inv,5)
                        
                        do ii = 1,3
                            do jj = ii+1,4

                                ib = cfgs(ic)%mols(im)%ilist%ivpair(inv,ii)
                                jb = cfgs(ic)%mols(im)%ilist%ivpair(inv,jj)

                                if( (i == ib .and. j == jb) .or. (i == jb .and. j == ib) ) then

                                    !AB: negative value is for INCLUDED Coulombic & VdW bonded interactions
                                    !AB: make sure the exclusion does not count to bond energy
                                    if( ltpinv(itp) < 0 ) then 
                                        itp =-MAXINT2
                                    else
                                        itp = MAXINT2
                                    end if

                                    !this is not a bond - set exc list to max(potential number)+3
                                    cfgs(ic)%mols(im)%nlist%exclist(i,j) = itp
                                    cfgs(ic)%mols(im)%nlist%exclist(j,i) = itp

                                    goto 101

                                end if

                            end do
                        end do

                    end do

                end if !( ntpbond > 0 )

            end if !( im == jm )

101         continue

            bx = cfgs(ic)%mols(jm)%atms(j)%rpos(1)
            by = cfgs(ic)%mols(jm)%atms(j)%rpos(2)
            bz = cfgs(ic)%mols(jm)%atms(j)%rpos(3)

            xx = ax - bx
            yy = ay - by
            zz = az - bz

            call pbc_cart_pos_calc(ic, xx, yy, zz)

            rsq = xx * xx + yy * yy + zz * zz

            if (im /= jm .and. rsq < radmin2) then
                overlap = .true.
                return
            end if

            if (rsq < radius2) then

                neighbors = neighbors + 1

                if (neighbors > cfgs(ic)%mols(im)%nlist%max_nonb) then

                    !AB: old way to dump REVCON upon failure
                    !AB: now REVCON includes energy(ies), which are not known/available here
                    !call dump_revcon(1,0,cgfeng(:))

                    fail = 127
                    exit jm_loop

                    !AB: it's dangerous to call error() from within loops
                    !AB: a process specific error will result in "dead-end hang-up"
                    !call error(127)
                end if

                cfgs(ic)%mols(im)%nlist%nbr_list(1, neighbors, i) = jm
                cfgs(ic)%mols(im)%nlist%nbr_list(2, neighbors, i) = j

            end if

        end do jm_j_loop

    end do jm_loop

    !AB: the safest way is to call error() from an outer loop by all processes
    !AB: so that every process checks for and reports errors simultaneously
    !call gsum(fail)
    if( fail > 0 ) call error(127)

    cfgs(ic)%mols(im)%nlist%numnbrs(i) = neighbors

    call set_atom_rzero(ic, im, i)

end subroutine atom_nbrlist_overlap


subroutine atom_overlap(ic, im , i, mindist, overlap)

    use kinds_f90
    use constants_module, only : FZERO
    use cell_module

    implicit none

    integer, intent(in) :: ic, im, i
    real(kind = wp), intent(in) :: mindist
    logical, intent(out) :: overlap

    integer :: j, jm
    real (kind = wp) :: xx, yy, zz, ax, ay, az, bx, by, bz, r, rsq
    real (kind = wp) :: rx, ry, rz, radmin2

    radmin2 = mindist * mindist

    overlap = .false.

    ax = cfgs(ic)%mols(im)%atms(i)%rpos(1)
    ay = cfgs(ic)%mols(im)%atms(i)%rpos(2)
    az = cfgs(ic)%mols(im)%atms(i)%rpos(3)

    do jm = 1, cfgs(ic)%num_mols

!AB: this would be more efficient:
!       if( im == jm ) cycle

        do j = 1, cfgs(ic)%mols(jm)%natom

!AB: skip fictitious atoms if any (set mass to zero)
            if( cfgs(ic)%mols(jm)%atms(j)%mass < FZERO ) cycle

            bx = cfgs(ic)%mols(jm)%atms(j)%rpos(1)
            by = cfgs(ic)%mols(jm)%atms(j)%rpos(2)
            bz = cfgs(ic)%mols(jm)%atms(j)%rpos(3)

            xx = ax - bx
            yy = ay - by
            zz = az - bz

            call pbc_cart_pos_calc(ic, xx, yy, zz)

            rsq = xx * xx + yy * yy + zz * zz

!AB: What about atoms within the same 'atomic field' - ???
!AB: I guess it will never produce an overlap in that case
!AB: It also "duplicates" the overlap check in the VDW energy calculations
!AB: In any case, the check for im == jm (with cycle) 
!AB: outside the atoms loop would be much more efficient

            if (im /= jm .and. rsq < radmin2) then
                overlap = .true.
                return
            end if

        end do

    end do

end subroutine atom_overlap


subroutine molecule_overlap(ic, im , mindist, overlap)

    use kinds_f90
    use constants_module, only : FZERO
    use cell_module

    implicit none

    integer, intent(in) :: ic, im
    real(kind = wp), intent(in) :: mindist
    logical, intent(out) :: overlap

    integer :: i, j, jm
    real (kind = wp) :: xx, yy, zz, ax, ay, az, bx, by, bz, r, rsq
    real (kind = wp) :: rx, ry, rz, radmin2

    radmin2 = mindist * mindist

    overlap = .false.

    do i = 1, cfgs(ic)%mols(im)%natom

!AB: skip fictitious atoms if any (set mass to zero)
        if( cfgs(ic)%mols(im)%atms(i)%mass < FZERO ) cycle

        ax = cfgs(ic)%mols(im)%atms(i)%rpos(1)
        ay = cfgs(ic)%mols(im)%atms(i)%rpos(2)
        az = cfgs(ic)%mols(im)%atms(i)%rpos(3)

        do jm = 1, cfgs(ic)%num_mols

!AB: this would be more efficient:
!           if( im == jm ) cycle

            do j = 1, cfgs(ic)%mols(jm)%natom

!AB: skip fictitious atoms if any (set mass to zero)
                if( cfgs(ic)%mols(jm)%atms(j)%mass < FZERO ) cycle

                bx = cfgs(ic)%mols(jm)%atms(j)%rpos(1)
                by = cfgs(ic)%mols(jm)%atms(j)%rpos(2)
                bz = cfgs(ic)%mols(jm)%atms(j)%rpos(3)

                xx = ax - bx
                yy = ay - by
                zz = az - bz

                call pbc_cart_pos_calc(ic, xx, yy, zz)

                rsq = xx * xx + yy * yy + zz * zz

                if (im /= jm .and. rsq < radmin2) then
                    overlap = .true.
                    return
                end if

            end do

        end do

    end do

end subroutine molecule_overlap


!> checks whether any atom in molecule im has moved more than the verlet shell width
subroutine check_verlet_mol(ib, im, vshell, reset)

    use kinds_f90
    use cell_module
    use thbpotential_module, only : numthb

    implicit none

    integer, intent(in) :: ib, im
    real(kind = wp), intent(in) :: vshell
    logical, intent(inout) :: reset

    integer :: i

    do i = 1, cfgs(ib)%mols(im)%natom
    
        reset = .false.

        call check_verlet(ib, im , i, vshell, reset)
        
        if( reset ) return

    end do

end subroutine check_verlet_mol


!> checks whether an atom has moved more than the verlet shell width
subroutine check_verlet(ib, im , i, delta, reset)

    use kinds_f90
    use cell_module

    implicit none

    integer, intent(in) :: ib, im, i
    real(kind = wp), intent(in) :: delta
    logical, intent(inout) :: reset

    real (kind = wp) :: radius2, xx, yy, zz, ax, ay, az, bx, by, bz
    real (kind = wp) :: rx, ry, rz !, rsq

    !radius is the verlet skin distance squared
    radius2 = delta**2 * 0.25_wp !AB: allow half of the shell for the neighbours to move

    reset = .false.

    ax = cfgs(ib)%mols(im)%atms(i)%rpos(1)
    ay = cfgs(ib)%mols(im)%atms(i)%rpos(2)
    az = cfgs(ib)%mols(im)%atms(i)%rpos(3)

    bx = cfgs(ib)%mols(im)%atms(i)%r_zero(1)
    by = cfgs(ib)%mols(im)%atms(i)%r_zero(2)
    bz = cfgs(ib)%mols(im)%atms(i)%r_zero(3)

    rx = ax - bx
    ry = ay - by
    rz = az - bz

    call pbc_cart_pos_calc(ib, rx, ry, rz)
    
    reset = ( rx*rx+ry*ry+rz*rz > radius2 )

end subroutine check_verlet



!> This procedure updates the neighbour lists of atoms in the system in response to
!> a successful move of atom `j` in molecule `jm`. It is assumed that when this
!> procedure is invoked the neighbour list for atom `j` in molecule `jm` is correct.
!> This procedure is called after successful atom translation moves. It is also
!> called indirectly via `update_mol_verlet_cell` after successful molecule translation
!> or rotation moves.
subroutine update_atm_verlet_cell(ib, vshell, shortcut, jm, j)

    use kinds_f90
    use cell_module
    use thbpotential_module, only : numthb

    implicit none

    integer, intent(in) :: ib, jm, j
    real(kind = wp), intent(in) :: vshell, shortcut
    !logical, intent(inout) :: reset

    integer :: im, i, nb
    logical :: checkself

    do im = 1, cfgs(ib)%num_mols
        do i = 1, cfgs(ib)%mols(im)%natom

            checkself = .true.

            !TU-: This loop checks if atom (jm,j) is in the neighbour list
            !TU-: of (im,i). If it is, then the neighbour list of (im,i)
            !TU-: is updated. Why? This procedure is called in response to (jm,j)
            !TU-: having just been moved. For atoms which have (jm,j) as a neighbour
            !TU-: we must update their lists in case the movement of (jm,j) 
            !TU-: results in (jm,j) no longer being a neighbour of these atoms.
            !TU-:
            !TU-: POSSIBLE IMPROVEMENT: Since this is in response to movement of
            !TU-: (jm,j), instead of updating the whole neighbour list of (im,i)
            !TU-: it may be sufficient to check if (jm,j) is has left the neighbour
            !TU-: list cut-off distance from (im,i), and if it has, remove (jm,j)
            !TU-: from the list of (im,i). This would be faster than updating the
            !TU-: whole list for (im,i). However I could be wrong: What if this
            !TU-: procedure is called during a molecule move: in which case not
            !TU-: just atom (jm,j) has moved. Need to think...
            do nb = 1,cfgs(ib)%mols(im)%nlist%numnbrs(i)

              if(   cfgs(ib)%mols(im)%nlist%nbr_list(1, nb, i) == jm  &
              .and. cfgs(ib)%mols(im)%nlist%nbr_list(2, nb, i) == j ) then

                  call atom_nbrlist(ib, im, i, shortcut, vshell)

                  checkself = .false.
                  cycle
              end if

            end do

            !TU-: This branch is invoked if (jm,j) is NOT in the neighbour list
            !TU-: of (im,i). The do loop here checks if (im,i) is in the neighbour
            !TU-: list of (jm,j), and if it is, updates the neighbour list of
            !TU-: (im,i). Why? This procedure is called in response to (jm,j) having
            !TU-: just been moved. At this point, the neighbour list of (jm,j) is
            !TU-: correct - it has necessarily been updated during the move. Here
            !TU-: we catch atoms for which (im,i) is in the neighbour list of (jm,j)
            !TU-: but for which (jm,j) is not in the neighbour list of (im,i) - 
            !TU-: indicating that the list of (im,i) is out of date. Hence we update
            !TU-: the list of (im,i).
            !TU-:
            !TU-: POSSIBLE IMPROVEMENT: Since this is in response to movement of
            !TU-: (jm,j), instead of updating the whole neighbour list of (im,i)
            !TU-: it may be sufficient to check if (jm,j) is has left the neighbour
            !TU-: list cut-off distance from (im,i), and if it has, remove (jm,j)
            !TU-: from the list of (im,i). This would be faster than updating the
            !TU-: whole list for (im,i). However I could be wrong: What if this
            !TU-: procedure is called during a molecule move: in which case not
            !TU-: just atom (jm,j) has moved. Need to think...
            if( checkself ) then

              do nb = 1,cfgs(ib)%mols(jm)%nlist%numnbrs(j)

                if(   cfgs(ib)%mols(jm)%nlist%nbr_list(1, nb, j) == im  &
                .and. cfgs(ib)%mols(jm)%nlist%nbr_list(2, nb, j) == i ) then

                  call atom_nbrlist(ib, im, i, shortcut, vshell)

                end if

              end do

            end if

        end do
    end do

end subroutine update_atm_verlet_cell




!> This procedure updates the neighbour lists of atoms in the system in response
!> to a successful move of molecule `im` It is assumed that when this procedure is 
!> that the neighbour lists for all atoms in molecule `im` are correct. This
!> procedure is called after a successful molecule translation or rotation
!> move (for molecule `im`). It calls `update_atm_verlet_cell` for all atoms
!> in the molecule. This updates the neighbour lists for all relevant atoms in 
!> the system due to the recent movement of all the atoms in molecule `im`.
subroutine update_mol_verlet_cell(ib, vshell, shortcut, im)

    use kinds_f90
    use cell_module
    use thbpotential_module, only : numthb

    implicit none

    integer, intent(in) :: ib, im
    real(kind = wp), intent(in) :: vshell, shortcut

    integer :: i

    do i = 1, cfgs(ib)%mols(im)%natom

        call update_atm_verlet_cell(ib, vshell, shortcut, im, i)

    end do

end subroutine update_mol_verlet_cell




!> checks whether any atom in replica ib has moved more than the verlet shell width
!> and resets atoms' NL if necessary
subroutine update_verlet_cell(ib, vshell, shortcut, reset)

    use kinds_f90
    use cell_module
    use thbpotential_module, only : numthb

    implicit none

    integer, intent(in) :: ib
    real(kind = wp), intent(in) :: vshell, shortcut
    logical, intent(inout) :: reset

    integer :: im, i
    logical :: resat

    reset = .false.

    do im = 1, cfgs(ib)%num_mols

        do i = 1, cfgs(ib)%mols(im)%natom

            resat = .false.

            call update_verlet_atm(ib, im , i, vshell, shortcut, resat)

            reset = ( reset .or. resat )

        end do

    end do

end subroutine update_verlet_cell


!> checks whether any atom in molecule im has moved more than the verlet shell width
!> and resets atoms' NL if necessary
subroutine update_verlet_mol(ib, im, vshell, shortcut, reset)

    use kinds_f90
    use cell_module
    use thbpotential_module, only : numthb

    implicit none

    integer, intent(in) :: ib, im
    real(kind = wp), intent(in) :: vshell, shortcut
    logical, intent(inout) :: reset

    integer :: i
    logical :: resat

    reset = .false.

    do i = 1, cfgs(ib)%mols(im)%natom

        resat = .false.

        call update_verlet_atm(ib, im , i, vshell, shortcut, resat)

        reset = ( reset .or. resat )

    end do

end subroutine update_verlet_mol


!> checks whether an atom has moved more than the verlet shell width
!> and resets the atom's neighbour list if necessary
subroutine update_verlet_atm(ib, im , i, vshell, shortcut, reset)

    use kinds_f90
    use cell_module

    implicit none

    integer, intent(in) :: ib, im, i
    real(kind = wp), intent(in) :: vshell, shortcut
    logical, intent(inout) :: reset

    real (kind = wp) :: radius2, xx, yy, zz, ax, ay, az, bx, by, bz
    real (kind = wp) :: rx, ry, rz !, rsq

    !radius is the verlet skin distance squared
    radius2 = vshell**2 * 0.25_wp !AB: allow half of the shell for the neighbours to move

    reset = .false.

    ax = cfgs(ib)%mols(im)%atms(i)%rpos(1)
    ay = cfgs(ib)%mols(im)%atms(i)%rpos(2)
    az = cfgs(ib)%mols(im)%atms(i)%rpos(3)

    bx = cfgs(ib)%mols(im)%atms(i)%r_zero(1)
    by = cfgs(ib)%mols(im)%atms(i)%r_zero(2)
    bz = cfgs(ib)%mols(im)%atms(i)%r_zero(3)

    rx = ax - bx
    ry = ay - by
    rz = az - bz

    call pbc_cart_pos_calc(ib, rx, ry, rz)
    
    reset = ( rx*rx+ry*ry+rz*rz > radius2 )

    if( reset ) call atom_nbrlist(ib, im, i, shortcut, vshell)

end subroutine update_verlet_atm


subroutine set_rzero(ib)

    use cell_module
    implicit none

    integer, intent(in) :: ib

    integer :: im, i

    do im = 1, cfgs(ib)%num_mols

        do i = 1, cfgs(ib)%mols(im)%natom

            call set_atom_rzero(ib, im, i)

        end do

    end do

end subroutine set_rzero


subroutine set_mol_rzero(ib, im)

    use cell_module

    implicit none

    integer, intent(in) :: ib, im

    integer :: i

    do i = 1, cfgs(ib)%mols(im)%natom

        call set_atom_rzero(ib, im, i)

    end do

end subroutine set_mol_rzero


subroutine set_atom_rzero(ib, im, i)

    use kinds_f90

    use cell_module

    implicit none

    integer, intent(in) :: ib, im ,i

    if(.not.allocated(cfgs(ib)%mols(im)%atms(i)%r_zero)) return

    cfgs(ib)%mols(im)%atms(i)%r_zero(1) = cfgs(ib)%mols(im)%atms(i)%rpos(1)
    cfgs(ib)%mols(im)%atms(i)%r_zero(2) = cfgs(ib)%mols(im)%atms(i)%rpos(2)
    cfgs(ib)%mols(im)%atms(i)%r_zero(3) = cfgs(ib)%mols(im)%atms(i)%rpos(3)

end subroutine set_atom_rzero


!****************************************************************************
! controls setting up of nonbonded three-body interaction list
!****************************************************************************/
subroutine set_thblist(ib)

    use cell_module

    implicit none

    integer, intent(in) :: ib

    integer :: i, im

    do im = 1, cfgs(ib)%num_mols

        do i = 1, cfgs(ib)%mols(im)%natom

            call atom_thblist(ib, im, i)

        end do

    end do
 
end subroutine set_thblist


!***************************************************************************
! calculates the three body interaction list - the function does not the
! neigbourlist to identify triplets
!****************************************************************************/
subroutine atom_thblist(ib, im, i)

    use cell_module
    use kinds_f90
    use thbpotential_module
    use constants_module, only : uout
    use thblist_module, only : zero_atomthblist
    use comms_mpi_module, only : gsum

    implicit none

    integer, intent(in) :: ib, im, i

    integer :: ithb, ltypei, ltypej, ltypek, j, jm, k, km, kstart
    integer :: nthb, fail

    real(kind = wp) :: ix, iy, iz, jx, jy, jz, kx, ky, kz, xx, yy, zz, rx, ry, rz
    real(kind = wp) :: rxij, ryij, rzij, rxik, ryik, rzik, rxjk, ryjk, rzjk
    real(kind = wp) :: rij, rik, rjk

    call zero_atomthblist(i, cfgs(ib)%mols(im)%tlist)

    fail = 0

    nthb = 0

    !loop over three body potentials
    do ithb = 1, numthb

        ltypei = cfgs(ib)%mols(im)%atms(i)%atlabel

        !check that potential and atom types are the same
        if (ltypei == thbtypes(ithb,1)) then

            ! start search for ij pair 
            jm_loop: &
            do jm = 1, cfgs(ib)%num_mols

                do j = 1, cfgs(ib)%mols(jm)%natom

                    ltypej = cfgs(ib)%mols(jm)%atms(j)%atlabel

                    !TU: It is possible to have (jm,j) be same atom as (im,i); the
                    !TU: below line stops this
                    if(jm==im .and. j==i) cycle

                    !check potential and atom types for j match
                    if (ltypej == thbtypes(ithb,2)) then

                        !search for ik pair (kk > jj)
                        do km = jm, cfgs(ib)%num_mols

                            kstart = 1
                            if (jm == km) kstart = j + 1

                            do k = kstart, cfgs(ib)%mols(km)%natom

                                ltypek = cfgs(ib)%mols(km)%atms(k)%atlabel

                                !TU: The above 'if(jm == km)' conditional ensures that k/=j, but does not
                                !TU: ensure that k/=i. Hence the next line
                                if(km==im .and. k==i) cycle

                                !check potential and atom types for k match
                                ! and the global no of j must be greater than that for k
                                !TU: The above claim about global index of j being > k is surely wrong?!
                                if (ltypek == thbtypes(ithb, 3)) then

                                    ! now have suitable types - calc distances
                                    ix = cfgs(ib)%mols(im)%atms(i)%rpos(1)
                                    iy = cfgs(ib)%mols(im)%atms(i)%rpos(2)
                                    iz = cfgs(ib)%mols(im)%atms(i)%rpos(3)

                                    jx = cfgs(ib)%mols(jm)%atms(j)%rpos(1)
                                    jy = cfgs(ib)%mols(jm)%atms(j)%rpos(2)
                                    jz = cfgs(ib)%mols(jm)%atms(j)%rpos(3)

                                    kx = cfgs(ib)%mols(km)%atms(k)%rpos(1)
                                    ky = cfgs(ib)%mols(km)%atms(k)%rpos(2)
                                    kz = cfgs(ib)%mols(km)%atms(k)%rpos(3)
                     
                                    rx = ix - jx
                                    ry = iy - jy
                                    rz = iz - jz

                                    call pbc_cart_pos_calc(ib, rx, ry, rz)

                                    rij = sqrt(rx * rx + ry * ry + rz * rz)

                                    rx = ix - kx
                                    ry = iy - ky
                                    rz = iz - kz

                                    call pbc_cart_pos_calc(ib, rx, ry, rz)
                                    
                                    rik = sqrt(rx * rx + ry * ry + rz * rz)

                                    rx = jx - kx
                                    ry = jy - ky
                                    rz = jz - kz

                                    call pbc_cart_pos_calc(ib, rx, ry, rz)

                                    rjk = sqrt(rx * rx + ry * ry + rz * rz)

                                    if(rij <= prmthb(ithb,3) .and. rik <= prmthb(ithb,4) .and. rjk <= prmthb(ithb,5)) then

                                        nthb = nthb + 1

                                        if( nthb > cfgs(ib)%mols(im)%tlist%maxtrip ) then
                                    
                                           fail = 133
                                           exit jm_loop

                                           !AB: it's dangerous to call error() from within loops
                                           !AB: a process specific error will result in "dead-end hang-up"
                                           !call error(133)
                                        end if

                                        cfgs(ib)%mols(im)%tlist%thb_triplets(i, nthb, 1) = ithb

                                        !use global no rather than nbrlist as the later may change
                                        cfgs(ib)%mols(im)%tlist%thb_triplets(i, nthb, 2) = jm
                                        cfgs(ib)%mols(im)%tlist%thb_triplets(i, nthb, 3) = j
                                        cfgs(ib)%mols(im)%tlist%thb_triplets(i, nthb, 4) = km
                                        cfgs(ib)%mols(im)%tlist%thb_triplets(i, nthb, 5) = k

                                    end if

                                end if

                            end do

                        end do

                    end if

                end do

            end do jm_loop

        end if

        !AB: the safest way is to call error() from an outer loop by all processes
        !AB: so that every process checks for and reports errors simultaneously
        call gsum(fail)
        if( fail > 0 ) call error(133)

    end do ! end loop over potentials

    cfgs(ib)%mols(im)%tlist%numtrip(i) = nthb

end subroutine atom_thblist


!> debugging routine for periodic checks on consistency of energy updates
!AB: the main structure taken from volume move routine
!subroutine check_energy(ib, job, beta, energytot) !, iter)
subroutine check_energy(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, energyvdw, & 
                        energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                        emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, emolvdwn, emolvdwb, &
                        emolthree, emolpair, emolang, emolfour, emolmany, emolext, volume)

    use kinds_f90
    use constants_module, only : uout
    use control_type
    use cell_module
    use species_module
    use comms_mpi_module, only : master
    use parallel_loop_module, only : idgrp
    use thbpotential_module, only : numthb
    
        !> configuration id/index
    integer, intent(in) :: ib

        ! MC step (iteration)
    !integer, intent(in) :: iter

        !> simulation control structure
    type(control), intent(inout) :: job

        !> self-explanatory
    real(kind = wp), intent(inout) :: beta, extpress!, energytot(:)

    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), energyfour(:), &
                      energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                      energyext(:), energymfa(:), volume(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                      emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                      emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                      emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                      emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                      emolmany(nconfigs,number_of_molecules), emolrealself(nconfigs,number_of_molecules), &
                      emolrealcrct(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules)

    real(kind=wp) :: oldtotal, oldenth, oldvdw, oldreal, oldrcp, oldpair, oldthree, & 
                     oldfour, oldmany, oldext, oldmfa, oldang, oldvir, oldvolm, deltav

    real(kind=wp) :: newtotal, newenth, newvdw, newreal, newrcp, newpair, newthree, & 
                     newfour, newmany, newext, newmfa, newang, newvir, norm

    !temp vectors for molecular energies
    real(kind = wp) :: nrealnonb(number_of_molecules), nrealbond(number_of_molecules), nrcp(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules), npair(number_of_molecules), &
                       nthree(number_of_molecules), nang(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules), nself(number_of_molecules), ncrct(number_of_molecules), &
                       next(number_of_molecules)

    logical :: reset = .false.
    
    !integer :: iproc, iout

    oldvolm  = cfgs(ib)%vec%volume
    oldtotal = energytot(ib)
    oldenth  = oldtotal + extpress * oldvolm

    oldvdw   = energyvdw(ib)
    oldreal  = energyreal(ib)
    oldrcp   = energyrcp(ib)
    oldpair  = energypair(ib)
    oldthree = energythree(ib)
    oldfour  = energyfour(ib)
    oldmany  = energymany(ib)
    oldext   = energyext(ib)
    oldmfa   = energymfa(ib)
    oldang   = energyang(ib)
    oldvir   = virialtot(ib)

    newenth  = 0.0_wp
    newvdw   = 0.0_wp
    newreal  = 0.0_wp
    newrcp   = 0.0_wp
    newpair  = 0.0_wp
    newthree = 0.0_wp
    newfour  = 0.0_wp
    newmany  = 0.0_wp
    newext   = 0.0_wp
    newmfa   = 0.0_wp
    newang   = 0.0_wp
    newvir   = 0.0_wp

    if(job%coultype(ib) > 0 .or. job%coultype(ib) < -1) then

        call setup_ewald(ib, job)

        if(job%coultype(ib) == 1) call total_recip(ib, newrcp, nrcp, newvir)

    endif

    !TU: Update the three-body lists 
    if(numthb > 0) call set_thblist(ib)
    
    if(job%uselist) then

        !call update_verlet_cell(ib, job%verletshell, job%shortrangecut, reset)

        call setnbrlists(ib, job%shortrangecut, job%verletshell, job%linklist)
        if( reset ) call set_rzero(ib)

        call total_energy(ib, job%coultype(ib), job%vdw_cut, job%shortrangecut, job%dielec, &
                          newreal, newvdw, newthree, newpair, newang, newfour, newmany, newext, newmfa, &
                          newvir, nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb, &
                          npair, nthree, nang, nfour, nmany, next)

    else

        call total_energy_nolist(ib, job%coultype(ib), job%vdw_cut, job%shortrangecut, job%dielec, &
                          newreal, newvdw, newthree, newpair, newang, newfour, newmany, newext, newmfa, &
                          newvir, nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb, &
                          npair, nthree, nang, nfour, nmany, next)

    end if

    if( job%usecavitybias ) then

        call create_cavitylist(ib, job%cavity_radius)

    endif

    newtotal = newrcp + newreal + newvdw + newthree + newpair + newang + newfour + newmany + newext + newmfa
    newenth  = newtotal + extpress * volume(ib)

    deltav = newtotal - oldtotal

    if( .not. master ) return

    !AB: only master(s) open and write into files

    norm = 1.0_wp
 
    if( abs(newtotal) > job%checkenergyprec ) then 

        norm = norm/newtotal

    else if( abs(deltav) > job%checkenergyprec ) then

        norm = 1.0e10_wp

    end if

    write(uout,*)

    write(uout,'(1x,a,i4,a,i4,a,4(e12.5,a))') "Workgroup ", idgrp,", box ", ib, &
          " check: U_recalc - U_accum = ", deltav, " ", beta*deltav, " ", &
          beta*deltav/cfgs(ib)%number_of_atoms, " ",  deltav*norm, " (internal, kT, kT/atom, dU/U)"

    !write(uout,'(1x,a,i4,a,i4,a,e12.5,a,e12.5,a,4(e12.5,a))') "Workgroup ", idgrp,", box ", ib, &
    !     " : U_recalc - U_accum = ", newtotal," - ", oldtotal, " = ", deltav, " ", beta*deltav, " ", & 
    !     beta*deltav/cfgs(ib)%number_of_atoms, " ",  deltav/newtotal, " (internal, kT, kT/atom, dU/U)"

    write(uout,*)

    write(uout,*) & 
    "----------------------------------------------------------------------------------------------------"

    write(uout,*)

    !if( abs(beta*deltav) > 1.0e-5_wp ) then
    !if( abs(newtotal) > 1.0e-10_wp .and. abs(deltav) > 1.0e-10_wp ) then

    if( abs(deltav*norm) > job%checkenergyprec .and. abs(beta*deltav) > job%checkenergyprec ) then

        write(uout,*)"*** WARNING *** Both absolute difference (kT) and relative error are too high..."

        write(uout,*)"*** WARNING *** Check Hamiltonian contributions:"

        write(uout,*)

        if( abs(newvdw) > job%checkenergyprec ) then

          deltav = newvdw - oldvdw

          write(uout,'(a,i4,a,e12.5,a,e12.5,a,3(e12.5,a))') "check_energy(", idgrp, "): delta_U(VdW)   = ", & 
                newvdw," - ", oldvdw, " = ", deltav, " ", beta*deltav, " ", & 
                deltav/newvdw, " (internal, kT, dU/U)"

        end if

        if( abs(newreal) > job%checkenergyprec ) then

          deltav = newreal - oldreal

          write(uout,'(a,i4,a,e12.5,a,e12.5,a,3(e12.5,a))') "check_energy(", idgrp, "): delta_U(real)  = ", & 
                newreal," - ", oldreal, " = ", deltav, " ", beta*deltav, " ", & 
                deltav/newreal, " (internal, kT, dU/U)"

        end if

        if( abs(newrcp) > job%checkenergyprec ) then

          deltav = newrcp - oldrcp

          write(uout,'(a,i4,a,e12.5,a,e12.5,a,3(e12.5,a))') "check_energy(", idgrp, "): delta_U(recip) = ", & 
                newrcp," - ", oldrcp, " = ", deltav, " ", beta*deltav, " ", & 
                deltav/newrcp, " (internal, kT, dU/U)"

        end if

        if( abs(newpair) > job%checkenergyprec ) then

          deltav = newpair - oldpair

          write(uout,'(a,i4,a,e12.5,a,e12.5,a,3(e12.5,a))') "check_energy(", idgrp, "): delta_U(pair)  = ", & 
                newpair," - ", oldpair, " = ", deltav, " ", beta*deltav, " ", & 
                deltav/newpair, " (internal, kT, dU/U)"

        end if

        if( abs(newthree) > job%checkenergyprec ) then

          deltav = newthree - oldthree

          write(uout,'(a,i4,a,e12.5,a,e12.5,a,3(e12.5,a))') "check_energy(", idgrp, "): delta_U(three) = ", & 
                newthree," - ", oldthree, " = ", deltav, " ", beta*deltav, " ", & 
                deltav/newthree, " (internal, kT, dU/U)"

        end if

        if( abs(newfour) > job%checkenergyprec ) then

          deltav = newfour - oldfour

          write(uout,'(a,i4,a,e12.5,a,e12.5,a,3(e12.5,a))') "check_energy(", idgrp, "): delta_U(four)  = ", & 
                newfour," - ", oldfour, " = ", deltav, " ", beta*deltav, " ", & 
                deltav/newfour, " (internal, kT, dU/U)"

        end if

        if( abs(newmany) > job%checkenergyprec ) then

          deltav = newmany - oldmany

          write(uout,'(a,i4,a,e12.5,a,e12.5,a,3(e12.5,a))') "check_energy(", idgrp, "): delta_U(many)  = ", & 
                newmany," - ", oldmany, " = ", deltav, " ", beta*deltav, " ", & 
                deltav/newmany, " (internal, kT, dU/U)"

        end if

        if( abs(newext) > job%checkenergyprec ) then

          deltav = newext - oldext

          write(uout,'(a,i4,a,e12.5,a,e12.5,a,3(e12.5,a))') "check_energy(", idgrp, "): delta_U(ext)   = ", & 
                newext," - ", oldext, " = ", deltav, " ", beta*deltav, " ", & 
                deltav/newext, " (internal, kT, dU/U)"

        end if

        if( abs(newmfa) > job%checkenergyprec ) then

          deltav = newmfa - oldmfa

          write(uout,'(a,i4,a,e12.5,a,e12.5,a,3(e12.5,a))') "check_energy(", idgrp, "): delta_U(mfa)   = ", & 
                newmfa," - ", oldmfa, " = ", deltav, " ", beta*deltav, " ", & 
                deltav/newmfa, " (internal, kT, dU/U)"

        end if

        if( abs(newang) > job%checkenergyprec ) then

          deltav = newang - oldang

          write(uout,'(a,i4,a,e12.5,a,e12.5,a,3(e12.5,a))') "check_energy(", idgrp, "): delta_U(angle) = ", & 
                newang," - ", oldang, " = ", deltav, " ", beta*deltav, " ", & 
                deltav/newang, " (internal, kT, dU/U)"

        end if

        write(uout,*)

        write(uout,*) & 
        "----------------------------------------------------------------------------------------------------"

        write(uout,*)

    end if

    deltav = newvir - oldvir

!AB: commented out checks for the virial, as its calculation is incomplete (i.e. pressure is not calculated)
!
!    if( abs(newvir) > 1.0e-10_wp .and. abs(deltav) > 1.0e-10_wp ) then
!
!        write(uout,'(a,i4,a,e12.5,a,e12.5,a,3(e12.5,a))') "check_energy(", idgrp, "): delta_V(vir)   = ", & 
!              newvir," - ", oldvir, " = ", deltav, " ", beta*deltav, " ", & 
!              deltav/newvir, " (internal, kT, dV/V)"
!
!        write(uout,*)
!
!        write(uout,*) & 
!        "----------------------------------------------------------------------------------------------------"
!
!        write(uout,*)
!
!    end if

end subroutine check_energy


end module
