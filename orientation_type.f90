!***************************************************************************
!   Copyright (C) 2015 by T L Underwood                                    *
!   t.l.underwood@bath.ac.uk                                               *
!                                                                          *
!***************************************************************************

!> Module containing derived type containing information orientation
!> of a molecular species
module orientation_spec_type

    use kinds_f90

    implicit none


!> Derived type specifying t
type orientation_rule

        !> Integer label representing the 'symmetry' of the molecule
    integer :: sym

        !> Matrix containing basis vectors for the molecule's local reference frame
        !> - which defines its orientation. This is allocatable: it may not be
        !> necessary to specify all 3 vectors for, e.g., linear molecules.
    real(kind=wp), allocatable :: basis(:,:)

        !> Array containing the rule specifiying how the orientation is to be calculated
    integer, allocatable :: rule(:,:)

end type orientation


end module orientation_type
