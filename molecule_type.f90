! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module molecule_type

    use kinds_f90
    use bondlist_type
    use anglist_type
    use invlist_type
    use dihlist_type
    use atom_type
    use nbrlist_type
    use thblist_type

    implicit none


type molecule

        !> the number of atoms in this molecule
    integer :: natom = 0

        !> the maximum no of atoms possible in this molecule duiring the simulation
    integer :: mxatom = 0

        !> mass of the molecule
    real (kind = wp) :: mass

        !> mass of the molecule
    real (kind = wp) :: charge

        !> matrix containing centre of mass of molecule
    real (kind = wp) :: rcom(3)

        !> stored centre of mass of molecule (for back-up before MC moves)
    real (kind = wp) :: store_rcom(3)

        !> the name of molecules
    character*8 :: molname

        !> integer label for molecule type
    integer :: mol_label

        !> identifier of COM the molecule belongs to in FED calculation
    integer ::   idcom

        !> flag for molecule bearing any charge (check for the charges only once)
    logical ::   has_charge = .false.

        !> flag indicating that phase factors have been allocated
    !logical ::   has_phase = .false.

        !> flag indicating that atom densities (smden) have been allocated
    logical ::   has_smden = .false.

        !> flag to indicate molecule is an atomic field without internal structure
    logical ::   is_field = .false.

        !> flag to indicate molecule is being treated as a rigid molecule
    logical ::   rigid_body = .false.

        !> flag to exclude all coulomb interactions between atoms in molecule
    logical ::   exc_coul_ints = .false.

        !> store x centre of mass of a molecule
    !real (kind = wp) ::   store_xcom

        !> store y centre of mass of a molecule
    !real (kind = wp) ::   store_ycom

        !> store z centre of mass of a molecule
    !real (kind = wp) ::   store_zcom

        !> the atom number in a global environment
    integer, dimension(:), allocatable :: glob_no

        !> the atom data ie positions, mass etc
    type (atom), dimension(:), allocatable :: atms

        !> the neighbourlist
    type(nbrlist)   :: nlist

        !> the three body neighbourlist
    type(thblist)   :: tlist

        !> the bond list
    type (bondlist)  :: blist

        !> list of angles in molecule
    type (anglist)   :: alist

        !> list of inversions in molecule
    type (invlist)   :: ilist

        !> list of dihedral angles
    type (dihlist)   :: dlist

        !> atom cos phase factrs
    !real(kind = wp), allocatable, dimension(:,:)   ::    cphase
    
        !> atom sin phase factrs
    !real(kind = wp), allocatable, dimension(:,:)   ::    sphase

        !> atom temprorary cos phase factrs
    !real(kind = wp), allocatable, dimension(:,:)   ::    cphtmp
    
        !> atom temprorary sin phase factrs
    !real(kind = wp), allocatable, dimension(:,:)   ::    sphtmp

        !> density of an atom for second moment approx potentials
    real(kind = wp), dimension(:), allocatable :: smden

        !> Flag determining whether this molecule as a whole is to be considered as a 
        !> 'particle' for the purposes of PSMC. The alternative (if the flag is set to
        !> .false.) is that this is a container molecule, whose atoms are to be considered
        !> as PSMC particles. This flag will be .true. if the molecule is earmarked to
        !> move during the simulation.
    logical :: psmcparticle

        !> Position of lattice site corresponding to this molecule - for PSMC
    real (kind = wp) :: psmcsite(3)

        !> Displacement of the molecule from the lattice site - for PSMC
    real (kind = wp) :: psmcu(3)

        !> Molecule's orientation (i.e., the basis vectors for its local reference frame).
        !> orientation(:,1), orientation(:,2), and orientation(:,3) are the x-, y- and
        !> z-basis vectors respectively
    real(kind = wp)  :: orientation(3,3)

end type molecule

!> 'species' correspond to a particular molecular type
type species

    !> max number of molecule instances
    integer :: max_mols = 0

    !> number of molecule instances
    integer :: num_mols = 0

    !> flag to indicate molecule is an atomic field without internal structure
    logical :: is_field = .false.

    !> flag to indicate molecule is being treated as a rigid molecule
    logical :: rigid_body = .false.

    !> flag to exclude all coulomb interactions between atoms in molecule
    logical :: exc_coul_ints = .false.

    !> flag to indicate that molecules are subject to translation as a whole
    logical :: translate = .false.

    !> flag to indicate that molecules are subject to rotation as a whole
    logical :: rotate = .false.

    !> flag to indicate that molecules contain atoms subject to displacement
    logical :: atoms_move = .false.

    !type(molecule) :: base_mol
    !type(molecule), dimension(:), allocatable :: mols

    integer, dimension(:), allocatable :: mol_id

    !> number atoms of each type
    integer, dimension(:), allocatable :: num_elems

    !integer, dimension(:), allocatable :: elm_id

    !integer, dimension(:), allocatable :: atm_id

end type species


end module
