! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module invlist_type

    use kinds_f90

    implicit none

type invlist

    integer :: ninv = 0  ! the number of inversions for this molecule
    integer, dimension(:,:), allocatable :: ivpair ! stores connected atoms and potential number

end type

end module
