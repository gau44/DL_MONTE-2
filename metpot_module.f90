! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

!*!!!!!!!!!!!!!!!!!!!!!
! Originally based on !
!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 module declaring global metal interaction variables and
! arrays
!
! copyright - daresbury laboratory
! author    - i.t.todorov july 2004
! modified for dl_monty - 2008
!
!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module metpot_module

    use kinds_f90

    implicit None

    integer,                        save :: nmetpot
    Data                                    nmetpot / 0 /

    !> the number of parameters for vdw
    integer, parameter :: mxpmet = 7

    integer :: mxmet

    integer,           allocatable, save :: lstmet(:),ltpmet(:)

    real(kind = wp) :: dlrmet

    real( Kind = wp ), allocatable, save :: prmmet(:,:)
    real( Kind = wp ), allocatable, save :: vmet(:,:,:), dmet(:,:,:), fmet(:,:,:)
    real( Kind = wp ), allocatable, save :: elrcm(:),vlrcm(:)

    logical :: lmetab

    public :: allocate_metal_arrays

contains

    subroutine allocate_metal_arrays(nmet)

    use kinds_f90
    use constants_module, only : MAXMESH, uout

    use species_module, only : number_of_elements

    implicit none

    integer, intent(in) :: nmet
    integer :: ntpatm
    integer, dimension( 1:8 ) :: fail

    fail = 0

    nmetpot = nmet

    ntpatm = number_of_elements
    mxmet = (ntpatm * (ntpatm + 1)) / 2

    allocate (lstmet(1:mxmet),            stat = fail(1))
    allocate (ltpmet(1:mxmet),            stat = fail(2))
    allocate (prmmet(1:mxmet,1:mxpmet),   stat = fail(3))
    allocate (vmet(1:MAXMESH,1:mxmet,1:2), stat = fail(4))
    allocate (dmet(1:MAXMESH,1:mxmet,1:2), stat = fail(5))
    allocate (fmet(1:MAXMESH,1:mxmet,1:2), stat = fail(6))
     allocate (elrcm(0:ntpatm),            stat = fail(7))
    allocate (vlrcm(0:ntpatm),            stat = fail(8))

    if (any(fail > 0)) then

        call error(142)

    endif

    lstmet = 0
    ltpmet = 0

    prmmet = 0.0_wp
    vmet   = 0.0_wp
    dmet   = 0.0_wp
    fmet   = 0.0_wp

    elrcm  = 0.0_wp
    vlrcm  = 0.0_wp

    end subroutine allocate_metal_arrays

    subroutine read_metpot(cutoff, engunit)

    use kinds_f90
    use constants_module, only : uout, ufld, ATM_TYPE_METAL
    use parse_module
    use species_module, only : number_of_elements, element, get_species_type
    use comms_mpi_module, only : idnode

    implicit none

    real(kind = wp), intent(in) :: cutoff, engunit

    integer :: itpmet, ntab, katom1, katom2, j, keypot, jtpatm, nread
    integer :: typi, typj, keymet, i, atm1typ, atm2typ

    real(kind = wp) :: parpot(mxpmet)

    character*8 :: atom1, atom2
    character :: line*100, word*40, keyword*4

    logical :: lmet_safe, safe

    safe = .true.
    lmet_safe = .true.
    lmetab = .false.

    nread = ufld

    if (idnode == 0) write(uout,"(/,/,' nonbonded (metal) potentials ',/)")

    do itpmet = 1, nmetpot

        parpot = 0.0_wp

        call get_line(safe, nread, line)
        if (.not.safe) go to 1000

        call get_word(line,word)
        atom1=word
        call get_word(line,word)
        atm1typ = get_species_type(word)

        call get_word(line,word)
        atom2=word
        call get_word(line,word)
        atm2typ = get_species_type(word)

        call get_word(line,word)
        call lower_case(word)

        keyword=word

        if (keyword(1:3) == 'eam') then
            keypot=0
        else if (keyword == 'stch'  ) then
            keypot=1
        else if (keyword == 'fnsc'  ) then
            keypot=2
        else if (keyword == 'gupt') then
            keypot=3
        else

            call error(143)
            
        endif

        if (keypot > 0) then ! read in potentials with parameters

            call get_word(line, word)
            parpot(1) = word_2_real(word)
            call get_word(line, word)
            parpot(2) = word_2_real(word)
            call get_word(line, word)
            parpot(3) = word_2_real(word)
            call get_word(line, word)
            parpot(4) = word_2_real(word)
            call get_word(line, word)
            parpot(5) = word_2_real(word)
            call get_word(line, word)
            parpot(6) = word_2_real(word)
            call get_word(line, word)
            parpot(7) = word_2_real(word)

            if (idnode == 0) write(uout,"(1x,i2,3x,2a8,3x,a4,1x,10f15.6)") &
                itpmet,atom1,atom2,keyword,(parpot(j),j=1,mxpmet)

        else                 ! read in eam tables


            lmetab = .true.

        endif


        !check atom types exist
        katom1 = 0
        katom2 = 0

        do jtpatm = 1, number_of_elements
           if (atom1 == element(jtpatm) .and. atm1typ == ATM_TYPE_METAL) katom1 = jtpatm
           if (atom2 == element(jtpatm) .and. atm2typ == ATM_TYPE_METAL) katom2 = jtpatm
        end do

        if (katom1 == 0 .or. katom2 == 0) then

            call error(144)
           
        endif

        typi = max(katom1,katom2)
        typj = min(katom1,katom2)

        keymet = (typi * (typi - 1)) / 2 + typj

        if (keymet > mxmet) then

            call error(145)
            
        endif

        if (lstmet(keymet) /= 0) then

            call error(146)
            
        endif

        lstmet(keymet) = itpmet
        ltpmet(itpmet) = keypot

        ! convert energies to internal unit

        if (keypot > 0) then
            parpot(1) = parpot(1) * engunit

            if (keypot == 2) then
                parpot(2) = parpot(2) * engunit
                parpot(3) = parpot(3) * engunit
                parpot(5) = parpot(5) * engunit
            else if (keypot == 3) Then
                parpot(4) = parpot(4) * engunit
            endif

            do i = 1, mxpmet
                prmmet(itpmet,i) = parpot(i)
            enddo

        endif

    enddo 

    if(lmetab) then

        call metal_table_read(cutoff, engunit)

    endif

    if (nmetpot /= 0) call metal_generate(cutoff)

    return
1000 continue
    call error(147)

end subroutine

subroutine metal_generate(cutoff)

    !***********************************************************************
 
    !dl_poly subroutine for generating potential energy and
    ! force arrays for metal potentials
 
    ! copyright - daresbury laboratory
    ! author    - w. smith june 1995
    ! amended   - w. smith  march 2006
    ! adapted   - dlmonte 2008
    ! ***********************************************************************

    use kinds_f90
    use species_module, only : number_of_elements
    use constants_module, only : uout, MAXMESH

    implicit none

    real(kind = wp), intent(in) :: cutoff

    integer :: i, kmet, mmet, katm1, katm2, ntpatm, imet, nmet
    integer :: nnn, mmm
    real(kind = wp) :: rrr, eps, sig, cc0, cc1, cc2, ccc
    real(kind = wp) :: ddd, bet, cut1, cut2, aaa, rr0, ppp, qqq

    dlrmet = cutoff / real(MAXMESH-4)

    !construct arrays for metal potentials

    kmet = 0
    do katm1 = 1, number_of_elements

        do katm2 = 1, katm1

            kmet = kmet + 1
            imet = lstmet(kmet)

            if(imet == 0) cycle

            if(ltpmet(imet) > 0)then


                !store array specification parameters

                vmet(1,imet,1) = dble(MAXMESH)
                vmet(2,imet,1) = 0.d0
                vmet(3,imet,1) = dlrmet * dble(MAXMESH-4)
                vmet(4,imet,1) = dlrmet

                do i=1,4

                    vmet(i,imet,2) = vmet(i,imet,1)
                    dmet(i,imet,1) = vmet(i,imet,1)
                    dmet(i,imet,2) = 0.0
    
                enddo

                if(ltpmet(imet) == 1)then !sutton-chen potentials

                    eps = prmmet(imet,1)
                    sig = prmmet(imet,2)
                    nnn = nint(prmmet(imet,3))
                    mmm = nint(prmmet(imet,4))

                    do i = 5, MAXMESH

                        rrr = dble(i) * dlrmet
                        vmet(i,imet,1) = eps * (sig / rrr)**nnn
                        vmet(i,imet,2) = dble(nnn) * eps * (sig / rrr)**nnn
                        dmet(i,imet,1) = (sig/rrr)**mmm
                        dmet(i,imet,2) = dble(mmm) * (sig / rrr)**mmm

                    enddo

                    if(katm1 == katm2)then

                        dmet(1,imet,2) = (prmmet(imet,1) * prmmet(imet,5))**2
                        dmet(2,imet,2) = (prmmet(imet,1) * prmmet(imet,5))**2

                    else

                        nmet = lstmet((katm1 * (katm1 + 1)) / 2)
                        mmet = lstmet((katm2 * (katm2 + 1)) / 2)
                        dmet(1,imet,2) = (prmmet(nmet,1)*prmmet(nmet,5))**2
                        dmet(2,imet,2) = (prmmet(mmet,1)*prmmet(mmet,5))**2

                    endif

                else if(ltpmet(imet) == 2) then !finnis sinclair potentials

                    cc0 = prmmet(imet,1)
                    cc1 = prmmet(imet,2)
                    cc2 = prmmet(imet,3)
                    ccc = prmmet(imet,4)
                    ddd = prmmet(imet,6)
                    bet = prmmet(imet,7)
                    cut1 = ccc + 4.0 * dlrmet
                    cut2 = ddd + 4.0 * dlrmet

                    do i = 5, MAXMESH

                        rrr = real(i) * dlrmet
                        vmet(i,imet,1) = 0.0
                        vmet(i,imet,2) = 0.0
                        dmet(i,imet,1) = 0.0
                        dmet(i,imet,2) = 0.0

                        if(rrr <= cut1) then
  
                            vmet(i,imet,1) = (cc0 + cc1 * rrr + cc2 * rrr * rrr) * ( rrr - ccc)**2
                            vmet(i,imet,2) = -rrr * (2.0 * (cc0 + cc1 * rrr + cc2 * rrr * rrr)*   &
                                           (rrr - ccc) + (cc1 + 2.0 * cc2 * rrr) * (rrr - ccc)**2)

                        endif

                        if(rrr <= cut2) then

                            dmet(i,imet,1) = (rrr - ddd)**2 + bet * (rrr - ddd)**3 / ddd
                            dmet(i,imet,2) = -rrr * (2.0 * (rrr - ddd) + 3.0 * bet * (rrr - ddd)**2 / ddd)

                        endif

                    enddo

                    if(katm1 == katm2)then

                        dmet(1,imet,2) = prmmet(imet,5)**2
                        dmet(2,imet,2) = prmmet(imet,5)**2

                    else

                        nmet = lstmet((katm1 * (katm1 + 1)) / 2)
                        mmet = lstmet((katm2 * (katm2 + 1)) / 2)
                        dmet(1,imet,2) = prmmet(nmet,5)**2
                        dmet(2,imet,2) = prmmet(mmet,5)**2

                    endif

                else if(ltpmet(imet) == 3) then !gupta potentials

                    aaa = prmmet(imet,1)
                    rr0 = prmmet(imet,2)
                    ppp = prmmet(imet,3)
                    qqq = prmmet(imet,5)

                    do i = 5, MAXMESH

                        rrr = real(i) * dlrmet
                        vmet(i,imet,1) = aaa * exp(-ppp * (rrr - rr0) / rr0)
                        vmet(i,imet,2) = vmet(i,imet,1) * rrr * ppp / rr0
                        dmet(i,imet,1) = exp(-2.0 * qqq * (rrr - rr0) / rr0)
                        dmet(i,imet,2) = 2.0 * dmet(i,imet,1) * rrr * qqq / rr0

                    enddo

                    dmet(1,imet,2) = prmmet(imet,4)**2
                    dmet(2,imet,2) = prmmet(imet,4)**2

                else if(.not.lmetab) then

                    call error(148)

                endif

            endif

        enddo

    enddo

end subroutine

subroutine metal_table_read(cutoff, engunit)

    use kinds_f90
    use constants_module, only : MAXMESH, uout, utbl
    use parse_module
    use species_module, only : number_of_elements, element
    use comms_mpi_module, only : idnode

    implicit none

    real(kind = wp), intent(in) :: cutoff, engunit

    integer :: nread, numpot, ipot, ngrid, katom1, katom2, ktype, i, j, k
    integer :: imet, jtpatm, typi, typj, keymet

    real(kind = wp) :: start, finish
    real(kind = wp) :: buffer(MAXMESH)

    character :: line*100, word*40, keyword*4
    character :: atom1*8, atom2*8

    logical :: safe

    nread = utbl 

    if (idnode == 0) open (nread, file = 'TABEAM')

    !skip header record

    call get_line(safe, nread, line)
    if(.not.safe) call abort_eamtable_read()

    !read number of potential functions in file

    call get_line(safe, nread, line)
    if(.not.safe) call abort_eamtable_read()

    call get_word(line,word)
    numpot = nint(word_2_real(word))

    do ipot = 1, numpot

        !read data type, atom labels, number of points, start and end

        call get_line(safe, nread, line)
        if(.not.safe) call abort_eamtable_read()

        !indentify data type

        ktype=1
        call get_word(line, word)
        call lower_case(word)
        if (word(1:4) == 'pair') then
            ktype = 1
        else If (word(1:4) == 'dens') then
            ktype = 2
        else If (word(1:4) == 'embe') then
            ktype = 3
        else

            call error(149)
            
        endif


        !identify atom types
        call get_word(line,atom1)
        if (ktype == 1) Then
            Call get_word(line,atom2)
        else
            atom2 = atom1
        endif

        ! data specifiers

        call get_word(line,word)
        ngrid = nint(word_2_real(word))

        call get_word(line,word)
        start  = word_2_real(word)

        call get_word(line,word)
        finish = word_2_real(word)


        !check atom indentities
        katom1=0
        katom2=0

        do jtpatm = 1, number_of_elements

            if (atom1 == element(jtpatm)) katom1 = jtpatm
            if (atom2 == element(jtpatm)) katom2 = jtpatm

        enddo 

        if (katom1 == 0) then

            call error(150)
            
        endif

        if (katom2 == 0) then

            call error(151)
            
        endif


        if(MAXMESH < ngrid) then

            call error(152)

        endif

        !store working parameters (start shifted for DL_POLY interpolation)

        buffer(1) = real(ngrid + 4, wp)
        buffer(2) = start
        buffer(3) = finish
        buffer(4) = (finish - start) / real(ngrid - 1, wp)
        buffer(2) = buffer(2) - 5.0 * buffer(4)
        buffer(3) = buffer(3) - buffer(4)

        if (idnode == 0) write(uout,"(1x,i10,4x,2a8,3x,a4,2x,2i6,1p,3e15.6)") &
        ipot,atom1,atom2,'EAM-',ktype,ngrid,start,finish,buffer(4)

        !read potential arrays

        k=4
        do j = 1, (ngrid + 3) / 4

            call get_line(safe, nread, line)
            if(.not.safe) call abort_eamtable_read()

            call get_word(line,word)
            buffer(k+1) = word_2_real(word)
            call get_word(line,word)
            buffer(k+2) = word_2_real(word)
            call get_word(line,word)
            buffer(k+3) = word_2_real(word)
            call get_word(line,word)
            buffer(k+4) = word_2_real(word)
          
            k=k+4

        enddo

        !copy data to internal arrays

        if (ktype == 1) then

            ! check range against specified cutoff

            if( cutoff < finish) then

                call error(153)
                
            endif

            ! identify potential

            typi = max(katom1,katom2)
            typj = min(katom1,katom2)

            keymet = (typi * (typi - 1)) / 2 + typj
            imet = lstmet(keymet)

            !pair potential terms

            vmet(1,imet,1) = buffer(1)
            vmet(2,imet,1) = buffer(2)
            vmet(3,imet,1) = buffer(3)
            vmet(4,imet,1) = buffer(4)

            do i = 5, MAXMESH

                if(i - 4 > ngrid) then
                    vmet(i,imet,1) = 0.d0
                else
                    vmet(i,imet,1) = buffer(i) * engunit
                    buffer(i) = buffer(i) * engunit
                endif

            enddo

            !calculate derivative of pair potential function
            call metal_deriv(imet, vmet, buffer)

            !adapt derivatives for use in interpolation

            do i = 5, ngrid + 4
                vmet(i,imet,2) = -(real(i) * buffer(4) + buffer(2)) * vmet(i,imet,2)
            enddo

        else if(ktype == 2) then

            !check range against specified cutoff

            if( cutoff < finish) then
                
                call error(153)

            endif

            !density  terms
            dmet(1,katom1,1) = buffer(1)
            dmet(2,katom1,1) = buffer(2)
            dmet(3,katom1,1) = buffer(3)
            dmet(4,katom1,1) = buffer(4)

            do i = 5, MAXMESH

                if(i - 4 > ngrid) then
                    dmet(i,katom1,1) = 0.d0
                else
                    dmet(i,katom1,1) = buffer(i)
                endif

            enddo

            ! calculate derivative of density function

            call metal_deriv(katom1, dmet, buffer)

            !adapt derivatives for use in interpolation

            dmet(1,katom1,2) = 0.d0
            dmet(2,katom1,2) = 0.d0
            dmet(3,katom1,2) = 0.d0
            dmet(4,katom1,2) = 0.d0
            do i = 5, ngrid + 4
                dmet(i,katom1,2) = -(real(i) * buffer(4) + buffer(2)) * dmet(i,katom1,2)
            enddo

        elseif (ktype == 3) then

            !embedding function arrays

            fmet(1,katom1,1) = buffer(1)
            fmet(2,katom1,1) = buffer(2)
            fmet(3,katom1,1) = buffer(3)
            fmet(4,katom1,1) = buffer(4)

            do i = 5, MAXMESH

                if (i - 4 > ngrid) then
                    fmet(i,katom1,1) = 0.d0
                else
                    fmet(i,katom1,1) = buffer(i) * engunit
                    buffer(i) = buffer(i) * engunit
                endif

            enddo

            !calculate derivative of embedding function

            call metal_deriv(katom1, fmet, buffer)

        endif

    enddo

    if (idnode == 0) then
        close (utbl)
        write(uout,'(/,/,1x,a)') 'potential tables read from TABEAM file'
    endif

end subroutine

subroutine metal_deriv(ityp, vvv, buffer)

    !**********************************************************************

    !calculate numerical derivatives of tabulated EAM metal potentials

    !copyright daresbury laboratory
    !author w.smith march 2006
    !modified for dl_monte 200

    !**********************************************************************

    use kinds_f90
    use constants_module, only : MAXMESH
    implicit none

    integer ityp, i, npt

    real(kind = wp) :: vvv(MAXMESH,mxmet,2), buffer(MAXMESH)
    real(kind = wp) :: delmet, aa0, aa1, aa2, aa3, aa4, d1y, d2y, d3y, d4y
    real(kind = wp) :: f0, f1, f2, f3, f4


    ! interpolation parameters

    vvv(1,ityp,2)=buffer(1)
    vvv(2,ityp,2)=buffer(2)
    vvv(3,ityp,2)=buffer(3)
    vvv(4,ityp,2)=buffer(4)

    !construct interpolation table

    delmet = buffer(4)
    npt = nint(buffer(1)) - 2

    do i = 7, npt

        aa0 = buffer(i)
        f0 = buffer(i-2) / aa0
        f1 = buffer(i-1) / aa0
        f2 = 1.d0
        f3 = buffer(i+1) / aa0
        f4 = buffer(i+2) / aa0

        !calculate numerical differences for 5-point interpolation

        d1y = (f1 - f0)
        d2y = (f2 - f1) - (f1 - f0)
        d3y = (f3 - f0) + 3.d0 * (f1 - f2)
        d4y = (f4 - f3) + 3.d0 * (f2 - f3) + 3.d0 * (f2 - f1) + (f0 - f1)

        !calculate polynomial coefficients
        aa0 = aa0 / delmet
        aa4 = d4y / 24.d0
        aa3 = (d3y + 12.d0 * aa4) / 6.d0
        aa2 = (d2y + 6.d0 * aa3 - 14.d0 * aa4) / 2.d0
        aa1 = d1y + 3.d0 * aa2 - 7.d0 * aa3 + 15.d0 * aa4

        !calculate derivatives

        vvv(i,ityp,2) = aa1 * aa0

        !derivatives at extremes of range

        if(i == 7)then

            vvv(5,ityp,2) = (aa1 - 4.d0 * aa2 + 12.d0 * aa3 - 32.d0 * aa4) * aa0
            vvv(6,ityp,2) = (aa1 - 2.d0 * aa2 + 3.d0 * aa3 - 4.d0 * aa4) * aa0

        else if(i == npt) then

            vvv(npt+1,ityp,2) = (aa1 + 2.d0 * aa2 + 3.d0 * aa3 + 4.d0 * aa4) * aa0
            vvv(npt+2,ityp,2) = (aa1 + 4.d0 * aa2 + 12.d0 * aa3 + 32.d0 * aa4) * aa0

        endif

    enddo

    !set derivatives to zero beyond end point of function

    do i = npt+3, MAXMESH
        vvv(i,ityp,2) = 0.0
    enddo

end subroutine

subroutine abort_eamtable_read()

    !***********************************************************************

    !dl_poly error exit subroutine for reading TABEAM file

    !copyright - daresbury laboratory
    !author    - w. smith   mar 2006
    !modified for dl_monte 2008

    !***********************************************************************

    use constants_module, only : utbl

    implicit none

    write(6,*)'*** error in reading EAM potentials'
    stop

end subroutine

subroutine metal_rho(ai, aj, r, rho)

!***********************************************************************
!
!     dl_poly subroutine for calculating local density in metals
!     using the verlet neighbour list and sutton-chen potentials
!
!     parallel replicated data version
!
!     copyright - daresbury laboratory 1995
!     author    - w. smith june 1995
!     amended   - w. smith  march 2006
!     adapted heavily for mc - 2008
!
!     wl
!     2006/11/28 16:32:45
!     1.7
!     Exp
!
!***********************************************************************

    use kinds_f90

    implicit none

    integer, intent(in) :: ai, aj
    real(kind = wp), intent(in) :: r
    real(kind = wp), intent(out) :: rho

    integer :: k0, l
    real(kind = wp) :: ab, vk0, vk1, vk2, t1, t2, ppp, rdr, rrr, density

    rho = 0.0

    if(ai > aj) then
        ab = ai * (ai - 1.0) * 0.5 + aj + 0.5
    else
        ab = aj * (aj - 1.0) * 0.5 + ai + 0.5
    endif

    k0 = lstmet(int(ab))

    if(k0 == 0.and.lmetab) then

          !local density for EAM table
          !call eamden(itp, r, rho)

    elseif(k0 >= 1) then

!       print*,'k0',k0,ai,aj,ltpmet(k0)

        if((ltpmet(k0) >= 1).and.(abs(dmet(1,k0,1)).gt.1.e-10)) then

            ! interpolation parameters

            rdr = 1.d0 / dmet(4,k0,1)
            rrr = r - dmet(2,k0,1)
            l = min(nint(rrr * rdr), int(dmet(1,k0,1)) -1)
            ppp = rrr * rdr - real(l,wp)

            ! calculate density using 3-point interpolation

            vk0 = dmet(l-1,k0,1)
            vk1 = dmet(l,k0,1)
            vk2 = dmet(l+1,k0,1)

            t1 = vk1 + ppp * (vk1 - vk0)
            t2 = vk1 + ppp * (vk2 - vk1)

            if(ppp < 0.0)then

                density = t1 + 0.50 * (t2 - t1) * (ppp - 1.0)

            else

                density = t2 + 0.50 * (t2 - t1) * (ppp - 1.0)

            endif


            rho = rho + density * dmet(1,k0,2)

        endif

    endif

end subroutine

subroutine metal_energy(ai, aj, r, eng, vir)

!***********************************************************************
!
!     dl_poly subroutine for calculating local density in metals
!     using the verlet neighbour list and sutton-chen potentials
!
!     parallel replicated data version
!
!     copyright - daresbury laboratory 1995
!     author    - w. smith june 1995
!     amended   - w. smith  march 2006
!     adapted heavily for mc - 2008
!
!     wl
!     2006/11/28 16:32:45
!     1.7
!     Exp
!
!***********************************************************************

    use kinds_f90

    implicit none

    integer, intent(in) :: ai, aj
    real(kind = wp), intent(in) :: r
    real(kind = wp), intent(out) :: eng, vir

    integer :: k0, l, typi, typj, keymet
    real(kind = wp) :: ab, vk0, vk1, vk2, t1, t2, ppp, rdr, rrr

    eng = 0.0
    vir = 0.0

    if(ai > aj) then
        ab = ai * (ai - 1.0) * 0.5 + aj + 0.5
    else
        ab = aj * (aj - 1.0) * 0.5 + ai + 0.5
    endif

    k0 = lstmet(int(ab))

    if(k0 /= 0.and.lmetab) then

          if (r < vmet(3,k0,1)) then

        rdr = 1.0 / vmet(4,k0,1)
        rrr = r - vmet(2,k0,1)

        l = min(nint(rrr / vmet(4,k0,1)), int(vmet(1,k0,1)) - 1)
        ppp = (rrr / vmet(4,k0,1)) - real(l, wp)

        !calculate embedding energy using 3-point interpolation

        vk0 = vmet(l-1,k0,1)
        vk1 = vmet(l,k0,1)
        vk2 = vmet(l+1,k0,1)

        t1 = vk1 + (vk1 - vk0) * ppp
        t2 = vk1 + (vk2 - vk1) * ppp

        if (ppp < 0.0) then
              eng = eng + (t1 + 0.5d0 * (t2 - t1) * (ppp + 1.d0))
        else
              eng = eng + (t2 + 0.5d0 * (t2 - t1) * (ppp - 1.d0))
        endif
!           print*,'metfrc',eng,ai,aj,k0,r
!     stop
    endif


    elseif(k0 >= 1) then

        if((ltpmet(k0) >= 1).and.(abs(vmet(1,k0,1)).gt.1.e-10)) then

            ! interpolation parameters

            rdr = 1.d0 / vmet(4,k0,1)
            rrr = r - vmet(2,k0,1)
            l = min(nint(rrr * rdr), int(vmet(1,k0,1)) -1)
            ppp = rrr * rdr - real(l,wp)

            ! calculate density using 3-point interpolation

            vk0 = vmet(l-1,k0,1)
            vk1 = vmet(l,k0,1)
            vk2 = vmet(l+1,k0,1)

            t1 = vk1 + ppp * (vk1 - vk0)
            t2 = vk1 + ppp * (vk2 - vk1)

            if(ppp < 0.0)then

                eng = eng + t1 + 0.50 * (t2 - t1) * (ppp - 1.0)

            else

                eng = eng + t2 + 0.50 * (t2 - t1) * (ppp - 1.0)

            endif

        endif

    endif

end subroutine

subroutine metal_eamrho(ktyp, r, vrho)

    use kinds_f90

    implicit none

    integer, intent(in) :: ktyp
    real(kind = wp), intent(in) :: r
    real(kind = wp), intent(out) :: vrho

    integer :: l
    real(kind = wp) :: rdr, rrr, ppp, vk0, vk1, vk2, t1, t2

    vrho = 0.0

    if(abs(dmet(1,ktyp,1)) <  1.0e-10) return

    !apply truncation of potential
    if(r <= dmet(3,ktyp,1)) then

        !interpolation parameters
        rdr = 1.0 / dmet(4,ktyp,1)
        rrr = r - dmet(2,ktyp,1)
        l = min(nint(rrr * rdr), int(dmet(1,ktyp,1)) - 1)
        ppp = rrr * rdr - real(l, wp)

        !calculate density using 3-point interpolation

        vk0 = dmet(l-1,ktyp,1)
        vk1 = dmet(l,ktyp,1)
        vk2 = dmet(l+1,ktyp,1)

        t1 = vk1 + ppp * (vk1 - vk0)
        t2 = vk1 + ppp * (vk2 - vk1)
        
        if(ppp < 0.d0) then
            vrho = t1 + 0.5 * (t2 - t1) * (ppp + 1.0)
        else
            vrho = t2 + 0.50 * (t2 - t1) * (ppp - 1.0)
        endif

    endif

end subroutine

subroutine eam_embed(ktyp, trho, eng)

    use kinds_f90

    implicit none

    integer, intent(in) :: ktyp

    real(kind = wp), intent(in) :: trho
    real(kind = wp), intent(out) :: eng

    integer :: l
    real(kind = wp) :: fk0, fk1, fk2, rrr, ppp, t1, t2, rdr

    eng = 0.0_wp

    if (trho > 0.0_wp) then

        rrr = trho - fmet(2,ktyp,1)
        l = min(nint(rrr / fmet(4,ktyp,1)), int(fmet(1,ktyp,1)) - 1)
        ppp = (rrr / fmet(4,ktyp,1)) - real(l, wp)

        !calculate embedding energy using 3-point interpolation

        fk0 = fmet(l-1,ktyp,1)
        fk1 = fmet(l,ktyp,1)
        fk2 = fmet(l+1,ktyp,1)

        t1 = fk1 + (fk1 - fk0) * ppp
        t2 = fk1 + (fk2 - fk1) * ppp

        if (ppp < 0.0) then


            eng = eng - (t1 + 0.50_wp * (t2 - t1) * (ppp + 1.0_wp))

        else

            eng = eng - (t2 + 0.50_wp * (t2 - t1) * (ppp - 1.0_wp))

        endif

    endif

end subroutine

end module
