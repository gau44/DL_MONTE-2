! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module gibbs_moves

    use kinds_f90

        !> attempted atom transfers from box1 to box2
    integer ::    attemptedgibbstrans_atmb1tob2 = 0

        !> attempted atom transfers from box2 to box1
    integer ::    attemptedgibbstrans_atmb2tob1 = 0

        !> successful atom transfers from box1 to box2
    integer ::    successfulgibbstrans_atmb1tob2 = 0

        !> successful atom transfers from box2 to box1
    integer ::    successfulgibbstrans_atmb2tob1 = 0

        !> attempted atom exchange from box1 to box2
    integer ::    attemptedgibbsexch_atmb1tob2 = 0

        !> attempted atom exchange from box2 to box1
    integer ::    attemptedgibbsexch_atmb2tob1 = 0

        !> successful atom exchange from box1 to box2
    integer ::    successfulgibbsexch_atmb1tob2 = 0

        !> successful atom exchange from box2 to box1
    integer ::    successfulgibbsexch_atmb2tob1 = 0

        !> attempted molecule transfers from box1 to box2
    integer ::    attemptedgibbstrans_molb1tob2 = 0

        !> attempted molecule transfers from box2 to box1
    integer ::    attemptedgibbstrans_molb2tob1 = 0

        !> successful molecule transfers from box1 to box2
    integer ::    successfulgibbstrans_molb1tob2 = 0

        !> successful molecule transfers from box2 to box1
    integer ::    successfulgibbstrans_molb2tob1 = 0

        !> look up table for gibbs atom types for transfer moves
    integer, allocatable ::    gibbstrans_atmlookup(:)

        !> look up table for gibbs atom types for exchange moves
    integer, allocatable ::    gibbsexch_atmlookup1(:)
    integer, allocatable ::    gibbsexch_atmlookup2(:)

        !> lookup table of types for molecule types in transfer moves
    integer, allocatable ::    gibbstrans_mollookup(:)

        !> lookup table of types for molecule types in exchange moves
    integer, allocatable ::    gibbsexch_mollookup(:)

contains

!*************************************************************************
! transfers atoms between boxes
! assumptions
! gas atom has no charge
! box 1 contains a framework molecule (eg zeolite) and the gas atom is added/removed
! from the last molecule to allow host - gas interaction to be calculated.
! box 2 contains a single molecule containing the gas.
!************************************************************************/
subroutine gibbs_atom_transfer(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                      emolpair, emolang, emolfour, emolmany, emolext, volume)

    use kinds_f90
    use species_module
    use control_type
    use constants_module, only : uout
    use cell_module
    use field_module
    use random_module, only : duni
    use comms_mpi_module, only : master !, mxnode, gsync
    use vdw_module, only : vdw_lrc_energy


    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), &
                                      energyreal(:), energyvdw(:), energythree(:), energypair(:), &
                                      energyang(:), energyfour(:), energymany(:), energyext(:), energymfa(:), &
                                      volume(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), &
                     emolrealnonb(nconfigs,number_of_molecules), &
                     emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                     emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                     emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                     emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                     emolmany(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules)

    integer :: i, j, ib_in, ib_out, ngas_out, ngas_in, im_in, im_out, ii, typ, choice
    integer :: atm_in, atm_out, gno, igrid, buf(2)

    real(kind=wp) :: arg, distmax, deltav, deltavb, otot, itot, outreal, outrcp, outvdw, outthree, outpair, &
                     outang, outfour, outmany, outext, outmfa, outtotal, outenth, outvir, inreal, inrcp, invdw, & 
                     inthree, inpair, inang, infour, inmany, inext, inmfa, intotal, inenth, invir, pcav

     !temp vectors for molecular energies
    real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules), orcp(number_of_molecules), &
                       ovdwn(number_of_molecules), ovdwb(number_of_molecules), opair(number_of_molecules), &
                       othree(number_of_molecules),  oang(number_of_molecules), ofour(number_of_molecules), &
                       omany(number_of_molecules), ocrct(number_of_molecules), oext(number_of_molecules), &
                       irealnonb(number_of_molecules), irealbond(number_of_molecules), ircp(number_of_molecules), &
                       ivdwn(number_of_molecules), ivdwb(number_of_molecules), ipair(number_of_molecules), &
                       ithree(number_of_molecules), iang(number_of_molecules), ifour(number_of_molecules), &
                       imany(number_of_molecules), icrct(number_of_molecules), iext(number_of_molecules)

    logical :: atom_out, overlap, accept

    !TU: Number of each type of atom, for calculating the long-range VdW correction
    integer :: out_old_nums_elemts(number_of_elements), out_new_nums_elemts(number_of_elements), &
                in_old_nums_elemts(number_of_elements),  in_new_nums_elemts(number_of_elements)


    outreal = 0.0_wp
    outrcp = 0.0_wp
    outvdw = 0.0_wp
    outthree = 0.0_wp
    outpair = 0.0_wp
    outang = 0.0_wp
    outmany = 0.0_wp
    outfour = 0.0_wp
    outext = 0.0_wp
    outmfa = 0.0_wp
    outenth = 0.0_wp
    outvir = 0.0_wp

    inreal = 0.0_wp
    inrcp = 0.0_wp
    invdw = 0.0_wp
    inthree = 0.0_wp
    inpair = 0.0_wp
    inang = 0.0_wp
    inmany = 0.0_wp
    infour = 0.0_wp
    inext = 0.0_wp
    inmfa = 0.0_wp
    inenth = 0.0_wp
    invir = 0.0_wp

    !first set up appropriate boxes for calculation of energies
    if(ib == 1) then

        ib_out = 1
        ib_in = 2
        im_out = cfgs(ib_out)%num_mols
        im_in = 1

        attemptedgibbstrans_atmb1tob2 = attemptedgibbstrans_atmb1tob2 + 1

    else

        ib_out = 2
        ib_in = 1
        im_in = cfgs(ib_in)%num_mols
        im_out = 1

        attemptedgibbstrans_atmb2tob1 = attemptedgibbstrans_atmb2tob1 + 1

    endif

    out_old_nums_elemts = cfgs(ib_out)%nums_elemts
    out_new_nums_elemts = out_old_nums_elemts
    in_old_nums_elemts = cfgs(ib_in)%nums_elemts
    in_new_nums_elemts = in_old_nums_elemts


    !get the atom type to transferred
    choice = int(job%num_atmtran_gibbs * duni() + 1)

    typ = gibbstrans_atmlookup(choice)

    !TU: Obsolete; replace with cfgs(ib)%num_elemts; see 'findnumtype' code for details
    !insert the atom
    call findnumtype(ib_in, typ, ngas_in)
    
    ! make sure dont go over array limits
    if (ngas_in > cfgs(ib_in)%mols(im_in)%mxatom) call error(207)

    !TU: Obsolete; replace with cfgs(ib)%num_elemts; see 'findnumtype' code for details
    !check that there is an atom that can be removed
    call findnumtype(ib_out, typ, ngas_out)

    !TU: WARNING: Returning the function here is risky. Even if the move is rejected it should still
    !TU: be used in gathering statistics. There is a risk that statistics gathered after this line of
    !TU: code would not occur if ngas_out==0
    if (ngas_out == 0) return

    accept = .false.

    if(job%usecavitybias) then

        call insert_atom_atcavity(ib_in, typ, im_in, atm_in, igrid, pcav, job%lauto_nbrs, atom_out)

        ! if atom got out the cell/slit => reject insertion!
        if( atom_out ) goto 1000

        if (pcav < 0.0) then !< 0.0 means no cavities - do random insert

            call insert_atom(ib_in, typ, im_in, atm_in, job%lauto_nbrs, atom_out)

            ! if atom got out the cell/slit => reject insertion!
            if( atom_out ) goto 1000

            pcav = 1.0_wp

        endif

    else

        !find a position to place atom - it is always placed at the end of the list
        call insert_atom(ib_in, typ, im_in, atm_in, job%lauto_nbrs, atom_out)

        ! if atom got out the cell/slit => reject insertion!
        if( atom_out ) goto 1000

        pcav = 1.0_wp

    endif

    !TU: The insert_atom procedure above updates cfgs(ib_in)%nums_elemts to reflect insertion
    in_new_nums_elemts = cfgs(ib_in)%nums_elemts


    if(job%uselist) then

        ! calculate nbrlist + check for overlap
        call atom_nbrlist(ib_in, im_in , atm_in, job%shortrangecut, job%verletshell)

        ! evaluate energy
        call atom_energy(ib_in, im_in, atm_in, job%coultype(ib_in), job%dielec, & 
                         job%vdw_cut, job%shortrangecut, job%verletshell, &
                         inreal, invdw, inthree, inpair, inang, infour, inext, inmfa, invir, &
                         irealnonb, irealbond, icrct, ivdwn, ivdwb, ipair, ithree, iang, ifour, iext, &
                         .false., .true.)

    else

        ! evaluate energy
        call atom_energy_nolist(ib_in, im_in, atm_in, job%coultype(ib_in), job%dielec, &
                         job%vdw_cut, job%shortrangecut, &
                         inreal, invdw, inthree, inpair, inang, infour, inext, inmfa, invir,  &
                         irealnonb, irealbond, icrct, ivdwn, ivdwb, ipair, ithree, iang, ifour, iext, &
                         .false., .true.)

    endif

    !TU: invdw is the actually change in VdW relative to the old configuration. Hence
    !TU: add the CHANGE in the long-range VdW correction
    invdw = invdw + ( vdw_lrc_energy(1.0_wp/volume(ib_in),in_new_nums_elemts) &
                      - vdw_lrc_energy(1.0_wp/volume(ib_in),in_old_nums_elemts) )

    intotal = inreal + invdw + inthree + inpair + inang + inmany + infour + inext + inmfa

    !chalculate chemical potential
    !!chem_pot_atm(ib1,typ) = volume(ib1) * exp(-beta * intotal) / (natom + 1);

    !TU: select_molatom would hang if there were no atoms of the specified type - won't happen due to safety check above
    call select_molatom(typ, ib_out, im_out, atm_out)

    !get energy of the atom being removed

    !TU: Note that we haven't yet removed the atom (which happens only upon acceptance)
    !TU: and so must manually set new_nums_elemts (instead of just setting it to 
    !TU: cfgs(ib)%nums_elemts)
    out_new_nums_elemts(typ) = out_new_nums_elemts(typ) - 1

    if(job%uselist) then

        call atom_energy(ib_out, im_out, atm_out, job%coultype(ib_out), job%dielec, &
                     job%vdw_cut, job%shortrangecut, job%verletshell, &
                     outreal, outvdw, outthree, outpair, outang, outfour, outext, outmfa, outvir,  &
                     orealnonb, orealbond, ocrct, ovdwn, ovdwb, opair, othree, oang, ofour, oext,  &
                     .false., .true.)

    else

        call atom_energy_nolist(ib_out, im_out, atm_out, job%coultype(ib_out), job%dielec, &
                     job%vdw_cut, job%shortrangecut, &
                     outreal, outvdw, outthree, outpair, outang, outfour, outext, outmfa, outvir,  &
                     orealnonb, orealbond, ocrct, ovdwn, ovdwb, opair, othree, oang, ofour, oext,  &
                     .false., .true.)

    endif

    !TU: outvdw is actually the negative change in VdW relative to the old configuration. Hence
    !TU: add the negative of the CHANGE in the long-range VdW correction
    outvdw = outvdw - ( vdw_lrc_energy(1.0_wp/volume(ib_out),out_new_nums_elemts) &
                      - vdw_lrc_energy(1.0_wp/volume(ib_out),out_old_nums_elemts) )

    outtotal = outreal + outvdw + outthree + outpair + outang &
             + outmany + outfour + outext + outmfa

    arg = exp(-beta * (intotal - outtotal + log(volume(ib_out) * (ngas_in+1) / (volume(ib_in) * ngas_out)) / beta))

    if (master) then

        if(duni() < arg)  accept = .true.

    endif

    !test for accept or rejection
    if (accept) then

        if(ib == 1) then
            successfulgibbstrans_atmb1tob2 = successfulgibbstrans_atmb1tob2 + 1
        else
            successfulgibbstrans_atmb2tob1 = successfulgibbstrans_atmb2tob1 + 1
        endif

        !update the energies for the atom removed from a box

        energytot(ib_out) = energytot(ib_out) - outtotal
        enthalpytot(ib_out) = enthalpytot(ib_out) - outenth
        virialtot(ib_out) = virialtot(ib_out) - outvir
        energyreal(ib_out) = energyreal(ib_out) - outreal
        energyvdw(ib_out) = energyvdw(ib_out) - outvdw
        energypair(ib_out) = energypair(ib_out) - outpair
        energythree(ib_out) = energythree(ib_out) - outthree
        energyang(ib_out) = energyang(ib_out) - outang
        energyfour(ib_out) = energyfour(ib_out) - outfour
        energymany(ib_out) = energymany(ib_out) - outmany
        energyext(ib_out) = energyext(ib_out) - outext
        energymfa(ib_out) = energymfa(ib_out) - outmfa

        if(job%lmoldata) then

            do ii = 1, number_of_molecules
                emolrealnonb(ib_out,ii) = emolrealnonb(ib_out,ii) - orealnonb(ii)
                emolrealbond(ib_out,ii) = emolrealbond(ib_out,ii) - orealbond(ii)
                emolvdwn(ib_out,ii) = emolvdwn(ib_out,ii) - ovdwn(ii)
                emolvdwb(ib_out,ii) = emolvdwb(ib_out,ii) - ovdwb(ii)
                emolpair(ib_out,ii) = emolpair(ib_out,ii) - opair(ii)
                emolthree(ib_out,ii) = emolthree(ib_out,ii) - othree(ii)
                emolang(ib_out,ii) = emolang(ib_out,ii) - oang(ii)
                emolfour(ib_out,ii) = emolfour(ib_out,ii) - ofour(ii)
                emolmany(ib_out,ii) = emolmany(ib_out,ii) - omany(ii)
                emolext(ib_out,ii) = emolext(ib_out,ii) - oext(ii)

                otot = orealnonb(ii) + orealbond(ii) + ovdwn(ii) + ovdwb(ii) + opair(ii)   &
                               + othree(ii) + oang(ii) + ofour(ii) + omany(ii) + oext(ii)

                emoltot(ib_out,ii) = emoltot(ib_out,ii) - otot

            enddo

        endif

        !remove the atom and update the neighbourlist
        call remove_atom(ib_out, im_out, atm_out, job%uselist, job%lauto_nbrs)

        if(job% uselist) call setnbrlists(ib_out, job%shortrangecut, job%verletshell,  job%linklist)

        !update the energies for the atom inserted in a box
        energytot(ib_in) = energytot(ib_in) + intotal
        enthalpytot(ib_in) = enthalpytot(ib_in) - inenth
        virialtot(ib_in) = virialtot(ib_in) - invir
        energyreal(ib_in) = energyreal(ib_in) - inreal
        energyvdw(ib_in) = energyvdw(ib_in) + invdw
        energypair(ib_in) = energypair(ib_in) - inpair
        energythree(ib_in) = energythree(ib_in) - inthree
        energyang(ib_in) = energyang(ib_in) - inang
        energyfour(ib_in) = energyfour(ib_in) - infour
        energymany(ib_in) = energymany(ib_in) - inmany
        energyext(ib_in) = energyext(ib_in) - inext
        energymfa(ib_in) = energymfa(ib_in) - inmfa

        if(job%lmoldata) then

            do ii = 1, number_of_molecules
                emolrealnonb(ib_in,ii) = emolrealnonb(ib_in,ii) - irealnonb(ii)
                emolrealbond(ib_in,ii) = emolrealbond(ib_in,ii) - irealbond(ii)
                emolvdwn(ib_in,ii) = emolvdwn(ib_in,ii) - ivdwn(ii)
                emolvdwb(ib_in,ii) = emolvdwb(ib_in,ii) - ivdwb(ii)
                emolpair(ib_in,ii) = emolpair(ib_in,ii) - ipair(ii)
                emolthree(ib_in,ii) = emolthree(ib_in,ii) - ithree(ii)
                emolang(ib_in,ii) = emolang(ib_in,ii) - iang(ii)
                emolfour(ib_in,ii) = emolfour(ib_in,ii) - ifour(ii)
                emolmany(ib_in,ii) = emolmany(ib_in,ii) - imany(ii)
                emolext(ib_in,ii) = emolext(ib_in,ii) - iext(ii)

                itot = irealnonb(ii) + irealbond(ii) + ivdwn(ii) + ivdwb(ii) + ipair(ii)   &
                               + ithree(ii) + iang(ii) + ifour(ii) + imany(ii) + iext(ii)

                emoltot(ib_in,ii) = emoltot(ib_in,ii) - itot

            enddo

        endif

        !update neighbourlist for the other atoms
        if(job% uselist) call setnbrlists(ib_in, job%shortrangecut, job%verletshell,  job%linklist)

        return

    endif
    !else ! reject put back and tidy up - nothing is required for the inserted atom

        !remove the atom that was inserted
1000    call remove_atom(ib_in, im_in, atm_in, job%uselist, job%lauto_nbrs)

    !endif


end subroutine


!*********************************************************************************
! gibbs routine to exchange the atom types - assuming they are of identical charge
!*********************************************************************************
subroutine gibbs_atom_exchange(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                      emolpair, emolang, emolfour, emolmany, emolext, volume)

    use kinds_f90
    use species_module
    use control_type
    use constants_module, only : uout
    use cell_module
    use field_module
    use random_module, only : duni
    use comms_mpi_module, only : master !, mxnode, gsync
    use vdw_module, only : vdw_lrc_energy


    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), &
                                      energyreal(:), energyvdw(:), energythree(:), energypair(:), &
                                      energyang(:), energyfour(:), energymany(:), energyext(:), energymfa(:), &
                                      volume(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), &
                      emolrealnonb(nconfigs,number_of_molecules), &
                      emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                      emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                      emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                      emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                      emolmany(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules)

    integer :: i, j, ii, ib_one, ib_two, im_one, im_two, k, typ_one, typ_two
    integer :: atm_one, atm_two, gno, buf(2)
    integer :: num_typ1_one, num_typ1_two, num_typ2_one, num_typ2_two
    integer :: atyp

    real(kind=wp) :: fact, arg, distmax, deltav_one, deltav_two, deltavb_one, deltavb_two, &
                     otot, itot, onereal, onercp, onevdw, onethree, onepair, oneext, onemfa, &
                     oneang, onefour, onemany, onetotal, oneenth, onevir, tworeal, tworcp, twovdw, &
                     twothree, twopair, twoang, twofour, twomany, twoext, twomfa, twototal, twoenth, twovir

    real(kind = wp) :: oldreal, oldvdw, oldthree, oldpair, oldang, oldfour, &
                       oldmany, oldext, oldmfa, oldtotal, oldenth, oldvir, &
                       newreal, newvdw, newthree, newpair, newang, newfour, &
                       newmany, newext, newmfa, newtotal, newenth, newvir

    !temp vectors for molecular energies
    real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules), orcp(number_of_molecules), &
                       ovdwn(number_of_molecules), ovdwb(number_of_molecules), opair(number_of_molecules), &
                       othree(number_of_molecules),  oang(number_of_molecules), ofour(number_of_molecules), &
                       omany(number_of_molecules), ocrct(number_of_molecules), oext(number_of_molecules), &
                       nrealnonb(number_of_molecules), nrealbond(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules), npair(number_of_molecules), &
                       nthree(number_of_molecules), nang(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules),  ncrct(number_of_molecules), next(number_of_molecules), &
                       irealnonb(number_of_molecules), irealbond(number_of_molecules), ircp(number_of_molecules), &
                       ivdwn(number_of_molecules), ivdwb(number_of_molecules), ipair(number_of_molecules), &
                       ithree(number_of_molecules), iang(number_of_molecules), ifour(number_of_molecules), &
                       imany(number_of_molecules), icrct(number_of_molecules), iext(number_of_molecules)

    logical :: accept

    !TU: Number of each type of atom, for calculating the long-range VdW correction
    integer :: one_old_nums_elemts(number_of_elements), one_new_nums_elemts(number_of_elements), &
               two_old_nums_elemts(number_of_elements), two_new_nums_elemts(number_of_elements)


    onereal = 0.0_wp
    onevdw = 0.0_wp
    onethree = 0.0_wp
    onepair = 0.0_wp
    oneang = 0.0_wp
    onemany = 0.0_wp
    onefour = 0.0_wp
    oneext = 0.0_wp
    onemfa = 0.0_wp
    oneenth = 0.0_wp
    onevir = 0.0_wp

    tworeal = 0.0_wp
    twovdw = 0.0_wp
    twothree = 0.0_wp
    twopair = 0.0_wp
    twoang = 0.0_wp
    twomany = 0.0_wp
    twofour = 0.0_wp
    twoext = 0.0_wp
    twomfa = 0.0_wp
    twoenth = 0.0_wp
    twovir = 0.0_wp

    !first set up appropriate boxes for calculation of energies
    if(ib == 1) then

        ib_one = 1
        ib_two = 2
        im_one = cfgs(ib_one)%num_mols
        im_two = 1

        attemptedgibbsexch_atmb1tob2 = attemptedgibbsexch_atmb1tob2 + 1

    else

        ib_one = 2
        ib_two = 1
        im_two = cfgs(ib_two)%num_mols
        im_one = 1

        attemptedgibbsexch_atmb2tob1 = attemptedgibbsexch_atmb2tob1 + 1

    endif

    one_old_nums_elemts = cfgs(ib_one)%nums_elemts
    one_new_nums_elemts = one_old_nums_elemts
    two_old_nums_elemts = cfgs(ib_two)%nums_elemts
    two_new_nums_elemts = two_old_nums_elemts


    !get the atom pair to be exchanged
    k = int(job%num_atmexch_gibbs * duni() + 1)

    !atom in box one
    typ_one = gibbsexch_atmlookup1(k)
    typ_two = gibbsexch_atmlookup2(k)

    !TU: Obsolete; replace with cfgs(ib)%num_elemts; see 'findnumtype' code for details
    call findnumtype(ib_one, typ_one, num_typ1_one)
    call findnumtype(ib_one, typ_two, num_typ2_one)
    call findnumtype(ib_two, typ_one, num_typ1_two)
    call findnumtype(ib_two, typ_two, num_typ2_two)

    !TU: Check that there are atoms fo the specified species to swap
    if (num_typ1_one == 0 .or. num_typ2_two == 0) return

    !TU: select_atom will hang if there are no atoms of the specified type. The above check
    !TU: ensures this will not occur
    !select the atom in box one to be mutated
    call select_atom(typ_one, ib_one, im_one, atm_one)
    !select the atom in box two to be mutated
    call select_atom(typ_two, ib_two, im_two, atm_two)

    
    atyp = cfgs(ib_one)%mols(im_one)%atms(atm_one)%atype

    oldreal = 0.0_wp
    oldvdw = 0.0_wp
    oldthree = 0.0_wp
    oldpair = 0.0_wp
    oldang = 0.0_wp
    oldmany = 0.0_wp
    oldfour = 0.0_wp
    oldext = 0.0_wp
    oldmfa = 0.0_wp
    oldenth = 0.0_wp
    oldvir = 0.0_wp

    !energy of atom
    if (atyp /= ATM_TYPE_SEMI) then

        if (job%uselist) then

            call atom_energy(ib_one, im_one, atm_one, job%coultype(ib_one), job%dielec, &
                             job%vdw_cut, job%shortrangecut, job%verletshell, &
                             oldreal, oldvdw, oldthree, oldpair, oldang, &
                             oldfour, oldext, oldmfa, oldvir, orealnonb, orealbond, ocrct, &
                             ovdwn, ovdwb, opair, othree, oang, ofour, oext, .false., .false.)

        else

            call atom_energy_nolist(ib_one, im_one, atm_one, job%coultype(ib_one), job%dielec, &
                             job%vdw_cut, job%shortrangecut, oldreal, oldvdw, oldthree, oldpair, &
                             oldang, oldfour, oldext, oldmfa, oldvir, orealnonb, orealbond, ocrct, &
                             ovdwn, ovdwb, opair, othree, oang, ofour, oext, .true., .false.)

        endif

    else

        call atom_tersoff_energy(ib_one, im_one, atm_one, job%shortrangecut, oldmany, oldvir)

    endif

    !TU: Add long-range correction
    oldvdw = oldvdw + vdw_lrc_energy(1.0_wp/volume(ib_one),one_old_nums_elemts)

    oldtotal = oldvdw + oldthree + oldpair + oldang + oldmany + oldfour + oldext + oldmfa

    !oldenth = oldtotal + extpress * volume(ib)

    !mutate the atom
    call mutate_atom(ib_one, im_one, atm_one, gibbsexch_atmlookup2(k))

    !TU: The mutate_atom procedure updates cfgs(ib_one)%nums_elemts to reflect mutation
    one_new_nums_elemts = cfgs(ib_one)%nums_elemts


    !calculate the new energy
    atyp = cfgs(ib_one)%mols(im_one)%atms(atm_one)%atype

    newreal = 0.0_wp
    newvdw = 0.0_wp
    newthree = 0.0_wp
    newpair = 0.0_wp
    newang = 0.0_wp
    newmany = 0.0_wp
    newfour = 0.0_wp
    newext = 0.0_wp
    newmfa = 0.0_wp
    newenth = 0.0_wp
    newvir = 0.0_wp

    if (atyp /= ATM_TYPE_SEMI) then

        if (job%uselist) then

            call atom_energy(ib_one, im_one, atm_one, job%coultype(ib_one), job%dielec, &
                             job%vdw_cut, job%shortrangecut, job%verletshell, newreal, newvdw, &
                             newthree, newpair, newang, newfour, newext, newmfa, newvir,  &
                             nrealnonb, nrealbond, ncrct, nvdwn, nvdwb, npair, nthree, nang, &
                             nfour, next, .false., .false.)

        else

            call atom_energy_nolist(ib_one, im_one, atm_one, job%coultype(ib_one), job%dielec, &
                             job%vdw_cut, job%shortrangecut, newreal, newvdw, newthree, &
                             newpair, newang, newfour, newext, newmfa, newvir,  &
                             nrealnonb, nrealbond, ncrct, nvdwn, nvdwb, npair, nthree, nang, &
                             nfour, next, .true., .false.)

        endif

    else

        call atom_tersoff_energy(ib_one, im_one, atm_one, job%shortrangecut, newmany, newvir)

    endif

    !TU: Add long-range correction
    newvdw = newvdw + vdw_lrc_energy(1.0_wp/volume(ib_one),one_new_nums_elemts)

    newtotal = newvdw + newthree + newpair + newang + newmany + newfour + newext + newmfa
    !newenth = newtotal + extpress * volume(ib)

    !get the value of delta_U_1
    deltav_one = newtotal - oldtotal
    deltavb_one = -beta * deltav_one

    print*,'one',oldtotal,newtotal,deltav_one, deltavb_one


    !!!!!! this is where the atom in box two is mutated
    atyp = cfgs(ib_two)%mols(im_two)%atms(atm_two)%atype

    oldreal = 0.0_wp
    oldvdw = 0.0_wp
    oldthree = 0.0_wp
    oldpair = 0.0_wp
    oldang = 0.0_wp
    oldmany = 0.0_wp
    oldfour = 0.0_wp
    oldext = 0.0_wp
    oldmfa = 0.0_wp
    oldenth = 0.0_wp
    oldvir = 0.0_wp

    !energy of atom
    if (atyp /= ATM_TYPE_SEMI) then

        if (job%uselist) then

            call atom_energy(ib_two, im_two, atm_two, job%coultype(ib_two), job%dielec, &
                             job%vdw_cut, job%shortrangecut, job%verletshell, oldreal, oldvdw, &
                             oldthree, oldpair, oldang, oldfour, oldext, oldmfa, oldvir,  &
                             orealnonb, orealbond, ocrct, ovdwn, ovdwb, opair, othree, oang, &
                             ofour, oext, .false., .false.)

        else

            call atom_energy_nolist(ib_two, im_two, atm_two, job%coultype(ib_two), job%dielec, &
                             job%vdw_cut, job%shortrangecut, oldreal, oldvdw, oldthree, &
                             oldpair, oldang, oldfour, oldext, oldmfa, oldvir,  &
                             orealnonb, orealbond, ocrct, ovdwn, ovdwb, opair, othree, oang, &
                             ofour, oext, .true., .false.)

        endif

    else

        call atom_tersoff_energy(ib_two, im_two, atm_two, job%shortrangecut, oldmany, oldvir)

    endif

    !TU: Add the long-range correction
    oldvdw = oldvdw + vdw_lrc_energy(1.0_wp/volume(ib_two),two_old_nums_elemts)

    oldtotal = oldvdw + oldthree + oldpair + oldang + oldmany + oldfour + oldext + oldmfa

    !oldenth = oldtotal + extpress * volume(ib)

    !mutate the atom
    call mutate_atom(ib_two, im_two, atm_two, gibbsexch_atmlookup1(k))

    !TU: The mutate_atom procedure updates cfgs(ib_in)%nums_elemts to reflect mutation
    two_new_nums_elemts = cfgs(ib_two)%nums_elemts


    !calculate the new energy
    atyp = cfgs(ib_two)%mols(im_two)%atms(atm_two)%atype

    newreal = 0.0_wp
    newvdw = 0.0_wp
    newthree = 0.0_wp
    newpair = 0.0_wp
    newang = 0.0_wp
    newmany = 0.0_wp
    newfour = 0.0_wp
    newext = 0.0_wp
    newmfa = 0.0_wp
    newenth = 0.0_wp
    newvir = 0.0_wp

    if (atyp /= ATM_TYPE_SEMI) then

        if (job%uselist) then

            call atom_energy(ib_two, im_two, atm_two, job%coultype(ib_two), job%dielec, &
                             job%vdw_cut, job%shortrangecut, job%verletshell, newreal, newvdw, &
                             newthree, newpair, newang, newfour, newext, newmfa, newvir,  &
                             nrealnonb, nrealbond, ncrct, nvdwn, nvdwb, npair, nthree, nang, &
                             nfour, next, .false., .false.)

        else

            call atom_energy_nolist(ib_two, im_two, atm_two, job%coultype(ib_two), job%dielec, &
                             job%vdw_cut, job%shortrangecut, newreal, newvdw, newthree, newpair, &
                             newang, newfour, newext, newmfa, newvir,  &
                             nrealnonb, nrealbond, ncrct, nvdwn, nvdwb, npair, &
                             nthree, nang, nfour, next, .true., .false.)

        endif

    else

        call atom_tersoff_energy(ib_two, im_two, atm_two, job%shortrangecut, newmany, newvir)

    endif

    !TU: Add the long-range correction
    newvdw = newvdw + vdw_lrc_energy(1.0_wp/volume(ib_two),two_new_nums_elemts)

    newtotal = newvdw + newthree + newpair + newang + newmany + newfour + newext + newmfa
    !newenth = newtotal + extpress * volume(ib)

    !get the value of delta_U_2
    deltav_two = newtotal - oldtotal
    deltavb_two = -beta * deltav_two

!   print*,'two',oldtotal,newtotal,deltav_two, deltavb_two
    arg = exp(deltavb_one + deltavb_two)
    fact = (num_typ1_two * num_typ2_one) / ((num_typ1_one + 1) * (num_typ2_two + 1))

!   print*,'num',num_typ1_one,num_typ1_two,num_typ2_one,num_typ2_two
!   print*,'arggg', arg, fact, fact*arg,deltavb_one + deltavb_two

    accept = .false.

    if (master) then

        if(duni() < (fact * arg))  accept = .true.

    endif

    !test for accept or rejection
    if (accept) then
print*,'success'
        if(ib == 1) then
            successfulgibbstrans_atmb1tob2 = successfulgibbstrans_atmb1tob2 + 1
        else
            successfulgibbstrans_atmb2tob1 = successfulgibbstrans_atmb2tob1 + 1
        endif

        !update the energies for the atom removed from a box

        energytot(ib_one) = energytot(ib_one) + deltav_one
        enthalpytot(ib_one) = enthalpytot(ib_one) - oneenth
        virialtot(ib_one) = virialtot(ib_one) - onevir
        energyreal(ib_one) = energyreal(ib_one) - onereal
        energyvdw(ib_one) = energyvdw(ib_one) - onevdw
        energypair(ib_one) = energypair(ib_one) - onepair
        energythree(ib_one) = energythree(ib_one) - onethree
        energyang(ib_one) = energyang(ib_one) - oneang
        energyfour(ib_one) = energyfour(ib_one) - onefour
        energymany(ib_one) = energymany(ib_one) - onemany
        energyext(ib_one) = energyext(ib_one) - oneext
        energymfa(ib_one) = energymfa(ib_one) - onemfa

        if(job%lmoldata) then

            do ii = 1, number_of_molecules
                emolrealnonb(ib_one,ii) = emolrealnonb(ib_one,ii) - orealnonb(ii)
                emolrealbond(ib_one,ii) = emolrealbond(ib_one,ii) - orealbond(ii)
                emolvdwn(ib_one,ii) = emolvdwn(ib_one,ii) - ovdwn(ii)
                emolvdwb(ib_one,ii) = emolvdwb(ib_one,ii) - ovdwb(ii)
                emolpair(ib_one,ii) = emolpair(ib_one,ii) - opair(ii)
                emolthree(ib_one,ii) = emolthree(ib_one,ii) - othree(ii)
                emolang(ib_one,ii) = emolang(ib_one,ii) - oang(ii)
                emolfour(ib_one,ii) = emolfour(ib_one,ii) - ofour(ii)
                emolmany(ib_one,ii) = emolmany(ib_one,ii) - omany(ii)
                emolext(ib_one,ii) = emolext(ib_one,ii) - oext(ii)

                otot = orealnonb(ii) + orealbond(ii) + ovdwn(ii) + ovdwb(ii) + opair(ii)   &
                               + othree(ii) + oang(ii) + ofour(ii) + omany(ii) + oext(ii)

                emoltot(ib_one,ii) = emoltot(ib_one,ii) - otot

            enddo

        endif

        if(job% uselist) call setnbrlists(ib_one, job%shortrangecut, job%verletshell,  job%linklist)

        !update the energies for the atom inserted in a box
        energytot(ib_two) = energytot(ib_two) + deltav_two
        enthalpytot(ib_two) = enthalpytot(ib_two) - twoenth
        virialtot(ib_two) = virialtot(ib_two) - twovir
        energyreal(ib_two) = energyreal(ib_two) - tworeal
        energyvdw(ib_two) = energyvdw(ib_two) + twovdw
        energypair(ib_two) = energypair(ib_two) - twopair
        energythree(ib_two) = energythree(ib_two) - twothree
        energyang(ib_two) = energyang(ib_two) - twoang
        energyfour(ib_two) = energyfour(ib_two) - twofour
        energymany(ib_two) = energymany(ib_two) - twomany
        energyext(ib_two) = energyext(ib_two) - twoext
        energymfa(ib_two) = energymfa(ib_two) - twomfa

        if(job%lmoldata) then

            do ii = 1, number_of_molecules
                emolrealnonb(ib_two,ii) = emolrealnonb(ib_two,ii) - irealnonb(ii)
                emolrealbond(ib_two,ii) = emolrealbond(ib_two,ii) - irealbond(ii)
                emolvdwn(ib_two,ii) = emolvdwn(ib_two,ii) - ivdwn(ii)
                emolvdwb(ib_two,ii) = emolvdwb(ib_two,ii) - ivdwb(ii)
                emolpair(ib_two,ii) = emolpair(ib_two,ii) - ipair(ii)
                emolthree(ib_two,ii) = emolthree(ib_two,ii) - ithree(ii)
                emolang(ib_two,ii) = emolang(ib_two,ii) - iang(ii)
                emolfour(ib_two,ii) = emolfour(ib_two,ii) - ifour(ii)
                emolmany(ib_two,ii) = emolmany(ib_two,ii) - imany(ii)
                emolext(ib_two,ii) = emolext(ib_two,ii) - iext(ii)

                itot = irealnonb(ii) + irealbond(ii) + ivdwn(ii) + ivdwb(ii) + ipair(ii)   &
                               + ithree(ii) + iang(ii) + ifour(ii) + imany(ii) + iext(ii)

                emoltot(ib_two,ii) = emoltot(ib_two,ii) - itot

            enddo

        endif

        !update neighbourlist for the other atoms
        if(job% uselist) call setnbrlists(ib_two, job%shortrangecut, job%verletshell,  job%linklist)

    else ! reject put back and tidy up - nothing is required for the inserted atom

        !change the atoms back
        call mutate_atom(ib_one, im_one, atm_one, gibbsexch_atmlookup1(k))
        call mutate_atom(ib_two, im_two, atm_two, gibbsexch_atmlookup2(k))

    endif


end subroutine


subroutine gibbs_mol_transfer(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, emolvdwn, &
                      emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext, volume)

    use kinds_f90
    use species_module
    use control_type
    use constants_module, only : uout
    use cell_module
    use field_module
    use random_module, only : duni
    use comms_mpi_module, only : master
    use vdw_module, only : vdw_lrc_energy


    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), &
                                      energyreal(:), energyvdw(:), energythree(:), energypair(:), &
                                      energyang(:), energyfour(:), energymany(:), energyext(:), &
                                      energymfa(:), volume(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), &
                     emolrealnonb(nconfigs,number_of_molecules), &
                     emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                     emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                     emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                     emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                     emolmany(nconfigs,number_of_molecules), emolrealself(nconfigs,number_of_molecules), &
                     emolrealcrct(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules)

    integer :: i, j, ib_in, ib_out, ngas_out, ngas_in, im_in, im_out, ii, typ, choice
    integer :: mol_in, mol_out, gno, igrid, fail, buf(2)

    real(kind=wp) :: arg, distmax, deltav, deltavb, otot, itot, &
                     outreal, outrcp, outvdw, outthree, outpair, &
                     outang, outfour, outmany, outext, outmfa, outtotal, outenth, outvir, &
                     inreal, inrcp, invdw, inthree, inpair, &
                     inang, infour, inmany, inext, inmfa, intotal, inenth, invir, &
                     pcav, inselfcoul, inselfvdw, outselfcoul, outselfvdw

     !temp vectors for molecular energies
    real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules), orcp(number_of_molecules), &
                       ovdwn(number_of_molecules), ovdwb(number_of_molecules), opair(number_of_molecules), &
                       othree(number_of_molecules),  oang(number_of_molecules), ofour(number_of_molecules), &
                       omany(number_of_molecules), ocrct(number_of_molecules), oself(number_of_molecules), &
                       oext(number_of_molecules), &
                       irealnonb(number_of_molecules), irealbond(number_of_molecules), ircp(number_of_molecules), &
                       ivdwn(number_of_molecules), ivdwb(number_of_molecules), ipair(number_of_molecules), &
                       ithree(number_of_molecules), iang(number_of_molecules), ifour(number_of_molecules), &
                       imany(number_of_molecules), icrct(number_of_molecules), iself(number_of_molecules), &
                       iext(number_of_molecules)

    logical :: atom_out, overlap, accept, by_com

    !TU: Number of each type of atom, for calculating the long-range VdW correction
    integer :: out_old_nums_elemts(number_of_elements), out_new_nums_elemts(number_of_elements)
    integer ::  in_old_nums_elemts(number_of_elements),  in_new_nums_elemts(number_of_elements)
    integer :: iatom, atlabel


    by_com = .false.

    outreal = 0.0_wp
    outrcp = 0.0_wp
    outvdw = 0.0_wp
    outthree = 0.0_wp
    outpair = 0.0_wp
    outang = 0.0_wp
    outfour = 0.0_wp
    outmany = 0.0_wp
    outext = 0.0_wp
    outmfa = 0.0_wp
    outenth = 0.0_wp
    outvir = 0.0_wp

    inreal = 0.0_wp
    inrcp = 0.0_wp
    invdw = 0.0_wp
    inthree = 0.0_wp
    inpair = 0.0_wp
    inang = 0.0_wp
    infour = 0.0_wp
    inmany = 0.0_wp
    inext = 0.0_wp
    inmfa = 0.0_wp
    inenth = 0.0_wp
    invir = 0.0_wp

    fail = 0

    !first set up appropriate boxes for calculation of energies
    if(ib == 1) then

        ib_out = 1
        ib_in = 2
        im_out = int(cfgs(ib_out)%num_mols * duni() + 1)
        im_in = 1

        attemptedgibbstrans_molb1tob2 = attemptedgibbstrans_molb1tob2 + 1

    else

        ib_out = 2
        ib_in = 1
        im_in = cfgs(ib_in)%num_mols + 1
        im_out = int(cfgs(ib_out)%num_mols * duni() + 1)

        attemptedgibbstrans_molb2tob1 = attemptedgibbstrans_molb2tob1 + 1

    endif

    out_old_nums_elemts = cfgs(ib_out)%nums_elemts
    out_new_nums_elemts = out_old_nums_elemts
    in_old_nums_elemts = cfgs(ib_in)%nums_elemts
    in_new_nums_elemts = in_old_nums_elemts

    !get the atom type to transferred
    choice = int(job%num_moltran_gibbs * duni() + 1)

    typ = gibbstrans_mollookup(choice)

    !insert the molecule
    ngas_in = cfgs(ib_in)%mtypes(typ)%num_mols

    ! make sure dont go over array limits
    if (ngas_in > cfgs(ib_in)%mxmol) call error(208)

    !TU: Obsolete; replace with cfgs(ib)%num_elemts; see 'findnumtype' code for details
    !check that there is an atom there for ib1
    call findnumtype(ib_out, typ, ngas_out)

    !TU-: WARNING: Returning the function here is risky? Even if the move is rejected it should still
    !TU-: be used in gathering statistics. There is a risk that statistics gathered after this line of
    !TU: code would not occur if ngas_out==0
    if (ngas_out == 0) return

    by_com = ( uniq_mol(typ)%natom > 1 .and. &
             ( uniq_mol(typ)%rigid_body .or. uniq_mol(typ)%blist%npairs > 0 ) )

    if(job%usecavitybias) then


        !TU: Corrected a bug I think here; replaced the following line with the one after
        !call insert_atom_atcavity(ib_in, typ, im_in, mol_in, igrid, pcav, job%lauto_nbrs, atom_out)
        call insert_molecule_atcavity(ib_in, typ, im_in, typ, igrid, pcav, atom_out, by_com)

        if( atom_out ) then

            call remove_molecule(ib_in, im_in, fail) !, job%uselist, job%lauto_nbrs)
            if( fail > 0 ) call error(fail)
            return

        endif

        if (pcav < 0.0) then !< 0.0 means no cavities - do random insert

            !TU: Corrected a bug I think here; replaced the following line with the one after
            !call insert_atom(ib_in, typ, im_in, mol_in, job%lauto_nbrs, atom_out)
            call insert_molecule(ib_in, typ, im_in, atom_out, by_com)

            pcav = 1.0

            if( atom_out ) then

                call remove_molecule(ib_in, im_in, fail) !, job%uselist, job%lauto_nbrs)
                if( fail > 0 ) call error(fail)
                return

            endif

        endif

    else

        !find a position to place molecule - it is always placed at the end of the list
        call insert_molecule(ib_in, typ, im_in, atom_out, by_com)
        pcav = 1.0

        if( atom_out ) then

            call remove_molecule(ib_in, im_in, fail) !, job%uselist, job%lauto_nbrs)
            if( fail > 0 ) call error(fail)
            return

        endif

    endif

    !TU: The insert_molecule procedure above updates cfgs(ib)%nums_elemts to reflect insertion
    in_new_nums_elemts = cfgs(ib_in)%nums_elemts


!AB: all insertion attempts have been treated already above
!    if( atom_out ) then
!
!        call remove_molecule(ib_in, im_in, fail) !, job%uselist, job%lauto_nbrs)
!        if( fail > 0 ) call error(fail)
!        return
!
!    endif

    ! calculate distances and nbrlist
    if(job%uselist) then

        call set_mol_nbrlist_overlap(ib_in, im_in, &
                 job%shortrangecut, job%verletshell, job%gibbsmindist, overlap)

        if (overlap) then

            call remove_molecule(ib_in, im_in, fail) !, job%uselist, job%lauto_nbrs)
            if( fail > 0 ) call error(fail)
            call clear_mol_nbrlist(ib_in, im_in)
            return

        endif

    else

        call molecule_overlap(ib_in, im_in, job%gibbsmindist, overlap)

        if (overlap) then

            call remove_molecule(ib_in, im_in, fail) !, job%uselist, job%lauto_nbrs)
            if( fail > 0 ) call error(fail)
            return

        else

            call set_molecule_exclist(ib_in, im_in)

        endif

    endif

    ! evaluate energy
    if(job%uselist) then

        call molecule_energy2(ib_in, im_in, job%coultype(ib_in), job%dielec, &
                     job%vdw_cut, job%shortrangecut, job%verletshell, &
                     inreal, inselfcoul, invdw, inselfvdw, inthree, &
                     inpair, inang, infour, inmany, inext, inmfa, invir, &
                     irealnonb, irealbond, iself, icrct, ivdwn, ivdwb, &
                     ipair, ithree, iang, ifour, imany, iext)

    else

        call molecule_energy2_nolist(ib_in, im_in, job%coultype(ib_in), job%dielec, &
                     job%vdw_cut, job%shortrangecut, &
                     inreal, inselfcoul, invdw, inselfvdw, inthree, &
                     inpair, inang, infour, inmany, inext, inmfa, invir,  &
                     irealnonb, irealbond, iself, icrct, ivdwn, ivdwb, &
                     ipair, ithree, iang, ifour, imany, iext)

    endif

    if (job%coultype(ib_in) == 1) then

        !JG: molecule_recip4 now deprecated replaced by insert_molecule_recip
        !call molecule_recip4(ib_in, im_in, inrcp, ircp, job%lmoldata)
        call insert_molecule_recip(ib_in, im_in, inrcp, ircp, job%lmoldata)
        inrcp = inrcp - energyrcp(ib_in)
        ircp(:) = ircp - emolrcp(ib_in,:)

    endif

    !TU: invdw is actually the change in VdW relative to the old configuration. Hence
    !TU: add the CHANGE in long-range VdW correction
    invdw = invdw + ( vdw_lrc_energy(1.0_wp/volume(ib_in),in_new_nums_elemts) &
                    - vdw_lrc_energy(1.0_wp/volume(ib_in),in_old_nums_elemts) )

    intotal = inreal + invdw + inthree + inpair + inang + infour + inmany &
            + inselfcoul + inselfvdw + inrcp + inext + inmfa

    !chalculate chemical potential
    !!chem_pot_mol(ib1,typ) = volume(ib1) * exp(-beta * intotal) / (natom + 1);

    !TU: select_molecule would hang if there were no mols of the target species - won't happen due to check above
    call select_molecule(typ, ib_out, im_out)

    !TU: Note that we haven't yet removed the molecule (which happens only upon acceptance)
    !TU: and so must manually set new_nums_elemts (instead of just setting it to cfgs(ib)%nums_elemts)
    do iatom = 1, cfgs(ib_out)%mols(im_out)%natom

        atlabel = cfgs(ib_out)%mols(im_out)%atms(iatom)%atlabel
        out_new_nums_elemts(atlabel) = out_new_nums_elemts(atlabel) - 1

    end do


    !get energy of the atom being removed
    ! evaluate energy
    if(job%uselist) then

        call molecule_energy2(ib_out, im_out, job%coultype(ib_out), job%dielec, &
                     job%vdw_cut, job%shortrangecut, job%verletshell, &
                     outreal, outselfcoul, outvdw, outselfvdw, outthree, &
                     outpair, outang, outfour, outmany, outext, outmfa, outvir, &
                     orealnonb, orealbond, oself, ocrct, ovdwn, ovdwb, &
                     opair, othree, oang, ofour, omany, oext)

    else

        call molecule_energy2_nolist(ib_out, im_out, job%coultype(ib_out), &
                     job%dielec, job%vdw_cut, job%shortrangecut, &
                     outreal, outselfcoul, outvdw, outselfvdw, outthree, &
                     outpair, outang, outfour, outmany, outext, outmfa, outvir, &
                     orealnonb, orealbond, oself, ocrct, ovdwn, ovdwb, &
                     opair, othree, oang, ofour, omany, oext)

    endif

    if (job%coultype(ib_out) == 1) then

        !JG: molecule_recip3 deprecated replaced by remove_molecule_recip
        !call molecule_recip3(ib_out, im_out, outrcp, orcp, job%lmoldata)
        call remove_molecule_recip(ib_out, im_out, outrcp, orcp, job%lmoldata)

        outrcp = energyrcp(ib_out) - outrcp
        orcp(:) = emolrcp(ib,:) - orcp(:)

    endif

    !TU: outvdw is actually the negative of the change in VdW relative to the old configuration. Hence
    !TU: add the negative of the CHANGE in long-range VdW correction
    outvdw = outvdw - ( vdw_lrc_energy(1.0_wp/volume(ib_out),out_new_nums_elemts) &
                      - vdw_lrc_energy(1.0_wp/volume(ib_out),out_old_nums_elemts) )

    outtotal = outreal + outvdw + outthree + outpair + outang + outfour + outmany &
             + outselfcoul + outselfvdw + outrcp + outext + outmfa

    arg = (  ngas_out*volume(ib_in) / ((ngas_in+1)*volume(ib_out))  ) * exp( -beta * (intotal - outtotal)  )

    accept = .false.

    if (master) then

        if(duni() < arg)  accept = .true.

    endif

    !test for accept or rejection
    if (accept) then

        if(ib == 1) then
            successfulgibbstrans_molb1tob2 = successfulgibbstrans_molb1tob2 + 1
        else
            successfulgibbstrans_molb2tob1 = successfulgibbstrans_molb2tob1 + 1
        endif

        !update the energies for the atom removed from a box

        energytot(ib_out) = energytot(ib_out) - outtotal
        enthalpytot(ib_out) = enthalpytot(ib_out) - outenth
        virialtot(ib_out) = virialtot(ib_out) - outvir
        energyrcp(ib_out) = energyrcp(ib_out) - outrcp
        energyreal(ib_out) = energyreal(ib_out) - (outreal + outselfcoul)
        energyvdw(ib_out) = energyvdw(ib_out) - (outvdw + outselfvdw)
        energypair(ib_out) = energypair(ib_out) - outpair
        energythree(ib_out) = energythree(ib_out) - outthree
        energyang(ib_out) = energyang(ib_out) - outang
        energyfour(ib_out) = energyfour(ib_out) - outfour
        energymany(ib_out) = energymany(ib_out) - outmany
        energyext(ib_out) = energyext(ib_out) - outext
        energymfa(ib_out) = energymfa(ib_out) - outmfa

        if(job%lmoldata) then

            do ii = 1, number_of_molecules
                emolrealnonb(ib_out,ii) = emolrealnonb(ib_out,ii) - orealnonb(ii)
                emolrealbond(ib_out,ii) = emolrealbond(ib_out,ii) - orealbond(ii)
                emolvdwn(ib_out,ii) = emolvdwn(ib_out,ii) - ovdwn(ii)
                emolvdwb(ib_out,ii) = emolvdwb(ib_out,ii) - ovdwb(ii)
                emolpair(ib_out,ii) = emolpair(ib_out,ii) - opair(ii)
                emolthree(ib_out,ii) = emolthree(ib_out,ii) - othree(ii)
                emolang(ib_out,ii) = emolang(ib_out,ii) - oang(ii)
                emolfour(ib_out,ii) = emolfour(ib_out,ii) - ofour(ii)
                emolmany(ib_out,ii) = emolmany(ib_out,ii) - omany(ii)
                emolext(ib_out,ii) = emolext(ib_out,ii) - oext(ii)

                otot = orealnonb(ii) + orealbond(ii) + ovdwn(ii) + ovdwb(ii) + opair(ii)   &
                               + othree(ii) + oang(ii) + ofour(ii) + omany(ii) + oext(ii)

                emoltot(ib_out,ii) = emoltot(ib_out,ii) - otot

            enddo

        endif

        !update the energies for the atom inserted in a box
        energytot(ib_in) = energytot(ib_in) + intotal
        enthalpytot(ib_in) = enthalpytot(ib_in) - inenth
        virialtot(ib_in) = virialtot(ib_in) - invir
        energyrcp(ib_in) = energyrcp(ib_in) + inrcp
        energyreal(ib_in) = energyreal(ib_in) + (inreal + inselfcoul)
        energyvdw(ib_in) = energyvdw(ib_in) + (invdw + inselfvdw)
        energypair(ib_in) = energypair(ib_in) - inpair
        energythree(ib_in) = energythree(ib_in) - inthree
        energyang(ib_in) = energyang(ib_in) - inang
        energyfour(ib_out) = energyfour(ib_in) - infour
        energymany(ib_in) = energymany(ib_in) - inmany
        energyext(ib_in) = energyext(ib_in) - inext
        energymfa(ib_in) = energymfa(ib_in) - inmfa

        if(job%lmoldata) then

            do ii = 1, number_of_molecules
                emolrealnonb(ib_in,ii) = emolrealnonb(ib_in,ii) - irealnonb(ii)
                emolrealbond(ib_in,ii) = emolrealbond(ib_in,ii) - irealbond(ii)
                emolvdwn(ib_in,ii) = emolvdwn(ib_in,ii) - ivdwn(ii)
                emolvdwb(ib_in,ii) = emolvdwb(ib_in,ii) - ivdwb(ii)
                emolpair(ib_in,ii) = emolpair(ib_in,ii) - ipair(ii)
                emolthree(ib_in,ii) = emolthree(ib_in,ii) - ithree(ii)
                emolang(ib_in,ii) = emolang(ib_in,ii) - iang(ii)
                emolfour(ib_in,ii) = emolfour(ib_in,ii) - ifour(ii)
                emolmany(ib_in,ii) = emolmany(ib_in,ii) - imany(ii)
                emolext(ib_in,ii) = emolext(ib_in,ii) - iext(ii)

                itot = irealnonb(ii) + irealbond(ii) + ivdwn(ii) + ivdwb(ii) + ipair(ii)   &
                               + ithree(ii) + iang(ii) + ifour(ii) + imany(ii) + iext(ii)

                emoltot(ib_in,ii) = emoltot(ib_in,ii) - itot

            enddo

        endif

        call remove_molecule(ib_out, im_out, fail) !, job%uselist, job%lauto_nbrs)
        if( fail > 0 ) call error(fail)

        if(job%uselist) then

            call setnbrlists(ib_in, job%shortrangecut, job%verletshell,  job%linklist)
            call setnbrlists(ib_out, job%shortrangecut, job%verletshell,  job%linklist)

        else

            call set_exclist(ib_in)
            call set_exclist(ib_out)

        endif

        if(job%coultype(ib) == 1) then

            !calculate Ewald sums again as out of sync
            call total_recip(ib_in, outmany, omany, outvir)
            call update_rcpsums(ib_in)
            call total_recip(ib_out, outmany, omany, outvir)
            call update_rcpsums(ib_out)

        endif

        !update neighbourlist for the other atoms
        if(job% uselist) call setnbrlists(ib_in, job%shortrangecut, job%verletshell,  job%linklist)

    else ! reject put back and tidy up - nothing is required for the inserted atom

        !remove the atom that was inserted
        call remove_molecule(ib_in, im_in, fail) !, job%uselist, job%lauto_nbrs)
        if( fail > 0 ) call error(fail)

    endif


end subroutine

end module
