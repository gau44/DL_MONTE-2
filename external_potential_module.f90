! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module external_potential_module

  !!
  !! Stephen J. Cox
  !! May 2011
  !!
  !! stephen.cox.10@ucl.ac.uk

  use kinds_f90

  implicit none

  !> number of external potentials
  integer, save :: nextpot

  !> number of parameters in the external potential
  integer, parameter :: mxpext=10
  ! Might consider to making this dependent on the 
  ! type of potential chosen (these external potentials)
  ! could get quite confusing. This would mean making 
  ! "parpot" in "read_external_pot" routine allocatable
  
  !> look up table for extpot and types of external potenial
  integer,  allocatable, save :: lstext(:)
  integer,  allocatable, save :: ltpext(:)
  
  !> external potential parameters
  real(kind = wp), allocatable, save :: prmext(:,:)

contains

  subroutine allocate_extpot_arrays(nextern)

    use kinds_f90
    use constants_module, only: uout
    use species_module,   only: number_of_elements

    implicit none
    integer, intent(in) :: nextern

    integer, parameter :: err=171

    integer, dimension(3) :: fail
    integer :: ntpatm

    fail = 0

    ntpatm = number_of_elements

    allocate(lstext(ntpatm ),          stat = fail(1))
    allocate(ltpext(nextern),          stat = fail(2))
    allocate(prmext(0:mxpext,nextern), stat = fail(3))

    if(any(fail > 0)) call error(err)

    nextpot = nextern

    lstext(:) = 0
    ltpext(:) = 0
    prmext(:,:) = 0.0_wp

  end subroutine allocate_extpot_arrays

  subroutine read_external_pot(engunit, vdw_rcut, vdw_ecap)
    
    use kinds_f90
    use constants_module
    use control_type
    use parse_module
    use species_module
    use slit_module, only : in_bulk
    use comms_mpi_module, only : master
    !use parallel_loop_module, only : master
    
    implicit none
    
    real (kind = wp), intent(in) :: engunit, vdw_rcut, vdw_ecap

    integer, parameter :: err1=181, err2=182, err3=183, err4=184

    real(kind = wp) :: parpot(0:mxpext), dc126, a, b, c

    integer  :: nread, iextpot, keypot
    integer  :: katom1, jtpatm, ntpatm, atm1typ, maxpar, j, i
    logical  :: safe, isflt

    character(len=100) :: line
    character(len=40 ) :: word 
    character(len=8  ) :: keyword
    character(len=8  ) :: atom1
    
    !AB: short-range core separations at the repuslion cap:
    !12-6 potential (LJ is also set via this function)

    dc126(a,b,c)=( (-b+sqrt(b*b+4.0_wp*a*c)) / (2.0_wp*c) )**(1.0_wp/6.0_wp)

    nread   = ufld
    ntpatm  = number_of_elements
    
    if( master ) then 

        if( in_bulk ) then

            write(uout,"(/,/,' using external potential(s) along Z dimension in 3D PBC: ',/)")
            write(uout,"(/,' please reference the following paper ',/)")
            write(uout,"(/,' S.J. Cox, S.M. Kathmann, J.A. Purton, M.J. Gillan and A. Michaelides ',/)")
            write(uout,"(/,' Phys. Chem. Cem. Phys. 2012, 14, 7944-7949 ',/)")

        else

            write(uout,"(/,/,' using external potential(s) along Z dimension in 2D PBC (slit): ',/)")
            write(uout,"(/,' please reference the following publications ',/)")
            write(uout,"(/,a,/)")" Broukhno A.V., `Free Energy and Surface Forces in polymer systems:"
            write(uout,"(/,a,/)")" Monte Carlo Simulation Studies`, PhD Thesis, Lund, Sweden 2003"
            write(uout,"(/,a,/)")" Brukhno A.V., Akinshina A., Coldrick Z., Nelson A. and Auer S."
            write(uout,"(/,a,/)")" `Phase phenomena in supported lipid films under varying electric potential`"
            write(uout,"(/,a,/)")" Soft Matter. 2011, 7, 1006"

        endif

    endif

    do iextpot = 1, nextpot

       parpot = 0.0_wp

       !AB: number of the compulsory parameters (max)
       maxpar = mxpext

       call get_line(safe, nread, line)
       if(.not. safe) call error(err1)            ! Something's wrong with the ext pot line

       call get_word(line,word)
       atom1 = word

       call get_word(line,word)
       atm1typ = get_species_type(word)

       call get_word(line,word)
       call lower_case(word)

       keyword = word

       if( keyword == 'hs' .or. keyword == 'sw' .or. keyword == 'hsqw' ) then
       !TU: hard-sphere (hard-core) plus square-well potential

           keypot = -1
           maxpar = 2

       else if( keyword == 'lj' .or. keyword == 'slj' .or. keyword == 'surflj' ) then
       !JP: Lennard-Jones potential

           keypot = 1 ! <- 2
           maxpar = 2

       else if( keyword == '12-6' .or. keyword == 's12-6' .or. keyword == 'surf12-6' ) then
       !JP: 12-6 potential

           keypot = 2 ! <- 1
           maxpar = 2

       else if( keyword == '9-3' .or. keyword == 's9-3' .or. keyword == 'surf9-3' ) then
       !AB: 9-3 potential

           keypot = 3 ! <- 12
           maxpar = 2

       else if( keyword == '10-4' .or. keyword == 's10-4' .or. keyword == 'surf10-4' ) then
       !AB: 10-4 potential

           keypot = 4 ! <- 13
           maxpar = 2

       else if( keyword == 'wca' .or. keyword == 'wcalj' ) then
       !AB: Weeks-chandler-Andersen (WCA) - center-shifted & truncated LJ repulsion (only!) (9)
       !AB: center-shifted, truncated-&-shifted LJ all go as 'lj' now

           keypot = 5 ! <- 9
           maxpar = 2

       else if( keyword == 'hbnd' ) then
       !JP: Hydrogen-bond 12-10 potential (6)

           keypot = 6
           maxpar = 2

       else if( keyword == 'ew' ) then
       !JP: Espanol - Warren (10)

           keypot = 7 ! <- 10
           maxpar = 2

       else if( keyword == 'buck' ) then
       !JP: Buckingham exp-6 potential (4)

           keypot = 8 ! <- 4
           maxpar = 3

       else if( keyword == 'mors' .or. keyword == 'morl' ) then
       !JP: Morse potential (8)

           keypot = 9 ! <- 8
           maxpar = 4

       else if( keyword == 'pnm' .or. keyword == 'pwnm' .or. keyword == 'ljnm' ) then
       !AB: simple two powers potential: u(r)=a*(d/r)^n-b*(d/r)^m (11)

           keypot = 10 ! <- 11
           maxpar = 4

       else if (keyword ==    'zharm') then
          keypot = 11
       else if (keyword ==  'xyzharm') then
          keypot = 12
       else if (keyword ==   'zmorse') then
          keypot = 13
       else if (keyword == 'xy-zmors') then
          keypot = 14
       else if (keyword == 'zmors110') then
          keypot = 15

       else if (keyword == 'tophat') then

          !TU: Tophat function - see below for details
          keypot = 16

       else
          call error(err2)                        ! Given potential undefined
       end if

       do i=1, mxpext
          call get_word(line,word)
          if( word == "" .or. word(1:1) == "#" .or. indexnum(word,isflt) < 1) exit
          maxpar = i
          !if( word == "???" ) exit
          parpot(i)=word_2_real(word)
       end do
       
       !AB: index of the first optional parameter (if any)
       maxpar = maxpar+1

       if(master ) write(uout,'(1x,8x,a8,1x,a8,1x,5f15.6,/,&
                                & 27x,              5f15.6,/,&
                                & 27x,              5f15.6    )')&
            atom1, keyword, (parpot(j),j=1,mxpext)

       katom1=0

       do jtpatm=1,ntpatm
          if (atom1 == element(jtpatm) .and. atm1typ == eletype(jtpatm)) katom1=jtpatm
       end do

       if (katom1 == 0) call error(err3)          ! Atom given not defined 
       
       ! flag this atom as interacting with extpot
       lstext(katom1) = iextpot

       ! set the type of potential
       ltpext(iextpot) = keypot
       
       ! convert energies to internal unit (except when not needed)
       if( keypot > 0 .and. keypot < 11 ) parpot(1) = parpot(1)*engunit

       ! convert energies to internal unit
       select case(keypot)

          case(-1)
          !if (keypot == -1) then
            !TU: hard-sphere (hard-core) + square well potential  ('hs'/'hsqw' -> -1)

            if( parpot(1) < 0.0_wp ) then
                if( master ) write(uout,*) "ERROR: Wall hard-core interaction distance is negative!"
                call error(999)
            end if

            !if( parpot(1) > vdw_rcut ) then
            !    if( master ) write(uout,*) "ERROR: Hard-sphere interaction radius > cut-off radius!"
            !    call error(999)
            !end if

            parpot(0) = parpot(1)

            if( abs(parpot(2)) > 1.0e-10_wp ) then

                !if( parpot(2) > vdw_rcut ) then
                !    if( master ) write(uout,*) "ERROR: Square-well interaction radius > VdW cut-off!"
                !    call error(999)
                !end if

                if( parpot(2) < parpot(1) ) then
                    if( master ) write(uout,*) "ERROR: Wall square-well interaction distance < hard-core!"
                    call error(999)
                end if

            end if

            !AB: it makes sense to allow either attractive well or repulsive shoulder!
            if( abs(parpot(3)) > 1.0e-10_wp ) parpot(3) = parpot(3) * engunit

          case(1)
        !else if( keypot == 1 ) then !if (keypot == 2) then

            !JP: Lennard-Jones potential
            !AB: also shifted Lennard-Jones potential 
            !AB: also surface centered Lennard-Jones potential

            !maxpar = 3

            !call get_word(line,word)
            !if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            !AB: parpot(3) sets the distance from the particle centre to the 12-6 origin (i.e. surface diameter)
            !AB: so the 12-6 interaction is shifted to the particle surface(s)

            if( maxpar == 3 ) then

              if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*)'ERROR: negative surface diameter for the LJ (Ext) surface!!! ',iextpot

                call error(999)

              else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'LJ (Ext) pair potential (',iextpot,') origin is shifted to D_surf = ',parpot(3)

              end if

            end if

            !AB: short-range core separation at the repuslion cap:
            parpot(0) = dc126( 4.0_wp*parpot(1)*parpot(2)**12, 4.0_wp*parpot(1)*parpot(2)**6, vdw_ecap )

        case(2)
        !else if( keypot == 2 ) then !if (keypot == 1) then

            !JP: 12-6 potential ('12-6' -> 1)
            !AB: also shifted 12-6 potential
            !AB: also surface centered 12-6 potential

            parpot(2)=parpot(2)*engunit

            !maxpar = 3

            !call get_word(line,word)
            !if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            !AB: parpot(3) sets the distance from the particle centre to the 12-6 origin (i.e. surface diameter)
            !AB: so the 12-6 interaction is shifted to the particle surface(s)

            if( maxpar == 3 ) then

              if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*)'ERROR: negative surface diameter for the 12-6 surface!!! ',iextpot

                call error(999)

              else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    '12-6 (Ext) pair potential (',iextpot,') origin is shifted to D_surf = ',parpot(3)

              end if

            end if

            !AB: short-range core separation at the repuslion cap:
            parpot(0) = dc126( parpot(1), parpot(2), vdw_ecap )

        case(3,4)
        !else if( keypot == 3 .or. keypot == 4 ) then !if( keypot == 12 .or. keypot == 13 ) then

            !JG: a/r^n-b/r^m potential, n = [9,10] and m = [3,4]

            if ( parpot(1) < 0.0_wp ) then

                if( master ) write(uout,*) "ERROR: power (9-3 or 10-4) repulsion parameter (a) must be positive!!!"

                call error(999)

            end if

            if ( parpot(2) < 0.0_wp ) then

                if( master ) write(uout,*) "ERROR: power (9-3 or 10-4) attraction parameter (b) must be positive!!!"

                call error(999)

            end if

            parpot(2) = parpot(2)*engunit

            !maxpar = 3

            !call get_word(line,word)
            !if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            if( maxpar == 3 ) then

              if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*) &
                             'ERROR: negative surface diameter for the Power n-m (LJ-type Ext) core surface!!! ',iextpot

                call error(999)

              else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'Power n-m (LJ-type Ext) pair potential (',iextpot,') origin is shifted to D_surf = ',parpot(maxpar)

              end if

            end if

        case(5)
        !else if (keypot == 5) then !if (keypot == 9) then

            !JP: Weeks-chandler-Anderson - shifted & truncated Lenard-Jones ('wca' -> 9)

            parpot(2) = Abs(parpot(2))

            !AB: reset the shift of origin if unsuitable value is given in the input

            if( parpot(3) > parpot(2)/2.0_wp ) &
                parpot(3) = Sign(1.0_wp,parpot(3))*parpot(2)/2.0_wp

            parpot(4) = 2.0_wp**(1.0_wp/6.0_wp)*parpot(2)+parpot(3)

            !AB: judging by the original code in vdw_interpolate_module.f90::vdw_generate(..)
            !AB: parpot(4) serves as an extra cut-off specific to this interaction
            !AB: but it makes more sense to do the check and resetting here rather than there

            !AB: make sure it's <= cut-off
            !if( parpot(4) == 0.0_wp) parpot(4) = vdw_rcut
            if( parpot(4) == 0.0_wp .or. parpot(4) > vdw_rcut ) parpot(4) = vdw_rcut

            !maxpar = 3

            !call get_word(line,word)
            !if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            if( maxpar == 3 ) then

              if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*) &
                             'ERROR: negative surface diameter for the WCA-LJ (Ext) core surface!!! ',iextpot

                call error(999)

              else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'WCA-LJ (Ext) pair potential (',iextpot,') origin is shifted to D_surf = ',parpot(maxpar)

              end if

            end if

        case(6)
        !else if (keypot == 6) then

            !JP: Hydrogen-bond 12-10 potential ('hbnd' -> 6)

            parpot(2)=parpot(2)*engunit

            !maxpar = 3

            !call get_word(line,word)
            !if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            if( maxpar == 3 ) then

              if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*) &
                             'ERROR: negative surface diameter for the Hydrogen-bond (Ext) core surface!!! ',iextpot

                call error(999)

              else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'Hydrogen-bond (Ext) pair potential (',iextpot,') origin is shifted to D_surf = ',parpot(maxpar)

              end if

            end if

        case(7)
        !else if (keypot == 7) then !if (keypot == 10) then

            !JP: espanol - warren ('ew' -> 10)

            !AB: originally there was nothing here in terms of the input for this case

            !AB: judging by the original code in vdw_interpolate_module.f90::vdw_generate(..)
            !AB: parpot(2) serves as an extra cut-off specific to this interaction
            !AB: but it makes more sense to do the check and resetting here rather than there

            !AB: make sure it's <= cut-off
            !if( parpot(2) == 0.0_wp) parpot(2) = vdw_rcut
            if( parpot(2) == 0.0_wp .or. parpot(2) > vdw_rcut ) parpot(2) = vdw_rcut

            !maxpar = 3

            !call get_word(line,word)
            !if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            if( maxpar == 3 ) then

              if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*) &
                             'ERROR: negative surface diameter for the Espanol-Warren (Ext) core surface!!! ',iextpot

                call error(999)

              else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'Espanol-Warren (Ext) pair potential (',iextpot,') origin is shifted to D_surf = ',parpot(maxpar)

              end if

            end if

        case(8)
        !else if (keypot == 8) then !if (keypot == 4) then

            !JP: Buckingham exp-6 potential ('buck' -> 4)

            parpot(3)=parpot(3)*engunit

            !maxpar = 4

            !call get_word(line,word)
            !if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            if( maxpar == 4 ) then

              if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*)'ERROR: negative surface diameter for the Buckingham (Ext) core surface!!! ',iextpot

                call error(999)

              else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'Buckingham (Ext) pair potential (',iextpot,') origin is shifted to D_surf = ',parpot(maxpar)

              end if

            end if

        case(9)
        !else if (keypot == 9) then !if (keypot == 8) then

            ! Morse potential ('mors' -> 8)
            ! also 'morl' potential - 'mors' plus A*r^{-12} repulsion ('morl' -> 13)

            !AB: this potential was not implemented in the original vdw_interpolate_module.f90 - ???

            !JG: Morse potential exceptions

            if( parpot(1) < 0.0_wp ) then

                if( master ) write(uout,*)'ERROR: Negative Morse De parameter detected!!! ',iextpot

                call error(999)

            else if( parpot(2) < 0.0_wp ) then 

                if( master ) write(uout,*)'ERROR: Negative Morse k parameter detected!!!',iextpot

                call error(999)

            else if( parpot(3) < 0.0_wp ) then

                if( master ) write(uout,*)'ERROR: Negative Morse r0 distance detected!!! ',iextpot

                call error(999)

            else if( parpot(4) < 0.0_wp ) then

                if( master ) write(uout,*)'ERROR: Negative Morl LJ repulsion detected!!! ',iextpot

                call error(999)

            else if( parpot(4) > 0.0_wp ) then

                !JG: Remember to correct for units of repulsion term
                parpot(4) = parpot(4) * engunit

            end if

            !maxpar = 5

            !call get_word(line,word)
            !if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            if( maxpar == 5 ) then

              if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*) &
                             'ERROR: negative surface diameter for the Morse (Ext) core surface!!! ',iextpot

                call error(999)

              else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'Morse (Ext) pair potential (',iextpot,') origin is shifted to D_surf = ',parpot(maxpar)

              end if

            end if

        case(10)
        !else if( keypot == 10 ) then !if (keypot == 11) then

            !JG: a/r^n-b/r^m potential

            if ( parpot(1) < 0.0_wp ) then

                if( master ) write(uout,*) "ERROR: pwnm/ljnm repulsion parameter (a) must be positive!!!"

                call error(999)

            else if ( parpot(2) < 0.0_wp ) then

                if( master ) write(uout,*) "ERROR: pwnm/ljnm attraction parameter (b) must be positive!!!"

                call error(999)

            else if ( parpot(3) > 0.0_wp .and. parpot(4) > 0.0_wp .and. parpot(3) < parpot(4) ) then

                if( master ) write(uout,*) "ERROR: pwnm/ljnm repulsion power (n) smaller than attraction power (m)!!!"

                call error(999)

            end if

            parpot(2) = parpot(2)*engunit

            !maxpar = 5

            !call get_word(line,word)
            !if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            if( maxpar == 5 ) then

              if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*) &
                             'ERROR: negative surface diameter for the Power n-m (LJ-type Ext) core surface!!! ',iextpot

                call error(999)

              else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'Power n-m (LJ-type Ext) pair potential (',iextpot,') origin is shifted to D_surf = ',parpot(maxpar)

              end if

            end if

          case(11)
             parpot(1) = parpot(1)*engunit
             parpot(3) = parpot(3)*engunit
          case(12)
             do i=1,3
                parpot(i) = parpot(i)*engunit
                parpot(i+6) = parpot(i+6)*engunit
             end do
          case(13)
             parpot(1) = parpot(1)*engunit
          case(14)
             parpot(1) = parpot(1)*engunit
          case(15)
             parpot(1) = parpot(1)*engunit

          case(16)

             !TU: Tophat function

             !TU: For this the energy is H for (centre-width/2)<z<(centre+width/2), and 0 otherwise
             !TU: There are 3 parameters: 
             !TU: 1) H (dimension energy)
             !TU: 2) width (dimension length)
             !TU: 3) centre (dimension length)

             parpot(1) = parpot(1)*engunit

             if ( parpot(2) < 0.0_wp ) then

                if( master ) write(uout,*) "ERROR: width for tophat external potential is negative!"
                call error(999)

             end if

          case default
             call error(err4)
       end select

       if( master ) write(uout,'(/,1x,a,i3,2(a,f12.5),/)') &
           'VdW(Ext) repulsion capped at D_core(',iextpot,') = ',parpot(0),' by E_cap = ',vdw_ecap

       !set the type of potential - done above
       !ltpext(iextpot) = keypot

       ! copy across parameters
       prmext(:,iextpot) = parpot(:)
       
    end do
    
  end subroutine read_external_pot
  
  subroutine ext_pot_energy(atype, x, y, z, extener)

    use kinds_f90
    use constants_module, only: uout, TORADIANS, PI
    use vdw_module, only: HS_ENERGY
    !use slit_module, only: slit_type

    implicit none
    integer , intent(in)         :: atype
    real(kind = wp), intent(in)  :: x, y, z
    real(kind = wp), intent(out) :: extener

    integer, parameter :: err=172

    integer         :: iext, keyext
    real(kind = wp) :: kx , ky , kz , &
                       x0 , y0 , z0 , &
                       Dx0, Dy0, Dz0
    real(kind = wp) :: a0S, dEf, kr0
    real(kind = wp) :: H, centre, width
    real(kind = wp) :: rr, x1, y1, shift, xyprefac, xx, yy
    !! real(wp) :: extener_test

    real(kind = wp) :: zzz
    real(kind = wp) :: gk1, gk2, vk1, vk2
    real(kind = wp) :: dhc, dsh, dsh2, dsh3, dsh4, dsh5, dsh6, dsh12

    !AB: << potential form definitions

    include "potential_forms.inc"

    !AB: >> potential form definitions

    extener = 0.0_wp
    kx  = 0.0_wp
    ky  = 0.0_wp
    kz  = 0.0_wp
    x0  = 0.0_wp
    y0  = 0.0_wp
    z0  = 0.0_wp
    Dx0 = 0.0_wp
    Dy0 = 0.0_wp
    Dz0 = 0.0_wp
    a0S = 0.0_wp
    dEf = 0.0_wp
    kr0 = 0.0_wp

    rr       = 0.0_wp
    x1       = 0.0_wp
    y1       = 0.0_wp
    shift    = 0.0_wp
    xyprefac = 0.0_wp
    xx       = 0.0_wp
    yy       = 0.0_wp

    !!    extener_test = 0.0_wp

    ! get potential function number
    iext = lstext(atype)   ! think this is right though not sure
    if(iext == 0) return

    keyext = ltpext(iext)

    zzz = z
    dhc = prmext(0,iext) !0.0_wp !AB: currently "hard core" does not apply to the walls

    select case(keyext)

        case (-1)

            !TU: Code for hard-sphere potential ('hs')

            !AB: z = z-z_wall in the slit case 
            !zzz = abs(z)

            !if( zzz < prmext(1,iext) ) then
            if( zzz < dhc ) then

                extener = HS_ENERGY

                return

            else if ( zzz < prmext(2,iext) ) then

                extener = prmext(3,iext)

                return

            end if

        !AB: NOTE FOR THE REST
        !AB: enabling potentials starting at the effective particle surfaces
        !AB: rather than from the particle centers (this is a new feature!)
        !AB: although the surface distance definition is a little complicated...

        !AB: dsh = prmext(max+1,iext) ! where max is the number of compulsory parameters
        !AB: dhc = dhc+dsh            ! this is effective hard-core / overlap distance

        case (1) ! [<-2] Lennard-Jones (LJ) potential

            dsh = prmext(3,iext)
            dhc = dhc+dsh

            !AB: z = z-z_wall in the slit case 
            !zzz = abs(z)

            !AB: check for overlapping cores due to steep short-range repulsion
            if( zzz < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                extener = HS_ENERGY

                return

            else

                dsh = zzz-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the LJ core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! Lennard-Jones potential
                a = prmext(1,iext)
                b = prmext(2,iext)

                dsh6 = b/dsh
                dsh6 = dsh6*dsh6*dsh6
                !dsh6 = dsh6*dsh6

                extener = vlj( dsh6*dsh6, a )  ! vlj(sri6,a)

                !extener = 4.0_wp * a *(b / dsh)**6 * ((b / dsh)**6 - 1.0_wp)

                return

            end if

        case (2) ! [<-1] 12-6 potential

            dsh = prmext(3,iext)
            dhc = dhc+dsh

            !AB: z = z-z_wall in the slit case 
            !zzz = abs(z)

            !AB: check for overlapping cores due to steep short-range repulsion
            if( zzz < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                extener = HS_ENERGY

                return

            else

                dsh = zzz-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the 12-6 core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! 12-6 potential
                a = prmext(1,iext)
                b = prmext(2,iext)

                dsh6 = dsh*dsh*dsh
                dsh6 = dsh6*dsh6
                !dsh6 = 1.0_wp/dsh6

                extener = v126( 1.0_wp/dsh6, a , b ) ! v126(ri6,a,b)

                !extener = (a / dsh**6 - b) / dsh**6

                return

            end if

        case (3) ! [<-12] Simple Powers potential (9-3) potential (JG/AB)

            dsh = prmext(3,iext)
            dhc = dhc+dsh

            !AB: z = z-z_wall in the slit case 
            !zzz = abs(z)

            if( zzz < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                extener = HS_ENERGY

                return

            else

                dsh = zzz-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the power (LJ) n-m core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! simple power (LJ) n-m potential
                a = prmext(1,iext)
                b = prmext(2,iext)
                c = dsh*dsh*dsh

                extener = v93( 1.0_wp/c, a, b ) != (a*ri3*ri3-b)*ri3

                !extener = vpnm( a*d, b*c ) != aric - brid
                !extener = a/dsh**c - b/dsh**d

                return

            end if

        case (4) ! [<-13] Simple Powers potential (10-4) potential (JG/AB)

            dsh = prmext(3,iext)
            dhc = dhc+dsh

            !AB: z = z-z_wall in the slit case 
            !zzz = abs(z)

            if( zzz < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                extener = HS_ENERGY

                return

            else

                dsh = zzz-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the power (LJ) n-m core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! simple power (LJ) n-m potential
                a = prmext(1,iext)
                b = prmext(2,iext)

                dsh = 1.0_wp/dsh*dsh
                c = dsh*dsh
                d = dsh*c

                extener = v104( d, c, a, b ) != (a*ri6-b)*ri4

                !extener = vpnm( a*d, b*c ) != aric - brid
                !extener = a/dsh**c - b/dsh**d

                return

            end if

        case (5) ! [<-9] Weeks-chandler-Andersen (WCA), based around "shifted & truncated" LJ, potential by I.T.Todorov

            !AB: the initial definition included shifting to the effective particle surfaces via 'c'
            !AB: now most of the potentials have similar generalised definitions

            !AB: NOTE: we have a *custom* definition of WCA based on *full* LJ (prone to confusion)
            !AB: whereas the standard WCA definiton is includes LJ-repulsion only!
            !AB: keeping it for legacy considerations - everybody seems happy with this in DL_POLY-4 anyway

            dsh = prmext(3,iext)
            dhc = dhc+dsh

            !AB: z = z-z_wall in the slit case 
            !zzz = abs(z)

            if( zzz < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                extener = HS_ENERGY

                return

            else

                dsh = zzz-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the WCA (LJ) core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! Weeks-chandler-Anderson (shifted & truncated Lenard-Jones) (i.t.todorov)
                a = prmext(1,iext)
                b = prmext(2,iext)
                !c = prmext(3,iext)
                d = prmext(4,iext)

                dsh6 = b/dsh
                dsh6 = dsh*dsh*dsh
                dsh6 = dsh6*dsh6

                if( dsh < d ) extener = vwca( a, dsh6 ) !=  4.0_wp*a* brc6 * (brc6 - 1.0_wp) + a

                !if( dsh < d ) extener = 4.0_wp * a * (b / dsh)**6 * ((b / dsh)**6 - 1.0_wp) + a
                !extener = 4.0_wp * a * (b / (r - c))**6 * ((b / (r - c))**6 - 1.0_wp) + a

                return

            end if

        case (6) ! Hydrogen-bond 12-10 potential

            dsh = prmext(3,iext)
            dhc = dhc+dsh

            !AB: z = z-z_wall in the slit case 
            !zzz = abs(z)

            if( zzz < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                extener = HS_ENERGY

                return

            else

                dsh = zzz-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the Hydrogen-bonding core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! Hydrogen-bond 12-10 potential
                a = prmext(1,iext)
                b = prmext(2,iext)

                dsh2 = dsh*dsh
                dsh5 = dsh2*dsh2*dsh
                dsh6 = dsh2*dsh2*dsh2

                extener = vhbd( a/(dsh6*dsh6), b/(dsh5*dsh5) ) != ri12a - ri10b

                !extener = a / dsh**12 - b / dsh**10

                return

            end if

        case (7) ! [<-10] Espanol-Warren (EW) potential

            dsh = prmext(3,iext)
            dhc = dhc+dsh

            !AB: z = z-z_wall in the slit case 
            !zzz = abs(z)

            if( zzz < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                extener = HS_ENERGY

                return

            else

                dsh = zzz-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the Espanol-Warren core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! espanol - warren
                a = prmext(1,iext)
                b = prmext(2,iext)

                if( dsh < b ) extener = vew( a, b-dsh ) != 0.5_wp*a*br*br

                !if( dsh < b ) extener = 0.5_wp * a * (b-dsh)**2

                return

            end if

        case (8) ! [<-4] Buckingham exp-6  potential

            dsh = prmext(4,iext)
            dhc = dhc+dsh

            !AB: z = z-z_wall in the slit case 
            !zzz = abs(z)

            if( zzz < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                extener = HS_ENERGY

                return

            else

                dsh = zzz-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the Buckingham core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! Buckingham exp-6 potential
                a = prmext(1,iext)
                b = prmext(2,iext)
                c = prmext(3,iext)

                dsh2 = dsh*dsh

                extener = vbuk( a*exp(-dsh/b), c/(dsh2*dsh2*dsh2) ) != aexprb - sri6

                !vk1  = a * exp(-zzz / b)
                !vk2  = - c / (rsq * rsq * rsq)
                !extener = vk1 + vk2

                return

            end if

        case (9) ! [<-8] Morse plus D*r^{-12} potential (JP/JG)

            dsh = prmext(5,iext)
            dhc = dhc+dsh

            !AB: z = z-z_wall in the slit case 
            !zzz = abs(z)

            if( zzz < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                extener = HS_ENERGY

                return

            else

                dsh = zzz-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the Morse core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! Morse potential (general form)
                a = prmext(1,iext)
                b = prmext(2,iext)
                c = prmext(3,iext)
                d = prmext(4,iext)

                !extener = a * ((1.0_wp - exp( -c * (dsh - b) ))**2 - 1.0_wp) ! + d/dsh**12

                extener = vmrs( dsh, a, b, c ) != a*( (1.0_wp - exp(-c * (r - b)))**2 - 1.0_wp ) 

                if( d > 0.0_wp ) then

                    dsh6 = dsh*dsh*dsh
                    dsh6 = dsh6*dsh6

                    extener = extener + d/(dsh6*dsh6)

                end if

                return

            end if

        case (10) ! [<-11] Simple Powers potential (n > m) potential (JG/AB)

            dsh = prmext(5,iext)
            dhc = dhc+dsh

            !AB: z = z-z_wall in the slit case 
            !zzz = abs(z)

            if( zzz < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                extener = HS_ENERGY

                return

            else

                dsh = zzz-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the power (LJ) n-m core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! simple power (LJ) n-m potential
                a = prmext(1,iext)
                b = prmext(2,iext)
                c = prmext(3,iext)
                d = prmext(4,iext)

                extener = vpnm( a/dsh**int(c), b/dsh**int(d) ) != aric - brid

                !extener = a/dsh**c - b/dsh**d

                return

            end if

    case(11)
       ! zharm potental - testing purposes
       kz  = prmext(1,iext)
       z0  = prmext(2,iext)
       Dz0 = prmext(3,iext)

       extener = 0.5_wp*kz*(z-z0)**2 - Dz0
       
    case(12)
       ! xyzharm potential - testing purposes
       kx  = prmext(1,iext)
       ky  = prmext(2,iext)
       kz  = prmext(3,iext)
       x0  = prmext(4,iext)
       y0  = prmext(5,iext)
       z0  = prmext(6,iext)
       Dx0 = prmext(7,iext)
       Dy0 = prmext(8,iext)
       Dz0 = prmext(9,iext)

       extener = 0.5_wp*kx*(x-x0)**2 - Dx0 + &
                 0.5_wp*ky*(y-y0)**2 - Dy0 + &
                 0.5_wp*kz*(z-z0)**2 - Dz0

    case(13)
       ! zmorse potential: no x-y dependence
       Dz0 = prmext(1,iext)
       kz  = prmext(2,iext)
       z0  = prmext(3,iext)

       extener = Dz0*(1.0_wp - exp(-kz*(z - z0)))**2 - Dz0

    case(14)
       ! zmorse potential: modulated by a periodic xy function
       Dz0 = prmext(1,iext)
       kz  = prmext(2,iext)
       z0  = prmext(3,iext)
       a0S = prmext(4,iext)
       dEf = prmext(5,iext)
       kr0 = prmext(6,iext)

       xx = x
       yy = y

!       write(*,*) "old: xx, yy = " , xx, yy

       rr  = a0S/sqrt(3.0_wp)
       x1  = rr*cos(60.0_wp*TORADIANS)
       y1  = rr*sin(60.0_wp*TORADIANS)
       kx  = kr0/rr                     ! could use either kx or ky 

       ! convert to scaled coordinates
       xx = xx / rr
       shift = real(int(xx),kind=wp)
       xx = xx - shift

       yy = yy / a0S
       shift = real(int(yy),kind=wp)
       yy = yy - shift
       

       ! wrap x into "unit cell"
       if     (xx >= 0.5_wp) then            
          xx = xx - 1.0_wp
       else if(xx < -0.5_wp) then
          xx = xx + 1.0_wp
       end if

       ! wrap y into "unit cell"
       if     (yy >= 0.5_wp) then            
          yy = yy - 1.0_wp
       else if(yy < -0.5_wp) then
          yy = yy + 1.0_wp
       end if

       ! convert back to actual coordinates

       xx = xx*rr
       yy = yy*a0S

       xyprefac = ((( 1.0_wp - exp( -kx*sqrt( (xx   )**2 + (yy   )**2 ) ) )**2 - 1.0_wp + &
                    ( 1.0_wp - exp( -kx*sqrt( (xx-x1)**2 + (yy-y1)**2 ) ) )**2 - 1.0_wp + &
                    ( 1.0_wp - exp( -kx*sqrt( (xx-x1)**2 + (yy+y1)**2 ) ) )**2 - 1.0_wp + &
                    ( 1.0_wp - exp( -kx*sqrt( (xx+x1)**2 + (yy-y1)**2 ) ) )**2 - 1.0_wp + &
                    ( 1.0_wp - exp( -kx*sqrt( (xx+x1)**2 + (yy+y1)**2 ) ) )**2 - 1.0_wp   &
                   )*(-1.0_wp) + (1.0_wp/dEf - 1.0_wp)                        &
                  )*dEf

       Dz0 = Dz0*xyprefac

       extener =  Dz0*(1.0_wp - exp(-kz*(z - z0)))**2 - Dz0

    case(15)
       ! zmorse potential: modulated by a periodic xy function: models 110 surface
       Dz0 = prmext(1,iext)
       kz  = prmext(2,iext)
       z0  = prmext(3,iext)
       a0S = prmext(4,iext)
       dEf = prmext(5,iext)
       kr0 = prmext(6,iext)

       xx = x
       yy = y

       rr  = a0S/sqrt(2.0_wp)
       x1  = a0S
       y1  = rr
       kx  = kr0/rr

       ! convert to scaled coordinates
       xx = xx / a0S
       shift = real(int(xx), kind=wp)
       xx = xx - shift

       yy = yy / rr
       shift = real(int(yy), kind=wp)
       yy = yy - shift

       ! wrap x into "unit cell"
       if     (xx >= 0.5_wp) then            
          xx = xx - 1.0_wp
       else if(xx < -0.5_wp) then
          xx = xx + 1.0_wp
       end if
       
       ! wrap y into "unit cell"
       if     (yy >= 0.5_wp) then            
          yy = yy - 1.0_wp
       else if(yy < -0.5_wp) then
          yy = yy + 1.0_wp
       end if
       
       xx = xx*a0S
       yy = yy*rr

       xyprefac = (((1.0_wp - exp(-kx*sqrt(xx**2 + yy**2)))**2 - 1.0_wp &
                   )*(-1.0_wp) + (1.0_wp/dEf - 1.0_wp)              &
                  )*dEf

       Dz0 = Dz0*xyprefac

       extener =  Dz0*(1.0_wp - exp(-kz*(z - z0)))**2 - Dz0

    case(16)

       !TU: Tophat function

       !TU: For this the energy is H for (centre-width/2)<z<(centre+width/2), and 0 otherwise
       !TU: There are 3 parameters: 
       !TU: 1) H (dimension energy)
       !TU: 2) width (dimension length)
       !TU: 3) centre (dimension length)

       H      = prmext(1,iext)
       width  = prmext(2,iext)
       centre = prmext(3,iext)

       extener = 0.0_wp

       if(zzz>(centre-0.5_wp*width) .and. zzz<(centre+0.5_wp*width)) then

          extener = H

       end if

    case default
       
       call error(err)
       
    end select

!!     extener_test = 5.0e-3_wp*(x - 75.0_wp)**2 - 2000.0_wp + &
!!                    5.0e-3_wp*(y - 75.0_wp)**2 - 2000.0_wp + &
!!                    5.0e-3_wp*(z -100.0_wp)**2 - 2000.0_wp
!! 
!!     write(*,*) "Calc - test =", extener - extener_test

  end subroutine ext_pot_energy

end module external_potential_module
