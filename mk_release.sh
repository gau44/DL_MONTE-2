#!/bin/bash

# Simple script to make a DL_MONTE release archive.

VERSION="2.07"

ARCHIVENAME="dl_monte_release_v${VERSION}.tar.gz"

tar -zcf $ARCHIVENAME *.f90 *.inc SERIAL \
	              build-all build-par build-ser build: \
		      Makefile_SRL Makefile_PRL Makefile \
		      README LICENSE CITATION \
		      DL_MONTE-206_Manual.pdf DL_MONTE-2_Hacking_guide.pdf

