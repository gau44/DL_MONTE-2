! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module statistics_type

use kinds_f90

type statistics

        ! step energy
    real(kind = wp), dimension(:), allocatable ::    stpeng

        ! average energy
    real(kind = wp), dimension(:), allocatable ::    aveeng

        ! fluctuations energy
    real(kind = wp), dimension(:), allocatable ::    flceng

        ! rolling average energy
    real(kind = wp), dimension(:), allocatable ::    raveeng

        ! sum energy
    real(kind = wp), dimension(:), allocatable ::    zumeng

        ! stack energy
    real(kind = wp), dimension(:,:), allocatable ::   stkeng

    ! statistics fo lattice vectors / stress

        ! step values of lattice vectors
    real(kind = wp) ::   stp_latvec(3,3), stp_strs(3,3)

        ! average lattice vectors
    real(kind = wp) ::   ave_latvec(3,3), ave_strs(3,3)

        ! rolling average lattice vectors
    real(kind = wp) ::   rave_latvec(3,3), rave_strs(3,3)

        ! sum of lattice vectors
    real(kind = wp) ::   zum_latvec(3,3), zum_strs(3,3)

        ! fluctuations in lattice vectors
    real(kind = wp) ::   flc_latvec(3,3), flc_strs(3,3)

        ! stack: lattice vectors
    real(kind = wp), dimension(:,:,:), allocatable ::   stk_latvec

        ! stack: stress
    real(kind = wp), dimension(:,:,:), allocatable ::   stk_strs

    ! statistics for number of atomic and molecular species

        ! stack: num species
    real(kind = wp), dimension(:,:), allocatable ::   stk_nspc

        ! stack: num molecules
    real(kind = wp), dimension(:,:), allocatable ::   stk_nmol

        ! step species
    real(kind = wp), dimension(:), allocatable ::   stp_nspc

        ! average species
    real(kind = wp), dimension(:), allocatable ::   ave_nspc

        ! rolling average species
    real(kind = wp), dimension(:), allocatable ::   rave_nspc

        ! sum of species
    real(kind = wp), dimension(:), allocatable ::   zum_nspc

        ! fluctuations in species
    real(kind = wp), dimension(:), allocatable ::   flc_nspc

        ! step molecules
    real(kind = wp), dimension(:), allocatable ::   stp_nmol

        ! average molecules
    real(kind = wp), dimension(:), allocatable ::   ave_nmol

        ! rolling average molecules
    real(kind = wp), dimension(:), allocatable ::   rave_nmol

        ! sum of molecules
    real(kind = wp), dimension(:), allocatable ::   zum_nmol

        ! fluctuations in molecules
    real(kind = wp), dimension(:), allocatable ::   flc_nmol


    ! variables for discrimination of energy over molecule types

        ! step: total energy
    real(kind = wp), dimension(:,:), allocatable ::   stp_emol

        ! average: total energy
    real(kind = wp), dimension(:,:), allocatable ::   ave_emol

        ! rolling average: total energy
    real(kind = wp), dimension(:,:), allocatable ::   rave_emol

        ! sum: total energy
    real(kind = wp), dimension(:,:), allocatable ::   zum_emol

        ! fluctuation: total energy
    real(kind = wp), dimension(:,:), allocatable ::   flc_emol

        ! stack: total energy
    real(kind = wp), dimension(:,:,:), allocatable ::   stk_emol


        ! thermodynamic data - eg chem potentials
        ! step chem potential
    real(kind = wp), dimension(:), allocatable ::   stp_achp1
    real(kind = wp), dimension(:), allocatable ::   stp_achp2

        ! average chem potential
    real(kind = wp), dimension(:), allocatable ::   ave_achp1
    real(kind = wp), dimension(:), allocatable ::   ave_achp2

        ! rolling average chem potential
    real(kind = wp), dimension(:), allocatable ::   rave_achp1
    real(kind = wp), dimension(:), allocatable ::   rave_achp2

        ! sum of chem potential
    real(kind = wp), dimension(:), allocatable ::   zum_achp1
    real(kind = wp), dimension(:), allocatable ::   zum_achp2

        ! fluctuations in chem potential
    real(kind = wp), dimension(:), allocatable ::   flc_achp1
    real(kind = wp), dimension(:), allocatable ::   flc_achp2


        ! stack for atomic chemical potential - forward 
    real(kind = wp), dimension(:,:), allocatable ::   stk_achp1

        ! stack for atomic chemical potential - backward 
    real(kind = wp), dimension(:,:), allocatable ::   stk_achp2

        ! stores the ideal part of the chemical potential
    real(kind = wp), dimension(:), allocatable :: ideal1, ideal2


        ! the no of configurations used for z-density and no of bins used for cal
    integer :: nsample, nsamp_typ, nsamp_wdm, nsamp_zden, zden_points

        ! spacing for zdensity bins
    real(kind = wp) :: zden_r

        ! the z-density
    real(kind = wp), dimension(:,:), allocatable ::   zdens

        ! the no of configurations used for rdf calculation and no of bins used
    integer :: nsamp_rdf, rdf_points

        ! spacing for rdf bins and max distance for rdfs
    real(kind = wp) :: rdf_r, rdf_max

        ! rdfs
    real(kind = wp), dimension(:,:), allocatable ::   mrdf, rdfs, crdf

        !the number of times the displacements have been sampled
    integer :: nsamp_disp

        !arrays to store the displacements - these may be prohibitively expensive
    real(kind = wp), dimension(:,:), allocatable ::   ave_disp, rave_disp, zum_disp, flc_disp
    real(kind = wp), dimension(:,:,:), allocatable ::   stk_disp, stp_disp

end type

end module
