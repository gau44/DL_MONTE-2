Subroutine dcell(aaa,bbb)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 subroutine to calculate the dimensional properies of a
! simulation cell specified by the input 3x3 matrix aaa (cell vectors in
! rows, the matrix is in the form of one dimensional reading
! (row1,row2,row3).
!
! The results are returned in the array bbb, with:
!
! bbb(1 to 3) - lengths of cell vectors
! bbb(4 to 6) - cosines of cell angles
! bbb(7 to 9) - perpendicular cell widths
! bbb(10)     - cell volume
!
! copyright - daresbury laboratory
! author    - w.smith july 1992
! amended   - i.t.todorov may 2008
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  Use kinds_f90

  Implicit None

  Real( Kind = wp ), Intent( In    ) :: aaa(3,3)
  Real( Kind = wp ), Dimension( 1:10 ), Intent(   Out ) :: bbb

  Real( Kind = wp ) :: axb1,axb2,axb3,bxc1,bxc2,bxc3,cxa1,cxa2,cxa3, &
                       x(1:3),y(1:3),z(1:3),d(1:3)

! calculate lengths of cell vectors

  bbb(1)=Sqrt(aaa(1,1)**2+aaa(1,2)**2+aaa(1,3)**2)
  bbb(2)=Sqrt(aaa(2,1)**2+aaa(2,2)**2+aaa(2,3)**2)
  bbb(3)=Sqrt(aaa(3,1)**2+aaa(3,2)**2+aaa(3,3)**2)

! calculate cosines of cell angles

  bbb(4)=(aaa(1,1)*aaa(2,1)+aaa(1,2)*aaa(2,2)+aaa(1,3)*aaa(2,3))/(bbb(1)*bbb(2))
  bbb(5)=(aaa(1,1)*aaa(3,1)+aaa(1,2)*aaa(3,2)+aaa(1,3)*aaa(3,3))/(bbb(1)*bbb(3))
  bbb(6)=(aaa(2,1)*aaa(3,1)+aaa(2,2)*aaa(3,2)+aaa(2,3)*aaa(3,3))/(bbb(2)*bbb(3))

! calculate vector products of cell vectors

  axb1=aaa(1,2)*aaa(2,3)-aaa(1,3)*aaa(2,2)
  axb2=aaa(1,3)*aaa(2,1)-aaa(1,1)*aaa(2,3)
  axb3=aaa(1,1)*aaa(2,2)-aaa(1,2)*aaa(2,1)

  bxc1=aaa(2,2)*aaa(3,3)-aaa(2,3)*aaa(3,2)
  bxc2=aaa(2,3)*aaa(3,1)-aaa(2,1)*aaa(3,3)
  bxc3=aaa(2,1)*aaa(3,2)-aaa(2,2)*aaa(3,1)

  cxa1=aaa(3,2)*aaa(1,3)-aaa(1,2)*aaa(3,3)
  cxa2=aaa(1,1)*aaa(3,3)-aaa(1,3)*aaa(3,1)
  cxa3=aaa(1,2)*aaa(3,1)-aaa(1,1)*aaa(3,2)

! calculate volume of cell

  bbb(10)=Abs(aaa(1,1)*bxc1+aaa(1,2)*bxc2+aaa(1,3)*bxc3)

! calculate cell perpendicular widths

  d(1)=bbb(10)/Sqrt(bxc1*bxc1+bxc2*bxc2+bxc3*bxc3)
  d(2)=bbb(10)/Sqrt(cxa1*cxa1+cxa2*cxa2+cxa3*cxa3)
  d(3)=bbb(10)/Sqrt(axb1*axb1+axb2*axb2+axb3*axb3)

  x(1)=Abs(aaa(1,1))/bbb(1) ; y(1)=Abs(aaa(1,2))/bbb(1) ; z(1)=Abs(aaa(1,3))/bbb(1)
  x(2)=Abs(aaa(2,1))/bbb(2) ; y(2)=Abs(aaa(2,2))/bbb(2) ; z(2)=Abs(aaa(2,3))/bbb(2)
  x(3)=Abs(aaa(3,1))/bbb(3) ; y(3)=Abs(aaa(3,2))/bbb(3) ; z(3)=Abs(aaa(3,3))/bbb(3)

! distribute widths

  If      (x(1) >= x(2) .and. x(1) >= x(3)) Then
     bbb(7)=d(1)
     If (y(2) >= y(3)) Then
        bbb(8)=d(2)
        bbb(9)=d(3)
     Else
        bbb(8)=d(3)
        bbb(9)=d(2)
     End If
  Else If (x(2) >= x(1) .and. x(2) >= x(3)) Then
     bbb(7)=d(2)
     If (y(1) >= y(3)) Then
        bbb(8)=d(1)
        bbb(9)=d(3)
     Else
        bbb(8)=d(3)
        bbb(9)=d(1)
     End If
  Else
     bbb(7)=d(3)
     If (y(1) >= y(2)) Then
        bbb(8)=d(1)
        bbb(9)=d(2)
     Else
        bbb(8)=d(2)
        bbb(9)=d(1)
     End If
  End If

End Subroutine dcell

subroutine open_nodefiles(fname, iopen)

    use comms_mpi_module, only : master
    use parallel_loop_module, only : idgrp

    implicit none

    integer, intent(in) :: iopen
    character*6, intent(in) :: fname

    character*10 :: filename
    character*4 :: cnode(99)
    data cnode/  &
               '.001','.002','.003','.004','.005','.006','.007','.008','.009'  &
       ,'.010','.011','.012','.013','.014','.015','.016','.017','.018','.019'  &
       ,'.020','.021','.022','.023','.012','.025','.026','.027','.028','.029'  &
       ,'.030','.031','.032','.033','.034','.035','.036','.037','.038','.039'  &
       ,'.040','.041','.042','.043','.044','.014','.046','.047','.048','.049'  &
       ,'.050','.051','.052','.053','.054','.055','.056','.057','.058','.059'  &
       ,'.060','.061','.062','.063','.064','.065','.066','.067','.068','.069'  &
       ,'.070','.071','.072','.073','.074','.075','.076','.077','.078','.079'  &
       ,'.080','.081','.082','.083','.084','.085','.086','.087','.088','.089'  &
       ,'.090','.091','.092','.093','.094','.095','.096','.097','.098','.099'/

    if (master .and. idgrp /= 0) then

        if (idgrp <= 100) then

            filename = fname//cnode(idgrp)

            open (unit = iopen,file = filename)

        else

            call error(334)

        endif

    endif 

end subroutine
