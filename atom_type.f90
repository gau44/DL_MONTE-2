! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

!> @brief 
!> - Data type defining an atom  
!> @usage 
!> - call as `<atom_container>%<attribute>`
!> - @stdspecs
!> @using 
!> - `kinds_f90`

!> @typefor an atom

module atom_type

    use kinds_f90

    implicit none


type atom

        !> name of the atom
    character*8 :: atmname

        !> identifier of the atom
    integer ::   atlabel

        !> atom type identifier, either core/semi/metal/spin
    integer ::   atype

        !> tag on the atom to identify specific site
    integer ::   site

        !> identifier of COM the atom belongs to in FED calculation (not just the molecule)
    integer ::   idcom

        !> flag for atom bearing any charge (check for the charge only once)
    logical ::   is_charged

        !> mass of the atom
    real (kind = wp) ::   mass

        !> charge on the atom
    real (kind = wp) ::   charge

!       !> the global atom number an atom is in
!   integer, dimension(:), allocatable ::   glob_no

        !> matrix containing the atom position
    real (kind = wp) ::   rpos(3)

        !> matrix to store the atom position
    real (kind = wp) ::   store_rpos(3)

        !> matrix to store the atom position in the neighbour-list (auto-updated)
    real (kind = wp), dimension(:), allocatable ::   r_zero

        !> Position of lattice site corresponding to this atom - for PSMC
    real (kind = wp) :: psmcsite(3)

        !> Displacement of the atom from the lattice site - for PSMC
    real (kind = wp) :: psmcu(3)

        !> Spin of the atom - for 'spin' type atoms
    real (kind = wp) :: spin(3)

        !> Array to store the spin of the atom 
    real (kind = wp) :: store_spin(3)
    
end type

end module
