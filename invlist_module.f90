! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module invlist_module

    use kinds_f90

    implicit none

contains

subroutine alloc_invlist_arrays(list, n)

    use invlist_type

    implicit none
    type(invlist), intent(inout) :: list
    integer , intent(in) :: n

    list%ninv = n

    allocate (list%ivpair(n,5))

end subroutine

subroutine add_quads(list, atomi, atomj, atomk, atoml, pot, n)

    use invlist_type

    implicit none
    type(invlist), intent(inout) :: list
    integer , intent(in) :: n
    integer , intent(in) :: atomi(n), atomj(n), atomk(n), atoml(n), pot(n)
    integer :: i

    do i = 1, n

        list%ivpair(i,1) = atomi(i)
        list%ivpair(i,2) = atomj(i)
        list%ivpair(i,3) = atomk(i)
        list%ivpair(i,4) = atoml(i)
        list%ivpair(i,5) = pot(i)

    enddo

end subroutine

subroutine alloc_and_copy_invlist_arrays(nlist, olist)

    use invlist_type

    implicit none
    type(invlist), intent(in) :: olist  ! old bond list
    type(invlist), intent(inout) :: nlist  ! new bond list

    integer :: i, fail

    if (olist%ninv == 0) return

    fail = 0
    if(allocated(nlist%ivpair)) deallocate (nlist%ivpair, stat = fail)

    if (fail > 0) call error(342)

    nlist%ninv = olist%ninv

    fail = 0
    allocate (nlist%ivpair(nlist%ninv,5), stat = fail)

    if (fail > 0) call error(341)

    do i = 1, nlist%ninv

        nlist%ivpair(i,:) = olist%ivpair(i,:)

    enddo

end subroutine

subroutine copy_invlist(nlist, olist)

    use invlist_type

    implicit none
    type(invlist), intent(in) :: olist  ! old bond list
    type(invlist), intent(inout) :: nlist  ! new bond list

    integer :: i

    if (olist%ninv == 0) return

    nlist%ninv = olist%ninv


    do i = 1, nlist%ninv

        nlist%ivpair(i,:) = olist%ivpair(i,:)

    enddo

end subroutine

subroutine print_invlist(list)

    use constants_module, only : uout
    use invlist_type
    use comms_mpi_module, only : idnode

    implicit none
    type(invlist), intent(inout) :: list

    integer :: i

    if (idnode /= 0) return

    do i = 1, list%ninv

        write(uout,"(/,1x,'inversion (i,j,k,l) between atoms ',3i4,' with potential ',i4)")list%ivpair(i,:)

    enddo

end subroutine

end module
