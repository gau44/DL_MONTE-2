! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module dihlist_type

    use kinds_f90

    implicit none

type dihlist

    integer :: ndih = 0  ! the number of dihedral interactions for this molecule
    integer, dimension(:,:), allocatable :: dipair ! stores connected atoms and potential number
    logical, dimension(:), allocatable :: diexc  ! stores exclusion details for each dihedral 1-4 pair

end type

end module
