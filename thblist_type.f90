! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module thblist_type

    use kinds_f90

    implicit none


    !> Derived type storing the triplets used in three-body interactions for a
    !> given molecule. Note that the atom in the molecule is always in the centre
    !> of the triplet
type thblist

        !> The maximum number of triplets which can be associated with any atom in the molecule
    integer :: maxtrip  ! the maximum number of triplets per atom

        !> The number of triplets associated with each atom in the molecule    
    integer, dimension(:), allocatable :: numtrip  ! the number of angles for this atom

        !> The locations of the atoms in the triplet, and the type of three-body potential associated
        !> with the triplet. The type of three-body potential associated with the triplet (an integer
        !> label) is tlist%thb_triplets(i,n,1). The molecule and atom indices for the atoms 'j' and 'k'
        !> in the nth triplet associated with atom i are jmol=tlist%thb_triplets(i,n,2),
        !> jatm=tlist%thb_triplets(i,n,3), kmol=tlist%thb_triplets(i,n,4), and
        !> katm=tlist%thb_triplets(i,n,5).
    integer, dimension(:,:,:), allocatable :: thb_triplets ! stores connected atoms and potential number

end type

end module
