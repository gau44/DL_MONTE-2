# DL_MONTE-2

This is the development branch of DL_MONTE. It is intended for DL_MONTE developers 
and users who wish to test features under development not yet included in a 
DL_MONTE release. Note that certain features in this branch may not be stable.

Typical users should not use this version, rather they should download the latest 
stable release of DL_MONTE. See 
[here](https://gitlab.com/dl_monte/user-hub/-/wikis/home) for information on
how to do this, as well as links to further DL_MONTE documentation.


Notes for developers
====================

We welcome contributions to DL_MONTE from the community. However, to ensure the 
sustainability and stability of DL_MONTE, new contributions to the source code 
must meet a number of criteria:
- adhere to the coding style described in the 'DL_MONTE-2_Coding_Guide'.
- be well tested, at the very least passing regression tests in the DL_MONTE test
  suite (see below).
- not be detrimental to the performance of DL_MONTE, i.e. not slow down DL_MONTE.
- adhere to general 'best coding practices' (see e.g. 
  [here](https://en.wikipedia.org/wiki/Best_coding_practices)).

Moreover new functionality should, where applicable:
- be accompanied by contributions to the DL_MONTE test suite which demonstrate 
  that the functionality works correctly, as well as regression tests which will 
  help catch errors in the functionality introduced by future developers. 
- include provisions for catching 'obvious' errors in user input and return 
  helpful error messages informing the user as such.


Testing
-------
The [DL_MONTE test suite](https://gitlab.com/dl_monte/dl_monte_tests) contains
regression tests that must be passed by all contributions. See that repository
for details on how to run the tests manually and add new tests. 

Note that GitLab's continuous integration functionality (controlled by the 
.gitlab-ci.yml file in this repository) automatically runs the test suite on 
GitLab servers every time a commit is pushed. Moreover, it checks that DL_MONTE
compiles in 'SRL dir' and 'SRL tab' mode. If any test is failed then a
red cross appears next to the commit; if they are all passed then a green cross
appears.

Note that currently we unfortunatley do not have tests to check MPI compilation, 
or to ensure that DL_MONTE's performance is not significantly reduced by new 
contributions. Please check that your contributions are such that you haven't
introduced a bug into the MPI mode.


Procedure for contributing new code
-----------------------------------

The workflow for contributing new code to DL_MONTE is as follows:

1. Create a fork of this repository.
2. Add your contributions to this fork (as git commits).
3. Ensure that your contributions adhere to the code standards given above, and
   that the tests are passed.
4. When you are finished, you should create a merge request (to merge your 
   contributions into the master branch of this repository). Your proposed 
   contributions will then be examined by one of the DL_MONTE maintainers. If 
   the code standards are met and all tests are passed then the merge request 
   should be accepted. If not, feedback will be provided regarding what changes
   should be made before the merge request is accepted.

If your contribution is included in this development repository then it will be
included in the next release.

Consider, if applicable, also adding documentation for your contribution in the
form of examples, tutorials, or in the manual. Consider also contributing to 
the test suite. The relevant repositories are in the overarching 
[DL_MONTE group](https://gitlab.com/dl_monte). 
