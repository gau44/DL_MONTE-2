! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module dihedral_module

    use kinds_f90

    implicit none

    integer ::    numdih

        !> the dihedral potetial parameters
    real(kind = wp), allocatable, dimension(:,:) :: prmdih
    integer, allocatable, dimension(:) :: ltpdih

contains

subroutine alloc_dihpar_arrays(npar)

    use kinds_f90
    use constants_module, only : uout

    implicit none

    integer, intent(in) :: npar

    integer :: fail(2)

    fail = 0
    numdih = npar

    allocate(prmdih(npar, 7), stat = fail(1))
    allocate(ltpdih(npar), stat = fail(2))

    if(any(fail > 0)) then

        call error(128)

    endif

end subroutine alloc_dihpar_arrays


!reads in, prints out, and convets to internal units dihedral potentisla

subroutine read_dihedral_par(npar, unit)

    use kinds_f90
    use constants_module, only : uout, ufld, TORADIANS
    use parse_module
    use comms_mpi_module, only : master !, idnode

    implicit none

    integer, intent(in) :: npar

    integer :: nread
    real(kind = wp), intent(in) :: unit

    integer :: i

    character :: line*100, keyword*8, word*40, intchar*4

    logical safe

    safe = .true.

    nread = ufld

    if( master ) write(uout,"(/,/,24('-'),/,' dihedral potentials: ',/,24('-'))")

    do i = 1, npar

        call get_line(safe, nread, line)
        if (.not.safe) go to 1000

        call get_word(line, keyword)

        if( keyword == 'cos' .or. keyword == '-cos' ) then ! cosine

            ltpdih(i) = 1

        else if( keyword == 'harm' .or. keyword == '-harm' ) then !harmonic dihedral potential

            ltpdih(i) = 2

        else if( keyword == 'hcos' .or. keyword == '-hcos' ) then !harmonic cosine dihedral potential

            ltpdih(i) = 3

        else if( keyword == 'cos3' .or. keyword == '-cos3' ) then ! triple cosine

            ltpdih(i) = 4

        else if( keyword == 'ryck' .or. keyword == '-ryck' ) then ! Ryckaert-Bellemans
        
            ltpdih(i) = 5

        else if( keyword == 'rbf' .or. keyword == '-rbf' ) then ! Fluorinated Ryckaert-Bellemans

            ltpdih(i) = 6

        else if( keyword == 'opls' .or. keyword == '-opls' ) then

            ltpdih(i) = 7

        else 

            call error(172)

        endif

        if( keyword(1:1) == '-' ) ltpdih(i) = -ltpdih(i)

        call get_word(line, word)
        prmdih(i,1) = word_2_real(word)
        call get_word(line, word)
        prmdih(i,2) = word_2_real(word)
        call get_word(line, word)
        prmdih(i,3) = word_2_real(word)
        call get_word(line, word)
        prmdih(i,4) = word_2_real(word)
        call get_word(line, word)
        prmdih(i,5) = word_2_real(word)
        call get_word(line, word)
        prmdih(i,6) = word_2_real(word)
        call get_word(line, word)
        prmdih(i,7) = word_2_real(word)

        !if( master ) &
        !    write(uout,"(1x,i4,'  type : ',a,' parameters : ', 4f15.7)") &
        !          i, keyword, prmdih(i,:)

        if( master ) write(uout,"(1x,i5,3x,a8,3x,10f15.6)") &
            i,keyword, prmdih(i,:)

        prmdih(i,1) =  prmdih(i,1) * unit

        if ( abs(ltpdih(i)) == 4) then

            !TU-: For cos3 parameters 2 and 3 are energies

            prmdih(i,2) = prmdih(i,2) * unit
            prmdih(i,3) = prmdih(i,3) * unit

        else if ( abs(ltpdih(i)) == 7) then

            !TU: For OPLS parameters 1, 2 and 3 are energies, 4 and 5 are
            !TU: dimensionless (since they are fractions of VdW and Coulomb
            !TU: to include), 6 is an energy, and 7 is an angle in degrees

            prmdih(i,2) = prmdih(i,2) * unit
            prmdih(i,3) = prmdih(i,3) * unit
            prmdih(i,6) = prmdih(i,6) * unit
            prmdih(i,7) = prmdih(i,7) * TORADIANS


        else

            !TU-: For cos, harm, hcos (and ryck and rbf which have no 2nd parameter) 
            !TU-: the 2nd parameter is an angle in degrees

            prmdih(i,2) = prmdih(i,2) * TORADIANS

        endif

        !TU: Catch some input errors and set sensible default behaviour
    
        if( (prmdih(i,4) < 0.0_wp) .or. (prmdih(i,5) < 0.0_wp) .or. &
            (prmdih(i,4) > 1.000000001_wp) .or. (prmdih(i,5) > 1.000000001_wp) ) then
    
             call cry(uout,'',"ERROR: Detected in FIELD a factor for inclusion for 1-4 VdW " &
                      //"or Coulomb not between 0 and 1.",999)
    
        end if
    
        if( ltpdih(i) < 0 .and. prmdih(i,4) == 0.0_wp ) then
    
            write(intchar,'(I4)') i
    
            call cry(uout,'',"WARNING: Dihedral "//intchar//" in FIELD specified to INCLUDE 1-4 " &
                 //"Coulomb interactions, but inclusion factor not set or set to 0.0."//achar(10) &
                 //"          Setting factor to 1.0: 1-4 Coulomb interactions will be FULLY " &
                 //"INCLUDED for this dihedral.",0)
    
            prmdih(i,4) = 1.0_wp
    
        end if
    
        if( ltpdih(i) < 0 .and. prmdih(i,5) == 0.0_wp ) then
    
            write(intchar,'(I4)') i

            call cry(uout,'',"WARNING: Dihedral "//intchar//" in FIELD specified to INCLUDE 1-4 " &
                 //"VdW interactions, but inclusion factor not set or set to 0.0."//achar(10) &
                 //"          Setting factor to 1.0: 1-4 VdW interactions will be FULLY " &
                 //"INCLUDED for this dihedral.",0)

            prmdih(i,5) = 1.0_wp
    
        end if

    
        if( ltpdih(i) > 0 .and. (prmdih(i,4) /= 0.0_wp .or. prmdih(i,5) /= 0.0_wp) ) then
    
            write(intchar,'(I4)') i
    
            call cry(uout,'',"WARNING: Dihedral "//intchar//" in FIELD specified to EXCLUDE 1-4 " &
                 //"Coulomb and VdW interactions, but detected non-zero inclusion factor."//achar(10) &
                 //"          Choosing 1-4 Coulomb and VdW interactions to be INCLUDED with inclusion " &
                 //"factors above (4th and 5th values).",0)
    
            ltpdih(i) = -ltpdih(i)
    
        end if

    enddo


    return

1000 call error(130)

end subroutine

!> calculates the energy for an dihedral
subroutine calc_dihedral_energy(pot, phi, eng)

    use kinds_f90
    use constants_module, only : PI, TWOPI, RTWOPI

    integer, intent(in) :: pot
    real(kind = wp), intent(in) :: phi
    real(kind = wp), intent(out) :: eng

    real(kind = wp) :: p1, p2, p3, p4, p5

    integer :: typ
    
    typ = abs(ltpdih(pot))

    eng = 0.0_wp

    if(typ == 1) then !torsion dihedral potential

        p1 = prmdih(pot, 1)
        p2 = prmdih(pot, 2)
        p3 = prmdih(pot, 3)

        eng = p1 * (1.0_wp + cos(p3 * phi - p2))

    else if (typ == 2) then  !harmonic improper dihedral

        p1 = prmdih(pot, 1)
        p2 = phi - prmdih(pot, 2)
        p3 = p2 - nint(phi * RTWOPI) * TWOPI

        eng = 0.5_wp * p1 * (p3 * p3)

    else if (typ == 3) then ! harmonic cosine dihedral

        p1 = prmdih(pot, 1)
        p2 = prmdih(pot, 2)
 
        eng = 0.5_wp * p1 * (cos(phi) - cos(p2))**2

    else if (typ == 4) then !3-term cosine dihedral

        p1 = prmdih(pot, 1)
        p2 = prmdih(pot, 2)
        p3 = prmdih(pot, 3)

        eng = 0.5_wp * (p1 * (1.0_wp + cos(phi)) + p2 * (1.0_wp - cos(2.0_wp * phi)) + p3 * (1.0_wp + cos(3.0_wp * phi)))

    else if (typ == 5) then ! ryckaert-bellemans potential chem.phys.lett., vol.30, p.123, 1975.
                                    ! the same form is used as dl_poly
        p1 = prmdih(pot, 1)
 
        eng = p1 * (1.116_wp - 1.462_wp * cos(phi) - 1.578_wp * (cos(phi))**2 + 0.368_wp * (cos(phi))**3 + 3.156_wp * &
              (cos(phi))**4 + 3.788_wp * (cos(phi))**5)

    else if (typ == 6) then ! ryckaert-bellemans potential-Rice at al., JCP 104, 2101, (1996).

        p1 = prmdih(pot, 1)

        eng = p1 * (3.55_wp - 2.78_wp * cos(phi) - 3.56_wp * (cos(phi))**2 - 1.64_wp * (cos(phi))**3 + 7.13_wp * &
              (cos(phi))**4 + 12.84_wp * (cos(phi))**5 + 9.67_wp * exp(-56.0_wp * (phi - PI)**2))

    else if (typ == 7) then ! OPLS

        !TU: Note that parameters 4 and 5 correspond to the factor to include for Coulomb and VdW! The
        !TU: current significance of parameters 1-7 reflects that in DL_POLY 4.
        p1 = prmdih(pot, 1)
        p2 = prmdih(pot, 2)
        p3 = prmdih(pot, 3)
        p4 = prmdih(pot, 6)
        p5 = prmdih(pot, 7)

        eng = p1 + 0.5_wp * (p2 * (1.0_wp + cos(phi-p5)) + p3 * (1.0_wp - cos(2.0_wp * (phi-p5))) &
            + p4 * (1.0_wp + cos(3.0_wp * (phi-p5))))

    else

        call error(175)

    endif


end subroutine

end module
