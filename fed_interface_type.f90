! ***************************************************************************
! *   Copyright (C) 2015 by A.V.Brukhno                                     *
! *   andrey.brukhno[@]stfc.ac.uk                                           *
! *                                                                         *
! ***************************************************************************

!> @brief
!> - Data type defining a FED calculation instance `'fed'` via switches, parameters & grid(s)
!> @usage 
!> - call as `<fed_interface_type>%<attribute>` where normally `<fed_interface_type> = 'fed'`
!> - @stdspecs
!> @using 
!> - `kinds_f90`

!> @typefor FED calculation instance `'fed'`: switches, parameters & grid(s)

module fed_interface_type

    use kinds_f90
    !use control_type

    implicit none

type fed_interface

        !> switch to determine if FED calculation to be done
    logical :: is_on = .false.

    logical :: is_bias_input = .false.

        !> enumerate flavors of FED calculation (to be defined...) \n
        !> 0 -> generic (general/direct, straitforward FED approach, one of: UMS/EE/WL/TM) \n
        !> 1 -> lattice switch (implemented separately, see `psmc_module.f90`; uses generic methods as biasing means) \n
        !> 2 -> phase switch (implemented separately, see `psmc_module.f90`; uses generic methods as biasing means) \n
        !> 3 -> histogram re-weighting (get intermediate FED profiles on-the-fly or from the post-simulation analysis) \n
        !> ... any other involved approach? - add it here
    integer :: flavor = 0

        !> enumerate methods of FED calculation (to be defined...) \n
        !> 0 -> no FED calculus \n
        !> 1 -> Umbrella Sampling (US) \n
        !> 2 -> Expanded/extended Ensemble (classical EE, on a grid of discrete substates/values) \n
        !> 3 -> Wang-Landau (WL EE/DOS/???) \n
        !> 4 -> Transition Matrix (TM) \n
        !> ??? negative values reserved for Kirkwood's gradual switching ON/OFF for atoms/molecules/clusters etc.
    integer :: method = 0

        !> number of (sub)states for FED calculation, or bins on a FED grid
    integer :: numstates = 1

        !> number of steps between attempted FED steps
    integer :: numfedskip = 1000

        !> number of steps between bias feedback updates (only used for EE/WL/TM)
    integer :: numupdskip = 10000

        !> number of steps between bias feedback updates without output to FEDDAT_???._??? (only used for TM)
    integer :: numupdskipfine = 1000

        !> enumerate possible order parameters (reaction coordinates) for FED calculation (see constants_module.f90) \n
        !> -1 -> COM distance \n
        !> 0 -> T (multicanonical) \n
        !> 1 -> Lambda (coupling strength; Hamiltonian transformation) \n
        !> 2 -> V (volume variation) \n
        !> 3 -> density, etc. (to be implemented) \n
        !> ... negative values reserved for `natural` order parameters based on configuration/structure/molecule position
    integer :: par_kind = 0

        !> kind/type of correction for FED parameter (reaction coordinate) \n
        !> - 0 -> no correction applied (bare FED calculation) \n
        !> - 1 -> correction for spherical area (COM distance related) \n
        !> - 2 -> correction for bin volume (COM distance related)
    integer :: par_corr = 0

        !> switch to toggle between discrete and continuous parameter sets
    integer :: im_cont = 0

        !> kind/type of incrementing FED parameter (reaction coordinate) \n
        !> - 0 -> linear, continuous grid, based on analysis of configuration/structure \n
        !> - 1 -> linear, discrete (Temperature/Lambda) or continuous (otherwise) grid \n
        !> - 2 -> quadratic, discrete set (Temperature/Lambda only) \n
        !> - 3 -> cubic, discrete set (Temperature/Lambda only)
    integer :: inc_kind = 1

        !> number of bias smoothing iterations (currently implemented via 3-point running averages)
    integer :: nitr_smooth = 0

        !> starting point/bin on the bias grid for smoothing
    integer :: nbeg_smooth = 0

        !> ending point/bin on the bias grid for smoothing
    integer :: nend_smooth = 0

        !> minimum value of FED parameter (reaction coordinate)
    real (kind = wp) :: par_min = 0.0_wp

        !> maximum value of FED parameter (reaction coordinate)
    real (kind = wp) :: par_max = 0.0_wp

        !> increment (delta) of FED parameter (reaction coordinate)
    real (kind = wp) :: par_inc = 0.0_wp

        !> Umbrella center - location of the minimum (only used for umbrella sampling)
    real (kind = wp) :: ums_mid = 0.0_wp

        !> Umbrella strength (only used for umbrella sampling)
    real (kind = wp) :: ums_frc = 0.0_wp

        !> EE bias update weight (scale) or WL bias droplet (only used for EE or WL)
    real (kind = wp) :: upd_val = 1.0_wp

        !> FED feedback tuning up scaling factor
    real (kind = wp) :: upd_scl = 1.0_wp

        !> weight of the central bin in bias smoothing
    real (kind = wp) :: wgt_smooth = 0.5_wp

        !> For TM: flag determining if 'tm_matrix' is read from a file at initialisation
    logical :: tm_resume = .false.

        !> Flag determining if only the tridiagonal elements of the transition matrix will
        !> be stored and output and read from TMATRX files (as opposed to the full matrix)
    logical :: tm_tridiag = .false.

        !> For confining the order parameter to a specific window in order-parameter space
    logical :: is_window = .false.

        !> Lower bound (inclusive) of the window in order parameter space
    real (kind = wp) :: window_min = -1.0E100_wp

        !> Upper bound (inclusive) of the window in order parameter space
    real (kind = wp) :: window_max = 1.0E100_wp

        !> Atomic species label (element number) for species earmarked for 'nspecatom' order
        !> parameter 
    integer :: par_natoms_spec_label

        !> Atomic species type (integer label for 'core', 'spin' etc.) for species earmarked 
        !> for 'nspecatom' order parameter
    integer :: par_natoms_spec_type

        !> Name of atomic species and type (e.g. 'Ce core')  earmarked for 'nspecatom' order
        !> parameter
    character*40 :: par_natoms_spec_name = 'none'


end type fed_interface

type fed_interface_name

        !> name for flavor of FED calculation (generic/Lattice-Switch/Phase-Switch/Histogram-Reweights/none)
    character*40 :: flavor = 'none'

        !> name for method of FED calculation (Umbrella/X-Ensemble/Wang-Landau/Transition-Matrix/none)
    character*40 :: method = 'none'

        !> name for FED order parameter (COM-dist/Temperature/Beta/Lambda/Volume/Density/none)
    character*40 :: parameter = 'none'

end type fed_interface_name

end module fed_interface_type
