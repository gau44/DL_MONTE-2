! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! *                                                                         *
! *   T.L.Underwood - Lattice/Phase-Switch MC method & optimizations        *
! *   t.l.Underwood[@]bath.ac.uk                                            *
! ***************************************************************************

!> @brief
!> - Configuration (microstate): reading CONFIG file, setting up atom positions
!> @usage 
!> - configuration set via atom positions, `<config_type>` instance `'cfg'` normally
!> - @stdusage 
!> - @stdspecs

!> @modulefor configuration (microstate) treatment: CONFIG file, atom positions

module config_module

    implicit none

contains

!> allocate arrays for CONIFG fles
subroutine alloc_config_molecules(cfg)

    use config_type
    use molecule_type

    implicit none

        !> configuration(s) container - array in the case of several replicas being used
    type (config), intent(inout) :: cfg

    if (cfg%mxmol < cfg%num_mols .or. cfg%mxmol == 0) cfg%mxmol = cfg%num_mols

    ! allocate molecule container
    allocate(cfg%mols(cfg%mxmol))

    cfg%newjob = .true.

end subroutine

!> allocate arrays for a molecular type (species)
subroutine alloc_species(mtype)

    use config_type
    use molecule_type
    use species_module, only : number_of_elements

    implicit none

    !> molecular species container
    type(species), intent(inout) :: mtype

    if( mtype%max_mols < mtype%num_mols .or. mtype%max_mols == 0) &
        mtype%max_mols = mtype%num_mols

    !array for molecule ids/indices
    if( allocated(mtype%mol_id) ) deallocate(mtype%mol_id)
    allocate(mtype%mol_id(mtype%max_mols))

    mtype%mol_id(:) = 0

    if( allocated(mtype%num_elems) ) deallocate(mtype%num_elems)
    if( .not.allocated(mtype%num_elems) ) &
        allocate(mtype%num_elems(number_of_elements))

    mtype%num_elems(:) = 0

    !array for molecules
    !allocate(mtype%mols(mtype%max_mols))

end subroutine alloc_species

!> read in CONFIG file containing atom positions (within molecules)
subroutine inputconfig(job, ib, cfg, cfgfmt)

    use kinds_f90
    use config_type
    use species_module
    use molecule_type
    use atom_module, only : set_atom_type, set_atom_pos, set_atom_type, set_atom_site, &
                            set_atom_zero_pos, set_atom_spin
    use control_type
    use constants_module
    use parse_module
    use latticevectors_module
    use coul_module
    use molecule_module, only : alloc_mol_atms_arrays, alloc_and_copy_mol_arrays, &
                                copy_molecule, set_molecule_mass, set_molecule_charge
    use comms_mpi_module, only : master

    implicit none

        !> global simulation control switches & parameters
    type (control), intent(inout) :: job

        !> global configuration container (3D atom positions, cartesian)
    type (config), intent(inout) :: cfg

        !> index of the input configuration (appears not being used here, but see `cell_module.f90`)
    integer, intent(in) :: ib

        !> flag for the input format: 1/2 - Cartesian (cubic/orthorhombic), 0 - fractional, else -> error(209)
    integer, intent(inout) :: cfgfmt

    integer :: i, j, k, imp, imq, natms, num_mol_atms, max_mol_atms, indx, numi, max_mtype
    integer :: kw0, kw1, kw2, kw3

    integer :: nread, atmtype, levcfg

    real (kind = wp) :: vol, pos(3), shift(3), pos1(3), rsq, rsq1, rsq2, cosphi, spin(3)

    character :: record*100, word*40, chmol*40
    character :: symbol*8

    !TU: The last line read in the file - usefull to output if something has gone wrong
    character :: wholeline*100
    character :: errormsg*72
    
    logical :: safe, founda, is_conf_ortho, is_nm, is_float
    
    type(molecule), target :: temp_mol

    nread = ucfg
    cfg%number_of_atoms = 0
    cfg%maxno_of_atoms = 0
    indx = 0

    call get_line(safe, nread, record)
    wholeline = record
    if (.not.safe) go to 1000
    cfg%cfg_title = record(1:80)

    call get_line(safe, nread, record)
    wholeline = record
    if (.not.safe) go to 1000
    call get_word(record, word)
    levcfg = nint(word_2_real(word))
    call get_word(record, word)
    cfgfmt = nint(word_2_real(word))

    natms = 0
    is_float = .false.
    call get_word(record, word)
    if( indexnum(word,is_float) == 1 .and. .not.is_float) natms = nint(word_2_real(word))

    is_nm = ( cfgfmt < 0 )

    if( is_nm ) cfgfmt=-cfgfmt

!AB: 'use orthogonal' is a directive in CONTROL requesting to use orthogonal PBC/Ewald routes if possible 
!AB: (introduced by JG)
    is_conf_ortho = job%useorthogonal

!AB: Below is a check for consistency of CONFIG input vs CONTROL (prior checking the cell matrix)
!AB: If this check is not done one can start with the orthogonal cell initially, but eventually 
!AB: end up with non-orthogonal cell (due to the most general volume move type allowed with cfgfmt=3).
!AB: Since this is the only place where the matrix orthogonality is checked, 
!AB: the ortho-flags will be preserved, presuming cell orthogonality throughout the entire simulation,
!AB: which is not guaranteed for cfgfmt > 2.

    if( abs(cfgfmt) > 2 .and. is_conf_ortho ) &
        call cry(uout,'', &
                "ERROR: cell format in CONFIG incompatible with directive 'use ortho [PBC]' in CONTROL "//&
               &"(amend either CONTROL or CONFIG) !!!",999)

    !read in lattice vectors
    call inputlatticevectors(cfg%vec)

    !Check orthogonality of lattice vectors, or not
    call checkorthogonality( cfg%vec, is_conf_ortho )

!    print *, "useorthogonal = ", job%useorthogonal

    if( is_conf_ortho .neqv. job%useorthogonal ) then
        job%useorthogonal = is_conf_ortho
        call cry(uout,'', &
                 "WARNING: cell matrix in CONFIG incompatible with directive 'use ortho [PBC]' in CONTROL - the flag is reset!",0)
!                "ERROR: cell matrix in CONFIG incompatible with directive 'use ortho [PBC]' in CONTROL "//&
!               &"(amend CONTROL or CONFIG) !!!",999)
    endif

    if( job%is_slit .and. abs(cfgfmt) == 3 ) &
        call cry(uout,'', &
                "ERROR: slit format in CONFIG incompatible with cell type in CONTROL [no slit directive] "//&
               &"(amend either CONTROL or CONFIG) !!!",999)

    !TU: Check cell shape is appropriate for NPT ensemble
    select case( job%type_vol_move )

           case(NPT_LOG, NPT_INV2, NPT_LIN2)
           
               !TU: Orthorhombic cell is required
               if( abs(cfgfmt) > 2 &
              .or. cfg%vec%latvector(1,2) /= 0.0_wp .or. cfg%vec%latvector(1,3) /= 0.0_wp &
              .or. cfg%vec%latvector(2,1) /= 0.0_wp .or. cfg%vec%latvector(2,3) /= 0.0_wp &
              .or. cfg%vec%latvector(3,1) /= 0.0_wp .or. cfg%vec%latvector(3,2) /= 0.0_wp ) &
                   call cry(uout,'', &
                            "ERROR: NPT ensemble (volume move) specs inapplicable to NON-ORTHORHOMBIC cell "//&
                           &"(amend either CONTROL or CONFIG) !!!",15)

           case(NPT_LIN) !(NPT_INV, NPT_LIN)
               
               !TU: Cubic cell is required

               if( abs(cfgfmt) > 1 &
              .or. cfg%vec%latvector(1,1) /= cfg%vec%latvector(2,2) &
              .or. cfg%vec%latvector(1,1) /= cfg%vec%latvector(3,3) &
              .or. cfg%vec%latvector(2,2) /= cfg%vec%latvector(3,3) & 
              .or. cfg%vec%latvector(1,2) /= 0.0_wp .or. cfg%vec%latvector(1,3) /= 0.0_wp &
              .or. cfg%vec%latvector(2,1) /= 0.0_wp .or. cfg%vec%latvector(2,3) /= 0.0_wp &
              .or. cfg%vec%latvector(3,1) /= 0.0_wp .or. cfg%vec%latvector(3,2) /= 0.0_wp ) &
                   call cry(uout,'', &
                            "ERROR: NPT ensemble (volume move) specs inapplicable to NON-CUBIC cell "//&
                           &"(amend either CONTROL or CONFIG) !!!",15)

           case(NPT_VEC)

               !AB: Parallelepiped cell is required (most general case)
               if( abs(cfgfmt) /= 3 ) &
                   call cry(uout,'', &
                            "ERROR: NPT ensemble (volume move) specs inconsistent with NON-PARALLELEPIPED cell "//&
                           &"(amend either CONTROL or CONFIG) !!!",15)

           case default

               !TU: I.e., NPT_OFF or NPT_VEC
               !TU: Do nothing - any cell shape is allowed

               continue

    end select

    call cellsize(cfg%vec, vol)

    !AB: On request from a user (in discussions), check the cutoffs and warn the user if necessary
    if(  job%shortrangecut > HALF*cfg%vec%latvector(1,1) &
    .or. job%shortrangecut > HALF*cfg%vec%latvector(2,2) &
    .or. job%shortrangecut > HALF*cfg%vec%latvector(3,3) &
    .or. job%vdw_cut > HALF*cfg%vec%latvector(1,1) &
    .or. job%vdw_cut > HALF*cfg%vec%latvector(2,2) &
    .or. job%vdw_cut > HALF*cfg%vec%latvector(3,3) ) &
         call cry(uout,'', &
              "WARNING: Cutoff radius (global and/or VDW) is greater than half the cell dimension(s) "//&
             &"- be ware!",0)

    call get_line(safe, nread, record)
    wholeline = record
    if (.not.safe) go to 1000
    call get_word(record, word)
    call lower_case(word)
    if (word(1:6) /= 'nummol') then
        !call error(104)
        errormsg = "'nummol' keyword expected but not found"
        goto 1000
    end if

    call get_word(record, word)
    if( word == "" .or. word(1:1) == "#" ) then
        errormsg = "'nummol' must be followed by number of molecules"
        go to 1000
    end if
    cfg%num_mols = nint(word_2_real(word))

    !AB: any molecule present in CONFIG?

    !TU: Old code is below; I've allowed 0-molecule configurations
    ! if(cfg%num_mols < 1) then
    !     errormsg = "'nummol' must be > 0"
    !     go to 1000
    ! end if

    allocate(cfg%mtypes(number_of_molecules))
    allocate(cfg%mxmol_type(number_of_molecules))
    
    cfg%mxmol_type = 0
    cfg%mxmol = 0

    do k = 1, number_of_molecules

        call get_word(record, word)
        if( word == "" .or. word(1:1) == "#" .or. word(1:5) == "shift" ) then
            errormsg = "expected more integers after 'nummol'"
            go to 1000
        end if
        
        max_mtype = nint(word_2_real(word))

        cfg%mxmol_type(k) = max_mtype
        cfg%mtypes(k)%max_mols = max_mtype
        cfg%mtypes(k)%num_mols = 0

        if( max_mtype > 0) then

            cfg%mxmol = cfg%mxmol + max_mtype
            cfg%maxno_of_atoms = cfg%maxno_of_atoms + max_mtype * uniq_mol(k)%mxatom

            call alloc_species(cfg%mtypes(k))

            cfg%mtypes(k)%is_field      =  uniq_mol(k)%is_field
            cfg%mtypes(k)%rigid_body    =  uniq_mol(k)%rigid_body
            cfg%mtypes(k)%exc_coul_ints =  uniq_mol(k)%exc_coul_ints

        else

            errormsg = "Max. number of molecules after 'nummol' must be > 0"
            go to 1000
            
        endif

    enddo

    !AB: find out if any displacement of the entire configuration is required
    shift(:) = 0.0_wp

    call get_word(record, word)
    call lower_case(word)
    if( word(1:5) == 'shift' ) then

        call get_word(record, word)
        if( word == "" .or. word(1:1) == "#" ) then
            errormsg = "expected 3 real values after 'shift'"
            go to 1000
        end if
        shift(1) = word_2_real(word)
        call get_word(record, word)
        if( word == "" .or. word(1:1) == "#" ) then
            errormsg = "expected 3 real values after 'shift'"
            go to 1000
        end if
        shift(2) = word_2_real(word)
        call get_word(record, word)
        if( word == "" .or. word(1:1) == "#" ) then
            errormsg = "expected 3 real values after 'shift'"
            go to 1000
        end if
        shift(3) = word_2_real(word)

        if( master ) & 
          write(uout,"(/,1x,3(a,e15.6),a)")'shifting configuration in this cell by dR = ( ', &
                shift(1),", ",shift(2),", ",shift(3)," )"
    endif

    call alloc_config_molecules(cfg)

    imp = 0
    imq = 0

    allocate(cfg%nums_elemts(number_of_elements))
    cfg%nums_elemts = 0

    do i = 1, cfg%num_mols

        call get_line(safe, nread, record)
        wholeline = record
        if (.not.safe) go to 1000

        call get_word(record, word)
        call lower_case(word)
        
        write(chmol,*) i

        if( word(1:6) /= 'molecu' ) then
            !TU: OLD CODE
            !            call cry(uout,'', &
            !                 "ERROR: could not read data for molecule "//trim(adjustL(chmol))//" !!!",103)
            errormsg = " could not read data for molecule "//trim(adjustL(chmol))
            goto 1000
        end if

        call get_word(record, word)
        if( word == "" .or. word(1:1) == "#" ) then
            errormsg = "expected molecule name after 'molecule'"
            go to 1000
        end if
        cfg%mols(i)%molname = word
        
        chmol = trim(chmol)//" '"//trim(cfg%mols(i)%molname)//"' "
        
        founda = .false.
        do k = 1, number_of_molecules

            if (cfg%mols(i)%molname == uniq_mol(k)%molname) then
            
                founda = .true.

                cfg%mols(i)%mol_label = k
                
                cfg%mtypes(k)%num_mols = cfg%mtypes(k)%num_mols+1
                
                if( cfg%mtypes(k)%num_mols > cfg%mtypes(k)%max_mols ) &
                    call cry(uout,'', &
                        "ERROR: too many instances of molecule type "//trim(adjustL(chmol))// &
                        " in CONFIG !!! Increase max no. of molecules?",200)

                cfg%mtypes(k)%mol_id( cfg%mtypes(k)%num_mols ) = i

                max_mol_atms = uniq_mol(k)%mxatom

                cfg%mols(i)%is_field      = uniq_mol(k)%is_field
                cfg%mols(i)%rigid_body    = uniq_mol(k)%rigid_body
                cfg%mols(i)%exc_coul_ints = uniq_mol(k)%exc_coul_ints

                imq = k

                exit

            end if

        end do

        if(.not.founda) then
            !TU: OLD CODE
            !call error(107)
            errormsg = "molecule type '"//trim(cfg%mols(i)%molname)//"' not defined in FIELD"
            goto 1000
        end if

        !get size of molecule and set atom arrays
        call get_word(record, word)
        if( word == "" .or. word(1:1) == "#" ) then
            errormsg = "expected number of atoms after '"//trim(cfg%mols(i)%molname)//"'"
            go to 1000
        end if
        num_mol_atms = nint(word_2_real(word))

        !AB: any atom in molecule?

        !TU: Old code is below; I've allowed 0 atoms in a molecule
        ! if (num_mol_atms < 1) then
        !     errormsg = "number of atoms in molecule must be > 0"
        !     goto 1000
        ! end if
            
        !AB: too many atoms in molecule?
        if( num_mol_atms > max_mol_atms .or. max_mol_atms < 1 ) then
            errormsg = "too many atoms in molecule '"//trim(cfg%mols(i)%molname)//"'. Increase max atoms in FIELD?"
            goto 1000
        end if
        
        call alloc_mol_atms_arrays(cfg%mols(i), num_mol_atms, max_mol_atms, &
             job%lauto_nbrs, job%lsample_displacement)

!debugging
!        if( cfg%mols(i)%mol_label /= uniq_mol(k)%mol_label ) then
!            write(uout,"(/,1x,a)")"inconsistent labels ", &
!                cfg%mols(i)%mol_label, uniq_mol(k)%mol_label, &
!                " for molecule instance and uniq_mol '",uniq_mol(k)%molname,"'"
!        end if

        !TU: Insist that the number of atoms in this molecule and the analogous
        !TU: template molecule are the same if unique charges are in use
        if( unique_charges(cfg%mols(i)%mol_label) .and. &
            cfg%mols(i)%natom /= uniq_mol(cfg%mols(i)%mol_label)%natom ) then
           
            call cry(uout,'', &
                     "ERROR: Molecular species with unique atomic charges must "// &
                     "have same number of atoms in molecules in CONFIG as in FIELD.",999)

        end if

        do j = 1, cfg%mols(i)%natom

            call get_line(safe, nread, record)
            wholeline = record
            if (.not.safe) go to 1000

            !call lower_case(record)
            call get_word(record, symbol) 
            if(len_trim(symbol) == 0) then
                call cry(uout,'', &
                    "Unexpected blank line in CONFIG where atom definition should be!",999)
            end if
            
            call get_word(record, word)
            if(len_trim(word) == 0) then

                word = "core"
                
            endif

            atmtype = get_species_type(word)

            founda = .false.
            do k = 1, number_of_elements

                if(symbol == element(k) .and. atmtype == eletype(k)) then
                
                    founda = .true.
                    
                    cfg%mols(i)%atms(j)%atmname = symbol

                    if( unique_charges(cfg%mols(i)%mol_label) ) then

                        !TU: Check that the atomic species matches that of the analogous atom in the molecule
                        !TU: template if unique atomic charges are in use for this molecule
                        if( k  /= uniq_mol(cfg%mols(i)%mol_label)%atms(j)%atlabel .or. &
                            atmtype /= uniq_mol(cfg%mols(i)%mol_label)%atms(j)%atype        ) then

                            call cry(uout,'', &
                                "ERROR: Molecular species with unique atomic charges must "// &
                                "have same atom order in molecules in CONFIG as in FIELD.",999)

                        end if

                        !TU: Set the charge of this atom to the corresponding atom ('j') in 
                        !TU: the relevenat template molecule (defined in FIELD).
                        call set_atom_type(cfg%mols(i)%atms(j), k, &
                             uniq_mol(cfg%mols(i)%mol_label)%atms(j)%charge, atm_mass(k), atmtype)

                    else
                    
                        call set_atom_type(cfg%mols(i)%atms(j), k, atm_charge(k), atm_mass(k), atmtype)

                    end if
                    
                    if( cfg%mtypes(imq)%num_mols == 1 ) &
                        cfg%mtypes(imq)%num_elems(k) = cfg%mtypes(imq)%num_elems(k)+1

                    cfg%nums_elemts(k) = cfg%nums_elemts(k)+1

!debugging
!                    if( i==1 .or. i==cfg%num_mols ) then
!                        write(uout,*)
!                        write(uout,*)cfg%mols(i)%atms(j)%atype, &
!                                     cfg%mols(i)%atms(j)%mass, &
!                                     cfg%mols(i)%atms(j)%charge, &
!                                     cfg%mols(i)%atms(j)%is_charged
!                    endif

                endif

            enddo

            if(.not.founda) then

                !TU: OLD CODE
                !call error(108)
                errormsg = "Atom species present not defined in FIELD"
                goto 1000

            end if

            call get_line(safe, nread, record)
            wholeline = record
            if (.not.safe) go to 1000

            !call lower_case(record)
            call get_word(record, word)
            if( word == "" .or. word(1:1) == "#" ) then
                errormsg = "expected 3 coordinates for atom '"//trim(symbol)//"'"
                go to 1000
            end if
            pos(1) = word_2_real(word)            
            call get_word(record, word)
            if( word == "" .or. word(1:1) == "#" ) then
                errormsg = "expected 3 coordinates for atom '"//trim(symbol)//"'"
                go to 1000
            end if
            pos(2) = word_2_real(word)
            call get_word(record, word)
            if( word == "" .or. word(1:1) == "#" ) then
                errormsg = "expected 3 coordinates for atom '"//trim(symbol)//"'"
                go to 1000
            end if
            pos(3) = word_2_real(word)

            pos(:) = pos(:)+shift(:) ! AB: translate all positions (nm or Angstroem)

            if( is_nm ) then
                pos(1) = pos(1)*10.0_wp ! AB: transform nm -> Angstroem
                pos(2) = pos(2)*10.0_wp ! AB: transform nm -> Angstroem
                pos(3) = pos(3)*10.0_wp ! AB: transform nm -> Angstroem
            endif

            !AB: check if any atom is outside the box
            if( cfgfmt == 0 ) then

                if(  abs(pos(1)) > 0.5_wp &
                .or. abs(pos(2)) > 0.5_wp &
                .or. abs(pos(3)) > 0.5_wp ) then

                    if( master ) then
                        if( indx < 9 ) then

                            write(uout,*) 
                            write(uout,*) "WARNING: initial atom position outside the primary cell (",ib, &
                                          ") - molecule #",i,",  atom #",j,",  global #",indx + 1
                            write(uout,*) pos(:)

                        else if( indx == 9 ) then

                            write(uout,*) 
                            write(uout,*) "WARNING: initial atom position outside the primary cell - more atoms found..."

                        end if
                    end if

                end if

            else if( cfgfmt < 3 ) then
            !AB: NOTE: this check only makes sense for orthorhombic cell and/or fractional coordinates
            !AB: one would have to convert to fractional coordinates to make it general
            !AB: which implies doing so after the entire configuration has been swallowed

                if(  abs(pos(1)) > cfg%vec%latvector(1,1)*0.5_wp &
                .or. abs(pos(2)) > cfg%vec%latvector(2,2)*0.5_wp &
                .or. abs(pos(3)) > cfg%vec%latvector(3,3)*0.5_wp ) then

                    if( master ) then
                        if( indx < 9 ) then

                            write(uout,*) 
                            write(uout,*) "WARNING: initial atom position outside the primary cell (",ib, &
                                          ") - molecule #",i,",  atom #",j,",  global #",indx + 1
                            write(uout,*) pos(:)

                        else if( indx == 9 ) then

                            write(uout,*) 
                            write(uout,*) "WARNING: initial atom position outside the primary cell - more atoms found..."

                        end if
                    end if

                end if

            end if

            call set_atom_pos(cfg%mols(i)%atms(j), pos)
            call set_atom_zero_pos(cfg%mols(i)%atms(j), pos)

            call get_word(record, word)
            call set_atom_site(cfg%mols(i)%atms(j), nint(word_2_real(word)))

            if (levcfg >= 1) then
                call get_line(safe, nread, record)
                wholeline = record
                if (.not.safe) go to 1000
            endif

            if (levcfg >= 2) then
                call get_line(safe, nread, record)
                wholeline = record
                if (.not.safe) go to 1000
            endif

            !TU: Read spin for 'spin' type atoms from the 3 reals on the next line
            if( atmtype == ATM_TYPE_SPIN ) then

                if(levcfg > 0) then

                    call cry(uout,'', &
                        "ERROR: In CONFIG 'levcfg>0' (which includes atomic velocities) cannot be "//&
                        "used with 'spin' type atoms !!!",999)
                    
                end if

                call get_line(safe, nread, record)
                wholeline = record
                if (.not.safe) go to 1000
                call get_word(record, word)
                if( word == "" .or. word(1:1) == "#" ) then
                    errormsg = "expected orientation vector for atom '"//trim(symbol)//"'"
                    go to 1000
                end if
                spin(1) = word_2_real(word)
                call get_word(record, word)
                if( word == "" .or. word(1:1) == "#" ) then
                    errormsg = "expected orientation vector for atom '"//trim(symbol)//"'"
                    go to 1000
                end if
                spin(2) = word_2_real(word)
                call get_word(record, word)
                if( word == "" .or. word(1:1) == "#" ) then
                    errormsg = "expected orientation vector for atom '"//trim(symbol)//"'"
                    go to 1000
                end if
                spin(3) = word_2_real(word)

                call set_atom_spin(cfg%mols(i)%atms(j), spin)

            end if
                
            cfg%number_of_atoms = cfg%number_of_atoms + 1

            !set the global indices
            indx = indx + 1
            cfg%mols(i)%glob_no(j) = indx
            
        enddo !j = 1, cfg%mols(i)%natom

        !TU: What is this block of code used for? I think it does nothing and can be removed
        word = cfg%mols(i)%molname
        call lower_case(word)
        
        !AB: set molecule total mass and charge
        call set_molecule_mass(cfg%mols(i))
        call set_molecule_charge(cfg%mols(i))

        cycle


        !TU: The code below is unreachable due to the 'cycle' above! What should we
        !TU: do about this? The code looks useful!
        
        !AB: the code below is for testing molecules 
        if( cfgfmt /= 0 .and. trim(word) == "spce" ) then
        
            if( cfg%mols(i)%natom /= 3 ) &
                call cry(uout,'', &
                        "ERROR: number of atoms for SPC/E water model =/= 3 "//&
                       &"(amend CONFIG and/or FIELD) !!!",999)

            symbol = trim(adjustL(cfg%mols(i)%atms(1)%atmname))
            call lower_case(symbol)

            if( symbol(1:1) == 'o' ) then
                kw0 = 1
                kw1 = 2
                kw2 = 3
            endif 

            symbol = trim(adjustL(cfg%mols(i)%atms(2)%atmname))
            call lower_case(symbol)

            if( symbol(1:1) == 'o' ) then
                kw0 = 2
                kw1 = 1
                kw2 = 3
            endif

            symbol = trim(adjustL(cfg%mols(i)%atms(3)%atmname))
            call lower_case(symbol)

            if( symbol(1:1) == 'o' ) then
                kw0 = 3
                kw1 = 1
                kw2 = 2
            endif
            
            !if( master ) write(uout,*)"inputconfig(): SPCE water model atoms: O = ",kw0," H1 = ",kw1," H2 = ",kw2

            pos(:) = cfg%mols(i)%atms(kw1)%rpos(:) - cfg%mols(i)%atms(kw0)%rpos(:)
                        
            pos(1) = pos(1) - anint(pos(1)/cfg%vec%latvector(1,1))*cfg%vec%latvector(1,1)
            pos(2) = pos(2) - anint(pos(2)/cfg%vec%latvector(2,2))*cfg%vec%latvector(2,2)
            pos(3) = pos(3) - anint(pos(3)/cfg%vec%latvector(3,3))*cfg%vec%latvector(3,3)

            rsq = pos(1)**2 + pos(2)**2 + pos(3)**2

            if( abs(rsq-1.0_wp) > 1.e-3_wp ) then !&

                write(uout,*) 
                write(uout,*) "inputconfig(): check O-H distance (1) in SPC/E water molecule (",i,") = ",sqrt(rsq)
                
                call cry(uout,'', &
                    "ERROR: O-H distance (1) for SPC/E water model =/= 1.0 A "//&
                   &"(amend CONFIG and/or FIELD) !!!",999)
            endif
                  
            pos1(:) = cfg%mols(i)%atms(kw2)%rpos(:) - cfg%mols(i)%atms(kw0)%rpos(:)
            
            pos1(1) = pos1(1) - anint(pos1(1)/cfg%vec%latvector(1,1))*cfg%vec%latvector(1,1)
            pos1(2) = pos1(2) - anint(pos1(2)/cfg%vec%latvector(2,2))*cfg%vec%latvector(2,2)
            pos1(3) = pos1(3) - anint(pos1(3)/cfg%vec%latvector(3,3))*cfg%vec%latvector(3,3)
      
            rsq1 = pos1(1)**2 + pos1(2)**2 + pos1(3)**2

            if( abs(rsq1-1.0_wp) > 1.e-3_wp ) then !&

                write(uout,*) 
                write(uout,*) "inputconfig(): check O-H distance (2) in SPC/E water molecule (",i,") = ",sqrt(rsq1)
                
                call cry(uout,'', &
                    "ERROR: O-H distance (2) for SPC/E water model =/= 1.0 A "//&
                   &"(amend CONFIG and/or FIELD) !!!",999)
            endif
            
            cosphi = (pos(1)*pos1(1) + pos(2)*pos1(2) + pos(3)*pos1(3))/sqrt(rsq*rsq1)
            
            !rsq2 = (pos1(1) - pos(1))**2 + (pos1(2) - pos(2))**2 + (pos1(3) - pos(3))**2
            !cosphi = -(rsq2 - (rsq + rsq1)) / (2.0_wp*sqrt(rsq*rsq1))

            if( abs(cosphi-cos(109.47_wp*PI/180.0_wp)) > 1.e-3_wp ) then !&

                write(uout,*) 
                write(uout,*) "inputconfig(): check H-O-H angle in SPC/E water molecule (",i,") = ",&
                              acos(cosphi)*180.0_wp/PI," =/= ",acos(cos(109.47_wp*PI/180.0_wp))*180.0_wp/PI
                
                call cry(uout,'', &
                    "ERROR: H-O-H angle for SPC/E water model =/= 109.47 (deg)"//&
                   &"(amend CONFIG and/or FIELD) !!!",999)
            endif

        endif

    enddo

    if( master ) & 
        write(uout,"(/,1x,'the maximum no of atoms/molecules in this cell: ',i8,a,i8)") & 
              cfg%maxno_of_atoms," / ",cfg%mxmol 

    if (master) &
        write(uout,"(/,1x,'the factual no of atoms/molecules in this cell: ',i8,a,i8)") &
              cfg%number_of_atoms," / ",cfg%num_mols

    return

    
1000 continue

    !TU: Old code
    !call error(200)

    call cry(uout,'',"ERROR: reading CONFIG has stopped : "//trim(errormsg) &
             //achar(10)//"        Problematic line is: '"//trim(wholeline)//"'", 200)

end subroutine inputconfig


end module
