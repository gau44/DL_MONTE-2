!***************************************************************************
!   Copyright (C) 2016 by T L Underwood                                    *
!   t.l.underwood@bath.ac.uk                                               *
!                                                                          *
!***************************************************************************

!> Module containing procedures for I/O, initialisation and manipulation of
!> molecular orientations (local basis vectors)
module orientation_module

    use kinds_f90
    use constants_module

    implicit none

        !> Minimum 'allowed' separation between any pair of atoms in a molecule for it to be
        !> considered to have an orientation. If all pairs of atoms are found to be closer than 
        !> this when trying to choose a z-axis for the molecule then an error is thrown. The idea is
        !> to catch if atoms are on top of each other, in which case the orientation is meaningless.
    real(kind=wp), save :: ORIENTATION_SAFE_THRESHOLD = 0.05_wp

        !> Minimum 'allowed' separation between a pair of atoms in the xy-plane of a molecule for it
        !> to be regarded as not linear. If the projection of the separations between all pairs of 
        !> atoms onto the xy-plane are all less than this value then the molecule is regarded as linear.
    real(kind=wp), save :: ORIENTATION_LIN_THRESHOLD = 0.05_wp
    

        !> Array containing the 'class' of each molecular species, e.g., linear, general, without orientation.
    integer, allocatable, dimension(:), save :: orientation_class

        !> Array containing the 'rule' for calculating the local z-axis for each molecular species.
        !> For molecular species 's' the z-axis points from atom orientation_rule_z(1,s) to orientation_rule_z(2,s).
    integer, allocatable, dimension(:,:), save :: orientation_rule_z

        !> Array containing the 'rule' for calculating the local y-axis for each molecular species.
        !> For molecular species 's' the y-axis is along the projection of the vector from atom orientation_rule_y(1,s) to 
        !> orientation_rule_y(2,s) onto the xy-plane (where the xy-plane is defined as that perpendicular to the
        !> z-plane determined from the rule set out in orientation_rule_z).
    integer, allocatable, dimension(:,:), save :: orientation_rule_y


contains


    !> Initialises the orientation 'classes' and 'rules' for all molecular species. The classes and rules are
    !> determined from the template molecule for each species stored in `species_module`.
    subroutine setup_orientation(job)

        use control_type
        use comms_mpi_module, only : master
        use molecule_type
        use species_module, only : uniq_mol, number_of_molecules

        implicit none

            !> Simulation control variables
        type (control), intent(in) :: job

            ! Separation vector between pair of atoms and its squared magnitude
            ! - taking into account periodic boundary conditions
        real(kind=wp) :: sepvec(3), sepsq, sepvec_max(3), sepsq_max

            ! Projection of separation vector between pair of atoms onto x/y axis 
            ! and its squared magnitude - taking into account periodic boundary conditions
        real(kind=wp) :: sepvecxy(3), sepsqxy, sepvecxy_max(3), sepsqxy_max

            ! z-basis vector 
        real(kind=wp) :: zbasis(3)

            ! Molecular species index
        integer :: im

            ! Atom indices
        integer :: i, j


        ! Safety checks: forbid orientations being used in conjunction with atomic GCMC

        if( job%gcmcatom .or. job%swapatoms .or.job%semiwidomatms .or. job%gibbsatomtran &
            .or. job%gibbsatomexch .or. job%semigrandatms ) then
       
            call cry(uout, '', &
                "ERROR: Atom swaps and/or insertions are forbidden with 'use orientation'.", 999)

        end if

        if( master ) then
        
            write(uout,*)    
            write(uout,*) "-------------------------------------------------"
            write(uout,*) "              orientation rules                  "
            write(uout,*) "-------------------------------------------------"
            write(uout,*)
            
        end if


        allocate(orientation_class(number_of_molecules))
        allocate(orientation_rule_z(2,number_of_molecules))
        allocate(orientation_rule_y(2,number_of_molecules))
        orientation_class = -1
        orientation_rule_z = -1
        orientation_rule_y = -1

        ! Loop over molecular species
        do im = 1, number_of_molecules
            

            if(master) write(uout,*) "molecule name         : "//trim(uniq_mol(im)%molname)

            ! Determine the class of this species
            
            if( uniq_mol(im)%natom <= 1 ) then

                ! There is no orientation associated with this molecule.
                orientation_class(im) = ORIENTATION_NONE

                if(master) then
                    
                    write(uout,*) "orientation class     : no orientation"
                    write(uout,*)

                end if

            else if( uniq_mol(im)%natom == 2) then

                ! The molecule is linear
                orientation_class(im) = ORIENTATION_LIN

                if(master) write(uout,*) "Orientation class     : linear"

                ! For linear molecules we only consider the z-direction: the y- and x-directions are redundant
                ! and left unused

                ! Set the rule such that the orientation points from atom 1 to atom 2
                orientation_rule_z(1,im) = 1
                orientation_rule_z(2,im) = 2

                ! ... but check that the atoms are not too close to each other so to make
                ! the orientation meaningless.

                sepvec = uniq_mol(im)%atms(2)%rpos - uniq_mol(im)%atms(1)%rpos
                sepsq =  dot_product(sepvec, sepvec)

                if( sqrt(sepsq) < ORIENTATION_SAFE_THRESHOLD) then

                    call cry(uout,'', &
                        "ERROR: Atom separation in a template molecule below safe value for defining orientations", 999)

                end if

                if(master) write(uout,'(a,i4,a,i4)') "z-axis rule           : atom ",orientation_rule_z(1,im), &
                    " -> atom ",orientation_rule_z(2,im)


            else

                ! The molecule is 'general' for now, but still could in fact be linear! - see below
                orientation_class(im) = ORIENTATION_GEN

                ! Determine the rule procedurally

                ! The pair of atoms within the molecule with the largest separation will define the z-axis
                ! Find this pair of atoms and set 'rule' accordingly. Note that the z-axis will by construction
                ! point from the lower index atom to the higher index atom

                sepvec_max = 0.0_wp
                sepsq_max = 0.0_wp

                do i = 1, uniq_mol(im)%natom

                    do j = i+1, uniq_mol(im)%natom

                        sepvec = uniq_mol(im)%atms(j)%rpos - uniq_mol(im)%atms(i)%rpos
                        sepsq = dot_product(sepvec,sepvec)

                        if(sepsq > sepsq_max) then

                            sepsq_max = sepsq
                            sepvec_max = sepvec

                            orientation_rule_z(1,im) = i
                            orientation_rule_z(2,im) = j

                        end if

                    end do

                end do

                ! Check that the atoms are all not too close to each other so to make
                ! the orientation meaningless.

                if( sqrt(sepsq_max) < ORIENTATION_SAFE_THRESHOLD) then

                    call cry(uout,'', &
                        "ERROR: Atom separation in a template molecule below safe value for defining orientations", 999)

                end if


                ! The pair of atoms with the largest projection on to the xy-plane (using the z-axis calculated
                ! above) will define the y-axis. Find this pair of atoms and set 'rule' accordingly.

                zbasis = sepvec_max / sqrt(sepsq_max)

                sepvecxy_max = 0.0_wp
                sepsqxy_max = 0.0_wp

                do i = 1, uniq_mol(im)%natom

                    do j = i+1, uniq_mol(im)%natom

                        sepvec = uniq_mol(im)%atms(j)%rpos - uniq_mol(im)%atms(i)%rpos
                        sepvecxy = sepvec - zbasis * dot_product(zbasis, sepvec)

                        sepsqxy = dot_product(sepvecxy,sepvecxy)

                        if(sepsqxy > sepsqxy_max) then

                            sepsqxy_max = sepsqxy
                            sepvecxy_max = sepvecxy

                            orientation_rule_y(1,im) = i
                            orientation_rule_y(2,im) = j

                        end if

                    end do

                end do

                ! Check that the atom separations in the xy-plane are all not too close to each other so to make
                ! the y-axis meaningless, i.e., check that the molecule is not linear. If it is linear then
                ! redefine the class of the molecule accordingly

                if( sqrt(sepsqxy_max) < ORIENTATION_LIN_THRESHOLD) then

                    orientation_class(im) =  ORIENTATION_LIN


                    if(master) then
                        
                        write(uout,*) "orientation class     : linear"
                        write(uout,'(a,i4,a,i4)') " z-axis rule           : atom ",orientation_rule_z(1,im), &
                            " -> atom ",orientation_rule_z(2,im)
                        write(uout,*)

                    end if

                else

                    if(master) then

                        write(uout,*) "orientation class     : general"
                        write(uout,'(a,i4,a,i4)') " z-axis rule           : atom ",orientation_rule_z(1,im), &
                            " -> atom ",orientation_rule_z(2,im)
                        write(uout,'(a,i4,a,i4)') " y-axis rule           : atom ",orientation_rule_y(1,im), &
                            " -> atom ",orientation_rule_y(2,im)
                        write(uout,*)

                    end if

                end if


            end if     

        end do

    end subroutine setup_orientation




    !> Sets the orientation for a specified molecule using the class and 'rules' corresponding to
    !> its molecular species
    subroutine set_orientation(ib, im)

        use cell_module, only : cfgs, pbc_sepvec_atoms

        implicit none

            !> Configuration identifier (index in '`cfgs`' array)
        integer, intent(in) :: ib

            !> Index of molecule
        integer, intent(in) :: im

            ! Species of the molecule
        integer :: typ

            ! Separation vector - taking into account periodic boundary conditions
        real(kind=wp) :: sepvec(3)

            ! x, y and z basis vectors
        real(kind=wp) :: zbasis(3), ybasis(3), xbasis(3)

            ! Atom labels
        integer :: i, j


        typ = cfgs(ib)%mols(im)%mol_label

        select case(orientation_class(typ))

        case(ORIENTATION_NONE)
           
            ! Do nothing if the molecule has no orientation

            return

        case(ORIENTATION_LIN)

            ! For linear molecules only the z basis vector matters

            ! The normalised vector from atom orientation_rule_z(1,typ) to orientation_rule_z(2,typ)
            ! will be the z-axis for the molecule

            i = orientation_rule_z(1,typ)
            j = orientation_rule_z(2,typ)

            sepvec = pbc_sepvec_atoms(ib, im, i, im, j)
            
            cfgs(ib)%mols(im)%orientation(:,3) = sepvec / sqrt( dot_product(sepvec, sepvec) )

        case(ORIENTATION_GEN)

            ! For general molecules all basis vectors must be calculates

            ! The normalised vector from atom orientation_rule_z(1,typ) to orientation_rule_z(2,typ)
            ! will be the z-axis for the molecule

            i = orientation_rule_z(1,typ)
            j = orientation_rule_z(2,typ)

            sepvec = pbc_sepvec_atoms(ib, im, i, im, j)

            zbasis = sepvec / sqrt( dot_product(sepvec, sepvec) )

            cfgs(ib)%mols(im)%orientation(:,3) = zbasis

            ! The vector from atom orientation_rule_y(1,typ) to orientation_rule_y(2,typ) defines the 
            ! y-axis for the molecule: the projection of this vector onto the xy-plane (using the 
            ! z-axis calculated above) is taken to be the y-axis

            i = orientation_rule_y(1,typ)
            j = orientation_rule_y(2,typ)

            sepvec = pbc_sepvec_atoms(ib, im, i, im, j)

            ybasis = sepvec - zbasis * dot_product(zbasis, sepvec)
            ybasis = ybasis / sqrt( dot_product(ybasis, ybasis) )

            cfgs(ib)%mols(im)%orientation(:,2) = ybasis

            ! The x-axis is calculated from the cross product of the other two axes

            xbasis(1) = ybasis(2) * zbasis(3) - zbasis(2) * ybasis(3)
            xbasis(2) = - ybasis(1) * zbasis(3) + zbasis(1) * ybasis(3)
            xbasis(3) = ybasis(1) * zbasis(2) - zbasis(1) * ybasis(2)

            cfgs(ib)%mols(im)%orientation(:,1) = xbasis

        case default

            call cry(uout,'', &
                     "ERROR: Unrecognised molecular orientation class ", 999)

        end select

    end subroutine set_orientation




    !> Sets the orientations for all molecules in the specified box using using the classes and 
    !> 'rules' corresponding to their molecular species
    subroutine set_orientations(ib)

        use cell_module, only : cfgs

        implicit none

            !> Configuration identifier (index in '`cfgs`' array)
        integer, intent(in) :: ib

        integer :: im

        do im = 1, cfgs(ib)%num_mols

            call set_orientation(ib, im)

        end do

    end subroutine set_orientations




    !> Outputs all orientation objects in the specified box to the specified unit
    subroutine dump_orientations(unit, ib, istep)

        use cell_module, only : cfgs

        implicit none

            !> Unit to write to
        integer, intent(in) :: unit

            !> Configuration identifier (index in '`cfgs`' array)
        integer, intent(in) :: ib

            !> Iteration number (used in the header of the frame for this iteration)
        integer, intent(in) :: istep

        integer :: im, i, typ


        ! Header
        write(unit,'(a,i11,a,i4)') "Iteration ",istep," : box ",ib

        ! Cell matrix
        write(unit,'(3f15.7)') (cfgs(ib)%vec%latvector(1,i), i = 1,3)
        write(unit,'(3f15.7)') (cfgs(ib)%vec%latvector(2,i), i = 1,3)
        write(unit,'(3f15.7)') (cfgs(ib)%vec%latvector(3,i), i = 1,3)

        ! Number of molecules
        write(unit,"(i10)") cfgs(ib)%num_mols

        ! Molecule data
        do im = 1, cfgs(ib)%num_mols

            ! Get molecule type
            typ = cfgs(ib)%mols(im)%mol_label

            select case(orientation_class(typ))

            case(ORIENTATION_NONE)
           
                write(unit,'(2x,a,a)') cfgs(ib)%mols(im)%molname," no orientation"
                write(unit,'(2x,3f15.7)') cfgs(ib)%mols(im)%rcom(:)

            case(ORIENTATION_LIN)

                write(unit,'(2x,a,a)') cfgs(ib)%mols(im)%molname," linear"
                write(unit,'(2x,3f15.7)') cfgs(ib)%mols(im)%rcom(:)
                write(unit,'(2x,3f15.7)') cfgs(ib)%mols(im)%orientation(:,3)
                
            case(ORIENTATION_GEN)

                write(unit,'(2x,a,a)') cfgs(ib)%mols(im)%molname," general"
                write(unit,'(2x,3f15.7)') cfgs(ib)%mols(im)%rcom(:)
                write(unit,'(2x,3f15.7)') cfgs(ib)%mols(im)%orientation(:,1)
                write(unit,'(2x,3f15.7)') cfgs(ib)%mols(im)%orientation(:,2)
                write(unit,'(2x,3f15.7)') cfgs(ib)%mols(im)%orientation(:,3)
                
            case default
                
                call cry(uout,'', &
                    "ERROR: Unrecognised molecular orientation class", 999)
                
            end select

        end do

    end subroutine dump_orientations





end module orientation_module
