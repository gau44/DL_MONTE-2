! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   J. Grant - Modifications for Ewald refactor                           *
! *   r.j.grant[@]bath.ac.uk                                                  *
! ***************************************************************************

module coul_type

    use kinds_f90

    implicit none

type coul

        !> flag indicating whether it is the first time through
    logical ::  newjob

        !> no of cells to search over x for g vectors
    integer ::    gcellx

        !> no of cells to search over y for g vectors
    integer ::    gcelly

        !> no of cells to search over z for g vectors
    integer ::    gcellz

        !> the size of g-vector arrays - padded to allow volume to change
    integer ::    gvector_size = 0

        !> total no of gvectors
    integer ::    numgvec

    !JG: nxyz associated with each rcp vector
    integer, allocatable, dimension(:,:)   ::    nxyz
!    integer, allocatable, dimension(:)   ::    ny
!    integer, allocatable, dimension(:)   ::    nz
        !> tells the recip space to use gsum
    logical ::    gsumrecip


        !> eta of ewald sum
    real(kind = wp) :: eta

        !> recip space cutoff
    real(kind = wp) :: rcpspacecut

        !> real space cutoff
    real(kind = wp) :: realspacecut

    real(kind = wp), allocatable, dimension(:)   ::    gx
    real(kind = wp), allocatable, dimension(:)   ::    gy
    real(kind = wp), allocatable, dimension(:)   ::    gz

    real(kind = wp), allocatable, dimension(:)   ::    gxx
    real(kind = wp), allocatable, dimension(:)   ::    gyy
    real(kind = wp), allocatable, dimension(:)   ::    gzz

    real(kind = wp), allocatable, dimension(:)   ::    gxy
    real(kind = wp), allocatable, dimension(:)   ::    gxz
    real(kind = wp), allocatable, dimension(:)   ::    gyz

    real(kind = wp), allocatable, dimension(:)   ::    g0
    real(kind = wp), allocatable, dimension(:)   ::    g1

        !> atom cos phase sum
    real(kind = wp), allocatable, dimension(:)   ::    cphsum
        !> atom sin phase sum
    real(kind = wp), allocatable, dimension(:)   ::    sphsum

        !> atom complex rcpsum
    complex(kind = wp), allocatable, dimension(:)   ::    rcpsum

end type

end module
