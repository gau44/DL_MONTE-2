! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! *                                                                         *
! *   T.L.Underwood - Lattice/Phase-Switch MC method & optimizations        *
! *   t.l.Underwood[@]bath.ac.uk                                            *
! ***************************************************************************

!TU: A WARNING regarding the transition matrix implementation in MC moves here:
!TU: Moves which are rejected 'early' due to exceeding the VdW cap, or due
!TU: to an atom being outside the slit, are NOT counted in the transition 
!TU: matrix: the transition matrix is not incremented for such rejected moves.
!TU: This may cause artifacts in FED results. One solution to this is to 
!TU: increase the VdW cap when using transition matrix. In fact, this is 
!TU: done automatically for PSMC with hard spheres.

module mc_moves

    use kinds_f90

    implicit none

        !> total number of atom moves
    integer ::    total_atm_moves

        !> no of successful atom moves
    integer ::    successful_atm_moves

        !> total number of molecule moves
    integer ::    total_mol_moves

        !> total number of molecule rotations
    integer ::    total_mol_rots

        !> number of successful molecule moves
    integer ::    successful_mol_moves

        !> number of successful molecule rotations
    integer ::    successful_mol_rots

        !> number of attempted swaps
    integer ::    attemptedswaps

        !> number of successful swaps
    integer ::    successfulswaps

        !> number of attempted atom swaps for each type of swap move
    integer, dimension(:,:), allocatable :: attemptedswaps_atm

        !> number of successful atom swaps for each type of swap move
    integer, dimension(:,:), allocatable :: successfulswaps_atm
    
        !> total number of abandoned (empty) attempts for atoms
    integer ::    empty_atm_moves

        !> total number of abandoned (empty) attempts for molecules
    integer ::    empty_mol_moves

        !> the no of types that can move
    integer ::    nmovetyp

        !> no of mol types that can move
    integer ::    nmolmovtyp

        !> no of mol types that can rotate
    integer ::    nmolrottyp

        !> stores types of moving atoms
    integer, dimension(:), allocatable :: movetyp

        !> list of flags for types of mol that move
    integer, dimension(:), allocatable :: molmovtyp

        !> list of flags for types of mol that rotate
    integer, dimension(:), allocatable :: molrottyp

        !> no of attempted molecule translations(moves)
    integer, allocatable ::    attempted_mol_moves(:)

        !> no of attempted molecule rotations
    integer, allocatable ::    attempted_mol_rots(:)

        !> no of attempted atom moves
    integer, dimension(:,:), allocatable ::    attempted_atm_moves

        !> atom type for swap
    integer, allocatable ::    swaptype1(:)

        !> atom type for swap
    integer, allocatable ::    swaptype2(:)

    integer, allocatable ::    swaplookup1(:,:)

    integer, allocatable ::    swaplookup2(:,:)

        !> real value between 0 and 1 used to determine the type of
        !> atom swap move used given a random number uniformly drawn
        !> from between 0 and 1
    real (kind=wp), dimension(:), allocatable :: atmswapfreq_thresh
    
        !> total number of times NL had to be reset
    integer, dimension(:), allocatable  ::    nbrlist_resets

        !> no of atom moves during a displacement update cycle
    real (kind=wp), dimension(:,:), allocatable ::    no_atm_moves

        !> max displacement of atoms allowed
    real (kind=wp), dimension(:,:), allocatable ::    distance_atm_max

    !TU-: These are not different variables for different boxes, but possibly should be
    
        !> no of molecule moves during a displacement update cycle
    real (kind=wp), dimension(:), allocatable ::    no_mol_moves

        !> max displacement of molecules allowed
    real (kind=wp), dimension(:), allocatable ::    distance_mol_max

        !> max rotation of molecules
    real (kind=wp), dimension(:), allocatable ::    rotate_mol_max

        !> no of molecule rotations during a displacement update cycle
    real (kind=wp), dimension(:), allocatable ::    no_mol_rots

contains

!write out max distances and rotations for restart
subroutine dump_distances(atmmove, molmove, molrot)

    use constants_module, only : urest
    !use comms_mpi_module, only : idnode

    implicit none

    logical, intent(in) :: atmmove, molmove, molrot

    !AB: in this case it is clearer to simply call this routine by master(s) only
    !AB: see dump_restart(iter, atmmove, molmove, molrot) in montecarlo_module.f90

    !if (idnode == 0) then

        if(atmmove) write(urest,*) distance_atm_max

        if(molmove) write(urest,*) distance_mol_max

        if(molrot)  write(urest,*) rotate_mol_max

    !endif

end subroutine

!get max displacements etc for restart
subroutine read_distances(atmmove, molmove, molrot)

    use constants_module, only : urest
    !use comms_mpi_module, only : idnode

    implicit none

    logical, intent(in) :: atmmove, molmove, molrot

    !if (idnode == 0) then

        if(atmmove) read(urest,*) distance_atm_max

        if(molmove) read(urest,*) distance_mol_max

        if(molrot)  read(urest,*) rotate_mol_max

    !endif

end subroutine

!*************************************************************************
! moves atoms using random choice
!************************************************************************/
!> moves atoms at random
!> Note that PSMC is assumed to be enabled if the optional argument 'ib_pas' is present. In this case
!> box 'ib_pas' is updated as the passive box while 'ib' is updated as the active box.
subroutine move_atoms(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrealcrct, emolrcp, emolvdwn, emolvdwb, emolthree, &
                      emolpair, emolang, emolfour, emolmany, emolext, ib_pas)

    use kinds_f90
    use species_module
    use control_type
    use cell_module
    use field_module
    use atom_module, only : store_atom_pos, restore_atom_pos
    !use comms_mpi_module, only : gsum
    use random_module, only : duni

    use constants_module, only : uout, FED_PAR_DIST2, FED_PAR_PSMC, FED_PAR_PSMC_HS, FED_TM, ATM_TYPE_SEMI
    use fed_calculus_module, only : fed, fed_tm_inc
    use fed_order_module, only : fed_upd_com_by, fed_param_dbias, fed_param_dbias_psmc, fed_upd_com_dist2, &
                                 fed_param_hist_inc, fed_window_reject

    use psmc_module, only : real2frac, frac2real, set_atom_u_from_pos

    implicit none

    integer, intent(in) :: ib
    type(control), intent(inout) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), &
                      energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                      energyfour(:), energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                      emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                      emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                      emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                      emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                      emolmany(nconfigs,number_of_molecules), emolrealcrct(nconfigs,number_of_molecules), &
                      emolext(nconfigs,number_of_molecules)

    !TU: Optional variable: the passive box to update analogously to the active box ib. If this argument
    !TU: is not present then there is no update to a passive box. If this argument is present then PSMC is assumed.
    integer, intent(in), optional :: ib_pas

    integer :: k, i, j, typ, atyp, niter, icom, im !, fail

    real(kind=wp) :: rmove(3), weight

    real(kind=wp) :: arg, distmax, deltav, deltavb, otot, ntot, oldreal, oldrcp, oldvdw, oldthree, oldpair, oldang, &
                     oldfour, oldmany, oldext, oldmfa, oldtotal, oldenth, oldvir, newreal, newrcp, newvdw, newthree,  &
                     newpair, newang, newfour, newmany, newext, newmfa, newtotal, newenth, newvir

     !temp vectors for molecular energies
    real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules), orcp(number_of_molecules), &
                       ovdwn(number_of_molecules), ovdwb(number_of_molecules), opair(number_of_molecules), &
                       othree(number_of_molecules),  oang(number_of_molecules), ofour(number_of_molecules), &
                       omany(number_of_molecules), ocrct(number_of_molecules), oext(number_of_molecules), &
                       nrealnonb(number_of_molecules), nrealbond(number_of_molecules), nrcp(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules), npair(number_of_molecules), &
                       nthree(number_of_molecules), nang(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules), ncrct(number_of_molecules), next(number_of_molecules)

    logical :: atom_found, do_ewald_rcp, atom_out, reset, is_fed_com, is_fed_param_psmc

    !TU: Relevant variables for the passive box for PSMC (variables which end in '_pas')
    integer :: typ_pas, atyp_pas

    real(kind=wp) :: rmove_pas(3)
    
    real(kind=wp) :: otot_pas, ntot_pas, oldreal_pas, oldrcp_pas, oldvdw_pas, oldthree_pas, oldpair_pas, oldang_pas, &
                     oldfour_pas, oldmany_pas, oldext_pas, oldmfa_pas, oldtotal_pas, oldenth_pas, oldvir_pas, &
                     newreal_pas, newrcp_pas, newvdw_pas, newthree_pas, newpair_pas, newang_pas, newfour_pas, &
                     newmany_pas, newext_pas, newmfa_pas, newtotal_pas, newenth_pas, newvir_pas

    real(kind = wp) :: orealnonb_pas(number_of_molecules), orealbond_pas(number_of_molecules), &
                       orcp_pas(number_of_molecules), ovdwn_pas(number_of_molecules), &
                       ovdwb_pas(number_of_molecules), opair_pas(number_of_molecules), &
                       othree_pas(number_of_molecules),  oang_pas(number_of_molecules), &
                       ofour_pas(number_of_molecules), omany_pas(number_of_molecules), &
                       ocrct_pas(number_of_molecules), oext_pas(number_of_molecules), &
                       nrealnonb_pas(number_of_molecules), nrealbond_pas(number_of_molecules), &
                       nrcp_pas(number_of_molecules), nvdwn_pas(number_of_molecules), &
                       nvdwb_pas(number_of_molecules), npair_pas(number_of_molecules), &
                       nthree_pas(number_of_molecules), nang_pas(number_of_molecules), &
                       nfour_pas(number_of_molecules), nmany_pas(number_of_molecules), &
                       ncrct_pas(number_of_molecules), next_pas(number_of_molecules)

    logical :: ib_pas_on, do_ewald_pas, atom_out_pas, reset_pas

    !AB: these help to reduce the number of repetitive checks
    do_ewald_rcp = .false.
    do_ewald_pas = .false.
    is_fed_com = .false.

    ib_pas_on = present(ib_pas)

    oldenth = 0.0_wp
    oldvir = 0.0_wp
    oldreal = 0.0_wp
!    oldrcp = 0.0_wp
    oldvdw = 0.0_wp
    oldthree = 0.0_wp
    oldpair = 0.0_wp
    oldang = 0.0_wp
    oldfour = 0.0_wp
    oldmany = 0.0_wp
    oldext = 0.0_wp
    oldmfa = 0.0_wp

    oldrcp = energyrcp(ib)
    newrcp = oldrcp

    newenth = 0.0_wp
    newvir = 0.0_wp
    newreal = 0.0_wp
!    newrcp = 0.0_wp
    newvdw = 0.0_wp
    newthree = 0.0_wp
    newpair = 0.0_wp
    newang = 0.0_wp
    newfour = 0.0_wp
    newmany = 0.0_wp
    newext = 0.0_wp
    newmfa = 0.0_wp

    nrcp = 0.0_wp
    orcp = 0.0_wp

    !fail  = 0
    niter = 0
    
    !AB: this is a new, much faster implementation of the random atom selection where
    !AB: atoms are chosen from only those molecular species that contain movable atoms;
    !AB: the list of atom moves is checked against all the molecular species to ensure 
    !AB: that all the movable atoms can be definitely found (see montecarlo_module::setupmc);
    !AB: yet, this is not a perfect solution, as some species may contain only a small subset 
    !AB: of movable atoms (out of the entire set of all their atoms)
    
    call select_species_atomov(ib, typ, k, j, i, movetyp, atom_found)

    if( .not.atom_found .or. typ < 1 .or. typ /= cfgs(ib)%mols(j)%atms(i)%atlabel ) then
#ifdef DEBUG
!debugging
        write(*,*)"move_atoms(): no movable atom found..."
#endif
        
        empty_atm_moves = empty_atm_moves+1
        return
    endif

    atyp = cfgs(ib)%mols(j)%atms(i)%atype

    is_fed_param_psmc = ( fed%par_kind == FED_PAR_PSMC .or. fed%par_kind == FED_PAR_PSMC_HS )

    do_ewald_rcp = ( job%coultype(ib) == 1 .and. cfgs(ib)%mols(j)%atms(i)%is_charged )

    distmax = distance_atm_max(typ,ib)  !distance moved for this type

    total_atm_moves = total_atm_moves + 1
    attempted_atm_moves(typ,ib) = attempted_atm_moves(typ,ib) + 1

    !store positions
    call store_atom_pos(cfgs(ib)%mols(j)%atms(i))

    !get initial energy
    if (atyp /= ATM_TYPE_SEMI) then

        if (job%uselist) then

            call atom_energy(ib, j, i, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                             oldreal, oldvdw, oldthree, oldpair, oldang, oldfour, oldext, oldmfa, oldvir,  &
                             orealnonb, orealbond, ocrct, ovdwn, ovdwb, opair, othree, oang, ofour, oext,  &
                             .true., .false.)

            if (atyp == ATM_TYPE_METAL) call atom_metal_energy_full(ib, job%shortrangecut, oldmany, omany)

        else

            call atom_energy_nolist(ib, j, i, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut,  &
                             oldreal, oldvdw, oldthree, oldpair, oldang, oldfour, oldext, oldmfa, oldvir,  &
                             orealnonb, orealbond, ocrct, ovdwn, ovdwb, opair, othree, oang, ofour, oext, &
                             .true., .false.)

        endif

    else

        call atom_tersoff_energy(ib, j, i, job%shortrangecut, oldmany, oldvir)
        omany(j) = oldmany

    endif

    !AB: check for any overlaps missed in the past
    if( oldvdw > vdwcap ) then 
        write(uout,*)"move_atoms(): stuck due to VdW overlap(s)!..", &
                     oldvdw," > ",vdwcap," (",j,", ",i,") - need to increase VDW ecap?"
        flush(uout)
        
        !goto 1000 ! reject - new energy is too high
    !end if

    !AB: check for any overlaps missed in the past
    else if( oldext > vdwcap ) then 
        write(uout,*)"move_atoms(): stuck due to EXT overlap(s)!..", &
                     oldext," > ",vdwcap," (",j,", ",i,") - need to increase VDW ecap?"
        flush(uout)
        
        !goto 1000 ! reject - new energy is too high
    end if

    !TU: Do the same as above for the passive box in PSMC
    if( ib_pas_on ) then

        typ_pas  = cfgs(ib_pas)%mols(j)%atms(i)%atlabel
        atyp_pas = cfgs(ib_pas)%mols(j)%atms(i)%atype

        do_ewald_pas = ( job%coultype(ib_pas) == 1 .and. cfgs(ib_pas)%mols(j)%atms(i)%is_charged )

        oldenth_pas = 0.0_wp
        oldvir_pas = 0.0_wp
        oldreal_pas = 0.0_wp
        !oldrcp_pas = 0.0_wp 
        oldvdw_pas = 0.0_wp
        oldthree_pas = 0.0_wp
        oldpair_pas = 0.0_wp
        oldang_pas = 0.0_wp
        oldfour_pas = 0.0_wp
        oldmany_pas = 0.0_wp
        oldext_pas = 0.0_wp
        oldmfa_pas = 0.0_wp

        oldrcp_pas = energyrcp(ib_pas)
        newrcp_pas = oldrcp_pas

        newenth_pas = 0.0_wp
        newvir_pas = 0.0_wp
        newreal_pas = 0.0_wp
        !newrcp_pas = 0.0_wp
        newvdw_pas = 0.0_wp
        newthree_pas = 0.0_wp
        newpair_pas = 0.0_wp
        newang_pas = 0.0_wp
        newfour_pas = 0.0_wp
        newmany_pas = 0.0_wp
        newext_pas = 0.0_wp
        newmfa_pas = 0.0_wp

        nrcp_pas = 0.0_wp
        orcp_pas = 0.0_wp

        call store_atom_pos(cfgs(ib_pas)%mols(j)%atms(i))

        if (atyp_pas /= ATM_TYPE_SEMI) then
           
            if (job%uselist) then
 
                call atom_energy(ib_pas, j, i, job%coultype(ib_pas), job%dielec, job%vdw_cut, job%shortrangecut, &
                             job%verletshell, oldreal_pas, oldvdw_pas, oldthree_pas, oldpair_pas, oldang_pas, &
                             oldfour_pas, oldext_pas, oldmfa_pas, oldvir_pas, orealnonb_pas, orealbond_pas, &
                             ocrct_pas, ovdwn_pas, ovdwb_pas, opair_pas, othree_pas, oang_pas, &
                             ofour_pas, oext_pas, .true., .false.)

                if (atyp == ATM_TYPE_METAL) call atom_metal_energy_full(ib_pas, job%shortrangecut, oldmany_pas, omany_pas)

             else

                 call atom_energy_nolist(ib_pas, j, i, job%coultype(ib_pas), job%dielec, job%vdw_cut, &
                             job%shortrangecut, oldreal_pas, oldvdw_pas, oldthree_pas, oldpair_pas, oldang_pas, &
                             oldfour_pas, oldext_pas, oldmfa_pas, oldvir_pas, orealnonb_pas, orealbond_pas, &
                             ocrct_pas, ovdwn_pas, ovdwb_pas, opair_pas, othree_pas, oang_pas, ofour_pas, &
                             oext_pas, .true., .false.)

              end if
              
        else
          
              call atom_tersoff_energy(ib_pas, j, i, job%shortrangecut, oldmany_pas, oldvir_pas)
              omany_pas(j) = oldmany_pas

        end if

    endif

    atom_out = .false.
    atom_out_pas = .false.

    !move atom
    !call move_atom(ib, j, i, distmax, atom_out)

    ! displacement
    rmove(1) = (2.0_wp * duni() - 1.0_wp) * distmax * job % atmmove(1,k) 
    rmove(2) = (2.0_wp * duni() - 1.0_wp) * distmax * job % atmmove(2,k) 
    rmove(3) = (2.0_wp * duni() - 1.0_wp) * distmax * job % atmmove(3,k) 

    call move_atom_by(ib, j, i, rmove, atom_out)

    !TU: Do the same as above for the passive box in PSMC
    if( ib_pas_on ) then

        !TU: Calculate the move to make on the atom in the passive box 
        !TU: - use the equivalent change in fractional coordinates in the passive box
        rmove_pas = rmove
        call real2frac( cfgs(ib)%vec, rmove_pas(1), rmove_pas(2), rmove_pas(3) )
        call frac2real( cfgs(ib_pas)%vec, rmove_pas(1), rmove_pas(2), rmove_pas(3) )

        call move_atom_by(ib_pas, j, i, rmove_pas, atom_out_pas)

    end if

    !AB: advance randmon number selection to preserve the random sequence
    arg = duni()

    deltavb = 0.0_wp

    weight = cfgs(ib)%mols(j)%atms(i)%mass

    !AB: prepare for FED sampling - before any possible rejection!

    icom = cfgs(ib)%mols(j)%atms(i)%idcom

    !AB: activate FED if needed (checking fed%par_kind is sufficient)
    is_fed_com = ( fed%par_kind == FED_PAR_DIST2 .and. icom > 0 )

    !AB: rearrange this for other FED parameters, not related to COM:s
    !is_fed_com = ( is_fed_com .and. icom > 0 )

    !AB: make sure nb-lists are not updated unduely upon early rejections
    reset = .false.
    reset_pas = .false.

    !JG:  The changes made to the Ewald summation should not impact on FED or slit calculations
    ! Til now, the only modifications are to cossum and sinsum
    ! These are only copied to cphsum, sphsum in the event of a successful move

    if( is_fed_com ) then

        !AB: update the relevant COM for FED parameter
        call fed_upd_com_by(ib, icom, rmove, weight)

        deltavb = fed_param_dbias(ib,cfgs(ib))

        !AB: stay within the defined range
        if( deltavb > job%toler ) goto 1000

        !if( deltavb > job%toler ) then 
        !    write(uout,*)"move_atoms(): rejected getting out of the range...", &
        !                 deltavb," > ",job%toler
        !    goto 1000 ! reject - new energy is too high
        !end if

    end if

    !if in slit, make sure atom is still within the cell, otherwise reject the move!
    if( atom_out .or. atom_out_pas ) goto 1000 ! reject - atom got outside the slit

    !check NL if in auto-update mode
    if(job%lauto_nbrs) then

        call check_verlet(ib, j, i, job%verletshell, reset)

        if( reset ) then
            nbrlist_resets(ib) = nbrlist_resets(ib)+1
            call atom_nbrlist(ib, j, i, job%shortrangecut, job%verletshell)
        endif

        !TU: Do the same as above for the passive box in PSMC
        if( ib_pas_on ) then

           call check_verlet(ib_pas, j, i, job%verletshell, reset_pas)

           if(reset_pas) call atom_nbrlist(ib_pas, j, i, job%shortrangecut, job%verletshell)

        end if

    endif

    !calculate manybody terms if this is a metal
    !if(atyp == ATM_TYPE_METAL) call atom_metal_energy(ib, j, i, job%shortrangecut, oldmany, newmany, omany, nmany)

    !get new energy
    if (atyp /= ATM_TYPE_SEMI) then

        if (job%uselist) then

            call atom_energy(ib, j, i, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                             newreal, newvdw, newthree, newpair, newang, newfour, newext, newmfa, newvir,  &
                             nrealnonb, nrealbond, ncrct, nvdwn, nvdwb, npair, nthree, nang, nfour, next,  &
                             .true., .false.)

            if (atyp == ATM_TYPE_METAL) call atom_metal_energy_full(ib, job%shortrangecut, newmany, nmany)

        else

            call atom_energy_nolist(ib, j, i, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, &
                             newreal, newvdw, newthree, newpair, newang, newfour, newext, newmfa, newvir,  &
                             nrealnonb, nrealbond, ncrct, nvdwn, nvdwb, npair, nthree, nang, nfour, next,  &
                             .true., .false.)

        endif

        if( newvdw > vdwcap ) goto 1000 ! reject - new energy is too high
        if( newext > vdwcap ) goto 1000 ! reject - new energy is too high
        !if( newvdw + newext > vdwcap ) then 
        !    write(uout,*)"move_atoms(): rejected due to VdW overlap...", &
        !                 newvdw," > ",vdwcap
        !    goto 1000 ! reject - new energy is too high
        !end if

    else

        call atom_tersoff_energy(ib, j, i, job%shortrangecut, newmany, newvir)

        if( newmany > job%toler ) goto 1000 ! reject - new energy is too high

        nmany(j) = newmany

    endif

    !if( newvdw > vdwcap ) goto 1000 !then 
    !    write(uout,*)"move_atoms(): rejected due to VdW overlap...", &
    !                 newvdw," > ",vdwcap
    !    goto 1000 ! reject - new energy is too high
    !end if

    if( do_ewald_rcp ) call move_atom_recip(ib, j, i, oldrcp, newrcp, orcp, nrcp, job%lmoldata)

    oldtotal = oldreal + oldrcp + oldvdw + oldthree + oldpair + oldang + oldmany + oldfour + oldext + oldmfa 
    oldenth  = oldtotal + extpress * cfgs(ib)%vec%volume

    newtotal = newreal + newrcp + newvdw + newthree + newpair + newang + newmany + newfour + newext + newmfa
    newenth  = newtotal + extpress * cfgs(ib)%vec%volume

    !TU: Do the same as above for the passive box in PSMC
    if( ib_pas_on ) then

        if (atyp_pas /= ATM_TYPE_SEMI) then

            if (job%uselist) then

                call atom_energy(ib_pas, j, i, job%coultype(ib_pas), job%dielec, job%vdw_cut, job%shortrangecut, &
                          job%verletshell, newreal_pas, newvdw_pas, newthree_pas, newpair_pas, newang_pas, &
                          newfour_pas, newext_pas, newmfa_pas, newvir_pas, nrealnonb_pas, nrealbond_pas, ncrct_pas, &
                          nvdwn_pas, nvdwb_pas, npair_pas, nthree_pas, nang_pas, nfour_pas, next_pas,  &
                          .true., .false.)

                if (atyp == ATM_TYPE_METAL) call atom_metal_energy_full(ib_pas, job%shortrangecut, newmany_pas, nmany_pas)

            else

                call atom_energy_nolist(ib_pas, j, i, job%coultype(ib_pas), job%dielec, job%vdw_cut, job%shortrangecut, &
                          newreal_pas, newvdw_pas, newthree_pas, newpair_pas, newang_pas, newfour_pas, &
                          newext_pas, newmfa_pas, newvir_pas, nrealnonb_pas, nrealbond_pas, ncrct_pas, nvdwn_pas, nvdwb_pas, &
                          npair_pas, nthree_pas, nang_pas, nfour_pas, next_pas,  &
                          .true., .false.)

            endif

            if( newvdw_pas > vdwcap ) goto 1000 ! reject - new energy is too high
            if( newext_pas > vdwcap ) goto 1000 ! reject - new energy is too high
            !if( newvdw + newext > vdwcap ) then 
            !    write(uout,*)"move_atoms(): rejected due to VdW overlap...", &
            !                 newvdw," > ",vdwcap
            !    goto 1000 ! reject - new energy is too high
            !end if

        else

            call atom_tersoff_energy(ib_pas, j, i, job%shortrangecut, newmany_pas, newvir_pas)
            nmany(j) = newmany

        endif
        
        if( do_ewald_pas ) &
            call move_atom_recip(ib_pas, j, i, oldrcp_pas, newrcp_pas, orcp_pas, nrcp_pas, job%lmoldata)

        oldtotal_pas = oldreal_pas + oldrcp_pas + oldvdw_pas + oldthree_pas + oldpair_pas + oldang_pas + &
                       oldmany_pas + oldfour_pas + oldext_pas + oldmfa_pas
        oldenth_pas  = oldtotal_pas + extpress * cfgs(ib_pas)%vec%volume

        newtotal_pas = newreal_pas + newrcp_pas + newvdw_pas + newthree_pas + newpair_pas + newang_pas + &
                       newmany_pas + newfour_pas + newext_pas + newmfa_pas

        newenth_pas = newtotal_pas + extpress * cfgs(ib_pas)%vec%volume

    end if

    !TU: If PSMC order parameter is in use then accordingly bias the acceptance probability
    if( is_fed_param_psmc ) then

        !TU: deltavb should be 0 at this point if we are using the PSMC order parameter. 
        !TU: The difference in the bias between the new state and the current state is given by
        !TU: fed_param_dbias_psmc(energy1, energy2), where energy1 and energy2 are the NEW 
        !TU: energies of phases 1 and 2.
        !TU: NB: oldtotal, oldtotal_pas, newtotal, and newtotal_pas are the energies of the ATOM
        !TU: which has just been moved, not the energy of the system. However energy1 and energy2
        !TU: should be TOTAL.
        if( ib == 1) then

            deltavb = fed_param_dbias_psmc( energytot(1) + newtotal - oldtotal, &
                                            energytot(2) + newtotal_pas - oldtotal_pas )

        else

            !TU: WARNING - I assume that ib==2 if ib/=1. Something has gone horribly
            !TU: wrong if this is not the case at this point, hence I don't even check.

            deltavb = fed_param_dbias_psmc(energytot(1) + newtotal_pas - oldtotal_pas, &
                                           energytot(2) + newtotal - oldtotal )

        end if

        !AB: stay within the defined range
        if( deltavb > job%toler ) goto 1000

    end if

    !TU: In general we need code here to set 'deltavb' for other order parameters, i.e. not COM or PSMC.
    !TU: I understand this is unnecessary in this procedure for, e.g. volume as the order parameter, since
    !TU: the volume is unchanged by a translation move, and hence deltavb=0. But 'in general', here
    !TU: is where 'deltavb' should be set.

    !metropoolis
    deltav  = newtotal - oldtotal
    deltavb = deltavb + deltav * beta

    !TU: Update transition matrix if it is in use: the canonical (i.e, without biasing) probability is
    !TU: min(1,exp(-deltav*beta)).
    !TU: Note that this is only reached if the trial state is 'in bounds' for the order parameter range,
    !TU: and hence the transition matrix is only updated for in bounds trial states.
    if( fed % method == FED_TM .and. ( is_fed_com .or. is_fed_param_psmc ) ) then
        
        call fed_tm_inc(  min( 1.0_wp, exp(-deltav*beta) )  )

    end if

    !TU: Reject the move if it leaves the window or takes us fuether from the window
    if( fed % is_window .and. ( is_fed_com .or. is_fed_param_psmc ) ) then

        if( fed_window_reject() ) goto 1000

    end if
    
    if( deltavb > job%toler ) goto 1000 ! reject - new energy is too high
    if( deltavb < 0.0_wp .or. arg < exp(-deltavb) ) then

        !AB: update the molecule COM
        !call update_mol_com_by(ib, j, rmove)

        cfgs(ib)%mols(j)%rcom(:) = cfgs(ib)%mols(j)%rcom(:) + rmove(:) * weight / cfgs(ib)%mols(j)%mass

        if( is_fed_com ) then
            !AB: update the COM difference for FED parameter
            call fed_upd_com_dist2(ib)
           
            !AB: bias is also updated if needed (e.g. WL)
            call fed_param_hist_inc(ib,.true.)
           
        else if( is_fed_param_psmc ) then 
            !TU: update the FED histogram & bias if needed (e.g. WL) for LSMC
           
            call fed_param_hist_inc(ib,.true.)

        end if

        !make sure image convention is still ok (done globally before every MC move)
        !call pbc_atom(ib, j, i)

!debug<
!        if( abs(oldrcp - energyrcp(ib)) > 1.e-8 ) &
!            write(*,*)"SOS: oldrcp - energyrcp(ib) = ",oldrcp - energyrcp(ib), &
!                      " & newrcp = ",newrcp," while moving atom ",typ
!debug>
        !accept update energies etc
        energytot(ib) = energytot(ib) + deltav
        enthalpytot(ib) = enthalpytot(ib) + (newenth - oldenth)
        virialtot(ib) = virialtot(ib) + (newvir - oldvir)
        energyreal(ib) = energyreal(ib) + (newreal - oldreal)
        energyvdw(ib) = energyvdw(ib) + (newvdw - oldvdw)
        energypair(ib) = energypair(ib) + (newpair - oldpair)
        energythree(ib) = energythree(ib) + (newthree - oldthree)
        energyang(ib) = energyang(ib) + (newang - oldang)
        energyfour(ib) = energyfour(ib) + (newfour - oldfour)
        energymany(ib) = energymany(ib) + (newmany - oldmany)
        energyext(ib) = energyext(ib) + (newext - oldext)
        energymfa(ib) = energymfa(ib) + (newmfa - oldmfa)

        no_atm_moves(typ,ib) = no_atm_moves(typ,ib) + 1
        successful_atm_moves = successful_atm_moves + 1

        if( do_ewald_rcp ) then
            energyrcp(ib) = newrcp
            call update_rcpsums(ib)
        end if

        !TU: Do the same as above for the passive box in PSMC (for energies only - not counters)
        if( ib_pas_on ) then
           
            energytot(ib_pas) = energytot(ib_pas) + (newtotal_pas - oldtotal_pas)
            enthalpytot(ib_pas) = enthalpytot(ib_pas) + (newenth_pas - oldenth_pas)
            virialtot(ib_pas) = virialtot(ib_pas) + (newvir_pas - oldvir_pas)
            !energyrcp(ib_pas) = energyrcp(ib_pas) + (newrcp_pas - oldrcp_pas)
            energyreal(ib_pas) = energyreal(ib_pas) + (newreal_pas - oldreal_pas)
            energyvdw(ib_pas) = energyvdw(ib_pas) + (newvdw_pas - oldvdw_pas)
            energypair(ib_pas) = energypair(ib_pas) + (newpair_pas - oldpair_pas)
            energythree(ib_pas) = energythree(ib_pas) + (newthree_pas - oldthree_pas)
            energyang(ib_pas) = energyang(ib_pas) + (newang_pas - oldang_pas)
            energyfour(ib_pas) = energyfour(ib_pas) + (newfour_pas - oldfour_pas)
            energymany(ib_pas) = energymany(ib_pas) + (newmany_pas - oldmany_pas)
            energyext(ib_pas) = energyext(ib_pas) + (newext_pas - oldext_pas)
            energymfa(ib_pas) = energymfa(ib_pas) + (newmfa_pas - oldmfa_pas)

            if( do_ewald_pas ) then
                energyrcp(ib_pas) = newrcp_pas
                call update_rcpsums(ib_pas)
            endif

        end if

        if(job%lmoldata) then

            do im = 1, number_of_molecules

                emolrealnonb(ib,im) = emolrealnonb(ib,im) + (nrealnonb(im) - orealnonb(im))
                emolrealbond(ib,im) = emolrealbond(ib,im) + (nrealbond(im) - orealbond(im))
                emolrealcrct(ib,im) = emolrealcrct(ib,im) + (ncrct(im) - ocrct(im))
                emolrcp(ib,im) = emolrcp(ib,im) + (nrcp(im) - orcp(im))
                emolvdwn(ib,im) = emolvdwn(ib,im) + (nvdwn(im) - ovdwn(im))
                emolvdwb(ib,im) = emolvdwb(ib,im) + (nvdwb(im) - ovdwb(im))
                emolpair(ib,im) = emolpair(ib,im) + (npair(im) - opair(im))
                emolthree(ib,im) = emolthree(ib,im) + (nthree(im) - othree(im))
                emolang(ib,im) = emolang(ib,im) + (nang(im) - oang(im))
                emolfour(ib,im) = emolfour(ib,im) + (nfour(im) - ofour(im))
                emolmany(ib,im) = emolmany(ib,im) + (nmany(im) - omany(im))
                emolext(ib,im) = emolext(ib,im) + (next(im) - oext(im))

                otot = orealnonb(im) + orealbond(im) + orcp(im) + ovdwn(im) + ovdwb(im) + opair(im) &
                     + othree(im) + oang(im) + ofour(im) + omany(im) + ocrct(im) + oext(im)
                ntot = nrealnonb(im) + nrealbond(im) + nrcp(im) + nvdwn(im) + nvdwb(im) + npair(im) &
                     + nthree(im) + nang(im) + nfour(im) + nmany(im) + ncrct(im) + next(im)

                emoltot(ib,im) = emoltot(ib,im) + (ntot - otot)

                !TU: Do the same as above for the passive box in PSMC
                if( ib_pas_on ) then
                   
                    emolrealnonb(ib_pas,im) = emolrealnonb(ib_pas,im) + (nrealnonb_pas(im) - orealnonb_pas(im))
                    emolrealbond(ib_pas,im) = emolrealbond(ib_pas,im) + (nrealbond_pas(im) - orealbond_pas(im))
                    emolrealcrct(ib_pas,im) = emolrealcrct(ib_pas,im) + (ncrct_pas(im) - ocrct_pas(im))
                    emolrcp(ib_pas,im) = emolrcp(ib_pas,im) + (nrcp_pas(im) - orcp_pas(im))
                    emolvdwn(ib_pas,im) = emolvdwn(ib_pas,im) + (nvdwn_pas(im) - ovdwn_pas(im))
                    emolvdwb(ib_pas,im) = emolvdwb(ib_pas,im) + (nvdwb_pas(im) - ovdwb_pas(im))
                    emolpair(ib_pas,im) = emolpair(ib_pas,im) + (npair_pas(im) - opair_pas(im))
                    emolthree(ib_pas,im) = emolthree(ib_pas,im) + (nthree_pas(im) - othree_pas(im))
                    emolang(ib_pas,im) = emolang(ib_pas,im) + (nang_pas(im) - oang_pas(im))
                    emolfour(ib_pas,im) = emolfour(ib_pas,im) + (nfour_pas(im) - ofour_pas(im))
                    emolmany(ib_pas,im) = emolmany(ib_pas,im) + (nmany_pas(im) - omany_pas(im))
                    emolext(ib_pas,im) = emolext(ib_pas,im) + (next_pas(im) - oext_pas(im))
                    
                    otot_pas = orealnonb_pas(im) + orealbond_pas(im) + orcp_pas(im) + ovdwn_pas(im) + ovdwb_pas(im) + &
                         opair_pas(im) + othree_pas(im) + oang_pas(im) + ofour_pas(im) + omany_pas(im) + &
                         ocrct_pas(im) + oext_pas(im)
                    ntot_pas = nrealnonb_pas(im) + nrealbond_pas(im) + nrcp_pas(im) + nvdwn_pas(im) + nvdwb_pas(im) + &
                         npair_pas(im) + nthree_pas(im) + nang_pas(im) + nfour_pas(im) + nmany_pas(im) + &
                         ncrct_pas(im) + next_pas(im)
                    
                    emoltot(ib_pas,im) = emoltot(ib_pas,im) + (ntot_pas - otot_pas)

                end if

            enddo

        endif

        !update the complete nbrlist
        !if(job%lauto_nbrs .and. reset) then
        if( reset ) then !AB: can be set '.true.' above only if job%lauto_nbrs is '.true.'

            !call setnbrlists(ib, job%shortrangecut, job%verletshell,  job%linklist, .false.)
            !call update_verlet_cell(ib, job%verletshell, job%shortrangecut, reset)
            !call set_rzero(ib)

            !AB: much quicker way of updating NL
            call update_atm_verlet_cell(ib, job%verletshell, job%shortrangecut, j, i)

        endif

        !TU: Update PSMC displacements for both boxes
        !TU: (note that the lattice site positions are unchanged during an atom move).
        if( ib_pas_on ) then

            !TU: Do the same as above for the passive box in PSMC
            if( reset_pas ) then !AB: can be set '.true.' above only if job%lauto_nbrs is '.true.'

                !call setnbrlists(ib_pas, job%shortrangecut, job%verletshell,  job%linklist, .false.)
                !call update_verlet_cell(ib_pas, job%verletshell, job%shortrangecut, reset_pas)
                !call set_rzero(ib_pas)

                !AB: much quicker way of updating NL
                call update_atm_verlet_cell(ib_pas, job%verletshell, job%shortrangecut, j, i)

            end if

            call set_atom_u_from_pos(ib, j, i)
            call set_atom_u_from_pos(ib_pas, j, i)

        end if

        return

    endif
    !else

1000    call restore_atom_pos(cfgs(ib)%mols(j)%atms(i))  ! rejected - put atom back

        !write(uout,*)"move_atoms(): rejected ... ", &
        !             newtotal," - ",oldtotal," = ",deltav,&
        !             " *k_B = ",deltavb," >? ",vdwcap,", (",job%toler,")", reset

        !if(job%lauto_nbrs .and. reset) then
        !AB: can be set '.true.' above only if job%lauto_nbrs is '.true.'

        if( reset ) call atom_nbrlist(ib, j, i, job%shortrangecut, job%verletshell)

        !TU: Do the same as above for the passive box in PSMC
        if( ib_pas_on ) then

            call restore_atom_pos(cfgs(ib_pas)%mols(j)%atms(i))

            if( reset_pas ) call atom_nbrlist(ib_pas, j, i, job%shortrangecut, job%verletshell)

        endif

        !AB: revert the attempted displacement in COM
        if( is_fed_com ) then
        
            call fed_upd_com_by(ib, icom, -rmove, weight)
        
            !AB: bias is also updated if needed (e.g. WL)
            call fed_param_hist_inc(ib,.false.)
        
        else if( is_fed_param_psmc ) then
            !TU: update the FED histogram & bias if needed (e.g. WL)
            !TU: but revert the parameter state (value & id/index)
        
            call fed_param_hist_inc(ib,.false.)

        end if

    !endif

end subroutine

!*************************************************************************
! moves atoms sequentially 
!************************************************************************/
subroutine move_atoms_seq(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrealcrct, emolrcp, emolvdwn, emolvdwb, emolthree, &
                      emolpair, emolang, emolfour, emolmany, emolext)

    use kinds_f90
    use species_module
    use control_type
    use cell_module
    use field_module
    use atom_module, only : store_atom_pos, restore_atom_pos
    use random_module, only : duni
    !use comms_mpi_module, only : gsum

    use constants_module, only : uout, FED_PAR_DIST2, FED_TM !, FED_PAR_DIST1
    use fed_calculus_module, only : fed, fed_tm_inc
    use fed_order_module, only : fed_upd_com_by, fed_param_dbias, fed_upd_com_dist2, fed_param_hist_inc, fed_window_reject

    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), energyfour(:), &
                      energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                      energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                      emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                      emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                      emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                      emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                      emolmany(nconfigs,number_of_molecules), emolrealcrct(nconfigs,number_of_molecules), &
                      emolext(nconfigs,number_of_molecules)

    integer :: il, jl, i, j, k, typ, atyp, icom, im !, fail

    real(kind=wp) :: rmove(3), weight

    real(kind = wp) :: arg, distmax, deltav, deltavb, otot, ntot, oldreal, oldrcp, oldvdw, oldthree, &
                       oldpair, oldang, oldfour, oldmany, oldext, oldmfa, oldtotal, oldenth, oldvir, &
                       newreal, newrcp, newvdw, newthree, newpair, newang, newfour, newmany, newext, &
                       newmfa, newtotal, newenth, newvir

    !temp vectors for molecular energies
    real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules), orcp(number_of_molecules), &
                       ovdwn(number_of_molecules), ovdwb(number_of_molecules), opair(number_of_molecules), &
                       othree(number_of_molecules),  oang(number_of_molecules), ofour(number_of_molecules), &
                       omany(number_of_molecules), ocrct(number_of_molecules), oext(number_of_molecules), &
                       nrealnonb(number_of_molecules), nrealbond(number_of_molecules), nrcp(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules), npair(number_of_molecules), &
                       nthree(number_of_molecules), nang(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules), ncrct(number_of_molecules), next(number_of_molecules)

    logical :: atom_found, do_ewald_rcp, atom_out, reset, is_fed_com

    !AB: reduce the number of repetitive checks
    do_ewald_rcp = .false.

    !AB: activate FED if needed (checking fed%par_kind is sufficient)
    is_fed_com = ( fed%par_kind == FED_PAR_DIST2 )

    !fail = 0

    do jl = 1, cfgs(ib)%num_mols

        i_loop: &
        do il = 1, cfgs(ib)%mols(jl)%natom

            j = jl
            i = il

            typ = cfgs(ib)%mols(j)%atms(i)%atlabel

            atom_found = .false.
            if( job%useseqmovernd ) then
            !AB: this is a new, much faster implementation of the random atom selection where
            !AB: atoms are chosen from only those molecular species that contain movable atoms;
            !AB: the list of atom moves is checked against all the molecular species to ensure 
            !AB: that all the movable atoms can be definitely found (see montecarlo_module::setupmc);
            !AB: yet, this is not a perfect solution, as some species may contain only a small subset 
            !AB: of movable atoms (out of the entire set of all their atoms)

                call select_species_atomov(ib, typ, k, j, i, movetyp, atom_found)

!debugging
                if( .not.atom_found ) then !.or. typ < 1 .or. typ /= cfgs(ib)%mols(j)%atms(i)%atlabel ) then
#ifdef DEBUG
                    write(*,*)"move_atoms() *** SOS *** no movable atom found..."
#endif
                    empty_atm_moves = empty_atm_moves+1
                    cycle
                endif

            else

                !AB: make sure the atom is amongst the movable ones
                do k = 1, job%numatmmove
                    atom_found = ( typ == movetyp(k) )
                    if( atom_found ) exit
                    !debugging
                    !then
                    !    if( i /= k ) &
                    !        write(*,*)"select_species_atomov(): moving atom ",i,&
                    !                  " found under index ",k," in the atom-move list..."
                    !    return
                    !endif 
                enddo

                if( .not.atom_found ) then 

                    !write(*,*)"move_atoms() *** SOS *** atom ",typ," not movable..."
                
                    if( typ < 1 .or. typ > number_of_elements ) &
                        write(*,*)"move_atoms() *** SOS *** (non-movable) atom label ",typ," is out of range..."

                    empty_atm_moves = empty_atm_moves+1
                    cycle
                endif

            end if

            !if( typ /= movetyp(k) ) cycle

            atyp = cfgs(ib)%mols(j)%atms(i)%atype

            do_ewald_rcp = ( job%coultype(ib) == 1 .and. cfgs(ib)%mols(j)%atms(i)%is_charged )

            oldenth = 0.0_wp
            oldvir = 0.0_wp
            oldreal = 0.0_wp
            !oldrcp = 0.0_wp
            oldvdw = 0.0_wp
            oldthree = 0.0_wp
            oldpair = 0.0_wp
            oldang = 0.0_wp
            oldfour = 0.0_wp
            oldmany = 0.0_wp
            oldext = 0.0_wp
            oldmfa = 0.0_wp

            oldrcp = energyrcp(ib)
            newrcp = oldrcp

            newenth = 0.0_wp
            newvir = 0.0_wp
            newreal = 0.0_wp
            !newrcp = 0.0_wp
            newvdw = 0.0_wp
            newthree = 0.0_wp
            newpair = 0.0_wp
            newang = 0.0_wp
            newfour = 0.0_wp
            newmany = 0.0_wp
            newext = 0.0_wp
            newmfa = 0.0_wp

            omany = 0.0_wp
            oext = 0.0_wp
            nmany = 0.0_wp
            next = 0.0_wp
            orcp = 0.0_wp
            nrcp = 0.0_wp

            distmax = distance_atm_max(typ,ib)  !distance moved for this type

            total_atm_moves = total_atm_moves + 1
            attempted_atm_moves(typ,ib) = attempted_atm_moves(typ,ib) + 1

            !store positions 
            call store_atom_pos(cfgs(ib)%mols(j)%atms(i))

            !get initial energy
            if (atyp /= ATM_TYPE_SEMI) then

                if(job%uselist) then

                    call atom_energy(ib, j, i, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                         oldreal, oldvdw, oldthree, oldpair, oldang, oldfour, oldext, oldmfa, oldvir,  &
                         orealnonb, orealbond, ocrct, ovdwn, ovdwb, opair, othree, oang, ofour, oext, &
                         .true., .false.)

                    if (atyp == ATM_TYPE_METAL) call atom_metal_energy_full(ib, job%shortrangecut, oldmany, omany)
                
                else

                    call atom_energy_nolist(ib, j, i, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, &
                         oldreal, oldvdw, oldthree, oldpair, oldang, oldfour, oldext, oldmfa, oldvir,  &
                         orealnonb, orealbond, ocrct, ovdwn, ovdwb, opair, othree, oang, ofour, oext, &
                         .true., .false.)

                endif
                 
            else

                call atom_tersoff_energy(ib, j, i, job%shortrangecut, oldmany, oldvir)
                omany(j) = oldmany

            endif

            !AB: check for any overlaps missed in the past
            if( oldvdw > vdwcap ) then 
                write(uout,*)"move_atoms_seq(): stuck due to VdW overlap(s)!..", &
                             oldvdw," > ",vdwcap," (",j,", ",i,") - need to increase VDW ecap?"
                flush(uout)
                
                !goto 1000 ! reject - new energy is too high
            !end if

            !AB: check for any overlaps missed in the past
            else if( oldext > vdwcap ) then 
                write(uout,*)"move_atoms_seq(): stuck due to EXT overlap(s)!..", &
                             oldext," > ",vdwcap," (",j,", ",i,") - need to increase VDW ecap?"
                flush(uout)
                
                !goto 1000 ! reject - new energy is too high
            end if

            atom_out = .false.

            !move atom
            rmove(1) = (2.0_wp * duni() - 1.0_wp) * distmax * job % atmmove(1,k)
            rmove(2) = (2.0_wp * duni() - 1.0_wp) * distmax * job % atmmove(2,k)
            rmove(3) = (2.0_wp * duni() - 1.0_wp) * distmax * job % atmmove(3,k)

            call move_atom_by(ib, j, i, rmove, atom_out)

            !AB: advance randmon number selection to preserve the random sequence
            arg = duni()

            deltavb = 0.0_wp

            weight = cfgs(ib)%mols(j)%atms(i)%mass

            !AB: prepare for FED sampling - before any possible rejection!

            icom = cfgs(ib)%mols(j)%atms(i)%idcom

            !AB: rearrange this for other FED parameters, not related to COM:s
            !is_fed_com = ( is_fed_com .and. icom > 0 )
            is_fed_com = ( fed%par_kind == FED_PAR_DIST2 .and. icom > 0 )

            !AB: make sure nb-lists are not updated unduely upon early rejections
            reset = .false.

            if( is_fed_com ) then

                !AB: update the relevant COM for FED parameter
                call fed_upd_com_by(ib, icom, rmove, weight)

                deltavb = fed_param_dbias(ib,cfgs(ib))

                !AB: stay within the defined range
                if( deltavb > job%toler ) goto 1000

            end if

            !if in slit, make sure atom is still within the cell, otherwise reject the move!
            if( atom_out ) goto 1000 ! reject - atom got outside the slit

            !calculate manybody terms if this is a metal
            !if(atyp == ATM_TYPE_METAL) call atom_metal_energy(ib, j, i, job%shortrangecut, oldmany, newmany, omany, nmany)

            !new distances
            !if(job%lauto_nbrs .and. job%uselist) then
            if(job%lauto_nbrs) then

                call check_verlet(ib, j, i, job%verletshell, reset)

                if( reset ) then
                    nbrlist_resets(ib) = nbrlist_resets(ib)+1
                    call atom_nbrlist(ib, j, i, job%shortrangecut, job%verletshell)
                endif
  
            endif

            !get new energy
            if (atyp /= ATM_TYPE_SEMI) then

                if(job%uselist) then 

                    call atom_energy(ib, j, i, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                         newreal, newvdw, newthree, newpair, newang, newfour, newext, newmfa, newvir,  &
                         nrealnonb, nrealbond, ncrct, nvdwn, nvdwb, npair, nthree, nang, nfour, next, &
                         .true., .false.)

                    if (atyp == ATM_TYPE_METAL) call atom_metal_energy_full(ib, job%shortrangecut, newmany, nmany)

                else

                    call atom_energy_nolist(ib, j, i, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, &
                         newreal, newvdw, newthree, newpair, newang, newfour, newext, newmfa, newvir,  &
                         nrealnonb, nrealbond, ncrct, nvdwn, nvdwb, npair, nthree, nang, nfour, next, &
                         .true., .false.)

                endif

                if( newvdw > vdwcap ) goto 1000 ! reject - new energy is too high
                if( newext > vdwcap ) goto 1000 ! reject - new energy is too high
                !if( newvdw + newext > vdwcap ) then 
                !    write(uout,*)"move_atoms_seq(): rejected due to VdW overlap..."
                !    goto 1000 ! reject - new energy is too high
                !end if

            else

                call atom_tersoff_energy(ib, j, i, job%shortrangecut, newmany, newvir)

                if( newmany > job%toler ) goto 1000 ! reject - new energy is too high

                nmany(j) = newmany

            endif
            
            if( do_ewald_rcp ) call move_atom_recip(ib, j, i, oldrcp, newrcp, orcp, nrcp, job%lmoldata)

            !now the recip space terms have been calculated
            !sum the old energies
            oldtotal = oldreal + oldrcp + oldvdw + oldthree + oldpair + oldang + oldmany + oldfour + oldext + oldmfa
            oldenth  = oldtotal + extpress * cfgs(ib)%vec%volume

            newtotal = newreal + newrcp + newvdw + newthree + newpair + newang + newmany + newfour + newext + newmfa
            newenth  = newtotal + extpress * cfgs(ib)%vec%volume

            !TU: In general we need code here to set 'deltavb' for other order parameters, i.e. not COM.
            !TU: I understand this is unnecessary in this procedure for, e.g. volume as the order parameter, since
            !TU: the volume is unchanged by a translation move, and hence deltavb=0. But 'in general', here
            !TU: is where 'deltavb' should be set.

            !metropoolis
            deltav = (newtotal - oldtotal)

            deltavb = deltavb + deltav * beta

            !TU: Update transition matrix if it is in use: the canonical (i.e, without biasing) probability is
            !TU: min(1,exp(-deltav*beta)).
            !TU: Note that this is only reached if the trial state is 'in bounds' for the order parameter range,
            !TU: and hence the transition matrix is only updated for in bounds trial states.
            if( fed % method == FED_TM .and. is_fed_com  ) then
               
               call fed_tm_inc(  min( 1.0_wp, exp(-deltav*beta) )  )
               
            end if

            !TU: Reject the move if it leaves the window or takes us fuether from the window                          
            if( fed % is_window .and. is_fed_com ) then
               
               if( fed_window_reject() ) goto 1000
               
            end if

            if( deltavb > job%toler ) goto 1000 ! reject - new energy is too high
            if( deltavb < 0.0_wp .or. arg < exp(-deltavb) ) then

                !AB: update the molecule COM

                cfgs(ib)%mols(j)%rcom(:) = cfgs(ib)%mols(j)%rcom(:) + rmove(:) * weight / cfgs(ib)%mols(j)%mass

                !rmove(:) = rmove(:) * cfgs(ib)%mols(j)%atms(i)%mass / cfgs(ib)%mols(j)%mass
                !call update_mol_com_by(ib, j, rmove)

                !AB: update the COM difference for FED parameter
                if( is_fed_com ) then 
                
                    call fed_upd_com_dist2(ib)
                
                    !AB: bias is also updated if needed (e.g. WL)
                    call fed_param_hist_inc(ib,.true.)

                end if

                !make sure image convention is still ok (done globally before every MC move)
                !call pbc_atom(ib, j, i)

                !accept update energies etc
                energytot(ib) = energytot(ib) + deltav
                enthalpytot(ib) = enthalpytot(ib) + (newenth - oldenth)
                virialtot(ib) = virialtot(ib) + (newvir - oldvir)
                energyreal(ib) = energyreal(ib) + (newreal - oldreal)
                energyvdw(ib) = energyvdw(ib) + (newvdw - oldvdw)
                energypair(ib) = energypair(ib) + (newpair - oldpair)
                energythree(ib) = energythree(ib) + (newthree - oldthree)
                energyang(ib) = energyang(ib) + (newang - oldang)
                energyfour(ib) = energyfour(ib) + (newfour - oldfour)
                energymany(ib) = energymany(ib) + (newmany - oldmany)
                energyext(ib) = energyext(ib) + (newext - oldext)
                energymfa(ib) = energymfa(ib) + (newmfa - oldmfa)

                no_atm_moves(typ,ib) = no_atm_moves(typ,ib) + 1
                successful_atm_moves = successful_atm_moves + 1

                if( do_ewald_rcp ) then
                    energyrcp(ib) = newrcp
                    call update_rcpsums(ib)
                endif

                if(atyp == ATM_TYPE_METAL) call copy_atom_density(ib, j, i)

                if(job%lmoldata) then

                    do im = 1, number_of_molecules

                        emolrealnonb(ib,im) = emolrealnonb(ib,im) + (nrealnonb(im) - orealnonb(im))
                        emolrealbond(ib,im) = emolrealbond(ib,im) + (nrealbond(im) - orealbond(im))
                        emolrealcrct(ib,im) = emolrealcrct(ib,im) + (ncrct(im) - ocrct(im))
                        emolrcp(ib,im) = emolrcp(ib,im) + (nrcp(im) - orcp(im))
                        emolvdwn(ib,im) = emolvdwn(ib,im) + (nvdwn(im) - ovdwn(im))
                        emolvdwb(ib,im) = emolvdwb(ib,im) + (nvdwb(im) - ovdwb(im))
                        emolpair(ib,im) = emolpair(ib,im) + (npair(im) - opair(im))
                        emolthree(ib,im) = emolthree(ib,im) + (nthree(im) - othree(im))
                        emolang(ib,im) = emolang(ib,im) + (nang(im) - oang(im))
                        emolfour(ib,im) = emolfour(ib,im) + (nfour(im) - ofour(im))
                        emolmany(ib,im) = emolmany(ib,im) + (nmany(im) - omany(im))
                        emolext(ib,im) = emolext(ib,im) + (next(im) - oext(im))

                        otot = orealnonb(im) + orealbond(im) + orcp(im) + ovdwn(im) + ovdwb(im) + opair(im) &
                             + othree(im) + oang(im) + ofour(im) + omany(im) + ocrct(im) + oext(im)
                        ntot = nrealnonb(im) + nrealbond(im) + nrcp(im) + nvdwn(im) + nvdwb(im) + npair(im) &
                             + nthree(im) + nang(im) + nfour(im) + nmany(im) + ncrct(im) + next(im)

                        emoltot(ib,im) = emoltot(ib,im) + (ntot - otot)

                    enddo

                endif

                !must recalc whol nbrlist as other atoms will be out of date on accepting this move
                !if( job%uselist .and. job%lauto_nbrs .and. reset ) then
                if( reset ) then

                    !call setnbrlists(ib, job%shortrangecut, job%verletshell,  job%linklist, .false.)
                    !call update_verlet_cell(ib, job%verletshell, job%shortrangecut, reset)
                    !call set_rzero(ib)

                    !AB: much quicker way of updating NL
                    call update_atm_verlet_cell(ib, job%verletshell, job%shortrangecut, j, i)

                endif

                cycle

            endif
            !else

1000        call restore_atom_pos(cfgs(ib)%mols(j)%atms(i))  ! rejected-put atom back!!

!debugging
!                write(uout,*)"move_atoms_seq(): rejected ... ", &
!                      newtotal," - ",oldtotal," = ",deltav,&
!                      " *k_B = ",deltavb," >? ",vdwcap,", (",job%toler,")"

            !AB: revert the attempted displacement in COM
            if( is_fed_com ) then 
                
                call fed_upd_com_by(ib, icom, -rmove, weight)
                
                !AB: bias is also updated if needed (e.g. WL)
                call fed_param_hist_inc(ib,.false.)

            end if

            !atom move has failed so recalculate the nbr list for an atom - if on atomatic
            !if(job%uselist .and. job%lauto_nbrs .and. reset) then
            if( reset ) call atom_nbrlist(ib, j, i, job%shortrangecut, job%verletshell)

            !endif

        enddo i_loop

!debugging
!        !AB: make sure ALL processes aware of the local error and call error(..) -> MPI_FINALIZE(..)
!        call gsum(fail)
!        if( fail > 0 ) call error(302)

    enddo 

end subroutine

!> does a rigid molecule move
!> Note that PSMC is assumed to be enabled if the optional argument 'ib_pas' is present. In this case
!> box 'ib_pas' is updated as the passive box while 'ib' is updated as the active box.
subroutine move_molecules(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                      emolpair, emolang, emolfour, emolmany, emolext, ib_pas)

    use kinds_f90
    use species_module
    use control_type
    use cell_module
    use field_module
    use random_module, only : duni

    use constants_module, only : uout, FED_PAR_DIST2, FED_PAR_PSMC, FED_PAR_PSMC_HS, FED_TM !, FED_PAR_DIST1
    use fed_calculus_module, only : fed, fed_tm_inc
    use fed_order_module, only : fed_upd_com_by, fed_param_dbias, fed_param_dbias_psmc, fed_upd_com_dist2, &
                                 fed_param_hist_inc, fed_window_reject

    use psmc_module, only : real2frac, frac2real, set_mol_u_from_pos

    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), &
                      energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                      energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                      emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                      emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                      emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                      emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                      emolmany(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules)

    !TU: Optional variable: the passive box to update analogously to the active box ib. If this argument
    !TU: is not present then there is no update to a passive box. If this argument is present then PSMC is assumed.
    integer, intent(in), optional :: ib_pas

    integer :: k, i, typ, im, ia, icom1, icom2, ngas, niter

    real(kind=wp) :: rmove(3), weight(2)

    real(kind=wp) :: arg, distmax, deltav, deltavb, otot, ntot, oldreal, oldrcp, oldvdw, oldthree, &
                     oldmany, oldext, oldmfa, oldtotal, oldenth, oldvir, newreal, newrcp, newvdw, newthree, &
                     newmany, newext, newmfa, newtotal, newenth, newvir

     !temp vectors for molecular energies
    real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules), orcp(number_of_molecules), &
                       ovdwn(number_of_molecules), ovdwb(number_of_molecules), &
                       othree(number_of_molecules),  ofour(number_of_molecules), &
                       omany(number_of_molecules), oext(number_of_molecules), &
                       nrealnonb(number_of_molecules), nrealbond(number_of_molecules), nrcp(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules),  &
                       nthree(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules), next(number_of_molecules)

    logical :: mol_found, reset, do_ewald_rcp, atom_out, is_fed_com, is_fed_param_psmc

    !TU: Relevant variables for the passive box for PSMC (variables which end in '_pas')
    integer :: typ_pas

    real(kind=wp) :: rmove_pas(3)

    real(kind=wp) :: otot_pas, ntot_pas, oldreal_pas, oldrcp_pas, oldvdw_pas, oldthree_pas, oldpair_pas, oldang_pas, &
                     oldfour_pas, oldmany_pas, oldext_pas, oldmfa_pas, oldtotal_pas, oldenth_pas, oldvir_pas, &
                     newreal_pas, newrcp_pas, newvdw_pas, newthree_pas, newpair_pas, newang_pas, newfour_pas, &
                     newmany_pas, newext_pas, newmfa_pas, newtotal_pas, newenth_pas, newvir_pas

    real(kind = wp) :: orealnonb_pas(number_of_molecules), orealbond_pas(number_of_molecules), &
                       orcp_pas(number_of_molecules), ovdwn_pas(number_of_molecules), &
                       ovdwb_pas(number_of_molecules), opair_pas(number_of_molecules), &
                       othree_pas(number_of_molecules),  oang_pas(number_of_molecules), &
                       ofour_pas(number_of_molecules), omany_pas(number_of_molecules), &
                       ocrct_pas(number_of_molecules), oext_pas(number_of_molecules), &
                       nrealnonb_pas(number_of_molecules), nrealbond_pas(number_of_molecules), &
                       nrcp_pas(number_of_molecules), nvdwn_pas(number_of_molecules), &
                       nvdwb_pas(number_of_molecules), npair_pas(number_of_molecules), &
                       nthree_pas(number_of_molecules), nang_pas(number_of_molecules), &
                       nfour_pas(number_of_molecules), nmany_pas(number_of_molecules), &
                       ncrct_pas(number_of_molecules), next_pas(number_of_molecules)

    logical :: ib_pas_on, reset_pas, do_ewald_pas, atom_out_pas

    !AB: this is a new, much faster implementation of the random molecule selection where
    !AB: molecules are chosen from only those molecular species that contain movable molecules;
    !AB: the list of molecule moves is checked against all the molecular species to ensure 
    !AB: that all the movable molecules can be definitely found (see montecarlo_module::setupmc);

    call select_species_molmov(ib, typ, k, im, molmovtyp, mol_found)

    if( .not.mol_found ) then

#ifdef DEBUG
!debugging
        if( cfgs(ib)%mtypes(typ)%num_mols < 1  ) then
            write(*,*)"move_molecules(): movable molecule type ",typ," is empty..."
        elseif (typ /= cfgs(ib)%mols(im)%mol_label .or. typ < 1 ) then
            write(*,*)"move_molecules(): no movable molecule found..."
        endif
#endif

        empty_mol_moves = empty_mol_moves+1

        return
    endif

#ifdef DEBUG
!debugging
    if( im < 1 .or. typ < 1 .or. k < 1 .or. im > cfgs(ib)%num_mols .or. im > cfgs(ib)%mxmol ) then
        write(*,*)"move_molecules(*SOS*): found movable molecule ",typ,&
                  " with index ",im," (move type: ",k,")"
        write(*,*)"move_molecules(*SOS*): in mol. species  containing ",cfgs(ib)%mtypes(typ)%num_mols," mols out of ",&
                  cfgs(ib)%mtypes(typ)%max_mols," (max)"
        write(*,*)"move_molecules(*SOS*): in configuration containing ",cfgs(ib)%num_mols," mols out of ",cfgs(ib)%mxmol," (max)"
        STOP
        return
    endif
#endif

    reset = .false.
    reset_pas = .false.

    !AB: reduce the number of repetitive checks
    do_ewald_rcp = .false.
    do_ewald_pas = .false.

    ib_pas_on = present(ib_pas)

    oldenth = 0.0_wp
    oldvir = 0.0_wp
    oldreal = 0.0_wp
    !oldrcp = 0.0_wp
    oldvdw = 0.0_wp
    oldthree = 0.0_wp
    oldmany = 0.0_wp
    oldext = 0.0_wp
    oldmfa = 0.0_wp

    oldrcp = energyrcp(ib)
    newrcp = oldrcp

    newenth = 0.0_wp
    newvir = 0.0_wp
    newreal = 0.0_wp
    !newrcp = 0.0_wp
    newvdw = 0.0_wp
    newthree = 0.0_wp
    newmany = 0.0_wp
    newext = 0.0_wp
    newmfa = 0.0_wp

    is_fed_param_psmc = ( fed%par_kind == FED_PAR_PSMC .or. fed%par_kind == FED_PAR_PSMC_HS )

    do_ewald_rcp = ( job%coultype(ib) == 1 .and. cfgs(ib)%mols(im)%has_charge )

    call store_molecule_pos(ib,im)

    distmax = distance_mol_max(typ)   !distance moved for this type
    total_mol_moves = total_mol_moves + 1
    attempted_mol_moves(typ) = attempted_mol_moves(typ) + 1

    !get initial energy
    if(job%uselist) then

        call molecule_energy(ib, im, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                     oldreal, oldvdw, oldthree, oldmany, oldext, oldmfa, oldvir,  &
                     orealnonb, orealbond, ovdwn, ovdwb, othree, ofour, omany, oext)

    else

        call molecule_energy_nolist(ib, im, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, &
                     oldreal, oldvdw, oldthree, oldmany, oldext, oldmfa, oldvir,  &
                     orealnonb, orealbond, ovdwn, ovdwb, othree, ofour, omany, oext)

    endif

    !AB: check for any overlaps missed in the past
    if( oldvdw > vdwcap ) then 
        write(uout,*)"move_molecules(): stuck due to VdW overlap(s)!..", &
                     oldvdw," > ",vdwcap," (",im,") - need to increase VDW ecap?" !,", ",i,")"
        flush(uout)
        
        !goto 1000 ! reject - new energy is too high
    !end if

    !AB: check for any overlaps missed in the past
    else if( oldext > vdwcap ) then 
        write(uout,*)"move_molecules(): stuck due to EXT overlap(s)!..", &
                     oldext," > ",vdwcap," (",im,") - need to increase VDW ecap?" !,", ",i,")"
        flush(uout)
        
        !goto 1000 ! reject - new energy is too high
    end if

    !TU: Do the same as above for the passive box in PSMC

    if( ib_pas_on ) then

        typ_pas = cfgs(ib_pas)%mols(im)%mol_label

        do_ewald_pas = ( job%coultype(ib_pas) == 1 .and. cfgs(ib_pas)%mols(im)%has_charge )

        oldenth_pas = 0.0_wp
        oldvir_pas = 0.0_wp
        oldreal_pas = 0.0_wp
        !oldrcp_pas = energyrcp(ib_pas)
        oldvdw_pas = 0.0_wp
        oldthree_pas = 0.0_wp
        oldpair_pas = 0.0_wp
        oldang_pas = 0.0_wp
        oldfour_pas = 0.0_wp
        oldmany_pas = 0.0_wp
        oldext_pas = 0.0_wp
        oldmfa_pas = 0.0_wp

        oldrcp_pas = energyrcp(ib_pas)
        newrcp_pas = oldrcp_pas

        newenth_pas = 0.0_wp
        newvir_pas = 0.0_wp
        newreal_pas = 0.0_wp
        !newrcp_pas = 0.0_wp
        newvdw_pas = 0.0_wp
        newthree_pas = 0.0_wp
        newpair_pas = 0.0_wp
        newang_pas = 0.0_wp
        newfour_pas = 0.0_wp
        newmany_pas = 0.0_wp
        newext_pas = 0.0_wp
        newmfa_pas = 0.0_wp

        nrcp_pas = 0.0_wp
        orcp_pas = 0.0_wp

        call store_molecule_pos(ib_pas,im)
                                                                                             
        if(job%uselist) then

            call molecule_energy(ib_pas, im, job%coultype(ib_pas), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                         oldreal_pas, oldvdw_pas, oldthree_pas, oldmany_pas, oldext_pas, oldmfa_pas, oldvir_pas,  &
                         orealnonb_pas, orealbond_pas, ovdwn_pas, ovdwb_pas, othree_pas, ofour_pas, omany_pas, oext_pas)

        else

            call molecule_energy_nolist(ib_pas, im, job%coultype(ib_pas), job%dielec, job%vdw_cut, job%shortrangecut, &
                         oldreal_pas, oldvdw_pas, oldthree_pas, oldmany_pas, oldext_pas, oldmfa_pas, oldvir_pas,  &
                         orealnonb_pas, orealbond_pas, ovdwn_pas, ovdwb_pas, othree_pas, ofour_pas, omany_pas, oext_pas)

        endif

    end if

    atom_out = .false.
    atom_out_pas = .false.

    ! displacement
    rmove(1) = (2.0_wp * duni() - 1.0_wp) * distmax * job % molmove(1,k) !molmovtyp(typ))
    rmove(2) = (2.0_wp * duni() - 1.0_wp) * distmax * job % molmove(2,k) !molmovtyp(typ))
    rmove(3) = (2.0_wp * duni() - 1.0_wp) * distmax * job % molmove(3,k) !molmovtyp(typ))

    call move_molecule_by(ib, im, rmove, atom_out)

    !if( do_ewald_rcp ) call move_molecule_recip(ib, im, newrcp, nrcp, job%lmoldata)

    !TU: Do the same as above for the passive box in PSMC
    if( ib_pas_on ) then

        !TU: Calculate the move to make on the molecule in the passive box
        !TU: - use the equivalent change in fractional coordinates in the passive box
        rmove_pas = rmove
        call real2frac( cfgs(ib)%vec, rmove_pas(1), rmove_pas(2), rmove_pas(3) )
        call frac2real( cfgs(ib_pas)%vec, rmove_pas(1), rmove_pas(2), rmove_pas(3) )

        call move_molecule_by(ib_pas, im, rmove_pas, atom_out_pas)

        !if( do_ewald_pas ) &
        !    call move_molecule_recip(ib_pas, im, newrcp_pas, nrcp_pas, job%lmoldata)

    end if

    !AB: advance randmon number selection to preserve the random sequence
    arg = duni()

    deltavb = 0.0_wp

    !AB: prepare for FED sampling - before any possible rejection!

    icom1 = cfgs(ib)%mols(im)%idcom

    !AB: activate FED if needed (checking fed%par_kind is sufficient)
    is_fed_com = ( fed%par_kind == FED_PAR_DIST2 .and. icom1 /= 0 )

    !AB: rearrange this for other FED parameters, not related to COM:s
    !is_fed_com = ( is_fed_com .and. icom1 /= 0 )

    weight(:) = 0.0_wp

    if( is_fed_com ) then

        if( icom1 > 0 ) then
            !AB: the case of whole molecule belonging to same COM

            weight(icom1) = cfgs(ib)%mols(im)%mass

        else
            !AB: the case of atom subset(s) belonging to same or different COM:s

            do ia = 1,cfgs(ib)%mols(im)%natom

               icom2 = cfgs(ib)%mols(im)%atms(ia)%idcom

               if( icom2 > 0 ) then

                   weight(icom2) = weight(icom2) + cfgs(ib)%mols(im)%atms(ia)%mass

               end if

            end do

            icom1 = -icom1

        endif

        !AB: update the relevant COM for FED parameter
        !call fed_upd_com_by(ib, icom, rmove, weight)
        if( weight(1) > 0.0_wp ) call fed_upd_com_by(ib, 1, rmove, weight(1))
        if( weight(2) > 0.0_wp ) call fed_upd_com_by(ib, 2, rmove, weight(2))

        deltavb = fed_param_dbias(ib,cfgs(ib))

        !AB: stay within the defined range
        if( deltavb > job%toler ) goto 1000

    end if

    !if in slit, make sure all atoms are still within the cell, otherwise reject the move!
    !TU: Note that atom_out_pas will be .false. if PSMC is not in use
    if( atom_out .or. atom_out_pas ) goto 1000 ! reject - atom got outside the slit

    !AB: use NL or not
    if( job%uselist ) then

        !check NL if in auto-update mode
        if( job%lauto_nbrs ) then

            !AB: much quicker way of updating NL
            call update_verlet_mol(ib, im, job%verletshell, job%shortrangecut, reset)
            if( reset ) nbrlist_resets(ib) = nbrlist_resets(ib)+1

            !call check_verlet_mol(ib, im, job%verletshell, reset)
            !if( reset ) then 
            !    nbrlist_resets(ib) = nbrlist_resets(ib)+1
            !    call set_mol_nbrlist(ib, im, job%shortrangecut, job%verletshell)
            !end if

        endif

        call molecule_energy(ib, im, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                     newreal, newvdw, newthree, newmany, newext, newmfa, newvir,  &
                     nrealnonb, nrealbond, nvdwn, nvdwb, nthree, nfour, nmany, next)

    else

        call molecule_energy_nolist(ib, im, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, &
                     newreal, newvdw, newthree, newmany, newext, newmfa, newvir,  &
                     nrealnonb, nrealbond, nvdwn, nvdwb, nthree, nfour, nmany, next)

    endif

    !TU: Do the same for the passive box in PSMC
    if( ib_pas_on ) then

        if( job%uselist ) then

            !TU: Do the same as above for the passive box in PSMC
            if( job%lauto_nbrs ) then

                !AB: much quicker way of updating NL
                call update_verlet_mol(ib_pas, im, job%verletshell, job%shortrangecut, reset_pas)
                if( reset ) nbrlist_resets(ib_pas) = nbrlist_resets(ib_pas)+1

                !call check_verlet_mol(ib_pas, im, job%verletshell, reset_pas)
                !if( reset_pas ) call set_mol_nbrlist(ib_pas, im, job%shortrangecut, job%verletshell)

            end if

            call molecule_energy(ib_pas, im, job%coultype(ib_pas), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                         newreal_pas, newvdw_pas, newthree_pas, newmany_pas, newext_pas, newmfa_pas, newvir_pas,  &
                         nrealnonb_pas, nrealbond_pas, nvdwn_pas, nvdwb_pas, nthree_pas, nfour_pas, nmany_pas, next_pas)

        else

            call molecule_energy_nolist(ib_pas, im, job%coultype(ib_pas), job%dielec, job%vdw_cut, job%shortrangecut, &
                         newreal_pas, newvdw_pas, newthree_pas, newmany_pas, newext_pas, newmfa_pas, newvir_pas,  &
                         nrealnonb_pas, nrealbond_pas, nvdwn_pas, nvdwb_pas, nthree_pas, nfour_pas, nmany_pas, next_pas)

        endif

    end if

    if( newvdw > vdwcap ) goto 1000 ! reject - new energy is too high
    if( newext > vdwcap ) goto 1000 ! reject - new energy is too high
    !if( newvdw + newext > vdwcap ) then 
    !    write(uout,*)"move_molecules(): rejected due to VdW overlap..."
    !    goto 1000 ! reject - new energy is too high
    !end if
    
    if( do_ewald_rcp ) call move_molecule_recip(ib, im, newrcp, nrcp)!, job%lmoldata)

    !sum the energies
    oldtotal = oldreal + oldrcp + oldvdw + oldthree + oldmany + oldext + oldmfa
    oldenth  = oldtotal + extpress * cfgs(ib)%vec%volume

    newtotal = newreal + newrcp + newvdw + newthree + newmany + newext + newmfa
    newenth  = newtotal + extpress * cfgs(ib)%vec%volume

    !TU: Do the same for the passive box in PSMC
    if( ib_pas_on ) then

        if( do_ewald_pas ) &
            call move_molecule_recip(ib_pas, im, newrcp_pas, nrcp_pas)!, job%lmoldata)

        oldtotal_pas = oldreal_pas + oldrcp_pas + oldvdw_pas + oldthree_pas + oldmany_pas + oldext_pas + oldmfa_pas
        oldenth_pas  = oldtotal_pas + extpress * cfgs(ib_pas)%vec%volume

        newtotal_pas = newreal_pas + newrcp_pas + newvdw_pas + newthree_pas + newmany_pas + newext_pas + newmfa_pas
        newenth_pas  = newtotal_pas + extpress * cfgs(ib_pas)%vec%volume

    end if

    !TU: If PSMC order parameter is in use then accordingly bias the acceptance probability
    if( is_fed_param_psmc ) then

        !TU: deltavb should be 0 at this point if we are using the PSMC order parameter.
        !TU: The difference in the bias between the new state and the current state is given by
        !TU: fed_param_dbias_psmc(energy1, energy2), where energy1 and energy2 are the NEW
        !TU: energies of phases 1 and 2.

        !TU: NB: oldtotal, oldtotal_pas, newtotal, and newtotal_pas are the energies of the ATOM
        !TU: which has just been moved, not the energy of the system. However energy1 and energy2
        !TU: should be TOTAL.
        if( ib == 1) then

            deltavb = fed_param_dbias_psmc( energytot(1) + newtotal - oldtotal, &
                                            energytot(2) + newtotal_pas - oldtotal_pas )

        else

            !TU: WARNING - I assume that ib==2 if ib/=1. Something has gone horribly
            !TU: wrong if this is not the case at this point, hence I don't even check.

            deltavb = fed_param_dbias_psmc(energytot(1) + newtotal_pas - oldtotal_pas, &
                                           energytot(2) + newtotal - oldtotal )

        end if

        !TU: stay within the defined range
        if( deltavb > job%toler ) goto 1000

    end if

    !TU: In general we need code here to set 'deltavb' for other order parameters, i.e. not COM or PSMC.
    !TU: I understand this is unnecessary in this procedure for, e.g. volume as the order parameter, since
    !TU: the volume is unchanged by a translation move, and hence deltavb=0. But 'in general', here
    !TU: is where 'deltavb' should be set.

    !metropoolis
    deltav  = (newtotal - oldtotal)
    deltavb = deltavb + deltav * beta

    !TU: Update transition matrix if it is in use: the canonical (i.e, without biasing) probability is
    !TU: min(1,exp(-deltav*beta)).
    !TU: Note that this is only reached if the trial state is 'in bounds' for the order parameter range,
    !TU: and hence the transition matrix is only updated for in bounds trial states.
    if( fed % method == FED_TM .and. ( is_fed_com .or. is_fed_param_psmc ) ) then
        
        call fed_tm_inc(  min( 1.0_wp, exp(-deltav*beta) )  )

    end if

    !TU: Reject the move if it leaves the window or takes us further from the window                          
    if( fed % is_window .and. ( is_fed_com .or. is_fed_param_psmc ) ) then
       
       if( fed_window_reject() ) goto 1000
       
    end if

    if( deltavb > job%toler ) goto 1000 ! reject - new energy is too high
    if( deltavb < 0.0_wp .or. arg < exp(-deltavb) ) then

        !AB: update the COM difference for FED parameter
        if( is_fed_com ) then 
        
            call fed_upd_com_dist2(ib)
        
            !AB: bias is also updated if needed (e.g. WL)
            call fed_param_hist_inc(ib,.true.)
        
        else if( is_fed_param_psmc ) then
        
            !TU: update the FED histogram & bias if needed (e.g. WL) for PSMC
        
            call fed_param_hist_inc(ib,.true.)

        end if

        !make sure image convention is still ok
        !call pbc_molecule(ib, im)

        !accept update energies etc
        energytot(ib) = energytot(ib) + deltav
        enthalpytot(ib) = enthalpytot(ib) + (newenth - oldenth)
        virialtot(ib) = virialtot(ib) + (newvir - oldvir)
        !energyrcp(ib) = newrcp
        energyreal(ib) = energyreal(ib) + (newreal - oldreal)
        energyvdw(ib) = energyvdw(ib) + (newvdw - oldvdw)
        energythree(ib) = energythree(ib) + (newthree - oldthree)
        energymany(ib) = energymany(ib) + (newmany - oldmany)
        energyext(ib) = energyext(ib) + (newext - oldext)
        energymfa(ib) = energymfa(ib) + (newmfa - oldmfa)

        no_mol_moves(typ) = no_mol_moves(typ) + 1
        successful_mol_moves = successful_mol_moves + 1

        !debugging
        !if( abs(energyrcp(ib)).lt. 1.0e-10_wp ) write(*,*)'move_molecules():: accepted with energyrcp(ib) = ',energyrcp(ib)

        if( do_ewald_rcp ) then
            energyrcp(ib) = newrcp
            call update_rcpsums(ib)
        endif

        !must recalc whole nbrlist as other atoms will be out of sync on accepting the move
        !if( job%uselist .and. job%lauto_nbrs .and. reset ) then
        if( reset ) then

            !TU: Above the neighbour list for the atoms in the moved molecule have been updated.
            !TU: Say atom 'i' is such an atom. We must still update the neighbour lists of all atoms
            !TU: which are within the cut-off of 'i', i.e. all atoms within the (newly updated)
            !TU: neighbour list of 'i'. The below procedure does this for all such atoms 'i'. If
            !TU: this is not done then 'i' will 'see' the atoms in its local environment correctly,
            !TU: but the atoms in the local environment may not see 'i'. 
            call update_mol_verlet_cell(ib, job%verletshell, job%shortrangecut, im)

        endif

        !TU: Do the same as above for the passive box in PSMC (for energies only - not counters),
        !TU: and update PSMC displacements for both boxes (note that the lattice site positions are unchanged during an atom move).
        if( ib_pas_on ) then

            energytot(ib_pas) = energytot(ib_pas) + (newtotal_pas - oldtotal_pas)
            enthalpytot(ib_pas) = enthalpytot(ib_pas) + (newenth_pas - oldenth_pas)
            virialtot(ib_pas) = virialtot(ib_pas) + (newvir_pas - oldvir_pas)
            !energyrcp(ib_pas) = energyrcp(ib_pas) + (newrcp_pas - oldrcp_pas)
            energyreal(ib_pas) = energyreal(ib_pas) + (newreal_pas - oldreal_pas)
            energyvdw(ib_pas) = energyvdw(ib_pas) + (newvdw_pas - oldvdw_pas)
            energythree(ib_pas) = energythree(ib_pas) + (newthree_pas - oldthree_pas)
            energymany(ib_pas) = energymany(ib_pas) + (newmany_pas - oldmany_pas)
            energyext(ib_pas) = energyext(ib_pas) + (newext_pas - oldext_pas)
            energymfa(ib_pas) = energymfa(ib_pas) + (newmfa_pas - oldmfa_pas)

            if( do_ewald_pas ) then
                energyrcp(ib_pas) = newrcp_pas
                call update_rcpsums(ib_pas)
            endif

            if( reset_pas ) then

                !TU: Above the neighbour list for the atoms in the moved molecule have been updated.
                !TU: Say atom 'i' is such an atom. We must still update the neighbour lists of all atoms
                !TU: which are within the cut-off of 'i', i.e. all atoms within the (newly updated)
                !TU: neighbour list of 'i'. The below procedure does this for all such atoms 'i'. If
                !TU: this is not done then 'i' will 'see' the atoms in its local environment correctly,
                !TU: but the atoms in the local environment may not see 'i'.
                call update_mol_verlet_cell(ib_pas, job%verletshell, job%shortrangecut, im)

            endif

            call set_mol_u_from_pos(ib, im)
            call set_mol_u_from_pos(ib_pas, im)

        end if

         if(job%lmoldata) then

             do im = 1, number_of_molecules

                 emolrealnonb(ib,im) = emolrealnonb(ib,im) + (nrealnonb(im) - orealnonb(im))
                 emolrealbond(ib,im) = emolrealbond(ib,im) + (nrealbond(im) - orealbond(im))
                 orcp(im) = emolrcp(ib,im)
                 emolrcp(ib,im) = nrcp(im)
                 emolvdwn(ib,im) = emolvdwn(ib,im) + (nvdwn(im) - ovdwn(im))
                 emolvdwb(ib,im) = emolvdwb(ib,im) + (nvdwb(im) - ovdwb(im))
                 emolthree(ib,im) = emolthree(ib,im) + (nthree(im) - othree(im))
                 emolfour(ib,im) = emolfour(ib,im) + (nfour(im) - ofour(im))
                 emolmany(ib,im) = emolmany(ib,im) + (nmany(im) - omany(im))
                 emolext(ib,im) = emolext(ib,im) + (next(im) - oext(im))

                 otot = orealnonb(im) + orealbond(im) + orcp(im) + ovdwn(im) + ovdwb(im)    &
                        + othree(im) + ofour(im) + omany(im) + oext(im)
                 ntot = nrealnonb(im) + nrealbond(im) + nrcp(im) + nvdwn(im) + nvdwb(im)    &
                        + nthree(im) + nfour(im) + nmany(im) + next(im)

                 emoltot(ib,im) = emoltot(ib,im) + (ntot - otot)

                 !TU: Do the same as above for the passive box in PSMC
                 if( ib_pas_on ) then

                     emolrealnonb(ib_pas,im) = emolrealnonb(ib_pas,im) + (nrealnonb_pas(im) - orealnonb_pas(im))
                     emolrealbond(ib_pas,im) = emolrealbond(ib_pas,im) + (nrealbond_pas(im) - orealbond_pas(im))
                     orcp_pas(im) = emolrcp(ib_pas,im)
                     emolrcp(ib_pas,im) = nrcp_pas(im)
                     emolvdwn(ib_pas,im) = emolvdwn(ib_pas,im) + (nvdwn_pas(im) - ovdwn_pas(im))
                     emolvdwb(ib_pas,im) = emolvdwb(ib_pas,im) + (nvdwb_pas(im) - ovdwb_pas(im))
                     emolthree(ib_pas,im) = emolthree(ib_pas,im) + (nthree_pas(im) - othree_pas(im))
                     emolfour(ib_pas,im) = emolfour(ib_pas,im) + (nfour_pas(im) - ofour_pas(im))
                     emolmany(ib_pas,im) = emolmany(ib_pas,im) + (nmany_pas(im) - omany_pas(im))
                     emolext(ib_pas,im) = emolext(ib_pas,im) + (next_pas(im) - oext_pas(im))

                     otot_pas = orealnonb_pas(im) + orealbond_pas(im) + orcp_pas(im) + ovdwn_pas(im) + ovdwb_pas(im)    &
                            + othree_pas(im) + ofour_pas(im) + omany_pas(im) + oext_pas(im)
                     ntot_pas = nrealnonb_pas(im) + nrealbond_pas(im) + nrcp_pas(im) + nvdwn_pas(im) + nvdwb_pas(im)    &
                            + nthree_pas(im) + nfour_pas(im) + nmany_pas(im) + next_pas(im)

                     emoltot(ib_pas,im) = emoltot(ib_pas,im) + (ntot_pas - otot_pas)
 
                 end if

             enddo

         endif

         return

    endif
    !else

1000    call restore_molecule_pos(ib, im)  ! rejected => put molecule back!!

        !AB: restore molecule NLs by resetting
        !if( reset ) call set_mol_nbrlist(ib, im , job%shortrangecut, job%verletshell)
        if( reset ) call update_verlet_mol(ib, im, job%verletshell, job%shortrangecut, reset)

        !TU: Do the same as above for the passive box in PSMC
        if( ib_pas_on ) then

            call restore_molecule_pos(ib_pas, im)

            if( reset_pas ) &
                call update_verlet_mol(ib_pas, im, job%verletshell, job%shortrangecut, reset_pas)
                !call set_mol_nbrlist(ib_pas, im , job%shortrangecut, job%verletshell)

        end if

        !AB: revert the attempted displacement in COM
        if( is_fed_com ) then 
        
            !call fed_upd_com_by(ib, icom, -rmove, weight)
            if( weight(1) > 0.0_wp ) call fed_upd_com_by(ib, 1, -rmove, weight(1))
            if( weight(2) > 0.0_wp ) call fed_upd_com_by(ib, 2, -rmove, weight(2))
        
            !AB: bias is also updated if needed (e.g. WL)
            call fed_param_hist_inc(ib,.false.)
        
        else if( is_fed_param_psmc ) then
        
            !TU: update the FED histogram & bias if needed (e.g. WL)
            !TU: but revert the parameter state (value & id/index)
        
            call fed_param_hist_inc(ib,.false.)

        end if

    !endif

end subroutine


!> does a sequential rigid molecule move
subroutine move_molecules_seq(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                      emolpair, emolang, emolfour, emolmany, emolext)

    use kinds_f90
    use species_module
    use control_type
    use cell_module
    use field_module
    use random_module, only : duni

    use constants_module, only : uout, FED_PAR_DIST2, FED_TM !, FED_PAR_DIST1
    use fed_calculus_module, only : fed, fed_tm_inc
    use fed_order_module, only : fed_upd_com_by, fed_param_dbias, fed_upd_com_dist2, fed_param_hist_inc, fed_window_reject

    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), &
                      energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                      energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                      emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                      emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                      emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                      emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                      emolmany(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules)

    integer :: k, i, typ, imol, iml, im, ia, icom1, icom2

    real(kind=wp) :: rmove(3), weight(2)

    real(kind=wp) :: arg, distmax, deltav, deltavb, otot, ntot, oldreal, oldrcp, oldvdw, oldthree,  &
                     oldmany, oldext, oldmfa, oldtotal, oldenth, oldvir, newreal, newrcp, newvdw, newthree, &
                     newmany, newext, newmfa, newtotal, newenth, newvir

     !temp vectors for molecular energies
    real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules), orcp(number_of_molecules), &
                       ovdwn(number_of_molecules), ovdwb(number_of_molecules),  &
                       othree(number_of_molecules), ofour(number_of_molecules), &
                       omany(number_of_molecules), oext(number_of_molecules), &
                       nrealnonb(number_of_molecules), nrealbond(number_of_molecules), nrcp(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules),  &
                       nthree(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules), next(number_of_molecules)

    logical :: mol_found, reset, do_ewald_rcp, atom_out, is_fed_com

    !AB: reduce the number of repetitive checks
    do_ewald_rcp = .false.

    !AB: activate FED if needed (checking fed%par_kind is sufficient)
    is_fed_com = ( fed%par_kind == FED_PAR_DIST2 )

    do iml = 1, cfgs(ib)%num_mols

        imol = iml

        typ = cfgs(ib)%mols(imol)%mol_label

        mol_found = .false.
        if( job%useseqmolmovernd ) then
        !AB: this is a new, much faster implementation of the random molecule selection where
        !AB: molecules are chosen from only those molecular species that contain movable molecules;
        !AB: the list of molecule moves is checked against all the molecular species to ensure 
        !AB: that all the movable molecules can be definitely found (see montecarlo_module::setupmc);

            call select_species_molmov(ib, typ, k, imol, molmovtyp, mol_found)

            !debugging
            if( .not.mol_found ) then

#ifdef DEBUG
                if( cfgs(ib)%mtypes(typ)%num_mols < 1  ) then
                    write(*,*)"move_molecules(): movable molecule type ",typ," is empty..."
                elseif (typ /= cfgs(ib)%mols(imol)%mol_label .or. typ < 1 ) then
                    write(*,*)"move_molecules(): no movable molecule found..."
                endif
#endif
                empty_mol_moves = empty_mol_moves+1

                cycle
            endif

#ifdef DEBUG
            !if( imol < 1 .or. typ < 1 .or. k < 1 ) then
            if( imol < 1 .or. typ < 1 .or. k < 1 .or. imol > cfgs(ib)%num_mols .or. imol > cfgs(ib)%mxmol ) then
                write(*,*)"move_molecules_seq(*SOS*): found 'movable' molecule ",typ,&
                          " with index ",imol," (move type: ",k,")"
                STOP
                return
            endif
#endif

        else

            !AB: make sure the molecule is amongst the movable ones
            do k = 1, job%nummolmove
                mol_found = ( typ == molmovtyp(k) )
                if( mol_found ) exit
            enddo

            !debugging
            if( .not.mol_found ) then

#ifdef DEBUG
                if( cfgs(ib)%mtypes(typ)%num_mols < 1  ) then
                    write(*,*)"move_molecules(): movable molecule type ",typ," is empty..."
                elseif (typ /= cfgs(ib)%mols(imol)%mol_label .or. typ < 1 ) then
                    write(*,*)"move_molecules(): no movable molecule found..."
                endif
#endif

                empty_mol_moves = empty_mol_moves+1

                cycle
            endif

#ifdef DEBUG
            if( imol < 1 .or. typ < 1 .or. k < 1 ) then
                write(*,*)"move_molecules_seq(*SOS*): found 'movable' molecule ",typ,&
                          " with index ",imol," (move type: ",k,")"
                STOP
                return
            endif
#endif

        endif

        !if (molmovtyp(typ) == 0) cycle 

        reset = .false.

        do_ewald_rcp = ( job%coultype(ib) == 1 .and. cfgs(ib)%mols(imol)%has_charge )

        oldenth = 0.0_wp
        oldvir = 0.0_wp
        oldreal = 0.0_wp
        !oldrcp = 0.0_wp
        oldvdw = 0.0_wp
        oldthree = 0.0_wp
        oldmany = 0.0_wp
        oldext = 0.0_wp
        oldmfa = 0.0_wp

        oldrcp = energyrcp(ib)
        newrcp = oldrcp

        newenth = 0.0_wp
        newvir = 0.0_wp
        newreal = 0.0_wp
        !newrcp = 0.0_wp
        newvdw = 0.0_wp
        newthree = 0.0_wp
        newmany = 0.0_wp
        newext = 0.0_wp
        newmfa = 0.0_wp

        distmax = distance_mol_max(typ)   !distance moved for this type
        total_mol_moves = total_mol_moves + 1
        attempted_mol_moves(typ) = attempted_mol_moves(typ) + 1

        !store positions and com
        call store_molecule_pos(ib, imol)

        !get initial energy
        if( job%uselist ) then

            call molecule_energy(ib, imol, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                     oldreal, oldvdw, oldthree, oldmany, oldext, oldmfa, oldvir,  &
                     orealnonb, orealbond, ovdwn, ovdwb, othree, ofour, omany, oext)

        else

            call molecule_energy_nolist(ib, imol, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, &
                     oldreal, oldvdw, oldthree, oldmany, oldext, oldmfa, oldvir,  &
                     orealnonb, orealbond, ovdwn, ovdwb, othree, ofour, omany, oext)

        endif

        !AB: check for any overlaps missed in the past
        if( oldvdw > vdwcap ) then 
            write(uout,*)"move_molecules_seq(): stuck due to VdW overlap(s)!..", &
                         oldvdw," > ",vdwcap," (",imol,") - need to increase VDW ecap?" !,", ",i,")"
            flush(uout)
            
            !goto 1000 ! reject - new energy is too high
        !end if

        !AB: check for any overlaps missed in the past
        else if( oldext > vdwcap ) then 
            write(uout,*)"move_molecules_seq(): stuck due to EXT overlap(s)!..", &
                         oldext," > ",vdwcap," (",imol,") - need to increase VDW ecap?" !,", ",i,")"
            flush(uout)
            
            !goto 1000 ! reject - new energy is too high
        end if

        atom_out = .false.

        !move atom
        !call move_molecule(ib, imol, distmax, atom_out)

        ! displacement
        rmove(1) = (2.0_wp * duni() - 1.0_wp) * distmax * job % molmove(1,k) !molmovtyp(typ))
        rmove(2) = (2.0_wp * duni() - 1.0_wp) * distmax * job % molmove(2,k) !molmovtyp(typ))
        rmove(3) = (2.0_wp * duni() - 1.0_wp) * distmax * job % molmove(3,k) !molmovtyp(typ))

        call move_molecule_by(ib, imol, rmove, atom_out)

        !if( do_ewald_rcp ) call move_molecule_recip(ib, imol, newrcp, nrcp, job%lmoldata)

        !accept = .false.

        !AB: advance randmon number selection to preserve the random sequence
        arg = duni()

        deltavb = 0.0_wp

        !AB: prepare for FED sampling - before any possible rejection!

        icom1 = cfgs(ib)%mols(imol)%idcom

        !AB: rearrange this for other FED parameters, not related to COM:s
        !is_fed_com = ( is_fed_com .and. icom1 /= 0 )
        is_fed_com = ( fed%par_kind == FED_PAR_DIST2 .and. icom1 /= 0 )

        weight(:) = 0.0_wp

        if( is_fed_com ) then
        !if( job%fedcalc ) then

            if( icom1 > 0 ) then
            !AB: the case of whole molecule belonging to same COM

                weight(icom1) = cfgs(ib)%mols(imol)%mass

            else
            !AB: the case of atom subset(s) belonging to same or different COM:s

                do ia = 1,cfgs(ib)%mols(imol)%natom

                   icom2 = cfgs(ib)%mols(imol)%atms(ia)%idcom

                   if( icom2 > 0 ) weight(icom2) = weight(icom2) + cfgs(ib)%mols(imol)%atms(ia)%mass

                end do

                icom1 = -icom1

            endif

            !AB: update the relevant COM for FED parameter
            if( weight(1) > 0.0_wp ) call fed_upd_com_by(ib, 1, rmove, weight(1))
            if( weight(2) > 0.0_wp ) call fed_upd_com_by(ib, 2, rmove, weight(2))

            deltavb = fed_param_dbias(ib,cfgs(ib))

            !AB: stay within the defined range
            if( deltavb > job%toler ) goto 1000

        end if

        !if in slit, make sure all atoms are still within the cell, otherwise reject the move!
        if( atom_out ) goto 1000 ! reject - atom got outside the slit

        !AB: use NL or not
        if( job%uselist ) then

            !check NL if in auto-update mode
            if( job%lauto_nbrs ) then

                call update_verlet_mol(ib, imol, job%verletshell, job%shortrangecut, reset)
                if( reset ) nbrlist_resets(ib) = nbrlist_resets(ib)+1
                
                !call check_verlet_mol(ib, imol, job%verletshell, reset)
                !if( reset ) then !call set_mol_nbrlist(ib, imol, job%shortrangecut, job%verletshell)
                !    nbrlist_resets(ib) = nbrlist_resets(ib)+1
                !    call set_mol_nbrlist(ib, imol, job%shortrangecut, job%verletshell)
                !end if

            endif

            call molecule_energy(ib, imol, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                     newreal, newvdw, newthree, newmany, newext, newmfa, newvir,  &
                     nrealnonb, nrealbond, nvdwn, nvdwb, nthree, nfour, nmany, next)

        else

            call molecule_energy_nolist(ib, imol, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, &
                     newreal, newvdw, newthree, newmany, newext, newmfa, newvir,  &
                     nrealnonb, nrealbond, nvdwn, nvdwb, nthree, nfour, nmany, next)

        endif

        if( newvdw > vdwcap ) goto 1000 ! reject - new energy is too high
        if( newext > vdwcap ) goto 1000 ! reject - new energy is too high
        !if( newvdw + newext > vdwcap ) then 
        !    write(uout,*)"move_molecules_seq(): rejected due to VdW overlap..."
        !    goto 1000 ! reject - new energy is too high
        !end if
        
        if( do_ewald_rcp ) call move_molecule_recip(ib, imol, newrcp, nrcp)!, job%lmoldata)

        oldtotal = oldreal + oldrcp + oldvdw + oldthree + oldmany + oldext + oldmfa

        oldenth = oldtotal + extpress * cfgs(ib)%vec%volume

        newtotal = newreal + newrcp + newvdw + newthree + newmany + newext + newmfa

        newenth = newtotal + extpress * cfgs(ib)%vec%volume

        !TU: In general we need code here to set 'deltavb' for other order parameters, i.e. not COM.
        !TU: I understand this is unnecessary in this procedure for, e.g. volume as the order parameter, since
        !TU: the volume is unchanged by a translation move, and hence deltavb=0. But 'in general', here
        !TU: is where 'deltavb' should be set.

        !metropoolis
        deltav = (newtotal - oldtotal)

        deltavb = deltavb + deltav * beta

        !TU: Update transition matrix if it is in use: the canonical (i.e, without biasing) probability is
        !TU: min(1,exp(-deltav*beta)).
        !TU: Note that this is only reached if the trial state is 'in bounds' for the order parameter range,
        !TU: and hence the transition matrix is only updated for in bounds trial states.
        if( fed % method == FED_TM .and. is_fed_com ) then
           
           call fed_tm_inc(  min( 1.0_wp, exp(-deltav*beta) )  )
           
        end if

        !TU: Reject the move if it leaves the window or takes us fuether from the window                          
        if( fed % is_window .and. is_fed_com ) then
           
           if( fed_window_reject() ) goto 1000
           
        end if

        if( deltavb > job%toler ) goto 1000 ! reject - new energy is too high
        if( deltavb < 0.0_wp .or. arg < exp(-deltavb) ) then

            !AB: update the COM difference for FED parameter
            if( is_fed_com ) then 
            
                call fed_upd_com_dist2(ib)
            
                !AB: bias is also updated if needed (e.g. WL)
                call fed_param_hist_inc(ib,.true.)

            end if

            !make sure image convention is still ok
            !call pbc_molecule(ib, imol)

            !accept update energies etc
            energytot(ib) = energytot(ib) + deltav
            enthalpytot(ib) = enthalpytot(ib) + (newenth - oldenth)
            virialtot(ib) = virialtot(ib) + (newvir - oldvir)
            !energyrcp(ib) = newrcp
            energyreal(ib) = energyreal(ib) + (newreal - oldreal)
            energyvdw(ib) = energyvdw(ib) + (newvdw - oldvdw)
            energythree(ib) = energythree(ib) + (newthree - oldthree)
            energymany(ib) = energymany(ib) + (newmany - oldmany)
            energyext(ib) = energyext(ib) + (newext - oldext)
            energymfa(ib) = energymfa(ib) + (newmfa - oldmfa)

            no_mol_moves(typ) = no_mol_moves(typ) + 1
            successful_mol_moves = successful_mol_moves + 1

            if( do_ewald_rcp ) then
                energyrcp(ib) = newrcp
                call update_rcpsums(ib)
            endif

            !must recalc whole nbrlist as other atoms will be out of sync on accepting the move
            !if( job%uselist .and. job%lauto_nbrs .and. reset ) then
            if( reset ) then


                !TU: Above the neighbour list for the atoms in the moved molecule have been updated.
                !TU: Say atom 'i' is such an atom. We must still update the neighbour lists of all atoms
                !TU: which are within the cut-off of 'i', i.e. all atoms within the (newly updated)
                !TU: neighbour list of 'i'. The below procedure does this for all such atoms 'i'. If
                !TU: this is not done then 'i' will 'see' the atoms in its local environment correctly,
                !TU: but the atoms in the local environment may not see 'i'.
                call update_mol_verlet_cell(ib, job%verletshell, job%shortrangecut, im)

            endif

            if(job%lmoldata) then

                do im = 1, number_of_molecules

                    emolrealnonb(ib,im) = emolrealnonb(ib,im) + (nrealnonb(im) - orealnonb(im))
                    emolrealbond(ib,im) = emolrealbond(ib,im) + (nrealbond(im) - orealbond(im))
                    orcp(im) = emolrcp(ib,im)
                    emolrcp(ib,im) = nrcp(im)
                    emolvdwn(ib,im) = emolvdwn(ib,im) + (nvdwn(im) - ovdwn(im))
                    emolvdwb(ib,im) = emolvdwb(ib,im) + (nvdwb(im) - ovdwb(im))
                    emolthree(ib,im) = emolthree(ib,im) + (nthree(im) - othree(im))
                    emolfour(ib,im) = emolfour(ib,im) + (nfour(im) - ofour(im))
                    emolmany(ib,im) = emolmany(ib,im) + (nmany(im) - omany(im))
                    emolext(ib,im) = emolext(ib,im) + (next(im) - oext(im))

                    otot = orealnonb(im) + orealbond(im) + orcp(im) + ovdwn(im) + ovdwb(im) +   &
                           othree(im) + ofour(im) + omany(im) + oext(im)
                    ntot = nrealnonb(im) + nrealbond(im) + nrcp(im) + nvdwn(im) + nvdwb(im) +   &
                           nthree(im) + nfour(im) + nmany(im) + next(im)

                    emoltot(ib,im) = emoltot(ib,im) + (ntot - otot)

                enddo

            endif

            cycle

        end if
        !else

1000        call restore_molecule_pos(ib, imol)  ! rejected-put mol back!!

            !AB: restore molecule NLs by resetting
            !if( reset ) call set_mol_nbrlist(ib, imol, job%shortrangecut, job%verletshell)
            if( reset ) call update_verlet_mol(ib, imol, job%verletshell, job%shortrangecut, reset)

            !AB: revert the attempted displacement in COM
            if( is_fed_com ) then 
            
                !call fed_upd_com_by(ib, icom, -rmove, weight)
                if( weight(1) > 0.0_wp ) call fed_upd_com_by(ib, 1, -rmove, weight(1))
                if( weight(2) > 0.0_wp ) call fed_upd_com_by(ib, 2, -rmove, weight(2))
            
                !AB: bias is also updated if needed (e.g. WL)
                call fed_param_hist_inc(ib,.false.)

            end if

        !endif

    enddo ! end loop over molecules

end subroutine

!> does a rigid molecule rotate
!> Note that PSMC is assumed to be enabled if the optional argument 'ib_pas' is present. In this case
!> box 'ib_pas' is updated as the passive box while 'ib' is updated as the active box.
subroutine rotate_molecules(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                      emolpair, emolang, emolfour, emolmany, emolext, ib_pas)

    use kinds_f90
    use species_module
    use control_type
    use cell_module
    use field_module
    use random_module, only : duni

    use constants_module, only : uout, FED_PAR_DIST2, FED_PAR_PSMC, FED_PAR_PSMC_HS, FED_TM !, FED_PAR_DIST1
    use fed_calculus_module, only : fed, fed_tm_inc
    use fed_order_module, only : fed_upd_com_by, fed_param_dbias, fed_param_dbias_psmc, fed_upd_com_dist2, &
                                 fed_param_hist_inc, fed_window_reject

    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), &
                      energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                      energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                      emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                      emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                      emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                      emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                      emolmany(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules)

    !TU: Optional variable: the passive box to update analogously to the active box ib. If this argument
    !TU: is not present then there is no update to a passive box. If this argument is present then PSMC is assumed.
    integer, intent(in), optional :: ib_pas

    integer :: k, i, typ, im, ia, icom1, icom2, ngas, niter

    real(kind=wp) :: rmove(3,2), weight(2), wght, rot(9)

    real(kind=wp) :: arg, distmax, deltav, deltavb, otot, ntot, oldreal, oldrcp, oldvdw, oldthree,  &
                     oldmany, oldext, oldmfa, oldtotal, oldenth, oldvir, newreal, newrcp, newvdw, newthree,  &
                     newmany, newext, newmfa, newtotal, newenth, newvir

     !temp vectors for molecular energies
    real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules), orcp(number_of_molecules), &
                       ovdwn(number_of_molecules), ovdwb(number_of_molecules), &
                       othree(number_of_molecules),  ofour(number_of_molecules), &
                       omany(number_of_molecules), oext(number_of_molecules), &
                       nrealnonb(number_of_molecules), nrealbond(number_of_molecules), nrcp(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules),  &
                       nthree(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules), next(number_of_molecules)

    logical :: mol_found, reset, do_ewald_rcp, atom_out, is_fed_com, is_fed_param_psmc

    !TU: Relevant variables for the passive box for PSMC (variables which end in '_pas')
    integer :: typ_pas
    
    real(kind=wp) :: otot_pas, ntot_pas, oldreal_pas, oldrcp_pas, oldvdw_pas, oldthree_pas, oldpair_pas, oldang_pas, &
                     oldfour_pas, oldmany_pas, oldext_pas, oldmfa_pas, oldtotal_pas, oldenth_pas, oldvir_pas, &
                     newreal_pas, newrcp_pas, newvdw_pas, newthree_pas, newpair_pas, newang_pas, newfour_pas, &
                     newmany_pas, newext_pas, newmfa_pas, newtotal_pas, newenth_pas, newvir_pas

    real(kind = wp) :: orealnonb_pas(number_of_molecules), orealbond_pas(number_of_molecules), &
                       orcp_pas(number_of_molecules), ovdwn_pas(number_of_molecules), &
                       ovdwb_pas(number_of_molecules), opair_pas(number_of_molecules), &
                       othree_pas(number_of_molecules),  oang_pas(number_of_molecules), &
                       ofour_pas(number_of_molecules), omany_pas(number_of_molecules), &
                       ocrct_pas(number_of_molecules), oext_pas(number_of_molecules), &
                       nrealnonb_pas(number_of_molecules), nrealbond_pas(number_of_molecules), &
                       nrcp_pas(number_of_molecules), nvdwn_pas(number_of_molecules), &
                       nvdwb_pas(number_of_molecules), npair_pas(number_of_molecules), &
                       nthree_pas(number_of_molecules), nang_pas(number_of_molecules), &
                       nfour_pas(number_of_molecules), nmany_pas(number_of_molecules), &
                       ncrct_pas(number_of_molecules), next_pas(number_of_molecules)

    logical :: ib_pas_on, reset_pas, do_ewald_pas, atom_out_pas
    
    !AB: this is a new, much faster implementation of the random molecule selection where
    !AB: molecules are chosen from only those molecular species that contain rotatable molecules;
    !AB: the list of molecule roatations is checked against all the molecular species to ensure 
    !AB: that all the rotatable molecules can be definitely found (see montecarlo_module::setupmc);
    
    call select_species_molrot(ib, typ, k, im, molrottyp, mol_found)

    if( .not.mol_found ) then
    
#ifdef DEBUG
!debugging
        if( cfgs(ib)%mtypes(typ)%num_mols < 1  ) then
            write(*,*)"rotate_molecules(): rotatable molecule type ",typ," is empty..."
        elseif (typ /= cfgs(ib)%mols(im)%mol_label .or. typ < 1 ) then
            write(*,*)"rotate_molecules(): no rotatable molecule found..."
        endif
#endif

        empty_mol_moves = empty_mol_moves+1

        return
    endif
    
#ifdef DEBUG
!debugging
    if( im < 1 .or. typ < 1 .or. k < 1 .or. im > cfgs(ib)%num_mols .or. im > cfgs(ib)%mxmol ) then
        write(*,*)"rotate_molecules(*SOS*): found rotatable molecule ",typ,&
                  " with index ",im," (move type: ",k,")"
        write(*,*)"rotate_molecules(*SOS*): in mol. species  containing ",cfgs(ib)%mtypes(typ)%num_mols," mols out of ",&
                  cfgs(ib)%mtypes(typ)%max_mols," (max)"
        write(*,*)"rotate_molecules(*SOS*): in configuration containing ",cfgs(ib)%num_mols," mols out of ",cfgs(ib)%mxmol," (max)"
        STOP
        return
    endif
#endif

    reset = .false.
    reset_pas = .false.

    !AB: reduce the number of repetitive checks
    do_ewald_rcp = ( job%coultype(ib) == 1 .and. cfgs(ib)%mols(im)%has_charge )
    do_ewald_pas = .false.

    is_fed_param_psmc = ( fed%par_kind == FED_PAR_PSMC .or. fed%par_kind == FED_PAR_PSMC_HS )

    ib_pas_on = present(ib_pas)
    
    oldenth = 0.0_wp
    oldvir = 0.0_wp
    oldreal = 0.0_wp
    !oldrcp = 0.0_wp
    oldvdw = 0.0_wp
    oldthree = 0.0_wp
    oldmany = 0.0_wp
    oldext = 0.0_wp
    oldmfa = 0.0_wp

    oldrcp = energyrcp(ib)
    newrcp = oldrcp

    newenth = 0.0_wp
    newvir = 0.0_wp
    newreal = 0.0_wp
    !newrcp = 0.0_wp
    newvdw = 0.0_wp
    newthree = 0.0_wp
    newmany = 0.0_wp
    newext = 0.0_wp
    newmfa = 0.0_wp

    call store_molecule_pos(ib,im)

    distmax = rotate_mol_max(typ)   !distance moved for this type

    total_mol_rots = total_mol_rots + 1
    attempted_mol_rots(typ) = attempted_mol_rots(typ) + 1

    !get initial energy
    if(job%uselist) then

        call molecule_energy(ib, im, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                     oldreal, oldvdw, oldthree, oldmany, oldext, oldmfa, oldvir,  &
                     orealnonb, orealbond, ovdwn, ovdwb, othree, ofour, omany, oext)

    else

        call molecule_energy_nolist(ib, im, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, &
                     oldreal, oldvdw, oldthree, oldmany, oldext, oldmfa, oldvir,  &
                     orealnonb, orealbond, ovdwn, ovdwb, othree, ofour, omany, oext)

    endif

    !AB: check for any overlaps missed in the past
    if( oldvdw > vdwcap ) then 
        write(uout,*)"rotate_molecules(): stuck due to VdW overlap(s)!..", &
                     oldvdw," > ",vdwcap," (",im,") - need to increase VDW ecap?" !,", ",i,")"
        flush(uout)
        
        !goto 1000 ! reject - new energy is too high
    !end if

    !AB: check for any overlaps missed in the past
    else if( oldext > vdwcap ) then 
        write(uout,*)"rotate_molecules(): stuck due to EXT overlap(s)!..", &
                     oldext," > ",vdwcap," (",im,") - need to increase VDW ecap?" !,", ",i,")"
        flush(uout)
        
        !goto 1000 ! reject - new energy is too high
    end if

    !TU: Do the same as above for the passive box in PSMC

    if( ib_pas_on ) then

        typ_pas = cfgs(ib_pas)%mols(im)%mol_label

        do_ewald_pas = ( job%coultype(ib_pas) == 1 .and. cfgs(ib_pas)%mols(im)%has_charge )

        oldenth_pas = 0.0_wp
        oldvir_pas = 0.0_wp
        oldreal_pas = 0.0_wp
        !oldrcp_pas = energyrcp(ib_pas)
        oldvdw_pas = 0.0_wp
        oldthree_pas = 0.0_wp
        oldpair_pas = 0.0_wp
        oldang_pas = 0.0_wp
        oldfour_pas = 0.0_wp
        oldmany_pas = 0.0_wp
        oldext_pas = 0.0_wp
        oldmfa_pas = 0.0_wp

        oldrcp_pas = energyrcp(ib_pas)
        newrcp_pas = oldrcp_pas

        newenth_pas = 0.0_wp
        newvir_pas = 0.0_wp
        newreal_pas = 0.0_wp
        !newrcp_pas = 0.0_wp
        newvdw_pas = 0.0_wp
        newthree_pas = 0.0_wp
        newpair_pas = 0.0_wp
        newang_pas = 0.0_wp
        newfour_pas = 0.0_wp
        newmany_pas = 0.0_wp
        newext_pas = 0.0_wp
        newmfa_pas = 0.0_wp

        nrcp_pas = 0.0_wp
        orcp_pas = 0.0_wp

        call store_molecule_pos(ib_pas,im)
                                                                                             
        if(job%uselist) then

            call molecule_energy(ib_pas, im, job%coultype(ib_pas), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                         oldreal_pas, oldvdw_pas, oldthree_pas, oldmany_pas, oldext_pas, oldmfa_pas, oldvir_pas,  &
                         orealnonb_pas, orealbond_pas, ovdwn_pas, ovdwb_pas, othree_pas, ofour_pas, omany_pas, oext_pas)

        else

            call molecule_energy_nolist(ib_pas, im, job%coultype(ib_pas), job%dielec, job%vdw_cut, job%shortrangecut, &
                         oldreal_pas, oldvdw_pas, oldthree_pas, oldmany_pas, oldext_pas, oldmfa_pas, oldvir_pas,  &
                         orealnonb_pas, orealbond_pas, ovdwn_pas, ovdwb_pas, othree_pas, ofour_pas, omany_pas, oext_pas)

        endif

    end if

    atom_out = .false.
    atom_out_pas = .false.

    ! rotate molecule
    if (job%usequaternion) then

        call rotate_molecule_quat(ib, im, distmax, atom_out, rot)

    else

        call rotate_molecule(ib, im, distmax, atom_out)

    endif
    
    !if( do_ewald_rcp ) call move_molecule_recip(ib, im, newrcp, nrcp)!, job%lmoldata)

    !TU: Do the same as above for the passive box in PSMC
    if( ib_pas_on ) then

        !TU: Rotate the same molecule in the passive box using the same rotation matrix 'rot'
        !TU: as was used in the active box
        !TU: Note that 'rot' is garbage if quaternions are not used with PSMC - but this would be
        !TU: noticed at initialisation
        ! rotate molecule
        if (job%usequaternion) call rotate_molecule_quat_by(ib_pas, im, atom_out_pas, rot)

        !if( do_ewald_pas ) call move_molecule_recip(ib_pas, im, newrcp_pas, nrcp_pas, job%lmoldata)

    end if

    !AB: advance randmon number selection to preserve the random sequence
    arg = duni()

    deltavb = 0.0_wp

    !AB: prepare for FED sampling - before any possible rejection!

    icom1 = cfgs(ib)%mols(im)%idcom

    !AB: activate FED if needed (checking fed%par_kind is sufficient)
    is_fed_com = ( fed%par_kind == FED_PAR_DIST2 .and. icom1 < 0 )

    !AB: rearrange this for other FED parameters, not related to COM:s
    !is_fed_com = ( is_fed_com .and. icom1 /= 0 )

    rmove(:,:) = 0.0_wp
    weight(:)  = 0.0_wp

    if( is_fed_com ) then

        !if( icom1 > 0 ) then
            !AB: the case of whole molecule belonging to same COM (impossible under rotation)
        !    weight(icom1) = cfgs(ib)%mols(im)%mass
        !else
            !AB: the case of atom subset(s) belonging to same or different COM:s

            do ia = 1,cfgs(ib)%mols(im)%natom

               icom2 = cfgs(ib)%mols(im)%atms(ia)%idcom

               if( icom2 > 0 ) then

                   wght = cfgs(ib)%mols(im)%atms(ia)%mass

                   rmove(:,icom2) = rmove(:,icom2) + wght* &
                                  ( cfgs(ib)%mols(im)%atms(ia)%rpos(:) - &
                                    cfgs(ib)%mols(im)%atms(ia)%store_rpos(:) )

                   weight(icom2) = 1.0_wp !weight(icom2) + wght

               end if

            end do

            icom1 = -icom1

        !endif

        !AB: update the relevant COM for FED parameter
        !call fed_upd_com_by(ib, icom, rmove, weight)
        if( weight(1) > 0.0_wp ) call fed_upd_com_by(ib, 1, rmove, weight(1))
        if( weight(2) > 0.0_wp ) call fed_upd_com_by(ib, 2, rmove, weight(2))

        deltavb = fed_param_dbias(ib,cfgs(ib))

        !AB: stay within the defined range
        if( deltavb > job%toler ) goto 1000

    end if

    !if in slit, make sure all atoms are still within the cell, otherwise reject the move!
    !TU: Note that atom_out_pas will be .false. if PSMC is not in use
    !if( atom_out .or. atom_out_pas ) goto 1000 ! reject - atom got outside the slit
    if( atom_out .or. atom_out_pas ) then 
!        write(uout,*)"rotate_molecules(): rejected due to 'atom_out' in z..."
!      do i = 1, cfgs(ib)%mols(im)%natom
!        if( abs( abs(cfgs(ib)%mols(im)%atms(i)%rpos(3)) &
!                   - cfgs(ib)%vec%latvector(3,3)*0.5_wp) > 1.0e-12_wp ) &
!            write(uout,*)cfgs(ib)%mols(im)%atms(i)%store_rpos(3),' -> ',&
!                         cfgs(ib)%mols(im)%atms(i)%rpos(3),cfgs(ib)%vec%latvector(3,3)*0.5_wp
!      end do
        goto 1000 
    end if

    !broadcast the atom position to other nodes unless this is rep exchange calculation
    !if (.not.job%repexch) call broadcast_molecule_pos(ib, im)

    !new energy of molecule
    if(job%uselist) then

        !check NL if in auto-update mode
        if( job%lauto_nbrs ) then

            call update_verlet_mol(ib, im, job%verletshell, job%shortrangecut, reset)
            if( reset ) nbrlist_resets(ib) = nbrlist_resets(ib)+1
            
            !call check_verlet_mol(ib, im, job%verletshell, reset)
            !if( reset ) then !call set_mol_nbrlist(ib, im, job%shortrangecut, job%verletshell)
            !    nbrlist_resets(ib) = nbrlist_resets(ib)+1
            !    call set_mol_nbrlist(ib, im, job%shortrangecut, job%verletshell)
            !end if

        endif

        call molecule_energy(ib, im, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                     newreal, newvdw, newthree, newmany, newext, newmfa, newvir,  &
                     nrealnonb, nrealbond, nvdwn, nvdwb, nthree, nfour, nmany, next)

    else

        call molecule_energy_nolist(ib, im, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, &
                     newreal, newvdw, newthree, newmany, newext, newmfa, newvir,  &
                     nrealnonb, nrealbond, nvdwn, nvdwb, nthree, nfour, nmany, next)

    endif

    !TU: Do the same for the passive box in PSMC
    if( ib_pas_on ) then

        if(job%uselist) then

            !check NL if in auto-update mode
            if( job%lauto_nbrs ) then

                call update_verlet_mol(ib_pas, im, job%verletshell, job%shortrangecut, reset_pas)
                if( reset ) nbrlist_resets(ib_pas) = nbrlist_resets(ib_pas)+1


                !call check_verlet_mol(ib_pas, im, job%verletshell, reset_pas)
                !if( reset_pas ) call set_mol_nbrlist(ib_pas, im, job%shortrangecut, job%verletshell)

            endif

            call molecule_energy(ib_pas, im, job%coultype(ib_pas), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                         newreal_pas, newvdw_pas, newthree_pas, newmany_pas, newext_pas, newmfa_pas, newvir_pas,  &
                         nrealnonb_pas, nrealbond_pas, nvdwn_pas, nvdwb_pas, nthree_pas, nfour_pas, nmany_pas, next_pas)

        else

            call molecule_energy_nolist(ib_pas, im, job%coultype(ib_pas), job%dielec, job%vdw_cut, job%shortrangecut, &
                         newreal_pas, newvdw_pas, newthree_pas, newmany_pas, newext_pas, newmfa_pas, newvir_pas,  &
                         nrealnonb_pas, nrealbond_pas, nvdwn_pas, nvdwb_pas, nthree_pas, nfour_pas, nmany_pas, next_pas)

        endif

    end if

    if( newvdw > vdwcap ) goto 1000 ! reject - new energy is too high
    if( newext > vdwcap ) goto 1000 ! reject - new energy is too high
    !if( newvdw + newext > vdwcap ) then 
    !    write(uout,*)"rotate_molecules(): rejected due to VdW overlap..."
    !    goto 1000 ! reject - new energy is too high
    !end if
    
    if( do_ewald_rcp ) call move_molecule_recip(ib, im, newrcp, nrcp)!, job%lmoldata)

    oldtotal = oldreal + oldrcp + oldvdw + oldthree + oldmany + oldext + oldmfa
    oldenth  = oldtotal + extpress * cfgs(ib)%vec%volume

    newtotal = newreal + newrcp + newvdw + newthree + newmany + newext + newmfa
    newenth  = newtotal + extpress * cfgs(ib)%vec%volume

    !TU: Do the same for the passive box in PSMC
    if( ib_pas_on ) then

        if( do_ewald_pas ) call move_molecule_recip(ib_pas, im, newrcp_pas, nrcp_pas)!, job%lmoldata)

        oldtotal_pas = oldreal_pas + oldrcp_pas + oldvdw_pas + oldthree_pas + oldmany_pas + oldext_pas + oldmfa_pas
        oldenth_pas  = oldtotal_pas + extpress * cfgs(ib_pas)%vec%volume

        newtotal_pas = newreal_pas + newrcp_pas + newvdw_pas + newthree_pas + newmany_pas + newext_pas + newmfa_pas
        newenth_pas  = newtotal_pas + extpress * cfgs(ib_pas)%vec%volume

    end if

    !TU: If PSMC order parameter is in use then accordingly bias the acceptance probability
    if( is_fed_param_psmc ) then

        !TU: deltavb should be 0 at this point if we are using the PSMC order parameter.
        !TU: The difference in the bias between the new state and the current state is given by
        !TU: fed_param_dbias_psmc(energy1, energy2), where energy1 and energy2 are the NEW
        !TU: energies of phases 1 and 2.

        !TU: NB: oldtotal, oldtotal_pas, newtotal, and newtotal_pas are the energies of the ATOM
        !TU: which has just been moved, not the energy of the system. However energy1 and energy2
        !TU: should be TOTAL.
        if( ib == 1) then

            deltavb = fed_param_dbias_psmc( energytot(1) + newtotal - oldtotal, &
                                            energytot(2) + newtotal_pas - oldtotal_pas )

        else

            !TU: WARNING - I assume that ib==2 if ib/=1. Something has gone horribly
            !TU: wrong if this is not the case at this point, hence I don't even check.

            deltavb = fed_param_dbias_psmc(energytot(1) + newtotal_pas - oldtotal_pas, &
                                           energytot(2) + newtotal - oldtotal )

        end if

        !TU: stay within the defined range
        if( deltavb > job%toler ) goto 1000

    end if

    !TU: In general we need code here to set 'deltavb' for other order parameters, i.e. not COM or PSMC.
    !TU: I understand this is unnecessary in this procedure for, e.g. volume as the order parameter, since
    !TU: the volume is unchanged by a translation move, and hence deltavb=0. But 'in general', here
    !TU: is where 'deltavb' should be set.

    !metropoolis
    deltav  = (newtotal - oldtotal)
    deltavb = deltavb + deltav * beta

    !TU: Update transition matrix if it is in use: the canonical (i.e, without biasing) probability is
    !TU: min(1,exp(-deltav*beta)).
    !TU: Note that this is only reached if the trial state is 'in bounds' for the order parameter range,
    !TU: and hence the transition matrix is only updated for in bounds trial states.
    if( fed % method == FED_TM .and. ( is_fed_com .or. is_fed_param_psmc ) ) then
        
        call fed_tm_inc(  min( 1.0_wp, exp(-deltav*beta) )  )

    end if

    !TU: Reject the move if it leaves the window or takes us fuether from the window                          
    if( fed % is_window .and. ( is_fed_com .or. is_fed_param_psmc ) ) then
       
       if( fed_window_reject() ) goto 1000
       
    end if

    if( deltavb > job%toler ) goto 1000 ! reject - new energy is too high
    if( deltavb < 0.0_wp .or. arg < exp(-deltavb) ) then

         !AB: update the COM difference for FED parameter
         if( is_fed_com ) then 
         
             call fed_upd_com_dist2(ib)
         
         !AB: bias is also updated if needed (e.g. WL)
             call fed_param_hist_inc(ib,.true.)
         
         else if( is_fed_param_psmc ) then
         
             !TU: update the FED histogram & bias if needed (e.g. WL) for PSMC
             call fed_param_hist_inc(ib,.true.)

         end if

         !accept update energies etc
         energytot(ib) = energytot(ib) + deltav
         enthalpytot(ib) = enthalpytot(ib) + (newenth - oldenth)
         virialtot(ib) = virialtot(ib) + (newvir - oldvir)
         !energyrcp(ib) = newrcp
         energyreal(ib) = energyreal(ib) + (newreal - oldreal)
         energyvdw(ib) = energyvdw(ib) + (newvdw - oldvdw)
         energythree(ib) = energythree(ib) + (newthree - oldthree)
         energymany(ib) = energymany(ib) + (newmany - oldmany)
         energyext(ib) = energyext(ib) + (newext - oldext)
         energymfa(ib) = energymfa(ib) + (newmfa - oldmfa)

         no_mol_rots(typ) = no_mol_rots(typ) + 1
         successful_mol_rots = successful_mol_rots + 1
        
         !debugging
         !if( abs(energyrcp(ib)).lt. 1.0e-10_wp ) write(*,*)'rotate_molecules():: accepted with energyrcp(ib) = ',energyrcp(ib)

         if( do_ewald_rcp ) then
            energyrcp(ib) = newrcp
            call update_rcpsums(ib)
         endif

         !must recalc whole nbrlist as other atoms will be out of sync on accepting the move
         !if( job%uselist .and. job%lauto_nbrs .and. reset ) then
         if( reset ) then

             !TU: Above the neighbour list for the atoms in the moved molecule have been updated.
             !TU: Say atom 'i' is such an atom. We must still update the neighbour lists of all atoms
             !TU: which are within the cut-off of 'i', i.e. all atoms within the (newly updated)
             !TU: neighbour list of 'i'. The below procedure does this for all such atoms 'i'. If
             !TU: this is not done then 'i' will 'see' the atoms in its local environment correctly,
             !TU: but the atoms in the local environment may not see 'i'.
             call update_mol_verlet_cell(ib, job%verletshell, job%shortrangecut, im)

         endif

         !TU: Do the same as above for the passive box in PSMC (for energies only - not counters)
         if( ib_pas_on ) then
 
             if( reset_pas ) then

                 !TU: Above the neighbour list for the atoms in the moved molecule have been updated.
                 !TU: Say atom 'i' is such an atom. We must still update the neighbour lists of all atoms
                 !TU: which are within the cut-off of 'i', i.e. all atoms within the (newly updated)
                 !TU: neighbour list of 'i'. The below procedure does this for all such atoms 'i'. If
                 !TU: this is not done then 'i' will 'see' the atoms in its local environment correctly,
                 !TU: but the atoms in the local environment may not see 'i'.
                 call update_mol_verlet_cell(ib_pas, job%verletshell, job%shortrangecut, im)

             endif

             energytot(ib_pas) = energytot(ib_pas) + (newtotal_pas - oldtotal_pas)
             enthalpytot(ib_pas) = enthalpytot(ib_pas) + (newenth_pas - oldenth_pas)
             virialtot(ib_pas) = virialtot(ib_pas) + (newvir_pas - oldvir_pas)
             !energyrcp(ib_pas) = energyrcp(ib_pas) + (newrcp_pas - oldrcp_pas)
             energyreal(ib_pas) = energyreal(ib_pas) + (newreal_pas - oldreal_pas)
             energyvdw(ib_pas) = energyvdw(ib_pas) + (newvdw_pas - oldvdw_pas)
             energythree(ib_pas) = energythree(ib_pas) + (newthree_pas - oldthree_pas)
             energymany(ib_pas) = energymany(ib_pas) + (newmany_pas - oldmany_pas)
             energyext(ib_pas) = energyext(ib_pas) + (newext_pas - oldext_pas)
             energymfa(ib_pas) = energymfa(ib_pas) + (newmfa_pas - oldmfa_pas)
 
             if( do_ewald_pas ) then
                energyrcp(ib_pas) = newrcp_pas
                call update_rcpsums(ib_pas)
             endif
 
         end if

         if(job%lmoldata) then

             do im = 1, number_of_molecules

                 emolrealnonb(ib,im) = emolrealnonb(ib,im) + (nrealnonb(im) - orealnonb(im))
                 emolrealbond(ib,im) = emolrealbond(ib,im) + (nrealbond(im) - orealbond(im))
                 orcp(im) = emolrcp(ib,im)
                 emolrcp(ib,im) = nrcp(im)
                 emolvdwn(ib,im) = emolvdwn(ib,im) + (nvdwn(im) - ovdwn(im))
                 emolvdwb(ib,im) = emolvdwb(ib,im) + (nvdwb(im) - ovdwb(im))
                 emolthree(ib,im) = emolthree(ib,im) + (nthree(im) - othree(im))
                 emolfour(ib,im) = emolfour(ib,im) + (nfour(im) - ofour(im))
                 emolmany(ib,im) = emolmany(ib,im) + (nmany(im) - omany(im))
                 emolext(ib,im) = emolext(ib,im) + (next(im) - oext(im))

                 otot = orealnonb(im) + orealbond(im) + orcp(im) + ovdwn(im) + ovdwb(im)    &
                        + othree(im) + ofour(im) + omany(im) + oext(im)
                 ntot = nrealnonb(im) + nrealbond(im) + nrcp(im) + nvdwn(im) + nvdwb(im)    &
                        + nthree(im) + nfour(im) + nmany(im) + next(im)

                 emoltot(ib,im) = emoltot(ib,im) + (ntot - otot)

                 !TU: Do the same as above for the passive box in PSMC
                 if( ib_pas_on ) then

                     emolrealnonb(ib_pas,im) = emolrealnonb(ib_pas,im) + (nrealnonb_pas(im) - orealnonb_pas(im))
                     emolrealbond(ib_pas,im) = emolrealbond(ib_pas,im) + (nrealbond_pas(im) - orealbond_pas(im))
                     orcp_pas(im) = emolrcp(ib_pas,im)
                     emolrcp(ib_pas,im) = nrcp_pas(im)
                     emolvdwn(ib_pas,im) = emolvdwn(ib_pas,im) + (nvdwn_pas(im) - ovdwn_pas(im))
                     emolvdwb(ib_pas,im) = emolvdwb(ib_pas,im) + (nvdwb_pas(im) - ovdwb_pas(im))
                     emolthree(ib_pas,im) = emolthree(ib_pas,im) + (nthree_pas(im) - othree_pas(im))
                     emolfour(ib_pas,im) = emolfour(ib_pas,im) + (nfour_pas(im) - ofour_pas(im))
                     emolmany(ib_pas,im) = emolmany(ib_pas,im) + (nmany_pas(im) - omany_pas(im))
                     emolext(ib_pas,im) = emolext(ib_pas,im) + (next_pas(im) - oext_pas(im))

                     otot_pas = orealnonb_pas(im) + orealbond_pas(im) + orcp_pas(im) + ovdwn_pas(im) + ovdwb_pas(im)    &
                            + othree_pas(im) + ofour_pas(im) + omany_pas(im) + oext_pas(im)
                     ntot_pas = nrealnonb_pas(im) + nrealbond_pas(im) + nrcp_pas(im) + nvdwn_pas(im) + nvdwb_pas(im)    &
                            + nthree_pas(im) + nfour_pas(im) + nmany_pas(im) + next_pas(im)

                     emoltot(ib_pas,im) = emoltot(ib_pas,im) + (ntot_pas - otot_pas)
 
                 end if

             enddo

         endif

         return

    endif
    !else

1000    call restore_molecule_pos(ib, im)  ! rejected - put mol back!!

        !AB: restore molecule NLs by resetting
        !if( reset ) call set_mol_nbrlist(ib, im , job%shortrangecut, job%verletshell)
        if( reset ) call update_verlet_mol(ib, im, job%verletshell, job%shortrangecut, reset)

        !TU: Do the same as above for the passive box in PSMC
        if( ib_pas_on ) then

            call restore_molecule_pos(ib_pas, im)

            if( reset_pas ) &
                call update_verlet_mol(ib_pas, im, job%verletshell, job%shortrangecut, reset_pas)
                !call set_mol_nbrlist(ib_pas, im , job%shortrangecut, job%verletshell)

        end if
         
        !AB: revert the attempted displacement in COM
        if( is_fed_com ) then 
        
            !call fed_upd_com_by(ib, icom, -rmove, weight)
            if( weight(1) > 0.0_wp ) call fed_upd_com_by(ib, 1, -rmove, weight(1))
            if( weight(2) > 0.0_wp ) call fed_upd_com_by(ib, 2, -rmove, weight(2))
        
            !AB: bias is also updated if needed (e.g. WL)
            call fed_param_hist_inc(ib,.false.)
        
        else if( is_fed_param_psmc ) then
        
            !TU: update the FED histogram & bias if needed (e.g. WL)
            !TU: but revert the parameter state (value & id/index)
        
            call fed_param_hist_inc(ib,.false.)

        end if

    !endif

end subroutine

!> does a sequential rigid molecule rotate
subroutine rotate_molecules_seq(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                      emolpair, emolang, emolfour, emolmany, emolext)

    use kinds_f90
    use species_module
    use control_type
    use cell_module
    use field_module
    use random_module, only : duni

    use constants_module, only : uout, FED_PAR_DIST2, FED_TM !, FED_PAR_DIST1
    use fed_calculus_module, only : fed, fed_tm_inc
    use fed_order_module, only : fed_upd_com_by, fed_param_dbias, fed_upd_com_dist2, fed_param_hist_inc, fed_window_reject

    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), &
                      energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                      energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                      emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                      emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                      emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                      emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                      emolmany(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules)

    integer :: k, typ, imol, iml, im, ia, icom1, icom2

    real(kind=wp) :: rmove(3,2), weight(2), wght, rot(9)

    real(kind=wp) :: arg, distmax, deltav, deltavb, otot, ntot, oldreal, oldrcp, oldvdw, oldthree, oldmany, &
                     oldext, oldmfa, oldtotal, oldenth, oldvir, newreal, newrcp, newvdw, newthree, newmany, &
                     newext, newmfa, newtotal, newenth, newvir

    real(kind=wp) :: npair(number_of_molecules),nang(number_of_molecules)

     !temp vectors for molecular energies
    real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules), orcp(number_of_molecules), &
                       ovdwn(number_of_molecules), ovdwb(number_of_molecules),  &
                       othree(number_of_molecules),  ofour(number_of_molecules), &
                       omany(number_of_molecules), oext(number_of_molecules),  &
                       nrealnonb(number_of_molecules), nrealbond(number_of_molecules), nrcp(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules), &
                       nthree(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules), next(number_of_molecules)

    logical :: mol_found, reset, do_ewald_rcp, atom_out, is_fed_com

    !AB: reduce the number of repetitive checks
    do_ewald_rcp = .false.

    !AB: activate FED if needed (checking fed%par_kind is sufficient)
    is_fed_com = ( fed%par_kind == FED_PAR_DIST2 )

    do iml = 1, cfgs(ib)%num_mols
    
        imol = iml
        
        typ = cfgs(ib)%mols(imol)%mol_label

        if( job%useseqmolrotrnd ) then
        !AB: this is a new, much faster implementation of the random molecule selection where
        !AB: molecules are chosen from only those molecular species that contain movable molecules;
        !AB: the list of molecule moves is checked against all the molecular species to ensure 
        !AB: that all the movable molecules can be definitely found (see montecarlo_module::setupmc);
        
            call select_species_molrot(ib, typ, k, imol, molrottyp, mol_found)

            !debugging
            if( .not.mol_found ) then
            
#ifdef DEBUG
                if( cfgs(ib)%mtypes(typ)%num_mols < 1  ) then
                    write(*,*)"rotate_molecules(): rotatable molecule type ",typ," is empty..."
                elseif (typ /= cfgs(ib)%mols(imol)%mol_label .or. typ < 1 ) then
                    write(*,*)"rotate_molecules(): no rotatable molecule found..."
                endif
#endif
                empty_mol_moves = empty_mol_moves+1

                cycle
            endif

#ifdef DEBUG
!            if( imol < 1 .or. typ < 0 .or. k < 0 ) then
            if( imol < 1 .or. typ < 1 .or. k < 1 .or. imol > cfgs(ib)%num_mols .or. imol > cfgs(ib)%mxmol ) then
                write(*,*)"rotate_molecules(*SOS*): found 'rotatable' molecule ",typ,&
                          " with index ",imol," (move type: ",k,")"
                STOP
                return
            endif
#endif

        else

            !AB: make sure the molecule is amongst the movable ones
            do k = 1, job%nummolrot
                mol_found = ( typ == molrottyp(k) )
                if( mol_found ) exit
            enddo

            !debugging
            if( .not.mol_found ) then

#ifdef DEBUG
                if( cfgs(ib)%mtypes(typ)%num_mols < 1  ) then
                    write(*,*)"rotate_molecules(): rotatable molecule type ",typ," is empty..."
                elseif (typ /= cfgs(ib)%mols(imol)%mol_label .or. typ < 1 ) then
                    write(*,*)"rotate_molecules(): no rotatable molecule found..."
                endif
#endif

                empty_mol_moves = empty_mol_moves+1

                cycle
            endif

#ifdef DEBUG
            if( imol < 1 .or. typ < 1 .or. k < 1 ) then
                write(*,*)"rotate_molecules(*SOS*): found 'rotatable' molecule ",typ,&
                          " with index ",imol," (move type: ",k,")"
                STOP
                return
            endif
#endif

        endif

        !if (molrottyp(typ) == 0) cycle 
        reset = .false.
        
        do_ewald_rcp = ( job%coultype(ib) == 1 .and. cfgs(ib)%mols(imol)%has_charge )

        oldenth = 0.0_wp
        oldvir = 0.0_wp
        oldreal = 0.0_wp
        !oldrcp = 0.0_wp
        oldvdw = 0.0_wp
        oldthree = 0.0_wp
        oldmany = 0.0_wp
        oldext = 0.0_wp
        oldmfa = 0.0_wp

        oldrcp = energyrcp(ib)
        newrcp = oldrcp

        newenth = 0.0_wp
        newvir = 0.0_wp
        newreal = 0.0_wp
        !newrcp = 0.0_wp
        newvdw = 0.0_wp
        newthree = 0.0_wp
        newmany = 0.0_wp
        newext = 0.0_wp
        newmfa = 0.0_wp

        distmax = rotate_mol_max(typ)   !distance moved for this type

        total_mol_rots = total_mol_rots + 1
        attempted_mol_rots(typ) = attempted_mol_rots(typ) + 1

        !store positions and centre of mass
        call store_molecule_pos(ib, imol)

        !get initial energy
        if(job%uselist) then

            call molecule_energy(ib, imol, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                         oldreal, oldvdw, oldthree, oldmany, oldext, oldmfa, oldvir,  &
                         orealnonb, orealbond, ovdwn, ovdwb, othree, ofour, omany, oext)

        else

            call molecule_energy_nolist(ib, imol, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, &
                         oldreal, oldvdw, oldthree, oldmany, oldext, oldmfa, oldvir,  &
                         orealnonb, orealbond, ovdwn, ovdwb, othree, ofour, omany, oext)

        endif

        !AB: check for any overlaps missed in the past
        if( oldvdw > vdwcap ) then 
            write(uout,*)"rotate_molecules_seq(): stuck due to VdW overlap(s)!..", &
                         oldvdw," > ",vdwcap," (",imol,") - need to increase VDW ecap?" !,", ",i,")"
            flush(uout)
            
            !goto 1000 ! reject - new energy is too high
        !end if

        !AB: check for any overlaps missed in the past
        else if( oldext > vdwcap ) then 
            write(uout,*)"rotate_molecules_seq(): stuck due to EXT overlap(s)!..", &
                         oldext," > ",vdwcap," (",imol,") - need to increase VDW ecap?" !,", ",i,")"
            flush(uout)
            
            !goto 1000 ! reject - new energy is too high
        end if

        atom_out = .false.

        ! rotate molecule
        if (job%usequaternion) then

            call rotate_molecule_quat(ib, imol, distmax, atom_out, rot)

        else

            call rotate_molecule(ib, imol, distmax, atom_out)

        endif

        !if( do_ewald_rcp ) call move_molecule_recip(ib, imol, newrcp, nrcp, job%lmoldata)

        !AB: advance randmon number selection to preserve the random sequence
        arg = duni()

        deltavb = 0.0_wp

        !AB: prepare for FED sampling - before any possible rejection!

        icom1 = cfgs(ib)%mols(imol)%idcom

        !AB: rearrange this for other FED parameters, not related to COM:s
        !is_fed_com = ( is_fed_com .and. icom1 < 0 )
        is_fed_com = ( fed%par_kind == FED_PAR_DIST2 .and. icom1 < 0 )

        rmove(:,:) = 0.0_wp
        weight(:)  = 0.0_wp

        if( is_fed_com ) then

            !if( icom1 > 0 ) then
            !AB: the case of whole molecule belonging to same COM (impossible under rotation)
            !    weight(icom1) = cfgs(ib)%mols(imol)%mass
            !else
                !AB: the case of atom subset(s) belonging to same or different COM:s

                do ia = 1,cfgs(ib)%mols(imol)%natom

                   icom2 = cfgs(ib)%mols(imol)%atms(ia)%idcom

                   if( icom2 > 0 ) then

                       wght = cfgs(ib)%mols(imol)%atms(ia)%mass

                       rmove(:,icom2) = rmove(:,icom2) + wght* &
                                      ( cfgs(ib)%mols(imol)%atms(ia)%rpos(:) - &
                                        cfgs(ib)%mols(imol)%atms(ia)%store_rpos(:) )

                       weight(icom2) = 1.0_wp !weight(icom2) + wght

                   end if

                end do

                icom1 = -icom1

            !endif

            !AB: update the relevant COM for FED parameter
            !call fed_upd_com_by(ib, icom, rmove, weight)
            if( weight(1) > 0.0_wp ) call fed_upd_com_by(ib, 1, rmove, weight(1))
            if( weight(2) > 0.0_wp ) call fed_upd_com_by(ib, 2, rmove, weight(2))

            deltavb = fed_param_dbias(ib,cfgs(ib))

            !AB: stay within the defined range
            if( deltavb > job%toler ) goto 1000

        end if

        !if in slit, make sure all atoms are still within the cell, otherwise reject the move!
        if( atom_out ) goto 1000 ! reject - atom got outside the slit

        !new energy of molecule
        if(job%uselist) then

            !check NL if in auto-update mode
            if( job%lauto_nbrs ) then

                call update_verlet_mol(ib, imol, job%verletshell, job%shortrangecut, reset)
                if( reset ) nbrlist_resets(ib) = nbrlist_resets(ib)+1
                
                !call check_verlet_mol(ib, imol, job%verletshell, reset)
                !if( reset ) then !call set_mol_nbrlist(ib, imol, job%shortrangecut, job%verletshell)
                !    nbrlist_resets(ib) = nbrlist_resets(ib)+1
                !    call set_mol_nbrlist(ib, imol, job%shortrangecut, job%verletshell)
                !end if


            endif

            call molecule_energy(ib, imol, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                         newreal, newvdw, newthree, newmany, newext, newmfa, newvir,  &
                         nrealnonb, nrealbond, nvdwn, nvdwb, nthree, nfour, nmany, next)

        else

            call molecule_energy_nolist(ib, imol, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, &
                         newreal, newvdw, newthree, newmany, newext, newmfa, newvir,  &
                         nrealnonb, nrealbond, nvdwn, nvdwb, nthree, nfour, nmany, next)

        endif

        if( newvdw > vdwcap ) goto 1000 ! reject - new energy is too high
        if( newext > vdwcap ) goto 1000 ! reject - new energy is too high
        !if( newvdw + newext > vdwcap ) then 
        !    write(uout,*)"rotate_molecules_seq(): rejected due to VdW overlap..."
        !    goto 1000 ! reject - new energy is too high
        !end if
        
        if( do_ewald_rcp ) call move_molecule_recip(ib, imol, newrcp, nrcp)!, job%lmoldata)

        !sum energies
        oldtotal = oldreal + oldrcp + oldvdw + oldthree + oldmany + oldext + oldmfa
        oldenth  = oldtotal + extpress * cfgs(ib)%vec%volume

        newtotal = newreal + newrcp + newvdw + newthree + newmany + newext + newmfa
        newenth  = newtotal + extpress * cfgs(ib)%vec%volume

        !TU: In general we need code here to set 'deltavb' for other order parameters, i.e. not COM.
        !TU: I understand this is unnecessary in this procedure for, e.g. volume as the order parameter, since
        !TU: the volume is unchanged by a translation move, and hence deltavb=0. But 'in general', here
        !TU: is where 'deltavb' should be set.

        !metropoolis
        deltav  = (newtotal - oldtotal)
        deltavb = deltavb + deltav * beta

        !TU: Update transition matrix if it is in use: the canonical (i.e, without biasing) probability is
        !TU: min(1,exp(-deltav*beta)).
        !TU: Note that this is only reached if the trial state is 'in bounds' for the order parameter range,
        !TU: and hence the transition matrix is only updated for in bounds trial states.
        if( fed % method == FED_TM .and. is_fed_com ) then
           
           call fed_tm_inc(  min( 1.0_wp, exp(-deltav*beta) )  )
           
        end if

        !TU: Reject the move if it leaves the window or takes us fuether from the window                          
        if( fed % is_window .and. is_fed_com ) then
           
           if( fed_window_reject() ) goto 1000
           
        end if

        if( deltavb > job%toler ) goto 1000 ! reject - new energy is too high
        if( deltavb < 0.0_wp .or. arg < exp(-deltavb) ) then

             !AB: update the COM difference for FED parameter
             if( is_fed_com ) then 
              
                 call fed_upd_com_dist2(ib)
              
                 !AB: bias is also updated if needed (e.g. WL)
                 call fed_param_hist_inc(ib,.true.)

             end if

             !accept update energies etc
             energytot(ib) = energytot(ib) + deltav
             enthalpytot(ib) = enthalpytot(ib) + (newenth - oldenth)
             virialtot(ib) = virialtot(ib) + (newvir - oldvir)
             energyreal(ib) = energyreal(ib) + (newreal - oldreal)
             energyvdw(ib) = energyvdw(ib) + (newvdw - oldvdw)
             energythree(ib) = energythree(ib) + (newthree - oldthree)
             energymany(ib) = energymany(ib) + (newmany - oldmany)
             energyext(ib) = energyext(ib) + (newext - oldext)
             energymfa(ib) = energymfa(ib) + (newmfa - oldmfa)

             no_mol_rots(typ) = no_mol_rots(typ) + 1
             successful_mol_rots = successful_mol_rots + 1

             if( do_ewald_rcp ) then
                 energyrcp(ib) = newrcp
                 call update_rcpsums(ib)
             endif

             !must recalc whole nbrlist as other atoms will be out of sync on accepting the move
             !if( job%uselist .and. job%lauto_nbrs .and. reset ) then
             if( reset ) then

                 !TU: Above the neighbour list for the atoms in the moved molecule have been updated.
                 !TU: Say atom 'i' is such an atom. We must still update the neighbour lists of all atoms
                 !TU: which are within the cut-off of 'i', i.e. all atoms within the (newly updated)
                 !TU: neighbour list of 'i'. The below procedure does this for all such atoms 'i'. If
                 !TU: this is not done then 'i' will 'see' the atoms in its local environment correctly,
                 !TU: but the atoms in the local environment may not see 'i'.
                 call update_mol_verlet_cell(ib, job%verletshell, job%shortrangecut, im)

             endif

             if(job%lmoldata) then

                 do im = 1, number_of_molecules

                     emolrealnonb(ib,im) = emolrealnonb(ib,im) + (nrealnonb(im) - orealnonb(im))
                     emolrealbond(ib,im) = emolrealbond(ib,im) + (nrealbond(im) - orealbond(im))
                     orcp(im) = emolrcp(ib,im)
                     emolrcp(ib,im) = nrcp(im)
                     emolvdwn(ib,im) = emolvdwn(ib,im) + (nvdwn(im) - ovdwn(im))
                     emolvdwb(ib,im) = emolvdwb(ib,im) + (nvdwb(im) - ovdwb(im))
                     emolthree(ib,im) = emolthree(ib,im) + (nthree(im) - othree(im))
                     emolfour(ib,im) = emolfour(ib,im) + (nfour(im) - ofour(im))
                     emolmany(ib,im) = emolmany(ib,im) + (nmany(im) - omany(im))
                     emolext(ib,im) = emolext(ib,im) + (next(im) - oext(im))

                     otot = orealnonb(im) + orealbond(im) + orcp(im) + ovdwn(im) + ovdwb(im)    &
                            + othree(im) + ofour(im) + omany(im) + oext(im)
                     ntot = nrealnonb(im) + nrealbond(im) + nrcp(im) + nvdwn(im) + nvdwb(im)    &
                            + nthree(im) + nfour(im) + nmany(im) + next(im)

                     emoltot(ib,im) = emoltot(ib,im) + (ntot - otot)

                 enddo

             endif

             cycle

        endif
        !else

1000    call restore_molecule_pos(ib, imol)  ! rejected-put atom back!!

        !AB: restore molecule NLs by resetting
        !if( reset ) call set_mol_nbrlist(ib, imol , job%shortrangecut, job%verletshell)
        if( reset ) call update_verlet_mol(ib, imol, job%verletshell, job%shortrangecut, reset)

        !AB: revert the attempted displacement in COM
        if( is_fed_com ) then 
         
            !call fed_upd_com_by(ib, icom, -rmove, weight)
            if( weight(1) > 0.0_wp ) call fed_upd_com_by(ib, 1, -rmove, weight(1))
            if( weight(2) > 0.0_wp ) call fed_upd_com_by(ib, 2, -rmove, weight(2))
         
            !AB: bias is also updated if needed (e.g. WL)
            call fed_param_hist_inc(ib,.false.)

        end if

        !endif

    enddo  ! end loop over molecules 

end subroutine

!> configurational sampling via the exchange in position of ions
!> charged particles are not allowed to swap
subroutine swap_atoms(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                      emolpair, emolang, emolfour, emolmany, emolext)

    use kinds_f90
    use field_module
    use cell_module
    use control_type
    use species_module
    use random_module, only : duni

    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), energyfour(:), &
                      energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                      energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                      emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                      emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                      emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                      emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                      emolmany(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules)

    integer :: im, i, jm, j, typ, ii, atypi, atypj, buf(4), numtype1, numtype2

    real(kind = wp) :: deltav,deltavb, otot, ntot,  &
         oldreal, oldvdw, oldthree, oldpair, oldang, oldfour, oldmany, oldext, oldmfa, oldtotal, oldenth, oldvir, &
         newreal, newvdw, newthree, newpair, newang, newfour, newmany, newext, newmfa, newtotal, newenth, newvir, &
         tmpreal, tmpvdw, tmpthree, tmppair, tmpang, tmpfour, tmpmany, tmpext, tmpmfa, tmpvir, arg

     ! temp vectors for molecular energies
    real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules),  &
                        ovdwn(number_of_molecules), ovdwb(number_of_molecules), opair(number_of_molecules), &
                        othree(number_of_molecules), oang(number_of_molecules), ofour(number_of_molecules), &
                        omany(number_of_molecules), oext(number_of_molecules), &
                        nrealnonb(number_of_molecules), nrealbond(number_of_molecules), &
                        nvdwn(number_of_molecules), nvdwb(number_of_molecules), npair(number_of_molecules), &
                        nthree(number_of_molecules), nang(number_of_molecules), nfour(number_of_molecules), &
                        nmany(number_of_molecules), next(number_of_molecules), &
                        trealnonb(number_of_molecules), trealbond(number_of_molecules), &
                        tvdwn(number_of_molecules), tvdwb(number_of_molecules), tpair(number_of_molecules), &
                        tthree(number_of_molecules), tang(number_of_molecules), tfour(number_of_molecules), &
                        tmany(number_of_molecules), tcrct(number_of_molecules), text(number_of_molecules)

    !JG: rcp variables required when charges are different
    real(kind = wp) :: oldrcp, newrcp, tmprcp, orcp(number_of_molecules), nrcp(number_of_molecules)
    real(kind = wp) :: chgi, chgj

    real(kind = wp) :: rand
    
    logical :: do_ewald_rcp, charges_differ

    do_ewald_rcp = .false.

    !JG: Default behvaiour is charges are identical
    charges_differ = .false.

    tmprcp = 0.0_wp

    orcp = 0.0_wp
    nrcp = 0.0_wp

    orealnonb = 0.0_wp
    orealbond = 0.0_wp
    ovdwn = 0.0_wp
    ovdwb = 0.0_wp
    opair = 0.0_wp
    othree = 0.0_wp
    oang = 0.0_wp
    ofour = 0.0_wp
    omany = 0.0_wp
    oext = 0.0_wp

    nrealnonb = 0.0_wp
    nrealbond = 0.0_wp
    nvdwn = 0.0_wp
    nvdwb = 0.0_wp
    npair = 0.0_wp
    nthree = 0.0_wp
    nang = 0.0_wp
    nfour = 0.0_wp
    nmany = 0.0_wp
    next = 0.0_wp

    trealnonb = 0.0_wp
    trealbond = 0.0_wp
    tvdwn = 0.0_wp
    tvdwb = 0.0_wp
    tpair = 0.0_wp
    tthree = 0.0_wp
    tang = 0.0_wp
    tfour = 0.0_wp
    tmany = 0.0_wp
    tcrct = 0.0_wp
    text = 0.0_wp

    if(.not.job%exchangebias) then
        ! simple swap
        i = 0

        attemptedswaps = attemptedswaps + 1

        ! must zero as accumulators
        oldreal = 0.0_wp
        oldvdw = 0.0_wp
        oldthree = 0.0_wp
        oldpair = 0.0_wp
        oldang =  0.0_wp
        oldfour = 0.0_wp
        oldmany = 0.0_wp
        oldext = 0.0_wp
        oldmfa = 0.0_wp
        oldenth = 0.0_wp
        oldvir = 0.0_wp

        tmpreal = 0.0_wp
        tmpvdw = 0.0_wp
        tmpthree = 0.0_wp
        tmppair = 0.0_wp
        tmpang =  0.0_wp
        tmpfour = 0.0_wp
        tmpmany = 0.0_wp
        tmpext = 0.0_wp
        tmpmfa = 0.0_wp
        tmpvir = 0.0_wp

        
        !TU: Old code for choosing type of swap move - replaced below
        !typ = int (duni() * job%numswap + 1) ! randomly select which pair to swap

        !TU: Select the type of atom swap we will perform - using the correct relative
        !TU: frequencies of each type of atom swap move
        rand = duni()
        do typ = 1, job%numswap            
            if(rand < atmswapfreq_thresh(typ)) exit
        end do

        !TU: Obsolete: Replace with references to cfgs(ib)%num_elemts; see 'findnumtype' code for detail
        call findnumtype(ib, swaptype1(typ), numtype1)
        call findnumtype(ib, swaptype2(typ), numtype2)

        !TU-: Note that if it is impossible to find a pair then this is flagged as an empty
        !TU-: atom move as opposed to a swap move. 
        !TU-: TO DO: Check the convention for gcmc and gibbs for both atoms and molecules regarding empty moves.
        if( numtype1 == 0 .or. numtype2 == 0 ) then

            empty_atm_moves = empty_atm_moves+1
            return

        end if

        !TU: select_atom will hang if there are no atoms either of the target species - check introduced above ensures this is case
        call select_atom(swaptype1(typ), ib, im, i)
        call select_atom(swaptype2(typ), ib, jm, j)

        attemptedswaps_atm(typ,ib) = attemptedswaps_atm(typ,ib) + 1

        chgi = cfgs(ib)%mols(im)%atms(i)%charge
        chgj = cfgs(ib)%mols(jm)%atms(j)%charge

        charges_differ = ( chgi /= chgj )
        do_ewald_rcp   = ( job%coultype(ib) == 1 .and. charges_differ )

        atypi = cfgs(ib)%mols(im)%atms(i)%atype
        atypj = cfgs(ib)%mols(jm)%atms(j)%atype

        !accumulate energy for atoms to be swapped
        !energy of first atom
        if (atypi /= ATM_TYPE_SEMI) then

            if (job%uselist) then

                call atom_energy(ib, im, i, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                            tmpreal, tmpvdw, tmpthree, tmppair, tmpang, tmpfour, tmpext, tmpmfa, tmpvir,  &
                            trealnonb, trealbond, tcrct, tvdwn, tvdwb, tpair, tthree, tang, tfour, text, &
                            charges_differ, .true.)

            else

                call atom_energy_nolist(ib, im, i, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut,  &
                             tmpreal, tmpvdw, tmpthree, tmppair, tmpang, tmpfour, tmpext, tmpmfa, tmpvir,  &
                             trealnonb, trealbond, tcrct, tvdwn, tvdwb, tpair, tthree, tang, tfour, text, &
                             charges_differ, .true.)

            end if

        else

            call atom_tersoff_energy(ib, im, i, job%shortrangecut, tmpmany, tmpvir)

        endif

        oldreal = oldreal + tmpreal
        oldvdw = oldvdw + tmpvdw
        oldthree = oldthree + tmpthree
        oldpair = oldpair + tmppair
        oldang = oldang + tmpang
        oldmany = oldmany + tmpmany
        oldfour = oldfour + tmpfour
        oldext = oldext + tmpext
        oldmfa = oldmfa + tmpmfa
        oldvir = oldvir + tmpvir

        do ii = 1, number_of_molecules

            orealnonb(ii) = orealnonb(ii) + trealnonb(ii)
            orealbond(ii) = orealbond(ii) + trealbond(ii)
            ovdwn(ii) = ovdwn(ii) + tvdwn(ii)
            ovdwb(ii) = ovdwb(ii) + tvdwb(ii)
            opair(ii) = opair(ii) + tpair(ii)
            othree(ii) = othree(ii) + tthree(ii)
            oang(ii) = oang(ii) + tang(ii)
            ofour(ii) = ofour(ii) + tfour(ii)
            omany(ii) = omany(ii) + tmany(ii)
            oext(ii) = oext(ii) + text(ii)

        enddo

        !energy of second atom
        if (atypj /= ATM_TYPE_SEMI) then

            if (job%uselist) then

                call atom_energy(ib, jm, j, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                             tmpreal, tmpvdw, tmpthree, tmppair, tmpang, tmpfour, tmpext, tmpmfa, tmpvir,  &
                             trealnonb, trealbond, tcrct, tvdwn, tvdwb, tpair, tthree, tang, tfour, text, &
                             charges_differ, .true.)

            else

                call atom_energy_nolist(ib, jm, j, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut,  &
                             tmpreal, tmpvdw, tmpthree, tmppair, tmpang, tmpfour, tmpext, tmpmfa, tmpvir,  &
                             trealnonb, trealbond, tcrct, tvdwn, tvdwb, tpair, tthree, tang, tfour, text, &
                             charges_differ, .true.)

            end if


        else

            call atom_tersoff_energy(ib, jm, j, job%shortrangecut, tmpmany, tmpvir)

        endif

        oldreal = oldreal + tmpreal
        oldvdw = oldvdw + tmpvdw
        oldthree = oldthree + tmpthree
        oldpair = oldpair + tmppair
        oldang = oldang + tmpang
        oldfour = oldfour + tmpfour
        oldmany = oldmany + tmpmany
        oldext = oldext + tmpext
        oldmfa = oldmfa + tmpmfa
        oldvir = oldvir + tmpvir

        do ii = 1, number_of_molecules

            orealnonb(ii) = orealnonb(ii) + trealnonb(ii)
            orealbond(ii) = orealbond(ii) + trealbond(ii)
            ovdwn(ii) = ovdwn(ii) + tvdwn(ii)
            ovdwb(ii) = ovdwb(ii) + tvdwb(ii)
            opair(ii) = opair(ii) + tpair(ii)
            othree(ii) = othree(ii) + tthree(ii)
            oang(ii) = oang(ii) + tang(ii)
            ofour(ii) = ofour(ii) + tfour(ii)
            omany(ii) = omany(ii) + tmany(ii)
            oext(ii) = oext(ii) + text(ii)

        enddo

        if (atypi == ATM_TYPE_METAL) call atom_metal_energy_full(ib, job%shortrangecut, oldmany, omany)

        oldtotal = oldreal + oldvdw + oldthree + oldpair + oldang + oldmany + oldfour + oldext + oldmfa
        oldenth  = oldtotal + extpress * cfgs(ib)%vec%volume

        !JG: Rather than add and remove components just swap the difference in charge
        if( do_ewald_rcp ) &
            call swap_atoms_recip(ib, im, i, jm, j, chgi, chgj, tmprcp)

        !swap atoms
        call swapatompositions(ib, im, i, jm, j)

        !broadcast the atom position to other nodes unless this is rep exchange calculation
!       if (.not.job%repexch) then
!           call broadcast_atom_type(ib, im,i)
!           call broadcast_atom_type(ib, jm,j)
!       endif

        newreal = 0.0_wp
        newvdw = 0.0_wp
        newthree = 0.0_wp
        newpair = 0.0_wp
        newang = 0.0_wp
        newfour = 0.0_wp
        newmany = 0.0_wp
        newext = 0.0_wp
        newmfa = 0.0_wp
        newenth = 0.0_wp
        newvir = 0.0_wp

        tmpvir = 0.0_wp
        tmpreal = 0.0_wp
        tmpvdw = 0.0_wp
        tmpthree = 0.0_wp
        tmppair = 0.0_wp
        tmpang =  0.0_wp
        tmpfour = 0.0_wp
        tmpmany = 0.0_wp
        tmpext = 0.0_wp
        tmpmfa = 0.0_wp

        !energy of first atom
        if (atypi /= ATM_TYPE_SEMI) then

            if (job%uselist) then

                call atom_energy(ib, im, i, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                             tmpreal, tmpvdw, tmpthree, tmppair, tmpang, tmpfour, tmpext, tmpmfa, tmpvir,  &
                             trealnonb, trealbond, tcrct, tvdwn, tvdwb, tpair, tthree, tang, tfour, text, &
                             charges_differ, .true.)

            else

                call atom_energy_nolist(ib, im, i, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut,  &
                             tmpreal, tmpvdw, tmpthree, tmppair, tmpang, tmpfour, tmpext, tmpmfa, tmpvir,  &
                             trealnonb, trealbond, tcrct, tvdwn, tvdwb, tpair, tthree, tang, tfour, text, &
                             charges_differ, .true.)

            end if


        else

            call atom_tersoff_energy(ib, im, i, job%shortrangecut, tmpmany, tmpvir)

        endif

        newreal = newreal + tmpreal
        newvdw = newvdw + tmpvdw
        newthree = newthree + tmpthree
        newpair = newpair + tmppair
        newang = newang + tmpang
        newfour = newfour + tmpfour
        newmany = newmany + tmpmany
        newext = newext + tmpext
        newmfa = newmfa + tmpmfa
        newvir = newvir + tmpvir

        do ii = 1, number_of_molecules

            nrealnonb(ii) = nrealnonb(ii) + trealnonb(ii)
            nrealbond(ii) = nrealbond(ii) + trealbond(ii)
            nvdwn(ii) = nvdwn(ii) + tvdwn(ii)
            nvdwb(ii) = nvdwb(ii) + tvdwb(ii)
            npair(ii) = npair(ii) + tpair(ii)
            nthree(ii) = nthree(ii) + tthree(ii)
            nang(ii) = nang(ii) + tang(ii)
            nfour(ii) = nfour(ii) + tfour(ii)
            nmany(ii) = nmany(ii) + tmany(ii)
            next(ii) = next(ii) + text(ii)

        enddo

        !energy of first atom
        if (atypj /= ATM_TYPE_SEMI) then

            if (job%uselist) then

                call atom_energy(ib, jm, j, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                             tmpreal, tmpvdw, tmpthree, tmppair, tmpang, tmpfour, tmpext, tmpmfa, tmpvir,  &
                             trealnonb, trealbond, tcrct, tvdwn, tvdwb, tpair, tthree, tang, tfour, text, &
                             charges_differ, .true.)

            else

                call atom_energy_nolist(ib, jm, j, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut,  &
                             tmpreal, tmpvdw, tmpthree, tmppair, tmpang, tmpfour, tmpext, tmpmfa, tmpvir,  &
                             trealnonb, trealbond, tcrct, tvdwn, tvdwb, tpair, tthree, tang, tfour, text, &
                             charges_differ, .true.)

            end if


        else

            call atom_tersoff_energy(ib, jm, j, job%shortrangecut, tmpmany, tmpvir)

        endif

        newreal = newreal + tmpreal
        newvdw = newvdw + tmpvdw
        newthree = newthree + tmpthree
        newpair = newpair + tmppair
        newang = newang + tmpang
        newfour = newfour + tmpfour
        newmany = newmany + tmpmany
        newext = newext + tmpext
        newmfa = newmfa + tmpmfa
        newvir = newvir + tmpvir

        do ii = 1, number_of_molecules

            nrealnonb(ii) = nrealnonb(ii) + trealnonb(ii)
            nrealbond(ii) = nrealbond(ii) + trealbond(ii)
            nvdwn(ii) = nvdwn(ii) + tvdwn(ii)
            nvdwb(ii) = nvdwb(ii) + tvdwb(ii)
            npair(ii) = npair(ii) + tpair(ii)
            nthree(ii) = nthree(ii) + tthree(ii)
            nang(ii) = nang(ii) + tang(ii)
            nfour(ii) = nfour(ii) + tfour(ii)
            nmany(ii) = nmany(ii) + tmany(ii)
            next(ii) = next(ii) + text(ii)

        enddo

        if (atypj == ATM_TYPE_METAL) call atom_metal_energy_full(ib, job%shortrangecut, newmany, nmany)

        newtotal = newreal + newvdw + newthree + newpair + newang + newmany + newfour + newext
        newenth  = newtotal + extpress * cfgs(ib)%vec%volume

        deltav  = newtotal - oldtotal + tmprcp
        deltavb = deltav * beta

        arg = duni()

        if(deltavb < job%toler .and. arg < exp(-deltavb)) then

            energytot(ib) = energytot(ib) + deltav
            enthalpytot(ib) = enthalpytot(ib) + (newenth - oldenth)
            virialtot(ib) = virialtot(ib) + (newvir - oldvir)
            energyreal(ib) = energyreal(ib) + (newreal - oldreal)
            energyrcp(ib) = energyrcp(ib) + tmprcp
            energyvdw(ib) = energyvdw(ib) + (newvdw - oldvdw)
            energypair(ib) = energypair(ib) + (newpair - oldpair)
            energythree(ib) = energythree(ib) + (newthree - oldthree)
            energyang(ib) = energyang(ib) + (newang - oldang)
            energymany(ib) = energymany(ib) + (newmany - oldmany)
            energyext(ib) = energyext(ib) + (newext - oldext)
            energymfa(ib) = energymfa(ib) + (newmfa - oldmfa)

            successfulswaps = successfulswaps + 1
            
            successfulswaps_atm(typ,ib) = successfulswaps_atm(typ,ib) + 1

            do im = 1, number_of_molecules
                emolrealnonb(ib,im) = emolrealnonb(ib,im) + (nrealnonb(im) - orealnonb(im))
                emolrealbond(ib,im) = emolrealbond(ib,im) + (nrealbond(im) - orealbond(im))
                emolvdwn(ib,im) = emolvdwn(ib,im) + (nvdwn(im) - ovdwn(im))
                emolvdwb(ib,im) = emolvdwb(ib,im) + (nvdwb(im) - ovdwb(im))
                emolpair(ib,im) = emolpair(ib,im) + (npair(im) - opair(im))
                emolthree(ib,im) = emolthree(ib,im) + (nthree(im) - othree(im))
                emolang(ib,im) = emolang(ib,im) + (nang(im) - oang(im))
                emolfour(ib,im) = emolfour(ib,im) + (nfour(im) - ofour(im))
                emolmany(ib,im) = emolmany(ib,im) + (nmany(im) - omany(im))
                emolext(ib,im) = emolext(ib,im) + (next(im) - oext(im))

                otot = orealnonb(im) + orealbond(im) + ovdwn(im) + ovdwb(im) + opair(im)   &
                       + othree(im) + oang(im) + ofour(im) + omany(im) + oext(im)
                ntot = nrealnonb(im) + nrealbond(im) + nvdwn(im) + nvdwb(im) + npair(im)   &
                       + nthree(im) + nang(im) + nfour(im) + nmany(im) + next(im)

                emoltot(ib,im) = emoltot(ib,im) + (ntot - otot)

            enddo

            if(atypi == ATM_TYPE_METAL .and. atypj ==ATM_TYPE_METAL) call copy_density(ib)

            if( do_ewald_rcp ) call update_rcpsums(ib)

        else

            !failed swap atoms back !
            call swapatompositions(ib, im, i, jm, j) 

        endif

    endif

end subroutine

!> configurational sampling via the exchange in position of molecule
subroutine swap_molecules(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                      emolpair, emolang, emolfour, emolmany, emolext)

    use kinds_f90
    use field_module
    use cell_module
    use control_type
    use species_module
    use random_module, only : duni

    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), &
                      energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                      energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                      emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                      emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                      emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                      emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                      emolmany(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules)

    integer :: im, jm, typ, ii, buf(4)

    real(kind = wp) :: deltav,deltavb, otot, ntot,  &
       oldreal, oldvdw, oldthree, oldpair, oldang, oldmany, oldext, oldmfa, oldtotal, oldenth, oldvir, &
       newreal, newvdw, newthree, newpair, newang, newmany, newext, newmfa, newtotal, newenth, newvir, &
       tmpreal, tmpvdw, tmpthree, tmppair, tmpang, tmpmany, tmpext, tmpmfa, tmpvir, arg


     ! temp vectors for molecular energies
     real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules),  &
                        ovdwn(number_of_molecules), ovdwb(number_of_molecules), opair(number_of_molecules), &
                        othree(number_of_molecules), oang(number_of_molecules), ofour(number_of_molecules), &
                        omany(number_of_molecules),  oext(number_of_molecules), &
                        nrealnonb(number_of_molecules), nrealbond(number_of_molecules), &
                        nvdwn(number_of_molecules), nvdwb(number_of_molecules), npair(number_of_molecules), &
                        nthree(number_of_molecules), nang(number_of_molecules), nfour(number_of_molecules), &
                        nmany(number_of_molecules),  next(number_of_molecules), &
                        trealnonb(number_of_molecules), trealbond(number_of_molecules), &
                        tvdwn(number_of_molecules), tvdwb(number_of_molecules), tpair(number_of_molecules), &
                        tthree(number_of_molecules), tang(number_of_molecules), tfour(number_of_molecules), &
                        tmany(number_of_molecules), text(number_of_molecules)

    logical :: accept, overlap

    orealnonb = 0.0_wp
    orealbond = 0.0_wp
    ovdwn = 0.0_wp
    ovdwb = 0.0_wp
    opair = 0.0_wp
    othree = 0.0_wp
    oang = 0.0_wp
    ofour = 0.0_wp
    omany = 0.0_wp
    oext = 0.0_wp

    nrealnonb = 0.0_wp
    nrealbond = 0.0_wp
    nvdwn = 0.0_wp
    nvdwb = 0.0_wp
    npair = 0.0_wp
    nthree = 0.0_wp
    nang = 0.0_wp
    nfour = 0.0_wp
    nmany = 0.0_wp
    next = 0.0_wp

    trealnonb = 0.0_wp
    trealbond = 0.0_wp
    tvdwn = 0.0_wp
    tvdwb = 0.0_wp
    tpair = 0.0_wp
    tthree = 0.0_wp
    tang = 0.0_wp
    tfour = 0.0_wp
    tmany = 0.0_wp
    text = 0.0_wp

    if(.not.job%exchangebias) then
        ! simple swap
        attemptedswaps = attemptedswaps + 1

        ! must zero as accumulators
        oldreal = 0.0_wp
        oldvdw = 0.0_wp
        oldthree = 0.0_wp
        oldpair = 0.0_wp
        oldang =  0.0_wp
        oldmany = 0.0_wp
        oldext = 0.0_wp
        oldmfa = 0.0_wp
        oldenth = 0.0_wp
        oldvir = 0.0_wp

        tmpreal = 0.0_wp
        tmpvdw = 0.0_wp
        tmpthree = 0.0_wp
        tmppair = 0.0_wp
        tmpang =  0.0_wp
        tmpmany = 0.0_wp
        tmpext = 0.0_wp
        tmpmfa = 0.0_wp
        tmpvir = 0.0_wp

        !TU-: Here is where the type of molecule swap move to be performed is chosen.
        !TU-: Note that currently all possible types of swap move have the same likelihood
  
        typ = int (duni() * job%numswap + 1) ! randomly select which pair to swap 

        !TU: Check that there are molecules to swap
        if (      cfgs(ib)%mtypes(swaptype1(typ))%num_mols == 0  &
             .or. cfgs(ib)%mtypes(swaptype2(typ))%num_mols == 0 ) then

            empty_mol_moves = empty_mol_moves+1

            return

        end if

        !TU: Select the molecule. Note that select_molecule will hang if there are no molecules
        !TU: belonging to the target species. The check above ensures this won't happen
        call select_molecule(swaptype1(typ), ib, im)
        call select_molecule(swaptype2(typ), ib, jm)


        !store molecules
        call store_molecule_pos(ib, im)
        call store_molecule_pos(ib, jm)

        !accumulate energy for atoms to be swapped
        !get initial energy
        if(job%uselist) then

            call molecule_energy(ib, im, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                     tmpreal, tmpvdw, tmpthree, tmpmany, tmpext, tmpmfa, tmpvir,  &
                     trealnonb, trealbond, tvdwn, tvdwb, tthree, tfour, tmany, text)

        else

            call molecule_energy_nolist(ib, im, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, &
                     tmpreal, tmpvdw, tmpthree, tmpmany, tmpext, tmpmfa, tmpvir,  &
                     trealnonb, trealbond, tvdwn, tvdwb, tthree, tfour, tmany, text)

        endif

        oldreal = oldreal + tmpreal
        oldvdw = oldvdw + tmpvdw
        oldthree = oldthree + tmpthree
        oldpair = oldpair + tmppair
        oldang = oldang + tmpang
        oldmany = oldmany + tmpmany
        oldext = oldext + tmpext
        oldmfa = oldmfa + tmpmfa
        oldvir = oldvir + tmpvir

        do ii = 1, number_of_molecules

            orealnonb(ii) = orealnonb(ii) + trealnonb(ii)
            orealbond(ii) = orealbond(ii) + trealbond(ii)
            ovdwn(ii) = ovdwn(ii) + tvdwn(ii)
            ovdwb(ii) = ovdwb(ii) + tvdwb(ii)
            opair(ii) = opair(ii) + tpair(ii)
            othree(ii) = othree(ii) + tthree(ii)
            oang(ii) = oang(ii) + tang(ii)
            ofour(ii) = ofour(ii) + tfour(ii)
            omany(ii) = omany(ii) + tmany(ii)
            oext(ii) = oext(ii) + text(ii)

        enddo

        !energy of second atom
        if(job%uselist) then

            call molecule_energy(ib, jm, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                     tmpreal, tmpvdw, tmpthree, tmpmany, tmpext, tmpmfa, tmpvir,  &
                     trealnonb, trealbond, tvdwn, tvdwb, tthree, tfour, tmany, text)

        else

            call molecule_energy_nolist(ib, jm, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, &
                     tmpreal, tmpvdw, tmpthree, tmpmany, tmpext, tmpmfa, tmpvir,  &
                     trealnonb, trealbond, tvdwn, tvdwb, tthree, tfour, tmany, text)

        endif

        oldreal = oldreal + tmpreal
        oldvdw = oldvdw + tmpvdw
        oldthree = oldthree + tmpthree
        oldpair = oldpair + tmppair
        oldang = oldang + tmpang
        oldmany = oldmany + tmpmany
        oldext = oldext + tmpext
        oldmfa = oldmfa + tmpmfa
        oldvir = oldvir + tmpvir

        do ii = 1, number_of_molecules

            orealnonb(ii) = orealnonb(ii) + trealnonb(ii)
            orealbond(ii) = orealbond(ii) + trealbond(ii)
            ovdwn(ii) = ovdwn(ii) + tvdwn(ii)
            ovdwb(ii) = ovdwb(ii) + tvdwb(ii)
            opair(ii) = opair(ii) + tpair(ii)
            othree(ii) = othree(ii) + tthree(ii)
            oang(ii) = oang(ii) + tang(ii)
            ofour(ii) = ofour(ii) + tfour(ii)
            omany(ii) = omany(ii) + tmany(ii)
            oext(ii) = oext(ii) + text(ii)

        enddo


        oldtotal = oldreal + oldvdw + oldthree + oldpair + oldang + oldmany + oldext + oldmfa
        oldenth  = oldtotal + extpress * cfgs(ib)%vec%volume

        !swap atoms
        call swapmolpositions(ib, im, jm)

        if(job%uselist) then

            call set_mol_nbrlist(ib, im , job%shortrangecut, job%verletshell)
            call set_mol_nbrlist(ib, jm , job%shortrangecut, job%verletshell)

        endif

        newenth = 0.0_wp
        newvir = 0.0_wp
        newreal = 0.0_wp
        newvdw = 0.0_wp
        newthree = 0.0_wp
        newpair = 0.0_wp
        newang = 0.0_wp
        newmany = 0.0_wp
        newext = 0.0_wp
        newmfa = 0.0_wp

        tmpvir = 0.0_wp
        tmpreal = 0.0_wp
        tmpvdw = 0.0_wp
        tmpthree = 0.0_wp
        tmppair = 0.0_wp
        tmpang =  0.0_wp
        tmpmany = 0.0_wp
        tmpext = 0.0_wp
        tmpmfa = 0.0_wp

        !energy of first atom
        if(job%uselist) then

            call molecule_energy(ib, im, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                     tmpreal, tmpvdw, tmpthree, tmpmany, tmpext, tmpmfa, tmpvir,  &
                     trealnonb, trealbond, tvdwn, tvdwb, tthree, tfour, tmany, text)

        else

            call molecule_energy_nolist(ib, im, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, &
                     tmpreal, tmpvdw, tmpthree, tmpmany, tmpext, tmpmfa, tmpvir,  &
                     trealnonb, trealbond, tvdwn, tvdwb, tthree, tfour, tmany, text)

        endif

        newreal = newreal + tmpreal
        newvdw = newvdw + tmpvdw
        newthree = newthree + tmpthree
        newpair = newpair + tmppair
        newang = newang + tmpang
        newmany = newmany + tmpmany
        newext = newext + tmpext
        newmfa = newmfa + tmpmfa
        newvir = newvir + tmpvir

        do ii = 1, number_of_molecules

            nrealnonb(ii) = nrealnonb(ii) + trealnonb(ii)
            nrealbond(ii) = nrealbond(ii) + trealbond(ii)
            nvdwn(ii) = nvdwn(ii) + tvdwn(ii)
            nvdwb(ii) = nvdwb(ii) + tvdwb(ii)
            npair(ii) = npair(ii) + tpair(ii)
            nthree(ii) = nthree(ii) + tthree(ii)
            nang(ii) = nang(ii) + tang(ii)
            nfour(ii) = nfour(ii) + tfour(ii)
            nmany(ii) = nmany(ii) + tmany(ii)
            next(ii) = next(ii) + text(ii)

        enddo

        !energy of first atom
        if(job%uselist) then

            call molecule_energy(ib, jm, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                     tmpreal, tmpvdw, tmpthree, tmpmany, tmpext, tmpmfa, tmpvir,  &
                     trealnonb, trealbond, tvdwn, tvdwb, tthree, tfour, tmany, text)

        else

            call molecule_energy_nolist(ib, jm, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, &
                     tmpreal, tmpvdw, tmpthree, tmpmany, tmpext, tmpmfa, tmpvir,  &
                     trealnonb, trealbond, tvdwn, tvdwb, tthree, tfour, tmany, text)

        endif

        newreal = newreal + tmpreal
        newvdw = newvdw + tmpvdw
        newthree = newthree + tmpthree
        newpair = newpair + tmppair
        newang = newang + tmpang
        newmany = newmany + tmpmany
        newext = newext + tmpext
        newmfa = newmfa + tmpmfa
        newvir = newvir + tmpvir

        do ii = 1, number_of_molecules

            nrealnonb(ii) = nrealnonb(ii) + trealnonb(ii)
            nrealbond(ii) = nrealbond(ii) + trealbond(ii)
            nvdwn(ii) = nvdwn(ii) + tvdwn(ii)
            nvdwb(ii) = nvdwb(ii) + tvdwb(ii)
            npair(ii) = npair(ii) + tpair(ii)
            nthree(ii) = nthree(ii) + tthree(ii)
            nang(ii) = nang(ii) + tang(ii)
            nfour(ii) = nfour(ii) + tfour(ii)
            nmany(ii) = nmany(ii) + tmany(ii)
            next(ii) = next(ii) + text(ii)

        enddo

        newtotal = newreal + newvdw + newthree + newpair + newang + newmany + newext + newmfa
        newenth  = newtotal + extpress * cfgs(ib)%vec%volume

        !recip space energy of molecules + add to totals

        deltav = newtotal - oldtotal
        deltavb = deltav * beta

        arg = duni()

        if(deltavb < job%toler .and. arg < exp(-deltavb)) then

            energytot(ib) = energytot(ib) + deltav
            enthalpytot(ib) = enthalpytot(ib) + (newenth - oldenth)
            virialtot(ib) = virialtot(ib) + (newvir - oldvir)
            energyreal(ib) = energyreal(ib) + (newreal - oldreal)
            energyvdw(ib) = energyvdw(ib) + (newvdw - oldvdw)
            energypair(ib) = energypair(ib) + (newpair - oldpair)
            energythree(ib) = energythree(ib) + (newthree - oldthree)
            energyang(ib) = energyang(ib) + (newang - oldang)
            energymany(ib) = energymany(ib) + (newmany - oldmany)
            energyext(ib) = energyext(ib) + (newext - oldext)
            energymfa(ib) = energymfa(ib) + (newmfa - oldmfa)

            successfulswaps = successfulswaps + 1

            do im = 1, number_of_molecules
                emolrealnonb(ib,im) = emolrealnonb(ib,im) + (nrealnonb(im) - orealnonb(im))
                emolrealbond(ib,im) = emolrealbond(ib,im) + (nrealbond(im) - orealbond(im))
                emolvdwn(ib,im) = emolvdwn(ib,im) + (nvdwn(im) - ovdwn(im))
                emolvdwb(ib,im) = emolvdwb(ib,im) + (nvdwb(im) - ovdwb(im))
                emolpair(ib,im) = emolpair(ib,im) + (npair(im) - opair(im))
                emolthree(ib,im) = emolthree(ib,im) + (nthree(im) - othree(im))
                emolang(ib,im) = emolang(ib,im) + (nang(im) - oang(im))
                emolfour(ib,im) = emolfour(ib,im) + (nfour(im) - ofour(im))
                emolmany(ib,im) = emolmany(ib,im) + (nmany(im) - omany(im))
                emolext(ib,im) = emolext(ib,im) + (next(im) - oext(im))

                otot = orealnonb(im) + orealbond(im) + ovdwn(im) + ovdwb(im) + opair(im)   &
                       + othree(im) + oang(im) + ofour(im) + omany(im) + oext(im)
                ntot = nrealnonb(im) + nrealbond(im) + nvdwn(im) + nvdwb(im) + npair(im)   &
                       + nthree(im) + nang(im) + nfour(im) + nmany(im) + next(im)

                emoltot(ib,im) = emoltot(ib,im) + (ntot - otot)
            enddo

            !TU-: If a molecule swap move is accepted then all atoms have their neighbour
            !TU-: lists updated. This is overkill: updating the neighbour lists of the atoms
            !TU-: local to the swapped molecules is better.           
  if(job%uselist) call setnbrlists(ib, job%shortrangecut, job%verletshell,  job%linklist) 

            !copy phase factors

        else

            !failed swap atoms back - just reset positions
            call restore_molecule_pos(ib, im)
            call restore_molecule_pos(ib, jm)

            if(job%uselist) then

                call set_mol_nbrlist(ib, im , job%shortrangecut, job%verletshell)
                call set_mol_nbrlist(ib, jm , job%shortrangecut, job%verletshell)

            endif

        endif

    endif

end subroutine

!scp: scan is variation of move_molecules
!> does a rigid molecule move
!> Note that PSMC is assumed to be enabled if the optional argument 'ib_pas' is present. In this case
!> box 'ib_pas' is updated as the passive box while 'ib' is updated as the active box.
subroutine scan_molecules(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                      emolpair, emolang, emolfour, emolmany, emolext, ib_pas,scan_count)

    use kinds_f90
    use species_module
    use control_type
    use cell_module
    use field_module
    use random_module, only : duni

    use constants_module, only : uout, FED_PAR_DIST2, FED_PAR_PSMC, FED_PAR_PSMC_HS, FED_TM !, FED_PAR_DIST1
    use fed_calculus_module, only : fed, fed_tm_inc
    use fed_order_module, only : fed_upd_com_by, fed_param_dbias, fed_param_dbias_psmc, fed_upd_com_dist2, &
                                 fed_param_hist_inc, fed_window_reject

    use psmc_module, only : real2frac, frac2real, set_mol_u_from_pos

    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), &
                      energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                      energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                      emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                      emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                      emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                      emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                      emolmany(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules)

    !TU: Optional variable: the passive box to update analogously to the active box ib. If this argument
    !TU: is not present then there is no update to a passive box. If this argument is present then PSMC is assumed.
    integer, intent(in), optional :: ib_pas

    integer :: k, i, j, typ, im, ia, icom1, icom2, ngas, niter, scan_count, na, nb, nstp(3), nstep

    real(kind=wp) :: rmove(3), weight(2),ra(3), smove(3), sstp(3)

    real(kind=wp) :: arg, distmax, deltav, deltavb, otot, ntot, oldreal, oldrcp, oldvdw, oldthree, &
                     oldmany, oldext, oldmfa, oldtotal, oldenth, oldvir, newreal, newrcp, newvdw, newthree, &
                     newmany, newext, newmfa, newtotal, newenth, newvir

     !temp vectors for molecular energies
    real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules), orcp(number_of_molecules), &
                       ovdwn(number_of_molecules), ovdwb(number_of_molecules), &
                       othree(number_of_molecules),  ofour(number_of_molecules), &
                       omany(number_of_molecules), oext(number_of_molecules), &
                       nrealnonb(number_of_molecules), nrealbond(number_of_molecules), nrcp(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules),  &
                       nthree(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules), next(number_of_molecules)

    logical :: mol_found, reset, do_ewald_rcp, atom_out, is_fed_com, is_fed_param_psmc

    !TU: Relevant variables for the passive box for PSMC (variables which end in '_pas')
    integer :: typ_pas

    real(kind=wp) :: rmove_pas(3)

    real(kind=wp) :: otot_pas, ntot_pas, oldreal_pas, oldrcp_pas, oldvdw_pas, oldthree_pas, oldpair_pas, oldang_pas, &
                     oldfour_pas, oldmany_pas, oldext_pas, oldmfa_pas, oldtotal_pas, oldenth_pas, oldvir_pas, &
                     newreal_pas, newrcp_pas, newvdw_pas, newthree_pas, newpair_pas, newang_pas, newfour_pas, &
                     newmany_pas, newext_pas, newmfa_pas, newtotal_pas, newenth_pas, newvir_pas

    real(kind = wp) :: orealnonb_pas(number_of_molecules), orealbond_pas(number_of_molecules), &
                       orcp_pas(number_of_molecules), ovdwn_pas(number_of_molecules), &
                       ovdwb_pas(number_of_molecules), opair_pas(number_of_molecules), &
                       othree_pas(number_of_molecules),  oang_pas(number_of_molecules), &
                       ofour_pas(number_of_molecules), omany_pas(number_of_molecules), &
                       ocrct_pas(number_of_molecules), oext_pas(number_of_molecules), &
                       nrealnonb_pas(number_of_molecules), nrealbond_pas(number_of_molecules), &
                       nrcp_pas(number_of_molecules), nvdwn_pas(number_of_molecules), &
                       nvdwb_pas(number_of_molecules), npair_pas(number_of_molecules), &
                       nthree_pas(number_of_molecules), nang_pas(number_of_molecules), &
                       nfour_pas(number_of_molecules), nmany_pas(number_of_molecules), &
                       ncrct_pas(number_of_molecules), next_pas(number_of_molecules)

    logical :: ib_pas_on, reset_pas, do_ewald_pas, atom_out_pas

    !AB: this is a new, much faster implementation of the random molecule selection where
    !AB: molecules are chosen from only those molecular species that contain movable molecules;
    !AB: the list of molecule moves is checked against all the molecular species to ensure 
    !AB: that all the movable molecules can be definitely found (see montecarlo_module::setupmc);

    call select_species_molmov(ib, typ, k, im, molmovtyp, mol_found)
    scan_count = scan_count+1
    if( .not.mol_found ) then

!SCP: no point in running scan simulation if no molecules to scan
        if( cfgs(ib)%mtypes(typ)%num_mols < 1  ) then
        write(uout,*)"move_molecules(): movable molecule type ",typ," is empty..."
        elseif (typ /= cfgs(ib)%mols(im)%mol_label .or. typ < 1 ) then
        write(uout,*)"move_molecules(): no movable molecule found..."
        endif
        !        call flush(uout)
        call cry(uout,'', &
        "ERROR: no molecules to scan!!! Add scan molecule to CONFIG",999)


        empty_mol_moves = empty_mol_moves+1

        return
    endif

#ifdef DEBUG
!debugging
    if( im < 1 .or. typ < 1 .or. k < 1 .or. im > cfgs(ib)%num_mols .or. im > cfgs(ib)%mxmol ) then
        write(*,*)"move_molecules(*SOS*): found movable molecule ",typ,&
                  " with index ",im," (move type: ",k,")"
        write(*,*)"move_molecules(*SOS*): in mol. species  containing ",cfgs(ib)%mtypes(typ)%num_mols," mols out of ",&
                  cfgs(ib)%mtypes(typ)%max_mols," (max)"
        write(*,*)"move_molecules(*SOS*): in configuration containing ",cfgs(ib)%num_mols," mols out of ",cfgs(ib)%mxmol," (max)"
        STOP
        return
    endif
#endif

    reset = .false.
    reset_pas = .false.

    !AB: reduce the number of repetitive checks
    do_ewald_rcp = .false.
    do_ewald_pas = .false.

    ib_pas_on = present(ib_pas)
!scp need to check - set to false
ib_pas_on = .false.

    deltav  = 999999.9_wp
    deltavb = 99.9_wp
!scp working the step length shouldn't be done each step - problem is the number of configs (nconfigs)
      do na = 1,3
      RA(NA) = 0.0_wp
      do nb = 1,3
! box to be scanned - below is for full cell
!      RA(NA) = cfgs(ib)%vec%latvector(nb,na)*cfgs(ib)%vec%latvector(nb,na) + RA(NA)
!     modify in control - using "slab" : job % scan_svec() 
      RA(NA) = cfgs(ib)%vec%latvector(nb,na)*cfgs(ib)%vec%latvector(nb,na) + RA(NA)
      enddo
      RA(NA) = SQRT(RA(NA))*job%scan_svec(NA)
      nstp(na) = ra(na)/job % scan_step
      sstp(na) = ra(na)/nstp(na)
      enddo
      nstep= job % num_scan_start + scan_count -1
       !could save time - but if initial position is poor then this may confuse.
       ! if(nstep.eq.0) then
       !  deltav  = 0.0_wp
       !  deltavb = 0.0_wp
       ! goto 1000
       ! endif
      if(nstep.lt.nstp(3)) then
      smove(1) = 0.0_wp
      smove(2) = 0.0_wp
      smove(3) = (sstp(3)*nstep)/ra(3)
      else if(nstep.lt.nstp(3)*nstp(2)) then
      smove(1) = 0.0_wp
      i=(nstep/(nstp(3)))
      smove(2) = (sstp(2) * i)/ra(2)
      i = mod(nstep,nstp(3))
      smove(3) = (sstp(3)*i)/ra(3)
      else
      i=(nstep/(nstp(3)*nstp(2)))
      smove(1) = (sstp(1) * i)/ra(1)
      j=mod(nstep,(nstp(3)*nstp(2)))
      i=(j/(nstp(3)))
      smove(2) = (sstp(2) * i)/ra(2)
      i = mod(j,nstp(3))
      smove(3) = (sstp(3)*i)/ra(3)
      endif
    ! displacement
    do na = 1,3
    rmove(na) =0.0_wp
    do nb=1,3
    rmove(na) = rmove(na) + smove(nb) * cfgs(ib)%vec%latvector(na,nb)*job%scan_svec(na) * job % molmove(1,k)
    enddo
    enddo

    oldenth = 0.0_wp
    oldvir = 0.0_wp
    oldreal = 0.0_wp
    !oldrcp = 0.0_wp
    oldvdw = 0.0_wp
    oldthree = 0.0_wp
    oldmany = 0.0_wp
    oldext = 0.0_wp
    oldmfa = 0.0_wp

    oldrcp = energyrcp(ib)
    newrcp = oldrcp

    newenth = 0.0_wp
    newvir = 0.0_wp
    newreal = 0.0_wp
    !newrcp = 0.0_wp
    newvdw = 0.0_wp
    newthree = 0.0_wp
    newmany = 0.0_wp
    newext = 0.0_wp
    newmfa = 0.0_wp

    is_fed_param_psmc = ( fed%par_kind == FED_PAR_PSMC .or. fed%par_kind == FED_PAR_PSMC_HS )

    do_ewald_rcp = ( job%coultype(ib) == 1 .and. cfgs(ib)%mols(im)%has_charge )

    call store_molecule_pos(ib,im)

    distmax = distance_mol_max(typ)   !distance moved for this type
    total_mol_moves = total_mol_moves + 1
    attempted_mol_moves(typ) = attempted_mol_moves(typ) + 1

    !get initial energy
    if(job%uselist) then

        call molecule_energy(ib, im, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                     oldreal, oldvdw, oldthree, oldmany, oldext, oldmfa, oldvir,  &
                     orealnonb, orealbond, ovdwn, ovdwb, othree, ofour, omany, oext)

    else

        call molecule_energy_nolist(ib, im, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, &
                     oldreal, oldvdw, oldthree, oldmany, oldext, oldmfa, oldvir,  &
                     orealnonb, orealbond, ovdwn, ovdwb, othree, ofour, omany, oext)

    endif

    !AB: check for any overlaps missed in the past
    if( oldvdw > vdwcap ) then 
        write(uout,*)"move_molecules(): stuck due to VdW overlap(s)!..", &
                     oldvdw," > ",vdwcap," (",im,") - need to increase VDW ecap?" !,", ",i,")"
        !call flush(uout)
        
        goto 1000 ! reject - new energy is too high
    !end if

    !AB: check for any overlaps missed in the past
    else if( oldext > vdwcap ) then 
        write(uout,*)"move_molecules(): stuck due to EXT overlap(s)!..", &
                     oldext," > ",vdwcap," (",im,") - need to increase VDW ecap?" !,", ",i,")"
        !call flush(uout)
        
        goto 1000 ! reject - new energy is too high
    end if

    !TU: Do the same as above for the passive box in PSMC

    if( ib_pas_on ) then

        typ_pas = cfgs(ib_pas)%mols(im)%mol_label

        do_ewald_pas = ( job%coultype(ib_pas) == 1 .and. cfgs(ib_pas)%mols(im)%has_charge )

        oldenth_pas = 0.0_wp
        oldvir_pas = 0.0_wp
        oldreal_pas = 0.0_wp
        !oldrcp_pas = energyrcp(ib_pas)
        oldvdw_pas = 0.0_wp
        oldthree_pas = 0.0_wp
        oldpair_pas = 0.0_wp
        oldang_pas = 0.0_wp
        oldfour_pas = 0.0_wp
        oldmany_pas = 0.0_wp
        oldext_pas = 0.0_wp
        oldmfa_pas = 0.0_wp

        oldrcp_pas = energyrcp(ib_pas)
        newrcp_pas = oldrcp_pas

        newenth_pas = 0.0_wp
        newvir_pas = 0.0_wp
        newreal_pas = 0.0_wp
        !newrcp_pas = 0.0_wp
        newvdw_pas = 0.0_wp
        newthree_pas = 0.0_wp
        newpair_pas = 0.0_wp
        newang_pas = 0.0_wp
        newfour_pas = 0.0_wp
        newmany_pas = 0.0_wp
        newext_pas = 0.0_wp
        newmfa_pas = 0.0_wp

        nrcp_pas = 0.0_wp
        orcp_pas = 0.0_wp

        call store_molecule_pos(ib_pas,im)
                                                                                             
        if(job%uselist) then

            call molecule_energy(ib_pas, im, job%coultype(ib_pas), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                         oldreal_pas, oldvdw_pas, oldthree_pas, oldmany_pas, oldext_pas, oldmfa_pas, oldvir_pas,  &
                         orealnonb_pas, orealbond_pas, ovdwn_pas, ovdwb_pas, othree_pas, ofour_pas, omany_pas, oext_pas)

        else

            call molecule_energy_nolist(ib_pas, im, job%coultype(ib_pas), job%dielec, job%vdw_cut, job%shortrangecut, &
                         oldreal_pas, oldvdw_pas, oldthree_pas, oldmany_pas, oldext_pas, oldmfa_pas, oldvir_pas,  &
                         orealnonb_pas, orealbond_pas, ovdwn_pas, ovdwb_pas, othree_pas, ofour_pas, omany_pas, oext_pas)

        endif

    end if

    atom_out = .false.
    atom_out_pas = .false.
!scp
!    rmove(1) = (2.0_wp * duni() - 1.0_wp) * distmax * job % molmove(1,k) !molmovtyp(typ))
!    rmove(2) = (2.0_wp * duni() - 1.0_wp) * distmax * job % molmove(2,k) !molmovtyp(typ))
!    rmove(3) = (2.0_wp * duni() - 1.0_wp) * distmax * job % molmove(3,k) !molmovtyp(typ))

    call move_molecule_by(ib, im, rmove, atom_out)

    !if( do_ewald_rcp ) call move_molecule_recip(ib, im, newrcp, nrcp, job%lmoldata)

    !TU: Do the same as above for the passive box in PSMC
    if( ib_pas_on ) then

        !TU: Calculate the move to make on the molecule in the passive box
        !TU: - use the equivalent change in fractional coordinates in the passive box
        rmove_pas = rmove
        call real2frac( cfgs(ib)%vec, rmove_pas(1), rmove_pas(2), rmove_pas(3) )
        call frac2real( cfgs(ib_pas)%vec, rmove_pas(1), rmove_pas(2), rmove_pas(3) )

        call move_molecule_by(ib_pas, im, rmove_pas, atom_out_pas)

        !if( do_ewald_pas ) &
        !    call move_molecule_recip(ib_pas, im, newrcp_pas, nrcp_pas, job%lmoldata)

    end if

    !AB: advance randmon number selection to preserve the random sequence
    arg = duni()

    deltavb = 0.0_wp

    !AB: prepare for FED sampling - before any possible rejection!

    icom1 = cfgs(ib)%mols(im)%idcom

    !AB: activate FED if needed (checking fed%par_kind is sufficient)
    is_fed_com = ( fed%par_kind == FED_PAR_DIST2 .and. icom1 /= 0 )

    !AB: rearrange this for other FED parameters, not related to COM:s
    !is_fed_com = ( is_fed_com .and. icom1 /= 0 )

    weight(:) = 0.0_wp

    if( is_fed_com ) then

        if( icom1 > 0 ) then
            !AB: the case of whole molecule belonging to same COM

            weight(icom1) = cfgs(ib)%mols(im)%mass

        else
            !AB: the case of atom subset(s) belonging to same or different COM:s

            do ia = 1,cfgs(ib)%mols(im)%natom

               icom2 = cfgs(ib)%mols(im)%atms(ia)%idcom

               if( icom2 > 0 ) then

                   weight(icom2) = weight(icom2) + cfgs(ib)%mols(im)%atms(ia)%mass

               end if

            end do

            icom1 = -icom1

        endif

        !AB: update the relevant COM for FED parameter
        !call fed_upd_com_by(ib, icom, rmove, weight)
        if( weight(1) > 0.0_wp ) call fed_upd_com_by(ib, 1, rmove, weight(1))
        if( weight(2) > 0.0_wp ) call fed_upd_com_by(ib, 2, rmove, weight(2))

        deltavb = fed_param_dbias(ib,cfgs(ib))

        !AB: stay within the defined range
        if( deltavb > job%toler ) goto 1000

    end if

    !if in slit, make sure all atoms are still within the cell, otherwise reject the move!
    !TU: Note that atom_out_pas will be .false. if PSMC is not in use
    if( atom_out .or. atom_out_pas ) goto 1000 ! reject - atom got outside the slit

    !AB: use NL or not
    if( job%uselist ) then

        !check NL if in auto-update mode
        if( job%lauto_nbrs ) then

            !AB: much quicker way of updating NL
            call update_verlet_mol(ib, im, job%verletshell, job%shortrangecut, reset)
            if( reset ) nbrlist_resets(ib) = nbrlist_resets(ib)+1

            !call check_verlet_mol(ib, im, job%verletshell, reset)
            !if( reset ) then 
            !    nbrlist_resets(ib) = nbrlist_resets(ib)+1
            !    call set_mol_nbrlist(ib, im, job%shortrangecut, job%verletshell)
            !end if

        endif

        call molecule_energy(ib, im, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                     newreal, newvdw, newthree, newmany, newext, newmfa, newvir,  &
                     nrealnonb, nrealbond, nvdwn, nvdwb, nthree, nfour, nmany, next)

    else

        call molecule_energy_nolist(ib, im, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, &
                     newreal, newvdw, newthree, newmany, newext, newmfa, newvir,  &
                     nrealnonb, nrealbond, nvdwn, nvdwb, nthree, nfour, nmany, next)

    endif

    !TU: Do the same for the passive box in PSMC
    if( ib_pas_on ) then

        if( job%uselist ) then

            !TU: Do the same as above for the passive box in PSMC
            if( job%lauto_nbrs ) then

                !AB: much quicker way of updating NL
                call update_verlet_mol(ib_pas, im, job%verletshell, job%shortrangecut, reset_pas)

                !call check_verlet_mol(ib_pas, im, job%verletshell, reset_pas)
                !if( reset_pas ) call set_mol_nbrlist(ib_pas, im, job%shortrangecut, job%verletshell)

            end if

            call molecule_energy(ib_pas, im, job%coultype(ib_pas), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                         newreal_pas, newvdw_pas, newthree_pas, newmany_pas, newext_pas, newmfa_pas, newvir_pas,  &
                         nrealnonb_pas, nrealbond_pas, nvdwn_pas, nvdwb_pas, nthree_pas, nfour_pas, nmany_pas, next_pas)

        else

            call molecule_energy_nolist(ib_pas, im, job%coultype(ib_pas), job%dielec, job%vdw_cut, job%shortrangecut, &
                         newreal_pas, newvdw_pas, newthree_pas, newmany_pas, newext_pas, newmfa_pas, newvir_pas,  &
                         nrealnonb_pas, nrealbond_pas, nvdwn_pas, nvdwb_pas, nthree_pas, nfour_pas, nmany_pas, next_pas)

        endif

    end if

    if( newvdw > vdwcap ) goto 1000 ! reject - new energy is too high
    if( newext > vdwcap ) goto 1000 ! reject - new energy is too high
    !if( newvdw + newext > vdwcap ) then 
    !    write(uout,*)"move_molecules(): rejected due to VdW overlap..."
    !    goto 1000 ! reject - new energy is too high
    !end if
    
    if( do_ewald_rcp ) call move_molecule_recip(ib, im, newrcp, nrcp)!, job%lmoldata)

    !sum the energies
    oldtotal = oldreal + oldrcp + oldvdw + oldthree + oldmany + oldext + oldmfa
    oldenth  = oldtotal + extpress * cfgs(ib)%vec%volume

    newtotal = newreal + newrcp + newvdw + newthree + newmany + newext + newmfa
    newenth  = newtotal + extpress * cfgs(ib)%vec%volume

    !TU: Do the same for the passive box in PSMC
    if( ib_pas_on ) then

        if( do_ewald_pas ) &
            call move_molecule_recip(ib_pas, im, newrcp_pas, nrcp_pas)!, job%lmoldata)

        oldtotal_pas = oldreal_pas + oldrcp_pas + oldvdw_pas + oldthree_pas + oldmany_pas + oldext_pas + oldmfa_pas
        oldenth_pas  = oldtotal_pas + extpress * cfgs(ib_pas)%vec%volume

        newtotal_pas = newreal_pas + newrcp_pas + newvdw_pas + newthree_pas + newmany_pas + newext_pas + newmfa_pas
        newenth_pas  = newtotal_pas + extpress * cfgs(ib_pas)%vec%volume

    end if

    !TU: If PSMC order parameter is in use then accordingly bias the acceptance probability
    if( is_fed_param_psmc ) then

        !TU: deltavb should be 0 at this point if we are using the PSMC order parameter.
        !TU: The difference in the bias between the new state and the current state is given by
        !TU: fed_param_dbias_psmc(energy1, energy2), where energy1 and energy2 are the NEW
        !TU: energies of phases 1 and 2.

        !TU: NB: oldtotal, oldtotal_pas, newtotal, and newtotal_pas are the energies of the ATOM
        !TU: which has just been moved, not the energy of the system. However energy1 and energy2
        !TU: should be TOTAL.
        if( ib == 1) then

            deltavb = fed_param_dbias_psmc( energytot(1) + newtotal - oldtotal, &
                                            energytot(2) + newtotal_pas - oldtotal_pas )

        else

            !TU: WARNING - I assume that ib==2 if ib/=1. Something has gone horribly
            !TU: wrong if this is not the case at this point, hence I don't even check.

            deltavb = fed_param_dbias_psmc(energytot(1) + newtotal_pas - oldtotal_pas, &
                                           energytot(2) + newtotal - oldtotal )

        end if

        !TU: stay within the defined range
        if( deltavb > job%toler ) goto 1000

    end if

    !TU: In general we need code here to set 'deltavb' for other order parameters, i.e. not COM or PSMC.
    !TU: I understand this is unnecessary in this procedure for, e.g. volume as the order parameter, since
    !TU: the volume is unchanged by a translation move, and hence deltavb=0. But 'in general', here
    !TU: is where 'deltavb' should be set.

    !metropoolis
    deltav  = (newtotal - oldtotal)
    deltavb = deltavb + deltav * beta

    !TU: Update transition matrix if it is in use: the canonical (i.e, without biasing) probability is
    !TU: min(1,exp(-deltav*beta)).
    !TU: Note that this is only reached if the trial state is 'in bounds' for the order parameter range,
    !TU: and hence the transition matrix is only updated for in bounds trial states.
    if( fed % method == FED_TM ) then
        
        call fed_tm_inc(  min( 1.0_wp, exp(-deltav*beta) )  )

    end if

    !TU: Reject the move if it leaves the window or takes us further from the window                          
    if( fed % is_window ) then
       
       if( fed_window_reject() ) goto 1000
       
    end if

    if( deltavb > job%toler ) goto 1000 ! reject - new energy is too high
    !scp: rejecting move - for new iteration step
    goto 1000
! leave rest in tact for the moment

    if( deltavb < 0.0_wp .or. arg < exp(-deltavb) ) then

        !AB: update the COM difference for FED parameter
        if( is_fed_com ) then 
        
            call fed_upd_com_dist2(ib)
        
            !AB: bias is also updated if needed (e.g. WL)
            call fed_param_hist_inc(ib,.true.)
        
        else if( is_fed_param_psmc ) then
        
            !TU: update the FED histogram & bias if needed (e.g. WL) for PSMC
        
            call fed_param_hist_inc(ib,.true.)

        end if

        !make sure image convention is still ok
        !call pbc_molecule(ib, im)

        !accept update energies etc
        energytot(ib) = energytot(ib) + deltav
        enthalpytot(ib) = enthalpytot(ib) + (newenth - oldenth)
        virialtot(ib) = virialtot(ib) + (newvir - oldvir)
        !energyrcp(ib) = newrcp
        energyreal(ib) = energyreal(ib) + (newreal - oldreal)
        energyvdw(ib) = energyvdw(ib) + (newvdw - oldvdw)
        energythree(ib) = energythree(ib) + (newthree - oldthree)
        energymany(ib) = energymany(ib) + (newmany - oldmany)
        energyext(ib) = energyext(ib) + (newext - oldext)
        energymfa(ib) = energymfa(ib) + (newmfa - oldmfa)

        no_mol_moves(typ) = no_mol_moves(typ) + 1
        successful_mol_moves = successful_mol_moves + 1

        !debugging
        !if( abs(energyrcp(ib)).lt. 1.0e-10_wp ) write(*,*)'move_molecules():: accepted with energyrcp(ib) = ',energyrcp(ib)

        if( do_ewald_rcp ) then
            energyrcp(ib) = newrcp
            call update_rcpsums(ib)
        endif

        !must recalc whole nbrlist as other atoms will be out of sync on accepting the move
        !if( job%uselist .and. job%lauto_nbrs .and. reset ) then
        if( reset ) then

            call update_verlet_cell(ib, job%verletshell, job%shortrangecut, reset)

            !call setnbrlists(ib, job%shortrangecut, job%verletshell,  job%linklist, .false.)
            !call set_rzero(ib)

        endif

        !TU: Do the same as above for the passive box in PSMC (for energies only - not counters),
        !TU: and update PSMC displacements for both boxes (note that the lattice site positions are unchanged during an atom move).
        if( ib_pas_on ) then

            energytot(ib_pas) = energytot(ib_pas) + (newtotal_pas - oldtotal_pas)
            enthalpytot(ib_pas) = enthalpytot(ib_pas) + (newenth_pas - oldenth_pas)
            virialtot(ib_pas) = virialtot(ib_pas) + (newvir_pas - oldvir_pas)
            !energyrcp(ib_pas) = energyrcp(ib_pas) + (newrcp_pas - oldrcp_pas)
            energyreal(ib_pas) = energyreal(ib_pas) + (newreal_pas - oldreal_pas)
            energyvdw(ib_pas) = energyvdw(ib_pas) + (newvdw_pas - oldvdw_pas)
            energythree(ib_pas) = energythree(ib_pas) + (newthree_pas - oldthree_pas)
            energymany(ib_pas) = energymany(ib_pas) + (newmany_pas - oldmany_pas)
            energyext(ib_pas) = energyext(ib_pas) + (newext_pas - oldext_pas)
            energymfa(ib_pas) = energymfa(ib_pas) + (newmfa_pas - oldmfa_pas)

            if( do_ewald_pas ) then
                energyrcp(ib_pas) = newrcp_pas
                call update_rcpsums(ib_pas)
            endif

            if( reset_pas ) then

                call update_verlet_cell(ib_pas, job%verletshell, job%shortrangecut, reset_pas)

                !call setnbrlists(ib_pas, job%shortrangecut, job%verletshell,  job%linklist, .false.)
                !call set_rzero(ib_pas)

            endif

            call set_mol_u_from_pos(ib, im)
            call set_mol_u_from_pos(ib_pas, im)

        end if

         if(job%lmoldata) then

             do im = 1, number_of_molecules

                 emolrealnonb(ib,im) = emolrealnonb(ib,im) + (nrealnonb(im) - orealnonb(im))
                 emolrealbond(ib,im) = emolrealbond(ib,im) + (nrealbond(im) - orealbond(im))
                 orcp(im) = emolrcp(ib,im)
                 emolrcp(ib,im) = nrcp(im)
                 emolvdwn(ib,im) = emolvdwn(ib,im) + (nvdwn(im) - ovdwn(im))
                 emolvdwb(ib,im) = emolvdwb(ib,im) + (nvdwb(im) - ovdwb(im))
                 emolthree(ib,im) = emolthree(ib,im) + (nthree(im) - othree(im))
                 emolfour(ib,im) = emolfour(ib,im) + (nfour(im) - ofour(im))
                 emolmany(ib,im) = emolmany(ib,im) + (nmany(im) - omany(im))
                 emolext(ib,im) = emolext(ib,im) + (next(im) - oext(im))

                 otot = orealnonb(im) + orealbond(im) + orcp(im) + ovdwn(im) + ovdwb(im)    &
                        + othree(im) + ofour(im) + omany(im) + oext(im)
                 ntot = nrealnonb(im) + nrealbond(im) + nrcp(im) + nvdwn(im) + nvdwb(im)    &
                        + nthree(im) + nfour(im) + nmany(im) + next(im)

                 emoltot(ib,im) = emoltot(ib,im) + (ntot - otot)

                 !TU: Do the same as above for the passive box in PSMC
                 if( ib_pas_on ) then

                     emolrealnonb(ib_pas,im) = emolrealnonb(ib_pas,im) + (nrealnonb_pas(im) - orealnonb_pas(im))
                     emolrealbond(ib_pas,im) = emolrealbond(ib_pas,im) + (nrealbond_pas(im) - orealbond_pas(im))
                     orcp_pas(im) = emolrcp(ib_pas,im)
                     emolrcp(ib_pas,im) = nrcp_pas(im)
                     emolvdwn(ib_pas,im) = emolvdwn(ib_pas,im) + (nvdwn_pas(im) - ovdwn_pas(im))
                     emolvdwb(ib_pas,im) = emolvdwb(ib_pas,im) + (nvdwb_pas(im) - ovdwb_pas(im))
                     emolthree(ib_pas,im) = emolthree(ib_pas,im) + (nthree_pas(im) - othree_pas(im))
                     emolfour(ib_pas,im) = emolfour(ib_pas,im) + (nfour_pas(im) - ofour_pas(im))
                     emolmany(ib_pas,im) = emolmany(ib_pas,im) + (nmany_pas(im) - omany_pas(im))
                     emolext(ib_pas,im) = emolext(ib_pas,im) + (next_pas(im) - oext_pas(im))

                     otot_pas = orealnonb_pas(im) + orealbond_pas(im) + orcp_pas(im) + ovdwn_pas(im) + ovdwb_pas(im)    &
                            + othree_pas(im) + ofour_pas(im) + omany_pas(im) + oext_pas(im)
                     ntot_pas = nrealnonb_pas(im) + nrealbond_pas(im) + nrcp_pas(im) + nvdwn_pas(im) + nvdwb_pas(im)    &
                            + nthree_pas(im) + nfour_pas(im) + nmany_pas(im) + next_pas(im)

                     emoltot(ib_pas,im) = emoltot(ib_pas,im) + (ntot_pas - otot_pas)
 
                 end if

             enddo

         endif

         return

    endif
    !else

1000    continue


!scp:deltav print out data
        write(uscan,'(i10,3f15.5,3x,3f15.5,f18.7,1x,e20.7)')nstep,(rmove(j),j=1,3),                            &
        cfgs(ib)%mols(im)%atms(1)%rpos(1),cfgs(ib)%mols(im)%atms(1)%rpos(2),cfgs(ib)%mols(im)%atms(1)%rpos(3), &
        deltav/ job%energyunit,exp(-deltav*beta)


        call restore_molecule_pos(ib, im)  ! rejected => put molecule back!!

        !AB: restore molecule NLs by resetting
        !if( reset ) call set_mol_nbrlist(ib, im , job%shortrangecut, job%verletshell)
        if( reset ) call update_verlet_mol(ib, im, job%verletshell, job%shortrangecut, reset)

        !TU: Do the same as above for the passive box in PSMC
        if( ib_pas_on ) then

            call restore_molecule_pos(ib_pas, im)

            if( reset_pas ) &
                call update_verlet_mol(ib_pas, im, job%verletshell, job%shortrangecut, reset_pas)
                !call set_mol_nbrlist(ib_pas, im , job%shortrangecut, job%verletshell)

        end if

        !AB: revert the attempted displacement in COM
        if( is_fed_com ) then 
        
            !call fed_upd_com_by(ib, icom, -rmove, weight)
            if( weight(1) > 0.0_wp ) call fed_upd_com_by(ib, 1, -rmove, weight(1))
            if( weight(2) > 0.0_wp ) call fed_upd_com_by(ib, 2, -rmove, weight(2))
        
            !AB: bias is also updated if needed (e.g. WL)
            call fed_param_hist_inc(ib,.false.)
        
        else if( is_fed_param_psmc ) then
        
            !TU: update the FED histogram & bias if needed (e.g. WL)
            !TU: but revert the parameter state (value & id/index)
        
            call fed_param_hist_inc(ib,.false.)

        end if

    !endif

end subroutine


end module 
