! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! ***************************************************************************

!> write out an error message with its code and terminate the execution
subroutine error(kode)

!AB: to avoid "dead-end freezing" due to a silent stop in the failng proccess this routine 
!AB: must be called by EVERY PROCESS only after the error status has been gathered, because 
!AB: MPI_FINALISE(ierr) in exit_mpi_comms() MUST BE CALLED by EVERYONE to stop due to error gracefully

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 subroutine for printing error messages and bringing about a
! controlled termination of the program
!
! copyright - daresbury laboratory
! author    - i.t.todorov june 2004
! addapted for dl_monte
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

     use constants_module, only : uout
     use comms_mpi_module, only : master, is_parallel, idnode, exit_mpi_comms, gsync

     implicit None

     integer, intent( in    ) :: kode

     !if (idnode == 0) then
     if( master ) then

     write(uout,'(/,/,1x,a,i5)') 'DL_MONTE terminated due to error ', kode

     ! codes 1 - 99 are for errors associated with controls (CONTROL file)

     ! codes 21 - 30 are for errors due to inconsistencies between cell_type, coultype and such (CONTROL & FIELD files)
     ! codes 31 - 50 are for errors associated with slit & MFA initialisation (planar pore, see slit_module.f90)
     ! codes 51 - 60 are for errors associated with FED input (see fed_interface_module.f90)

     ! codes 100 - 200 are for errors about molecular topology and force-FIELD (potentials) parameter set up (FIELD file)

     if      (kode ==    1) Then

         write(uout,'(/,/,1x,a)') 'error - word_2_real failure'

     else if (kode ==    2) Then

        write(uout,'(/,/,1x,a)') 'error - failed to read title in CONTROL file'

     else if (kode ==    3) Then

        write(uout,'(/,/,1x,a)') 'error - unrecognised directive in CONTROL file'

     else if (kode ==    4) Then

        write(uout,'(/,/,1x,a)') 'error - problem reading control file'

     else if (kode ==    5) Then

        write(uout,'(/,/,1x,a)') 'error - expecting cutoff next'

     else if (kode ==    6) Then

        write(uout,'(/,/,1x,a)') 'error - unknown energy unit'

     else if (kode ==    7) Then

        write(uout,'(/,/,1x,a)') 'error - end of CONTROL file (no start directive found)'

     else if (kode ==    8) Then

        write(uout,'(/,/,1x,a)') 'error - temperature not set'

     else if (kode ==    9) Then

        write(uout,'(/,/,1x,a)') "error - absent or incomplete 'use' directive in CONTROL (missing 'finish'?)"

     else if (kode ==    10) Then

        write(uout,'(/,/,1x,a)') 'error - unrecognised use directive'

     else if (kode ==    11) Then

        write(uout,'(/,/,1x,a)') 'error - incorrect move directive'

     else if (kode ==    12) Then

        write(uout,'(/,/,1x,a)') 'error - incorrect specification for ewald'

     else if (kode ==    13) Then

        write(uout,'(/,/,1x,a)') 'error - incorrect specification for shift'

     else if (kode ==    14) Then

        write(uout,'(/,/,1x,a)') 'error - ensemble keyword has already been used'

     else if (kode ==    15) Then

        write(uout,'(/,/,1x,a)') 'error - incorrect ensemble has been given'

     else if (kode ==    16) Then

        write(uout,'(/,/,1x,a)') 'error - the number of pre-heating steps must be < equilibration steps'

     else if (kode ==    17) Then

        write(uout,'(/,/,1x,a)') 'error - frequency for atom movements is required'

     else if (kode ==    18) Then

        write(uout,'(/,/,1x,a)') 'error - frequency for molecular movements is required'

     else if (kode ==    19) Then

        write(uout,'(/,/,1x,a)') 'error - frequency for molecular/atomic rotations is required'

     else if (kode ==    20) Then

        write(uout,'(/,/,1x,a)') 'error - both semigrand ensemble for atoms and molecules is not allowed'

     ! codes 21 - 30 are for errors due to inconsistencies between cell_type, coultype and such (CONTROL & FIELD files)

     else if (kode ==    21) Then
        ! AB: for 3D-bulk coultype MUST BE greater than or equal -1
        ! AB: for 2D-slit coultype MUST BE less    than or equal 0

        write(uout,'(/,5(/,1x,a))') &
        'error - inconsistency between the requested type of simulation cell "slit" and Coulomb treatment:', &
        '        for 3D-bulk (normal geometry, XYZ-PBC), "cell_type" >-1, "coultype" MUST BE >=-1, otherwise', &
        '        for 2D-slit (planar geometry,  XY-PBC), "cell_type" < 0, "coultype" MUST BE <= 0', &
        '        *if 3D-bulk is meant, make sure keyword "slit" is NOT present', &
        '        *if 2D-slit is meant, make sure at least one valid directive "slit" IS present'

     else if (kode ==    22) Then
        ! AB: coultype == 0 -> NO electrostatics whatsoever, i.e. no charges, no charged walls!
        ! AB: MUST BE complemented by slit_type greater than -10 (see constants_module)

        write(uout,'(/,2(/,1x,a),a)') &
        'error - inconsistency between the requested type of simulation cell "slit" and Coulomb treatment:', &
        '        "coultype" = 0 -> NO electrostatics (no charges, no charged walls!),', &
        ' then "slit_type" MUST BE > -10 (or vice versa)'

     else if (kode ==    23) Then
        ! AB: coultype < 0 -> Coulomb interactions MUST BE treated one way or another, provided there are charges!
        ! AB: MUST BE complemented by slit_type less than -9

        write(uout,'(/,2(/,1x,a),a)') &
        'error - inconsistency between the requested type of simulation cell "slit" and Coulomb treatment:', &
        '        "coultype" < 0 -> electrostatics (if charges present!)', &
        ' MUST BE treated one way or another, then "slit_type" MUST BE <= -10'

     else if (kode ==    24) Then
        ! AB: mfa_type == 0 & coultype == -2 (or less) -> NO MFA correction in slit, i.e. pure "explicit" in-box Coulomb 
        ! AB: MUST BE complemented by slit_type = -10/-20/-30

        write(uout,'(/,2(/,1x,a),a)') &
        'error - inconsistency between the requested type of simulation cell "slit" and Coulomb treatment:', &
        '        "coultype" = -2 & "mfa" = 0 -> pure "explicit" electrostatics (if charges present!)', &
        ' in-box only, then "slit_type" MUST BE = -10/-20/-30'

     else if (kode ==    25) Then
        ! AB: slit_type == -10/-20/-30 -> NO MFA correction in slit, "direct" in-box Coulomb 
        ! AB: MUST BE complemented by mfa_type == 0 & coultype == -2 (or less)

        write(uout,'(/,2(/,1x,a),a)') &
        'error - inconsistency between the requested type of simulation cell "slit" and Coulomb treatment:', &
        '        "slit_type" = -10/-20/-30 -> pure "explicit" electrostatics (if charges present!)', &
        ' in-box only, then "coultype" MUST BE = -2 & "mfa" = 0'

     else if (kode ==    26) Then
        ! AB: slit_type < -10/-20/-30 -> MFA correction in slit beyond Coulomb cut-off
        ! AB: MUST BE complemented by abs(mfa_type) in [0,..,3] & coultype == -2 (or less)

        write(uout,'(/,2(/,1x,a),a)') &
        'error - inconsistency between the requested type of simulation cell "slit" and Coulomb treatment:', &
        '        "slit_type" < -10/-20/-30 -> "explicit" electrostatics with MFA correction', &
        ' (if charges present!), then "coultype" MUST BE = -2 & "mfa" in [0,...,3]'

     else if (kode ==    27) Then
        ! AB: slit_type < -1?/-2?/-3? -> MFA correction in slit beyond Coulomb cut-off
        ! AB: MUST BE complemented by abs(mfa_type) == ? & coultype == -2 (or less)

        write(uout,'(/,2(/,1x,a),a)') &
        'error - inconsistency between the requested type of simulation cell "slit" and Coulomb treatment:', &
        '        "slit_type" = -1?/-2?/-3? -> "explicit" electrostatics with MFA correction', &
        ' (if charges present!), then "coultype" MUST BE = -2 & "|mfa|" == ?'

     else if (kode ==    28) Then

        write(uout,'(/,2(/,1x,a))') & 
        'error - the switches for 3D-bulk/2D-slit are inconsistent:', &
        '        "cell_type" < 0 [>= 0] but [not] "is_slit"'

     else if (kode ==    29) Then

        write(uout,'(/,3(/,1x,a))') & 
        'error - inconsistency between FIELD and CONTROL files:', &
        '        slit with hard walls cannot be used with any external potential'

     ! codes 31 - 50 are for errors associated with slit & MFA calculus (planar pore, see slit_module.f90)

     else if (kode ==    31) Then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate memory for charge Z-distributions (MFA)'

     else if (kode ==    32) Then

        write(uout,'(/,/,1x,a)') 'error - failed to de-allocate memory of charge Z-distributions (MFA)'

     else if (kode ==    33) Then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate memory for potential Z-distributions (MFA)'

     else if (kode ==    34) Then

        write(uout,'(/,/,1x,a)') 'error - failed to de-allocate memory of potential Z-distributions (MFA)'

     else if (kode ==    35) Then

        write(uout,'(/,/,1x,a)') "error - no valid 'sample zden' directive found in CONTROL (for slit MFA)"

     ! codes 51 - 60 are for errors associated with FED input (see fed_interface_module.f90)

     else if (kode ==    51) Then

        write(uout,'(/,/,1x,a)') "error - failed to read FED input in 'use fed ...' block in CONTROL file"

     else if (kode ==    99) Then

        write(uout,'(/,/,1x,a)') 'error - Gibbs ensemble requires at least 2 configurations in CONFIG'

     ! codes 100 - 200 are for errors about molecular topology and force-FIELD (potentials) parameter set up (FIELD file)

     else if (kode ==   100) Then

        write(uout,'(/,/,1x,a)') 'error - unknown directive found in FIELD file (scanFIELD)'

     else if (kode ==   101 ) Then

        write(uout,'(/,/,1x,a)') 'error - unknown directive found in FIELD file'

     else if (kode ==   102 ) Then

        write(uout,'(/,/,1x,a)') 'error - could not find molecule keyword in FIELD file'

     else if (kode ==   103 ) Then

        write(uout,'(/,/,1x,a)') 'error - expected molecule keyword next'

     else if (kode ==   104 ) Then

        write(uout,'(/,/,1x,a)') 'error - expected nummols keyword next'

     else if (kode ==   105 ) Then

        write(uout,'(/,/,1x,a)') 'error - expected atoms keyword next'

     else if (kode ==   106 ) Then

        write(uout,'(/,/,1x,a)') 'error -  atoms in FIELD and config do not match'

     else if (kode ==   107) Then

        write(uout,'(/,/,1x,a)') 'error - molecule name in FIELD does not have a unique type'

     else if (kode ==   108) Then

        write(uout,'(/,/,1x,a)') 'error - CONFIG site name does not have a unique type (not found in FIELD)'

     else if (kode ==   109) Then

        write(uout,'(/,/,1x,a)') 'error while reading bonds in FIELD file'

     else if (kode ==   110) Then

        write(uout,'(/,/,1x,a)') 'error while reading angles in FIELD file'

     else if (kode ==   111) Then

        write(uout,'(/,/,1x,a)') 'error while trying to read potentials or close keyword'

     else if (kode ==   112) Then

        write(uout,'(/,/,1x,a)') 'error - expected atoms keyword'

     else if (kode ==   113) Then

        write(uout,'(/,/,1x,a)') 'error - whilst reading unique sites/atoms'

     else if (kode ==   114) Then

        write(uout,'(/,/,1x,a)') 'error - whilst reading bonded potentials'

     else if (kode ==   115) Then

        write(uout,'(/,/,1x,a)') 'error - unrecognised bond potential'

     else if (kode ==   116) Then

        write(uout,'(/,/,1x,a)') 'error - atom in bond potential not in unique atom list'

     else if (kode ==   117) Then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate memory for bond potentials'

     else if (kode ==   118) Then

        write(uout,'(/,/,1x,a)') 'error - n-m potential parameter 2 less than parameter 3'

     else if (kode ==   119) Then

        write(uout,'(/,/,1x,a)') 'error - n-m potential: r0 > cutoff'

     else if (kode ==   120) Then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate memory for bond list'

     else if (kode ==   121) Then

        write(uout,'(/,/,1x,a)') 'error - total no of bonds is exceeded'

     else if (kode ==   122) Then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate memory for vdw potentials'

     else if (kode ==   123) Then

        write(uout,'(/,/,1x,a)') 'error - whilst reading vdw potentials'

     else if (kode ==   124) Then

        write(uout,'(/,/,1x,a)') 'error - unrecognised vdw potential'

     else if (kode ==   125) Then

        write(uout,'(/,/,1x,a)') 'error - atom in vdw potential not in unique atom list'

     else if (kode ==   126) Then

        write(uout,'(/,/,1x,a)') 'error - a bond has been broken in energy calculation'

     else if (kode ==   127) Then

        write(uout,'(/,/,1x,a)') 'error - max number of neighbours exceeded'

     else if (kode ==   128) Then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate memory to angle potentials'

     else if (kode ==   129) Then

        write(uout,'(/,/,1x,a)') 'error - too many angles'

     else if (kode ==   130) Then

        write(uout,'(/,/,1x,a)') 'error - whilst reading angle potentials'

     else if (kode ==   131) Then

        write(uout,'(/,/,1x,a)') 'error - three body bond broken '

     else if (kode ==   132) Then

        write(uout,'(/,/,1x,a)') 'error - wrong directive reading potentials'

     else if (kode ==   133) Then

        write(uout,'(/,/,1x,a)') "error - maximum no of three-body triplets exceeded. Try increasing 'maxthbnbrs' in CONTROL"

     else if (kode ==   134) Then

        write(uout,'(/,/,1x,a)') 'error - cavity bias dimensions must be greater than zero'

     else if (kode ==   135) Then

        write(uout,'(/,/,1x,a)') 'error - cavity bias radius must be greater than zero'

     else if (kode == 136) then

        write(uout,'(/,/,1x,a)') 'error - cavity bias error in program'

     else if (kode == 137) then

        write(uout,'(/,/,1x,a)') 'error - expected moltypes keyword next'

     else if (kode == 138) then

        write(uout,'(/,/,1x,a)') 'error - wrong element type found'

     else if (kode == 139) then

        write(uout,'(/,/,1x,a)') 'error - trying to set wrong element type'

     else if (kode == 140) then

        write(uout,'(/,/,1x,a)') 'error - unrecognised angle potential'

     else if (kode == 141) then

        write(uout,'(/,/,1x,a)') 'error - an angle bond has broken'    

     else if (kode == 142) then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate memory for metal potentials'

     else if (kode == 143) then

        write(uout,'(/,/,1x,a)') 'error - unrecognised metal potential '

     else if (kode == 144) then

        write(uout,'(/,/,1x,a)') 'error - atom in metal potential not in unique atom list'

     else if (kode == 145) then

        write(uout,'(/,/,1x,a)') 'error - too many metal potentials specified'

     else if (kode == 146) then

        write(uout,'(/,/,1x,a)') 'error - metal interaction already given'

     else if (kode == 147) then

        write(uout,'(/,/,1x,a)') 'error - problem reading metal potentials'

     else if (kode == 148) then

        write(uout,'(/,/,1x,a)') 'error - error in generating metal tables'

     else if (kode == 149) then

        write(uout,'(/,/,1x,a)') 'error - incorrect keyword for eAM potentials '

     else if (kode == 150) then

        write(uout,'(/,/,1x,a)') 'error - eAM table: atom 1 not found '

     else if (kode == 151) then

        write(uout,'(/,/,1x,a)') 'error - eAM table: atom 2 not found '

     else if (kode == 152) then

        write(uout,'(/,/,1x,a)') 'error - MAXMeSH must be >= ngrid + 4'

     else if (kode == 153) then

        write(uout,'(/,/,1x,a)') 'error - cutoff to short for eAM table '

     else if (kode == 154) then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate memory for tersoff potentials'

     else if (kode == 155) then

        write(uout,'(/,/,1x,a)') 'error - unrecognised tersoff potential '

     else if (kode == 156) then

        write(uout,'(/,/,1x,a)') 'error - atom in tersoff potential not in unique atom list'

     else if (kode == 157) then

        write(uout,'(/,/,1x,a)') 'error - tersoff interaction already given'

     else if (kode == 158) then

        write(uout,'(/,/,1x,a)') 'error - atom in tersoff cross potential not in unique atom list'

     else if (kode == 159) then

        write(uout,'(/,/,1x,a)') 'error - too many tersoff potentials specified'

     else if (kode == 160) then

        write(uout,'(/,/,1x,a)') 'error - problem reading teroff potentials'

     else if (kode == 161) then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate memory to three body potentials'

     else if (kode == 162) then

        write(uout,'(/,/,1x,a)') 'error - wrong thb potential type '

     else if (kode == 163) then

        write(uout,'(/,/,1x,a)') 'error - three body i-atom not found '

     else if (kode == 164) then

        write(uout,'(/,/,1x,a)') 'error - three body j-atom not found '

     else if (kode == 165) then

        write(uout,'(/,/,1x,a)') 'error - three body k-atom not found '

     else if (kode == 166) then

        write(uout,'(/,/,1x,a)') 'error - in reading three body potential'

     else if (kode == 167) then

        write(uout,'(/,/,1x,a)') 'error - expected keyword maxatom'

     else if (kode == 168) then

        write(uout,'(/,/,1x,a)') 'error - random number seeds are outside of bounds or first three are all 1'

     else if (kode == 169) then

        write(uout,'(/,/,1x,a)') 'error - seeds from both user input and clock specified'

     else if (kode == 170) then

        write(uout,'(/,/,1x,a)') 'error - job time set to zero'

     else if (kode == 171) then

        write(uout,'(/,/,1x,a)') 'error - close time set to zero'

     else if (kode == 172) then

        write(uout,'(/,/,1x,a)') 'error - inversion potential type not recognised'

     else if (kode == 173) then

        write(uout,'(/,/,1x,a)') 'error - inversion potential out of range'

     else if (kode == 174) then

        write(uout,'(/,/,1x,a)') 'error - dihedral potential type not recognised'

     else if (kode == 175) then

        write(uout,'(/,/,1x,a)') 'error - dihedral potential out of range'

     else if (kode == 176) then

        write(uout,'(/,/,1x,a)') 'error - Feynman-Hibbs correction is only implemented in vdw_tables_module.f90'

     else if (kode == 181) then

        write(uout,'(/,/,1x,a)') 'error - in reading external potential(s)'

     else if (kode == 182) then

        write(uout,'(/,/,1x,a)') 'error - unknown external potential in FIELD'

     else if (kode == 183) Then

        write(uout,'(/,/,1x,a)') 'error - atom in external potential not in unique atom list'

     else if (kode == 184) then

        write(uout,'(/,/,1x,a)') 'error - undefined external potential (unknown but set)'

     !codes 200-300 for config file

     else if (kode ==   200) Then

        write(uout,'(/,/,1x,a)') 'error - during config file read'

     else if (kode ==   201) Then

        write(uout,'(/,/,1x,a)') 'error - the system is not charge neutral'

     else if (kode ==   202) Then

        write(uout,'(/,/,1x,a)') 'error - maximum number of atoms in molecule reached (insertion impossible)'

     else if (kode ==   203) Then

        write(uout,'(/,/,1x,a)') 'error - maximum number of atoms in replica reached (insertion impossible)'

     else if (kode ==   204) Then

        write(uout,'(/,/,1x,a)') 'error - maximum number of molecules reached (about to exceed)'

     else if (kode ==   205) Then

        write(uout,'(/,/,1x,a)') 'error - remove_mol - no of atoms are out of sync'

     else if (kode ==   206) Then

        write(uout,'(/,/,1x,a)') 'error - more than 1 cell is required'

     else if (kode ==   207) Then

        write(uout,'(/,/,1x,a)') 'error - no gas atoms has been reached'

     else if (kode ==   208) Then

        write(uout,'(/,/,1x,a)') 'error - no gas molecules has been reached'

     else if (kode ==   209) Then

        write(uout,'(/,/,1x,a)') 'error - wrong CONFIG format: 0 = fractional, 1/2/3/6 = Cartesian coordinates'

     else if (kode ==   210) then

        write(uout,'(/,/,1x,a)') 'error - incorrect no of molecules read in'

     else if (kode ==   211) then

        write(uout,'(/,/,1x,a)') 'error - number of atoms in molecule must be greater that 0'

     else if (kode ==   236) then

        write(uout,'(/,/,1x,a)') 'error - the number of mc moves must lie between 1 and 1e5'

     else if (kode ==   237) then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate memory for zdensity'

     else if (kode ==   238) then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate memory for rdfs'

     else if (kode ==   239) then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate memory for displacements'

     else if (kode ==   240) then

        write(uout,'(/,/,1x,a)') 'error - failed to find molecule in species type array (see remove_molecule)'

     else if (kode ==   241) then

        write(uout,'(/,/,1x,a)') 'error - failed to find molecule in species type array (see remove_molecule)'

     else if (kode ==   242) then

        write(uout,'(/,/,1x,a)') 'error - failed to find molecule in species type array (see remove_molecule)'

     ! 300 misc errors

     else if (kode ==   300) Then

        write(uout,'(/,/,1x,a)') 'error - failed to (de-) allocate cell memory'

     else if (kode ==   301) Then

        write(uout,'(/,/,1x,a)') 'error - gcmc is only allowed for a single cell'

     else if (kode ==   302) Then

        write(uout,'(/,/,1x,a)') 'error - in move_atoms: can not move this atom'             
        
     else if (kode ==   303) Then

        write(uout,'(/,/,1x,a)') 'error - the number of cells must be greater that 0 and less than 1000'        

     else if (kode ==   304) Then

        write(uout,'(/,/,1x,a)') 'error - key word cells expected next'      

     else if (kode ==   305) Then

        write(uout,'(/,/,1x,a)') 'error - key word molecule expected next' 

     else if (kode ==   306) Then

        write(uout,'(/,/,1x,a)') 'error - total number of molecules must be greater that 0'

     else if (kode ==   307) Then

        write(uout,'(/,/,1x,a)') 'error - unrecognised molecule type found'

     else if (kode ==   308) Then

        write(uout,'(/,/,1x,a)') 'error - expected number of sub molecules next'

     else if (kode ==   309) Then

        write(uout,'(/,/,1x,a)') 'error - expected number of atoms next'

     else if (kode ==   310) Then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate memory for error function'

     else if (kode ==   311) Then

        write(uout,'(/,/,1x,a)') 'error - trying to put strating atom in wrong domain'

     else if (kode ==   312) Then

        write(uout,'(/,/,1x,a)') 'error - GCMC/Gibbs: given atom or molecule not found in unique atom list'

     else if (kode == 313) then

        write(uout,'(/,/,1x,a)') 'error - lost or gained a bond in the neighbourlist'

     else if (kode == 314) then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate memory for molecules'

     else if (kode == 315) then

        write(uout,'(/,/,1x,a)') 'error - the maximum no of g-vectors has been exceeded'


     else if (kode == 316) then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate memory for temp ewald arrays'

     else if (kode == 317) then

        write(uout,'(/,/,1x,a)') 'error - for gcmc of atoms at least two molecules are reuired'
        write(uout,'(/,/,1x,a)') '        with the at least one gas atom in the last molecule'

     else if (kode == 318) then

        write(uout,'(/,/,1x,a)') 'error - got wrong mc move choice (or no move)'

     else if (kode == 319) then

        write(uout,'(/,/,1x,a)') 'error - transmutation ensemble atoms must have the same charge'

     else if (kode == 320) then

        write(uout,'(/,/,1x,a)') 'error - semi-grand ensemble atoms must have the same charge'

     else if (kode == 321) then

        write(uout,'(/,/,1x,a)') 'error - trying to move atom not present in unique list of atoms'

     else if (kode == 322) then

        write(uout,'(/,/,1x,a)') 'error - multiple and incompatible ensembles have been chosen for atoms'

     else if (kode == 323) then

        write(uout,'(/,/,1x,a)') 'error - multiple and incompatible ensembles have been chosen for molecules'

     else if (kode == 324) then

        write(uout,'(/,/,1x,a)') 'error - molecule name not recognised as a distinct type'

     else if (kode == 325) then

        write(uout,'(/,/,1x,a)') 'error - gcmc: molecule name not recognised as a distinct type'

     else if (kode == 326) then

        write(uout,'(/,/,1x,a)') 'error - the number of moves must total 100'

     else if (kode == 327) then

        write(uout,'(/,/,1x,a)') 'error - domains must be greater than 3 * cutoff'

     else if (kode == 328) then

        write(uout,'(/,/,1x,a)') 'error - error in calculating processor division'

     else if (kode == 329) then

        write(uout,'(/,/,1x,a)') 'error - atomic species are charged (not allowed for domain decomposition)'

     else if (kode == 330) then

        write(uout,'(/,/,1x,a)') 'error - domain decomposition mc requires a neighbourlist'

     else if (kode == 331) then

        write(uout,'(/,/,1x,a)') 'error - atoms and molecules must be moved sequentially in dd mc'

     else if (kode == 332) then

        write(uout,'(/,/,1x,a)') 'error - cavity bias not available in dd mc'

     else if (kode == 333) then

        write(uout,'(/,/,1x,a)') 'error - only one molecule is allowed in dd mc at present'

     else if (kode == 334) then

        write(uout,'(/,/,1x,a)') 'error - more than 100 replicas'

     else if (kode == 335) then

        write(uout,'(/,/,1x,a)') 'error - the number of nodes must be a multiple of the number of replicas (>1)'

     else if (kode == 336) then

        write(uout,'(/,/,1x,a)') 'error - it is not possible to swap both molecules and atoms'

     else if (kode == 337) then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate nbrlist memory'

     else if (kode == 338) then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate excluded list memory'

     else if (kode == 339) then

        write(uout,'(/,/,1x,a)') 'error - distributed g-vectors requires more than one node'

     else if (kode == 340) then

        write(uout,'(/,/,1x,a)') 'error - distributed g-vectors can not be used with threads'

     else if (kode == 341) then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate memory for bondlist'

     else if (kode == 342) then

        write(uout,'(/,/,1x,a)') 'error - failed to deallocate memory for bondlist'

     else if (kode == 343) then

        write(uout,'(/,/,1x,a)') 'error - failed to deallocate nbrlist memory'

     else if (kode == 344) then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate temporary memory'

     else if (kode == 345) then

        write(uout,'(/,/,1x,a)') 'error - failed to deallocate temporary memory'

     else if (kode == 346) then

        write(uout,'(/,/,1x,a)') 'error - maximum number of molecules for a type is zero'

     else if (kode == 347) then

        write(uout,'(/,/,1x,a)') 'error - the value of rmax has exceeded 0.5 cell width'

     else if (kode == 348) then

        write(uout,'(/,/,1x,a)') 'error - the value of rmax has exceeded verlet shell width'

     else if (kode == 349) Then

        write(uout,'(/,/,1x,a)') 'error - gibbs atom exchange: atom not in unique atom list'

     else if (kode == 350) then

        write(uout,'(/,/,1x,a)') 'error - gibbs atom exchange: atoms must have the same charge'

     else if (kode == 351) then

        write(uout,'(/,/,1x,a)') 'error - the workgroup array is the wrong size'                

     else if (kode == 352) then

        write(uout,'(/,/,1x,a)') 'error - molecule name for translation does not have a unique type'

     else if (kode == 353) then

        write(uout,'(/,/,1x,a)') 'error - molecule name for rotation does not have a unique type'

     else if (kode == 354) then

        write(uout,'(/,/,1x,a)') 'error - failed to allocate temporary memory for replica exchange'

     else if (kode == 355) then

        write(uout,'(/,/,1x,a)') 'error - replica exchange led to a mismatch upon config/box update'

     else if (kode == 356) then

        write(uout,'(/,/,1x,a)') 'error - replica exchange atom type not found'

     else if (kode == 357) then

        write(uout,'(/,/,1x,a)') 'error - failed to copy molecule'

     else if (kode == 358) then

        write(uout,'(/,/,1x,a)') 'error - failed to copy molecular phase factors'

     else if (kode == 359) then

        write(uout,'(/,/,1x,a)') 'error - maximum no of neighbours exceeded for displacements'

     !TU: PSMC error: if during the MC loop the displacements exceed the safety threshold 
     else if(kode == 360) then

        write(uout,'(/,/,1x,a)') 'error - a PSMC displacement has exceeded the threshold value'

     else if (kode == 371) then

        write(uout,'(/,/,1x,a)') 'error - distributed g-vectors must be used on more than one node'

     else if (kode == 999) then

        write(uout,'(/,/,1x,a)') 'error - fatal failure : cannot proceed any further'
        write(uout,'(1x,a)')     'error - for clues check outputs of all processes..'

     else

        write(uout,'(/,/,1x,a)') 'error - unnamed error found'

     end if

! close all i/o channels

     close(uout)

     endif ! done - if( master ) !(idnode == 0) 

! exit comms (deallocation is automatic) -> Everyone must call MPI_FINALIZE(ierr)
     if( is_parallel ) then 

         call gsync()
         call exit_mpi_comms()

     endif

     STOP

end subroutine error
