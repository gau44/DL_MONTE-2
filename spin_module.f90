!***************************************************************************
!   Copyright (C) 2018 by T L Underwood                                    *
!   t.l.underwood@bath.ac.uk                                               *
!                                                                          *
!***************************************************************************

!> @brief
!> - Module containing procedures relating to the intrinsic orientations of atoms,
!>   including the atom rotation Monte Carlo move
!> @usage 
!> - Note that the `<control_type>` instance `'job'` determines a lot of the
!>   behaviour in this module, especially the atom rotation Monte Carlo move. 
!> - @stdusage 
!> - @stdspecs
!> @using
!> - `kinds_f90`

module spin_module

    use kinds_f90

    implicit none

        !> Threshold spin magnitude for it to be considered zero 
    real(kind=wp), parameter :: SPIN_ZERO = 0.01_wp

        !> Tolerance for spin magnitude (which should be 1) before the spin is normalised
    real(kind=wp), parameter :: SPIN_TOL = 1.0E-7_wp
    
        !> Total number of atom rotation moves performed so far
    integer ::    total_atm_rots

        !> Total number of successful atom rotation moves so far
    integer ::    total_successful_atm_rots

       !> No of atomic species (types) that can rotate
    integer ::    natmrottyp

       !> List of integer labels for atomic species which will be
       !> subject to atom rotation moves during this simulation
    integer, dimension(:), allocatable :: atmrottyp

        !> no of attempted atom rotations for each species since last max. rotation update
    integer, allocatable, dimension(:) ::    attempted_atm_rots

        !> no of successful atom rotations for each species since last max. rotation update
    integer, allocatable, dimension(:) ::    successful_atm_rots
    
        !> maximum rotation to be applied to each atomic species
    real (kind=wp), dimension(:), allocatable :: rotate_atm_max

    

contains

    

    !> Initialise variables and perform safety checks pertaining to
    !> spin-type atoms and atom rotation moves    
    subroutine setup_spins(job)

        use control_type
        use species_module
        use comms_mpi_module, only : master
        use constants_module, only : uout, ATM_TYPE_SPIN, FED_PS
        use cell_module
        use fed_calculus_module, only : fed
        use psmc_module, only : psmc
        
        implicit none

            !> Simulation parameters
        type(control), intent(inout) :: job

        integer :: ib, i, j, k
        logical :: found
        character :: tag
        real(kind=wp) :: spinmag
        
        total_atm_rots = 0
        total_successful_atm_rots = 0
        natmrottyp = 0
        
        allocate(atmrottyp(job%numatmrot))
        atmrottyp = 0
        
        allocate(attempted_atm_rots(number_of_elements))
        allocate(successful_atm_rots(number_of_elements))
        attempted_atm_rots = 0
        successful_atm_rots = 0

        allocate(rotate_atm_max(number_of_elements))
        rotate_atm_max = job%atmrot


        ! Check if there are any 'spin' atomic species present (in FIELD)
        
        do j = 1, number_of_elements

            if ( eletype(j) == ATM_TYPE_SPIN ) then

                job%is_spin = .true.

                if( master) write(uout,"(/,1x,a)") "'spin' type atoms are present in system"

                call cry(uout,'(/,1x,a,/)',&
                    "WARNING: 'spin' type atoms are a new feature under development.",0)

                exit
                        
            endif

        enddo


        
        ! Safety checks

        

        if( job%is_spin ) then
          
            if( job%semiwidomatms .or. job%gibbsatomexch .or. &
                job%gcmcmol  .or. job%semigrandmols .or. &
                job%swapmols .or. job%gibbsmoltran ) then

                call cry(uout, '', &
                    "ERROR: Move type used which is currently not "// &
                    "supported with 'spin' type atoms.", 999)

            end if

            ! PSMC is forbidden because it requires a little more coding (in the atom rotation procedure)
            if( fed%is_on .and. fed%flavor == FED_PS ) then

                call cry(uout, '', &
                    "ERROR: PSMC is currently not supported with 'spin' type atoms.", 999)

            end if


            ! Rotation of molecules with the Euler method is forbidden because it is
            ! not supported (though quaternion rotation is).
            if( job%rotatemol .and. (.not. job%usequaternion) ) then

                call cry(uout,'', &
                        "ERROR: Euler method for molecule rotations cannot be used with spin type atoms. "// &
                        "Try 'use rotquaternion' in CONTROL",999)

            end if

        end if      


        

        if( job%rotateatm ) then
        
            ! Cycle over all atomic species which have been earmarked for rotation
            ! in the CONTROL file (which sets the control variables in 'job')
            ! Check that these species have been read from FIELD (in which case they
            ! have been stored in 'element' AND 'eletype' in 'species_module'),
            ! and store their atomic species integer label in 'atmrottype'.
            ! Check also that they are 'spin' type atoms
            do k = 1, job%numatmrot

                ! Single-character type tag for species - should be 'p' for 'spin' type
                tag = set_species_type(job%atmroter_type(k))
                
                if( job%atmroter_type(k) /= ATM_TYPE_SPIN ) then

                    call cry(uout,'(/,1x,a,/)',&
                        "ERROR: Atom species '"//trim(job%atmroter(k))//" "//tag// &
                        "' in CONTROL must have type 'spin' for atom rotations.", 999)
                    
                end if
                
                found = .false.
            
                do j = 1, number_of_elements

                    if (element(j) == job%atmroter(k) .and. eletype(j) == job%atmroter_type(k) ) then

                        found = .true.
                        
                        natmrottyp = natmrottyp+1
                    
                        atmrottyp(k) = j
                    
                        exit

                    endif

                enddo

                if (.not.found) then
                    
                    call cry(uout,'(/,1x,a,/)',&
                        "ERROR: Atom species '"//trim(job%atmroter(k))//" "//tag// &
                        "' in CONTROL not defined in FIELD.", 999)
                    
                end if

                if( master ) then
                
                    write(uout,"(/,1x,5a)") "atomic species '", trim(element(j)), " ",tag, &
                                            "' is movable by rotation"
                
                endif

            enddo

            call cry(uout,'(/,1x,a,/)',&
                    "WARNING: Atom rotation moves are a new feature under development.",0)

            
            if( natmrottyp /= job%numatmrot ) then

                call cry(uout,'', &
                    "ERROR: Atom rotations requested in CONTROL do not correspond with species in FIELD !!!",999)

            end if
        
        
            ! Safety checks regarding use of atom rotation moves

            ! If the orientations are not normalised then normalise them - with 0
            ! orientation being set to (0,0,1)
            
            do ib = 1, nconfigs

                do i = 1, cfgs(ib)%mols(1)%natom
                 
                    spinmag = sqrt( dot_product(cfgs(ib)%mols(1)%atms(i)%spin, cfgs(ib)%mols(1)%atms(i)%spin) )

                    if( spinmag < SPIN_ZERO ) then

                        call cry(uout, '(/,1x,a,/)',&
                            "WARNING: Atom orientation with magnitude close to zero. Setting orientation to (0,0,1)", 0)

                        cfgs(ib)%mols(1)%atms(i)%spin = (/ 0.0_wp, 0.0_wp, 1.0_wp /)
                        
                    else if( (spinmag > 1.0_wp + SPIN_TOL) .or. &
                            (spinmag < 1.0_wp - SPIN_TOL) ) then

                        call cry(uout, '(/,1x,a,/)',&
                            "WARNING: Atom orientation with magnitude no close enough to 1.0. Normalising it...", 0)
                        
                        cfgs(ib)%mols(1)%atms(i)%spin = cfgs(ib)%mols(1)%atms(i)%spin / spinmag
                        
                    end if
                    
                end do

                
            end do

        end if
        
        
    end subroutine setup_spins


    

    !> Output statistics regarding the atom rotation moves to `uout`, including the
    !> total number of moves, the successful number of moves, and the current rotation
    !> angle used in the rotation moves for each atomic species.
    subroutine print_rotate_atom_stats()

        use constants_module
        use comms_mpi_module, only : master
        use species_module, only : set_species_type, element, number_of_elements, eletype

        implicit none
        
        integer :: i
        character :: tag

        if( master ) then 
        
            write(uout,"(/,1x,'total no of atom rotations      :',i10)") total_atm_rots
            write(uout,"(  1x,'successful no of atom rotations :',i10,f16.8,/)") total_successful_atm_rots, &
                    real(total_successful_atm_rots,wp)/real(total_atm_rots,wp)

            do i = 1, number_of_elements
            
                tag = set_species_type(eletype(i))
                write(uout,"(1x,'angle of rot for (degrees) ',a8,1x,a1,' : ',f8.4)") element(i), tag, rotate_atm_max(i) / TORADIANS
                
            enddo

        end if
        
    end subroutine print_rotate_atom_stats



    !> Update the atom rotation move size    
    subroutine update_atm_rot_max(job)

        use species_module, only : number_of_elements
        use constants_module, only : PI
        use control_type
        
        implicit none

            !> Simulation parameters
        type(control), intent(inout) :: job

        integer :: i
        real(kind=wp) :: ratio
        
        do i = 1, number_of_elements

            if( attempted_atm_rots(i) < 1 ) cycle
                
            ratio = successful_atm_rots(i) * 1.0_wp / attempted_atm_rots(i)
            
            if (ratio > job%acc_atmrot_ratio) then

                rotate_atm_max(i) = rotate_atm_max(i) * 1.05

            else

                rotate_atm_max(i) = rotate_atm_max(i) * 0.95

            endif

            if (rotate_atm_max(i) > PI) rotate_atm_max(i) = PI
            
            successful_atm_rots(i) = 0
            attempted_atm_rots(i) = 0
            
        enddo

    end subroutine update_atm_rot_max


    
        
    !> Monte Carlo move to rotate the spin of an atom. For now this procedure chooses a random atom
    !> from molecule 1 in the given box and tries to rotate it. Safety checks performed elsewhere
    !> should make sure that this is a sensible thing to do!
    !> Note that the rotation is done using the quaternion method similarly to molecule rotations.
    subroutine rotate_atoms(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                            energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                            energymfa, emoltot, emolrealnonb, emolrealbond, emolrealcrct, emolrcp, emolvdwn, emolvdwb, &
                            emolthree, emolpair, emolang, emolfour, emolmany, emolext)

        use control_type
        use cell_module
        use species_module
        use random_module, only : duni
        use atom_module, only : set_atom_spin, store_atom_spin, restore_atom_spin, rotate_spin_by
        use field_module, only : atom_energy, atom_energy_nolist
        use fed_calculus_module, only : fed, fed_tm_inc
        use fed_order_module, only : fed_param_dbias, fed_param_hist_inc, fed_window_reject
        use constants_module, only : FED_PAR_LCS2, FED_PAR_LCS4, FED_TM

        implicit none

            !> Box number
        integer, intent(in) :: ib

            !> Simulation parameters
        type(control), intent(inout) :: job
        
        real(kind = wp), intent(in) :: beta, extpress

            !> Energies
        real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), &
                                          energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                                          energyfour(:), energyext(:), energymfa(:)

            !> Molecular energies
        real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                                          emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                                          emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                                          emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                                          emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                                          emolmany(nconfigs,number_of_molecules), emolrealcrct(nconfigs,number_of_molecules), &
                                          emolext(nconfigs,number_of_molecules)

        ! Random number, energy difference, energy difference times beta (with bias and without bias ('_can'))
        real(kind=wp) :: arg, deltav, deltavb, deltavb_can

        ! Old and new contributions to the energy
        real(kind=wp) :: oldreal, oldvdw, oldthree, oldpair, oldang, oldfour, oldext, oldmfa, oldvir, &
                         orealnonb(number_of_molecules), orealbond(number_of_molecules), ocrct(number_of_molecules), &
                         ovdwn(number_of_molecules), ovdwb(number_of_molecules), opair(number_of_molecules), &
                         othree(number_of_molecules), oang(number_of_molecules), ofour(number_of_molecules), &
                         oext(number_of_molecules), &
                         newreal, newvdw, newthree, newpair, newang, newfour, newext, newmfa, newvir, &
                         nrealnonb(number_of_molecules), nrealbond(number_of_molecules), ncrct(number_of_molecules), &
                         nvdwn(number_of_molecules), nvdwb(number_of_molecules), npair(number_of_molecules), &
                         nthree(number_of_molecules), nang(number_of_molecules), nfour(number_of_molecules), &
                         next(number_of_molecules)

        ! Flag for spin-dependent order parameters
        logical :: is_fed_spin
        
        ! Molecule number, atom number and species integer label for the rotating atom
        integer :: j, i, typ

        ! Number of atoms of the selected type
        integer :: numtype

        ! Rotation matrix
        real(kind=wp) :: rot(9)

        integer :: im
        real(kind=wp) :: otot, ntot
        

        is_fed_spin = ( fed%par_kind == FED_PAR_LCS2 ) .or. ( fed%par_kind == FED_PAR_LCS4 )

        
        oldvir = 0.0_wp

        oldreal = 0.0_wp
        oldvdw = 0.0_wp
        oldthree = 0.0_wp
        oldpair = 0.0_wp
        oldang = 0.0_wp
        oldfour = 0.0_wp
        oldext = 0.0_wp
        oldmfa = 0.0_wp
        oldvir = 0.0_wp
        orealnonb = 0.0_wp
        orealbond = 0.0_wp

        ocrct = 0.0_wp
        ovdwn = 0.0_wp
        ovdwb = 0.0_wp
        opair = 0.0_wp
        othree = 0.0_wp
        oang = 0.0_wp
        ofour = 0.0_wp
        oext = 0.0_wp

        newvir = 0.0_wp

        newreal = 0.0_wp
        newvdw = 0.0_wp
        newthree = 0.0_wp
        newpair = 0.0_wp
        newang = 0.0_wp
        newfour = 0.0_wp
        newext = 0.0_wp
        newmfa = 0.0_wp
        newvir = 0.0_wp
        nrealnonb = 0.0_wp
        nrealbond = 0.0_wp

        ncrct = 0.0_wp
        nvdwn = 0.0_wp
        nvdwb = 0.0_wp
        npair = 0.0_wp
        nthree = 0.0_wp
        nang = 0.0_wp
        nfour = 0.0_wp
        next = 0.0_wp

        
        ! Select atom to be rotated

        ! Old way of selecting an atom - assumes only one molecule in the system, and that
        !            it is sensible to rotate any of the atoms in this molecule
        !j = 1
        !i = int( duni() * cfgs(ib)%mols(j)%natom ) + 1
        !typ = cfgs(ib)%mols(j)%atms(i)%atlabel

        ! Choose a random integer typ between 1 and job%numatmrot. We will move an atom
        ! belonging to the nth atomic species earmarked for atom rotations, which is 
        ! atmrottyp(typ)
        typ = int (duni() * job%numatmrot + 1)

        !TU: Need to check that there are any such atoms otherwise select_atom will hang
        !TU: Note that findnummol is obsolete and will be replaced soon
        !TU-: What to do about counters for 'empty' moves? These are not tracked here
        call findnumtype(ib, typ, numtype)
        if( numtype == 0 ) return

        !TU: select_atom will hang if there are no atoms of the target type. The above
        !TU: ensures this is only called if there are >0 atoms of the target type
        call select_atom(atmrottyp(typ), ib, j, i)
        
        total_atm_rots = total_atm_rots + 1
        attempted_atm_rots(typ) = attempted_atm_rots(typ) + 1
        

        call store_atom_spin(cfgs(ib)%mols(j)%atms(i))


        
        ! Calculate the energy of the atom

        ! Note that currently only the VdW energy can possibly be affected by an atom rotation move.
        ! Hence it is unnecessary to calculate the metal or tersoff energy here

        ! NOTE: The 'atom_energy' procedures will calculate all contributions to an atom's energy,
        !       even though an atom rotation move can only change the VdW energy. Hence it is wasteful
        !       recalculate, e.g. the Coulomb energy of the atom. This is something which could be
        !       improved in the future.
        
        if (job%uselist) then

            call atom_energy(ib, j, i, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                             oldreal, oldvdw, oldthree, oldpair, oldang, oldfour, oldext, oldmfa, oldvir,  &
                             orealnonb, orealbond, ocrct, ovdwn, ovdwb, opair, othree, oang, ofour, oext,  &
                             .true., .false.)

        else

            call atom_energy_nolist(ib, j, i, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut,  &
                             oldreal, oldvdw, oldthree, oldpair, oldang, oldfour, oldext, oldmfa, oldvir,  &
                             orealnonb, orealbond, ocrct, ovdwn, ovdwb, opair, othree, oang, ofour, oext, &
                             .true., .false.)

        endif



        ! Rotate the atom using the quaternion method

        rot = generate_rot_matrix_quat(rotate_atm_max(typ))

        call rotate_spin_by(cfgs(ib)%mols(j)%atms(i), rot)

        
        
        ! Recalculate the energy of the atom
        
        if (job%uselist) then

            call atom_energy(ib, j, i, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                             newreal, newvdw, newthree, newpair, newang, newfour, newext, newmfa, newvir,  &
                             nrealnonb, nrealbond, ncrct, nvdwn, nvdwb, npair, nthree, nang, nfour, next,  &
                             .true., .false.)

        else

            call atom_energy_nolist(ib, j, i, job%coultype(ib), job%dielec, job%vdw_cut, job%shortrangecut,  &
                             newreal, newvdw, newthree, newpair, newang, newfour, newext, newmfa, newvir,  &
                             nrealnonb, nrealbond, ncrct, nvdwn, nvdwb, npair, nthree, nang, nfour, next, &
                             .true., .false.)

        endif


        deltavb = 0.0_wp
        
        if( is_fed_spin ) then


            deltavb = fed_param_dbias(ib,cfgs(ib))

            ! Reject the move if the energy difference is outside the tolerance
            if( deltavb > job%toler ) goto 1000

        end if
        

        ! Accept or reject according to the Metropolis algorithm

        ! Note that I am assuming that only the VdW energy can change due to an atom rotation move
        deltav  = newvdw - oldvdw

        !TU: deltavb_can is deltavb if there were no biasing ('canonical')
        deltavb_can = deltav * beta

        !TU: deltavb includes the effect of biasing (if it is in use - deltavb was set to account for 
        !TU: biasing, or set to 0, earlier)
        deltavb = deltavb + deltavb_can

        !TU: Update transition matrix if it is in use: the canonical (i.e, without biasing) probability is
        !TU: min(1,exp(-deltavb_can)).
        !TU: Note that this is only reached if the trial state is 'in bounds' for the order parameter range,
        !TU: and hence the transition matrix is only updated for in bounds trial states.
        if( fed % method == FED_TM .and. is_fed_spin ) then
        
            call fed_tm_inc(  min( 1.0_wp, exp(-deltavb_can) )  )

        end if


        !TU: Reject the move if it leaves the window or takes us fuether from the window                          
        if( fed % is_window .and. is_fed_spin ) then
       
           if( fed_window_reject() ) goto 1000
       
        end if



        ! Reject the move if the new energy is too high
        if( deltavb > job%toler ) goto 1000

        arg = duni()

        if( deltavb < 0.0_wp .or. arg < exp(-deltavb) ) then


            
            ! Move is accepted...
            if( is_fed_spin ) then

               call fed_param_hist_inc(ib,.true.)

            end if

            
            successful_atm_rots(typ) = successful_atm_rots(typ) + 1

            ! Update only the VdW and total energies, and the enthalpy, since only these
            ! will change as a result of this move type.
            ! I'm not sure about the virial; does it even work? I've assumed this can change too.

            energytot(ib) = energytot(ib) + deltav        
            energyvdw(ib) = energyvdw(ib) + deltav
            enthalpytot(ib) = enthalpytot(ib) + deltav * extpress
            virialtot(ib) = virialtot(ib) + (newvir - oldvir)

            if(job%lmoldata) then

                do im = 1, number_of_molecules

                    ! For the molecular energies update only the non-bonded and pair energies,
                    ! since only these will change as a result of this move type (see above)

                    emolrealnonb(ib,im) = emolrealnonb(ib,im) + (nrealnonb(im) - orealnonb(im))
                    emolpair(ib,im) = emolpair(ib,im) + (npair(im) - opair(im))
                    emolvdwn(ib,im) = emolvdwn(ib,im) + (nvdwn(im) - ovdwn(im))
                    
                    otot = ovdwn(im) + opair(im) + ovdwn(im)
                    ntot = nvdwn(im) + npair(im) + ovdwn(im) 
                    
                    emoltot(ib,im) = emoltot(ib,im) + (ntot - otot)

                end do

            end if
            
            total_atm_rots = total_atm_rots + 1
            total_successful_atm_rots = total_successful_atm_rots + 1
            
            return

        end if

        
        
1000    continue


        
        ! Move is rejected...

        
        
        ! Restore previous spin
        call restore_atom_spin(cfgs(ib)%mols(j)%atms(i))


        !AB: update the FED histogram & bias if needed (e.g. WL)
        !AB: but revert the parameter state (value & id/index)
        if( is_fed_spin ) then

            call fed_param_hist_inc(ib,.false.)

        end if


        
    end subroutine rotate_atoms
    

    
    
    
end module spin_module
