! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! *                                                                         *
! *   T.L.Underwood - Lattice/Phase-Switch MC method & optimizations        *
! *   t.l.Underwood[@]bath.ac.uk                                            *
! ***************************************************************************

module montecarlo_module

    use kinds_f90
    use statistics_type
    use molecule_type
    use mc_moves
    use vol_moves
    use gcmc_moves
    use gibbs_moves

    implicit none

        !> the number of boxes in the simulation
    integer ::    nconfig

        !> total number of mc moves possible per box within the simulation
    integer ::    num_mc_moves

        !> list of moves to be made at random during simulation
    integer, allocatable ::    mc_move_list(:)

        !> external pressure
    real (kind=wp) ::    extpress

        !> 1 / beta
    real (kind=wp) ::    betainv

        !> 1/ temp * boltz
    real (kind=wp) ::    beta

        !> total virial
    real (kind=wp), dimension(:), allocatable ::    virialtot

        !> total enthalpy
    real (kind=wp), dimension(:), allocatable ::    enthalpytot

        !> total energy
    real (kind=wp), dimension(:), allocatable ::    energytot

        !> energy from recip space ewald
    real (kind=wp), dimension(:), allocatable ::    energyrcp

        !> energy from real space ewald
    real (kind=wp), dimension(:), allocatable ::    energyreal

        !> energy from vdw - non-bonded two body
    real (kind=wp), dimension(:), allocatable ::    energyvdw

        !> energy from pair - bonded two body
    real (kind=wp), dimension(:), allocatable ::    energypair

        !> three-body energy
    real (kind=wp), dimension(:), allocatable ::    energythree

        !> three-body energy - bonded
    real (kind=wp), dimension(:), allocatable ::    energyang

        !> four body energy
    real (kind=wp), dimension(:), allocatable ::    energyfour

        !> many body (metal) energy
    real (kind=wp), dimension(:), allocatable ::    energymany

        !> external potential energy
    real (kind=wp), dimension(:), allocatable ::    energyext

        !> external Coulomb MFA potential energy
    real (kind=wp), dimension(:), allocatable ::    energymfa

        !> volume of cell(s)
    real (kind=wp), dimension(:), allocatable ::    volume

        !> total energy broken down for molecules
    real (kind=wp), dimension(:,:), allocatable ::    emoltot

        !> recip space coulombic energy broken down for molecules
    real (kind=wp), dimension(:,:), allocatable ::    emolrcp

        !> real space coulombic energy broken down for molecules
    real (kind=wp), dimension(:,:), allocatable ::    emolrealnonb

        !> real space coulombic energy broken down for molecules
    real (kind=wp), dimension(:,:), allocatable ::    emolrealbond

        !> self coulombic energy broken down for molecules
    real (kind=wp), dimension(:,:), allocatable ::    emolrealself

        !> bond correction coulombic energy broken down for molecules
    real (kind=wp), dimension(:,:), allocatable ::    emolrealcrct

        !> nonbonded vdw energy broken down for molecules - energy in diff mol types*/
    real (kind=wp), dimension(:,:), allocatable ::    emolvdwn

        !> nonbonded vdw energy broken down for molecules - energy same mol type
    real (kind=wp), dimension(:,:), allocatable ::    emolvdwb

        !> bonded pair energy broken down for molecules
    real (kind=wp), dimension(:,:), allocatable ::    emolpair

        !> nonbonded three body energy broken down for molecules
    real (kind=wp), dimension(:,:), allocatable ::    emolthree

        !> bonded three body (angle) energy broken down for molecules
    real (kind=wp), dimension(:,:), allocatable ::    emolang

        !> nonbonded four body total energy broken down for molecules
    real (kind=wp), dimension(:,:), allocatable ::    emolfour

        !> manybody energy broken down for molecules
    real (kind=wp), dimension(:,:), allocatable ::   emolmany

        !> external energy broken down for molecules
    real (kind=wp), dimension(:,:), allocatable ::   emolext

    real (kind=wp) :: tzero, tinit, eltime

    type(statistics), allocatable, dimension (:) :: stats

    logical, save :: is_GCMC = .false.

    logical, save :: sample_coords = .false.

    integer, save :: ioatmv = 151, iomlmv = 152, iomlrt = 153, iovlmv = 154

    integer, save :: icell_dcd, matoms, matoms_dcd, mframes_dcd, ifstart_dcd, ifstep_dcd, iclose_dcd
    !SCP: counter recording number of calls to scan_mol
        integer, save :: scan_count = 0

    contains


!************************************************************
! standard mc simulation in nvt ensemble only
! ***********************************************************/
subroutine normalmc(job, numcfgs)

    use kinds_f90
    use arrays_module
    use control_type
    use constants_module
    use species_module
    use statistics_module
    use latticevectors_module
    use cell_module
    use field_module
    use coul_module
    use comms_omp_module, only : mptime
    use metpot_module, only : nmetpot
    use comms_mpi_module, only : master, is_parallel, idnode, gsync_world, gsum_world, write_mpi_stats !, mxnode
    use parallel_loop_module, only : setup_workgroups, wkgrp_size, idgrp, open_nodefiles
    use random_module, only : duni, init_duni

    use rep_exchange_module, only : attempted_rep_exch, rejected_rep_exch, successful_rep_exch, &
                                    initialise_rep_exchange, rep_exch_config!, rep_exch_beta !, rep_exchange
    use spin_module, only : setup_spins, rotate_atoms, print_rotate_atom_stats, update_atm_rot_max
    
    !TU: 'id_cur', 'param_cur' and 'bias_vec' are used here 
    !TU: because in PSMC they are output to 'PSDATA' via the procedure 'psmc_data_output'.
    use fed_calculus_module, only : fed, fed_bias_upd, fed_bias_upd_tm, fed_attempt_temp, fed_moves_output, &
                                    id_cur, param_cur, bias_vec

    use fed_order_module, only    : fed_param_check, fed_param_check_psmc, fed_param_reset

    use psmc_module, only : psmc, ib_act, ib_pas, psmc_data_output, psmc_melt_check !, initialise_psmc
    use psmc_moves,  only : switch_move, total_switch_moves, successful_switch_moves

    use orientation_module, only : setup_orientation, set_orientations, dump_orientations

    use yamldata_module, only : yaml_set_output_flags, yaml_write_metadata, yaml_write_frame

    !AB: planar pore (2D slit) stuff
    use slit_module, only : in_slit, mfa_type, zero_MFA_arrays, sample_MFA_charge_zdens, &
                            print_MFA_charge_zdens, read_mfa_data, read_zch_dens

    use thbpotential_module, only : numthb

    use vdw_module, only : vdw_lrc_energy
    
    implicit none

    type(control), intent(inout) :: job

    integer, intent(in) :: numcfgs

    integer :: i, im, ibox0, ibox, iter, choice, selection, lines, npage, ierr, mvol, vbin, next_flush

    real(kind = wp) :: dummy, blng

    real (kind = wp) :: mrealnonb(number_of_molecules), &
                        mrealbond(number_of_molecules), & 
                        mrcp(number_of_molecules), &
                        mrealself(number_of_molecules), &
                        mrealcrct(number_of_molecules), &
                        mvdwn(number_of_molecules), & 
                        mvdwb(number_of_molecules), &
                        mpair(number_of_molecules), &
                        mthree(number_of_molecules), &
                        mang(number_of_molecules), &
                        mfour(number_of_molecules), &
                        mmany(number_of_molecules), &
                        mext(number_of_molecules)

    !arrays for displacement
    real (kind = wp), dimension (:), allocatable :: ratio, vratio, mratio

    integer, dimension(:), allocatable :: interrupt

    logical :: nlflag, is_multi_task, is_gibbs_ens, is_fed_com, is_slit_mfa, safe

    character :: tag1*1, word*40, text*80

    allocate(ratio(number_of_elements))
    allocate(vratio(9))
    allocate(mratio(number_of_molecules))

!AB: I moved the workgroup setup (only reset for replica-exchange) here (from below)
!AB: to be able to read in CONFIG.??? files different for each replica

    is_multi_task = .false.

    !for replica exchange the workgroups need to be established
    if (job%repexch) then

        call setup_workgroups(job%numreplicas, job%repexch_inc, job%systemp, job%distribgvec, job%lparallel_atom)

        is_multi_task = ( job%repexch .and. abs(job%repexch_inc) < 1.e-10 )

    endif

    if (job % usefeynmanhibbs) call fh_correction(job%systemp, job%shortrangecut)

    !AB: when work-groups are used (replica-exchange) 'master' exists for each group
    !AB: the output data from every master/group go into separate files (<NAME>.<NUM> where <NUM>=idgrp)

    !AB: resetting rigid bodies here defeats the use (or not) of 'rigid' keyword in FIELD
    !AB: it also cotradicts mixing atom and molecule moves for flexible molecules (as they are reset as 'rigid')
    !AB: finally, it is at odds with the current exclusion list concept -
    !AB: depending on diffrenet combinations of 'rigid' and 'exclude' in FIELD
    
    !before reading the configurations - setup the rigid bodies
    !call setup_rigid_bodies(job)

!AB: NOTE: job%numreplicas (in 'repexch' case) and 'nconfig' (for Gibbs ensemble only) 
!AB: serve two different concepts and, hence, modes of simulation!

    !read config file - the staring config is loaded to all nodes
    nconfig = numcfgs
    if(nconfig == 0) call error(206)

    call inputcells(job, nconfig)

    !AB: the next call extends reading configurations via file unit 'ucfg' 
    !AB: so pay attention to close(ucfg) if/when moving it
    !AB: see inputcells(job, nconfig) for reference

    !TU: Setup and initialise orientations if required
    if( job % useorientation ) then

        call setup_orientation(job)

        do i=1, nconfig

            call set_orientations(i)

        end do

    end if

!AB: I moved the workgroup setup (only reset for replica-exchange) above
!AB: to be able to read in CONFIG.??? files different for each replica

!    is_multi_task = .false.
!    !for replica exchange the workgroups need to be established
!    if (job%repexch) then
!        call setup_workgroups(job%numreplicas, job%repexch_inc, job%systemp, job%distribgvec, job%lparallel_atom)
!        is_multi_task = ( job%repexch .and. abs(job%repexch_inc) < 1.e-10 )
!    endif

    !AB: the final check for consistency of FED parameter specs (incl. temperature/s)
    !TU: (This procedure does nothing if PSMC is in use - 'fed_param_check_psmc' takes care of things below)
    !if( job % fedcalc ) 
    call fed_param_check(job, nconfig)

    !AB: The PSMC case has been included above (to do the common checks first)
    !TU: Read CONFIG.1 and CONFIG.2 files to get PSMC ideal phases, and initialise PSMC variables
    !if( fed%flavor == FED_PS ) call initialise_psmc(job)

    allocate(interrupt(0:job%numreplicas))
    interrupt = 0

    iter = 1

    npage=25
    lines=0


    ! main setup of arrays
    ! the energies and move arrays are set up on all nodes 
    call setenergies
    call setupmc(job)
    call setup_volume_moves(job)
    call setup_spins(job)
    
    !TU: Set seeds in random_module.f90 to those in 'job', thus initialising the random number generator.
    !TU: Note that setupmc(job) (called above) sets the seed variables in 'job', and must be called before
    !TU: init_duni(job), which transfers those variables to random_module.f90
    call init_duni(job)
    dummy = duni()

    ! get starting time
    call mptime(tzero)

    ! create statistics object for each box and zero arrays
    allocate(stats(nconfig))

    ! set beta & Bjerrum length
    betainv = job % systemp * BOLTZMAN
    beta = 1.0_wp / betainv
    blng = CTOINTERNAL*beta / job%dielec

    if( master ) then

        write(uout,"(/,1x,'energy unit conversion factor (user -> internal) =', f20.10)") job%energyunit
        write(uout,"(/,1x,'beta (deca-J/mol) & Bjerrum length (Angstroem)   =',2f20.10)") beta, blng
        write(uout,"(/,1x,50('='),/)")

    end if

    !get data for restart if required (check if the moves correspond to the previous run!)
    if( job%sysrestart .and. master ) then

        call open_nodefiles('RSTART', urest, ierr, 'old') ! do this once, before rewriting RSTART file(s)
        call read_restart(job%moveatm, job%movemol, job%rotatemol)
        close(urest)

    endif

    if( job%type_vol_move /= 0 ) then

        nlflag = .true.

        job%dcd_cell = 2 !AB: orthorhomibc cell (default) - 'imcon' is more appropriate (but not available here)
        if (job%type_vol_move == NPT_VEC ) job%dcd_cell = 3
    !    icell_dcd = 1
    !else 
    !    nlflag = .false.
    !    icell_dcd = 0

    endif
    icell_dcd = 1 !job%dcd_cell

    mframes_dcd = job%maxiterations/job%sysdump
    ifstart_dcd = 1
    ifstep_dcd  = job%sysdump
    iclose_dcd  = 0

    is_slit_mfa = ( in_slit .and. mfa_type /= 0 .and. job%lzdensity )
    if( is_slit_mfa ) then 
        call zero_MFA_arrays(job%nzden+1, nconfig)

        if( mfa_type > 0 ) then
            call read_mfa_data(1,job%nzden+1,beta)
            call read_zch_dens(1,job%nzden)

            !do ic=2,nconfs
            !   pot_corr(ic,:)   = pot_corr(1,:)
            !   charge_new(ic,:) = charge_new(1,:)
            !   charge_old(ic,:) = charge_old(1,:)
            !end do
        end if
    end if

    matoms      = 0
    matoms_dcd  = 0
    ! zero total energies
    do i = 1, nconfig

        matoms      = max(matoms,cfgs(i)%maxno_of_atoms)
        matoms_dcd  = max(matoms_dcd,cfgs(i)%number_of_atoms)

        call zerostats(job%stacksize, job%energyunit, stats(i), job%repexch)

        call zerotypes(stats(i))

        if (job%semiwidomatms) call zero_semiwidom_chempot(i, job%numsemiwidomatms, stats(i), semitype1, semitype2)
        if (job%semigrandatms) call zero_semigrand_chempot(i, job%numsemigrandatms, stats(i), semitype1, semitype2)

        !set up the cavity bias arrays
        if (job%usecavitybias) then

            !AB: not necessary as allocations are now done in build_cavitygrid(..)
            !call alloc_cavitygrid(cfgs(i), job%cavity_x, job%cavity_y, job%cavity_z)

            call build_cavitygrid(i, job%cavity_x, job%cavity_y, job%cavity_z) 

            call create_cavitylist(i, job%cavity_radius)

        endif

        if( job%lzdensity ) then
            call zero_zdens(stats(i), job%nzden)
            if( is_slit_mfa ) call print_MFA_charge_zdens(job, cfgs, i, 0, stats(i))
        end if

        if (job%lrdfs) call zero_rdf(stats(i), job%nrdfs, job%rdf_max)

        if (job%lsample_displacement) call zero_displacements(stats(i), cfgs(i)%maxno_of_atoms)

    enddo

    is_GCMC = ( job%gcmcatom .or. job%gcmcmol .or. job%gibbsatomtran .or. job%gibbsmoltran & 
                             .or. job%gibbs_indvol .or. job%gibbsatomexch .or. job%gibbsmolexch )

    if( is_GCMC ) matoms_dcd = matoms

    ! create list of moves
    if (job%sysheat < 0) then

        !no preaheating of system
        call create_mc_moves(job, .false.)

    else

        !preheat the system by having atom moves + vol moves only
        call create_mc_moves(job, .true.)

    endif

    !AB: only master(s) open/close/write into files (with inside check)

    if( job % lmovstats ) then
        if( job%moveatm ) call open_nodefiles('ATOMOVES', ioatmv, ierr)
        if( job%movemol ) call open_nodefiles('MOLMOVES', iomlmv, ierr)
        if( job%rotatemol ) call open_nodefiles('ROTMOVES', iomlrt, ierr)
        if( job%type_vol_move /= NPT_OFF ) call open_nodefiles('VOLMOVES', iovlmv, ierr)
    end if

    !AB: open the files for output
    call open_nodefiles('REVIVE', urevi, ierr) 
    call open_nodefiles('PTFILE', uplt, ierr)

    !AB: conditionally used files:
    if( job%lsample_energy )     call open_nodefiles('ENERGY', ueng, ierr)
    if( job%lsample_volume )     call open_nodefiles('VOLDAT', unpt, ierr)
    !if( job%type_vol_move /= 0 ) call open_nodefiles('VOLDAT', unpt, ierr)
      if( job%scanmol ) then
      call open_nodefiles('SCAN', uscan, ierr) !scp for molecule scan
      write(uscan,'(''iteration'',17x,''relative coordinates'',30x,''atom 1 coordinates'', &
       10x,''relative energy (dE)    exp(dE*beta)'')')
      endif
    sample_coords = job%sysdump > 0
    next_flush = 10*job%sysdump

    !AB: trajectory files are re-opened/closed on-the-fly (see within the loop!)
    if( sample_coords ) then 

        
        if( job%archive_format == VTK ) then

            if( job%is_gibbs_ensemble ) then
                call open_nodefiles('VTK-1', uarch, ierr)
                call open_nodefiles('VTK-2', uarch2, ierr)
            else
                call open_nodefiles('VTK', uarch, ierr)
            end if

        else if( job%archive_format < DLP2 ) then

            if( job%is_gibbs_ensemble ) then
                call open_nodefiles('ARCHIVE-1', uarch, ierr)
                call open_nodefiles('ARCHIVE-2', uarch2, ierr)
            else
                call open_nodefiles('ARCHIVE', uarch, ierr)
            end if

        else if( job%archive_format < DCD ) then

            if( job%is_gibbs_ensemble ) then
                call open_nodefiles('HISTORY-1', uarch, ierr)
                call open_nodefiles('HISTORY-2', uarch2, ierr)
            else
                call open_nodefiles('HISTORY', uarch, ierr)
            end if
            
        end if

    end if

    !AB: not clear what is sampled and collected - ???
    !AB: opens every time before writing (in the loop!)
    !if( job%lsample_displacement ) call open_nodefiles('DISPLA', udsp, ierr) 

    !TU: PSMC output file
    if( fed%flavor == FED_PS ) call open_nodefiles('PSDATA', upsmc, ierr)

    !TU: Orientation output file
    if( job%useorientation .and. &
        (job%sample_orientation .or. job%sample_orientation_init) ) &
        call open_nodefiles('ORIENT', uorient, ierr)

    !TU: Dump the initial orientations if specified to do so 
    if( job%useorientation .and. job % sample_orientation_init ) then

        if( master ) then

            write(uout,"(/,1x,'outputting initial orientations to ORIENT*',/)")
                    
            do ibox0 = 1, nconfig
               
                call dump_orientations(uorient, ibox0, 0)
                
            end do
            
        end if

    end if

    !TU: YAML data file initialisation
    if( job%useyamldata ) then

       call yaml_set_output_flags(job)

       if( job % is_gibbs_ensemble ) then

           !TU: For the Gibbs ensemble we need two YAMLDATA file, one for each box

           call open_nodefiles('YAMLDATA-1', uyaml, ierr)
           call open_nodefiles('YAMLDATA-2', uyaml2, ierr)

           if( master) then

               call yaml_write_metadata(uyaml, job)
               call yaml_write_metadata(uyaml2, job)

           end if

       else

           call open_nodefiles('YAMLDATA', uyaml, ierr)

           if( master) call yaml_write_metadata(uyaml, job)

       end if

    end if

    !---- end of setup

    call mptime(eltime)
    call time_stamp("Initialisation phase 1 - elapsed time: ", eltime-tzero, eltime, uout)

    flush(uout)

    if(nmetpot > 0) call setden_work

    do i = 1, nconfig

        !TU-: 'job%max_nonb_nbrs' is 0 by default (it is set to 0 in 'initialise_defaults' 
        !TU-: in 'control_module.f90'), and remains as such unless it is explicitly changed
        !TU-: via the directive 'maxnonbondnbrs' in CONTROL.
        call alloc_list_arrays(i, job%uselist, job%lauto_nbrs, job%max_nonb_nbrs, job%max_thb_nbrs)

        call cellsize(cfgs(i)%vec, volume(i))

        if(job%coultype(i) > 0 .or. job%coultype(i) < -1 ) then
        !if(job%coultype(i) > -2 .and. job%coultype(i) /= 0) then

            !make sure recip lattice vectors are upto date
            !this must be done on all nodes
            call setup_ewald(i, job)

        endif

        if( job%uselist ) then 

            call setnbrlists(i, job%shortrangecut, job%verletshell, job%linklist)

            if(job%lauto_nbrs) call set_rzero(i)

        endif

        !TU: Set three-body lists if there are any three-body potentials
        if(numthb > 0) call set_thblist(i)


        !AB initialise exclusion list (first time - with output)
        !call set_exclusions(i)
        call set_exclist(i)

    enddo

    if (any(job%coultype == 1)) call setrcp_work(job%coultype)
    
    call mptime(eltime)
    call time_stamp("Initialisation phase 2 - elapsed time: ", eltime-tzero, eltime, uout)

    flush(uout)


    if( master ) then
        write(uout,"(1x,50('-'))")
        write(uout,"(a)")"                  initial energies "
        write(uout,"(1x,50('-'))")
    end if

    emoltot = 0.0_wp
    mrealnonb = 0.0_wp
    mrealbond = 0.0_wp
    mrcp = 0.0_wp
    mvdwn = 0.0_wp
    mvdwb = 0.0_wp
    mpair = 0.0_wp
    mthree = 0.0_wp
    mang = 0.0_wp
    mfour = 0.0_wp
    mmany = 0.0_wp
    mext = 0.0_wp

    do i = 1, nconfig

        if(job%coultype(i) == 1) then

            call total_recip(i, energyrcp(i), mrcp, virialtot(i))

        endif

        !calculate initial energy
        if(job%uselist) then

            call total_energy(i, job%coultype(i), job%vdw_cut, job%shortrangecut, job%dielec, &
                          energyreal(i), energyvdw(i), energythree(i), energypair(i), energyang(i), &
                          energyfour(i), energymany(i), energyext(i), energymfa(i), virialtot(i), &
                          mrealnonb, mrealbond, mrealself, mrealcrct, mvdwn, mvdwb, mpair, &
                          mthree, mang, mfour, mmany, mext)

        else

            call total_energy_nolist(i, job%coultype(i), job%vdw_cut, job%shortrangecut, job%dielec,  &
                          energyreal(i), energyvdw(i), energythree(i), energypair(i), energyang(i), &
                          energyfour(i), energymany(i), energyext(i), energymfa(i), virialtot(i), &
                          mrealnonb, mrealbond, mrealself, mrealcrct, mvdwn, mvdwb, mpair, &
                          mthree, mang, mfour, mmany, mext)

        endif

        energytot(i) = energyrcp(i) + energyreal(i) + energyvdw(i) + energypair(i) + energythree(i) + &
                       energyang(i) + energyfour(i) + energymany(i) + energyext(i) + energymfa(i)

        call printtotal(i, energytot(i), energyrcp(i), energyreal(i), energyvdw(i), energypair(i), &
                        energythree(i), energyang(i), energyfour(i), energymany(i), energyext(i), &
                        energymfa(i), virialtot(i), volume(i), job%energyunit)

        if( master ) then
            write(uout,*)
            write(uout,"(1x,'long-range correction (vdw)     ',e20.10)") &
                 vdw_lrc_energy( 1.0_wp/volume(i), cfgs(i)%nums_elemts ) / job%energyunit
        end if

        do im = 1, number_of_molecules
        
            emolrealnonb(i,im) = mrealnonb(im)
            emolrealbond(i,im) = mrealbond(im)
            emolrealself(i,im) = mrealself(im)
            emolrealcrct(i,im) = mrealcrct(im)
            emolrcp(i,im) = mrcp(im)
            emolvdwn(i,im) = mvdwn(im)
            emolvdwb(i,im) = mvdwb(im)
            emolpair(i,im) = mpair(im)
            emolthree(i,im) = mthree(im)
            emolang(i,im) = mang(im)
            emolfour(i,im) = mfour(im)
            emolmany(i,im) = mmany(im)
            emolext(i,im) = mext(im)

            emoltot(i,im) = mrealnonb(im) + mrealbond(im) + mrcp(im) + mvdwn(im) + mvdwb(im) + mpair(im)  &
                             + mthree(im) + mang(im) + mfour(im) + mmany(im) + mrealself(im) + mrealcrct(im) + mext(im)

            call print_moltotal(im, emoltot(i,im), mrealnonb(im), mrealbond(im), mrealself(im), mrealcrct(im), mrcp(im), &
                                mvdwn(im), mvdwb(im), mpair(im), mthree(im), mang(im), mfour(im), mmany(im), mext(im), &
                                job%energyunit)

        enddo

        !copy initial densities
        if(nmetpot > 0) call copy_density(i)

        !copy initial phase sums
        if(job%coultype(i) == 1) then

            call update_rcpsums(i)

        endif

    enddo

    if (job%repexch) call initialise_rep_exchange(job, beta)

    !time to calculate the first energy
    call mptime(eltime)
    call time_stamp("Initialisation phase 3 - elapsed time: ", eltime-tzero, eltime, uout)

    !TU: Once the energies have been calculated we check that the initial order parameter
    !TU: (which depends on the energies) is within the allowed range, and initialise
    !TU: the current order parameter state and value.
    if( job % fedcalc .and.  fed%flavor == FED_PS ) call fed_param_check_psmc(energytot(1), energytot(2))

    if( master ) write(uout,*)'Resetting time and proceeding to simulation'

    !AB: close and re-open the main output file (for flushing)
    call open_nodefiles('OUTPUT', uout, ierr, 'add')

    tinit = eltime-tzero
    tzero = eltime

    is_gibbs_ens = ( job%gibbsatomtran .or. job%gibbsmoltran .or. &
                     job%gibbs_indvol .or. job%gibbsatomexch .or. job%gibbsmolexch )

    if( is_gibbs_ens .and. nconfig < 2) call error(99)

    is_fed_com = ( job%fedcalc .and. fed%par_kind == FED_PAR_DIST2 )

    call gsync_world()

    ibox = 0

!scp: if only doing a scan - limit the maximum number of iterations (assuming nconfig ==1)
     if(job%scanmol) then
  !scp:if only auto     if(num_mc_moves.eq.1.and.job%maxiterations.gt.job%max_scan) job%maxiterations=job%max_scan
        if(num_mc_moves.eq.1.and.job%maxiterations.gt.job%max_scan) job%maxiterations=job % num_scan_stop-job % num_scan_start
        if(num_mc_moves.gt.1)then
        write(uout,"(/,/,1x,'WARNING: more than one type of moves selected with scan - this is UNTESTED!!!!!',/,/)")
        write(uout,"(/,/,1x,'Unlikely to satisfy detailed balance',/,/)")
        endif
     endif


    ! start the mc loop over iterations
    do iter = 1, job%maxiterations

        if (job%sysheat == iter) then

            write(uout,"(/,/,1x,'NOTE: switching off pre-heating & starting full set of moves',/,/)")

            !switch off pre-heating of system
            call create_mc_moves(job, .false.)

        endif

        !make sure atoms are back in box
        if( job%usereset .and. mod(iter,job%reset_freq) == 0 ) then

            do i = 1, nconfig

                call pbc_simulation_cell(i)

            enddo

        endif

        ibox0 = ibox

        !TU: For PSMC we have two boxes: box 1 corresponds to phase 1; box 2 corresponds to phase 2.
        !TU: Monte Carlo moves are only applied to one phase/box at a time - the ACTIVE phase/box.
        !TU: When a Monte Carlo move is made in the active box, the passive box is updated analogously.
        !TU: The below code selects the currently active box for a Monte Carlo move.
        if( fed%flavor == FED_PS ) then

            ibox = ib_act        

        else

            !AB: to do: allow nconfig > 1, like nconfig = numreplicas (but <= n_threads) for parallel setup
            !AB: currently nconfig > 1 only in the case of Gibbs ensemble!

            !if (nconfig == 1) then

                ibox = 1

            !else 

            !AB: it appears the box hopping is only relevant for Gibbs ensemble (in both serial and parallel runs)
            !AB: add another condition(s) here for extra case(s) requiring setup with two randomly sampled configuration boxes

        end if

        !AB: If we allow scenaria with more than one box per workgroup, like below for Gibbs enemble (any other case?),
        !AB: then it must not be group ID (idgrp) that controls the number of MC pathways (trajectories) 
        !AB: and that is added as numerical suffix to the file names BUT the box ID (ibox)!!!
        !AB: Currently, more than one workgroup/master can be present only in the case of "replica-exchange", whereas
        !AB: the Gibbs ensemble is, in fact, a BROKEN exclusion case that would result in MESSING UP DATA in all files...

        if( is_gibbs_ens ) then

            ibox  = int(nconfig * duni()) + 1

        endif

        !AB: if jumped into another box -> reset old and current parameter values (COM:s) and id/indices
        if( is_fed_com .and. ibox0 /= ibox ) call fed_param_reset(ibox, cfgs(ibox), .true.)

        choice = int(num_mc_moves * duni() + 1)
        selection = mc_move_list(choice)

        ! update neighbourlists if necessary
        if(job%uselist) then

            if(.not.job%lauto_nbrs) then

                if (mod(iter,job%nbrlist) == 0) then

                    do i = 1, nconfig

                        call setnbrlists(i, job%shortrangecut, job%verletshell, job%linklist)

                    enddo

                endif

            endif

        endif

        !TU: Update three-body list when necessary
        if(mod(iter,job%thblist) == 0 .and. numthb > 0) then

            do i = 1, nconfig
                
                call set_thblist(i)

            end do
            
        end if
        
        !update cavity bias list at required frequency
        if (job%usecavitybias .and. mod(iter,job%cavity_update) == 0) then

            do i = 1, nconfig

                call create_cavitylist(i, job%cavity_radius)

            enddo

        endif

        select case (selection)

                case (1)

                    if(job%useseqmove) then

                        call move_atoms_seq(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                             energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                             energymfa, emoltot, emolrealnonb, emolrealbond, emolrealcrct, emolrcp, emolvdwn, emolvdwb, &
                             emolthree, emolpair, emolang, emolfour, emolmany, emolext)
                    else
                        call move_atoms(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                             energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                             energymfa, emoltot, emolrealnonb, emolrealbond, emolrealcrct, emolrcp, emolvdwn, emolvdwb, &
                             emolthree, emolpair, emolang, emolfour, emolmany, emolext)

                    endif
                    
                    !safe = .true.
                    !call check_mol_coms_ok(ibox,safe)
                    !if( .not.safe ) write(uout,'(/,a,/)')"...after atom move(s)..."

                case (2)

                    if (job%useseqmolmove) then
                        call move_molecules_seq(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                             energyreal, energyvdw, energythree, energypair, energyang, energymany, energyext, energymfa, &
                             emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                             emolpair, emolang, emolfour, emolmany, emolext)
                    else
                        call move_molecules(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                             energyreal, energyvdw, energythree, energypair, energyang, energymany, energyext, energymfa, &
                             emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                             emolpair, emolang, emolfour, emolmany, emolext)
                    endif
                    
                    !safe = .true.
                    !call check_mol_coms_ok(ibox,safe)
                    !if( .not.safe ) write(uout,'(/,a,/)')"...after molecule move(s)..."

                case (3)

                    if (job%useseqmolrot) then
                        call rotate_molecules_seq(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                             energyreal, energyvdw, energythree, energypair, energyang, energymany, energyext, energymfa, &
                             emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                             emolpair, emolang, emolfour, emolmany, emolext)
                    else
                        call rotate_molecules(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                             energyreal, energyvdw, energythree, energypair, energyang, energymany, energyext, energymfa, &
                             emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                             emolpair, emolang, emolfour, emolmany, emolext)
                    endif
                    
                    !safe = .true.
                    !call check_mol_coms_ok(ibox,safe)
                    !if( .not.safe ) write(uout,'(/,a,/)')"...after molecule rotation(s)..."

                case (4)

                    !TU: Note that for Gibbs volume moves both boxes are updated by 'move_volume': it makes no real
                    !TU: difference whether 'ibox' = 1 or 2, except that box 'ibox' has its volume updated stochastically
                    !TU: while the other box (i.e. 2 if ibox=1, or 1 if ibox=2) has its volume updated deterministically
                    !TU: (such that the volume of the two boxes is unchanged).
                    call move_volume(ibox, job, beta, betainv, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                            energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                            energymfa, emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, emolvdwn, &
                            emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext, volume)
                    
                    !safe = .true.
                    !call check_mol_coms_ok(ibox,safe)
                    !if( .not.safe ) write(uout,'(/,a,/)')"...after volume move..."

                    !if( master .and. mod(totalvolchanges,10) == 0 ) write(unpt,*) volume

                    if( job%lsample_volume .and. mod(totalvolchanges,job%sample_volume_freq) == 0 ) then

                        !if( master ) write(unpt,*)totalvolchanges,'  ',volume

                        if( master ) write(unpt,*) volume

                        call collect_volhist(ibox, job, volume)

                    end if

!                   endif

                case (5)

                    call gcmc_atoms(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                         energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                         energymfa, emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                         emolpair, emolang, emolfour, emolmany, emolext, volume)

                case (6)

                    call swap_atoms(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                         energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                         energymfa, emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                         emolpair, emolang, emolfour, emolmany, emolext)

                case (7)

                    call semiwidom_atoms(ibox, job, iter, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                         energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                         energymfa, emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                         emolpair, emolang, emolfour, emolmany, emolext, volume)

                    call sample_semiwidom_chempot(chem_pot_atm1, chem_pot_atm2, job%numsemiwidomatms, &
                                                  iter, job%sysequil, stats(ibox))

                case (8)


                case (9)

                    call gibbs_atom_transfer(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                         energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                         energymfa, emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                         emolpair, emolang, emolfour, emolmany, emolext, volume)

                case (10)

                    call gibbs_atom_exchange(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                         energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                         energymfa, emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                         emolpair, emolang, emolfour, emolmany, emolext, volume)

                case (11)

                    call gcmc_mols(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                         energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                         energymfa, emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, &
                         emolvdwn, emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext, volume)

                case (12)

                    call semigrand_atoms(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                         energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                         energymfa, emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                         emolpair, emolang, emolfour, emolmany, emolext, volume)

                    call sample_semiwidom_chempot(chem_pot_atm1, chem_pot_atm2, job%numsemigrandatms, &
                                                  iter, job%sysequil, stats(ibox))

                case (13)

                    call semigrand_mols(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                         energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                         energymfa, emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, &
                         emolvdwn, emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext, volume)

                case (14)

                    call swap_molecules(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                         energyreal, energyvdw, energythree, energypair, energyang, energymany, energyext, &
                         energymfa, emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                         emolpair, emolang, emolfour, emolmany, emolext)

                case (15)

                    call gibbs_mol_transfer(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                         energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                         energymfa, emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, &
                         emolvdwn, emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext, volume)

                case (16)
                                        
                    call rotate_atoms(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                             energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                             energymfa, emoltot, emolrealnonb, emolrealbond, emolrealcrct, emolrcp, emolvdwn, emolvdwb, &
                             emolthree, emolpair, emolang, emolfour, emolmany, emolext)
                
                case (17)
                !scp: version of move_molecules
                     call scan_molecules(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                             energyreal, energyvdw, energythree, energypair, energyang, energymany, energyext, &
                             energymfa, emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                             emolpair, emolang, emolfour, emolmany, emolext, ib_pas,scan_count)


                case(100)

                    !TU: PSMC switch move (swaps the labels ib_act and ib_pas with some probability)
                    call switch_move(job, ib_act, ib_pas, beta, betainv, extpress, energytot, cfgs)

                case (101)

                    !TU: PSMC atom translation move (performs 'normal' atom move on box ib_act,
                    !TU: and the analogous 'synchronising' move for box ib_pas) - note the extra argument
                    !TU: 'ib_pas' which makes 'move_atoms' perform the update on both boxes.
                    call move_atoms(ib_act, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                         energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                         energymfa, emoltot, emolrealnonb, emolrealbond, emolrealcrct, emolrcp, emolvdwn, &
                         emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext, ib_pas)
                case (102)

                    !TU: PSMC molecule translation move (performs 'normal' molecule move on box ib_act,
                    !TU: and the analogous 'synchronising' move for box ib_pas) - note the extra argument
                    !TU: 'ib_pas' which makes 'move_molecules' perform the update on both boxes.
                    call move_molecules(ibox, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                         energyreal, energyvdw, energythree, energypair, energyang, energymany, energyext, &
                         energymfa, emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                         emolpair, emolang, emolfour, emolmany, emolext, ib_pas)

                 case(103)

                    !TU: PSMC molecule rotationn move (performs 'normal' rotation move on box ib_act,
                    !TU: and the analogous 'synchronising' move for box ib_pas) - note the extra argument
                    !TU: 'ib_pas' which makes 'rotate_molecules' perform the update on both boxes.
                    call rotate_molecules(ib_act, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                         energyreal, energyvdw, energythree, energypair, energyang, energymany, energyext, &
                         energymfa, emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                         emolpair, emolang, emolfour, emolmany, emolext, ib_pas)

                 case(104)

                    !TU: PSMC volume move (performs 'normal' volume move on box ib_act,
                    !TU: and the analogous 'synchronising' move for box ib_pas)
                    !
                    !TU: Currently this is identical to case(4): this is because I am half way
                    !TU: through refactoring. Only one case is necessary for PSMC, Gibbs or otherwise:
                    !TU: this case should be deleted eventually.
                    call move_volume(ib_act, job, beta, betainv, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                         energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                         energymfa, emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, emolvdwn, &
                         emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext, volume)

                case default

                    call error(318)

        end select

        !TU: For debugging to check that the counter arrays are correct
        !do ibox0 = 1, nconfig
        !
        !    call checktypecounters(ibox)
        !
        !end do

        if (job%repexch .and. job%numreplicas > 1) then

            call gsync_world

            !if( any(interrupt > 0) .or. mod(iter,job % repexch_freq) == 0 ) then
            if( .not.is_multi_task .and. mod(iter,job % repexch_freq) == 0 ) then

                !interrupt(idgrp) = 1

                !call gsum_world(interrupt)

                !if( mod(iter,job % repexch_freq) /= 0 ) print*,'using interupt ',idnode,' iteration ', iter

                !AB: a new RE routine - swapping T-values (based on indices within a T-set)
                !call rep_exch_beta(ibox, job, energytot, beta, betainv)
                !call rep_exch_beta(ibox, job, stats, energytot, beta, betainv)

                !AB: hybrid (corrected) version by AB/JP, based on exchange of configuration/box pairs
                call rep_exch_config(ibox, job, beta, energytot, enthalpytot, energyrcp, energyreal, energyvdw, &
                        energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                        emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, emolvdwn, &
                        emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext, volume)

                !interrupt = 0

            endif

!        else 
        endif

        if( job%fedcalc ) then 

            if( mod(iter,job % fed_trial_freq) == 0 ) then

                if( fed%par_kind == FED_PAR_TEMP .or. fed%par_kind == FED_PAR_BETA ) then 

                    call fed_attempt_temp(ibox, job, beta, betainv, energytot, iter) 

                end if

            end if

            !AB: periodically tune-up EE or WL bias feedback & print out the obtained FED data
            if( mod(iter,fed%numupdskip) == 0 .or. iter == job%maxiterations ) then 

                call fed_bias_upd( fed%upd_scl, iter, (iter < job%maxiterations), is_multi_task )

                if( is_fed_com ) call fed_param_reset(ibox, cfgs(ibox), .false.)

            !TU: If TM is used then periodically update the bias function without printing output
            else if( fed % method == FED_TM .and. mod(iter,fed%numupdskipfine) == 0 ) then

                call fed_bias_upd_tm()

            end if

        end if

        !update maximum distance allowed to move for atoms
        if (job%moveatm.and.mod(iter,job%acc_atmmve_update) == 0.and.iter > 0) then
        
            if( master .and. job % lmovstats ) call open_nodefiles('ATOMOVES', ioatmv, ierr, 'add')

            do ibox0 = 1, nconfig

                do i = 1, number_of_elements       ! update max displacement distances

                    if( attempted_atm_moves(i,ibox0) < 1 ) cycle

                    ratio(i) = no_atm_moves(i,ibox0) / attempted_atm_moves(i,ibox0)

                    if( master .and. job % lmovstats ) then
                        write(ioatmv,*)"# Atom moves stats in cell ",ibox0, &
                                       " for element ",i," at iteration",iter
                        write(ioatmv,*)"max_delta (cur) = ",distance_atm_max(i,ibox0)
                        write(ioatmv,*)"acc / tot (cur) = ",no_atm_moves(i,ibox0), &
                                       " / ",attempted_atm_moves(i,ibox0)," = ",ratio(i)
                        write(ioatmv,*)
                    end if

                    !if( attempted_atm_moves(i,ibox0) > job%acc_atmmve_update & !) then
                    !    .and. abs(ratio(i) - job%acc_atmmve_ratio)/job%acc_atmmve_ratio > 0.01 )  then

                        if (ratio(i) > job%acc_atmmve_ratio) then
                            distance_atm_max(i,ibox0) = distance_atm_max(i,ibox0) * 1.05_wp
                        else
                            distance_atm_max(i,ibox0) = distance_atm_max(i,ibox0) * 0.95_wp
                        endif

                        no_atm_moves(i,ibox0) = 0.0_wp
                        attempted_atm_moves(i,ibox0) = 0

                    !endif

                enddo

                call check_distance_atoms(ibox0, distance_atm_max, job%uselist, job%verletshell, ierr, .false.)

            enddo
            
            if( master .and. job % lmovstats ) close(ioatmv)

        endif

        !update maximum distance allowed to move
        if (job%movemol.and.mod(iter,job%acc_molmve_update) == 0.and.iter > 0) then

            if( master .and. job % lmovstats ) call open_nodefiles('MOLMOVES', iomlmv, ierr, 'add')

            do i = 1, number_of_molecules     ! update max displacement distances

                if( attempted_mol_moves(i) < 1 ) cycle

                mratio(i) = no_mol_moves(i) / attempted_mol_moves(i)

                if( master .and. job % lmovstats ) then
                    write(iomlmv,*)"# Molecule moves stats for molecule ",i, &
                                   " at iteration",iter
                    write(iomlmv,*)"max_delta (cur) = ",distance_mol_max(i)
                    write(iomlmv,*)"acc / tot (cur) = ",no_mol_moves(i), &
                                " / ",attempted_mol_moves(i)," = ",mratio(i)
                    write(iomlmv,*)
                end if

                !if( attempted_mol_moves(i) > job%acc_molmve_update & !) then
                !    .and. abs(mratio(i) - job%acc_molmve_ratio)/job%acc_molmve_ratio > 0.01 )  then

                    if (mratio(i) > job%acc_molmve_ratio) then
                        distance_mol_max(i) = distance_mol_max(i) * 1.05_wp
                    else
                        distance_mol_max(i) = distance_mol_max(i) * 0.95_wp
                    endif

                    no_mol_moves(i) = 0.0_wp
                    attempted_mol_moves(i) = 0
                
                !endif

            enddo
            
            if( master .and. job % lmovstats ) close(iomlmv)

            call check_distance_molecules(ibox, distance_mol_max, job%uselist, job%verletshell, ierr, .false.)

        endif

        !update max rotation of a molecule
        if (job%rotatemol.and.mod(iter,job%acc_molrot_update) == 0.and.iter > 0) then
        
            if( master .and. job % lmovstats ) call open_nodefiles('ROTMOVES', iomlrt, ierr, 'add')

            do i = 1, number_of_molecules     ! update max displacement distances

                if( attempted_mol_rots(i) < 1 ) cycle
                
                mratio(i) = no_mol_rots(i) / attempted_mol_rots(i)

                if( master .and. job % lmovstats ) then
                    write(iomlrt,*)"# Molecule rotation stats for molecule ",i, &
                                   " at iteration",iter
                    write(iomlrt,*)"acc / tot (cur) = ",no_mol_rots(i), &
                                " / ",attempted_mol_rots(i)," = ",mratio(i)
                    write(iomlrt,*)"max_delta (cur) = ",rotate_mol_max(i)
                    write(iomlrt,*)
                end if

                !if( attempted_mol_rots(i) > job%acc_molrot_update &
                !    .and. abs(mratio(i) - job%acc_molrot_ratio)/job%acc_molrot_ratio > 0.01 )  then

                    if (mratio(i) > job%acc_molrot_ratio) then
                        rotate_mol_max(i) = rotate_mol_max(i) * 1.05
                    else
                        rotate_mol_max(i) = rotate_mol_max(i) * 0.95
                    endif

                    if (rotate_mol_max(i) > PI) rotate_mol_max(i) = PI

                    no_mol_rots(i) = 0.0
                    attempted_mol_rots(i) = 0
                
                !endif

            enddo
            
            if( master .and. job % lmovstats ) close(iomlrt)

        endif

        !TU: Update max rotation of an atom
        if (job%rotateatm .and. (mod(iter, job%acc_atmrot_update) == 0) .and. iter > 0) then
            
            call update_atm_rot_max(job)

        end if
            
        !update max vol change allowed
        if((job%type_vol_move /= NPT_OFF).and.(mod(iter,job%acc_vol_update) == 0).and.(iter > 0)) then
        
            if( master .and. job % lmovstats ) call open_nodefiles('VOLMOVES', iovlmv, ierr, 'add')

            do ibox0 = 1, nconfig

                do i = 1, 9        ! max vol change update

                    if( attemptedvolchange(i,ibox0) < 1 ) cycle

                    vratio(i) = novolchange(i,ibox0) / attemptedvolchange(i,ibox0)

                    if( master .and. job % lmovstats ) then 
                        write(iovlmv,*)"# Volume moves stats for box ",ibox0, &
                                       " index ",i," at iteration ",iter," : ",&
                                       attemptedvolchange(i,ibox0)," >? ",job%acc_vol_update
                    
                        write(iovlmv,*)"acc / tot (cur) = ",novolchange(i,ibox0), &
                                    " / ",attemptedvolchange(i,ibox0)," = ",vratio(i)
                        write(iovlmv,*)"max_delta (cur) = ",maxvolchange(i,ibox0)
                        write(iovlmv,*)
                    end if

                    !if( attemptedvolchange(i,ibox0) > job%acc_vol_update &
                    !    .and. abs(vratio(i) - job%acc_vol_ratio)/job%acc_vol_ratio > 0.01 )  then

                        if (vratio(i) > job%acc_vol_ratio) then
                            maxvolchange(i,ibox0) = maxvolchange(i,ibox0) * 1.05
                        else
                            maxvolchange(i,ibox0) = maxvolchange(i,ibox0) * 0.95
                        endif

                        novolchange(i,ibox0) = 0.0
                        attemptedvolchange(i,ibox0) = 0

                    !endif

                enddo

            enddo
            
            if( master .and. job % lmovstats ) close(iovlmv)

        endif

        !sample energies etc for statistics
        do ibox0 = 1, nconfig

            if(job%calcstress) then
                 ! calculate stress
                 !call total_stress(ibox0)
            endif

            !stats(ibox0)%nsample = stats(ibox0)%nsample + 1

            call samplestats(ibox0, job%sysequil, iter, &
                             energytot(ibox0), enthalpytot(ibox0), energyrcp(ibox0), energyreal(ibox0), &
                             energyvdw(ibox0), energythree(ibox0), energypair(ibox0), energyang(ibox0), &
                             energyfour(ibox0), energymany(ibox0), energyext(ibox0), energymfa(ibox0), &
                             virialtot(ibox0), volume(ibox0), stats(ibox0))

            call sampletypes(ibox0, iter, stats(ibox0), job%sysequil, &
                             emoltot(ibox0,:), emolrcp(ibox0,:), emolrealnonb(ibox0,:), emolrealbond(ibox0,:), &
                             emolrealself(ibox0,:), emolrealcrct(ibox0,:), emolvdwn(ibox0,:), emolvdwb(ibox0,:), &
                             emolpair(ibox0,:), emolthree(ibox0,:), emolang(ibox0,:), emolfour(ibox0,:), &
                             emolmany(ibox0,:), emolext(ibox0,:))

            if (iter >= job%sysequil) then

                if(job%lzdensity .and. mod(iter,job%zden_freq) == 0) call sample_zdensity(ibox0, stats(ibox0))

                if(job%lrdfs .and. mod(iter,job%rdfs_freq) == 0) call sample_rdfs(ibox0, stats(ibox0),job%lrdfx)

                !AB: not clear what is sampled and collected - ???
                !sample the displacements
                !if (job%lsample_displacement) &
                !    call sample_displacements(job%sample_disp_avg, iter, job%sysequil, ibox0, stats(ibox0))

            endif

            if( is_slit_mfa .and. mod(iter,job%zden_freq) == 0) &
                call sample_MFA_charge_zdens(ibox0, cfgs, volume(ibox0))

            !call check_mol_coms(ibox0)

        enddo

        !AB: originally the stats were output at the same frequency as printing into OUTPUT file(s)
        !AB: it is though more convenient to have a unique frequency to dump the stats
        if(master .and. mod(iter,job%statsout) == 0) then

            !TU: For PSMC output the active phase/box number to PTFILE.* before the statistics for
            !TU: each box are output to PTFILE.* below (via the procedure 'corout')
            if( fed%flavor == FED_PS ) write(uplt,"(3x,i1)") ib_act

            !AB: not clear what is sampled and collected - ???
            !if( job%lsample_displacement ) call open_nodefiles('DISPLA', udsp, ierr)

            do ibox0 = 1, nconfig

                call corout(iter, stats(ibox0))

                !AB: not clear what is sampled and collected - ???
                !if (job%lsample_displacement) call write_displacements(ibox0, stats(ibox0))

            enddo

            !AB: flush (write/close/reopen) the file (with inside check for 'master')
            !if( mod(iter,job%statsout*10) == 0 ) then 

                call open_nodefiles('PTFILE', uplt, ierr, 'add')

                if( job%lzdensity ) then

                    call open_nodefiles('ZDENSY', uzdn, ierr)

                    do i = 1, nconfig

                       call print_zdens(i, iter, stats(i))

                       if( is_slit_mfa ) then
                           call print_MFA_charge_zdens(job, cfgs, i, iter,stats(i))

                           !AB: need to update the Coulomb energy due to MFA variation
                           !if( mfa_type < 0) &
                           call update_mfa_energy(i, energymfa(i), energytot(i))
                       end if

                    end do

                    close(uzdn)

                endif

                if( job%lrdfs ) then

                    call open_nodefiles('RDFDAT', urdf, ierr)

                    do i = 1, nconfig

                       call print_rdfs(i, iter, stats(i))

                        end do

                        close(urdf)

                end if

                !AB: if V-sampling and collecting P(V) dump/flush the stats
                !if( master .and. job % lsample_volume ) then 
                if( job % lsample_volume ) then 

                    !AB: release the file unit to use it in write_volhist(job) for NPTDAT
                    close(unpt)

                    call write_volhist(job,iter)

                    !AB: reopen VOLDAT file for adding more V-samples (by appending)
                    call open_nodefiles('VOLDAT', unpt, ierr, 'add')

                end if

            !end if

        endif

        !TU: For PSMC...
        if( fed%flavor == FED_PS ) then

            !TU: Output PSMC information to PSDATA* every psmc%data_freq moves
            !TU: (flushed at frequency 10* - see below)
            if( mod(iter,psmc%data_freq) == 0 ) then
                
                if( master ) &
                    call psmc_data_output(iter, ib_act, id_cur, param_cur, bias_vec(id_cur) )
                
                !TU: Flush (write/close/reopen) the file (with inside check for 'master')
                if( mod( iter, 10*psmc%data_freq ) == 0 ) then 
                    
                    if( master .and. fed%flavor == FED_PS ) then
                        
                        !TU: Close and open the file (by appending)   
                        close(upsmc)
                        call open_nodefiles('PSDATA', upsmc, ierr, 'add')
                        
                    end if
                    
                end if

            endif

            !TU: For PSMC: Check that the displacements haven't become too large if psmc%melt_check is .true.
            !TU: every psmc%melt_freq moves. Print results of check to OUTPUT*
            if( psmc%melt_check .and. mod(iter,psmc%melt_freq) == 0 ) then

                call psmc_melt_check(job, iter) 

            end if

        end if

        
        !TU: For periodic dump of orientations and COMs...
        if( job%useorientation .and. job%sample_orientation ) then

            if( mod(iter,job%sample_orientation_freq) == 0 ) then
                
                if( master ) then
                    
                    do ibox0 = 1, nconfig
                        
                        !TU: Note that orientations must be set before being dumped    
                        call set_orientations(ibox0) 
                        call dump_orientations(uorient, ibox0, iter)
                        
                    end do
                    
                end if
                
                !TU: Flush (write/close/reopen) the file (with inside check for 'master')
                if( mod( iter, 10*job%sample_orientation_freq ) == 0 ) then 
                    
                    if( master ) then
                        
                        !TU: Close and open the file (by appending)   
                        close(uorient)
                        call open_nodefiles('ORIENT', uorient, ierr, 'add')
                        
                    end if
                    
                end if
                
            end if

        end if


        !TU: For periodic output to YAMLDATA
        if( job%useyamldata ) then

            if( mod(iter,job%yamldata_freq) == 0 ) then

                if( master ) then

                   if( fed%flavor == FED_PS ) then

                       !TU: Output data for the active box only in PSMC
                       call yaml_write_frame(ib_act, uyaml, job, iter, &
                            energytot, enthalpytot, energyrcp, energyreal, energyvdw, energythree, &
                            energypair, energyang, energyfour, energymany, energyext, energymfa, &
                            virialtot, volume )

                   else if( job%is_gibbs_ensemble ) then

                       !TU: For the Gibbs ensemble output the data for both boxes to different YAMLDATA
                       !TU: files
                       call yaml_write_frame(1, uyaml, job, iter, &
                            energytot, enthalpytot, energyrcp, energyreal, energyvdw, energythree, &
                            energypair, energyang, energyfour, energymany, energyext, energymfa, &
                            virialtot, volume )
                       call yaml_write_frame(2, uyaml2, job, iter, &
                            energytot, enthalpytot, energyrcp, energyreal, energyvdw, energythree, &
                            energypair, energyang, energyfour, energymany, energyext, energymfa, &
                            virialtot, volume )
                       
                   else

                       !TU: In all other cases output the data for box 1 only: the only time more than one 
                       !TU: per MPI workgroup is in the Gibbs ensemble and for PSMC
                       call yaml_write_frame(1, uyaml, job, iter, &
                            energytot, enthalpytot, energyrcp, energyreal, energyvdw, energythree, &
                            energypair, energyang, energyfour, energymany, energyext, energymfa, &
                            virialtot, volume )

                   end if

                end if

                !TU: Flush (write/close/reopen) the file (with inside check for 'master')
                if( mod( iter, 10*job%yamldata_freq ) == 0 ) then 


                    !TU: Close and open the YAMLDATA files (by appending)   
                    if( master ) then

                        if(job % is_gibbs_ensemble) then

                            close(uyaml)
                            call open_nodefiles('YAMLDATA-1', uyaml, ierr, 'add')
                            close(uyaml2)
                            call open_nodefiles('YAMLDATA-2', uyaml2, ierr, 'add')

                        else

                            close(uyaml)
                            call open_nodefiles('YAMLDATA', uyaml, ierr, 'add')

                        end if

                    end if

                end if

            end if

        end if


        if(mod(iter,job%sysprint) == 0) then

            if( master ) then 

                call mptime(eltime)

                write(text,"('Iteration ',i12,' - elapsed time: ')") iter
                call time_stamp(trim(text), eltime-tzero, eltime, uout)

                !write(uout,"(//,1x,'Iteration ',i10,' - elapsed time (seconds)',1x,f10.4,//)") &
                !           iter, eltime-tzero

                !write(uout,"(//,1x,'Iteration ',i10,' box ',i4,' - elapsed time (seconds)',1x,f10.4,//)") &
                !           iter, ibox, eltime-tzero

                !TU: For PSMC output the active phase/box number to OUTPUT.* before the statistics
                !TU: for each box are output to OUTPUT.* below (via the procedure 'checkpoint').
                if( fed%flavor == FED_PS ) write(uout,"(1x,'PSMC active box : ',i1,/)") ib_act

            end if

            do ibox0 = 1, nconfig

                call checkpoint(job%sysprint, lines, npage, iter, stats(ibox0))

                if (job%semiwidomatms.and.iter > job%sysheat) then
                    call checkpoint_semiwidom(stats(ibox0), semitype1, semitype2, job%numsemiwidomatms, job % systemp)
                endif

                if (job%semigrandatms.and.iter > job%sysheat) then
                    call checkpoint_semigrand(stats(ibox0), semitype1, semitype2, job%numsemigrandatms, job % systemp)
                endif

                call checkpointtypes(stats(ibox0), iter, job%sysequil, job%lmoldata)

                !AB: no point to synch stats in PTFILE with OUTPUT (most often stats need to have more data!)
                !call corout(iter, stats(ibox0))

            enddo

            lines = lines + 1

            !AB: only master(s) open/close/write into files (with inside check)
            call open_nodefiles('OUTPUT', uout, ierr, 'add')

        endif


        !TU: Neighbour list checks. 
        !TU: NOTE: Neighbour list checks should be performed before energy checks, because
        !TU: the check_energy procedure updates the neighbour lists: checking the neighbour
        !TU: lists immediately after they have been updated is a waste of time
        if( job%nbrlistcheck .and. (mod(iter,job%nbrlistcheckfreq) == 0) ) then

           do ibox0 = 1, nconfig

               call checknbrlists(ibox0, job%shortrangecut, iter)

           end do

        end if

        !TU: Check species counts are correct
        if( job%speciescountcheck .and. (mod(iter,job%speciescountcheckfreq) == 0) ) then

           do ibox0 = 1, nconfig

               call check_species_counters(ibox0)

           end do

        end if

        if(mod(iter,job%syscheck) == 0) then

            do ibox0 = 1, nconfig

                !write(uout,"(/,a,i2)")"The last MC step type : ",selection

                !AB: check the accuracy of MC algorithm = U_recalculated - U_accumulated (for all contributions)

                call check_energy(ibox0, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                     energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                     energymfa, emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, emolvdwn, &
                     emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext, volume)

            enddo

            !AB: only master(s) open/close/write into files (with inside check)
            call open_nodefiles('OUTPUT', uout, ierr, 'add')

        endif

        !write out archive data
        if (sample_coords .and. mod(iter,job%sysdump) == 0) then

            !AB: all boxes are written in the same REVCON file, just like input in CONFIG (should be split!!!)
            call dump_revcon(job%revcon_format, iter, energytot)

            if( master ) then 

                !AB: only master(s) open/close/write into files (with inside check)
                call open_nodefiles('RSTART', urest, ierr)

                call dump_restart(iter, ibox, job%moveatm, job%movemol, job%rotatemol)
                close(urest)

                if( job%archive_format < DCD ) then

                    !TU: For PSMC output the configuration of the active box only (not the passive
                    !TU: box which is essentially a workspace). Otherwise just output the configuration
                    !TU: of box 1
                    if( fed%flavor == FED_PS) then

                        call dump_config(uarch, ib_act, job%archive_format, energytot(ib_act), iter)

                    else

                        call dump_config(uarch, 1, job%archive_format, energytot(1), iter)

                    end if

                    flush(uarch)

                    !TU: For the Gibbs ensemble we must additionally output the configuration of
                    !TU: box 2 to another archive file
                    if( job%is_gibbs_ensemble ) then

                        call dump_config(uarch2, 2, job%archive_format, energytot(2), iter)
                        flush(uarch)

                     end if

                end if

                if( job%archive_format == 3 .or. job%archive_format > DLP4 ) then

                   if( iclose_dcd == 0 ) then
                             
                       if( job%is_gibbs_ensemble ) then

                           !TU: NB: DCD output in the Gibbs ensemble does not work because
                           !TU: 'write_config_dcd' does not fully support writing DCD format
                           !TU: to two different units. Hence the following does not work.
                           !TU: An error is raised in 'control_module.f90' stopping Gibbs
                           !TU: being used in conjunction with DCD

                           call write_config_dcd('TRAJECTORY-1', udcd, job%dcd_form, &
                                                 1, icell_dcd, matoms_dcd, mframes_dcd, &
                                                 iter, ifstep_dcd, iclose_dcd)

                           call write_config_dcd('TRAJECTORY-2', udcd2, job%dcd_form, &
                                                 2, icell_dcd, matoms_dcd, mframes_dcd, &
                                                 iter, ifstep_dcd, iclose_dcd)

                       else


                           !TU: NB: DCD output in PSMC not work as expected because
                           !TU: 'write_config_dcd' names the DCD file 'TRAJECTORY-2' in
                           !TU: PSMC. Hence the following does not work for now.
                           !TU: An error is raised in 'psmc_module.f90' stopping PSMC
                           !TU: being used in conjunction with DCD

                           !TU: For PSMC output the configuration of the active box only 
                           !TU: (not the passive box which is essentially a workspace). 
                           !TU: Otherwise just output the configuration of box 1
                           if( fed%flavor == FED_PS) then

                               call write_config_dcd('TRAJECTORY', udcd, job%dcd_form, &
                                                     ib_act, icell_dcd, matoms_dcd, mframes_dcd, &
                                                     iter, ifstep_dcd, iclose_dcd)

                           else

                               call write_config_dcd('TRAJECTORY', udcd, job%dcd_form, &
                                                     1, icell_dcd, matoms_dcd, mframes_dcd, &
                                                     iter, ifstep_dcd, iclose_dcd)

                           end if

                       end if

                    end if

                end if

            endif

        !TU: Extra loop of periodic dumps to REVCON.
        else if(job%extrarevcon .and.  mod(iter,job%extrarevconfreq) == 0) then 

            !AB: all boxes are written in the same REVCON file, just like input in CONFIG (should be split!!!)
            call dump_revcon(job%revcon_format, iter, energytot)

        endif

        if (iter >= job%sysequil) then

            if (job%lsample_energy .and. mod(iter,job%sample_energy_freq) == 0) then

                do i = 1, nconfig

                     call dump_energies(job, i, iter)

                enddo

            endif

        endif

        !find out if there is enough time to loop again
        call mptime(eltime)

        if( (job%jobtime - (eltime - tzero)) < job%closetime ) then 
        
            !write(text,"('WARNING: time limit reached - elapsed time: ')")
            call time_stamp('WARNING: time limit reached - elapsed time: ', eltime-tzero+tinit, eltime, uout)
            
            exit !stop running, calculate averages and total energy
        end if

    enddo ! end of iterations loop

    !print out final averages and fluctuations
    if (master) then

        write(uout,"(/,/,1x,100('='))")
        write(uout,*) "                         averages and fluctuations"
        !write(uout,"(/,/,1x,100'-')")

        if (job%lzdensity) then

            call open_nodefiles('ZDENSY', uzdn, ierr)

            do i = 1, nconfig

                call print_zdens(i, iter, stats(i))

            enddo

            close(uzdn)

        endif

        if (job%lrdfs) then

            call open_nodefiles('RDFDAT', urdf, ierr)

            do i = 1, nconfig

                call print_rdfs(i, iter, stats(i))

            enddo

            close(urdf)

        endif

!JG: The following check_energy is within if(master) but call gsum within subroutines
! This leads to deadlock so conditional must be ended

    endif

!JG: Then check_energy may proceed

    do i = 1, nconfig

        !AB: check the accuracy of MC algorithm = U_recalculated - U_accumulated (for all contributions)
        if( job%syscheck == 0 .or. iter < job%syscheck ) &
            call check_energy(i, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, &
                 energyreal, energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, &
                 energymfa, emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, emolvdwn, &
                 emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext, volume)

        call lastsummary(i, stats(i))

        if (job%semiwidomatms) call lastsummary_semiwidom(i, stats(i), job%numsemiwidomatms, &
                  semitype1, semitype2, job % systemp)
        if (job%semigrandatms) call lastsummary_semigrand(i, stats(i), job%numsemigrandatms, &
                  semitype1, semitype2, job % systemp)

        call lastsummarytypes(i, stats(i), job%lmoldata)

    enddo

!JG: and enter another if(master) to complete output

    if ( master ) then

        write(uout,"(/,/,1x,'----------------------------------------------------------------------------')")
        write(uout,*) "                         final energies "
        write(uout,"(1x,'----------------------------------------------------------------------------')")

    endif

    do i = 1, nconfig

        !AB: is re-calculation of the final energies needed?
!        goto 999

        if(job%coultype(i) > 0 .or. job%coultype(i) < -1) then

            call setup_ewald(i, job)

            if(job%coultype(i) == 1) call total_recip(i, energyrcp(i), mrcp, virialtot(i))

        endif

        do im = 1, number_of_molecules

            emoltot(i,im) = 0.0_wp
            mrealnonb(im) = 0.0_wp
            mrealbond(im) = 0.0_wp
            mrcp(im) = 0.0_wp
            mvdwn(im) = 0.0_wp
            mvdwb(im) = 0.0_wp
            mpair(im) = 0.0_wp
            mthree(im) = 0.0_wp
            mang(im) = 0.0_wp
            mfour(im) = 0.0_wp
            mmany(im) = 0.0_wp

        enddo

        !calculate final energy
        if(job%uselist) then
        
            !AB: do not reset NL (which is very time-consuming but not necessary due to previous checks)
            !call setnbrlists(i, job%shortrangecut, job%verletshell, job%linklist, .false.)
            
            call total_energy(i, job%coultype(i), job%vdw_cut, job%shortrangecut, job%dielec, &
                 energyreal(i), energyvdw(i), energythree(i), energypair(i), energyang(i), energyfour(i), &
                 energymany(i), energyext(i), energymfa(i), virialtot(i), &
                 mrealnonb, mrealbond, mrealself, mrealcrct, mvdwn, mvdwb, mpair, &
                 mthree, mang, mfour, mmany, mext)

        else

            call total_energy_nolist(i, job%coultype(i), job%vdw_cut, job%shortrangecut, job%dielec, &
                 energyreal(i), energyvdw(i), energythree(i), energypair(i), energyang(i), energyfour(i), &
                 energymany(i), energyext(i), energymfa(i), virialtot(i), &
                 mrealnonb, mrealbond, mrealself, mrealcrct, mvdwn, mvdwb, mpair, &
                 mthree, mang, mfour, mmany, mext)

        endif

!999     continue

        energytot(i) = energyrcp(i) + energyreal(i) + energyvdw(i) + energypair(i) + energythree(i) + &
                       energyang(i) + energyfour(i) + energymany(i) + energyext(i) + energymfa(i)

        call printtotal(i, energytot(i), energyrcp(i), energyreal(i), energyvdw(i), energypair(i), energythree(i), &
             energyang(i), energyfour(i), energymany(i), energyext(i), energymfa(i), virialtot(i), &
             volume(i), job%energyunit)

        if( master ) then
            write(uout,*)
            write(uout,"(1x,'long-range correction (vdw)     ',e20.10)") &
                 vdw_lrc_energy( 1.0_wp/volume(i), cfgs(i)%nums_elemts ) / job%energyunit
        end if


        do im = 1, number_of_molecules

            emolrealnonb(i,im) = mrealnonb(im)
            emolrealbond(i,im) = mrealbond(im)
            emolrealself(i,im) = mrealself(im)
            emolrealcrct(i,im) = mrealcrct(im)
            emolrcp(i,im) = mrcp(im)
            emolvdwn(i,im) = mvdwn(im)
            emolvdwb(i,im) = mvdwb(im)
            emolpair(i,im) = mpair(im)
            emolthree(i,im) = mthree(im)
            emolang(i,im) = mang(im)
            emolfour(i,im) = mfour(im)
            emolmany(i,im) = mmany(im)
            emolext(i,im) = mext(im)

            emoltot(i,im) = mrealnonb(im) + mrealbond(im) + mrcp(im) + mvdwn(im) + mvdwb(im) + mpair(im)  &
                             + mthree(im) + mang(im) + mfour(im) + mmany(im) + mext(im) + mrealself(im) + mrealcrct(im)

            call print_moltotal(im, emoltot(i,im), mrealnonb(im), mrealbond(im), mrealself(im), mrealcrct(im), mrcp(im), &
                                mvdwn(im), mvdwb(im), mpair(im), mthree(im), mang(im), mfour(im), mmany(im), mext(im), &
                                job%energyunit)

        enddo

        if( job%uselist .and. job%lauto_nbrs ) then
            if( master ) &
                write(uout,"(/,1x,'total no of NL auto-resets in replica    :',2i10)") i, nbrlist_resets(i)
        end if

    enddo

    if( iter > job%maxiterations ) iter = job%maxiterations

    if( master ) then

        write(uout,"(/,/,1x,'----------------------------------------------------------------------------')")
        write(uout,*) "                         processing data "
        write(uout,"(1x,'----------------------------------------------------------------------------')")

        if(job%moveatm) then

            !write(uout,"(/,1x,'total no of atom moves         :',i10)") total_atm_moves
            write(uout,"(/,1x,'total no of attempted & empty atom moves :',2i10)") total_atm_moves, empty_atm_moves
            write(uout,"(  1x,'successful no of atom moves & acceptance :',i10,f16.8,/)") successful_atm_moves, &
                  real(successful_atm_moves,wp)/real(total_atm_moves,wp)

            do i = 1, number_of_elements

                tag1 = set_species_type(eletype(i))

                write(uout,"(1x,'displacement (Angstroms) for ',a8,1x,a1, ' : ',10f8.4)") element(i), tag1, distance_atm_max(i,:)

            enddo

        endif

        if(job%movemol) then

            !write(uout,"(/,1x,'total no of mol moves          :',i10)") total_mol_moves
            write(uout,"(/,1x,'total no of attempted & empty mol moves  :',2i10)") total_mol_moves, empty_mol_moves
            write(uout,"(  1x,'successful no of mol moves & acceptance  :',i10,f16.8,/)") successful_mol_moves, &
                  real(successful_mol_moves,wp)/real(total_mol_moves,wp)

            do i = 1, number_of_molecules
                write(uout,"(1x,'displacement (Angstroms) for ',a8, ' : ',f8.4)") uniq_mol(i)%molname, distance_mol_max(i)
            enddo
            
        endif

        if(job%rotatemol) then

            write(uout,"(/,1x,'total no of mol rotations      :',i10)") total_mol_rots
            write(uout,"(  1x,'successful no of mol rotations :',i10,f16.8,/)") successful_mol_rots, &
                  real(successful_mol_rots,wp)/real(total_mol_rots,wp)

            do i = 1, number_of_molecules
                write(uout,"(1x,'angle of rot for (degrees) ',a8, ' : ',f8.4)") uniq_mol(i)%molname, rotate_mol_max(i) / TORADIANS
            enddo
        endif
        
        if(job%rotateatm) then

            call print_rotate_atom_stats()
            
        endif
        
        if(job%type_vol_move /= 0 ) then

            if(job%type_vol_move == NPT_GIBBS) then

                write(uout,"(/,1x,'total no of Gibbs volume moves       :',i10)") totalvolchanges
                write(uout,"(  1x,'successful no of Gibbs volume moves  :',i10,f16.8,/)") successfulvolchange, &
                      real(successfulvolchange,wp)/real(totalvolchanges,wp)

                write(uout,"(/,1x,'max volume variations in cell matrix :',/)")
                do i = 1, 9
                    write(uout,"(1x,'index',1x,i1,5x,10f8.4)")i, maxvolchange(i,:)
                enddo

            else

                write(uout,"(/,1x,'total no of volume moves       :',i10)") totalvolchanges
                write(uout,"(  1x,'successful no of volume moves  :',i10,f16.8,/)") successfulvolchange, &
                      real(successfulvolchange,wp)/real(totalvolchanges,wp)

                write(uout,"(/,1x,'max volume variations in cell matrix :',/)")
                do i = 1, 9
                    write(uout,"(1x,'index',1x,i1,5x,10f8.4)")i, maxvolchange(i,:)
                enddo

            end if

            if( job%lsample_volume ) then 
               close(unpt)
               call write_volhist(job,iter-1)
            endif


        endif

        if(job%swapatoms) then

            do ibox = 1, nconfigs

                if(nconfigs > 1 ) write(uout,"(/,1x,'atom swap info for box ',i10)") ibox
                    
                do i = 1, job%numswap

                    write(uout,"(/,1x,'total no of attempted atom swaps (',a,' to ',a,') = ',i10)") &
                        trim(job%swapele1(i)), trim(job%swapele2(i)),attemptedswaps_atm(i,ibox)
                    write(uout,"(1x,'successful no of atom swaps (',a,' to ',a,')      = ',i10,f16.8)") &
                        trim(job%swapele1(i)), trim(job%swapele2(i)), successfulswaps_atm(i,ibox), &
                        real(successfulswaps_atm(i,ibox),wp)/real(attemptedswaps_atm(i,ibox),wp)

                end do

            end do

        endif

        if(job%swapmols) then

            write(uout,"(/,1x,'total no of attempted swaps    = ',i10)") attemptedswaps
            write(uout,"(  1x,'successful no of swaps         = ',i10,f16.8)") successfulswaps, &
                  real(successfulswaps,wp)/real(attemptedswaps,wp)

        endif

        if(job%semiwidomatms) then

            write(uout,"(/,1x,'total no of semiwidom atom inserts = ',i10)") nint(nummutations)

        endif

        if(job%semigrandatms .or. job%semigrandmols) then

            write(uout,"(/,1x,'attempted forward mutations    : ',i10)") attforwardmutations
            write(uout,"(  1x,'successful forward mutations   : ',i10,f16.8)") forwardmutations, &
                  real(forwardmutations,wp)/real(attforwardmutations,wp)

            write(uout,"(/,1x,'attempted backward mutations   : ',i10)") attbackmutations
            write(uout,"(  1x,'successful backward mutations  : ',i10,f16.8)") backmutations, &
                  real(backmutations,wp)/real(attbackmutations,wp)

            write(uout,"(/,1x,'empty mutations                : ',i10)") emptymutations

        endif

        if(job%gcmcatom) then

            write(uout,"(/,1x,'attempted atom inserts  = ',i10)")  attemptedgcatminsert
            write(uout,"(  1x,'successful atom inserts = ',i10,f16.8)") sucessfulgcatminsert, &
                  real(sucessfulgcatminsert,wp)/real(attemptedgcatminsert,wp)

            write(uout,"(/,1x,'attempted & empty atom deletes  = ',2i10)") attemptedgcatmremove, &
                  emptygcatmremove
            write(uout,"(  1x,'successful atom deletes = ',i10,f16.8)") sucessfulgcatmremove, &
                  real(sucessfulgcatmremove,wp)/real(attemptedgcatmremove,wp)

        endif

        if(job%gcmcmol) then

            write(uout,"(/,1x,'attempted molecule inserts  = ',i10)")  attemptedgcmolinsert
            write(uout,"(  1x,'successful molecule inserts = ',i10,f16.8)") sucessfulgcmolinsert, &
                  real(sucessfulgcmolinsert,wp)/real(attemptedgcmolinsert,wp)

            write(uout,"(/,1x,'attempted & empty molecule deletes  = ',2i10)") attemptedgcmolremove, &
                  emptygcmolremove
            write(uout,"(  1x,'successful molecule deletes = ',i10,f16.8)") sucessfulgcmolremove, &
                  real(sucessfulgcmolremove,wp)/real(attemptedgcmolremove,wp)

        endif


        if(job%gibbsatomtran) then

            write(uout,"(/,1x,'attempted atom transfers box1 to box2  = ',i10)") attemptedgibbstrans_atmb1tob2
            write(uout,"(/,1x,'successful atom transfers box1 to box2 = ',i10,f16.8)") successfulgibbstrans_atmb1tob2, &
                  real(successfulgibbstrans_atmb1tob2,wp)/real(attemptedgibbstrans_atmb1tob2,wp)

            write(uout,"(  1x,'attempted atom transfers box2 to box1  = ',i10)") attemptedgibbstrans_atmb2tob1
            write(uout,"(  1x,'successful atom transfers box2 to box1 = ',i10,f16.8)") successfulgibbstrans_atmb2tob1, &
                  real(successfulgibbstrans_atmb2tob1,wp)/real(attemptedgibbstrans_atmb2tob1,wp)

        endif

        if(job%gibbsmoltran) then

            write(uout,"(/,1x,'attempted molecule transfers box1 to box2  = ',i10)") attemptedgibbstrans_molb1tob2
            write(uout,"(/,1x,'successful molecule transfers box1 to box2 = ',i10,f16.8)") successfulgibbstrans_molb1tob2, &
                  real(successfulgibbstrans_molb1tob2,wp)/real(attemptedgibbstrans_molb1tob2,wp)

            write(uout,"(  1x,'attempted molecule transfers box2 to box1  = ',i10)") attemptedgibbstrans_molb2tob1
            write(uout,"(  1x,'successful molecule transfers box2 to box1 = ',i10,f16.8)") successfulgibbstrans_molb2tob1, &
                  real(successfulgibbstrans_molb2tob1,wp)/real(attemptedgibbstrans_molb2tob1,wp)

        endif

        if (job%repexch) then

            write(uout,"(/,1x,'attempted replica exchange moves   = ',i10)") attempted_rep_exch
            write(uout,"(1x,  'accepted swaps in temperature down = ',i10)") successful_rep_exch(1)
            write(uout,"(1x,  'accepted swaps in temperature up   = ',i10)") successful_rep_exch(2)
            write(uout,"(1x,  'rejected swaps by master process   = ',i10)") rejected_rep_exch

        endif

        !AB: Output FED move counters
        if( job%fedcalc ) call fed_moves_output(nconfig) 

        !TU: Output PSMC move counters.
        if( fed%flavor == FED_PS ) then

           if( psmc%switch_freq > 0 ) then

              if( master ) then
                 
                 write(uout, "(/,1x,'attempted  phase switch moves = ',i10)") total_switch_moves
                 write(uout, "(  1x,'successful phase switch moves = ',i10,f16.8)") successful_switch_moves, &
                       real(successful_switch_moves,wp)/real(total_switch_moves,wp)

              end if

           end if

        end if

        !AB: write the last config-snapshots (used for restarting too! - so ensure syncing with RSTART)
        call dump_revcon(job%revcon_format, iter, energytot)

        !AB: write the last restart-point data
        if( ibox /= 0 ) then
            call open_nodefiles('RSTART', urest, ierr)
            call dump_restart(job%maxiterations, ibox, job%moveatm, job%movemol, job%rotatemol)
            close(urest)
        endif

        !AB: close the long open files
        close(urevi)
        close(uplt)

        !AB: close the conditionally open files
        if( job%lsample_energy ) close(ueng)
        if( sample_coords .and. job%archive_format /= DCD ) close(uarch)   
        if( sample_coords .and. job%archive_format /= DCD .and. job%is_gibbs_ensemble ) close(uarch2)

        !TU: Close the PSMC output file
        if( fed%flavor == FED_PS ) close(upsmc)

        !TU: Close the orientations output file
        if( job % useorientation ) close(uorient)

        !TU: Close the YAMLDATA output file(s)
        if( job % useyamldata ) close(uyaml)
        if( job % useyamldata .and. job%is_gibbs_ensemble ) close(uyaml2)
        

    endif

    !AB: not clear what is sampled and collected - ???
    !if (job%lsample_displacement) then
    !    call open_nodefiles('DISPLA', udsp, ierr)
    !    do ibox0 = 1, nconfig
    !        call write_displacements(ibox0, stats(ibox0))
    !    enddo
    !    if (master) close(udsp)
    !endif

    call gsync_world()

    !AB: sum up worldwise
    if( job%repexch ) then

        call gsum_world(attempted_rep_exch)
        call gsum_world(successful_rep_exch)
        call gsum_world(rejected_rep_exch)

        !attempted_rep_exch  = attempted_rep_exch/wkgrp_size
        !rejected_rep_exch   = rejected_rep_exch/wkgrp_size
        successful_rep_exch = successful_rep_exch/wkgrp_size

        if( idnode == 0 ) then

            write(uout,"(/,1x,'attempted replica exchange moves   (tot) = ',i10)") attempted_rep_exch

            write(uout,"(1x,  'accepted swaps in temperature down (tot) = ',i10)") successful_rep_exch(1)
            write(uout,"(1x,  'accepted swaps in temperature up   (tot) = ',i10)") successful_rep_exch(2)
            write(uout,"(1x,  'rejected swaps by master processes (tot) = ',i10)") rejected_rep_exch

        endif

    endif

    ! AB: clean up the memory


    if (allocated(ratio)) deallocate(ratio)
    if (allocated(vratio)) deallocate(vratio)
    if (allocated(mratio)) deallocate(mratio)
    if (allocated(interrupt)) deallocate(interrupt)

    ! AB: deallocate cell arrays
    call dealloc_configs

    !AB: volume distribution histogram(s) - deallocate!
    if( job%lsample_volume .and. allocated(volhist) ) deallocate(volhist)

    if( is_parallel ) call write_mpi_stats

    !final run time
    call mptime(eltime)

    if (master) then
        !write(uout,"(/,1x,'total elapsed time (seconds)',1x,f15.4)") eltime-tzero
        !write(text,"('Iteration ',i10,' - elapsed time: ')") iter
        call time_stamp('pure simulation time (excl. init.): ', eltime-tzero, eltime, uout)
        call time_stamp('total elapsed time   (incl. init.): ', eltime-tzero+tinit, eltime, uout)
    end if

end subroutine


subroutine setenergies

     use kinds_f90
     use species_module, only : number_of_molecules

     implicit none


     allocate(virialtot(nconfig))
     allocate(enthalpytot(nconfig))
     allocate(energytot(nconfig))
     allocate(energyrcp(nconfig))
     allocate(energyreal(nconfig))
     allocate(energyvdw(nconfig))
     allocate(energypair(nconfig))
     allocate(energythree(nconfig))
     allocate(energyang(nconfig))
     allocate(energyfour(nconfig))
     allocate(energymany(nconfig))
     allocate(energyext(nconfig))
     allocate(energymfa(nconfig))
     allocate(volume(nconfig))
     allocate(emoltot(nconfig, number_of_molecules))
     allocate(emolrcp(nconfig, number_of_molecules))
     allocate(emolrealnonb(nconfig, number_of_molecules))
     allocate(emolrealbond(nconfig, number_of_molecules))
     allocate(emolrealself(nconfig, number_of_molecules))
     allocate(emolrealcrct(nconfig, number_of_molecules))
     allocate(emolvdwn(nconfig, number_of_molecules))
     allocate(emolvdwb(nconfig, number_of_molecules))
     allocate(emolpair(nconfig, number_of_molecules))
     allocate(emolthree( nconfig, number_of_molecules))
     allocate(emolang(nconfig, number_of_molecules))
     allocate(emolfour(nconfig, number_of_molecules))
     allocate(emolmany(nconfig, number_of_molecules))
     allocate(emolext(nconfig, number_of_molecules))

     virialtot = 0.0_wp
     enthalpytot = 0.0_wp
     energytot = 0.0_wp
     energyrcp = 0.0_wp
     energyreal = 0.0_wp
     energyvdw = 0.0_wp
     energypair = 0.0_wp
     energythree = 0.0_wp
     energyang = 0.0_wp
     energyfour = 0.0_wp
     energymany = 0.0_wp
     volume = 0.0_wp

     emoltot = 0.0_wp
     emolrcp = 0.0_wp
     emolrealnonb = 0.0_wp
     emolrealbond = 0.0_wp
     emolrealself = 0.0_wp
     emolrealcrct = 0.0_wp
     emolvdwn = 0.0_wp
     emolvdwb = 0.0_wp
     emolpair = 0.0_wp
     emolthree = 0.0_wp
     emolang = 0.0_wp
     emolfour = 0.0_wp
     emolmany = 0.0_wp

end subroutine


!**
!    setup rigid bodies- this assumes that if a molecule type is translated or rotated
!    then it is a rigid body
subroutine setup_rigid_bodies(job)

    use control_type
    use kinds_f90
    use species_module, only : number_of_molecules, uniq_mol

    implicit none

    type(control), intent(inout) :: job
    integer :: j, k

    if (job%movemol) then

        !set flag for moveable molecules
        do j = 1, number_of_molecules

            do k = 1, job % nummolmove

                if (uniq_mol(j)%molname == job % molmover(k)) then

                    uniq_mol(j)%rigid_body = .true.

                endif

            enddo

         enddo

    endif

    if (job%rotatemol) then

        !set flag for moveable molecules
        do j = 1, number_of_molecules

            do k = 1, job % nummolrot

                if (uniq_mol(j)%molname == job % molroter(k)) then

                    uniq_mol(j)%rigid_body = .true.

                endif

            enddo

         enddo

    endif

end subroutine

!> setup the monte carlo arrays and zero
subroutine setupmc(job)

     use kinds_f90
     use constants_module
     use control_type
     use control_module
     use cell_module
     use latticevectors_module, only : invertlatvec
     use species_module, only : number_of_elements, number_of_molecules, element, atm_charge, &
                                uniq_mol, eletype, set_species_type, atm_mass
     use atom_module, only : set_atom_type
     use bondlist_type
     use bondlist_module, only : print_bondlist
     use angle_module, only : numang
     use anglist_module, only : print_anglist
     use nbrlist_module, only : alloc_exclist_array
     use comms_mpi_module, only : master
     use mc_moves
     use vol_moves,   only : vol_max, max_vol_bin
     use psmc_moves, only  : initialise_psmc_counters
     use fed_calculus_module, only : fed
     !use parallel_loop_module, only : master

     implicit none

     type(control), intent(inout) :: job

     integer :: fail(5), i, j, k, elmid, nmol_atmove, ic, na, nb
     integer :: nstp(3)
     real (kind = wp) :: sstp(3), ra(3)  !scp: for scan

     character :: tag1*1, tag2*1
     logical :: found, founda

     if (master) then

         !write(uout,"(/,/,1x,'*********************************************************************')")
         write(uout,"(/,/,1x,50('='))")
         write(uout,"(a)")"                simulation parameters "
         write(uout,"(1x,50('='))")
         !write(uout,"(1x,'*********************************************************************',/)")

     endif

     fail(:) = 0

     ! print run parameters
     call printandcheck(job)

     !get inverse of lattice vectors
     call invertlatvec(cfgs(1)%vec)

     extpress = job%syspres

     ! total number of times NL had to be reset
     if( job%uselist .and. job%lauto_nbrs ) then 
         allocate(nbrlist_resets(nconfig))
         nbrlist_resets(:) = 0
     end if

     if (job%semigrandatms) then

        if (job%semigrandmols) call error(20)

        allocate(semitype1(job%numsemigrandatms))
        allocate(semitype2(job%numsemigrandatms))

        allocate(chem_pot_atm1(nconfig, job%numsemigrandatms))
        allocate(chem_pot_atm2(nconfig, job%numsemigrandatms))
        chem_pot_atm1 = 0.0
        chem_pot_atm2 = 0.0
        nummutations = 0.0
        backmutations = 0
        forwardmutations = 0
        attbackmutations = 0
        attforwardmutations = 0
        emptymutations = 0

        do j = 1, number_of_elements

            do k = 1, job%numsemigrandatms

               if (element(j) == job%semigrandele1(k) .and. eletype(j) == job%semigrand1_type(k)) then
                   semitype1(k) = j
               endif

               if (element(j) == job%semigrandele2(k) .and. eletype(j) == job%semigrand2_type(k)) then
                   semitype2(k) = j
               endif

            enddo
        enddo

        do k = 1, job%numsemigrandatms

            i = semitype1(k)
            j = semitype2(k)

            if (atm_charge(i) /= atm_charge(j)) call error(320)

        enddo

        if(master) write(uout,"(/,1x,'semi grand ensemble: types to be mutatted')")

        do i = 1, job%numsemigrandatms

            tag1 = set_species_type(job%semigrand1_type(i))
            tag2 = set_species_type(job%semigrand2_type(i))

            if(master) write(uout,'(1x,a8,1x,a1,1x,a8,1x,a1)') job%semigrandele1(i), tag1, job%semigrandele2(i), tag2

        enddo

        if (master) write(uout,"(/,1x,'delta mu (eV) ',f10.4)") job%deltatrans

        deltamu = job%deltatrans * job%energyunit


     endif

     if (job%semigrandmols) then

        if (job%semigrandatms) call error(20)

        allocate(semitype1(job%numsemigrandmol))
        allocate(semitype2(job%numsemigrandmol))

        allocate(chem_pot_atm1(nconfig, job%numsemigrandmol))
        allocate(chem_pot_atm2(nconfig, job%numsemigrandmol))
        chem_pot_atm1 = 0.0
        chem_pot_atm2 = 0.0
        nummutations = 0.0
        backmutations = 0
        forwardmutations = 0
        attbackmutations = 0
        attforwardmutations = 0

        do j = 1, number_of_molecules

            do k = 1, job%numsemigrandmol

               if (uniq_mol(j)%molname == job%semigrand1_mols(k)) then
                   semitype1(k) = j
               endif

               if (uniq_mol(j)%molname == job%semigrand2_mols(k)) then
                   semitype2(k) = j
               endif

            enddo
        enddo

        if(master) write(uout,"(/,1x,'semi grand ensemble for molecules: types to be mutatted')")

        do i = 1, job%numsemigrandmol

            if(master) write(uout,'(1x,a8,1x,a8)') job%semigrand1_mols(i), job%semigrand2_mols(i)

        enddo

        if (master) write(uout,"(/,1x,'delta mu (energy units) ',f10.4)") job%deltatrans

        deltamu = job%deltatrans * job%energyunit


     endif


     if (job%semiwidomatms) then

        allocate(semitype1(job%numsemiwidomatms))
        allocate(semitype2(job%numsemiwidomatms))

        allocate(chem_pot_atm1(nconfig, job%numsemiwidomatms))
        allocate(chem_pot_atm2(nconfig, job%numsemiwidomatms))
        chem_pot_atm1 = 0.0
        chem_pot_atm2 = 0.0
        nummutations = 0.0
        backmutations = 0
        forwardmutations = 0
        attbackmutations = 0
        attforwardmutations = 0

        do j = 1, number_of_elements

            do k = 1, job%numsemiwidomatms

               if (element(j) == job%semiwidomele1(k) .and. eletype(j) == job%semiwidom1_type(k)) then
                   semitype1(k) = j
               endif

               if (element(j) == job%semiwidomele2(k) .and. eletype(j) == job%semiwidom2_type(k)) then
                   semitype2(k) = j
               endif

            enddo
        enddo

        do k = 1, job%numsemiwidomatms

            i = semitype1(k)
            j = semitype2(k)

            if (atm_charge(i) /= atm_charge(j)) call error(320)

        enddo

        if(master) write(uout,"(/,1x,'semi widom ensemble: types to be mutatted')")

        do i = 1, job%numsemiwidomatms

            tag1 = set_species_type(job%semiwidom1_type(i))
            tag2 = set_species_type(job%semiwidom2_type(i))

            if(master) write(uout,'(1x,a8,1x,a1,1x,a8,1x,a1)') job%semiwidomele1(i), tag1, job%semiwidomele2(i), tag2

        enddo

     endif

!--- setup and zero arrays
    if (job%moveatm) then

        fail(:) = 0

        allocate(no_atm_moves(number_of_elements,nconfig), stat=fail(1))
        allocate(distance_atm_max(number_of_elements,nconfig), stat=fail(2))
        allocate(attempted_atm_moves(number_of_elements,nconfig), stat=fail(3))
        
        allocate(movetyp(job%numatmmove), stat=fail(4))

        if( any(fail > 0) ) call cry(uout,'', &
                    "ERROR: setupmc() failed to allocate arrays for atom moves !!!",999)

        no_atm_moves(:,:) = 0.0_wp
        distance_atm_max(:,:) = 0.0_wp
        attempted_atm_moves(:,:) = 0

        movetyp(:) = 0
        nmovetyp = 0

        !if this atom type can move then set the flag to 1

        founda = .false.
        nmol_atmove = 0

        elmid = 0

        do k = 1, job%numatmmove

            found = .false.

            elmid = 0

            do j = 1, number_of_elements

                if(element(j) == job%atmmover(k) .and. eletype(j) == job%atmmover_type(k)) then

                    nmovetyp = nmovetyp+1

                    movetyp(k) = j

                    elmid = j

                    found = .true.

                    distance_atm_max(j,:) = job%iondisp

!debugging
                    !!write(uout,"(/,1x,a,i5,2a,2(a,i5),a,10f10.0)") &
                    !write(uout,*) &
                    !      "... found a movable atom # ",k," : '",element(j), &
                    !      "'  # ",j,"  of type ",eletype(j),", displ = ",distance_atm_max(j,:)

                    exit

                endif

            enddo

            if(.not.found) then

                call error(321)

            endif

            if( j > number_of_elements ) elmid = 0

            !found = .false.

            do j = 1, number_of_molecules

                !AB: old code - obsolete?
                !do i = 1, uniq_mol(j)%natom
                    !if(   uniq_mol(j)%atms(i)%atlabel == movetyp(k) &
                    !.and. uniq_mol(j)%atms(i)%atype == job%atmmover_type(k)) then

                    if( elmid > 0 ) then
                      if( cfgs(1)%mtypes(j)%num_elems(elmid) > 0 ) then

                        founda = .true.

                        if( cfgs(1)%mtypes(j)%rigid_body ) then

                            call cry(uout,'', &
                                 "ERROR: molecule '"//trim(adjustL(uniq_mol(j)%molname))//&
                                 &"' is rigid but its atom(s) are requested to move !!!",999)

                        else if( .not.cfgs(1)%mtypes(j)%atoms_move ) then

                            do ic = 1, nconfigs

                               cfgs(ic)%mtypes(j)%atoms_move =  .true.
                               
                            enddo

                            nmol_atmove = nmol_atmove+1

!debugging
                            !write(uout,"(/,1x,2a,2(a,i5))") &
                            !     "... found species movable by atom: '",uniq_mol(j)%molname, &
                            !     "'  # ",j,", atom_typ = ",elmid

                        endif

!debugging               
!                if( master ) then
!                    if( found ) then
!                        write(uout,"(/,1x,5a,3i10)") "species '", &
!                             trim(uniq_mol(j)%molname),"' contains movable atom '",&
!                             trim(element(movetyp(k))),"' ",j,k,movetyp(k)
!
!                        flush(uout)
!                     else
!                         write(uout,"(/,1x,5a,2i10)") "species '", &
!                              trim(uniq_mol(j)%molname),"' does not contain movable atom '",&
!                              trim(element(movetyp(k))),"' ",k,movetyp(k)
!                    endif
!                endif

                        cycle !exit

                      endif

                    endif

                !enddo

                !founda = founda .or. found
                
            enddo

        enddo
        
        if( master ) then
            if( founda ) then

                write(uout,"(/,1x,a,i10)") "number of species with movable atoms = ", nmol_atmove

            else

                write(uout,"(/,1x,a)") "no species with movable atoms found"

            endif
        endif

        if( nmovetyp /= job%numatmmove ) &
            call cry(uout,'', &
                 "ERROR: atom moves requested in CONTROL do not correspond with topology in FIELD !!!",999)

    endif

    if (job%movemol) then

        allocate(no_mol_moves(number_of_molecules))
        allocate(distance_mol_max(number_of_molecules))
        allocate(attempted_mol_moves(number_of_molecules))
        
        allocate(molmovtyp(job%nummolmove))
        
        no_mol_moves(:) = 0.0_wp
        attempted_mol_moves(:) = 0
        distance_mol_max(:) = job%moldisp
        
        molmovtyp(:) = 0
        nmolmovtyp = 0 !number_of_molecules

        !set flag for moveable molecules
        do k = 1, job % nummolmove

            found = .false.

            do j = 1, number_of_molecules

                if (uniq_mol(j)%molname == job % molmover(k)) then

                    found = .true.
                    
                    nmolmovtyp = nmolmovtyp+1

                    molmovtyp(k) = j

                    do ic = 1, nconfigs
                       cfgs(ic)%mtypes(j)%translate =  .true.
                    enddo

                    exit

                endif

            enddo

            if (.not.found) call error(352)

            if( master ) then

                write(uout,"(/,1x,3a)") "species '", &
                    trim(uniq_mol(j)%molname),"' is movable by translation"

            endif

        enddo
 
        if( nmolmovtyp /= job%nummolmove ) &
            call cry(uout,'', &
                 "ERROR: molecule moves requested in CONTROL do not correspond with species in FIELD !!!",999)

    endif

    if (job%rotatemol) then

        allocate(no_mol_rots(number_of_molecules))
        allocate(rotate_mol_max(number_of_molecules))
        allocate(attempted_mol_rots(number_of_molecules))
        
        allocate(molrottyp(job%nummolrot))

        no_mol_rots(:) = 0.0_wp
        attempted_mol_rots(:) = 0
        rotate_mol_max(:) = job%molrot
        
        molrottyp(:) = 0
        nmolrottyp = 0 !number_of_molecules

        !set flag for moveable molecules
        do k = 1, job % nummolrot

            found = .false.

            do j = 1, number_of_molecules

                if (uniq_mol(j)%molname == job % molroter(k)) then

                    found = .true.
                    
                    nmolrottyp = nmolrottyp+1
                    
                    molrottyp(k) = j
                    
                    do ic = 1, nconfigs
                       cfgs(ic)%mtypes(j)%rotate =  .true.
                    enddo

                    exit

                endif

            enddo

            if (.not.found) call error(353)

            if( master ) then

                write(uout,"(/,1x,3a)") "species '", &
                    trim(uniq_mol(j)%molname),"' is movable by rotation"

            endif

        enddo
 
        if( nmolrottyp /= job%nummolrot ) &
            call cry(uout,'', &
                 "ERROR: molecule rotations requested in CONTROL do not correspond with species in FIELD !!!",999)

    endif
    

!scp  scanmol set number for scanning - hopefully 1
   if (job%scanmol) then
!scp working the step length shouldn't be done each step
      if(nconfigs.gt.1) then
             call cry(uout,'', &
                 "ERROR: scan selected for more than 1 configuration",999)     
      endif 
      job % max_scan=1.0_wp
      do na = 1,3
      RA(NA) = 0.0_wp
      do nb = 1,3
! box to be scanned - below is for full cell
!      RA(NA) = cfgs(ib)%vec%latvector(nb,na)*cfgs(ib)%vec%latvector(nb,na) + RA(NA)
!     modify in control - using "slab" : job % scan_svec() 
      RA(NA) = cfgs(nconfigs)%vec%latvector(nb,na)*cfgs(nconfigs)%vec%latvector(nb,na) + RA(NA)
      enddo
      RA(NA) = SQRT(RA(NA))*job%scan_svec(NA)
      job % scan_nstp(na) = ra(na)/job % scan_step
      job % scan_sstp(na) = ra(na)/job % scan_nstp(na)
      job % max_scan=job % max_scan*job % scan_nstp(na)
      enddo
       if(job % num_scan_stop.eq.0) job % num_scan_stop = job % max_scan

        allocate(no_mol_moves(number_of_molecules))
        allocate(distance_mol_max(number_of_molecules))
        allocate(attempted_mol_moves(number_of_molecules))
        
        allocate(molmovtyp(job%nummolmove))
        
        no_mol_moves(:) = 0.0_wp
        attempted_mol_moves(:) = 0
        distance_mol_max(:) = job%moldisp
        
        molmovtyp(:) = 0
        nmolmovtyp = 0 !number_of_molecules

        !set flag for moveable molecules
        do k = 1, job % nummolmove

            found = .false.

            do j = 1, number_of_molecules

                if (uniq_mol(j)%molname == job % molmover(k)) then

                    found = .true.
                    
                    nmolmovtyp = nmolmovtyp+1

                    molmovtyp(k) = j

                    do ic = 1, nconfigs
                       cfgs(ic)%mtypes(j)%translate =  .true.
                    enddo

                    exit

                endif

            enddo

            if (.not.found) call error(352)

            if( master ) then

                write(uout,"(/,1x,3a)") "species '", &
                    trim(uniq_mol(j)%molname),"' is movable by translation"

            endif

        enddo
 
        if( nmolmovtyp /= job%nummolmove ) &
            call cry(uout,'', &
                 "ERROR: molecule moves requested in CONTROL do not correspond with species in FIELD !!!",999)

    endif


    if( job%type_vol_move /= 0 ) then 

        allocate(maxvolchange(9,nconfig))
        allocate(attemptedvolchange(9,nconfig))
        allocate(novolchange(9,nconfig))

        novolchange = 0.0_wp
        attemptedvolchange = 0
        maxvolchange = job%maxvolchange

        !AB: volume distribution histogram(s) (to do!)

        if( job%lsample_volume ) then

            vol_max = 0.0_wp

            do ic = 1, nconfigs
               vol_max = max(vol_max, cfgs(ic)%vec%volume)
            enddo

            !AB: allow at least double of the initial max volume
            vol_max = 2.0_wp*vol_max

            !max_vol_bin = vol_max / job%vol_bin

            !if( job%vol_bin_power > 1.0_wp ) &
            max_vol_bin = (vol_max/job%vol_bin)**(1.0_wp/job%vol_bin_power)

            allocate(volhist(max_vol_bin,nconfig))
            volhist = 0.0_wp

        endif

    endif

    successful_atm_moves = 0
    total_atm_moves = 0
    successful_mol_moves = 0
    total_mol_moves = 0
    successful_mol_rots = 0
    total_mol_rots = 0
    totalvolchanges = 0
    successfulvolchange = 0
    empty_atm_moves = 0
    empty_mol_moves = 0

    if(job%swapatoms .and. job%swapmols) call error(336)

    if(job%swapatoms) then

        allocate(swaptype1(job%numswap))
        allocate(swaptype2(job%numswap))

        if (job%exchangebias) then

            allocate(swaplookup1(job%swapfreq, job%numswap))
            allocate(swaplookup2(job%swapfreq, job%numswap))
            swaplookup1 = 0
            swaplookup2 = 0
        endif

        do j = 1, number_of_elements

            do k = 1, job%numswap

               if (element(j) == job%swapele1(k) .and. eletype(j) == job%swapele1_type(k)) then
                   swaptype1(k) = j
               endif

               if (element(j) == job%swapele2(k) .and. eletype(j) == job%swapele2_type(k)) then
                   swaptype2(k) = j
               endif

            enddo
        enddo

        do k = 1, job%numswap

            i = swaptype1(k)
            j = swaptype2(k)

            !if (atm_charge(i) /= atm_charge(j)) call error(320)

        enddo

        attemptedswaps = 0
        successfulswaps = 0

        allocate(attemptedswaps_atm(job%numswap,nconfigs))
        allocate(successfulswaps_atm(job%numswap,nconfigs))
        attemptedswaps_atm = 0
        successfulswaps_atm = 0

        !TU: If the frequency for any type of atom swap move is not explicitly specified in
        !TU: CONTROL, then job % atmswapfreq(i) for some 'i'. In this case perform all types
        !TU: of swap move with the same frequency (which is trivial for only one type of swap
        !TU: move - so we don't say anything in 'uout' in this case)
        if( any( job%atmswapfreq == 0 ) ) then

            job%atmswapfreq = 1

            if(job%numswap > 1) then
                call cry(uout,'', &
                    "WARNING: Frequencies of all types of atom swap move will be the same. "// &
                    "Consider setting frequencies explicitly in CONTROL",0)
            end if
                
        end if

        allocate(atmswapfreq_thresh(job%numswap))
        atmswapfreq_thresh(1) = job%atmswapfreq(1) * 1.0_wp / sum(job%atmswapfreq)
        do i = 2, job%numswap
            
            atmswapfreq_thresh(i) = atmswapfreq_thresh(i-1) + job%atmswapfreq(i) * 1.0_wp / sum(job%atmswapfreq)

        end do

        if (master) write(uout,"(/,1x,'atom positions will be exchanged: types swapped, relative frequency')")

        do i = 1, job%numswap

            tag1 = set_species_type(job%swapele1_type(i))
            tag2 = set_species_type(job%swapele2_type(i))

            if (master) write(uout,'(1x,a8,1x,a1,1x,a8,1x,a1,i6)') job%swapele1(i), tag1, job%swapele2(i), tag2, &
                job%atmswapfreq(i)

        enddo

        
    endif

    if(job%swapmols) then

        allocate(swaptype1(job%numswap))
        allocate(swaptype2(job%numswap))

        if (job%exchangebias) then

            allocate(swaplookup1(job%swapfreq, job%numswap))
            allocate(swaplookup2(job%swapfreq, job%numswap))
            swaplookup1 = 0
            swaplookup2 = 0
        endif

        do j = 1, number_of_molecules

            do k = 1, job%numswap

               if (uniq_mol(j)%molname == job%swapele1(k)) then
                   swaptype1(k) = j
               endif

               if (uniq_mol(j)%molname == job%swapele2(k)) then
                   swaptype2(k) = j
               endif

            enddo
        enddo

        attemptedswaps = 0
        successfulswaps = 0

        if (master) write(uout,"(/,1x,'molecule positions will be exchanged: types swapped')")

        do i = 1, job%numswap

            if (master) write(uout,'(1x,a8,1x,a8)') uniq_mol(i)%molname, job%swapele2(i)

        enddo

    endif

    if(job%gcmcatom) then

        if (master) write(uout,"(/,1x,'Grand Canonical (GCMC) ensemble using atoms')")

        if (master) write(uout,"(/,1x,'the number of different atom types               = ',i5)") &
                          job%numgcmcatom

        if (master) write(uout,"(/,1x,'the frequency of atom insertions/deletions       = ',i5)") &
                          job%gcmcatomfreq

        if (master) write(uout,"(/,1x,'the pair overlap (core) distance for insertions  = ',f10.4)") &
                          job%gcmcmindist

        allocate(gcmclookup(job%numgcmcatom))
        allocate(gcmc_pot(job%numgcmcatom))

        i = 0

        do k = 1, job%numgcmcatom

            found = .false.

            do j = 1, number_of_elements

!               write(uout,'(/,1x,a,i3,2(3a,i3),a,i3)') "Looking at ",k,"-th atom for GCMC: '",&
!                     job%gcmcatminsert(k),"' of type: ",job%gcmcatminsert_type(k)," cf: '",&
!                     element(j),"' / ",eletype(j)," =?= ",j

               if (element(j) == job%gcmcatminsert(k) .and. &
                   eletype(j) == job%gcmcatminsert_type(k)) then

                   gcmclookup(k) = j

                   found = .true.
                   i = i+1
!                   write(uout,'(1x,a,i3,3a,i3,a,i3)') "...found ",i,"-th atom for GCMC: '",&
!                         job%gcmcatminsert(k),"' of type: ",job%gcmcatminsert_type(k)," =?= ",gcmclookup(k)
               endif

            enddo

            if (.not.found) call error(312)

        enddo

        if( master .and. i /= job%numgcmcatom ) then

            write(uout,'(/,1x,2(a,i8),/)')&
                 "Number of atoms found for GCMC inconsistent: ",i," =/= ",job%numgcmcatom

            call error(999)

        endif

        attemptedgcatminsert = 0
        attemptedgcatmremove = 0
        sucessfulgcatminsert = 0
        sucessfulgcatmremove = 0
        emptygcatmremove = 0

!AB: original version:
!        if(job%usegaspres) then
!            if (master) write(uout,"(/,1x,'grand canonical monte carlo: types inserted using partial pressure (katms)')")
!            do i = 1, job%numgcmcatom
!                tag1 = set_species_type(job%gcmcatminsert_type(i))
!                if (master) write(uout,'(1x,a8,1x,a1,1x,f10.4)') job%gcmcatminsert(i), tag1, job%gcmc_atompot(i)
!                gcmc_pot(i) = job%gcmc_atompot(i) / PRSFACT
!            enddo
!        else
!            if (master) write(uout,"(/,1x,'grand canonical monte carlo: types inserted using Adams style (zeta)')")
!            do i = 1, job%numgcmcatom
!                tag1 = set_species_type(job%gcmcatminsert_type(i))
!                if (master) write(uout,'(1x,a8,1x,a1,1x,f10.4)') job%gcmcatminsert(i), tag1, job%gcmc_atompot(i)
!                gcmc_pot(i) = job%gcmc_atompot(i)
!            enddo
!        endif

!AB: new version with more options:
        if( job % usegaspres ) then

            if( master ) write(uout,"(/,1x,'the partial gas pressure of atoms (katms)        :')")

            do  i = 1, job%numgcmcatom

                tag1 = set_species_type(job%gcmcatminsert_type(i))
                if (master) write(uout,'(1x,a8,1x,a1,1x,f10.4)') job%gcmcatminsert(i), tag1, job%gcmc_atompot(i)

                gcmc_pot(i) = job%gcmc_atompot(i) / PRSFACT

            end do

        else if( job % usechempotkt ) then

            if( master ) write(uout,"(/,1x,'the chemical potential of atoms  (katms)         :')")

            do  i = 1, job%numgcmcatom

                tag1 = set_species_type(job%gcmcatminsert_type(i))
                if (master) write(uout,'(1x,a8,1x,a1,1x,f10.4)') job%gcmcatminsert(i), tag1, job%gcmc_atompot(i)

                gcmc_pot(i) = job%gcmc_atompot(i)

            end do

        else if( job % usechempotkj ) then

            if( master ) write(uout,"(/,1x,'Adams/Mezei zeta (activity) of atoms (kJ/mol)    :')")

            do  i = 1, job%numgcmcatom

                tag1 = set_species_type(job%gcmcatminsert_type(i))
                if (master) write(uout,'(1x,a8,1x,a1,1x,f10.4)') job%gcmcatminsert(i), tag1, job%gcmc_atompot(i)

                gcmc_pot(i) = job%gcmc_atompot(i) * job%energyunit

            end do

        else

            if( master ) write(uout,"(/,1x,'Fuchs/Muller/Frenkel activity of atoms (deca-J)  : ')")

            do i = 1, job%numgcmcatom

                tag1 = set_species_type(job%gcmcatminsert_type(i))
                if (master) write(uout,'(1x,a8,1x,a1,1x,f10.4)') job%gcmcatminsert(i), tag1, job%gcmc_atompot(i)

                gcmc_pot(i) = job%gcmc_atompot(i)

            end do

        end if

        if( job%type_vol_move /= 0 ) then

            call cry(uout,'', &
                    "WARNING: Grand Canonical MC combined with NpT ensemble can destabilise the system - use with caution!!!",0)

            !if (master) write(uout,"(/,1x,'grand canonical monte carlo: volume must be held constant')")

            !job%type_vol_move =  0

        end if

    end if

    if(job%gibbsatomtran) then

        allocate(gibbstrans_atmlookup(job%num_atmtran_gibbs))


        do k = 1, job%num_atmtran_gibbs

            found = .false.

            do j = 1, number_of_elements

               if (element(j) == job%gibbsatmtrans(k) .and. eletype(j) == job%gibbsatmtrans_type(k)) then
                   gibbstrans_atmlookup(k) = j
                   found = .true.
               endif

               if (.not.found) call error(312)

            enddo

        enddo

    endif

    if (job%gibbsatomtran) then

!       if(job%sysensemble /= "npt1") then
!            write(uout,*) " *** error: gibbs ensemble must have ensemble npt1"
!            stop
!       endif

        if (master) write(uout,"(/,1x,'gibbs ensemble monte carlo: types transfered')")

        do i = 1, job%num_atmtran_gibbs

            if (master) write(uout,'(/,1x,a8)')job%gibbsatmtrans(i)

        enddo

    endif

    if (job%gibbsatomexch) then

        allocate(gibbsexch_atmlookup1(job%num_atmexch_gibbs))
        allocate(gibbsexch_atmlookup2(job%num_atmexch_gibbs))

        do k = 1, job%num_atmexch_gibbs

            found = .false.

            do j = 1, number_of_elements

               if (element(j) == job%gibbsatmexchange1(k) .and. eletype(j) == job%gibbsatmexchange_type1(k)) then
                   gibbsexch_atmlookup1(k) = j
                   found = .true.
               endif

               if (.not.found) call error(349)

               if (element(j) == job%gibbsatmexchange2(k) .and. eletype(j) == job%gibbsatmexchange_type2(k)) then
                   gibbsexch_atmlookup2(k) = j
                   found = .true.
               endif

               if (.not.found) call error(349)

            enddo

        enddo

        do k = 1, job%num_atmexch_gibbs

            i = gibbsexch_atmlookup1(k)
            j = gibbsexch_atmlookup2(k)

            if (atm_charge(i) /= atm_charge(j)) call error(350)

        enddo

        if (master) write(uout,"(/,1x,'gibbs ensemble monte carlo: types exchaned')")

        do i = 1, job%num_atmexch_gibbs

            if (master) write(uout,'(/,1x,a8,2x,a8)')job%gibbsatmexchange1(i), job%gibbsatmexchange2

        enddo


    endif

    if(job%gibbsmoltran) then

        allocate(gibbstrans_mollookup(job%num_moltran_gibbs))

        
        do j = 1, number_of_molecules

            do k = 1, job%num_moltran_gibbs

               if (uniq_mol(j)%molname == job%gibbsmolinsert(k)) then
                   gibbstrans_mollookup(k) = j
               endif

            enddo

        enddo

    endif

    if(job%semigrandatms.and.job%semiwidomatms) call error(322)

    if (job%gcmcatom .and. job%gcmcmol) call error(323)

    if (job % gcmcmol) then

        if (nconfig > 1) call error(301)

        num_gcmc_mols = job%numgcmcmol

        if (master) write(uout,"(/,1x,'Grand Canonical (GCMC) ensemble using molecules')")

        if (master) write(uout,"(/,1x,'the number of different molecule types           = ',i5)") job%numgcmcmol

        if (master) write(uout,"(/,1x,'the frequency of mol insert/delete               = ',i5)") job%gcmcmolfreq

        if (master) write(uout,"(/,1x,'the minimum distance for inserts                 = ',f10.4)") job%gcmcmindist


        allocate(gcmc_pot(num_gcmc_mols))
        allocate(gcinsert_molecules(num_gcmc_mols))

        !setup the molecules to be inserted and report if one not found
        do i = 1, job%numgcmcmol

            found = .false.

            do j = 1, number_of_molecules

                 if (job%gcinsert_mols(i) == uniq_mol(j)%molname) then

                     found = .true.
                     gcinsert_molecules(i) = j

                 endif

            enddo

            if (.not.found) call error(234)

        enddo


        do i = 1, job%numgcmcmol

            elmid = gcinsert_molecules(i)

            if (master) write(uout,"(/,1x,'the number of atoms in molecule ',i3,' is ',i5)")i,  uniq_mol(elmid)%natom

            if (master) write(uout,"(/,1x,'the maximum number of atoms in molecule          = ',i5)") &
                             uniq_mol(elmid)%mxatom

            if (master) write(uout,"(/,1x,'molecule name ',a8)") uniq_mol(elmid)%molname

            if(job % usegaspres) then

                if (master) write(uout,"(/,1x,'the partial gas pressure of molecules (katms)    = ',f14.7)")job%gcmc_molpot(i)

                gcmc_pot(i) = job%gcmc_molpot(i) / PRSFACT

            else if(job % usechempotkt) then

                if (master) write(uout,"(/,1x,'the chemical potential of molecules   (kT)       = ',f14.7)")job%gcmc_molpot(i)

                gcmc_pot(i) = job%gcmc_molpot(i)

            else if(job % usechempotkj) then

                if (master) write(uout,"(/,1x,'Adams/Mezei zeta (activity) of molecs (kJ/mol)   = ',f14.7)")job%gcmc_molpot(i)

                gcmc_pot(i) = job%gcmc_molpot(i) * job%energyunit

            else

                if (master) write(uout,"(/,1x,'Fuchs/Muller/Frenkel activity of molecs (deca-J) = ',f10.4)")job%gcmc_molpot(i)

                gcmc_pot(i) = job%gcmc_molpot(i)

            endif

        enddo

        if( job%type_vol_move /= 0 ) then

            call cry(uout,'', &
                    "WARNING: Grand Canonical MC combined with NpT ensemble can destabilise the system - use with caution!!!",0)

            !if (master) write(uout,"(/,1x,'grand canonical monte carlo: volume must be held constant')")

            !job%type_vol_move =  0

        end if

        attemptedgcmolinsert = 0
        attemptedgcmolremove = 0
        sucessfulgcmolinsert = 0
        sucessfulgcmolremove = 0
        emptygcmolremove = 0

    endif

    if(job%semigrandatms.or.job%gibbsatomtran.or.job%gcmcatom) then

        if(numang > 0) call warning(1)

    endif


    !TU: Initialise PSMC move counters
    if( fed%flavor == FED_PS ) then
       
       call initialise_psmc_counters()
       
    end if
    

end subroutine

!> creates arrays and constructs mc move list to maintain detailed balance
subroutine create_mc_moves(job, lheat)

    use kinds_f90
    use constants_module, only : uout
    use control_type
    use fed_interface_module
    use psmc_module, only : psmc

    implicit none

    type(control), intent(inout) :: job
    logical, intent(in) :: lheat

    integer :: i, j

    !TU-: Do we really need the 'lheat' branch? Is it just an extra branch to maintain which
    !TU-: is unnecessary, given that its behaviour could be reproduced by changing the active
    !TU-: moves in CONTROL, and running a short preliminary 'lheat' simulation? I don't think
    !TU-: it has been used in a real simulation in years.
    
    num_mc_moves = 0
    
    if (allocated(mc_move_list)) deallocate (mc_move_list)

    if(lheat) then

        num_mc_moves = job%vol_change_freq + job%atmmovefreq + job%molmovefreq + job%molrotfreq   &
       &          + job%swapfreq + job%gibbs_atmexch_freq

    else

        num_mc_moves = job%vol_change_freq + job%atmmovefreq + job%molmovefreq + job%molrotfreq   &
                  + job%atmrotfreq + job%semiwidomatmsfreq + job%swapfreq + job%gcmcatomfreq &
                  + job%gibbs_atmtran_freq + job%gibbs_atmexch_freq + job%gcmcmolfreq &
                  + job%semigrandatmsfreq + job%semigrandmolfreq + job%gibbs_moltran_freq &
                  + psmc%switch_freq

        !JG: Currently total move num must be set to 100 as per below conditional
        ! If atom numbers is significantly bigger this causes issues wrt volume moves
        ! These take far longer espcially in a charged world so comment out statement
        ! This ensures backwards compatibilty of all tests and existing list implementation
        ! Adding alternative exceptions 0, also added at 10^5 to prevent excess memory usage 
        ! if (num_mc_moves /= 100) call error(236)
!scp        if (num_mc_moves < 1 .or. num_mc_moves > 1e5) call error(236)  !to allow a "no move" CONTROL to print an initial energy
if (num_mc_moves > 1e5) call error(236)
if (num_mc_moves < 1 ) num_mc_moves = 1     !to ensure that initial energy given if no moves.

    endif


    ! dimension the size of the list
    allocate(mc_move_list(num_mc_moves))

    if (lheat) then

        j = 1
        if (job%moveatm) then
            do i = 1, job%atmmovefreq
                !TU: Use PSMC atom move instead of 'normal' atom move in PSMC (label 101)
                if( fed%flavor == FED_PS ) then
                   mc_move_list(j) = 101
                else
                   mc_move_list(j) = 1
                end if
                j = j + 1
            enddo
        endif

        if (job%type_vol_move /= 0) then

             do i = 1, job%vol_change_freq
                !TU: Use PSMC volume move instead of 'normal' volume move in PSMC (label 104)
                if( fed%flavor == FED_PS ) then
                   mc_move_list(j) = 104
                else
                   mc_move_list(j) = 4
                end if
                j = j + 1
            enddo

        endif

        if (job%movemol) then
            do i = 1, job%molmovefreq
                !TU: Use PSMC molecule move instead of 'normal' molecule move in PSMC (label 102)
                if( fed%flavor == FED_PS ) then
                   mc_move_list(j) = 102
                else
                   mc_move_list(j) = 2
                end if
                j = j + 1
            enddo
        endif

        if (job%rotatemol) then
            do i = 1, job%molrotfreq
                !TU: Use PSMC molecule rotation move instead of 'normal' rotation move in PSMC (label 103)
                if( fed%flavor == FED_PS ) then
                   mc_move_list(j) = 103
                else
                   mc_move_list(j) = 3
                end if
                j = j +1
            enddo
        endif

        if (job%rotateatm) then
            do i = 1, job%atmrotfreq
                !!TU: Use PSMC molecule rotation move instead of 'normal' rotation move in PSMC (label 103)
                !if( fed%flavor == FED_PS ) then
                !   mc_move_list(j) = 116
                !else
                mc_move_list(j) = 16
                !end if
                j = j +1
            enddo
        endif
        
        if (job%swapatoms) then
            do i = 1, job%swapfreq
                mc_move_list(j) = 6
                j = j + 1
            enddo
        endif

        if (job%swapmols) then
            do i = 1, job%swapfreq
                mc_move_list(j) = 13
                j = j + 1
            enddo
        endif

    else

        j = 1
        if (job%moveatm) then
            do i = 1, job%atmmovefreq
                !TU: Use PSMC atom move instead of 'normal' atom move in PSMC (label 101)
                if( fed%flavor == FED_PS ) then
                   mc_move_list(j) = 101
                else
                   mc_move_list(j) = 1
                end if
                j = j + 1
            enddo
        endif

        if (job%movemol) then
            do i = 1, job%molmovefreq
                !TU: Use PSMC molecule move instead of 'normal' molecule move in PSMC (label 102)
                if( fed%flavor == FED_PS ) then
                   mc_move_list(j) = 102
                else
                   mc_move_list(j) = 2
                end if
                j = j + 1
            enddo
        endif

        if (job%rotatemol) then
            do i = 1, job%molrotfreq
                !TU: Use PSMC molecule rotation move instead of 'normal' rotation move in PSMC (label 103)
                if( fed%flavor == FED_PS ) then
                   mc_move_list(j) = 103
                else
                   mc_move_list(j) = 3
                end if
                j = j +1
            enddo
        endif

        if (job%rotateatm) then
            do i = 1, job%atmrotfreq
                !!TU: Use PSMC molecule rotation move instead of 'normal' rotation move in PSMC (label 103)
                !if( fed%flavor == FED_PS ) then
                !   mc_move_list(j) = 116
                !else
                mc_move_list(j) = 16
                !end if
                j = j +1
            enddo
        endif

        if (job%type_vol_move /= 0) then

           do i = 1, job%vol_change_freq
               !TU: Use PSMC volume move instead of 'normal' volume move in PSMC (label 104)
               if( fed%flavor == FED_PS ) then
                   mc_move_list(j) = 104
               else
                   mc_move_list(j) = 4
               end if
               j = j +1
            enddo

        endif

        if (job%gcmcatom)then
            do i = 1, job%gcmcatomfreq
                mc_move_list(j) = 5
                j = j + 1
            enddo
        endif

        if (job%swapatoms) then
            do i = 1, job%swapfreq
                mc_move_list(j) = 6
                j = j + 1
            enddo
        endif

        if (job%semiwidomatms) then
            do i = 1, job%semiwidomatmsfreq
                mc_move_list(j) = 7
                j = j + 1
            enddo
        endif

        if (job%gibbsatomtran) then
            do i = 1, job%gibbs_atmtran_freq
                mc_move_list(j) = 9
                j = j +1
            enddo
        endif

        if (job%gibbsatomexch) then
            do i = 1, job%gibbs_atmexch_freq
                mc_move_list(j) = 10
                j = j +1
            enddo
        endif

        if (job%gcmcmol) then
            do i = 1, job%gcmcmolfreq
                mc_move_list(j) = 11
                j = j + 1
            enddo
        endif

        if (job%semigrandatms) then
            do i = 1, job%semigrandatmsfreq
                mc_move_list(j) = 12
                j = j + 1
            enddo
        endif

        if (job%semigrandmols) then
            do i = 1, job%semigrandmolfreq
                mc_move_list(j) = 13
                j = j + 1
            enddo
        endif


        if (job%swapmols) then
            do i = 1, job%swapfreq
                mc_move_list(j) = 14
                j = j + 1
            enddo
        endif

        if (job%gibbsmoltran) then
            do i = 1, job%gibbs_moltran_freq
                mc_move_list(j) = 15
                j = j + 1
            enddo
        endif

        !TU: Code for PSMC switch moves (label 100)
        if( fed%flavor == FED_PS ) then
            do i = 1, psmc%switch_freq
                mc_move_list(j) = 100
                j = j + 1
            enddo
        end if

        !scp: scan move
        if (job%scanmol) then
              mc_move_list(j) = 17
              j = j + 1
        endif

    endif

end subroutine

subroutine printtotal(i, energytot, energyrcp, energyreal, energyvdw, &
                         energypair, energythree, energyang, energyfour, energymany, &
                         energyext, energymfa, virialtot, vol, unit)

    use kinds_f90
    use constants_module, only : uout
    use comms_mpi_module, only : master
    !use parallel_loop_module, only : master

    implicit none

    integer, intent(in) :: i
    real(kind = wp), intent(in) :: energytot, energyrcp, energyreal, energyvdw, &
                                   energypair, energythree, energyang, energyfour, energymany, &
                                   energyext, energymfa, virialtot, vol, unit

    if (master) then

        write(uout,"(/,1x,'break down of energies for box: ',i3)")i
        write(uout,"(/,1x,'total energy                    ',e20.10)") energytot / unit
        write(uout,"(1x,'reciprocal space coulomb        ',e20.10)") energyrcp / unit
        write(uout,"(1x,'real space coulomb              ',e20.10)") energyreal / unit
        write(uout,"(1x,'external mfa coulomb            ',e20.10)") energymfa / unit
        write(uout,"(1x,'nonbonded two body (vdw)        ',e20.10)") energyvdw / unit
        write(uout,"(1x,'bonded two body (pair)          ',e20.10)") energypair / unit
        write(uout,"(1x,'nonbonded three body            ',e20.10)") energythree / unit
        write(uout,"(1x,'bonded three body (angle)       ',e20.10)") energyang  / unit
        write(uout,"(1x,'bonded four body (angle)        ',e20.10)") energyfour / unit
        write(uout,"(1x,'many body energy                ',e20.10)") energymany / unit
        write(uout,"(1x,'external potential energy       ',e20.10)") energyext / unit
        write(uout,"(1x,'total virial                    ',e20.10)") virialtot / unit
        write(uout,"(1x,'volume                          ',e20.10)") vol        

    endif

end subroutine

subroutine print_moltotal(i, moltot, mrealnonb, mrealbond, mrealself, mrealcrct, mrcp, mvdwn, mvdwb, mpair,  &
                                 mthree, mang, mfour, mmany, mext, unit)

    use kinds_f90
    use constants_module, only : uout
    use species_module, only : uniq_mol 
    use comms_mpi_module, only : master
    !use parallel_loop_module, only : master

    implicit none

    integer, intent(in) :: i
    real(kind = wp), intent(in) :: moltot, mrealnonb, mrealbond, mrcp, mvdwn, mvdwb, mpair,  &
                                 mthree, mang, mfour, mmany, mext, unit, mrealself, mrealcrct

    if (master) then

        write(uout,"(/,1x,'components of energies by  molecule: ',a8)")uniq_mol(i)%molname
        write(uout,"(/,1x,'total energy                    ',e20.10)") moltot / unit
        write(uout,"(1x,'reciprocal space coulomb        ',e20.10)") mrcp / unit
        write(uout,"(1x,'real space coulomb - other types',e20.10)") mrealnonb / unit
        write(uout,"(1x,'real space coulomb - same type  ',e20.10)") mrealbond / unit
        write(uout,"(1x,'real space coulomb - self energy',e20.10)") mrealself / unit
        write(uout,"(1x,'real space coulomb - corection  ',e20.10)") mrealcrct / unit
        write(uout,"(1x,'vdw - other types mol           ',e20.10)") mvdwn / unit
        write(uout,"(1x,'vdw - same type mol             ',e20.10)") mvdwb / unit
        write(uout,"(1x,'bonded two body (pair)          ',e20.10)") mpair / unit
        write(uout,"(1x,'nonbonded three body            ',e20.10)") mthree / unit
        write(uout,"(1x,'bonded three body (angle)       ',e20.10)") mang / unit
        write(uout,"(1x,'many body energy                ',e20.10)") mmany / unit
        write(uout,"(1x,'external potential energy       ',e20.10)") mext / unit

    endif

end subroutine

subroutine read_restart(atmmove, molmove, molrot)

    use kinds_f90
    use mc_moves, only : distance_atm_max, distance_mol_max, rotate_mol_max, read_distances
    use constants_module, only : urest, THIRD
    use comms_mpi_module, only : master

    implicit none

    logical, intent(in) :: atmmove, molmove, molrot

    !AB: the file(s) are open by not only the root (idnode == 0) but every workgroup master (if job%repexch)
    !AB: in this case it is clearer to simply call this routine by master(s) only

    !if( master ) then

        call read_distances(atmmove, molmove, molrot)

        !read(urevi,*) iter, distance_atm_max, distance_mol_max, rotate_mol_max

        !if(atmmove) read(urevi,*) distance_atm_max
        !if(molmove) read(urevi,*) distance_mol_max
        !if(molrot)  read(urevi,*) rotate_mol_max

    !endif

end subroutine

subroutine dump_restart(iter, ic, atmmove, molmove, molrot)

    use kinds_f90
    use mc_moves, only : distance_atm_max, distance_mol_max, rotate_mol_max, dump_distances
    use constants_module, only : urevi, THIRD
    use comms_mpi_module, only : master

    implicit none

    integer, intent(in) :: iter, ic
    logical, intent(in) :: atmmove, molmove, molrot

    !AB: the file(s) are open by not only the root (idnode == 0) but every workgroup master (if job%repexch)
    !AB: in this case it is clearer to simply call this routine by master(s) only

    !if( master ) then

        call dump_distances(atmmove, molmove, molrot)

        write(urevi,*) iter, distance_atm_max, distance_mol_max, rotate_mol_max

        !if(atmmove) write(urevi,*) distance_atm_max !, distance_atm_max/volume(ic)**THIRD
        !if(molmove) write(urevi,*) distance_mol_max !, distance_mol_max/volume(ic)**THIRD
        !if(molrot)  write(urevi,*) rotate_mol_max

    !endif

end subroutine

subroutine dump_energies(job, ib, iter)

    use kinds_f90
    use species_module
    use control_type
    use constants_module, only : ueng
    use cell_module
    use field_module
    use coul_module, only : atomself_energy
    use comms_mpi_module, only : master !, idnode, mxnode, gsync
    !use parallel_loop_module, only : master

    implicit none

    integer, intent(in) :: ib, iter
    type(control), intent(inout) :: job

    integer :: i, j, typ, atyp

    real(kind = wp) :: otot, ntot, oldreal, oldrcp, oldvdw, oldthree, oldpair, oldext, oldmfa, &
                       oldang, oldfour, oldmany, oldtotal, oldenth, oldvir, newrcp, newmany, pu, chgi, oself

    !temp vectors for molecular energies
    real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules), orcp(number_of_molecules), &
                       ovdwn(number_of_molecules), ovdwb(number_of_molecules), opair(number_of_molecules), &
                       othree(number_of_molecules),  oang(number_of_molecules), ofour(number_of_molecules), &
                       omany(number_of_molecules), nrcp(number_of_molecules), nmany(number_of_molecules), &
                       ocrct(number_of_molecules), oext(number_of_molecules)

    character*1 :: tag

    !AB: the file(s) are open by not only the root (idnode == 0) but every workgroup master (if job%repexch)
    !AB: in this case one CANNOT simply call this routine by master(s) only due to possible parallel energy calc loops

    !if (idnode == 0) write(ueng, "(1x,'ITERATION',2x,i10,2x,i5,1x,i3)") iter, cfgs(ib)%num_mols, ib
    if( master ) write(ueng, "(1x,'ITERATION',2x,i10,2x,i5,1x,i3)") iter, cfgs(ib)%num_mols, ib

    pu = 1.0_wp / job%energyunit

    do j = 1, cfgs(ib)%num_mols

        !if (idnode == 0) write(ueng, "(1x,'MOLECULE',2x,i10)") cfgs(ib)%mols(j)%natom 
        if( master ) write(ueng, "(1x,'MOLECULE',2x,i10)") cfgs(ib)%mols(j)%natom 

        do i = 1, cfgs(ib)%mols(j)%natom

            typ = cfgs(ib)%mols(j)%atms(i)%atlabel
            atyp = cfgs(ib)%mols(j)%atms(i)%atype

            oldenth = 0.0_wp
            oldvir = 0.0_wp
            oldreal = 0.0_wp
            oldrcp = 0.0_wp
            oldvdw = 0.0_wp
            oldthree = 0.0_wp
            oldpair = 0.0_wp
            oldang = 0.0_wp
            oldmany = 0.0_wp
            oldfour = 0.0_wp
            oldext = 0.0_wp
            oldmfa = 0.0_wp

            newrcp = 0.0_wp
            oself = 0.0_wp
            oext = 0.0_wp

            !get initial energy
            if (atyp /= ATM_TYPE_SEMI) then

                if(job%uselist) then

                    call atom_energy(ib, j, i, job%coultype(ib), &
                         job%dielec, job%vdw_cut, job%shortrangecut, job%verletshell, &
                         oldreal, oldvdw, oldthree, oldpair, oldang, oldfour, oldext, oldmfa, oldvir,  &
                         orealnonb, orealbond, ocrct, ovdwn, ovdwb, opair, othree, oang, ofour, oext, &
                         .true., .false.)

                else

                    call atom_energy_nolist(ib, j, i, job%coultype(ib), &
                         job%dielec, job%vdw_cut, job%shortrangecut, &
                         oldreal, oldvdw, oldthree, oldpair, oldang, oldfour, oldext, oldmfa, oldvir,  &
                         orealnonb, orealbond, ocrct, ovdwn, ovdwb, opair, othree, oang, ofour, oext, &
                         .true., .false.)

                endif

            else

                call atom_tersoff_energy(ib, j, i, job%shortrangecut, oldmany, oldvir)

            endif

            !calculate recip space terms
            if(job%coultype(ib) == 1) then

                call add_atom_recip(ib, j, i, oldrcp, newrcp, orcp, nrcp, job%lmoldata)
                
                chgi = cfgs(ib)%mols(j)%atms(i)%charge

                call atomself_energy(chgi, cfgs(ib)%cl%eta, job%dielec, oself)

            endif

            !calculate manybody terms if this is a metal
            if(atyp == ATM_TYPE_METAL) call atom_metal_energy(ib, j, i, job%shortrangecut, oldmany, newmany, omany, nmany)

            !now the recip space terms have been calculated
            !sum the old energies

!           oldtotal = oldreal + newrcp + oldvdw + oldthree + oldpair + oldang + oldmany
!           oldtotal = oldreal + newrcp + oldvdw + oldthree + oldang  
            oldtotal = (newrcp + oldreal + oldvdw) + oself * 2.0

            !if (idnode == 0) then
            if( master ) then

                tag = set_species_type(cfgs(ib)%mols(j)%atms(i)%atype)

                write(ueng, "(1x,a8,1x,a1,4f20.10)") element(typ), tag, &
                      cfgs(ib)%mols(j)%atms(i)%rpos(:), oldtotal * pu

            endif

        enddo

    enddo

end subroutine

end module 
