!////////////////////////////////////////////////////////////////////////////////
!//                                                                            //
!//   Copyright (C) Dr Andrey V Brukhno - 2016                                 //
!//   abrukhno[@]gmail.com                                                     //
!//                                                                            //
!//   - general DCD formatted trajectory writing & reading rotuines            //
!//                                                                            //
!////////////////////////////////////////////////////////////////////////////////

!> @modulefor manipulating DCD trajectory files

module dcd_format_module

    use kinds_f90
    !use constants_module.f90, only :: iout, idcd

    !AB: decide later on dependencies
    !use cell_module   
    !use config_type

    implicit none

    integer, parameter :: iout = 6
    !integer, parameter :: idcd = 33

contains


!AB: <<-- dealing with trajectory in DCD format

!AB: <- writing trajectory in DCD format

subroutine dcd_open_for_writing(idcd, fname, title, header, iclose, &
                                natoms, nframes, ifstart, ifstep, icell, dtime)

    implicit none

    !> file unit for DCD trajectory file 
    integer*4, intent(in) :: idcd

    !> name of DCD trajectory file 
    character*(*), intent(in) :: fname

    !> title of DCD trajectory file 
    character*(80), intent(in) ::  title(2)

    !> four symbols PDB header (normally 'CORD')
    character*4, intent(in) ::  header

    !> flag for closing the file straight away (only DCD header written)
    integer*4, intent(inout) :: iclose

    !> number of atoms and frames in DCD trajectory file
    integer*4, intent(in) :: natoms, nframes, ifstart, ifstep

    !> flag for variable unit cell (i.e. cell matrix)
    integer*4, intent(in) :: icell

    !> MD time step (0 for MC)
    real*4, intent(in) :: dtime

    ! control sequence and other variables
    integer*4 :: cntrl1(9), cntrl2(10), ntitle, i

    !open(unit=idcd, file=trim(adjustL(fname)), form='unformatted', status='new', ACTION="WRITE", position='append', err=99)
    open(idcd, file=trim(adjustL(fname)), form='unformatted', status='replace', ACTION="WRITE", position='append', err=99)

    cntrl1 = 0
    cntrl2 = 0

    cntrl1(1) = nframes        ! number of frames (atom sets)
    cntrl1(2) = ifstart        ! starting step  (frame index)
    cntrl1(3) = ifstep         ! frame stepping (skip number)
    cntrl1(4) = ifstart+ifstep*(nframes-1) ! final number of steps (ifstart+ifstep*(nframes-1))
    !cntrl1(5) = 5              ! ??? - reserved but not used ???
    !cntrl1(6) = 6              ! ??? - reserved but not used ???
    !cntrl1(7) = 7              ! ??? - reserved but not used ???
    !cntrl1(8) = 8              ! number of degrees of freedom (do we need it?)
    !cntrl1(9) = 0              ! number of fixed atoms (in the first frame only - unsupported yet)
    !cntrl(10) = dtime          ! MD time (delta t)*10./488.8780249 (CHARMM convention for ps)
    cntrl2(1) = icell          ! flag for inclusion of PBC unit cell (crystallographic group - unsupported yet)
    cntrl2(2) = natoms         ! number of atoms present
    !cntrl2(3) = 13             ! ??? - reserved but not used ??? (can be set to virt_atoms for GCMC)
    !cntrl2(4) = 14             ! ??? - reserved but not used ???
    !cntrl2(5) = 15             ! ??? - reserved but not used ???
    !cntrl2(6) = 16             ! ??? - reserved but not used ???
    !cntrl2(7) = 17             ! ??? - reserved but not used ???
    !cntrl2(8) = 18             ! ??? - reserved but not used ???
    !cntrl2(9) = 19             ! ??? - reserved but not used ???
    !cntrl2(10)= 34             ! version of the DCD creator (CHARMM/NAMD)
    cntrl2(10)= 20             ! DLM-2.0

    ! AB: convert t_step into CHARMM units so that wordom converts it back to picosecs:
    !*t_step = *t_step*10./488.8780249;
    !printf("\nread_config_dcd(..) - time step has been converted to %g  (CHARMm format)\n",*t_step);
    !
    ! AB: convert t_step into CHARMM units so that wordom converts it back to femtosecs:
    !*t_step = *t_step*10000./488.8780249;

    write(idcd, err=999) header, cntrl1, dtime*10./488.8780249, cntrl2 ! header[char*4] & control sequence[int*20]

    ntitle = 1
    write(idcd, err=999) ntitle, (adjustL(title(i)),i=1,ntitle)  ! title of the trajectory [char*20]
    write(idcd, err=999) natoms                                  ! number of atoms per frame

    write(iout,*)
    write(iout,*) "Opended DCD file : '"//trim(adjustL(fname))//"' for writing"
    write(iout,*) 'DCD file header  : # frames / 1st / step = ', nframes,' / ', ifstart,' / ', ifstep ! #_frames, start & step
    write(iout,*) 'DCD file header  : # atoms / cell = ', natoms,' / ', icell ! #_atoms & cell switch
    write(iout,*) 'Proceeding to add frame(s) to the DCD trajectory...'

    flush(iout)

    if( iclose > 0 ) close(idcd)

    return

99  close(idcd)
    write(iout,*)'Could not open DCD file (header): ',trim(adjustL(fname))
    flush(iout)
    iclose = 99
    !stop

    return

999 close(idcd)
    write(iout,*)'Could not write DCD file (header): ',trim(adjustL(fname))
    flush(iout)
    iclose = 999
    !stop

end subroutine dcd_open_for_writing


subroutine dcd_write_next_frame4(idcd, fname, iopen, iclose, &
                                natoms, nframes, icell, x, y, z, cell_vars)

    implicit none

    !> file unit for DCD trajectory file 
    integer*4, intent(in) :: idcd

    !> name of DCD trajectory file 
    character*(*), intent(in) :: fname

    !> flags for opening the file
    integer*4, intent(in) :: iopen

    !> flag for closing the file
    integer*4, intent(inout) :: iclose

    integer*4, intent(in) :: natoms, nframes, icell

    real*4, intent(in)    :: x(*), y(*), z(*)

    !AB: cell_vars(6) = A, B, C, alpha, beta, gamma (PDB unit cell)
    real*8, intent(in)    :: cell_vars(*)

    integer, save :: i = 0
    integer, save :: iframe = 0

    if( iopen > 0 ) then
    !AB: may need to re-open a (partial) DCD file (FRAMES without HEADER - ?)

        close(idcd)
        open(idcd, file=trim(adjustL(fname)), form='unformatted', status='old', ACTION="WRITE", position='append', err=99)

        write(iout,*)
        write(iout,*) "Reopended DCD file '"//trim(adjustL(fname))//"' for writing (icell = ",icell,")"
        write(iout,*) 'Now adding frame(s) to the DCD trajectory...'

        flush(iout)

    end if

    i = i+1

    !write(iout,*) "Proceeding to write frame ",i," to DCD trajectory (icell = ",icell,"; natoms = ",natoms,")"

    if( icell > 0 ) write(idcd, err=999) cell_vars(1:6)   ! if dcd contains cell (PBC) details

    write(idcd, err=999) x(1:natoms)

    !write(iout,*) "Wrote X vector to DCD trajectory (natoms = ",natoms,")"

    write(idcd, err=999) y(1:natoms)

    !write(iout,*) "Wrote Y vector to DCD trajectory (natoms = ",natoms,")"

    write(idcd, err=999) z(1:natoms)

    !write(iout,*) "Wrote Z vector to DCD trajectory (natoms = ",natoms,")"

    iframe = iframe+1

    !write(iout,*)
    !write(iout,*) 'Appended a new frame to DCD trajectory: # frame / frames (tot) = ',iframe,' / ',nframes

    flush(iout)

    if( iclose > 0 ) close(idcd)

    return

99  close(idcd)
    write(iout,*)'Could not open DCD file (frames): ',trim(adjustL(fname))
    flush(iout)
    iclose = 99
    !stop

    return

999 close(idcd)
    write(iout,*)'Could not write further DCD file (frames): ',trim(adjustL(fname)),' (',i,')'
    flush(iout)
    iclose = 999
    !stop

end subroutine dcd_write_next_frame4

subroutine dcd_write_next_frame8(idcd, fname, iopen, iclose, &
                                natoms, nframes, icell, x, y, z, cell_vars)

    implicit none

    !> file unit for DCD trajectory file 
    integer*4, intent(in) :: idcd

    !> name of DCD trajectory file 
    character*(*), intent(in) :: fname

    !> flags for opening the file
    integer*4, intent(in) :: iopen

    !> flag for closing the file
    integer*4, intent(inout) :: iclose

    integer*4, intent(in) :: natoms, nframes, icell

    real*8, intent(in)    :: x(*), y(*), z(*)

    !AB: cell_vars(6) = A, B, C, alpha, beta, gamma (PDB unit cell)
    real*8, intent(in)    :: cell_vars(*)

    integer, save :: i = 0
    integer, save :: iframe = 0

    if( iopen > 0 ) then
    !AB: may need to re-open a (partial) DCD file (FRAMES without HEADER - ?)

        close(idcd)
        open(idcd, file=trim(adjustL(fname)), form='unformatted', status='old', ACTION="WRITE", position='append', err=99)

        write(iout,*)
        write(iout,*) "Reopended DCD file '"//trim(adjustL(fname))//"' for writing (icell = ",icell,")"
        write(iout,*) 'Now adding frame(s) to the DCD trajectory...'

        flush(iout)

    end if

    i = i+1

    !write(iout,*) "Proceeding to write frame ",i," to DCD trajectory (icell = ",icell,"; natoms = ",natoms,")"

    if( icell > 0 ) write(idcd, err=999) cell_vars(1:6)   ! if dcd contains cell (PBC) details

    write(idcd, err=999) x(1:natoms)

    !write(iout,*) "Wrote X vector to DCD trajectory (natoms = ",natoms,")"

    write(idcd, err=999) y(1:natoms)

    !write(iout,*) "Wrote Y vector to DCD trajectory (natoms = ",natoms,")"

    write(idcd, err=999) z(1:natoms)

    !write(iout,*) "Wrote Z vector to DCD trajectory (natoms = ",natoms,")"

    iframe = iframe+1

    !write(iout,*)
    !write(iout,*) 'Appended a new frame to DCD trajectory: # frame / frames (tot) = ',iframe,' / ',nframes

    flush(iout)

    if( iclose > 0 ) close(idcd)

    return

99  close(idcd)
    write(iout,*)'Could not open DCD file (frames): ',trim(adjustL(fname))
    flush(iout)
    iclose = 99
    !stop

    return

999 close(idcd)
    write(iout,*)'Could not write further DCD file (frames): ',trim(adjustL(fname)),' (',i,')'
    flush(iout)
    iclose = 999
    !stop

end subroutine dcd_write_next_frame8

!AB: -> writing trajectory in DCD format


!AB: <- reading trajectory in DCD format

subroutine dcd_open_for_reading(idcd, fname, title, header, iclose, &
                                natoms, nframes, ifstart, ifstep, icell, dtime)

    implicit none

    !> file unit for DCD trajectory file 
    integer*4, intent(in) :: idcd

    !> name of DCD trajectory file 
    character*(*), intent(in) :: fname

    !> title of DCD trajectory file 
    character*(80), intent(inout) ::  title(2)

    !> four symbols PDB header (normally 'CORD')
    character*4, intent(inout) ::  header

    !> flag for closing the file straight away (only DCD header written)
    integer*4, intent(inout) :: iclose

    !> number of atoms and frames in DCD trajectory file
    integer*4, intent(inout) :: natoms, nframes, ifstart, ifstep

    !> flag for variable unit cell (i.e. cell matrix)
    integer*4, intent(inout) :: icell

    !> MD time step (0 for MC)
    real*4, intent(inout) :: dtime

    ! control sequence and other variables
    integer*4 :: cntrl1(9), cntrl2(10), ntitle, i

    open(idcd, file=trim(adjustL(fname)), form='unformatted', status='old', ACTION="READ", err=99)

    read(idcd, err=999) header, cntrl1, dtime, cntrl2  ! header[char*4] & control sequence[int*20]

    nframes = cntrl1(1)        ! number of frames (atom sets)
    ifstart = cntrl1(2)        ! starting frame index
    ifstep  = cntrl1(3)        ! frame stepping (skip number)

    icell = cntrl2(1)

    read(idcd, err=999) ntitle, (title(i),i=1,ntitle)  ! title of the trajectory [char*80]
    read(idcd, err=999) natoms                         ! number of atoms per frame

    !write(iout,*) natoms, nframes                ! #_frames & #_atoms

    write(iout,*)
    write(iout,*) "Opended DCD file : '"//trim(adjustL(fname))//"' for reading"
    write(iout,*) 'DCD file header  : # frames / 1st / step = ', nframes,' / ', ifstart,' / ', ifstep ! #_frames, start & step
    write(iout,*) 'DCD file header  : # atoms / cell = ', natoms,' / ', icell ! #_atoms & cell switch
    write(iout,*) 'Proceeding to read frame(s) from the DCD trajectory...'

    flush(iout)

    if( iclose > 0 ) close(idcd)

    return

99  close(idcd)
    write(iout,*)'Could not open DCD file (header): ',trim(adjustL(fname))
    flush(iout)
    iclose = 99
    !stop

    return

999 close(idcd)
    write(iout,*)'Could not read DCD file (header): ',trim(adjustL(fname))
    flush(iout)
    iclose = 999
    !stop

end subroutine dcd_open_for_reading

subroutine dcd_read_next_frame4(idcd, fname, iopen, iclose, &
                               natoms, icell, x, y, z, cell_vars)

    implicit none

    !> file unit for DCD trajectory file 
    integer*4, intent(in) :: idcd

    !> name of DCD trajectory file 
    character*(*), intent(in) :: fname

    !> flags for opening & closing the file straight away (only DCD header written)
    integer*4, intent(inout) :: iopen, iclose

    integer*4, intent(in) :: natoms, icell

    real*4, intent(inout) :: x(*), y(*), z(*)

    !AB: cell_vars(6) = A, B, C, alpha, beta, gamma (PDB unit cell)
    real*8, intent(inout) :: cell_vars(*)

    integer, save :: i = 0
    integer, save :: iframe = 0

    if( iopen > 0 ) then
    !AB: may need to open a partial DCD file (FRAMES without HEADER)

        close(idcd)
        open(unit=idcd, file=trim(adjustL(fname)), form='unformatted', status='old', err=99)

        write(iout,*)
        write(iout,*) "Reopended DCD file '"//trim(adjustL(fname))//"' for reading (icell = ",icell,")"
        write(iout,*) 'Now reading frame(s) from the DCD trajectory...'

        flush(iout)

    end if

    i = i+1

    !write(iout,*) "Proceeding to read frame ",i," from DCD trajectory (icell = ",icell,"; natoms = ",natoms,")"

    if( icell > 0 ) read(idcd, err=66) cell_vars(1:6)   ! if dcd contains cell (PBC) details

    read(idcd, err=999) x(1:natoms)

    !write(iout,*) "Read X vector from DCD trajectory (natoms = ",natoms,")"

    read(idcd, err=999) y(1:natoms)

    !write(iout,*) "Read Y vector from DCD trajectory (natoms = ",natoms,")"

    read(idcd, err=999) z(1:natoms)

    !write(iout,*) "Read Z vector from DCD trajectory (natoms = ",natoms,")"

    iframe = iframe+1

    !write(iout,*)
    !write(iout,*) 'Done reading a new frame from DCD trajectory: # frame / frames (tot) = ',iframe,' / ',nframes

    if( iclose > 0 ) close(idcd)

    return

99  close(idcd)
    write(iout,*)'Could not open DCD file (frames): ',trim(adjustL(fname))
    flush(iout)
    iclose = 99
    !stop

    return

66  close(idcd)
    write(iout,*)'Could not read further DCD file (frame): ',trim(adjustL(fname)),' (cell_vars)'
    flush(iout)
    iclose = 999
    !stop

    return

999 close(idcd)
    write(iout,*)'Could not read further DCD file (frame): ',trim(adjustL(fname)),' (xyz_coords)'
    flush(iout)
    iclose = 999

end subroutine dcd_read_next_frame4

subroutine dcd_read_next_frame8(idcd, fname, iopen, iclose, &
                               natoms, icell, x, y, z, cell_vars)

    implicit none

    !> file unit for DCD trajectory file 
    integer*4, intent(in) :: idcd

    !> name of DCD trajectory file 
    character*(*), intent(in) :: fname

    !> flags for opening & closing the file straight away (only DCD header written)
    integer*4, intent(inout) :: iopen, iclose

    integer*4, intent(in) :: natoms, icell

    real*8, intent(inout) :: x(*), y(*), z(*)

    !AB: cell_vars(6) = A, B, C, alpha, beta, gamma (PDB unit cell)
    real*8, intent(inout) :: cell_vars(*)

    integer, save :: i = 0
    integer, save :: iframe = 0

    if( iopen > 0 ) then
    !AB: may need to open a partial DCD file (FRAMES without HEADER)

        close(idcd)
        open(unit=idcd, file=trim(adjustL(fname)), form='unformatted', status='old', err=99)

        write(iout,*)
        write(iout,*) "Reopended DCD file '"//trim(adjustL(fname))//"' for reading (icell = ",icell,")"
        write(iout,*) 'Now reading frame(s) from the DCD trajectory...'

        flush(iout)

    end if

    i = i+1

    !write(iout,*) "Proceeding to read frame ",i," from DCD trajectory (icell = ",icell,"; natoms = ",natoms,")"

    if( icell > 0 ) read(idcd, err=66) cell_vars(1:6)   ! if dcd contains cell (PBC) details

    read(idcd, err=999) x(1:natoms)

    !write(iout,*) "Read X vector from DCD trajectory (natoms = ",natoms,")"

    read(idcd, err=999) y(1:natoms)

    !write(iout,*) "Read Y vector from DCD trajectory (natoms = ",natoms,")"

    read(idcd, err=999) z(1:natoms)

    !write(iout,*) "Read Z vector from DCD trajectory (natoms = ",natoms,")"

    iframe = iframe+1

    !write(iout,*)
    !write(iout,*) 'Done reading a new frame from DCD trajectory: # frame / frames (tot) = ',iframe,' / ',nframes

    if( iclose > 0 ) close(idcd)

    return

99  close(idcd)
    write(iout,*)'Could not open DCD file (frames): ',trim(adjustL(fname))
    flush(iout)
    iclose = 99
    !stop

    return

66  close(idcd)
    write(iout,*)'Could not read further DCD file (frame): ',trim(adjustL(fname)),' (cell_vars)'
    flush(iout)
    iclose = 999
    !stop

    return

999 close(idcd)
    write(iout,*)'Could not read further DCD file (frame): ',trim(adjustL(fname)),' (xyz_coords)'
    flush(iout)
    iclose = 999

end subroutine dcd_read_next_frame8

subroutine dcd_read_all_frames4(idcd, fname, iopen, iclose, &
                               natoms, nframes, icell, x, y, z) !,cell_vars)

    implicit none

    !> file unit for DCD trajectory file 
    integer*4, intent(in) :: idcd

    !> name of DCD trajectory file 
    character*(*), intent(in) :: fname

    !> flags for opening & closing the file straight away (only DCD header written)
    integer*4, intent(inout) :: iopen, iclose

    integer*4, intent(in) :: natoms,nframes,icell

    real*4, intent(inout) :: x(natoms,nframes), &
                             y(natoms,nframes), &
                             z(natoms,nframes)

    !real*8, intent(inout) :: cell_vars(6)

    real*4  :: temp(natoms)

    integer :: i,j

    if( iopen > 0 ) then
    !AB: may need to open a partial DCD file (FRAMES without HEADER)

        close(idcd)
        open(unit=idcd, file=trim(adjustL(fname)), form='unformatted', status='old', err=99)

        write(iout,*)
        write(iout,*) "Reopended DCD file '"//trim(adjustL(fname))//"' for reading (icell = ",icell,")"
        write(iout,*) 'Now reading all frames from the DCD trajectory...'

        flush(iout)

    end if

    do i=1,nframes

        !if( icell > 0 ) read(idcd, err=999) cell_vars   ! if dcd contains cell (PBC) details

        read(idcd, err=999) temp
        do j=1,natoms
          x(j,i)=temp(j)
        end do

        read(idcd, err=999) temp
        do j=1,natoms
          y(j,i)=temp(j)
        end do

        read(idcd, err=999) temp
        do j=1,natoms
          z(j,i)=temp(j)
        end do

    end do

    if( iclose > 0 ) close(idcd)

    return

99  close(idcd)
    write(iout,*)'Could not open DCD file (frames): ',trim(adjustL(fname))
    flush(iout)
    iclose = 99
    !stop

    return

999 close(idcd)
    write(iout,*)'Could not read further DCD file (frames): ',trim(adjustL(fname)),' (',i-1,')'
    flush(iout)
    iclose = 999
    !stop

end subroutine dcd_read_all_frames4

!AB: -> reading trajectory in DCD format

!AB: -->> dealing with trajectory in DCD format

end module dcd_format_module

