! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module comms_omp_module

    use kinds_f90

    implicit none

    integer, save :: idthread
    integer :: thread_num, mxthread

    !$OMP THREADPRIVATE (idthread)

contains

subroutine init_omp_comms

    use constants_module, only : uout
    use omp_lib

    implicit none


    idthread = 0
    thread_num = 1
    mxthread = 1

    !$OMP PARALLEL

    !$ idthread = omp_get_thread_num()
    !$ thread_num = omp_get_num_procs( )
    !$ mxthread = omp_get_num_threads( )

    !$OMP END PARALLEL

end subroutine


!subroutine mptime(tt)
!
!    use kinds_f90
!    use omp_lib
!
!    implicit none
!
!    real(kind = wp), intent(inout):: tt
!
!    tt = OMP_get_wtime()
!
!end subroutine

  subroutine mptime(wwtime)

    use kinds_f90

    Implicit None

    Real( Kind = wp ) :: wwtime

    Logical,     Save :: newjob
    Data                 newjob / .true. /
    Character,   Save :: date*8
    Data                 date / ' ' /
    Integer,     Save :: days
    Data                 days / 0 /

    Character         :: date1*8,time*10,zone*5
    Integer           :: value(1:8)

    If (newjob) Then
       newjob = .false.

       Call date_and_time(date,time,zone,value)

       wwtime = Real(value(5),wp)*3600.0_wp + Real(value(6),wp)*60.0_wp   + &
                   Real(value(7),wp)           + Real(value(8),wp)/1000.0_wp
    Else
       Call date_and_time(date1,time,zone,value)

! time-per-timestep & start-up and close-down times
! are assumed to be shorter than 24h

       If (date /= date1) Then
          date = date1
          days = days + 1
       End If

       wwtime = Real(value(5),wp)*3600.0_wp + Real(value(6),wp)*60.0_wp   + &
                   Real(value(7),wp)           + Real(value(8),wp)/1000.0_wp + &
                   Real(days,wp)*86400.0_wp

    End If

  End subroutine mptime

subroutine exit_comms()


end subroutine

end module
